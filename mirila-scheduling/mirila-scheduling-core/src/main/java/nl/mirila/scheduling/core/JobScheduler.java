package nl.mirila.scheduling.core;

import com.google.inject.Inject;
import nl.mirila.core.recurrence.patterns.RecurrencePattern;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDateTime;
import java.util.Optional;

/**
 * The {@link JobScheduler} schedules or reschedules a job for a new run.
 */
public class JobScheduler {

    private static final Logger logger = LogManager.getLogger(JobScheduler.class);

    private final JobManager manager;

    /**
     * Initialize a new instance.
     */
    @Inject
    public JobScheduler(JobManager manager) {
        this.manager = manager;
    }

    /**
     * Reschedule the given {@link Job}, basically cloning the given job, based on the {@link RecurrencePattern} of the
     * job. If the job does not have a recurrence pattern, or the pattern does not result in a new occurrence, nothing
     * is rescheduled.
     *
     * @param job The original job.
     * @return The newly created job.
     */
    public Optional<Job> reschedule(Job job) {
        LocalDateTime offset = (job.getRunAt().isAfter(LocalDateTime.now()))
                ? job.getRunAt()
                : LocalDateTime.now();

        Optional<LocalDateTime> optional = job.getPattern()
                .flatMap(pattern -> pattern.getNextOccurrence(offset));

        if (optional.isEmpty()) {
            logger.debug("No new occurrence found for '{}:{}' ({}).", job.getGroup(), job.getName(), job.getId());
            return Optional.empty();
        }

        Job next = Job.of(job.getGroup(), job.getName())
                .withStatus(JobStatus.SCHEDULED)
                .withRunAt(optional.get())
                .withPoolId(job.getPoolId().orElse(null))
                .withPattern(job.getPattern().orElse(null))
                .withData(job.getData().orElse(null));
        manager.persist(next);
        logger.debug("Rescheduled job '{}:{}' ({})=>({}).", job.getGroup(), job.getName(), job.getId(), next.getId());
        return Optional.of(next);
    }

    /**
     * Schedules the given {@link Job} for processing.
     * <p>
     * The job must be scheduled for now, or in the future, or it should contain a pattern which can be used to
     * calculate a future occurrence.
     *
     * @param job The job to schedule.
     */
    public void schedule(Job job) {
        if ((job.getPattern().isPresent()) && (!isNowOrInFuture(job.getRunAt()))) {
            Optional<LocalDateTime> optional = job.getPattern().get().getNextOccurrence();
            if (optional.isEmpty()) {
                logger.info("No new occurrence for job '{}:{}' ({}).", job.getGroup(), job.getName(), job.getId());
                return;
            }
            job.withRunAt(optional.get());
        }
        if (isNowOrInFuture(job.getRunAt())) {
            job.withStatus(JobStatus.SCHEDULED);
            manager.persist(job);
            logger.debug("Scheduled job '{}:{}' ({}).", job.getGroup(), job.getName(), job.getId());
        } else {
            logger.warn("Not scheduling job '{}:{}' ({}), as it is set to process in the past {}.",
                        job.getGroup(),
                        job.getName(),
                        job.getId(),
                        job.getRunAt());
        }
    }

    /**
     * Return true if the given timestamp is now or in the future. For now, we use a margin of 1 minute in the past.
     */
    private boolean isNowOrInFuture(LocalDateTime timestamp) {
        return (LocalDateTime.now().minusMinutes(1).isBefore(timestamp));
    }

}
