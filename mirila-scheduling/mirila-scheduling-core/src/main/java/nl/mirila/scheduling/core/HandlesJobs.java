package nl.mirila.scheduling.core;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This {@link HandlesJobs} annotation provides the group and name for which a {@link JobHandler} processes.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface HandlesJobs {

    /**
     * Return the group of the jobs that this annotated {@link JobHandler} processes.
     */
    String group();

    /**
     * Return the name of the jobs that this annotated {@link JobHandler} processes.
     */
    String name() default JobHandlerRegistry.WILDCARD;

}
