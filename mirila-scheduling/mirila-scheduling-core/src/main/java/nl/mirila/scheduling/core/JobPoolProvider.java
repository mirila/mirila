package nl.mirila.scheduling.core;

import com.google.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.concurrent.BasicThreadFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import static nl.mirila.scheduling.core.SchedulerSettings.DEFAULT_POOL_ID;

/**
 * The {@link JobPoolProvider} provides thread pools, in the form of {@link ExecutorService} for processing {@link Job}.
 * The size and name of the pools can be configured in the settings, and are provided by the {@link SchedulerSettings}.
 */
public class JobPoolProvider {

    private static final Logger logger = LogManager.getLogger(JobPoolProvider.class);
    private static final Map<String, ExecutorService> pools = new TreeMap<>();

    private final SchedulerSettings settings;

    /**
     * Initialize a new instance.
     */
    @Inject
    public JobPoolProvider(SchedulerSettings settings) {
        this.settings = settings;
    }

    /**
     * Return the {@link ExecutorService}, a thread pool, related to the given pool id. It the pool does not exist,
     * it will be created.
     *
     * @param poolId The id of the pool.
     * @return The thread pool.
     */
    public ExecutorService getPool(String poolId) {
        poolId = StringUtils.defaultIfBlank(poolId, DEFAULT_POOL_ID);
        return pools.computeIfAbsent(poolId, this::createPool);
    }

    /**
     * Create a new {@link ExecutorService} thread pool related to the given pool id.
     * <p>
     * The custom thread pool executor creates pools with a configured max size, and with threads that shutdown after
     * 5 minutes idle time.
     *
     * @param poolId The id of the pool.
     * @return The thread pool.
     */
    private ExecutorService createPool(String poolId) {
        String poolName = settings.getPoolName(poolId);
        BasicThreadFactory factory = new BasicThreadFactory.Builder()
                .namingPattern(poolName + "-%d")
                .build();
        int poolSize = settings.getPoolSize(poolId);
        logger.info("Creating pool {} with a maximum of {} worker threads.", poolName, poolSize);
        return new ThreadPoolExecutor(0, poolSize, 5L, TimeUnit.MINUTES, new LinkedBlockingQueue<>(), factory);
    }

}
