package nl.mirila.scheduling.core;

import com.google.inject.Singleton;
import nl.mirila.core.datatype.Id;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicInteger;

import static nl.mirila.scheduling.core.JobStatus.SCHEDULED;

/**
 * The {@link InMemoryJobManager} is a {@link JobManager} that keeps all jobs in memory. The jobs are not persisted.
 *
 */
@Singleton
public class InMemoryJobManager implements JobManager {

    private static final AtomicInteger idIndex = new AtomicInteger();
    private static final Map<Id, Job> jobs = new TreeMap<>();

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Job> getProcessableJobs() {
        LocalDateTime endRange = LocalDateTime.now();
        LocalDateTime startRange = endRange.minusSeconds(3600);
        return jobs.values()
                .stream()
                .filter((job) -> job.getStatus().equals(SCHEDULED))
                .filter((job) -> job.getRunAt().isBefore(endRange))
                .filter((job) -> job.getRunAt().isAfter(startRange))
                .toList();
    }

    /**
     * Return the collection of jobs.
     */
    public Map<Id, Job> getJobs() {
        return jobs;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void persist(Job job) {
        if (job.getId() == null) {
            job.withId(Id.of(idIndex.incrementAndGet()));
        }
        jobs.put(job.getId(), job);
    }



}
