package nl.mirila.scheduling.core;

import java.util.List;

/**
 * The {@link JobManager} not only provides a list {@link Job}, but also manages jobs.
 */
public interface JobManager {

    /**
     * Return a list of processable jobs.
     */
    List<Job> getProcessableJobs();

    /**
     * Persist the given job.
     */
    void persist(Job job);

}
