package nl.mirila.scheduling.core;

/**
 * A {@link JobHandler} processes a job a certain job.
 * <p>
 * Each job must be annotated with a {@link HandlesJobs} annotation, specifying at least a group and potentially a name
 * for which kind of jobs this handler can be used.
 * <p>
 * JobHandlers must be registered at the {@link JobHandlerRegistry}. This can be done programmatically, using the
 * method {@link JobHandlerRegistry#register(Class)}, or by binding it in Guice.
 */
public interface JobHandler {

    /**
     * Process the given {@link Job}.
     *
     * @param job The job to process.
     */
    void handle(Job job);

}
