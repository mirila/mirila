package nl.mirila.scheduling.core;

import nl.mirila.core.datatype.Id;
import nl.mirila.core.recurrence.patterns.RecurrencePattern;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDateTime;
import java.util.Optional;

/**
 * A {@link Job} represents work that needs to be done at certain time, or time series.
 */
public class Job {

    private Id id;
    private final String group;
    private final String name;
    private JobStatus status;
    private LocalDateTime runAt;
    private LocalDateTime startedAt;
    private LocalDateTime processedAt;
    private String poolId;
    private RecurrencePattern pattern;
    private String data;

    /**
     * Initialize a new instance.
     */
    private Job(String group, String name) {
        this.group = group;
        this.name = name;
        id = null;
        status = JobStatus.SCHEDULED;
        runAt = LocalDateTime.now().plusSeconds(1);
        startedAt = null;
        processedAt = null;
        poolId = null;
        pattern = null;
        data = null;
    }

    /**
     * Create a new instance of {@link Job} with the given group and name.
     **/
    public static Job of(String group, String name) {
        return new Job(group, name);
    }

    /**
     * Return id.
     */
    public Id getId() {
        return id;
    }

    /**
     * Set id and return this instance for chaining purposes.
     **/
    public Job withId(Id id) {
        this.id = id;
        return this;
    }

    /**
     * Return jobName.
     */
    public String getGroup() {
        return group;
    }

    /**
     * Return name.
     */
    public String getName() {
        return name;
    }

    /**
     * Return status.
     */
    public JobStatus getStatus() {
        return status;
    }

    /**
     * Set status and return this instance for chaining purposes.
     **/
    public Job withStatus(JobStatus status) {
        if (status != null) {
            this.status = status;
        }
        return this;
    }

    /**
     * Return runAt.
     */
    public LocalDateTime getRunAt() {
        return runAt;
    }

    /**
     * Set runAt and return this instance for chaining purposes.
     **/
    public Job withRunAt(LocalDateTime runAt) {
        this.runAt = runAt;
        return this;
    }

    /**
     * Return startedAt.
     */
    public Optional<LocalDateTime> getStartedAt() {
        return Optional.ofNullable(startedAt);
    }

    /**
     * Set startedAt and return this instance for chaining purposes.
     **/
    public Job withStartedAt(LocalDateTime startedAt) {
        this.startedAt = startedAt;
        return this;
    }

    /**
     * Return processedAt.
     */
    public Optional<LocalDateTime> getProcessedAt() {
        return Optional.ofNullable(processedAt);
    }

    /**
     * Set processedAt and return this instance for chaining purposes.
     **/
    public Job withProcessedAt(LocalDateTime processedAt) {
        this.processedAt = processedAt;
        return this;
    }

    /**
     * Return the id of the pool to use.
     */
    public Optional<String> getPoolId() {
        return Optional.ofNullable(poolId);
    }

    /**
     * Set the id of the pool to use, and return this instance for chaining purposes.
     **/
    public Job withPoolId(String poolId) {
        this.poolId = (StringUtils.isBlank(poolId)) ? null : poolId;
        return this;
    }

    /**
     * Return pattern.
     */
    public Optional<RecurrencePattern> getPattern() {
        return Optional.ofNullable(pattern);
    }

    /**
     * Set pattern and return this instance for chaining purposes.
     **/
    public Job withPattern(RecurrencePattern pattern) {
        this.pattern = pattern;
        return this;
    }

    /**
     * Return data.
     */
    public Optional<String> getData() {
        return Optional.ofNullable(data);
    }

    /**
     * Set data and return this instance for chaining purposes.
     **/
    public Job withData(String data) {
        this.data = data;
        return this;
    }

}
