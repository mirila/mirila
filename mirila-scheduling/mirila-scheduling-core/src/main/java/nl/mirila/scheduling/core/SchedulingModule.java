package nl.mirila.scheduling.core;

import com.google.inject.AbstractModule;

public class SchedulingModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(JobHandlerRegistry.class).asEagerSingleton();
        bind(JobDispatcher.class).asEagerSingleton();
    }

}
