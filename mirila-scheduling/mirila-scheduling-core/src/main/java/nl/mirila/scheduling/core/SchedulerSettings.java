package nl.mirila.scheduling.core;

import com.google.inject.Inject;
import nl.mirila.core.settings.Settings;

/**
 * The settings that are relevant for the scheduler.
 */
public class SchedulerSettings {

    public static final String DEFAULT_POOL_ID = "default";

    public static final String KEY_INTERVAL = "scheduler.interval";
    public static final String KEY_OFFSET = "scheduler.offset";
    public static final String KEY_ACTIVE = "scheduler.active";
    public static final String KEY_LOG_JOB_COUNT = "scheduler.log-job-count";
    public static final String KEY_POOL_NAME = "scheduler.pools.%s.name";
    public static final String KEY_POOL_SIZE = "scheduler.pools.%s.size";

    private static final int DEFAULT_INTERVAL = 30;
    private static final int DEFAULT_OFFSET = 3600;
    private static final boolean DEFAULT_ACTIVE = true;
    private static final boolean DEFAULT_LOG_JOB_COUNT = false;
    private static final String DEFAULT_POOL_NAME = "default-worker";
    private static final int DEFAULT_POOL_SIZE = 3;

    private final Settings settings;

    /**
     * Initialize a new instance.
     */
    @Inject
    public SchedulerSettings(Settings settings) {
        this.settings = settings;
    }

    /**
     * Return the interval in seconds for fetching processable jobs.
     * <p>
     * Setting: {@value KEY_INTERVAL}. Default: {@value DEFAULT_INTERVAL}.
     */
    public int getInterval() {
        return settings.getInteger(KEY_INTERVAL).orElse(DEFAULT_INTERVAL);
    }

    /**
     * Return the offset in seconds to look in the past when fetching processable jobs.
     * <p>
     * Setting: {@value KEY_INTERVAL}. Default: {@value DEFAULT_INTERVAL}.
     */
    public int getOffset() {
        return settings.getInteger(KEY_OFFSET).orElse(DEFAULT_OFFSET);
    }

    /**
     * Return true if the count of processable jobs should be logged.
     * <p>
     * Setting: {@value KEY_LOG_JOB_COUNT}. Default: {@value DEFAULT_LOG_JOB_COUNT}.
     */
    public boolean shouldLogJobCount() {
        return settings.getBoolean(KEY_LOG_JOB_COUNT).orElse(DEFAULT_LOG_JOB_COUNT);
    }

    /**
     * Return true if the scheduler should be active.
     * <p>
     * Setting: {@value KEY_ACTIVE}. Default: {@value DEFAULT_ACTIVE}.
     */
    public boolean isActive() {
        return settings.getBoolean(KEY_ACTIVE).orElse(DEFAULT_ACTIVE);
    }

    /**
     * Return the pool name of the pool with the given id.
     * <p>
     * Setting: {@value KEY_POOL_NAME} with given pool id. Default: {@value DEFAULT_POOL_ID}.
     */
    public String getPoolName(String poolId) {
        String key = String.format(KEY_POOL_NAME, poolId);
        String defaultValue = (poolId.equals(DEFAULT_POOL_ID)) ? DEFAULT_POOL_NAME : poolId;
        return settings.getString(key).orElse(defaultValue);
    }

    /**
     * Return the pool size of the pool with the given id.
     * <p>
     * Setting: {@value KEY_POOL_NAME} with given pool id. Default: {@value DEFAULT_POOL_SIZE}.
     */
    public int getPoolSize(String poolId) {
        String key = String.format(KEY_POOL_SIZE, poolId);
        return settings.getInteger(key).orElse(DEFAULT_POOL_SIZE);
    }

}
