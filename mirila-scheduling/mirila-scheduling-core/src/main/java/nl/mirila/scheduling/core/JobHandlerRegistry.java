package nl.mirila.scheduling.core;

import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.TypeLiteral;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * The {@link JobHandlerRegistry} contains a list of all available {@link JobHandler}.
 */
public class JobHandlerRegistry {

    private static final Logger logger = LogManager.getLogger(JobHandlerRegistry.class);

    private static final Map<String, Map<String, Class<? extends JobHandler>>> handlers = new HashMap<>();

    public static final String WILDCARD = "*";
    private static boolean loaded = false;

    private final Injector injector;

    /**
     * Initialize a new instance and register each bound {@link JobHandler} class.
     */
    @Inject
    public JobHandlerRegistry(Injector injector) {
        this.injector = injector;
        if (!loaded) {
            injector.getAllBindings()
                    .keySet()
                    .stream()
                    .map(Key::getTypeLiteral)
                    .map(TypeLiteral::getRawType)
                    .filter(JobHandler.class::isAssignableFrom)
                    .forEach(this::registerUncheckedJobHandlerClass);
            loaded = true;
        }
    }

    /**
     * Register the given JobHandler class, in an anonymous state. This method is here to suppress the warning.
     *
     * @param handlerClass The class to register.
     */
    @SuppressWarnings("unchecked")
    private void registerUncheckedJobHandlerClass(Class<?> handlerClass) {
        register((Class<? extends JobHandler>) handlerClass);
    }

    /**
     * Register the given class of {@link JobHandler} for the given group and name of jobs.
     * <p>
     * The name may be using a {@link JobHandlerRegistry#WILDCARD} to process each job within the group, regardless of
     * its name.
     *
     * @param handlerClass The handler that processes jobs.
     */
    public void register(Class<? extends JobHandler> handlerClass) {
        if (!handlerClass.isAnnotationPresent(HandlesJobs.class)) {
            logger.warn("JobHandler {} misses the annotation @HandlesJob. Ignoring this handler.",
                        handlerClass.getName());
            return;
        }

        HandlesJobs annotation = handlerClass.getAnnotation(HandlesJobs.class);
        String jobGroup = annotation.group();
        String jobName = annotation.name();

        handlers.computeIfAbsent(jobGroup, (key) -> new HashMap<>());
        if (!handlers.get(jobGroup).containsKey(jobName)) {
            handlers.get(jobGroup).put(jobName, handlerClass);
            logger.info("Registered {} for jobs with for '{}:{}'", handlerClass.getName(), jobGroup, jobName);
        } else {
            logger.warn("Already registered a job handler for jobs '{}:{}'. Skipping job handler {}.",
                        jobGroup,
                        jobName,
                        handlerClass.getName());
        }
    }

    /**
     * Return the {@link JobHandler} for the given {@link Job}.
     *
     * @param job The job to handle.
     * @return The handler to process the job.
     */
    public Optional<? extends JobHandler> getHandler(Job job) {
        return getHandler(job.getGroup(), job.getName());
    }

    /**
     * Return the {@link JobHandler} for the job with the given job group and name.
     *
     * @param jobGroup The group of the job.
     * @param jobName The name of the job.
     * @return The handler to process the job.
     */
    public Optional<? extends JobHandler> getHandler(String jobGroup, String jobName) {
        Class<? extends JobHandler> handlerClass = null;
        if (handlers.containsKey(jobGroup)) {
            handlerClass = handlers.get(jobGroup).get(jobName);
            if (handlerClass == null) {
                handlerClass = handlers.get(jobGroup).get(WILDCARD);
            }
        }
        return (handlerClass == null)
                ? Optional.empty()
                : Optional.ofNullable(injector.getInstance(handlerClass));
    }

}
