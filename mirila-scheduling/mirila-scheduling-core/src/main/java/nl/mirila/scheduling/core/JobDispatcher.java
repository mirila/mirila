package nl.mirila.scheduling.core;

import com.google.inject.Inject;
import org.apache.commons.lang3.concurrent.BasicThreadFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static nl.mirila.scheduling.core.JobStatus.*;

/**
 * The {@link JobDispatcher} runs regularly to fetch a list of {@link Job}, queue the jobs into a worker thread pool,
 * to be picked up for further handling.
 */
public class JobDispatcher {

    private static final Logger logger = LogManager.getLogger(JobDispatcher.class);

    private final SchedulerSettings settings;
    private final JobHandlerRegistry registry;
    private final JobManager jobManager;
    private final JobPoolProvider poolProvider;
    private final JobScheduler scheduler;

    /**
     * Initialize a new instance.
     */
    @Inject
    public JobDispatcher(SchedulerSettings settings,
                         JobHandlerRegistry registry,
                         JobManager jobManager,
                         JobPoolProvider poolProvider,
                         JobScheduler scheduler) {
        this.settings = settings;
        this.registry = registry;
        this.jobManager = jobManager;
        this.poolProvider = poolProvider;
        this.scheduler = scheduler;

        BasicThreadFactory factory = new BasicThreadFactory.Builder()
                .namingPattern("job-dispatcher-pulse")
                .build();
        ScheduledExecutorService pulse = Executors.newSingleThreadScheduledExecutor(factory);
        pulse.scheduleWithFixedDelay(this::fetchJobs, 0, settings.getInterval(), TimeUnit.SECONDS);
    }

    /**
     * Fetch jobs using the {@link JobManager} and submit these jobs to the related thread pool, provided by the
     * {@link JobPoolProvider}.
     */
    private void fetchJobs() {
        if (!settings.isActive()) {
            logger.debug("Not fetching jobs as the logger is inactive.");
            return;
        }
        List<Job> jobs = jobManager.getProcessableJobs();
        if (settings.shouldLogJobCount()) {
            logger.info("Found {} jobs to process.", jobs.size());
        }
        jobs.forEach((job) -> {
            ExecutorService pool = poolProvider.getPool(job.getPoolId().orElse(""));
            jobManager.persist(job.withStatus(QUEUED));
            pool.submit(() -> dispatch(job));
        });
    }

    /**
     * Dispatch the given job to its related {@link JobHandler}, provided by the {@link JobHandlerRegistry}.
     * <p>
     * The method coarsely catch any exception to make sure the exception is logged and the job status is set correctly.
     *
     * @param job The job to dispatch.
     */
    public void dispatch(Job job) {
        Optional<? extends JobHandler> optional = registry.getHandler(job);
        if (optional.isEmpty()) {
            logger.warn("Could not find a handler for job '{}:{}' ({})", job.getGroup(), job.getName(), job.getId());
            jobManager.persist(job.withStatus(FAILED));
            return;
        }
        jobManager.persist(job.withStatus(PROCESSING).withStartedAt(LocalDateTime.now()));
        try {
            optional.get().handle(job);
            jobManager.persist(job.withStatus(PROCESSED).withProcessedAt(LocalDateTime.now()));
        } catch (Exception e) {
            jobManager.persist(job.withStatus(FAILED));
            logger.error("Unknown error while processing '{}:{}' ({}): {}",
                         job.getGroup(),
                         job.getName(),
                         job.getId(),
                         e.getMessage());
        }
        scheduler.reschedule(job);
    }

}
