package nl.mirila.scheduling.core;

import java.util.Optional;
import java.util.stream.Stream;

/**
 * The {@link JobStatus} represents all statuses that a {@link Job} may have.
 */
public enum JobStatus {

    /**
     * The job is created, but not yet scheduled.
     */
    CREATED("created"),

    /**
     * The job is scheduled, waiting to be picked up at a certain time.
     */
    SCHEDULED("scheduled"),


    /**
     * The job is pickup to be processed, but currently queued, waiting for an available worker thread.
     */
    QUEUED("queued"),

    /**
     * The job is being processed by a {@link JobHandler}.
     */
    PROCESSING("processing"),

    /**
     * The job is processed by a {@link JobHandler}, and finished without failures.
     */
    PROCESSED("processed"),

    /**
     * The job has failed. The cause of the failure could be anything, like unexpected exceptions, or requirements that
     * were not met.
     */
    FAILED("failed");

    private final String name;

    /**
     * Initialize a new instance.
     */
    JobStatus(String name) {
        this.name = name;
    }

    /**
     * Return the name of the enum.
     */
    @Override
    public String toString() {
        return name;
    }

    /**
     * Find the JobStatus, using the given name. If not found, an empty {@link Optional} is returned.
     *
     * @param name The name of the job status to find.
     * @return The job status, wrapped in an {@link Optional}.
     */
    public static Optional<JobStatus> findByName(String name) {
        return Stream.of(JobStatus.values())
                .filter((type) -> type.toString().equalsIgnoreCase(name))
                .findFirst();
    }
}
