package nl.mirila.scheduling.core.handlers;

import nl.mirila.scheduling.core.Job;
import nl.mirila.scheduling.core.JobHandler;
import nl.mirila.scheduling.core.JobStatus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseHandler implements JobHandler {
    private static final Logger logger = LogManager.getLogger(BaseHandler.class);

    public static final List<Job> handledJobs = new ArrayList<>();

    protected abstract JobStatus getStatus(Job job);

    @Override
    public void handle(Job job) {
        logger.debug("Processing job {} with current status '{}'.", job.getId(), job.getStatus());
        handledJobs.add(job);
        job.withStatus(getStatus(job));
    }

}
