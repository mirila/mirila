package nl.mirila.scheduling.core.handlers;

import nl.mirila.scheduling.core.HandlesJobs;
import nl.mirila.scheduling.core.Job;
import nl.mirila.scheduling.core.JobStatus;

@HandlesJobs(group = "successful-group")
public class SuccessfulJobHandler extends BaseHandler {

    @Override
    protected JobStatus getStatus(Job job) {
        return JobStatus.PROCESSED;
    }

}
