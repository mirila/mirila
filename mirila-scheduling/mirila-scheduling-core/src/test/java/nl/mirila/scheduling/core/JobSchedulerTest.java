package nl.mirila.scheduling.core;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import nl.mirila.core.recurrence.enums.TimeUnit;
import nl.mirila.core.recurrence.patterns.RecurrencePattern;
import nl.mirila.core.settings.modules.KeyValuesSettingsModule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

class JobSchedulerTest {

    private JobScheduler scheduler;
    private InMemoryJobManager manager;

    @BeforeEach
    void setUp() {
        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                install(new KeyValuesSettingsModule(Collections.emptyMap()));
                bind(JobManager.class).to(InMemoryJobManager.class);
            }
        });
        scheduler = injector.getInstance(JobScheduler.class);
        manager = injector.getInstance(InMemoryJobManager.class);
        manager.getJobs().clear();
    }

    @Test
    void testSchedule() {
        manager.getJobs().clear();
        scheduler.schedule(Job.of("group", "name"));
        assertThat(manager.getJobs()).hasSize(1);
    }

    @Test
    void testReschedule() {
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime tomorrow = now.plusDays(1);
        Job job = Job.of("group", "name")
                .withRunAt(now)
                .withPattern(RecurrencePattern.forRate().withTimeUnit(TimeUnit.DAILY));
        scheduler.schedule(job);
        Optional<Job> optional = scheduler.reschedule(job);

        assertThat(manager.getJobs()).hasSize(2);
        assertThat(job.getRunAt()).isEqualTo(now);
        assertThat(optional).isPresent();
        assertThat(optional.get().getRunAt())
                .isAfter(tomorrow.minusSeconds(10))
                .isBefore(tomorrow.plusSeconds(10));
    }

}