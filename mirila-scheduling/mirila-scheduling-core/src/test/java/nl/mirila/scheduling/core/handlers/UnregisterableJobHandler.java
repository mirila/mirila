package nl.mirila.scheduling.core.handlers;

import nl.mirila.scheduling.core.Job;
import nl.mirila.scheduling.core.JobStatus;

// Deliberately no @HandlesJob annotation
public class UnregisterableJobHandler extends BaseHandler {

    @Override
    protected JobStatus getStatus(Job job) {
        return JobStatus.PROCESSED;
    }

}
