package nl.mirila.scheduling.core;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import nl.mirila.core.settings.modules.KeyValuesSettingsModule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.HashMap;

import static nl.mirila.scheduling.core.JobStatus.*;
import static org.assertj.core.api.Assertions.assertThat;

class InMemoryJobManagerTest {

    private JobManager manager;

    @BeforeEach
    void setUp() {
        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                install(new KeyValuesSettingsModule(new HashMap<>()));
                bind(JobManager.class).to(InMemoryJobManager.class);
            }
        });
        manager = injector.getInstance(JobManager.class);
    }

    @Test
    void testGetProcessableJobs() {
        manager.persist(getJobAt(LocalDateTime.now().minusSeconds(86400), PROCESSED));
        manager.persist(getJobAt(LocalDateTime.now().minusSeconds(43200), FAILED));
        manager.persist(getJobAt(LocalDateTime.now().minusSeconds(1800), PROCESSING));
        manager.persist(getJobAt(LocalDateTime.now().minusSeconds(60), SCHEDULED));
        manager.persist(getJobAt(LocalDateTime.now(), SCHEDULED));
        manager.persist(getJobAt(LocalDateTime.now().plusSeconds(1800), SCHEDULED));
        manager.persist(getJobAt(LocalDateTime.now().plusSeconds(43200), SCHEDULED));
        manager.persist(getJobAt(LocalDateTime.now().plusSeconds(86400), SCHEDULED));

        assertThat(manager.getProcessableJobs())
                .hasSize(2);
    }

    private Job getJobAt(LocalDateTime timestamp, JobStatus status) {
        return Job.of("group", "name").withRunAt(timestamp).withStatus(status);
    }

}