package nl.mirila.scheduling.core.handlers;

import nl.mirila.scheduling.core.HandlesJobs;
import nl.mirila.scheduling.core.Job;
import nl.mirila.scheduling.core.JobStatus;

@HandlesJobs(group = "failing-group", name = "always")
public class FailingJobHandler extends BaseHandler {

    @Override
    protected JobStatus getStatus(Job job) {
        throw new RuntimeException("Constantly failing this job");
    }

}
