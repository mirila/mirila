package nl.mirila.scheduling.core;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import nl.mirila.core.settings.modules.KeyValuesSettingsModule;
import nl.mirila.scheduling.core.handlers.FailingJobHandler;
import nl.mirila.scheduling.core.handlers.SuccessfulJobHandler;
import nl.mirila.scheduling.core.handlers.UnregisterableJobHandler;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.Optional;

import static nl.mirila.scheduling.core.JobHandlerRegistry.WILDCARD;
import static org.assertj.core.api.Assertions.assertThat;

class JobHandlerRegistryTest {

    private JobHandlerRegistry registry;

    @BeforeEach
    void setUp() {
        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                install(new KeyValuesSettingsModule(Collections.emptyMap()));
                bind(SuccessfulJobHandler.class);
                bind(FailingJobHandler.class);
                bind(UnregisterableJobHandler.class);
                bind(JobHandlerRegistry.class);
            }
        });
        registry = injector.getInstance(JobHandlerRegistry.class);
    }

    @Test
    void testGetHandler() {
        Optional<? extends JobHandler> optional = registry.getHandler("successful-group", "anything");
        assertThat(optional).isPresent();
    }

    @Test
    void testGetHandlerWithEmptyString() {
        Optional<? extends JobHandler> optional = registry.getHandler("successful-group", "");
        assertThat(optional).isPresent();
    }

    @Test
    void testGetHandlerWithWildcard() {
        Optional<? extends JobHandler> optional = registry.getHandler("successful-group", WILDCARD);
        assertThat(optional).isPresent();
    }

    @Test
    void testGetHandlerWithJob() {
        Optional<? extends JobHandler> optional = registry.getHandler(Job.of("successful-group", "anything"));
        assertThat(optional).isPresent();
    }

    @Test
    void testGetSpecificHandlerWithIncorrectName() {
        Optional<? extends JobHandler> optional = registry.getHandler("failing-group", "incorrect-name");
        assertThat(optional).isEmpty();
    }

    @Test
    void testGetSpecificHandlerWithCorrectName() {
        Optional<? extends JobHandler> optional = registry.getHandler("failing-group", "always");
        assertThat(optional).isPresent();
    }

}