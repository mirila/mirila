package nl.mirila.scheduling.core;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Scopes;
import nl.mirila.core.settings.modules.KeyValuesSettingsModule;
import nl.mirila.scheduling.core.handlers.BaseHandler;
import nl.mirila.scheduling.core.handlers.FailingJobHandler;
import nl.mirila.scheduling.core.handlers.SuccessfulJobHandler;
import nl.mirila.scheduling.core.handlers.UnregisterableJobHandler;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static nl.mirila.scheduling.core.SchedulerSettings.*;
import static org.assertj.core.api.Assertions.assertThat;

class JobDispatcherTest {

    @Test
    void testHandle() throws InterruptedException {
        TestJobProvider provider = new TestJobProvider();
        provider.setJobType("successful-group", "a-name");
        provider.setJobCount(10);

        Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                final Map<String, String> settings = new HashMap<>();
                settings.put(KEY_INTERVAL, "1");
                settings.put(KEY_LOG_JOB_COUNT, "true");
                settings.put(KEY_POOL_SIZE.formatted("extra"), "5");
                install(new KeyValuesSettingsModule(settings));

                bind(TestJobProvider.class).in(Scopes.SINGLETON);
                bind(JobManager.class).toInstance(provider);
                bind(SuccessfulJobHandler.class);
                bind(FailingJobHandler.class);
                bind(UnregisterableJobHandler.class);
                install(new SchedulingModule());
            }
        });

        Thread.sleep(500);
        assertThat(BaseHandler.handledJobs)
                .hasSizeGreaterThanOrEqualTo(10)
                .map(Job::getStatus)
                .containsOnly(JobStatus.PROCESSED);

        provider.setJobType("failing-group", "always");
        provider.setPool("extra");

        Thread.sleep(1000);

        assertThat(BaseHandler.handledJobs)
                .hasSizeGreaterThanOrEqualTo(20)
                .map(Job::getStatus)
                .containsOnly(JobStatus.PROCESSED, JobStatus.FAILED);
    }

}