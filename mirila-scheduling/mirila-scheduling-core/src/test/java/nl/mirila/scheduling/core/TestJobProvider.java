package nl.mirila.scheduling.core;

import nl.mirila.core.datatype.Id;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

public class TestJobProvider implements JobManager {

    private AtomicInteger idCount = new AtomicInteger();

    private String group;
    private String name;
    private String poolId = "";
    private int jobCount = 1;

    @Override
    public List<Job> getProcessableJobs() {
        if (StringUtils.isNotBlank(group)) {
            List<Job> jobs = new ArrayList<>();
            IntStream.range(0, jobCount).forEach((i) -> {
                Job job = Job.of(group, name)
                        .withId(Id.of(idCount.incrementAndGet()))
                        .withPoolId(poolId);
                jobs.add(job);
            });
            return jobs;
        }
        return List.of();
    }

    @Override
    public void persist(Job job) {
        // No-op
    }

    public void setJobCount(int jobCount) {
        this.jobCount = jobCount;
    }

    public void setJobType(String group, String name) {
        this.group = group;
        this.name = name;
    }

    public void setPool(String id) {
        this.poolId = id;
    }

}
