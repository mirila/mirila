package nl.mirila.scheduling.dbs;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Scopes;
import nl.mirila.core.recurrence.enums.TimeUnit;
import nl.mirila.core.recurrence.patterns.RecurrencePattern;
import nl.mirila.core.settings.modules.KeyValuesSettingsModule;
import nl.mirila.dbs.sql.SqlEntityManager;
import nl.mirila.dbs.sql.SqlModule;
import nl.mirila.metrics.core.MetricsCoreModule;
import nl.mirila.model.management.module.ModelManagementModule;
import nl.mirila.model.management.query.QueryParameters;
import nl.mirila.scheduling.core.Job;
import nl.mirila.scheduling.core.JobScheduler;
import nl.mirila.security.auth.core.contexts.GuestSecurityContext;
import nl.mirila.security.auth.core.contexts.SecurityContext;
import org.jboss.resteasy.plugins.guice.RequestScoped;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static nl.mirila.core.settings.ApplicationSettings.KEY_REALM;
import static org.assertj.core.api.Assertions.assertThat;

class DatabaseJobManagerIT {

    private JobScheduler scheduler;
    private SqlEntityManager persistenceManager;

    @BeforeEach
    void setUp() {
        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                Map<String, String> settings = new HashMap<>();
                settings.put(KEY_REALM, "mirila");
                install(new KeyValuesSettingsModule(settings));
                install(new SqlModule());
                install(new ModelManagementModule());
                install(new MetricsCoreModule());
                install(new DatabaseSchedulingModule());

                bind(SecurityContext.class).toInstance(new GuestSecurityContext("mirila", "en_EN"));
                bindScope(RequestScoped.class, Scopes.SINGLETON);
            }
        });
        scheduler = injector.getInstance(JobScheduler.class);
        persistenceManager = injector.getInstance(SqlEntityManager.class);
        persistenceManager.runSqlStatement("truncate scheduled_jobs", List.of());
    }

    @Test
    void testSchedule() {
        scheduler.schedule(Job.of("group", "name"));
        long count = persistenceManager.count(QueryParameters.on(ScheduledJob.class));
        assertThat(count).isEqualTo(1);
    }

    @Test
    void testReschedule() {
        LocalDateTime now = LocalDateTime.now();
        Job job = Job.of("group", "name")
                .withRunAt(now)
                .withData("{ \"hello\": \"world\"")
                .withPattern(RecurrencePattern.forRate().withTimeUnit(TimeUnit.MONTHLY).withRepeat(1));
        scheduler.schedule(job);

        assertThat(job.getRunAt()).isEqualTo(now);

        Optional<Job> optional1 = scheduler.reschedule(job);
        long count1 = persistenceManager.count(QueryParameters.on(ScheduledJob.class));
        assertThat(count1).isEqualTo(2);
        assertThat(optional1).isPresent();

        Optional<Job> optional2 = scheduler.reschedule(optional1.get());
        long count2 = persistenceManager.count(QueryParameters.on(ScheduledJob.class));
        assertThat(count2).isEqualTo(2);
        assertThat(optional2).isEmpty();
    }

}