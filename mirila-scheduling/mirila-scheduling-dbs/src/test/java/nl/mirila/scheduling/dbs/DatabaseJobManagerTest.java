package nl.mirila.scheduling.dbs;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Scopes;
import nl.mirila.core.recurrence.patterns.RecurrencePattern;
import nl.mirila.core.settings.modules.KeyValuesSettingsModule;
import nl.mirila.dbs.fakedb.FakeDbEntityManager;
import nl.mirila.dbs.fakedb.FakeDbModule;
import nl.mirila.metrics.core.MetricsCoreModule;
import nl.mirila.model.management.module.ModelManagementModule;
import nl.mirila.model.management.query.QueryParameters;
import nl.mirila.scheduling.core.Job;
import nl.mirila.scheduling.core.JobManager;
import nl.mirila.scheduling.core.JobScheduler;
import nl.mirila.security.auth.core.contexts.GuestSecurityContext;
import nl.mirila.security.auth.core.contexts.SecurityContext;
import org.jboss.resteasy.plugins.guice.RequestScoped;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static nl.mirila.core.recurrence.enums.TimeUnit.DAILY;
import static nl.mirila.core.settings.ApplicationSettings.KEY_REALM;
import static org.assertj.core.api.Assertions.assertThat;

class DatabaseJobManagerTest {

    private JobScheduler scheduler;
    private FakeDbEntityManager persistenceManager;

    @BeforeEach
    void setUp() {
        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                Map<String, String> settings = new HashMap<>();
                settings.put(KEY_REALM, "test-realm");
                install(new KeyValuesSettingsModule(settings));
                install(new ModelManagementModule());
                install(new FakeDbModule());
                install(new MetricsCoreModule());
                install(new DatabaseSchedulingModule());

                bind(SecurityContext.class).toInstance(new GuestSecurityContext("test-realm", "en_EN"));
                bindScope(RequestScoped.class, Scopes.NO_SCOPE);

                bind(JobManager.class).to(DatabaseJobManager.class);
            }
        });
        scheduler = injector.getInstance(JobScheduler.class);
        persistenceManager = injector.getInstance(FakeDbEntityManager.class);
        persistenceManager.clearAll();
    }

    @Test
    void testSchedule() {
        scheduler.schedule(Job.of("group", "name"));
        long count = persistenceManager.count(QueryParameters.on(ScheduledJob.class));
        assertThat(count).isEqualTo(1);
    }

    @Test
    void testReschedule() {
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime tomorrow = now.plusDays(1);
        Job job = Job.of("group", "name")
                .withRunAt(now)
                .withPattern(RecurrencePattern.forRate().withTimeUnit(DAILY));
        scheduler.schedule(job);
        Optional<Job> optional = scheduler.reschedule(job);

        long count = persistenceManager.count(QueryParameters.on(ScheduledJob.class));
        assertThat(count).isEqualTo(2);

        assertThat(job.getRunAt()).isEqualTo(now);
        assertThat(optional).isPresent();
        assertThat(optional.get().getRunAt())
                .isAfter(tomorrow.minusSeconds(10))
                .isBefore(tomorrow.plusSeconds(10));
    }

}