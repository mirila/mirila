package nl.mirila.scheduling.dbs;

import com.google.inject.AbstractModule;
import com.google.inject.multibindings.Multibinder;
import nl.mirila.model.management.descriptor.ModelsPackageNames;
import nl.mirila.scheduling.core.JobManager;
import nl.mirila.scheduling.core.SchedulingModule;

public class DatabaseSchedulingModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(JobManager.class).to(DatabaseJobManager.class);
        install(new SchedulingModule());

        Multibinder<String> binder = Multibinder.newSetBinder(binder(), String.class, ModelsPackageNames.class);
        binder.addBinding().toInstance(ScheduledJob.class.getPackageName());
    }

}
