package nl.mirila.scheduling.dbs;

import com.fasterxml.jackson.annotation.JsonRootName;
import nl.mirila.core.datatype.Id;
import nl.mirila.model.core.annotations.ModelInfo;
import nl.mirila.model.core.references.Model;
import nl.mirila.scheduling.core.Job;

import java.time.LocalDateTime;

/**
 * The {@link ScheduledJob} is a storable version of the {@link Job}.
 */
@ModelInfo(
        singular = "ScheduledJob",
        plural = "ScheduledJobs",
        primaryKeyFields = {"id"}
)
@JsonRootName("ScheduledJob")
public class ScheduledJob implements Model {

    private Id id;
    private String group;
    private String name;
    private String status;
    private LocalDateTime runAt;
    private LocalDateTime startedAt;
    private LocalDateTime processedAt;
    private String poolId;
    private String pattern;
    private String data;

    /**
     * Return the id of this job.
     */
    public Id getId() {
        return id;
    }

    /**
     * Set the id of this job.
     **/
    public void setId(Id id) {
        this.id = id;
    }

    /**
     * Return the group of this job.
     */
    public String getGroup() {
        return group;
    }

    /**
     * Set the group of this job.
     **/
    public void setGroup(String group) {
        this.group = group;
    }

    /**
     * Return the name of this job.
     */
    public String getName() {
        return name;
    }

    /**
     * Set the name of this job.
     **/
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Return the status of this job.
     */
    public String getStatus() {
        return status;
    }

    /**
     * Set the status of this job.
     **/
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Return the timestamp at which this job should run.
     */
    public LocalDateTime getRunAt() {
        return runAt;
    }

    /**
     * Set the timestamp at which this job should run.
     */
    public void setRunAt(LocalDateTime runAt) {
        this.runAt = runAt;
    }

    /**
     * Return the timestamp at which this job was started.
     */
    public LocalDateTime getStartedAt() {
        return startedAt;
    }

    /**
     * Set the timestamp at which this job was started.
     */
    public void setStartedAt(LocalDateTime startedAt) {
        this.startedAt = startedAt;
    }

    /**
     * Return the timestamp at which this job was processed.
     */
    public LocalDateTime getProcessedAt() {
        return processedAt;
    }

    /**
     * Set the timestamp at which this job was processed.
     */
    public void setProcessedAt(LocalDateTime processedAt) {
        this.processedAt = processedAt;
    }

    /**
     * Return the id of the thread pool to use.
     */
    public String getPoolId() {
        return poolId;
    }

    /**
     * Set the id of the thread pool to use.
     */
    public void setPoolId(String poolId) {
        this.poolId = poolId;
    }

    /**
     * Return the string representation of the {@link Pattern}.
     */
    public String getPattern() {
        return pattern;
    }

    /**
     * Set the string representation of the {@link Pattern}.
     */
    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    /**
     * Return the string representation of the data.
     */
    public String getData() {
        return data;
    }

    /**
     * Set the string representation of the data.
     */
    public void setData(String data) {
        this.data = data;
    }

}
