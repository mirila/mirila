package nl.mirila.scheduling.dbs;

import com.google.inject.Inject;
import nl.mirila.core.recurrence.patterns.RecurrencePattern;
import nl.mirila.model.management.enums.RelationalOperator;
import nl.mirila.model.management.managers.EntityPersistenceManager;
import nl.mirila.model.management.query.QueryParameters;
import nl.mirila.model.management.query.filters.FieldComparisonFilter;
import nl.mirila.model.management.query.filters.Filter;
import nl.mirila.model.management.query.filters.FilterList;
import nl.mirila.model.management.results.Results;
import nl.mirila.scheduling.core.Job;
import nl.mirila.scheduling.core.JobManager;
import nl.mirila.scheduling.core.JobStatus;
import nl.mirila.scheduling.core.SchedulerSettings;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The {@link DatabaseJobManager} acts as a {@link JobManager} and uses an {@link EntityPersistenceManager} to store the
 * {@link Job}.
 */
public class DatabaseJobManager implements JobManager {

    private final EntityPersistenceManager persistenceManager;
    private final SchedulerSettings settings;

    /**
     * {@inheritDoc}
     */
    @Inject
    public DatabaseJobManager(EntityPersistenceManager persistenceManager, SchedulerSettings settings) {
        this.persistenceManager = persistenceManager;
        this.settings = settings;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Job> getProcessableJobs() {
        LocalDateTime endRange = LocalDateTime.now();
        LocalDateTime startRange = endRange.minusSeconds(settings.getOffset());
        Filter statusFilter = new FieldComparisonFilter("status", RelationalOperator.EQ, "scheduled");
        Filter startRangeFilter = new FieldComparisonFilter("runAt", RelationalOperator.GTE, startRange);
        Filter endRangeFilter = new FieldComparisonFilter("runAt", RelationalOperator.LTE, endRange);
        FilterList list = FilterList.of(statusFilter, startRangeFilter, endRangeFilter);
        QueryParameters<ScheduledJob> parameters = QueryParameters.on(ScheduledJob.class).withFilter(list);
        Results<ScheduledJob> results = persistenceManager.query(parameters);
        return results.stream()
                .map(this::toJob)
                .collect(Collectors.toList());
    }

    /**
     * Return a new {@link Job} for the given {@link ScheduledJob}.
     */
    private Job toJob(ScheduledJob scheduledJob) {
        String pattern = scheduledJob.getPattern();
        return Job.of(scheduledJob.getGroup(), scheduledJob.getName())
                .withId(scheduledJob.getId())
                .withStatus(JobStatus.findByName(scheduledJob.getStatus()).orElse(JobStatus.SCHEDULED))
                .withRunAt(scheduledJob.getRunAt())
                .withStartedAt(scheduledJob.getStartedAt())
                .withProcessedAt(scheduledJob.getProcessedAt())
                .withPattern(RecurrencePattern.fromCodes(pattern).orElse(null))
                .withPoolId(scheduledJob.getPoolId())
                .withData(scheduledJob.getData());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void persist(Job job) {
        ScheduledJob scheduledJob = toScheduledJob(job);
        if (scheduledJob.getId() != null) {
            persistenceManager.update(scheduledJob);
        } else {
            persistenceManager.create(scheduledJob);
            job.withId(scheduledJob.getId());
        }
    }

    /**
     * Return a new {@link ScheduledJob} for the given {@link Job}.
     */
    private ScheduledJob toScheduledJob(Job job) {
        ScheduledJob scheduledJob = new ScheduledJob();
        scheduledJob.setId(job.getId());
        scheduledJob.setGroup(job.getGroup());
        scheduledJob.setName(job.getName());
        scheduledJob.setStatus(job.getStatus().toString());
        scheduledJob.setRunAt(job.getRunAt());
        scheduledJob.setStartedAt(job.getStartedAt().orElse(null));
        scheduledJob.setProcessedAt(job.getProcessedAt().orElse(null));
        scheduledJob.setPoolId(job.getPoolId().orElse(null));
        scheduledJob.setPattern(job.getPattern().map(RecurrencePattern::toCodes).orElse(null));
        scheduledJob.setData(job.getData().orElse(null));
        return scheduledJob;
    }

}
