package nl.mirila.model.sql.generator.converter;

import org.junit.jupiter.api.Test;

import static nl.mirila.model.sql.generator.converter.ConverterUtils.getSqlName;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class ConverterUtilsTest {

    @Test
    void testGetSqlName() {
        assertThat(getSqlName("Test")).isEqualTo("test");
        assertThat(getSqlName("TestTest")).isEqualTo("test_test");
        assertThat(getSqlName("test-test")).isEqualTo("test_test");
    }

}
