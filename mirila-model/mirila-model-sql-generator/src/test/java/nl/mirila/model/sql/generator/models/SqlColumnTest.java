package nl.mirila.model.sql.generator.models;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class SqlColumnTest {

    @Test
    void testComplete() {
        SqlColumn column = SqlColumn.withName("test1")
                .withDataType("VARCHAR")
                .withSize(25)
                .withNotNull(true)
                .withDefaultValue("def");

        assertThat(column.asSql()).isEqualTo("`test1` VARCHAR(25) NOT NULL DEFAULT 'def'");
    }

    @Test
    void testNotRequired() {
        SqlColumn column = SqlColumn.withName("test2")
                .withDataType("BIT")
                .withSize(1)
                .withNotNull(false)
                .withDefaultValue("1");
        assertThat(column.asSql()).isEqualTo("`test2` BIT(1) DEFAULT 1");
    }

    @Test
    void testWithoutSize() {
        SqlColumn column = SqlColumn.withName("test3")
                .withDataType("INT")
                .withNotNull(true)
                .withDefaultValue("0");
        assertThat(column.asSql()).isEqualTo("`test3` INT NOT NULL DEFAULT 0");
    }

    @Test
    void testWithoutType() {
        SqlColumn column = SqlColumn.withName("test4")
                .withSize(123)
                .withNotNull(true)
                .withDefaultValue("testing");
        assertThat(column.asSql()).isEqualTo("`test4` VARCHAR(123) NOT NULL DEFAULT 'testing'");
    }

    @Test
    void testQuote() {
        SqlColumn column = SqlColumn.withName("test5")
                .withDataType("VARCHAR")
                .withSize(25)
                .withNotNull(true)
                .withDefaultValue("o'Brien");

        assertThat(column.asSql()).isEqualTo("`test5` VARCHAR(25) NOT NULL DEFAULT 'o''Brien'");
    }

    @Test
    void testFieldName() {
        SqlColumn column = SqlColumn.withName("my_test6")
                .withDataType("VARCHAR")
                .withSize(25)
                .withNotNull(true)
                .withDefaultValue("o'Brien");

        assertThat(column.asSql()).isEqualTo("`my_test6` VARCHAR(25) NOT NULL DEFAULT 'o''Brien'");
    }

    @Test
    void testAutoIncrement() {
        SqlColumn column = SqlColumn.withName("my_test7")
                .withDataType("INT")
                .withSize(11)
                .withAutoIncrement(true);

        assertThat(column.asSql()).isEqualTo("`my_test7` INT(11) NOT NULL AUTO_INCREMENT");
    }

    @Test
    void testCurrentTimestamp() {
        SqlColumn column = SqlColumn.withName("my_test8")
                .withDataType("TIMESTAMP")
                .withAutoIncrement(true)
                .withDefaultValue("NOW");

        assertThat(column.asSql())
                .isEqualTo("`my_test8` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP");
    }

}
