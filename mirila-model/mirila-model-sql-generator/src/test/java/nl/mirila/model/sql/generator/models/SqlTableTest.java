package nl.mirila.model.sql.generator.models;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class SqlTableTest {

    @Test
    void testAsSql() {
        SqlColumn column1 = SqlColumn.withName("test1")
                .withDataType("VARCHAR")
                .withSize(25)
                .withNotNull(true)
                .withDefaultValue("def")
                .withComment("This is 'a' field comment");

        SqlColumn column2 = SqlColumn.withName("test2")
                .withDataType("BIT")
                .withNotNull(true)
                .withDefaultValue("1");

        SqlColumn column3 = SqlColumn.withName("test3")
                .withDataType("INT")
                .withSize(11);

        SqlTable sqlTable = SqlTable.withName("tests")
                .withColumns(Arrays.asList(column1, column2, column3))
                .withPrimaryKey(List.of("column1"))
                .withComment("This is a table comment");

        String sql = sqlTable.asSql();

        assertThat(column1.getName()).isEqualTo("test1");
        assertThat(column1.getDataType()).isEqualTo("VARCHAR");
        assertThat(column1.getSize()).isEqualTo(25);
        assertThat(column1.getDefaultValue()).isEqualTo("def");

        assertThat(sql)
                .contains("CREATE TABLE `tests`")
                .contains("`test1` VARCHAR(25) NOT NULL DEFAULT 'def' COMMENT 'This is ''a'' field comment'")
                .contains("`test2` BIT")
                .contains("`test3` INT(11)")
                .contains("COMMENT 'This is a table comment'");
    }

}
