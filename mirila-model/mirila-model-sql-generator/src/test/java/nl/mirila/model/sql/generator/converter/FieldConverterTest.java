package nl.mirila.model.sql.generator.converter;

import nl.mirila.model.reader.models.FieldDefinition;
import nl.mirila.model.sql.generator.models.SqlColumn;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class FieldConverterTest {

    private FieldConverter converter;

    @BeforeEach
    void setUp() {
        converter = new FieldConverter();
    }

    @Test
    void testFieldConversion() {
        FieldDefinition definition = new FieldDefinition();
        definition.setName("my-field");
        definition.setType("string");
        definition.setDefaultValue("default");
        definition.setMaxLength(50);
        definition.setRequired(true);

        SqlColumn column = converter.convertField(definition);

        assertThat(column.getName()).isEqualTo("my_field");
        assertThat(column.getDataType()).isEqualTo("VARCHAR");
        assertThat(column.getDefaultValue()).isEqualTo("default");
        assertThat(column.getSize()).isEqualTo(50);
        assertThat(column.isNotNull()).isTrue();
        assertThat(column.isAutoIncrement()).isFalse();
    }

    @Test
    void testTypeConversion() {
        FieldDefinition definition = new FieldDefinition();
        definition.setName("testField");

        definition.setType("");
        assertThat(converter.getDataType(definition)).isEqualTo("VARCHAR");

        definition.setType(null);
        assertThat(converter.getDataType(definition)).isEqualTo("VARCHAR");

        definition.setType("string");
        assertThat(converter.getDataType(definition)).isEqualTo("VARCHAR");

        definition.setType("text");
        assertThat(converter.getDataType(definition)).isEqualTo("TEXT");

        definition.setType("json");
        assertThat(converter.getDataType(definition)).isEqualTo("JSON");

        definition.setType("id");
        assertThat(converter.getDataType(definition)).isEqualTo("INT");

        definition.setType("integer");
        assertThat(converter.getDataType(definition)).isEqualTo("INT");

        definition.setType("long");
        assertThat(converter.getDataType(definition)).isEqualTo("BIGINT");

        definition.setType("float");
        assertThat(converter.getDataType(definition)).isEqualTo("FLOAT");

        definition.setType("boolean");
        assertThat(converter.getDataType(definition)).isEqualTo("BIT");

        definition.setType("date");
        assertThat(converter.getDataType(definition)).isEqualTo("DATE");

        definition.setType("time");
        assertThat(converter.getDataType(definition)).isEqualTo("TIME");

        definition.setType("datetime");
        assertThat(converter.getDataType(definition)).isEqualTo("DATETIME");

        definition.setType("timestamp");
        assertThat(converter.getDataType(definition)).isEqualTo("TIMESTAMP");
    }

}
