package nl.mirila.model.sql.generator.converter;

import nl.mirila.model.reader.models.ModelDefinition;
import nl.mirila.model.sql.generator.models.SqlTable;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.impl.SimpleLog;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

/**
 * Converter of models.
 */
public class ModelConverter {

    private static final SimpleLog logger = new SimpleLog(ModelConverter.class.getSimpleName());


    private final FieldConverter fieldConverter;

    /**
     * Initialize a new instance.
     */
    public ModelConverter() {
        fieldConverter = new FieldConverter();
    }

    /**
     * Convert the given list of {@link ModelDefinition} into a list of {@link SqlTable}.
     * <p>
     * @param definitions The model definitions.
     * @return The SQL tables models.
     */
    public List<SqlTable> convertModels(Collection<ModelDefinition> definitions) {
        return definitions.stream()
                .filter((definition) -> !definition.isAbstract())
                .filter(ModelDefinition::isStorable)
                .map(this::convertModel)
                .sorted(Comparator.comparing(SqlTable::getName))
                .toList();
    }

    /**
     * Convert the given {@link ModelDefinition} into a {@link SqlTable}.
     * <p>
     * @param definition The model definition.
     * @return The SQL table.
     */
    public SqlTable convertModel(ModelDefinition definition) {
        String tableName = getTableName(definition);
        logger.info("Generating SQL for table " + tableName);

        List<String> primaryKey = definition.getPrimaryKeys()
                .stream()
                .map(ConverterUtils::getSqlName)
                .toList();

        return SqlTable.withName(tableName)
                .withColumns(fieldConverter.convertFields(definition.getFields()))
                .withPrimaryKey(primaryKey)
                .withComment(definition.getDescription());
    }

    /**
     * Return the table name based on the given {@link ModelDefinition}. We use the plural name, or otherwise the
     * singular name.
     * <p>
     * @param definition The model definition.
     * @return The SQL table name.
     */
    private String getTableName(ModelDefinition definition) {
        String name = (StringUtils.isNoneBlank(definition.getPluralName()))
                ? definition.getPluralName()
                : definition.getSingularName();
        return ConverterUtils.getSqlName(name);
    }

}
