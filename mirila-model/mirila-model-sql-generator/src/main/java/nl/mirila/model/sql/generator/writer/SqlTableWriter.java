package nl.mirila.model.sql.generator.writer;

import nl.mirila.core.exceptions.MirilaException;
import nl.mirila.model.sql.generator.models.SqlTable;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.impl.SimpleLog;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * Writer that stores multiple {@link SqlTable} as SQL to file.
 */
public class SqlTableWriter {

    private static final SimpleLog logger = new SimpleLog(SqlTableWriter.class.getSimpleName());

    private final String destPath;

    /**
     * Initialize a new instance.
     */
    public SqlTableWriter(String destPath) {
        if (!destPath.endsWith("/")) {
            destPath += "/";
        }
        this.destPath = destPath;
    }

    /**
     * Write the given list of {@link SqlTable} to file.
     */
    public void writeToSqlFile(List<SqlTable> sqlTables) {
        String sql = sqlTables.stream()
                .map(SqlTable::asSql)
                .collect(Collectors.joining("\n\n"));

        String sqlFileName = destPath + "database.sql";
        logger.info("Writing SQL to file " + sqlFileName);
        File file = new File(sqlFileName);
        try {
            FileUtils.writeStringToFile(file, sql, UTF_8);
        } catch (IOException e) {
            throw new MirilaException(e.getMessage(), e);
        }
    }

}
