package nl.mirila.model.sql.generator.models;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * Representation of a SQL column of a table.
 */
public class SqlColumn {

    final String name;
    String dataType;
    Integer size;
    boolean isNotNull;
    boolean isAutoIncrement;
    String defaultValue;
    String comment;

    /**
     * Initialize a new instance with the given name.
     */
    private SqlColumn(String name) {
        this.name = name;
    }

    /**
     * Return the name of this column.
     */
    public String getName() {
        return name;
    }

    /**
     * Return the data type of this column.
     */
    public String getDataType() {
        return dataType;
    }

    /**
     * Return the size of this column.
     */
    public Integer getSize() {
        return size;
    }

    /**
     * Return true if this column is marked as not null.
     */
    public boolean isNotNull() {
        return isNotNull;
    }

    /**
     * Return true if this column is marked auto-increment column.
     */
    public boolean isAutoIncrement() {
        return isAutoIncrement;
    }

    /**
     * Return the default value of this column.
     */
    public String getDefaultValue() {
        return defaultValue;
    }

    /**
     * Create a new instance of {@link SqlColumn} with the given name.
     * <p>
     * @param name The name.
     * @return The SQL column.
     */
    public static SqlColumn withName(String name) {
        return new SqlColumn(name);
    }

    /**
     * Add the given data type, and return the instance for chaining purposes.
     * <p>
     * @param dataType The data type.
     * @return This SQL column.
     */
    public SqlColumn withDataType(String dataType) {
        this.dataType = dataType;
        return this;
    }

    /**
     * Add the given size, and return the instance for chaining purposes.
     * <p>
     * @param size The size.
     * @return This SQL column.
     */
    public SqlColumn withSize(Integer size) {
        this.size = size;
        return this;
    }

    /**
     * Set to true if the column values may not be null, and return the instance for chaining purposes.
     * <p>
     * @param notNull True if the column is required.
     * @return This SQL column.
     */
    public SqlColumn withNotNull(boolean notNull) {
        isNotNull = notNull;
        return this;
    }

    /**
     * Set to true if the column is marked as auto-increment, and return the instance for chaining purposes.
     * <p>
     * @param autoIncrement True if auto-increment column.
     * @return This SQL column.
     */
    public SqlColumn withAutoIncrement(boolean autoIncrement) {
        isAutoIncrement = autoIncrement;
        return this;
    }

    /**
     * Add the given default size, and return the instance for chaining purposes.
     * <p>
     * @param defaultValue The default value.
     * @return This SQL column.
     */
    public SqlColumn withDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
        return this;
    }

    /**
     * Set the given comment and return this instance for chaining purposes.
     */
    public SqlColumn withComment(String comment) {
        this.comment = comment;
        return this;
    }

    /**
     * Return this SQL column as a SQL string.
     */
    public String asSql() {
        String sql = "`" + name + "` ";
        sql += (dataType != null) ? dataType : "VARCHAR";
        if ((size != null) && (size > 0)) {
            sql += "(" + size + ")";
        }
        if ((isNotNull) || (isAutoIncrement)) {
            sql += " NOT NULL";
        }

        if ((StringUtils.contains(defaultValue, "NOW")) && (isAutoIncrement)) {
            sql += " DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP";
        } else if (isAutoIncrement) {
            sql += " AUTO_INCREMENT";
        } else if (isNotBlank(defaultValue)) {
            boolean quoteStr = (defaultValue.equals("NOW"))
                    ? !Arrays.asList("TIMESTAMP", "DATE", "TIME", "DATETIME").contains(dataType)
                    : !Arrays.asList("INT", "BIT").contains(dataType);
            String value = quoteStr
                    ? "'" + defaultValue.replace("'", "''") + "'"
                    : defaultValue;
            sql += " DEFAULT " + value;
        }
        if (isNotBlank(comment)) {
            String normalized = StringUtils.abbreviate(comment, 2048).replace("'", "''");
            sql += String.format(" COMMENT '%s'", normalized);
        }
        return sql;
    }

}
