package nl.mirila.model.sql.generator.models;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * Representation of a SQL table.
 */
public class SqlTable {

    private final String name;
    private final List<SqlColumn> columns;
    private final List<String> primaryKey;
    private String comment;

    /**
     * Initialize a new instance with the given name.
     */
    private SqlTable(String name) {
        this.name = name;
        columns = new ArrayList<>();
        primaryKey = new ArrayList<>();
    }

    /**
     * Return the name of this table.
     */
    public String getName() {
        return name;
    }

    /**
     * Return the columns for this table.
     */
    public List<SqlColumn> getColumns() {
        return columns;
    }

    /**
     * Return the primary key for this table.
     */
    public List<String> getPrimaryKey() {
        return primaryKey;
    }

    /**
     * Create a new instance of {@link SqlTable} with the given name.
     * <p>
     * @param name The name.
     * @return The SQL table.
     */
    public static SqlTable withName(String name) {
        return new SqlTable(name);
    }

    /**
     * Add the given list of {@link SqlColumn} as columns and return the instance for chaining purposes.
     * <p>
     * @param columns The SQL columns.
     * @return This SQL table.
     */
    public SqlTable withColumns(Collection<SqlColumn> columns) {
        this.columns.clear();
        this.columns.addAll(columns);
        return this;
    }

    /**
     * Add the given list of column names as the primary key, and return the instance for chaining purposes.
     * <p>
     * @param primaryKey The primary key.
     * @return This SQL table.
     */
    public SqlTable withPrimaryKey(Collection<String> primaryKey) {
        this.primaryKey.clear();
        this.primaryKey.addAll(primaryKey);
        return this;
    }

    /**
     * Set the given comment and return this instance for chaining purposes.
     */
    public SqlTable withComment(String comment) {
        this.comment = comment;
        return this;
    }

    /**
     * Return this SQL table as a SQL string.
     */
    public String asSql() {
        String sql = "CREATE TABLE `" + name + "` (\n";
        sql += columns.stream()
                .map(SqlColumn::asSql)
                .map((line) -> "  " + line)
                .collect(Collectors.joining(",\n"));
        if (!primaryKey.isEmpty()) {
            sql += ",\n  primary key (" + String.join(",", primaryKey) + ")";
        }
        sql += "\n)";
        if (isNotBlank(comment)) {
            String normalized = StringUtils.abbreviate(comment, 2048).replace("'", "''");
            sql += String.format("\nCOMMENT '%s'", normalized);
        }
        sql += "\nCHARACTER SET utf8\nCOLLATE utf8_general_ci;";
        return sql;
    }

}
