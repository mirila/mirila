package nl.mirila.model.sql.generator;

import nl.mirila.model.reader.ModelFileReader;
import nl.mirila.model.reader.YamlFilesManager;
import nl.mirila.model.reader.models.ModelDefinition;
import nl.mirila.model.reader.utils.ModelExpander;
import nl.mirila.model.sql.generator.converter.ModelConverter;
import nl.mirila.model.sql.generator.models.SqlTable;
import nl.mirila.model.sql.generator.writer.SqlTableWriter;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Generate SQL based on Yaml files that contain model definitions.
 */
public class ModelSqlGenerator {

    /**
     * Load model definitions in Yaml files from the given source path, and generate pojos into the destination folder,
     * using the given package name.
     * <p>
     * @param srcPath The root folder that contains the Yaml files.
     * @param destPath The destination folder that will contain the SQL file.
     */
    public void generate(String srcPath, String destPath) {
        Map<String, ModelDefinition> models = loadModelDefinitions(srcPath);
        Map<String, ModelDefinition> expandedModels = ModelExpander.getInstance().getExpandedModels(models);
        writeSqlFiles(expandedModels, destPath);
    }

    /**
     * Load model definitions in Yaml files from the given source path.
     * <p>
     * @param srcPath The source path.
     * @return A map of model definitions.
     */
    protected Map<String, ModelDefinition> loadModelDefinitions(String srcPath) {
        YamlFilesManager fileManager = new YamlFilesManager(srcPath);
        ModelFileReader reader = new ModelFileReader();

        return fileManager.getSourceFilePaths()
                .stream()
                .map(reader::readModelDefinition)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toMap(ModelDefinition::getSingularName, Function.identity()));
    }

    /**
     * Write the given models as SQL files to the destination path.
     * <p>
     * @param expandedModels The expanded models.
     * @param destPath The destination path.
     */
    private void writeSqlFiles(Map<String, ModelDefinition> expandedModels, String destPath) {
        ModelConverter converter = new ModelConverter();
        SqlTableWriter writer = new SqlTableWriter(destPath);

        List<SqlTable> tables = converter.convertModels(expandedModels.values());
        writer.writeToSqlFile(tables);
    }

}
