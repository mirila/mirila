package nl.mirila.model.sql.generator.converter;

import static nl.mirila.core.utils.CaseConverter.KEBAB_CASE;
import static nl.mirila.core.utils.CaseConverter.PASCAL_CASE;

/**
 * Converter utilities needed for the SQL generator.
 */
public final class ConverterUtils {

    /**
     * This utility class may not be instantiated.
     */
    private ConverterUtils() {
    }

    /**
     * Convert a field name in the given name into a SQL table or column name.
     * <p>
     * @return The column name.
     */
    public static String getSqlName(String name) {
        return ((name.matches(".*[A-Z].*")) && (!name.contains("-")))
                ? PASCAL_CASE.toSnakeCase(name)
                : KEBAB_CASE.toSnakeCase(name);
    }

}
