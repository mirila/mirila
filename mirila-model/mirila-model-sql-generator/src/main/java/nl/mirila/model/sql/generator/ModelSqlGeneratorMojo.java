package nl.mirila.model.sql.generator;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import static org.apache.commons.lang3.StringUtils.isBlank;

/**
 * The mojo for the Model SQL generator.
 */
@Mojo(name = "mirila-model-sql-generator", defaultPhase = LifecyclePhase.COMPILE, threadSafe = true)
public class ModelSqlGeneratorMojo extends AbstractMojo {

    @Parameter(property = "sources.path")
    String srcPath;

    @Parameter(property = "destination.path")
    String destPath;

    /**
     * {@inheritDoc}
     */
    @Override
    public void execute() throws MojoExecutionException {
        if (isBlank(srcPath)) {
            String msg = "The sources.path configuration is missing.";
            getLog().error(msg);
            throw new MojoExecutionException(msg);
        }
        if (isBlank(destPath)) {
            String msg = "The destination.path configuration is missing.";
            getLog().error(msg);
            throw new MojoExecutionException(msg);
        }
        ModelSqlGenerator generator = new ModelSqlGenerator();
        generator.generate(srcPath, destPath);
    }

}
