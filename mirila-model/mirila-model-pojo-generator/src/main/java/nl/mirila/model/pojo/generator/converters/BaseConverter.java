package nl.mirila.model.pojo.generator.converters;

import com.squareup.javapoet.TypeSpec;
import nl.mirila.model.management.descriptor.ModelDescriptor;
import nl.mirila.model.reader.models.FieldDefinition;
import nl.mirila.model.reader.models.ModelDefinition;

import java.util.List;
import java.util.stream.Stream;

import static nl.mirila.core.utils.CaseConverter.CAMEL_CASE;
import static nl.mirila.core.utils.CaseConverter.KEBAB_CASE;
import static org.apache.commons.lang3.StringUtils.isBlank;

/**
 * A base class for all model to pojo converters.
 */
public abstract class BaseConverter {

    private static final List<String> reservedWords = Stream
            .of(("abstract assert boolean break byte case catch char class const continue default " +
                    "double do else enum extends false final finally float for goto if implements import " +
                    "instanceof int interface long native new null package private protected public return " +
                    "short static strictfp super switch synchronized this throw throws transient true try " +
                    "void volatile while").split(" "))
            .toList();

    /**
     * Write a {@link ModelDefinition} with model type of mixin as Java file.
     */
    public abstract TypeSpec getTypeSpec(ModelDefinition model);

    /**
     * Return true if the given name is a reserved word in Java.
     */
    protected boolean isReservedWord(String name) {
        return reservedWords.contains(name);
    }

    /**
     * Return the Java field name for the give model field name.
     * <p>
     * <p>If the variable name in the field starts with an illegal character,
     * of if the variable name is a reserved word, the variable name will be prefixed with an
     * underscore.
     * <p>
     * @param fieldName The name of the field.
     * @return The sanitized variable name.
     */
    protected String getSanitizedFieldName(String fieldName) {
        String name = fieldName;
        if ((!name.matches("[A-Za-z].*")) || (isReservedWord(name))) {
            name = "_" + name;
        }
        return KEBAB_CASE.toCamelCase(name);
    }

    /**
     * Return the column name for the given field name.
     * <p>
     * @param fieldName The name of the field.
     * @param columnCaseType The case type of the column.
     * @return The column name.
     */
    protected String getColumnNameForFieldName(String fieldName, String columnCaseType) {
        return switch (columnCaseType) {
            case "snake" -> KEBAB_CASE.toSnakeCase(fieldName);
            case "kebab" -> fieldName;
            default -> KEBAB_CASE.toCamelCase(fieldName);
        };
    }

    /**
     * Return a sanitized version of the given Javadoc string.
     * <p>
     * <p>Each dollar in the Javadoc must be escaped by prefixing them with another dollar.
     * <p>
     * @param javadoc The Javadoc.
     * @return The sanitized Javadoc.
     */
    protected String sanitizeJavadoc(String javadoc) {
        return (javadoc == null) ? null : javadoc.replace("$", "$$");
    }

    /**
     * Return the Javadoc based on the given description.
     * <p>
     * @param description The description
     * @return The Javadoc.
     */
    protected String getJavadoc(String description) {
        return (isBlank(description))
                ? ""
                : sanitizeJavadoc(description).trim();
    }

    /**
     * Return the name of the getter for the given {@link FieldDefinition}.
     * <p>
     * <p>A boolean field use the prefix "is", the rest uses the prefix "get".
     */
    protected String getGetterName(FieldDefinition field) {
        String pascalCasedName = CAMEL_CASE.toPascalCase(getSanitizedFieldName(field.getName()));
        if (field.getType().equals("boolean")) {
            List<String> startWords = ModelDescriptor.BOOLEAN_PREFIXES;
            String firstToken = KEBAB_CASE.getTokens(field.getName()).get(0);
            if (startWords.contains(firstToken)) {
                return getSanitizedFieldName(field.getName());
            } else {
                return "is" + pascalCasedName;
            }
        } else {
            return "get" + pascalCasedName;
        }
    }

    /**
     * Return the name of the setter for the given {@link FieldDefinition}.
     */
    protected String getSetterName(FieldDefinition field) {
        return "set" + CAMEL_CASE.toPascalCase(getSanitizedFieldName(field.getName()));
    }

}
