package nl.mirila.model.pojo.generator.converters;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;
import nl.mirila.model.reader.models.FieldDefinition;
import nl.mirila.model.reader.models.ModelDefinition;

import javax.lang.model.element.Modifier;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * Converter that converts abstract model definitions to Java interfaces.
 */
public class AbstractModelConverter extends BaseConverter {

    private final String packageName;
    private final DataTypeConverter dataTypeConverter;

    public AbstractModelConverter(String packageName) {
        this.packageName = packageName;
        dataTypeConverter = new DataTypeConverter(packageName, Collections.emptyList());
    }

    /**
     * Write a {@link ModelDefinition} with model type of mixin as Java file.
     */
    public TypeSpec getTypeSpec(ModelDefinition model) {
        TypeSpec.Builder builder = TypeSpec
                .interfaceBuilder(model.getSingularName())
                .addModifiers(Modifier.PUBLIC)
                .addJavadoc(getModelJavadoc(model))
                .addMethods(getMethodSpecs(model));

        model.getMixins().forEach((mixin) -> builder.addSuperinterface(ClassName.get(packageName, mixin)));
        return builder.build();
    }

    /**
     * Generate a Javadoc for the given {@link ModelDefinition}.
     * <p>
     * @param model The model definition.
     * @return The javadoc.
     */
    private String getModelJavadoc(ModelDefinition model) {
        return getJavadoc(model.getDescription());

    }

    /**
     * Generate a list of {@link MethodSpec} for all fields in the given {@link ModelDefinition}.
     * <p>
     * @param model The model definition.
     * @return The method specifications.
     */
    private List<MethodSpec> getMethodSpecs(ModelDefinition model) {
        return model.getFields()
                .stream()
                .map(this::fieldToMethodSpec)
                .flatMap(List::stream)
                .filter(Objects::nonNull)
                .toList();
    }

    /**
     * Generate a list of {@link MethodSpec} for all fields in the given {@link FieldDefinition}.
     * <p>
     * @param field The field definition.
     * @return The method specifications.
     */
    private List<MethodSpec> fieldToMethodSpec(FieldDefinition field) {
        String getterName = getGetterName(field);
        String setterName = getSetterName(field);
        String varName = getSanitizedFieldName(field.getName());
        String javadoc = sanitizeJavadoc(field.getDescription());
        TypeName fieldType = dataTypeConverter.typeToTypeName(field.getType());

        MethodSpec getter = MethodSpec
                .methodBuilder(getterName)
                .returns(fieldType)
                .addModifiers(Modifier.ABSTRACT, Modifier.PUBLIC)
                .addJavadoc(String.format("Return %s: %s", varName, javadoc))
                .build();

        MethodSpec setter = MethodSpec
                .methodBuilder(setterName)
                .addParameter(fieldType, varName)
                .addModifiers(Modifier.ABSTRACT, Modifier.PUBLIC)
                .addJavadoc(String.format("Set %s: %s", varName, javadoc))
                .build();

        return Arrays.asList(getter, setter);
    }

}
