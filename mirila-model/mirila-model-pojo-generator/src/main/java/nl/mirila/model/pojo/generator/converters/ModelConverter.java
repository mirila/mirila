package nl.mirila.model.pojo.generator.converters;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.squareup.javapoet.*;
import nl.mirila.core.datatype.Id;
import nl.mirila.core.datatype.Json;
import nl.mirila.model.core.annotations.*;
import nl.mirila.model.core.references.Model;
import nl.mirila.model.management.exceptions.UnknownModelException;
import nl.mirila.model.reader.exceptions.InvalidDefaultException;
import nl.mirila.model.reader.models.FieldDefinition;
import nl.mirila.model.reader.models.ModelDefinition;
import nl.mirila.model.reader.models.RightsDefinition;
import org.apache.commons.logging.impl.SimpleLog;

import javax.lang.model.element.Modifier;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoField;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static nl.mirila.core.utils.CaseConverter.CAMEL_CASE;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * Converter that converts model definitions to Java classes.
 */
public class ModelConverter extends BaseConverter {

    private static final SimpleLog logger = new SimpleLog(ModelConverter.class.getSimpleName());

    private final String columnCaseType;
    private final String packageName;
    private final DataTypeConverter dataTypeConverter;

    /**
     * Initialize a new instance.
     */
    public ModelConverter(String columnCaseType, String packageName, List<String> modelNames) {
        this.columnCaseType = columnCaseType;
        this.packageName = packageName;
        dataTypeConverter = new DataTypeConverter(packageName, modelNames);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TypeSpec getTypeSpec(ModelDefinition model) {
        TypeSpec.Builder builder = TypeSpec
                .classBuilder(model.getSingularName())
                .addModifiers(Modifier.PUBLIC)
                .addJavadoc(getModelJavadoc(model))
                .addAnnotations(getModelAnnotations(model))
                .addSuperinterface(Model.class)
                .addFields(getFieldSpecs(model))
                .addMethods(getMethodSpecs(model));

        if (!model.isAbstract()) {
            builder.addType(getFieldNamesClass(model));
        }

        model.getMixins().forEach((mixin) -> {
            ModelDefinition mixinModel = model.getMixinModels().get(mixin);
            if (mixinModel == null) {
                throw new UnknownModelException(mixin);
            }
            if (mixinModel.isAbstract()) {
                builder.addSuperinterface(ClassName.get(packageName, mixin));
            } else {
                builder.superclass(ClassName.get(packageName, mixin));
            }
        });
        return builder.build();
    }

    /**
     * Return a list of {@link AnnotationSpec} for this {@link ModelDefinition}.
     * <p>
     * @param model The model definition.
     * @return The list of annotations.
     */
    private List<AnnotationSpec> getModelAnnotations(ModelDefinition model) {
        ArrayList<AnnotationSpec> annotations = new ArrayList<>();
        AnnotationSpec.Builder modelBuilder = AnnotationSpec.builder(ModelInfo.class)
                .addMember("singular", "$S", model.getSingularName())
                .addMember("plural", "$S", model.getPluralName());

        model.getPrimaryKeys().forEach((fieldName) -> {
            addFieldNameAnnotationMember(modelBuilder, "primaryKeyFields", fieldName);
        });

        Optional<RightsDefinition> optional = model.getRightsFields();
        if (optional.isPresent()) {
            RightsDefinition rightsFields = optional.get();
            addFieldNameAnnotationMember(modelBuilder, "ownerIdsField", rightsFields.getOwnerIdsFieldName());
            addFieldNameAnnotationMember(modelBuilder, "groupIdsField", rightsFields.getGroupsIdsFieldName());
            addFieldNameAnnotationMember(modelBuilder, "rightsField", rightsFields.getRightsFieldName());
            addFieldNameAnnotationMember(modelBuilder, "rightsCategory", rightsFields.getRightsCategory());
        }
        annotations.add(modelBuilder.build());

        if (!model.isStorable()) {
            annotations.add(AnnotationSpec.builder(NonStorable.class).build());
        }

        AnnotationSpec jsonRootNameBuilder = AnnotationSpec.builder(JsonRootName.class)
                .addMember("value", "$S", model.getSingularName())
                .build();
        annotations.add(jsonRootNameBuilder);

        return annotations;
    }

    /**
     * Add an field name as an annotation member.
     * <p>
     * @param builder The annotation builder.
     * @param annotationField The name of the annotation field.
     * @param fieldName The field name of the model.
     */
    private void addFieldNameAnnotationMember(AnnotationSpec.Builder builder, String annotationField, String fieldName) {
        if (isNotBlank(fieldName)) {
            builder.addMember(annotationField, "$S", getSanitizedFieldName(fieldName));
        }
    }

    /**
     * Generate a Javadoc for the given {@link ModelDefinition}.
     * <p>
     * @param model The model definition.
     * @return The javadoc.
     */
    private String getModelJavadoc(ModelDefinition model) {
        return getJavadoc(model.getDescription());

    }

    /**
     * Generate a list of {@link FieldSpec} for all fields in the given {@link ModelDefinition}.
     * <p>
     * @param model The model.
     * @return The field specification.
     */
    private List<FieldSpec> getFieldSpecs(ModelDefinition model) {
        return model.getFields()
                .stream()
                .map(this::getFieldSpec)
                .toList();
    }

    /**
     * Generate the {@link FieldSpec} for the given {@link FieldDefinition}.
     * <p>
     * @param field The Wise field.
     * @return The field specification.
     */
    private FieldSpec getFieldSpec(FieldDefinition field) {
        TypeName typeName = dataTypeConverter.typeToTypeName(field.getType());
        String fieldName = getSanitizedFieldName(field.getName());

        return FieldSpec.builder(typeName, fieldName)
                .addModifiers(Modifier.PRIVATE)
                .addJavadoc(getFieldJavadoc(field))
                .addAnnotations(getFieldAnnotations(field))
                .build();
    }

    /**
     * Generate a list of {@link AnnotationSpec} for the given {@link FieldDefinition}.
     * <p>
     * @param field The Wise field.
     * @return A list of annotations.
     */
    private List<AnnotationSpec> getFieldAnnotations(FieldDefinition field) {
        ArrayList<AnnotationSpec> annotations = new ArrayList<>();

        if (field.isServerOnly()) {
            annotations.add(AnnotationSpec.builder(JsonIgnore.class).build());
        } else {

            AnnotationSpec jsonPropertyAnnotation = AnnotationSpec.builder(JsonProperty.class)
                    .addMember("access", "$L", getAccessForField(field))
                    .build();
            annotations.add(jsonPropertyAnnotation);
        }

        if (field.isRequired()) {
            annotations.add(AnnotationSpec.builder(Required.class).build());
        }

        if (field.isAutoGenerated()) {
            annotations.add(AnnotationSpec.builder(AutoGenerated.class).build());
        }

        if (field.getMaxLength() > 0) {
            AnnotationSpec maxLengthAnnotation = AnnotationSpec.builder(MaxLength.class)
                    .addMember("value", "$L", field.getMaxLength())
                    .build();
            annotations.add(maxLengthAnnotation);
        }

        return annotations;
    }

    /**
     * Return the correct {@link JsonProperty.Access} for the given {@link FieldDefinition}.
     * <p>
     * @param field The field.
     * @return The access.
     */
    public static JsonProperty.Access getAccessForField(FieldDefinition field) {
        return (field.isReadOnly())
                ? JsonProperty.Access.READ_ONLY
                : JsonProperty.Access.AUTO;
    }

    /**
     * Generate a Javadoc for the given {@link FieldDefinition}.
     * <p>
     * @param field The field definition.
     * @return The javadoc.
     */
    private String getFieldJavadoc(FieldDefinition field) {
        return getJavadoc(field.getDescription());
    }


    /**
     * Generate a list of {@link MethodSpec} for all fields in the given {@link ModelDefinition}.
     * <p>
     * @param model The model definition.
     * @return The method specifications.
     */
    private List<MethodSpec> getMethodSpecs(ModelDefinition model) {

        List<MethodSpec> methods = model.getFields()
                .stream()
                .map(this::fieldToMethodSpec)
                .flatMap(List::stream)
                .filter(Objects::nonNull)
                .collect(Collectors.toCollection(ArrayList::new)); // Return a mutable list.

        // Add the constructor at index 0, as we want the constructor to be the first method.
        methods.add(0, getConstructor(model));
        return methods;
    }

    /**
     * Generate a {@link MethodSpec} that contains the constructor for the pojo of the given {@link ModelDefinition}.
     * <p>
     * This constructor will contain all the assignment of the default values.
     * <p>
     * @param model The model definition.
     * @return The method specifications.
     */
    private MethodSpec getConstructor(ModelDefinition model) {
        List<CodeBlock> statements = model.getFields().stream()
                .filter((field) -> field.getDefaultValue() != null)
                .map(this::defaultValueToCodeBlock)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .toList();

        MethodSpec.Builder builder = MethodSpec.constructorBuilder();
        statements.forEach(builder::addStatement);
        return builder.addModifiers(Modifier.PUBLIC).build();
    }

    /**
     * Returns the assignment statement in a {@link CodeBlock} that applies to default value onto the property
     * that is related to the given {@link FieldDefinition}.
     * <p>
     * @param field The field.
     * @return The assignment statement.
     */
    private Optional<CodeBlock> defaultValueToCodeBlock(FieldDefinition field) {
        TypeName fieldType = dataTypeConverter.typeToTypeName(field.getType());
        String varName = getSanitizedFieldName(field.getName());
        String defaultValue = field.getDefaultValue().toString();

        if (defaultValue.equals("NULL")) {
            return Optional.of(CodeBlock.of("$L = null", varName));
        }

        String type = field.getType().toLowerCase();
        if ((isBlank(defaultValue)) && (!type.equals("string"))) {
            // Can not set empty to a non String variable.
            return Optional.empty();
        }

        try {
            if (fieldType.equals(ClassName.get(String.class))) {
                return Optional.of(CodeBlock.of("$L = $S", varName, defaultValue));
            } else if (fieldType.equals(ClassName.get(Id.class))) {
                return Optional.of(CodeBlock.of("$L = Id.of($S)", varName, defaultValue));
            } else if (fieldType.equals(ClassName.get(Json.class))) {
                return Optional.of(CodeBlock.of("$L = Json.of($S)", varName, defaultValue));
            } else if ((fieldType.equals(ClassName.get(Integer.class))) ||
                       (fieldType.equals(ClassName.get(Long.class)))) {
                return getCodeBlockForDefaultNumber(varName, defaultValue);
            } else if (fieldType.equals(ClassName.get(BigDecimal.class))) {
                return getCodeBlockForDefaultBigDecimal(varName, defaultValue);
            } else if (fieldType.equals(ClassName.get(LocalDate.class))) {
                return getCodeBlockForDefaultDate(varName, defaultValue);
            } else if (fieldType.equals(ClassName.get(LocalTime.class))) {
                return getCodeBlockForDefaultTime(varName, defaultValue);
            } else if (fieldType.equals(ClassName.get(LocalDateTime.class))) {
                return getCodeBlockForDefaultDateTime(varName, defaultValue);
            } else if (fieldType.equals(ClassName.get(Instant.class))) {
                return getCodeBlockForDefaultTimestamp(varName, defaultValue);
            } else if (fieldType.equals(ClassName.get(Boolean.class))) {
                return Optional.of(CodeBlock.of("$L = $L", varName, defaultValue));
            }
        }
        catch (IllegalArgumentException e) {
            throw new InvalidDefaultException(field, e);
        }

        String msg = "Do not known how to set the default for %s.%s of type %s. Ignoring this.";
        logger.warn(String.format(msg, field.getModel().getSingularName(), field.getName(), fieldType));
        return Optional.empty();
    }

    /**
     * Return the code block that assigns the default value to the {@link Integer} or {@link Long} field.
     * <p>
     * An invalid default value, will raise an {@link IllegalArgumentException}.
     * <p>
     * @param varName The variable name.
     * @param defaultValue The default value.
     * @return The code block, wrapped in an {@link Optional}.
     */
    private Optional<CodeBlock> getCodeBlockForDefaultNumber(String varName, String defaultValue) {
        boolean isChronoField = Stream.of(ChronoField.values())
                .anyMatch((field) -> field.name().equals(defaultValue));

        if (isChronoField) {
            return Optional.of(CodeBlock.of("$L = LocalDate.now().get(ChronoField.$L)", varName, defaultValue));
        } else if (defaultValue.matches("^-?\\d+$")) {
            return Optional.of(CodeBlock.of("$L = $L", varName, defaultValue));
        } else {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Return the code block that assigns the default value to the {@link BigDecimal} field.
     * <p>
     * An invalid default value, will raise an {@link IllegalArgumentException}.
     * <p>
     * @param varName The variable name.
     * @param defaultValue The default value.
     * @return The code block, wrapped in an {@link Optional}.
     */
    private Optional<CodeBlock> getCodeBlockForDefaultBigDecimal(String varName, String defaultValue) {
        // Try creating a BigDecimal.
        new BigDecimal(defaultValue);
        return Optional.of(CodeBlock.of("$L = new BigDecimal($S)", varName, defaultValue));
    }

    /**
     * Return the code block that assigns the default value to the {@link LocalDate} field.
     * <p>
     * An invalid default value, will raise an {@link IllegalArgumentException}.
     * <p>
     * @param varName The variable name.
     * @param defaultValue The default value.
     * @return The code block, wrapped in an {@link Optional}.
     */
    private Optional<CodeBlock> getCodeBlockForDefaultDate(String varName, String defaultValue) {
        if (defaultValue.equalsIgnoreCase("NOW")) {
            return Optional.of(CodeBlock.of("$L = LocalDate.now()", varName));
        } else if (defaultValue.matches("^\\d{4}-\\d{2}-\\d{2}$")) {
            return Optional.of(CodeBlock.of("$L = LocalDate.parse($S)", varName, defaultValue));
        } else {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Return the code block that assigns the default value to the {@link LocalTime} field.
     * <p>
     * An invalid default value, will raise an {@link IllegalArgumentException}.
     * <p>
     * @param varName The variable name.
     * @param defaultValue The default value.
     * @return The code block, wrapped in an {@link Optional}.
     */
    private Optional<CodeBlock> getCodeBlockForDefaultTime(String varName, String defaultValue) {
        if (defaultValue.equalsIgnoreCase("NOW")) {
            return Optional.of(CodeBlock.of("$L = LocalTime.now()", varName));
        } else if (defaultValue.matches("^\\d{2}:\\d{2}(:\\d{2}(\\.\\d{1,9})?)?$")) {
            return Optional.of(CodeBlock.of("$L = LocalTime.parse($S)", varName, defaultValue));
        } else {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Return the code block that assigns the default value to the {@link LocalDateTime} field.
     * <p>
     * An invalid default value, will raise an {@link IllegalArgumentException}.
     * <p>
     * @param varName The variable name.
     * @param defaultValue The default value.
     * @return The code block, wrapped in an {@link Optional}.
     */
    private Optional<CodeBlock> getCodeBlockForDefaultDateTime(String varName, String defaultValue) {
        if (defaultValue.equalsIgnoreCase("NOW")) {
            return Optional.of(CodeBlock.of("$L = LocalDateTime.now()", varName));
        } else if (defaultValue.matches("^\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}(:\\d{2}(\\.\\d{1,9})?)?$")) {
            return Optional.of(CodeBlock.of("$L = LocalDateTime.parse($S)", varName, defaultValue));
        } else {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Return the code block that assigns the default value to the timestamp field.
     * <p>
     * An invalid default value, will raise an {@link IllegalArgumentException}.
     * <p>
     * @param varName The variable name.
     * @param defaultValue The default value.
     * @return The code block, wrapped in an {@link Optional}.
     */
    private Optional<CodeBlock> getCodeBlockForDefaultTimestamp(String varName, String defaultValue) {
        if (defaultValue.equalsIgnoreCase("NOW")) {
            return Optional.of(CodeBlock.of("$L = Instant.now()", varName));
        } else if (defaultValue.matches("^\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}(:\\d{2}(\\.\\d{1,9})?)?Z?$")) {
            return Optional.of(CodeBlock.of("$L = Instant.parse($S)", varName, defaultValue));
        } else {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Generate a list of {@link MethodSpec} for all fields in the given {@link FieldDefinition}.
     * <p>
     * @param field The field definition.
     * @return The method specifications.
     */
    private List<MethodSpec> fieldToMethodSpec(FieldDefinition field) {
        String getterName = getGetterName(field);
        String setterName = getSetterName(field);
        String varName = getSanitizedFieldName(field.getName());
        String javadoc = sanitizeJavadoc(field.getDescription());
        TypeName typeName = dataTypeConverter.typeToTypeName(field.getType());

        boolean isOverriding = field.getParentFieldDefinition()
                .map(FieldDefinition::getModel)
                .map(ModelDefinition::isAbstract)
                .orElse(false);

        MethodSpec.Builder getterBuilder = MethodSpec
                .methodBuilder(getterName)
                .addModifiers(Modifier.PUBLIC)
                .returns(typeName)
                .addJavadoc(String.format("Return %s: %s", varName, javadoc))
                .addStatement(String.format("return %s", varName));
        if (isOverriding) {
            getterBuilder.addAnnotation(Override.class);
        }
        MethodSpec getter = getterBuilder.build();

        MethodSpec.Builder setterBuilder = MethodSpec
                .methodBuilder(setterName)
                .addModifiers(Modifier.PUBLIC)
                .addParameter(typeName, varName)
                .addJavadoc(String.format("Set %s: %s", varName, javadoc))
                .addCode(getSetterCodeBlock(varName, field.isRequired(), typeName));
        if (isOverriding) {
            setterBuilder.addAnnotation(Override.class);
        }
        MethodSpec setter = setterBuilder.build();
        return Arrays.asList(getter, setter);
    }

    /**
     * Return a {@link CodeBlock} for the setter of the given field name.
     * <p>
     * @param varName The field name.
     * @param isRequired True if the field is required.
     * @param fieldType The type of the field.
     * @return The code block for the setter.
     */
    private CodeBlock getSetterCodeBlock(String varName, boolean isRequired, TypeName fieldType) {
        if (isRequired) {
            String conditional = (fieldType == ClassName.get(String.class))
                    ? "if (($1L != null) && (!$1L.trim().length == 0))"
                    : "if ($1L != null)";
            CodeBlock.Builder builder = CodeBlock.builder();
            builder.beginControlFlow(conditional, varName);
            builder.addStatement("this.$1L = $1L", varName);
            builder.endControlFlow();
            return builder.build();
        }

        return CodeBlock.builder()
                .addStatement("this.$1L = $1L", varName)
                .build();
    }

    /**
     * Return the {@link TypeSpec} that contains the inner class Fields, containing the names for each field.
     * <p>
     * @param model The model.
     * @return The inner class.
     */
    private TypeSpec getFieldNamesClass(ModelDefinition model) {
        return TypeSpec.classBuilder("Fields")
                .addJavadoc("Convenience class that contains all fields names for this model.")
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC, Modifier.FINAL)
                .addFields(getFieldNamesSpec(model))
                .build();
    }

    /**
     * Return a list of {@link FieldSpec} containing the column names for each field.
     * <p>
     * @param model The model.
     * @return The static class members.
     */
    private List<FieldSpec> getFieldNamesSpec(ModelDefinition model) {
        return model.getFields()
                .stream()
                .map(this::getFieldNameSpec)
                .toList();
    }

    /**
     * Return the {@link FieldSpec} containing the field name as class member for the given field.
     * <p>
     * @param field The field.
     * @return The static class member.
     */
    private FieldSpec getFieldNameSpec(FieldDefinition field) {
        String fieldName = getSanitizedFieldName(field.getName());
        String constName = CAMEL_CASE.toScreamingSnakeCase(fieldName);

        return FieldSpec.builder(String.class, constName)
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC, Modifier.FINAL)
                .initializer("$S", fieldName)
                .build();
    }

}
