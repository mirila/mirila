package nl.mirila.model.pojo.generator;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import static org.apache.commons.lang3.StringUtils.isBlank;

/**
 * This Maven plugin converts loaded models into pojos.
 */
@Mojo(name = "mirila-model-pojo-generator", defaultPhase = LifecyclePhase.COMPILE, threadSafe = true)
public class ModelPojoGeneratorMojo extends AbstractMojo {

    @Parameter(property = "sources.path")
    String srcPath;

    @Parameter(property = "destination.path")
    String destPath;

    @Parameter(property = "package.name")
    String packageName;

    @Parameter(property = "column.case")
    String columnCaseType;

    /**
     * {@inheritDoc}
     */
    @Override
    public void execute() throws MojoExecutionException {
        if (isBlank(srcPath)) {
            String msg = "The sources.path configuration is missing.";
            getLog().error(msg);
            throw new MojoExecutionException(msg);
        }
        if (isBlank(destPath)) {
            String msg = "The destination.path configuration is missing.";
            getLog().error(msg);
            throw new MojoExecutionException(msg);
        }
        if (isBlank(packageName)) {
            String msg = "The package.name configuration is missing.";
            getLog().error(msg);
            throw new MojoExecutionException(msg);
        }

        ModelPojoGenerator generator = new ModelPojoGenerator();
        generator.generate(srcPath, destPath, packageName, columnCaseType);
    }

}
