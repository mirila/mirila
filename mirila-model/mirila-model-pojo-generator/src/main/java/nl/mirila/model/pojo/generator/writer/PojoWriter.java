package nl.mirila.model.pojo.generator.writer;

import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.TypeSpec;
import nl.mirila.model.pojo.generator.converters.AbstractModelConverter;
import nl.mirila.model.pojo.generator.converters.BaseConverter;
import nl.mirila.model.pojo.generator.converters.ModelConverter;
import nl.mirila.model.reader.models.ModelDefinition;
import org.apache.commons.logging.impl.SimpleLog;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

/**
 * Write Java files for {@link ModelDefinition}.
 */
public class PojoWriter {

    private static final SimpleLog logger = new SimpleLog(PojoWriter.class.getSimpleName());

    private static final List<String> reservedWords = Stream
            .of(("abstract assert boolean break byte case catch char class const continue default " +
                    "double do else enum extends false final finally float for goto if implements import " +
                    "instanceof int interface long native new null package private protected public return " +
                    "short static strictfp super switch synchronized this throw throws transient true try " +
                    "void volatile while").split(" "))
            .toList();

    private final String packageName;
    private final String destPath;

    private final ModelConverter modelConverter;
    private final AbstractModelConverter abstractModelConverter;

    /**
     * Initialize an instance.
     */
    public PojoWriter(String packageName, String destPath, String columnCaseType, List<String> modelNames) {
        this.packageName = packageName;
        this.destPath = destPath;

        modelConverter = new ModelConverter(columnCaseType, packageName, modelNames);
        abstractModelConverter = new AbstractModelConverter(packageName);
    }

    /**
     * Write the list of {@link ModelDefinition} as Java files.
     */
    public void writeModels(Map<String, ModelDefinition> models) {
        models.values().forEach(this::writeModel);
    }

    /**
     * Write the {@link ModelDefinition} as a Java file.
     */
    public void writeModel(ModelDefinition model) {
        logger.info("Generating pojo " + model.getSingularName());

        BaseConverter converter = (model.isAbstract()) ? abstractModelConverter : modelConverter;
        TypeSpec modelPojo;
        try {
            modelPojo = converter.getTypeSpec(model);
        } catch (Exception e) {
            String msg = String.format("Error occurred while processing model %s: %s",
                                       model.getSingularName(),
                                       e.getMessage());
            logger.error(msg, e);
            throw e;
        }

        JavaFile.Builder javaFileBuilder = JavaFile.builder(packageName, modelPojo);
        if (!model.isAbstract()) {
            model.getFields().stream()
                    .map(ModelConverter::getAccessForField)
                    .forEach(javaFileBuilder::addStaticImport);
        }

        try {
            javaFileBuilder.build().writeTo(new File(this.destPath));
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
    }

}
