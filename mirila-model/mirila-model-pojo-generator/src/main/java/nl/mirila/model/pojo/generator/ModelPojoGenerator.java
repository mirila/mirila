package nl.mirila.model.pojo.generator;

import nl.mirila.model.pojo.generator.writer.PojoWriter;
import nl.mirila.model.reader.ModelFileReader;
import nl.mirila.model.reader.YamlFilesManager;
import nl.mirila.model.reader.models.ModelDefinition;
import nl.mirila.model.reader.utils.ModelExpander;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Generate model pojo's based on Yaml files that contain model definitions.
 */
public class ModelPojoGenerator {

    /**
     * Load model definitions in Yaml files from the given source path, and generate pojos into the destination folder,
     * using the given package name.
     * <p>
     * @param srcPath The root folder that contains the Yaml files.
     * @param destPath The destination folder that will contain the pojo's.
     * @param packageName The package name.
     * @param columnCaseType The case type of the column names.
     */
    public void generate(String srcPath, String destPath, String packageName, String columnCaseType) {
        Map<String, ModelDefinition> models = loadModelDefinitions(srcPath);
        Map<String, ModelDefinition> expandedModels = ModelExpander.getInstance().getExpandedModels(models);
        writePojos(expandedModels, destPath, packageName, columnCaseType);
    }

    /**
     * Load model definitions in Yaml files from the given source path.
     * <p>
     * @param srcPath The source path.
     * @return A map of model definitions.
     */
    protected Map<String, ModelDefinition> loadModelDefinitions(String srcPath) {
        YamlFilesManager fileManager = new YamlFilesManager(srcPath);
        ModelFileReader reader = new ModelFileReader();

        return fileManager.getSourceFilePaths()
                .stream()
                .map(reader::readModelDefinition)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toMap(ModelDefinition::getSingularName,
                                          Function.identity(),
                                          (oldest, newest) -> oldest,
                                          TreeMap::new));
    }

    /**
     * Writes pojo's for the given model definitions into the destination folder, using the given package name.
     * <p>
     * @param models The model definitions.
     * @param destPath The destination folder.
     * @param packageName The package name.
     * @param columnCaseType The case type of the column names.
     */
    private void writePojos(Map<String, ModelDefinition> models,
                            String destPath,
                            String packageName,
                            String columnCaseType) {
        List<String> modelNames = new ArrayList<>(models.keySet());
        PojoWriter writer = new PojoWriter(packageName, destPath, columnCaseType, modelNames);
        writer.writeModels(models);
    }

}
