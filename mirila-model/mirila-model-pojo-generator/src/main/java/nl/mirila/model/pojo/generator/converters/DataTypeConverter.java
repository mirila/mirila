package nl.mirila.model.pojo.generator.converters;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeName;
import nl.mirila.core.datatype.Id;
import nl.mirila.core.datatype.Json;
import nl.mirila.model.core.references.Reference;
import nl.mirila.model.reader.exceptions.MissingTypeException;
import nl.mirila.model.reader.exceptions.UnknownTypeException;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.Map;

import static org.apache.commons.lang3.StringUtils.isBlank;

/**
 * Converter that converts model definition types to Java types.
 */
public final class DataTypeConverter {

    private String packageName;
    private List<String> modelNames;

    /**
     * Initialize a new instance.
     */
    public DataTypeConverter(String packageName, List<String> modelNames) {
        this.packageName = packageName;
        this.modelNames = modelNames;
    }

    /**
     * Return a JavaPoet {@link TypeName} for the given type in the field definition.
     * <p>
     * @param type The type.
     * @return The Java type class.
     */
    public TypeName typeToTypeName(String type) {
        if (isBlank(type)) {
            return ClassName.get(String.class);
        }
        type = type.trim();

        if (modelNames.contains(type)) {
            return ClassName.get(packageName, type);
        }

        if (type.contains(" ")) {
            String[] parts = type.split("\\s+(of|on|to)\\s+", 2);
            String containerType = parts[0];
            String specificType = parts[1];

            switch (containerType) {
                case "map":
                    String[] keyValueTypes = specificType.split("\\s*,\\s+", 2);
                    ClassName mapTypeName = ClassName.get(Map.class);
                    TypeName mapKeyTypeName = typeToTypeName(keyValueTypes[0]);
                    TypeName mapValueTypeName = typeToTypeName(keyValueTypes[1]);
                    return ParameterizedTypeName.get(mapTypeName, mapKeyTypeName, mapValueTypeName);
                case "list":
                    ClassName listTypeName = ClassName.get(List.class);
                    TypeName listValueTypeName = typeToTypeName(specificType);
                    return ParameterizedTypeName.get(listTypeName, listValueTypeName);
                case "reference":
                    return ClassName.get(Reference.class);
                default:
                    throw new UnknownTypeException(type);
            }
        }

        return switch (type.toLowerCase()) {
            case "id" -> ClassName.get(Id.class);
            case "string", "text" -> ClassName.get(String.class);
            case "json" -> ClassName.get(Json.class);
            case "integer" -> ClassName.get(Integer.class);
            case "long" -> ClassName.get(Long.class);
            case "float" -> ClassName.get(BigDecimal.class);
            case "boolean" -> ClassName.get(Boolean.class);
            case "date" -> ClassName.get(LocalDate.class);
            case "time" -> ClassName.get(LocalTime.class);
            case "datetime" -> ClassName.get(LocalDateTime.class);
            case "timestamp" -> ClassName.get(Instant.class);
            case "reference", "map", "list" -> throw new MissingTypeException(type);
            default -> throw new UnknownTypeException(type);
        };
    }

}
