package nl.mirila.model.pojo.generator.utils;

import com.squareup.javapoet.ClassName;
import nl.mirila.core.datatype.Id;
import nl.mirila.model.core.references.Reference;
import nl.mirila.model.pojo.generator.converters.DataTypeConverter;
import nl.mirila.model.reader.exceptions.MissingTypeException;
import nl.mirila.model.reader.exceptions.UnknownTypeException;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatNoException;

class DataTypeConverterTest {

    private DataTypeConverter converter;

    @BeforeEach
    void setUp() {
        converter = new DataTypeConverter("org.example.package", List.of("TestPojo1", "TestPojo2"));
    }

    @Test
    void testId() {
        Assertions.assertThat(converter.typeToTypeName("id")).isEqualTo(ClassName.get(Id.class));
    }

    @Test
    void testString() {
        Assertions.assertThat(converter.typeToTypeName("string")).isEqualTo(ClassName.get(String.class));
    }

    @Test
    void testText() {
        Assertions.assertThat(converter.typeToTypeName("text")).isEqualTo(ClassName.get(String.class));
    }

    @Test
    void testInteger() {
        Assertions.assertThat(converter.typeToTypeName("integer")).isEqualTo(ClassName.get(Integer.class));
    }

    @Test
    void testFloat() {
        Assertions.assertThat(converter.typeToTypeName("float")).isEqualTo(ClassName.get(BigDecimal.class));
    }

    @Test
    void testBoolean() {
        Assertions.assertThat(converter.typeToTypeName("boolean")).isEqualTo(ClassName.get(Boolean.class));
    }

    @Test
    void testDate() {
        Assertions.assertThat(converter.typeToTypeName("date")).isEqualTo(ClassName.get(LocalDate.class));
    }

    @Test
    void testTime() {
        Assertions.assertThat(converter.typeToTypeName("time")).isEqualTo(ClassName.get(LocalTime.class));
    }

    @Test
    void testDateTime() {
        Assertions.assertThat(converter.typeToTypeName("datetime")).isEqualTo(ClassName.get(LocalDateTime.class));
    }

    @Test
    void testReference() {
        assertThatExceptionOfType(MissingTypeException.class).isThrownBy(() -> converter.typeToTypeName("reference"));
        Assertions.assertThat(converter.typeToTypeName("reference to Example1.field1"))
                .isEqualTo(ClassName.get(Reference.class));
    }

    @Test
    void testList() {
        assertThatExceptionOfType(MissingTypeException.class).isThrownBy(() -> converter.typeToTypeName("list"));
    }

    @Test
    void testMap() {
        assertThatExceptionOfType(MissingTypeException.class).isThrownBy(() -> converter.typeToTypeName("map"));
    }

    @Test
    void testUnsupportedModelName() {
        assertThatExceptionOfType(UnknownTypeException.class).isThrownBy(() -> converter.typeToTypeName("UnknownPojo"));
    }

    @Test
    void testSupportedModelName() {
        assertThatNoException().isThrownBy(() -> converter.typeToTypeName("TestPojo1"));
    }


}
