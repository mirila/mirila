package nl.mirila.model.pojo.generator;

import nl.mirila.model.reader.models.ModelDefinition;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

class ModelPojoGeneratorTest {

    @Test
    void testGetModels() {
        Path srcPath = Paths.get("src", "test", "resources", "examples");
        ModelPojoGenerator generator = new ModelPojoGenerator();
        Map<String, ModelDefinition> models = generator.loadModelDefinitions(srcPath.toAbsolutePath().toString());

        assertThat(models.get("Example1").getSingularName()).isEqualTo("Example1");
        assertThat(models.get("Example2").getSingularName()).isEqualTo("Example2");
        assertThat(models.get("Example3").getSingularName()).isEqualTo("Example3");
        assertThat(models.get("Example4").getSingularName()).isEqualTo("Example4");
    }

    @Test
    void testGenerate() {
        Path srcPath = Paths.get("src", "test", "resources", "package");
        String src = srcPath.toAbsolutePath().toString();

        Path destPath = Paths.get("target", "generated-test-sources");
        String dest = destPath.toAbsolutePath().toString();

        String packageName = "nl.mirila.test.examples";
        String columnCaseType = "snake";

        ModelPojoGenerator generator = new ModelPojoGenerator();
        generator.generate(src, dest, packageName, columnCaseType);
    }

}
