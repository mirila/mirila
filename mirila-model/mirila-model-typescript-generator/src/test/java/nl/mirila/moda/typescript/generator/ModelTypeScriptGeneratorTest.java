package nl.mirila.moda.typescript.generator;

import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.nio.file.Paths;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatNoException;

class ModelTypeScriptGeneratorTest {

    @Test
    void testGenerate() {
        Path srcPath = Paths.get("src", "test", "resources");
        String src = srcPath.toAbsolutePath().toString();

        Path destPath = Paths.get("target", "generated-test-sources", "TestModels.ts");
        String dest = destPath.toAbsolutePath().toString();

        ModelTypeScriptGenerator generator = new ModelTypeScriptGenerator();
        assertThatNoException().isThrownBy(() -> generator.generate(src, dest, "My Test Project"));
    }

}
