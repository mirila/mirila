package nl.mirila.moda.typescript.generator.models;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class TypeScriptModel {

    private String name;
    private List<TypeScriptField> fields;
    private ObjectType objectType;

    /**
     * Initialize a new instance.
     */
    private TypeScriptModel(String name) {
        this.name = name;
        objectType = ObjectType.CLASS;
        fields = new ArrayList<>();
    }

    /**
     * Return the name.
     */
    public String getName() {
        return name;
    }

    /**
     * Set the given name and return this instance for chaining purposes.
     */
    public static TypeScriptModel forName(String name) {
        return new TypeScriptModel(name);
    }

    /**
     * Return the fields.
     */
    public List<TypeScriptField> getFields() {
        return fields;
    }

    /**
     * Set the given fields and return this instance for chaining purposes.
     */
    public TypeScriptModel withFields(Collection<TypeScriptField> fields) {
        this.fields.clear();
        this.fields.addAll(fields);
        return this;
    }

    /**
     * Return the objectType.
     */
    public ObjectType getObjectType() {
        return objectType;
    }

    /**
     * Set the given object type and return this instance for chaining purposes.
     */
    public TypeScriptModel withObjectType(
            ObjectType objectType) {
        this.objectType = objectType;
        return this;
    }

    /**
     * An enum to indicate the type of expected TypeScript object.
     */
    public enum ObjectType {
        CLASS,
        INTERFACE
    }

}
