package nl.mirila.moda.typescript.generator;

import nl.mirila.moda.typescript.generator.converters.ModelConverter;
import nl.mirila.moda.typescript.generator.models.TypeScriptModel;
import nl.mirila.moda.typescript.generator.writer.TypeScriptModelsWriter;
import nl.mirila.model.reader.ModelFileReader;
import nl.mirila.model.reader.YamlFilesManager;
import nl.mirila.model.reader.models.ModelDefinition;
import nl.mirila.model.reader.utils.ModelExpander;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Generate TypeScript models based on Yaml files that contain model definitions.
 */
public class ModelTypeScriptGenerator {

    private String srcPath;
    private String destPath;
    private String projectName;

    /**
     * Load model definitions in Yaml files from the given source path, and generate pojos into the destination folder,
     * using the given package name.
     *  @param srcPath The root folder that contains the Yaml files.
     * @param destPath The destination folder that will contain the SQL file.
     * @param projectName The name of the project.
     */
    public void generate(String srcPath, String destPath, String projectName) {
        this.srcPath = srcPath;
        this.destPath = destPath;
        this.projectName = projectName;
        Map<String, ModelDefinition> models = loadModelDefinitions(srcPath);
        Map<String, ModelDefinition> expandedModels = ModelExpander.getInstance().getExpandedModels(models);
        writeTypeScriptFiles(expandedModels, destPath);
    }

    /**
     * Load model definitions in Yaml files from the given source path.
     * <p>
     * @param srcPath The source path.
     * @return A map of model definitions.
     */
    protected Map<String, ModelDefinition> loadModelDefinitions(String srcPath) {
        YamlFilesManager fileManager = new YamlFilesManager(srcPath);
        ModelFileReader reader = new ModelFileReader();

        return fileManager.getSourceFilePaths()
                .stream()
                .map(reader::readModelDefinition)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toMap(ModelDefinition::getSingularName, Function.identity()));
    }

    /**
     * Write the given models as SQL files to the destination path.
     * <p>
     * @param expandedModels The expanded models.
     * @param destPath The destination path.
     */
    private void writeTypeScriptFiles(Map<String, ModelDefinition> expandedModels, String destPath) {
        ModelConverter converter = new ModelConverter();
        TypeScriptModelsWriter writer = new TypeScriptModelsWriter(destPath, projectName);

        List<TypeScriptModel> typeScriptModels = converter.convertModels(expandedModels.values());
        writer.writeToTypeScriptFile(typeScriptModels);
    }

}
