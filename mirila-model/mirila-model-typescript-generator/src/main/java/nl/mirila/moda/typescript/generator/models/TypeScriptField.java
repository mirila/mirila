package nl.mirila.moda.typescript.generator.models;

import static nl.mirila.core.utils.CaseConverter.KEBAB_CASE;

public class TypeScriptField {

    private String name;
    private String dataType;
    private boolean isRequired;

    private TypeScriptField(String name) {
        this.name = KEBAB_CASE.toCamelCase(name);
        isRequired = false;
    }

    /**
     * Return the name.
     */
    public String getName() {
        return name;
    }

    /**
     * Set the given name and return this instance for chaining purposes.
     */
    public static TypeScriptField withName(String name) {
        return new TypeScriptField(name);
    }

    /**
     * Return the data type.
     */
    public String getDataType() {
        return dataType;
    }

    /**
     * Set the given data type and return this instance for chaining purposes.
     */
    public TypeScriptField withDataType(String dataType) {
        this.dataType = dataType;
        return this;
    }

    /**
     * Return true if this field is required.
     */
    public boolean isRequired() {
        return isRequired;
    }

    /**
     * Set true if the field is required and return this instance for chaining purposes.
     */
    public TypeScriptField withRequired(boolean required) {
        isRequired = required;
        return this;
    }

}
