package nl.mirila.moda.typescript.generator.converters;

import nl.mirila.moda.typescript.generator.models.TypeScriptModel;
import nl.mirila.moda.typescript.generator.models.TypeScriptModel.ObjectType;
import nl.mirila.model.reader.models.ModelDefinition;
import org.apache.commons.logging.impl.SimpleLog;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

/**
 * Converter that converts a {@link ModelDefinition} into a {@link TypeScriptModel}.
 */
public class ModelConverter {

    private static final SimpleLog logger = new SimpleLog(ModelConverter.class.getSimpleName());

    private final FieldConverter fieldConverter;

    /**
     * Initialize a new instance.
     */
    public ModelConverter() {
        fieldConverter = new FieldConverter();
    }

    /**
     * Convert the given list of {@link ModelDefinition} into a list of {@link TypeScriptModel}.
     * <p>
     * @param definitions The model definitions.
     * @return The SQL tables models.
     */
    public List<TypeScriptModel> convertModels(Collection<ModelDefinition> definitions) {
        List<String> modelNames = definitions.stream()
                .map(ModelDefinition::getSingularName)
                .toList();

        return definitions.stream()
                .filter((definition) -> !definition.isAbstract())
                .map((definition) -> this.convertModel(definition, modelNames))
                .sorted(Comparator.comparing(TypeScriptModel::getName))
                .toList();
    }

    /**
     * Convert the given {@link ModelDefinition} into a {@link TypeScriptModel}.
     * <p>
     * @param definition The model definition.
     * @param allowedModels The list of allowed models.
     * @return The TypeScript model.
     */
    public TypeScriptModel convertModel(ModelDefinition definition, List<String> allowedModels) {
        String modelName = definition.getSingularName();
        logger.info("Generating TypeScript object for model " + modelName);

        return TypeScriptModel.forName(modelName)
                .withFields(fieldConverter.convertFields(definition.getFields(), allowedModels))
                .withObjectType(getObjectType(definition));
    }

    /**
     * Return the {@link TypeScriptModel.ObjectType} for the given {@link ModelDefinition}.
     * <p>
     * @param definition The model definition.
     * @return The object type.
     */
    private ObjectType getObjectType(ModelDefinition definition) {
        return (definition.isAbstract()) ? ObjectType.INTERFACE : ObjectType.CLASS;
    }

}
