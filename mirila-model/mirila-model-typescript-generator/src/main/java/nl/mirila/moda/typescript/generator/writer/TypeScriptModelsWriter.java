package nl.mirila.moda.typescript.generator.writer;

import com.github.jknack.handlebars.Handlebars;
import com.github.jknack.handlebars.Template;
import com.github.jknack.handlebars.io.ClassPathTemplateLoader;
import com.github.jknack.handlebars.io.TemplateLoader;
import nl.mirila.core.exceptions.MirilaException;
import nl.mirila.moda.typescript.generator.models.TypeScriptModel;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.impl.SimpleLog;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * Writer that stores multiple {@link TypeScriptModel} as SQL to file.
 */
public class TypeScriptModelsWriter {

    private static final SimpleLog logger = new SimpleLog(TypeScriptModelsWriter.class.getSimpleName());

    private final String destPath;
    private final String projectName;

    /**
     * Initialize a new instance.
     */
    public TypeScriptModelsWriter(String destPath, String projectName) {
        this.projectName = projectName;
        if (destPath.endsWith("/")) {
            destPath += "models.ts";
        } else if (!destPath.endsWith(".ts")) {
            destPath += ".ts";
        }
        this.destPath = destPath;
    }

    /**
     * Write the given list of {@link TypeScriptModel} to file.
     */
    public void writeToTypeScriptFile(List<TypeScriptModel> typeScriptModels) {
        TemplateLoader loader = new ClassPathTemplateLoader();
        Handlebars handlebars = new Handlebars(loader);
        try {
            Template template = handlebars.compile("interfaces");
            Map<String, Object> map = new HashMap<>();
            map.put("projectName", projectName);
            map.put("tab", "    ");
            map.put("models", typeScriptModels);
            String typeScriptCode = template.apply(map);
            File file = new File(destPath);
            FileUtils.writeStringToFile(file, typeScriptCode, UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
            throw new MirilaException(e.getMessage(), e);
        }
    }

}
