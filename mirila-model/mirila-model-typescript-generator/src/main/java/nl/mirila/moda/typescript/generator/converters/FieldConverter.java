package nl.mirila.moda.typescript.generator.converters;

import nl.mirila.moda.typescript.generator.models.TypeScriptField;
import nl.mirila.model.reader.exceptions.UnknownTypeException;
import nl.mirila.model.reader.models.FieldDefinition;
import org.apache.commons.lang3.NotImplementedException;

import java.util.List;

import static org.apache.commons.lang3.StringUtils.isBlank;

/**
 * Converter that converts a {@link FieldDefinition} into a {@link TypeScriptField}.
 */
public class FieldConverter {

    /**
     * Convert the given list of {@link FieldDefinition} into a list of {@link TypeScriptField}.
     * <p>
     * @param definitions The list of field definitions.
     * @param allowedModels The list of allowed models.
     * @return The list of sql columns.
     */
    public List<TypeScriptField> convertFields(List<FieldDefinition> definitions, List<String> allowedModels) {
        return definitions.stream()
                .filter((definition) -> !definition.isServerOnly())
                .map((definition) -> convertField(definition, allowedModels))
                .toList();
    }

    /**
     * Convert the given {@link FieldDefinition} into a {@link TypeScriptField}.
     * <p>
     * @param definition The field definitions.
     * @param allowedModels The list of allowed models.
     * @return The sql column.
     */
    public TypeScriptField convertField(FieldDefinition definition, List<String> allowedModels) {
        return TypeScriptField.withName(definition.getName())
                .withDataType(getDataType(definition, allowedModels))
                .withRequired(definition.isRequired());
    }

    /**
     * Convert a field type in the given {@link FieldDefinition} into a SQL data type.
     * <p>
     * @param definition The definition of the field.
     * @param allowedModels The list of allowed models.
     * @return The SDL data type.
     */
    protected String getDataType(FieldDefinition definition, List<String> allowedModels) {
        return getDataType(definition.getType(), allowedModels);
    }

    /**
     * Convert a field type in the given string into a SQL data type of the given {@link FieldDefinition}.
     * <p>
     * The given field may not be the same as {@link FieldDefinition#getType()}, but may also be a subtype,
     * for example, type could be 'string', while the actyal type is 'list of string'.
     * <p>
     * @param type The (specific) type of the field.
     * @param allowedModels The list of allowed models.
     * @return The SQL data type.
     */
    protected String getDataType(String type, List<String> allowedModels) {
        if (isBlank(type)) {
            return "any";
        }
        type = type.trim();
        if (allowedModels.contains(type)) {
            return type;
        }

        if (type.contains(" ")) {
            String[] parts = type.split("\\s+(of|on|to)\\s+", 2);
            String containerType = parts[0];
            String specificType = parts[1];

            String specificDataType = getDataType(specificType, allowedModels);

            return switch (containerType) {
                case "list" -> specificDataType + "[]";
                case "map" -> String.format("Record<string, %s>", specificDataType);
                case "reference" -> specificDataType;
                default -> throw new UnknownTypeException(type);
            };
        }

        return switch (type.toLowerCase()) {
            case "string", "text", "datetime", "time", "date" -> "string";
            case "id", "integer", "long", "float", "timestamp" -> "number";
            case "boolean" -> "boolean";
            case "json", "map" -> "Record<string, any>";
            case "list" -> "[]";
            case "reference" ->
                    // TODO: Add support for reference.
                    throw new NotImplementedException("Reference is not yet implemented.");
            default -> throw new UnknownTypeException(type);
        };
    }
}
