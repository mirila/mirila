package nl.mirila.moda.typescript.generator;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import static org.apache.commons.lang3.StringUtils.isBlank;

/**
 * The mojo for the Model TypeScript generator.
 */
@Mojo(name = "mirila-model-typescript-generator", defaultPhase = LifecyclePhase.COMPILE, threadSafe = true)
public class ModelTypeScriptGeneratorMojo extends AbstractMojo {

    @Parameter(property = "sources.path")
    String srcPath;

    @Parameter(property = "destination.path")
    String destPath;

    @Parameter(property = "project.name")
    String projectName;

    /**
     * {@inheritDoc}
     */
    @Override
    public void execute() throws MojoExecutionException {
        if (isBlank(srcPath)) {
            String msg = "The sources.path configuration is missing.";
            getLog().error(msg);
            throw new MojoExecutionException(msg);
        }
        if (isBlank(destPath)) {
            String msg = "The destination.path configuration is missing.";
            getLog().error(msg);
            throw new MojoExecutionException(msg);
        }
        if (isBlank(projectName)) {
            String msg = "The project name configuration is missing.";
            getLog().error(msg);
            throw new MojoExecutionException(msg);
        }
        ModelTypeScriptGenerator generator = new ModelTypeScriptGenerator();
        generator.generate(srcPath, destPath, projectName);
    }

}
