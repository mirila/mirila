package nl.mirila.model.management.managers;

import nl.mirila.model.core.references.Key;
import nl.mirila.model.core.references.Model;
import nl.mirila.model.management.parameters.ActionParameters;
import nl.mirila.model.management.query.QueryParameters;
import nl.mirila.model.management.results.Result;
import nl.mirila.model.management.results.Results;

/**
 * The EntityPersistenceManager manages entities of all kind of types and persists the data. Usually a database is used.
 * <p>
 * While the {@link EntityManager} is one manager per model, the EntityPersistenceManager is one class for all types of
 * entities.
 */
public interface EntityPersistenceManager {

    /**
     * Return true if this EntityPersistenceManager still requires a rights check on the results of a query.
     * <p>
     * Preferably, filtering on rights is done in the data storage, like the database. However, we may not assume that
     * this is working correctly. Therefore, we also have the ability to check the rights in the {@link EntityManager}.
     * <p>
     * EntityPersistenceManagers that already check the rights correctly, may override this method and set this to
     * false.
     */
    default boolean requiresRightsCheckAfterQuery() {
        return true;
    }

    /**
     * Count the entities of type entity {@link E} using the default entities provider.
     * <p>
     * @param queryParameters The query to process.
     * @param <E> The type of the entity.
     * @return The list.
     */
    <E extends Model> long count(QueryParameters<E> queryParameters);

    /**
     * Query for specific entities of type entity {@link E} using the default entities provider,
     * and return it wrapped in a {@link Results}.
     * <p>
     * @param queryParameters The query to process.
     * @param <E> The type of the entity.
     * @return The list.
     */
    <E extends Model> Results<E> query(QueryParameters<E> queryParameters);

    /**
     * Return the entity with the given primary key.
     * <p>
     * @param key The key of the expected entity.
     * @param <E> The type of the entity.
     * @return The entity, wrapped in a {@link Result}.
     */
    default <E extends Model> Result<E> read(Key<E> key) {
        return read(key, ActionParameters.on(key.getModelClass()));
    }
    /**
     * Return the entity with the given primary key.
     * <p>
     * @param key The key of the expected entity.
     * @param parameters The run parameters to use.
     * @param <E> The type of the entity.
     * @return The entity, wrapped in a {@link Result}.
     */
    <E extends Model> Result<E> read(Key<E> key, ActionParameters<E> parameters);

    /**
     * Create the given entity {@link E}.
     * <p>
     * @param entity The entity.
     * @param <E> The type of the entity.
     * @return The created entity.
     */
    @SuppressWarnings("unchecked")
    default <E extends Model> Result<E> create(E entity) {
        return create(entity, ActionParameters.on((Class<E>) entity.getClass()));
    }

    /**
     * Create the given entity {@link E}.
     * <p>
     * @param entity The entity.
     * @param parameters The run parameters to use.
     * @param <E> The type of the entity.
     * @return The created entity.
     */
    <E extends Model> Result<E> create(E entity, ActionParameters<E> parameters);

    /**
     * Updates the given entity {@link E}, with the given primary key.
     * <p>
     * @param entity The entity.
     * @param <E> The type of the entity.
     * @return The created entity.
     */
    @SuppressWarnings("unchecked")
    default <E extends Model> Result<E> update(E entity) {
        return update(entity, ActionParameters.on((Class<E>) entity.getClass()));
    }

    /**
     * Updates the given entity {@link E}, with the given primary key.
     * <p>
     * @param entity The entity.
     * @param parameters The run parameters to use.
     * @param <E> The type of the entity.
     * @return The created entity.
     */
    <E extends Model> Result<E> update(E entity, ActionParameters<E> parameters);

    /**
     * Delete the entity {@link E} with the given primary key.
     * <p>
     * @param key The key of the entity.
     * @param <E> The type of the entity.
     */
    default <E extends Model> void delete(Key<E> key) {
        delete(key, ActionParameters.on(key.getModelClass()));
    }

    /**
     * Delete the entity {@link E} with the given primary key.
     * <p>
     * @param key The key of the entity.
     * @param parameters The run parameters to use.
     * @param <E> The type of the entity.
     */
    <E extends Model> void delete(Key<E> key, ActionParameters<E> parameters);

    /**
     * Trigger to close the connection, if applicable.
     */
    default void close() {};

}
