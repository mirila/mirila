package nl.mirila.model.management.managers;

import com.google.inject.Inject;
import nl.mirila.model.core.references.Key;
import nl.mirila.model.core.references.Model;
import nl.mirila.model.management.hooks.HooksManager;
import nl.mirila.model.management.metrics.ModelManagementMetricsService;
import nl.mirila.model.management.parameters.ActionParameters;
import nl.mirila.model.management.parameters.CommonFlags;
import nl.mirila.model.management.query.QueryParameters;
import nl.mirila.model.management.results.Result;
import nl.mirila.model.management.results.Results;

/**
 * The EntityManager is wrapper, connecting abstract CRUD operations on entities of a model, to an the CRUD
 * implementations of an {@link EntityPersistenceManager}.
 */
public class EntityManager<E extends Model> {

    private final EntityPersistenceManager persistenceManager;
    private final HooksManager hooksManager;
    private final ModelManagementMetricsService metricsService;

    /**
     * Initialize a new instance.
     */
    @Inject
    public EntityManager(EntityPersistenceManager persistenceManager,
                         HooksManager hooksManager,
                         ModelManagementMetricsService metricsService) {
        this.persistenceManager = persistenceManager;
        this.hooksManager = hooksManager;
        this.metricsService = metricsService;
    }

    /**
     * Convenient method that calls the runnable if the hooks are enabled.
     */
    private void ifHooksEnabled(ActionParameters<E> parameters, Runnable runnable) {
        if (!parameters.hasFlag(CommonFlags.SKIP_HOOKS)) {
            runnable.run();
        }
    }

    /**
     * Count entities of type entity {@link E} using the default entities provider,
     * and return it wrapped in a {@link Results}.
     * <p>
     * @return The list.
     */
    public long count(QueryParameters<E> queryParameters) {
        metricsService.increaseCountAttempts();
        try {
            ifHooksEnabled(queryParameters, () -> hooksManager.beforeCount(queryParameters));
            long count = persistenceManager.count(queryParameters);
            ifHooksEnabled(queryParameters, () -> hooksManager.afterCount(count, queryParameters));
            metricsService.increaseSuccessfulCounts();
            return count;
        } catch (Exception e) {
            metricsService.increaseFailedCounts(e);
            throw e;
        }
    }

    /**
     * Query for specific entities of type entity {@link E} using the default entities provider,
     * and return it wrapped in a {@link Results}.
     * <p>
     * @return The list.
     */
    public Results<E> query(QueryParameters<E> queryParameters) {
        metricsService.increaseQueryAttempts();
        try {
            ifHooksEnabled(queryParameters, () -> hooksManager.beforeQuery(queryParameters));

            // Use the optional custom entities provider, or use the persistence manager.
            Results<E> results = queryParameters.getEntitiesProvider()
                    .map((customProvider) -> customProvider.provide(queryParameters))
                    .orElseGet(() -> persistenceManager.query(queryParameters));

            ifHooksEnabled(queryParameters, () -> hooksManager.afterQuery(results, queryParameters));
            metricsService.increaseSuccessfulQueries();
            return results;
        } catch (Exception e) {
            metricsService.increaseFailedQueries(e);
            throw e;
        }
    }

    /**
     * Return the entity with the given primary key.
     * <p>
     * @param key The key of the expected entity.
     * @return The entity, wrapped in a {@link Result}.
     */
    public Result<E> read(Key<E> key) {
        return read(key, ActionParameters.on(key.getModelClass()));
    }

    /**
     * Return the entity with the given primary key.
     * <p>
     * @param key The key of the expected entity.
     * @param parameters The run parameters to use.
     * @return The entity, wrapped in a {@link Result}.
     */
    public Result<E> read(Key<E> key, ActionParameters<E> parameters) {
        metricsService.increaseReadAttempts();
        try {
            ifHooksEnabled(parameters, () -> hooksManager.beforeRead(key, parameters));
            Result<E> result = persistenceManager.read(key, parameters);
            ifHooksEnabled(parameters, () -> hooksManager.afterRead(key, result, parameters));
            metricsService.increaseSuccessfulReads();
            return result;
        } catch (Exception e) {
            metricsService.increaseFailedReads(e);
            throw e;
        }
    }

    /**
     * Create the given entity {@link E}.
     * <p>
     * @param entity The entity.
     * @return The created entity.
     */
    @SuppressWarnings("unchecked")
    public Result<E> create(E entity) {
        return create(entity, ActionParameters.on((Class<E>) entity.getClass()));
    }

    /**
     * Create the given entity {@link E}.
     * <p>
     * @param entity The entity.
     * @param parameters The run parameters to use.
     * @return The created entity.
     */
    public Result<E> create(E entity, ActionParameters<E> parameters) {
        metricsService.increaseCreateAttempts();
        try {
            ifHooksEnabled(parameters, () -> hooksManager.beforeCreate(entity, parameters));
            Result<E> result = persistenceManager.create(entity, parameters);
            ifHooksEnabled(parameters, () -> hooksManager.afterCreate(result, parameters));
            metricsService.increaseSuccessfulCreates();
            return result;
        } catch (Exception e) {
            metricsService.increaseFailedCreates(e);
            throw e;
        }
    }

    /**
     * Updates the given entity {@link E}, with the given primary key.
     * <p>
     * @param entity The entity.
     * @return The created entity.
     */
    @SuppressWarnings("unchecked")
    public Result<E> update(E entity) {
        return update(entity, ActionParameters.on((Class<E>) entity.getClass()));
    }

    /**
     * Updates the given entity {@link E}, with the given primary key.
     * <p>
     * @param entity The entity.
     * @param parameters The run parameters to use.
     * @return The created entity.
     */
    public Result<E> update(E entity, ActionParameters<E> parameters) {
        metricsService.increaseUpdateAttempts();
        try {
            ifHooksEnabled(parameters, () -> hooksManager.beforeUpdate(entity, parameters));
            Result<E> result = persistenceManager.update(entity, parameters);
            ifHooksEnabled(parameters, () -> hooksManager.afterUpdate(result, parameters));
            metricsService.increaseSuccessfulUpdates();
            return result;
        } catch (Exception e) {
            metricsService.increaseFailedUpdates(e);
            throw e;
        }
    }

    /**
     * Delete the entity {@link E} with the given primary key.
     * <p>
     * @param key The key of the entity.
     */
    public void delete(Key<E> key) {
        delete(key, ActionParameters.on(key.getModelClass()));
    }

    /**
     * Delete the entity {@link E} with the given primary key.
     * <p>
     * @param key The key of the entity.
     * @param parameters The run parameters to use.
     */
    public void delete(Key<E> key, ActionParameters<E> parameters) {
        metricsService.increaseDeleteAttempts();
        try {
            ifHooksEnabled(parameters, () -> hooksManager.beforeDelete(key, parameters));
            persistenceManager.delete(key, parameters);
            ifHooksEnabled(parameters, () -> hooksManager.afterDelete(key, parameters));
            metricsService.increaseSuccessfulDeletes();
        } catch (Exception e) {
            metricsService.increaseFailedDeletes(e);
            throw e;
        }
    }

}
