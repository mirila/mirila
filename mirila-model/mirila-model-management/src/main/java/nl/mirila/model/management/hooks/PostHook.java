package nl.mirila.model.management.hooks;

import nl.mirila.model.core.references.Key;
import nl.mirila.model.core.references.Model;
import nl.mirila.model.management.parameters.ActionParameters;
import nl.mirila.model.management.query.QueryParameters;
import nl.mirila.model.management.results.Result;
import nl.mirila.model.management.results.Results;
import org.jboss.resteasy.plugins.guice.RequestScoped;

/**
 * Classes that implement this PostHook interface may override the afterXxx hooks.
 * <p>
 * Because the {@link HooksManager} uses the {@link RequestScoped}, all hooks registered in that manager, will have the
 * same scope.
 * <p>
 * All methods in this interface, are declared with an empty default body. This allows implementations to only implement
 * the methods that are needed.
 */
public interface PostHook {

    /**
     * Return an integer indicating its priority, where 0 is normal priority. Lower than 0 is high priority, and
     * higher than 0 is a low priority. Default is a normal priority.
     */
    default int getPostHookPriority() {
        return 0;
    }

    /**
     * Hook after a count for is executed.
     * <p>
     * @param count The count.
     * @param queryParameters The query that is processed.
     * @param <E> The type of the entity.
     */
    default <E extends Model> void afterCount(long count, QueryParameters<E> queryParameters) {
        //no-op
    }

    /**
     * Hook after a query for is executed, and resulted in the given {@link Results}.
     * <p>
     * @param queryParameters The parameters to use in the query.
     * @param results The entities, wrapped in a {@link Results}.
     * @param <E> The type of the entity.
     */
    default <E extends Model> void afterQuery(Results<E> results, QueryParameters<E> queryParameters) {
        //no-op
    }

    /**
     * Hook after the entity is fetched for reading.
     * <p>
     * @param key The key of the requested entity.
     * @param result The entity, wrapped in a {@link Result}.
     * @param parameters The parameters to use the entity was read.
     * @param <E> The type of the entity.
     */
    default <E extends Model> void afterRead(Key<E> key, Result<E> result, ActionParameters<E> parameters) {
        //no-op
    }

    /**
     * Hook after the given entity is created.
     * <p>
     * @param result The created entity, wrapped in a {@link Result}.
     * @param parameters The parameters to use the entity was created.
     * @param <E> The type of the entity.
     */
    default <E extends Model> void afterCreate(Result<E> result, ActionParameters<E> parameters) {
        //no-op
    }

    /**
     * Hook after the given entity is updated.
     * <p>
     * @param result The updated entity, wrapped in a {@link Result}.
     * @param parameters The parameters to use the entity was created.
     * @param <E> The type of the entity.
     */
    default <E extends Model> void afterUpdate(Result<E> result, ActionParameters<E> parameters) {
        //no-op
    }
    /**
     * Hook after the entity with the given primary key is deleted.
     * <p>
     * @param key The key of the entity.
     * @param parameters The parameters to use the entity was deleted.
     * @param <E> The type of the entity.
     */
    default  <E extends Model> void afterDelete(Key<E> key, ActionParameters<E> parameters) {
        //no-op
    }

}
