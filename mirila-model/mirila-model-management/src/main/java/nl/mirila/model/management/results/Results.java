package nl.mirila.model.management.results;

import nl.mirila.model.core.references.Model;

import java.util.*;

/**
 * A wrapper for the results of a specific model. As this wrapper implements an {@link AbstractCollection} standard
 * collection methods can be used.
 */
public class Results<E extends Model> extends AbstractCollection<E> {

    private final List<E> entities;

    /**
     * Initialize a new instance with the given entities.
     */
    private Results(List<E> entities) {
        Objects.requireNonNull(entities);
        this.entities = entities;
    }

    /**
     * Return the list of entities.
     */
    public List<E> get() {
        return entities;
    }

    /**
     * Create a new results wrapper with the given entities.
     * <p>
     * @param entities The entities.
     * @param <E> The type of the entities.
     * @return The results wrapper.
     */
    public static <E extends Model> Results<E> of(List<E> entities) {
        ArrayList<E> mutableList = new ArrayList<>(entities);
        return new Results<>(mutableList);
    }

    /**
     * Create an empty results wrapper.
     * <p>
     * @param <E> The type of the entities, if there were results.
     * @return The results wrapper, without entities.
     */
    public static <E extends Model> Results<E> empty() {
        return new Results<>(new ArrayList<>());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Iterator<E> iterator() {
        return entities.iterator();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int size() {
        return entities.size();
    }

}
