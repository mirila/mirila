package nl.mirila.model.management.parameters;

import nl.mirila.model.core.references.Model;

import java.util.*;

/**
 * The ActionParameters contains a set of {@link Option} and {@link Flag} for a specific model.
 * <p>
 * @param <E> The type of the model.
 */
public class ActionParameters<E extends Model> {

    private final Class<E> modelClass;
    private final Set<Option> options;
    private final Set<Flag> flags;

    /**
     * Initialize a new instance.
     */
    protected ActionParameters(Class<E> modelClass) {
        this.modelClass = modelClass;
        options = new HashSet<>();
        flags = new HashSet<>();
    }


    /**
     * Return a new instance for the given model.
     * <p>
     * @param modelClass The class of the model.
     * @param <T> The type of the model.
     * @return The ActionParameters.
     */
    public static <T extends Model> ActionParameters<T> on(Class<T> modelClass) {
        return new ActionParameters<>(modelClass);
    }

    /**
     * Return the class of the model.
     */
    public Class<E> getModelClass() {
        return modelClass;
    }

    /**
     * Return the options.
     */
    public Set<Option> getOptions() {
        return options;
    }

    /**
     * Return the {@link Option} of the given class, if available.
     * <p>
     * @param optionClass The class of the option.
     * @param <O> The type of the option.
     * @return The requested {@link Option}, wrapped in an {@link Optional}.
     */
    public <O extends Option> Optional<O> getOption(Class<O> optionClass) {
        return options.stream()
                .filter(optionClass::isInstance)
                .map(optionClass::cast)
                .findAny();
    }

    /**
     * Return the flags.
     */
    public Set<Flag> getFlags() {
        return flags;
    }

    /**
     * Return true if the flags contain the given {@link Flag}.
     */
    public boolean hasFlag(Flag flag) {
        return flags.contains(flag);
    }

    /**
     * Set the given {@link Option} and return this instance for chaining purposes.
     * <p>
     * @param option The option
     * @return This instance.
     */
    public ActionParameters<E> withOption(Option option) {
        options.add(option);
        return this;
    }

    /**
     * Set the given list of {@link Option} and return this instance for chaining purposes.
     * <p>
     * @param options The list of options.
     * @return This instance.
     */
    public ActionParameters<E> withOptions(List<Option> options) {
        this.options.addAll(options);
        return this;
    }

    /**
     * Set the given {@link Flag} and return this instance for chaining purposes.
     * <p>
     * @param flag The flag.
     * @return This instance.
     */
    public ActionParameters<E> withFlag(Flag flag) {
        flags.add(flag);
        return this;
    }

    /**
     * Set the given array of flag {@link Flag} and return this instance for chaining purposes.
     * <p>
     * @param flags The flags.
     * @return This instance.
     */
    public ActionParameters<E> withFlags(Flag... flags) {
        return withFlags(Arrays.asList(flags));
    }

    /**
     * Set the given list of {@link Flag} and return this instance for chaining purposes.
     * <p>
     * @param flags The flags.
     * @return This instance.
     */
    public ActionParameters<E> withFlags(List<Flag> flags) {
        this.flags.addAll(flags);
        return this;
    }

}
