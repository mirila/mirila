package nl.mirila.model.management.hooks;

import nl.mirila.model.core.references.Key;
import nl.mirila.model.core.references.Model;
import nl.mirila.model.management.parameters.ActionParameters;
import nl.mirila.model.management.query.QueryParameters;
import nl.mirila.model.management.results.Result;
import nl.mirila.model.management.results.Results;
import org.jboss.resteasy.plugins.guice.RequestScoped;

/**
 * Classes that implement this PreHook interface may override the beforeXxx hooks.
 * <p>
 * Because the {@link HooksManager} uses the {@link RequestScoped}, all hooks registered in that manager, will have the
 * same scope.
 * <p>
 * All methods in this interface, are declared with an empty default body. This allows implementations to only implement
 * the methods that are needed.
 */
public interface PreHook {

    /**
     * Return an integer indicating its priority, where 0 is normal priority. Lower than 0 is high priority, and
     * higher than 0 is a low priority. Default is a normal priority.
     */
    default int getPreHookPriority() {
        return 0;
    }

    /**
     * Hook before a count is executed, based on the given query parameters.
     * <p>
     * @param queryParameters The query that is processed.
     * @param <E> The type of the entity.
     */
    default <E extends Model> void beforeCount(QueryParameters<E> queryParameters) {
        //no-op
    }

    /**
     * Hook before a query is executed, and resulted in the given {@link Results}.
     * <p>
     * @param queryParameters The query that is processed.
     * @param <E> The type of the entity.
     */
    default <E extends Model> void beforeQuery(QueryParameters<E> queryParameters) {
        //no-op
    }

    /**
     * Hook before an entity with the given key will be fetched for reading.
     * <p>
     * @param key The {@link Key} of the entity.
     * @param <E> The type of the entity.
     */
    default <E extends Model> void beforeRead(Key<E> key, ActionParameters<E> parameters) {
        //no-op
    }

    /**
     * Hook before the given entity will be created.
     * <p>
     * @param entity The entity to create.
     * @param <E> The type of the entity.
     */
    default <E extends Model> void beforeCreate(E entity, ActionParameters<E> parameters) {
        //no-op
    }
    
    /**
     * Hook before the given entity will be updated.
     * <p>
     * @param entity The returned entity, wrapped in a {@link Result}.
     * @param <E> The type of the entity.
     */
    default <E extends Model> void beforeUpdate(E entity, ActionParameters<E> parameters) {
        //no-op
    }
    /**
     * Hook before the entity with the given primary key will be deleted.
     * <p>
     * @param key The key of the entity.
     * @param <E> The type of the entity.
     */
    default  <E extends Model> void beforeDelete(Key<E> key, ActionParameters<E> parameters) {
        //no-op
    }

}
