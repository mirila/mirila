package nl.mirila.model.management.exceptions;

import nl.mirila.core.exceptions.MirilaException;
import nl.mirila.model.core.references.Key;

import static javax.ws.rs.core.Response.Status.CONFLICT;

public class EntityAlreadyExistsException extends MirilaException {

    public EntityAlreadyExistsException(Key<?> key) {
        super(String.format("Can't create this entity: an entity with key '%s' already exists.", key),
              CONFLICT.getStatusCode());
    }

}
