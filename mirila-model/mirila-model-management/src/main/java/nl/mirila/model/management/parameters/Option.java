package nl.mirila.model.management.parameters;

import nl.mirila.model.management.hooks.PostHook;
import nl.mirila.model.management.hooks.PreHook;
import nl.mirila.model.management.managers.EntitiesProvider;
import nl.mirila.model.management.managers.EntityManager;
import nl.mirila.model.management.managers.EntityPersistenceManager;

/**
 * An Option is a dataset that can be passed via an {@link ActionParameters}. This is mainly used to add additional
 * instructions within the {@link EntityManager}, {@link EntitiesProvider}, {@link PreHook}, {@link PostHook} and
 * {@link EntityPersistenceManager}.
 * <p>
 * The class that works with a specific option may throw an exception if the related option is not provided.
 */
public interface Option {

}
