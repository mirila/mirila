package nl.mirila.model.management.metrics;

import com.google.inject.Inject;
import nl.mirila.metrics.core.MetricProvider;

import javax.inject.Singleton;

@Singleton
public class ModelManagementMetricsService {

    private static final String METRICS_KEY_COUNT_ATTEMPTS = "entity-manager.counts.total";
    private static final String METRICS_KEY_SUCCESSFUL_COUNTS = "entity-manager.counts.success";
    private static final String METRICS_KEY_FAILED_COUNTS = "entity-manager.counts.failed";
    private static final String METRICS_KEY_FAILED_COUNT_PREFIX = "entity-manager.counts.error.";

    private static final String METRICS_KEY_QUERY_ATTEMPTS = "entity-manager.queries.total";
    private static final String METRICS_KEY_SUCCESSFUL_QUERIES = "entity-manager.queries.success";
    private static final String METRICS_KEY_FAILED_QUERIES = "entity-manager.queries.failed";
    private static final String METRICS_KEY_FAILED_QUERY_PREFIX = "entity-manager.queries.exceptions.";

    private static final String METRICS_KEY_READ_ATTEMPTS = "entity-manager.reads.total";
    private static final String METRICS_KEY_SUCCESSFUL_READS = "entity-manager.reads.success";
    private static final String METRICS_KEY_FAILED_READS = "entity-manager.reads.failed";
    private static final String METRICS_KEY_FAILED_READ_PREFIX = "entity-manager.reads.exceptions";

    private static final String METRICS_KEY_CREATE_ATTEMPTS = "entity-manager.create.total";
    private static final String METRICS_KEY_SUCCESSFUL_CREATES = "entity-manager.create.success";
    private static final String METRICS_KEY_FAILED_CREATES = "entity-manager.create.failed";
    private static final String METRICS_KEY_FAILED_CREATE_PREFIX = "entity-manager.create.exceptions";

    private static final String METRICS_KEY_UPDATE_ATTEMPTS = "entity-manager.updates.total";
    private static final String METRICS_KEY_SUCCESSFUL_UPDATES = "entity-manager.updates.success";
    private static final String METRICS_KEY_FAILED_UPDATES = "entity-manager.updates.failed";
    private static final String METRICS_KEY_FAILED_UPDATE_PREFIX = "entity-manager.updates.exceptions";

    private static final String METRICS_KEY_DELETE_ATTEMPTS = "entity-manager.deletes.total";
    private static final String METRICS_KEY_SUCCESSFUL_DELETES = "entity-manager.deletes.success";
    private static final String METRICS_KEY_FAILED_DELETES = "entity-manager.deletes.failed";
    private static final String METRICS_KEY_FAILED_DELETE_PREFIX = "entity-manager.deletes.exceptions";

    private final MetricProvider provider;

    /**
     * Initialize a new instance.
     */
    @Inject
    public ModelManagementMetricsService(MetricProvider provider) {
        this.provider = provider;
    }

    /**
     * Increase the metrics for count attempts.
     */
    public void increaseCountAttempts() {
        provider.getIncrementor(METRICS_KEY_COUNT_ATTEMPTS).increment();
    }

    /**
     * Increase the metrics for successful counts.
     */
    public void increaseSuccessfulCounts() {
        provider.getIncrementor(METRICS_KEY_SUCCESSFUL_COUNTS).increment();
    }

    /**
     * Increase the metrics for failed counts.
     */
    public void increaseFailedCounts(Exception e) {
        provider.getIncrementor(METRICS_KEY_FAILED_COUNTS).increment();
        provider.getIncrementor(METRICS_KEY_FAILED_COUNT_PREFIX + e.getClass().getSimpleName()).increment();
    }

    /**
     * Increase the metrics for query attempts.
     */
    public void increaseQueryAttempts() {
        provider.getIncrementor(METRICS_KEY_QUERY_ATTEMPTS).increment();
    }

    /**
     * Increase the metrics for successful queries.
     */
    public void increaseSuccessfulQueries() {
        provider.getIncrementor(METRICS_KEY_SUCCESSFUL_QUERIES).increment();
    }

    /**
     * Increase the metrics for failed queries.
     */
    public void increaseFailedQueries(Exception e) {
        provider.getIncrementor(METRICS_KEY_FAILED_QUERIES).increment();
        provider.getIncrementor(METRICS_KEY_FAILED_QUERY_PREFIX + e.getClass().getSimpleName()).increment();
    }

    /**
     * Increase the metrics for read attempts.
     */
    public void increaseReadAttempts() {
        provider.getIncrementor(METRICS_KEY_READ_ATTEMPTS).increment();
    }

    /**
     * Increase the metrics for successful reads.
     */
    public void increaseSuccessfulReads() {
        provider.getIncrementor(METRICS_KEY_SUCCESSFUL_READS).increment();
    }

    /**
     * Increase the metrics for failed reads.
     */
    public void increaseFailedReads(Exception e) {
        provider.getIncrementor(METRICS_KEY_FAILED_READS).increment();
        provider.getIncrementor(METRICS_KEY_FAILED_READ_PREFIX + e.getClass().getSimpleName()).increment();
    }

    /**
     * Increase the metrics for create attempts.
     */
    public void increaseCreateAttempts() {
        provider.getIncrementor(METRICS_KEY_CREATE_ATTEMPTS).increment();
    }

    /**
     * Increase the metrics for successful creates.
     */
    public void increaseSuccessfulCreates() {
        provider.getIncrementor(METRICS_KEY_SUCCESSFUL_CREATES).increment();
    }

    /**
     * Increase the metrics for failed creates.
     */
    public void increaseFailedCreates(Exception e) {
        provider.getIncrementor(METRICS_KEY_FAILED_CREATES).increment();
        provider.getIncrementor(METRICS_KEY_FAILED_CREATE_PREFIX + e.getClass().getSimpleName()).increment();
    }

    /**
     * Increase the metrics for update attempts.
     */
    public void increaseUpdateAttempts() {
        provider.getIncrementor(METRICS_KEY_UPDATE_ATTEMPTS).increment();
    }

    /**
     * Increase the metrics for successful updates.
     */
    public void increaseSuccessfulUpdates() {
        provider.getIncrementor(METRICS_KEY_SUCCESSFUL_UPDATES).increment();
    }

    /**
     * Increase the metrics for failed updates.
     */
    public void increaseFailedUpdates(Exception e) {
        provider.getIncrementor(METRICS_KEY_FAILED_UPDATES).increment();
        provider.getIncrementor(METRICS_KEY_FAILED_UPDATE_PREFIX + e.getClass().getSimpleName()).increment();
    }

    /**
     * Increase the metrics for delete attempts.
     */
    public void increaseDeleteAttempts() {
        provider.getIncrementor(METRICS_KEY_DELETE_ATTEMPTS).increment();
    }

    /**
     * Increase the metrics for successful deletes.
     */
    public void increaseSuccessfulDeletes() {
        provider.getIncrementor(METRICS_KEY_SUCCESSFUL_DELETES).increment();
    }

    /**
     * Increase the metrics for failed deletes.
     */
    public void increaseFailedDeletes(Exception e) {
        provider.getIncrementor(METRICS_KEY_FAILED_DELETES).increment();
        provider.getIncrementor(METRICS_KEY_FAILED_DELETE_PREFIX + e.getClass().getSimpleName()).increment();
    }

}
