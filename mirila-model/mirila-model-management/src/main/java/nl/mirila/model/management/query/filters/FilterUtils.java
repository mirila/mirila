package nl.mirila.model.management.query.filters;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static nl.mirila.model.management.enums.RelationalOperator.EQ;
import static nl.mirila.model.management.query.filters.FilterList.MatchCondition.ONE;

public class FilterUtils {

    /**
     * Return a {@link FilterList} containing a {@link FieldComparisonFilter} for each value in the comma separated
     * values string. If there are no non-empty values, return an empty {@link Optional}.
     * <p>
     * The filter list is configured to match one of the values.
     *
     * @param fieldName The field name.
     * @param values The comma separated list of values.
     * @return The filter list.
     */
    public static Optional<FilterList> getFilterForAnyOfValues(String fieldName, String values) {
        return getFilterForAnyOfValues(fieldName, values, ",");
    }

    /**
     * Return a {@link FilterList} containing a {@link FieldComparisonFilter} for each value in the separated
     * values string. If there are no non-empty values, return an empty {@link Optional}.
     * <p>
     * The filter list is configured to match one of the values.
     *
     * @param fieldName The field name.
     * @param values The string of values separated by the given separator.
     * @param separator The separator to use to split the values.
     * @return The filter list.
     */
    public static Optional<FilterList> getFilterForAnyOfValues(String fieldName, String values, String separator) {
        return (values != null)
                ? getFilterForAnyOfValues(fieldName, values.split(separator))
                : Optional.empty();
    }

    /**
     * Return a {@link FilterList} containing a {@link FieldComparisonFilter} for each value in the values array.
     * If there are no non-empty values, return an empty {@link Optional}.
     * <p>
     * The filter list is configured to match one of the values.
     *
     * @param fieldName The field name.
     * @param values The array of values.
     * @return The filter list.
     */
    public static Optional<FilterList> getFilterForAnyOfValues(String fieldName, String... values) {
        List<Filter> filters = Arrays.stream(values)
                .filter(StringUtils::isNotEmpty)
                .map(value -> new FieldComparisonFilter(fieldName, EQ, value))
                .map(Filter.class::cast)
                .toList();
        return (filters.isEmpty())
                ? Optional.empty()
                : Optional.of(FilterList.of(filters).matching(ONE));
    }

}
