package nl.mirila.model.management.query.filters;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * The FilterList combines multiple filters into a single {@link Filter}.
 */
public class FilterList implements Filter {

    private final List<Filter> filters;
    private MatchCondition matchCondition;

    /**
     * Initialize a new instance.
     */
    private FilterList(List<Filter> filters, MatchCondition matchCondition) {
        this.filters = filters;
        this.matchCondition = matchCondition;
    }

    /**
     * Return the filters.
     */
    public List<Filter> getFilters() {
        return filters;
    }

    /**
     * Add the given {@link Filter}.
     */
    public void addFilter(Filter filter) {
        filters.add(filter);
    }

    /**
     * Return the matchCondition.
     */
    public MatchCondition getMatchCondition() {
        return matchCondition;
    }

    /**
     * Set the given matchCondition and return this instance for chaining purposes.
     */
    public FilterList matching(MatchCondition matchCondition) {
        this.matchCondition = matchCondition;
        return this;
    }

    /**
     * Return a new instance with the given array of {@link Filter}.
     * <p>
     * @param filters The filters in the array.
     * @return The filter list.
     */
    public static FilterList of(Filter... filters) {
        return new FilterList(Arrays.asList(filters), MatchCondition.ALL);
    }

    /**
     * Return a new instance with the given list of {@link Filter}.
     * <p>
     * @param filters The filters in the list.
     * @return The filter list.
     */
    public static FilterList of(List<Filter> filters) {
        Objects.requireNonNull(filters);
        return new FilterList(filters, MatchCondition.ALL);
    }

    /**
     * The match condition of a {@link FilterList}.
     */
    public enum MatchCondition {

        /**
         * Each {@link Filter} in the {@link FilterList} must match.
         */
        ALL,

        /**
         * At least one of the {@link Filter} in the {@link FilterList} must match.
         */
        ONE
    }

}
