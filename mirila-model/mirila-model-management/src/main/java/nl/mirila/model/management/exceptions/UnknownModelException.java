package nl.mirila.model.management.exceptions;

import nl.mirila.core.exceptions.MirilaException;

public class UnknownModelException extends MirilaException {

    public UnknownModelException(String className) {
        super(String.format("The model '%s' is unknown.", className));
    }

    public UnknownModelException(Class<?> klass) {
        super(String.format("The model '%s' is unknown.", klass.getSimpleName()));
    }

}
