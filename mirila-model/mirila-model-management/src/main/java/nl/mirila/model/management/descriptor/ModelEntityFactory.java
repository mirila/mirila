package nl.mirila.model.management.descriptor;

import nl.mirila.core.converters.DateConverter;
import nl.mirila.core.converters.StringConverter;
import nl.mirila.core.datatype.Id;
import nl.mirila.core.exceptions.InvalidStringConversionException;
import nl.mirila.core.exceptions.UnsupportedDateConversionTypeException;
import nl.mirila.core.exceptions.UnsupportedStringConversionTypeException;
import nl.mirila.model.core.exceptions.InvalidKeyException;
import nl.mirila.model.core.references.Key;
import nl.mirila.model.core.references.Model;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * A factory class for a specific model.
 * <p>
 * @param <M> The type of the model.
 */
public class ModelEntityFactory<M extends Model> {

    private static final Logger logger = LogManager.getLogger(ModelEntityFactory.class);

    private final ModelDescriptor<M> modelDescriptor;

    /**
     * Initialize an instance.
     */
    ModelEntityFactory(ModelDescriptor<M> modelDescriptor) {
        this.modelDescriptor = modelDescriptor;
    }

    /**
     * Creates an instance for the entity with the given key.
     * <p>
     * @param key The key of the entity for which the instance should be created.
     * @return The new instance.
     */
    public M createInstanceWithKey(Key<M> key) {
        List<String> keyFields = modelDescriptor.getPrimaryKeyFields();
        // Check if part sizes are correct.
        if (key.getParts().size() != keyFields.size()) {
            throw new InvalidKeyException(key);
        }

        // Add the key parts to the fields of the new instance.
        Map<String, Object> values = IntStream.range(0, key.getParts().size())
                .boxed()
                .collect(Collectors.toMap(keyFields::get, (i) -> key.getPart(i).orElseThrow()));

        // Check if classes of the parts match
        for (String fieldName : keyFields) {
            Class<?> fieldClass = modelDescriptor.getFieldClasses().get(fieldName);
            Object fieldValue = values.get(fieldName);
            if (!fieldClass.isInstance(fieldValue)) {
                throw new InvalidKeyException(key);
            }
        }
        return createInstanceWithValues(values);
    }

    /**
     * Creates an empty instance just in the default state. If the object could not be created,
     * an {@link UnsupportedOperationException} is thrown.
     * <p>
     * @return The empty instance.
     */
    public M createEmptyInstance() {
        Class<M> modelClass = modelDescriptor.getModelClass();

        @SuppressWarnings("unchecked")
        Constructor<M>[] constructors = (Constructor<M>[]) modelClass.getConstructors();

        Optional<Constructor<M>> optional = Stream.of(constructors)
                .filter((constructor) -> (constructor.getParameterCount() == 0))
                .findFirst();
        Constructor<M> constructor = optional.orElseThrow(() -> {
            String msg = String.format("Couldn't find a constructor without parameters for %s.",
                                       modelClass.getSimpleName());
            throw new UnsupportedOperationException(msg);
        });
        try {
            return constructor.newInstance();
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            String msg = String.format("Couldn't instantiate %s: %s",
                                       modelClass.getSimpleName(),
                                       e.getMessage());
            throw new UnsupportedOperationException(msg);
        }
    }

    /**
     * Creates an instance and sets the given values.
     * When fields can not be set, due to incompatible classes, this is logged, and that field is skipped.
     * <p>
     * @param values The values.
     * @return The newly created instance.
     */
    public M createInstanceWithValues(Map<String, Object> values) {
        M entity = createEmptyInstance();
        updateEntityWithValues(entity, values);
        return entity;
    }

    /**
     * Sets the given values to the given instance.
     * <p>
     * @param entity The entity.
     * @param values The values.
     */
    public void updateEntityWithValues(M entity, Map<String, Object> values) {
        Class<M> modelClass = modelDescriptor.getModelClass();
        Map<String, Class<?>> fieldClasses = modelDescriptor.getFieldClasses();
        fieldClasses.forEach((fieldName, fieldClass) -> {
            if (values.containsKey(fieldName)) {
                Object fieldValue = values.get(fieldName);
                if ((fieldClass == Id.class) && (fieldValue != null)) {
                    fieldValue = Id.of(fieldValue.toString());
                } else if ((fieldClass != String.class) && (fieldValue instanceof String valueString)) {
                    try {
                        // TODO: Add support of list an map.
                        fieldValue = StringConverter.convert(fieldClass, valueString);
                    } catch (InvalidStringConversionException | UnsupportedStringConversionTypeException e) {
                        logger.warn(e.getMessage());
                        return;
                    }
                } else if (fieldValue instanceof Date date) {
                    try {
                        fieldValue = DateConverter.convert(fieldClass, date);
                    } catch (UnsupportedDateConversionTypeException e) {
                        logger.warn(e.getMessage());
                        return;
                    }
                }

                try {
                    modelDescriptor.getFieldSetters()
                            .get(fieldName)
                            .invoke(entity, fieldValue);
                } catch (IllegalArgumentException | IllegalAccessException | InvocationTargetException e) {
                    logger.error("Could not set {}.{} to {}: {}.",
                                 modelClass.getSimpleName(),
                                 fieldName,
                                 fieldValue,
                                 e.getMessage());
                }
            }
        });
    }

    /**
     * Sets the given owner id to related field, if applicable.
     * <p>
     * If the field is an {@link Id}, the owner id is replaced with the given id.
     * If the field is a {@link Collection} of {@link Id}, the owner id is added if the collection doesn't contain it.
     * <p>
     * @param entity The entity to receive the owner id.
     * @param ownerId The owner id.
     */
    public void setOwnerId(M entity, Id ownerId) {
        Optional<String> optional = modelDescriptor.getOwnerIdField();
        if (optional.isEmpty()) {
            return;
        }

        String ownerIdFieldName = optional.get();
        Class<?> fieldClass = modelDescriptor.getFieldClasses().get(ownerIdFieldName);
        if (Id.class.isAssignableFrom(fieldClass)) {
            Method setter = modelDescriptor.getFieldSetters().get((ownerIdFieldName));
            try {
                setter.invoke(entity, ownerId);
            } catch (IllegalAccessException | InvocationTargetException e) {
                logger.error("Could not set {}.{} to {}: {}.",
                             entity.getClass().getSimpleName(),
                             ownerIdFieldName,
                             ownerId,
                             e.getMessage());
            }
        } else if (Collection.class.isAssignableFrom(fieldClass)) {
            Method getter = modelDescriptor.getFieldGetters().get(ownerIdFieldName);
            try {
                @SuppressWarnings("unchecked")
                Collection<Id> collection = (Collection<Id>) getter.invoke(entity);
                // Remove non-owner ids.
                collection.removeIf((id) -> Arrays.asList("", " ", "0", null).contains(id.asString()));
                if (!collection.contains(ownerId)) {
                    collection.add(ownerId);
                }
            } catch (IllegalAccessException | InvocationTargetException e) {
                logger.error("Could not get {}.{} to add {}: {}.",
                             entity.getClass().getSimpleName(),
                             ownerIdFieldName,
                             ownerId,
                             e.getMessage());
            }
        } else {
            logger.error("Could not set id field type {} on {}.{}.",
                         fieldClass.getSimpleName(),
                         entity.getClass().getSimpleName(),
                         ownerIdFieldName);
        }
    }

}
