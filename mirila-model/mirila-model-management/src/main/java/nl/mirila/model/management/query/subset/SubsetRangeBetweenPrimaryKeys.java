package nl.mirila.model.management.query.subset;

import nl.mirila.model.core.references.Key;

import java.util.Optional;

/**
 * Defines the range of a subset, from a start key to an end key.
 */
public class SubsetRangeBetweenPrimaryKeys extends SubsetRange {

    private final Key<?> inclusiveStartKey;
    private final Key<?> exclusiveEndKey;

    /**
     * Initializes this subset range.
     */
    protected SubsetRangeBetweenPrimaryKeys(Key<?> inclusiveStartKey) {
        this(inclusiveStartKey, null, null);
    }

    /**
     * Initializes this subset range.
     */
    protected SubsetRangeBetweenPrimaryKeys(Key<?> inclusiveStartKey, Key<?> exclusiveEndKey, Integer maxSize) {
        super(maxSize);
        this.inclusiveStartKey = inclusiveStartKey;
        this.exclusiveEndKey = exclusiveEndKey;
    }

    /**
     * Returns the inclusive start key, wrapped in an {@link Optional}.
     */
    public Optional<Key<?>> getInclusiveStartKey() {
        return Optional.ofNullable(inclusiveStartKey);
    }

    /**
     * Returns the exclusive end key, wrapped in an {@link Optional}.
     */
    public Optional<Key<?>> getExclusiveEndKey() {
        return Optional.ofNullable(exclusiveEndKey);
    }

    /**
     * Returns a {@link SubsetRangeBetweenPrimaryKeys} with the current start key and without an end key.
     * This method returns the current instance. Therefore, it has only semantic value. The developer can explicitly
     * tell show that the range goes to the end.
     * <p>
     * @return The subset range.
     */
    public SubsetRangeBetweenPrimaryKeys toEnd() {
        return this;
    }

    /**
     * Returns a new {@link SubsetRangeBetweenPrimaryKeys} with the current start key and the given end key.
     * <p>
     * @param exclusiveEndKey The exclusive end key.
     * @return The subset.
     */
    public SubsetRangeBetweenPrimaryKeys to(Key<?> exclusiveEndKey) {
        return new SubsetRangeBetweenPrimaryKeys(inclusiveStartKey, exclusiveEndKey, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SubsetRangeBetweenPrimaryKeys withMaxSize(int maxSize) {
        return new SubsetRangeBetweenPrimaryKeys(inclusiveStartKey, exclusiveEndKey, maxSize);
    }

}
