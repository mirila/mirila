package nl.mirila.model.management.parameters;

import nl.mirila.model.management.exceptions.EntityNotFoundException;
import nl.mirila.model.management.hooks.HooksManager;
import nl.mirila.model.management.managers.EntityManager;
import nl.mirila.model.management.managers.EntityPersistenceManager;

public enum CommonFlags implements Flag {

    /**
     * Do not use cache.
     */
    NO_CACHE,

    /**
     * Skip all rights checks in the {@link EntityManager} or {@link EntityPersistenceManager}.
     */
    SKIP_RIGHTS_CHECK,

    /**
     * Skip calling the hooks in the {@link HooksManager} in the {@link EntityManager}.
     */
    SKIP_HOOKS,

    /**
     * Do not throw an {@link EntityNotFoundException} in the {@link EntityManager}, if the entity is not found.
     */
    IGNORE_IF_NOT_EXISTS
    
}
