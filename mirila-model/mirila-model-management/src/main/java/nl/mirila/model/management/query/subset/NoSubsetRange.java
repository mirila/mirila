package nl.mirila.model.management.query.subset;

/**
 * A {@link SubsetRange} without any limitations.
 */
public class NoSubsetRange extends SubsetRangeBetweenPrimaryKeys {

    /**
     * Initialize a new instance.
     */
    protected NoSubsetRange() {
        super(null, null, Integer.MAX_VALUE);
    }

}
