package nl.mirila.model.management.exceptions;

import nl.mirila.core.exceptions.MirilaException;
import nl.mirila.model.core.references.Key;

import static javax.ws.rs.core.Response.Status.NOT_FOUND;

public class EntityNotFoundException extends MirilaException {

    public EntityNotFoundException(Key<?> key) {
        super(String.format("Entity with key '%s' does not exists.", key),
              NOT_FOUND.getStatusCode());
    }

}
