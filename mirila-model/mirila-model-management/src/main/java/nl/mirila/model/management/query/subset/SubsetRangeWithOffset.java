package nl.mirila.model.management.query.subset;

/**
 * Defines the range of a subset, with an offset.
 */
public class SubsetRangeWithOffset extends SubsetRange {

    private final int offset;

    /**
     * Initializes the SubsetRangeWithOffset.
     */
    protected SubsetRangeWithOffset(int offset) {
        super();
        this.offset = offset;
    }

    /**
     * Initializes the SubsetRangeWithOffset.
     */
    protected SubsetRangeWithOffset(int offset, int maxSize) {
        super(maxSize);
        this.offset = Math.max(0, offset);
    }

    /**
     * Returns the offset.
     */
    public int getOffset() {
        return offset;
    }

    /**
     * Returns a new SubsetRangeWithOffset with the current offset and a sensible max size,
     * which is set to the {@code Short.MAX_VALUE}.
     */
    public SubsetRangeWithOffset withSensibleMaxSize() {
        return new SubsetRangeWithOffset(offset, Short.MAX_VALUE);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SubsetRangeWithOffset withMaxSize(int maxSize) {
        return new SubsetRangeWithOffset(offset, maxSize);
    }

}
