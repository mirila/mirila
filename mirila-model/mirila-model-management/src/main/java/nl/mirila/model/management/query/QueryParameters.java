package nl.mirila.model.management.query;

import nl.mirila.model.core.references.Model;
import nl.mirila.model.management.managers.EntitiesProvider;
import nl.mirila.model.management.managers.EntityManager;
import nl.mirila.model.management.parameters.ActionParameters;
import nl.mirila.model.management.parameters.Flag;
import nl.mirila.model.management.parameters.Option;
import nl.mirila.model.management.query.filters.Filter;
import nl.mirila.model.management.query.filters.FilterList;
import nl.mirila.model.management.query.filters.FilterList.MatchCondition;
import nl.mirila.model.management.query.subset.SubsetRange;
import nl.mirila.model.management.results.Results;

import java.util.List;
import java.util.Optional;

import static nl.mirila.model.management.query.filters.FilterList.MatchCondition.ALL;

/**
 * An {@link ActionParameters} that is used for queries.
 */
public class QueryParameters<E extends Model> extends ActionParameters<E> {

    private Filter filter;
    private Sort sort;
    private SubsetRange subsetRange;
    private EntitiesProvider<E> entitiesProvider;

    /**
     * Initialize a new instance.
     */
    private QueryParameters(Class<E> modelClass) {
        super(modelClass);
        filter = null;
        sort = null;
        subsetRange = null;
        entitiesProvider = null;
    }

    /**
     * Return the {@link Filter} to use.
     */
    public Filter getFilter() {
        return Optional.ofNullable(filter).orElse(Filter.none());
    }

    /**
     * Return the {@link Sort} to use.
     */
    public Sort getSort() {
        return Optional.ofNullable(sort).orElse(Sort.none());
    }

    /**
     * Return the {@link SubsetRange} to use.
     */
    public SubsetRange getSubsetRange() {
        return Optional.ofNullable(subsetRange).orElse(SubsetRange.none());
    }

    /**
     * Return the {@link EntitiesProvider}, wrapped in an {@link Optional}.
     */
    public Optional<EntitiesProvider<E>> getEntitiesProvider() {
        return Optional.ofNullable(entitiesProvider);
    }

    /**
     * Create a new instance of {@link QueryParameters} for the given model class.
     * <p>
     * @param modelClass The class of the model.
     * @param <T> The type of the model.
     * @return A new QueryParameters.
     */
    public static <T extends Model> QueryParameters<T> on(Class<T> modelClass) {
        return new QueryParameters<>(modelClass);
    }

    /**
     * Set or add the given {@link Filter} and return this instance for chaining purposes.
     * <p>
     * If the filter is already set, the existing filter and the given filter will be wrapped in a {@link FilterList}.
     * If the filter is already a {@link FilterList} and with {@link MatchCondition#ALL}, the given filter will be
     * added to that list.
     * If the filter is already a {@link FilterList} and with {@link MatchCondition#ONE}, a new {@link FilterList}
     * will be created with {@link MatchCondition#ALL} and the existing list and the given filter are added to it.
     */
    public QueryParameters<E> withFilter(Filter filter) {
        if (this.filter != null) {
            if (this.filter instanceof FilterList filterList) {
                if (filterList.getMatchCondition().equals(ALL)) {
                    filterList.addFilter(filter);
                    return this;
                }
            }
            this.filter = FilterList.of(this.filter, filter);
        } else {
            this.filter = filter;
        }
        return this;
    }

    /**
     * Set the given {@link Sort} and return this instance for chaining purposes.
     */
    public QueryParameters<E> withSort(Sort sort) {
        this.sort = sort;
        return this;
    }

    /**
     * Set the given {@link SubsetRange} and return this instance for chaining purposes.
     */
    public QueryParameters<E> withSubsetRange(SubsetRange subsetRange) {
        this.subsetRange = subsetRange;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public QueryParameters<E> withOption(Option option) {
        return (QueryParameters<E>) super.withOption(option);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public QueryParameters<E> withOptions(List<Option> options) {
        return (QueryParameters<E>) super.withOptions(options);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public QueryParameters<E> withFlag(Flag flag) {
        return (QueryParameters<E>) super.withFlag(flag);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public QueryParameters<E> withFlags(Flag... flags) {
        return (QueryParameters<E>) super.withFlags(flags);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public QueryParameters<E> withFlags(List<Flag> flags) {
        return (QueryParameters<E>) super.withFlags(flags);
    }

    /**
     * Set the given {@link EntitiesProvider} and return this instance for chaining purposes.
     */
    public QueryParameters<E> usingEntityProvider(EntitiesProvider<E> provider) {
        this.entitiesProvider = provider;
        return this;
    }

    /**
     * Run this query with the given manager.
     * <p>
     * @param manager The entity manager.
     * @return The results of the query.
     */
    public Results<E> runWith(EntityManager<E> manager) {
        return manager.query(this);
    }

    /**
     * Run this count query with the given manager.
     * <p>
     * @param manager The entity manager.
     * @return The number of records.
     */
    public long countWith(EntityManager<E> manager) {
        return manager.count(this);
    }

}
