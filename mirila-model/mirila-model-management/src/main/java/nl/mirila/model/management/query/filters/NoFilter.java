package nl.mirila.model.management.query.filters;

/**
 * A {@link Filter} that represents no actual filtering.
 */
public class NoFilter implements Filter {

}
