package nl.mirila.model.management.query.filters;


/**
 * The base class for each filter.
 */
public interface Filter {

    /**
     * Return a {@link NoFilter}.
     */
    static Filter none() {
        return new NoFilter();
    }

}
