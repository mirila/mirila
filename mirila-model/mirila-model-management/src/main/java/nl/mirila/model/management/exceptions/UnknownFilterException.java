package nl.mirila.model.management.exceptions;

import nl.mirila.model.management.query.filters.Filter;

public class UnknownFilterException extends RuntimeException {

    public UnknownFilterException(Class<? extends Filter> filterClass) {
        super(String.format("Unknown filter %s. Aborting query ...", filterClass.getSimpleName()));
    }

}
