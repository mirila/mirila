package nl.mirila.model.management.module;

import com.google.inject.AbstractModule;
import com.google.inject.multibindings.Multibinder;
import nl.mirila.model.core.ModelCoreModule;
import nl.mirila.model.management.descriptor.ModelDescriptors;
import nl.mirila.model.management.hooks.HooksManager;
import nl.mirila.model.management.hooks.PostHook;
import nl.mirila.model.management.hooks.PreHook;
import nl.mirila.model.management.hooks.RightsHook;

public class ModelManagementModule extends AbstractModule {

    @Override
    protected void configure() {
        install(new ModelCoreModule());

        bind(ModelDescriptors.class).asEagerSingleton();

        bind(HooksManager.class);
        Multibinder<PreHook> preHookBinder = Multibinder.newSetBinder(binder(), PreHook.class);
        Multibinder<PostHook> postHookBinder = Multibinder.newSetBinder(binder(), PostHook.class);

        preHookBinder.addBinding().to(RightsHook.class);
        postHookBinder.addBinding().to(RightsHook.class);
    }

}
