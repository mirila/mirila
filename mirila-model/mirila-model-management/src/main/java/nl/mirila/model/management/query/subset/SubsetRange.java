package nl.mirila.model.management.query.subset;

import nl.mirila.model.core.references.Key;

import java.util.Optional;

/**
 * Defines the range of a subset. The abstract class must be used to instantiate concrete subset ranges.
 */
public abstract class SubsetRange {

    private final Integer maxSize;

    /**
     * Initializes a range without a max size.
     */
    protected SubsetRange() {
        maxSize = null;
    }

    /**
     * Initializes a range with the given max size. If the max size is 0 or less, the maxSize is set to null.
     */
    protected SubsetRange(Integer maxSize) {
        this.maxSize = ((maxSize != null) && (maxSize > 0)) ? maxSize : null;
    }

    /**
     * Returns a new subset range with the given max size.
     * <p>
     * @param maxSize The max size.
     * @return The new subset range.
     */
    public abstract SubsetRange withMaxSize(int maxSize);

    /**
     * Returns the max size, wrapped in an {@link Optional}.
     */
    public Optional<Integer> getMaxSize() {
        return Optional.ofNullable(maxSize);
    }

    /**
     * Returns a {@link SubsetRangeWithOffset} with the given offset.
     * <p>
     * @param offset The offset
     * @return The subset range.
     */
    public static SubsetRangeWithOffset from(int offset) {
        return new SubsetRangeWithOffset(offset);
    }

    /**
     * Returns a {@link SubsetRangeWithOffset} without an offset.
     * <p>
     * @return The subset range.
     */
    public static SubsetRangeWithOffset withoutOffset() {
        return new SubsetRangeWithOffset(0);
    }

    /**
     * Returns a {@link SubsetRangeBetweenPrimaryKeys} that starts at the given key.
     * <p>
     * @param inclusiveStartKey The start key.
     * @return The subset range.
     */
    public static SubsetRangeBetweenPrimaryKeys from(Key<?> inclusiveStartKey) {
        return new SubsetRangeBetweenPrimaryKeys(inclusiveStartKey);
    }

    /**
     * Returns a {@link SubsetRangeBetweenPrimaryKeys} that starts at the beginning.
     * <p>
     * @return The subset range.
     */
    public static SubsetRangeBetweenPrimaryKeys fromBeginning() {
        return new SubsetRangeBetweenPrimaryKeys(null);
    }

    /**
     * Return an empty {@link SubsetRange}.
     */
    public static SubsetRange none() {
        return new NoSubsetRange();
    }

}
