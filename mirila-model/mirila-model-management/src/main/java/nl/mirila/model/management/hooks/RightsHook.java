package nl.mirila.model.management.hooks;

import com.google.inject.Inject;
import nl.mirila.model.core.references.Key;
import nl.mirila.model.core.references.Model;
import nl.mirila.model.management.descriptor.ModelDescriptor;
import nl.mirila.model.management.descriptor.ModelDescriptors;
import nl.mirila.model.management.exceptions.EntityNotFoundException;
import nl.mirila.model.management.exceptions.NoRightsException;
import nl.mirila.model.management.managers.EntityManager;
import nl.mirila.model.management.managers.EntityPersistenceManager;
import nl.mirila.model.management.parameters.ActionParameters;
import nl.mirila.model.management.parameters.CommonFlags;
import nl.mirila.model.management.parameters.RightSettingOption;
import nl.mirila.model.management.query.QueryParameters;
import nl.mirila.model.management.results.Result;
import nl.mirila.model.management.results.Results;
import nl.mirila.security.rights.CurrentRightsActorProvider;
import nl.mirila.security.rights.ObjectRight;
import nl.mirila.security.rights.RightsValidator;
import nl.mirila.security.rights.objects.RightsActor;
import nl.mirila.security.rights.objects.RightsObject;

import java.util.HashSet;
import java.util.Set;

/**
 * The RightsHook implements the {@link PreHook} and {@link PostHook} that handles the rights of entities.
 */
public class RightsHook implements PreHook, PostHook {

    private final ModelDescriptors modelDescriptors;
    private final RightsValidator validator;
    private final CurrentRightsActorProvider currentRightsActorProvider;
    private final EntityPersistenceManager persistenceManager;

    /**
     * Initialize a new instance.
     */
    @Inject
    public RightsHook(ModelDescriptors modelDescriptors,
                      RightsValidator validator,
                      CurrentRightsActorProvider currentRightsActorProvider,
                      EntityPersistenceManager persistenceManager) {
        this.modelDescriptors = modelDescriptors;
        this.validator = validator;
        this.currentRightsActorProvider = currentRightsActorProvider;
        this.persistenceManager = persistenceManager;
    }

    /**
     * Return the priority of this hook. As this hook should be called as last hook, before the action of the
     * {@link EntityManager}, this method returns the highest possible value. Other hooks should not use the same
     * value.
     */
    @Override
    public int getPreHookPriority() {
        return Integer.MAX_VALUE;
    }

    /**
     * Return the priority of this hook. As this hook should be called as first hook, after the action of the
     * {@link EntityManager}, this method returns the lowest possible value. Other hooks should not use the same
     * value.
     */
    @Override
    public int getPostHookPriority() {
        return Integer.MIN_VALUE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <E extends Model> void afterQuery(Results<E> results, QueryParameters<E> queryParameters) {
        if (persistenceManager.requiresRightsCheckAfterQuery()) {
            ifRightsEnabled(queryParameters, () -> {
                Set<String> settingKeys = getSettingKeys(queryParameters);
                results.removeIf((entity) -> {
                    RightsObject rightsObject = getDescriptor(queryParameters).getRightsObjectForEntity(entity);
                    return !validator.hasRight(rightsObject, ObjectRight.READ, settingKeys);
                });
            });
        }
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public <E extends Model> void afterRead(Key<E> key, Result<E> result, ActionParameters<E> parameters) {
        result.ifPresent((entity) -> {
            ifRightsEnabled(parameters, () -> {
                RightsObject rightsObject = getDescriptor(parameters).getRightsObjectForEntity(entity);
                Set<String> settingKeys = getSettingKeys(parameters);
                if (!validator.hasRight(rightsObject, ObjectRight.READ, settingKeys)) {
                    throw new NoRightsException(ObjectRight.READ);
                }
            });
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <E extends Model> void beforeCreate(E entity, ActionParameters<E> parameters) {
        ifEntityRightsCheckNeeded(parameters, () -> {
            RightsActor currentActor = currentRightsActorProvider.get();

            // We may not create it based on maintain rights are other custom setting keys.
            // We do not test the given entity as it may be corrupt, so we use an empty entity of the same model.
            ModelDescriptor<E> descriptor = getDescriptor(parameters);
            String createRightsCategory = descriptor.getRightsCategory() + RightsValidator.SETTING_KEY_POSTFIX_CREATE;
            E emptyEntity = descriptor.getEntityFactory().createEmptyInstance();
            descriptor.getEntityFactory().setOwnerId(emptyEntity, currentActor.getActorId());
            RightsObject emptyRightsObject = descriptor.getRightsObjectForEntity(emptyEntity);
            if (!validator.hasRight(emptyRightsObject, ObjectRight.CREATE, Set.of(createRightsCategory))) {
                throw new NoRightsException(ObjectRight.CREATE);
            }

            // Check whether the given owner of the entity is empty or the current actor.
            RightsObject entityRightsObject = descriptor.getRightsObjectForEntity(entity);
            if ((!entityRightsObject.getOwnerIds().isEmpty()) &&
                    (!entityRightsObject.getOwnerIds().contains(currentActor.getActorId()))) {
                throw new NoRightsException(ObjectRight.CREATE);
            }

            // Ensure that the owner of this entity is set to the id of the current actor.
            descriptor.getEntityFactory().setOwnerId(entity, currentActor.getActorId());
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <E extends Model> void beforeUpdate(E entity, ActionParameters<E> parameters) {
        Key<E> key = getDescriptor(parameters).getPrimaryKeyForEntity(entity);
        checkRightsOnCurrentEntityIfNeeded(key, ObjectRight.UPDATE, parameters);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <E extends Model> void beforeDelete(Key<E> key, ActionParameters<E> parameters) {
        checkRightsOnCurrentEntityIfNeeded(key, ObjectRight.DELETE, parameters);
    }

    /**
     * Convenient method that calls the runnable if the hooks are enabled.
     */
    private <E extends Model> void ifRightsEnabled(ActionParameters<E> parameters, Runnable runnable) {
        if ((!parameters.hasFlag(CommonFlags.SKIP_RIGHTS_CHECK)) &&
                (getDescriptor(parameters).usesRights())) {
            runnable.run();
        }
    }

    /**
     * Return the {@link ModelDescriptor} for the class in the given {@link ActionParameters}.
     */
    private <E extends Model> ModelDescriptor<E> getDescriptor(ActionParameters<E> parameters) {
        return modelDescriptors.getDescriptorForModel(parameters.getModelClass());
    }

    /**
     * Convenient method that checks whether an entity check needs to be done.
     * <p>
     * This is not needed when the current user is a super user, has rights via the settings, or if right checks are
     * disabled.
     * <p>
     * @param parameters The run parameters to use.
     * @param entityRightsChecker The runnable that checks the entity rights.
     */
    private <E extends Model> void ifEntityRightsCheckNeeded(ActionParameters<E> parameters,
                                                             Runnable entityRightsChecker) {
        ifRightsEnabled(parameters, () -> {
            RightsActor currentActor = currentRightsActorProvider.get();
            Set<String> settingKeys = getSettingKeys(parameters);
            if ((!currentActor.isSuperUser()) && (!validator.hasRight(settingKeys))) {
                entityRightsChecker.run();
            }
        });
    }


    /**
     * Checks the object rights of the current actor on the object with the given key.
     * <p>
     * If the current actor doesn't have the rights, a {@link NoRightsException} will be thrown.
     * <p>
     * @param key The key of the entity.
     * @param right The object right to check for.
     * @param parameters The action parameters.
     * @param <E>
     */
    private <E extends Model>  void checkRightsOnCurrentEntityIfNeeded(Key<E> key,
                                                                       ObjectRight right,
                                                                       ActionParameters<E> parameters) {
        ifEntityRightsCheckNeeded(parameters, () -> {
            E entity = persistenceManager.read(key, parameters).orElseThrow(() -> new EntityNotFoundException(key));
            RightsObject rightsObject = modelDescriptors
                    .getDescriptorForModel(parameters.getModelClass())
                    .getRightsObjectForEntity(entity);
            if (!validator.hasRight(rightsObject, right)) {
                throw new NoRightsException(right);
            }
        });
    }


    /**
     * Return the settings keys from the given options. It will use the rights category of the model definition to add
     * - [rights.category].maintain: If this setting is set to true, the maintenance of all objects is allowed.
     * <p>
     * Additionally, if the options contain a {@link RightSettingOption}, it will use those keys as well.
     * <p>
     * @param parameters The run parameters to use.
     * @return A set of setting keys.
     */
    private <E extends Model> Set<String> getSettingKeys(ActionParameters<E> parameters) {
        String rightsCategory = getDescriptor(parameters).getRightsCategory();

        Set<String> keys = new HashSet<>();
        keys.add(rightsCategory + RightsValidator.SETTING_KEY_POSTFIX_MAINTAIN);

        parameters.getOptions().stream()
                .filter(RightSettingOption.class::isInstance)
                .map(RightSettingOption.class::cast)
                .map(RightSettingOption::getKeys)
                .forEach(keys::addAll);

        return keys;
    }

}
