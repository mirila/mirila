package nl.mirila.model.management.exceptions;

import nl.mirila.core.exceptions.MirilaException;

public class NonStorableModelException extends MirilaException {

    public NonStorableModelException(String className) {
        super(String.format("Non-storable model '%s' can not be used for regular data storage.", className));
    }

    public NonStorableModelException(Class<?> klass) {
        super(String.format("Non-storable model '%s' can not be used for regular data storage.", klass.getSimpleName()));
    }

}
