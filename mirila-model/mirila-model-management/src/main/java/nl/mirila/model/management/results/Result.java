package nl.mirila.model.management.results;

import nl.mirila.model.core.references.Model;

import java.util.NoSuchElementException;
import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * A wrapper for a result of a specific model.
 */
public class Result<E extends Model> {

    private E entity;

    /**
     * Initialize a new instance.
     */
    private Result(E entity) {
        this.entity = entity;
    }

    /**
     * Return the entity.
     */
    public E get() {
        return entity;
    }

    /**
     * Replace the content of this result with this new entity.
     * <p>
     * In some case it may be needed to replace the entity in this result, for example when working with hooks.
     * <p>
     * @param entity The new entity.
     */
    public void replace(E entity) {
        this.entity = entity;
    }

    /**
     * Return true if this result does not contain an entity.
     */
    public boolean isEmpty() {
        return (entity == null);
    }

    /**
     * Run the given consumer for the entity, if it is available.
     */
    public void ifPresent(Consumer<E> consumer) {
        if (entity != null) {
            consumer.accept(entity);
        }
    }

    /**
     * Return the entity, or if it is missing, the given alternative entity.
     */
    public E orElse(E other) {
        return (entity != null) ? entity : other;
    }

    /**
     * Return the entity, or throw a {@link NoSuchElementException} if it is missing.
     */
    public E orElseThrow() {
        if (entity == null) {
            throw new NoSuchElementException();
        }
        return entity;
    }

    /**
     * Return the entity, or throw an exception provided via the given supplier.
     */
    public <X extends Throwable> E orElseThrow(Supplier<X> supplier) throws X {
        if (entity == null) {
            throw supplier.get();
        }
        return entity;
    }

    /**
     * Create a new result wrapper containing the given entity.
     * <p>
     * @param entity The entity.
     * @param <E> The type of the entity.
     * @return The result with the entity.
     */
    public static <E extends Model> Result<E> of(E entity) {
        return new Result<>(entity);
    }

    /**
     * Create an empty result wrapper
     * <p>
     * @param <E> The type of the entity.
     * @return The result without an entity.
     */
    public static <E extends Model> Result<E> empty() {
        return new Result<>(null);
    }

}
