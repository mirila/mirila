package nl.mirila.model.management.managers;

import nl.mirila.model.core.references.Model;
import nl.mirila.model.management.query.QueryParameters;
import nl.mirila.model.management.results.Result;
import nl.mirila.model.management.results.Results;

/**
 * The {@link EntitiesProvider} provides a list of entities, wrapped in a {@link Results}.
 * <p>
 * Use this provider as an alternative for the default query method of the {@link EntityPersistenceManager}.
 * <p>
 * @param <E> The type of the model.
 */
public interface EntitiesProvider<E extends Model> {

    /**
     * Return the entity with the given primary key.
     * <p>
     * @param queryParameters The query parameters.
     * @return The entity, wrapped in a {@link Result}.
     */
    Results<E> provide(QueryParameters<E> queryParameters);

}
