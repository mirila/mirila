package nl.mirila.model.management.descriptor;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import nl.mirila.core.exceptions.MirilaException;
import nl.mirila.model.core.annotations.ModelInfo;
import nl.mirila.model.core.references.Model;
import nl.mirila.model.management.exceptions.UnknownModelException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.reflections.Reflections;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

/**
 * A container class for all {@link ModelDescriptor}.
 */
@Singleton
public class ModelDescriptors {

    private static final Logger logger = LogManager.getLogger(ModelDescriptors.class);

    private Map<String, ModelDescriptor<? extends Model>> map = new HashMap<>();

    /**
     * Load all models of within the given package and create a {@link ModelDescriptor} for all objects that have
     * are annotated with the model {@link ModelInfo}.
     * <p>
     * @param packageNames The fully qualified package name.
     */
    @Inject
    public ModelDescriptors(@ModelsPackageNames Set<String> packageNames) {
        if (packageNames.isEmpty()) {
            logger.warn("There are no package names bound, so no models to find.");
        }
        packageNames.forEach((packageName) -> {
            Reflections reflections = new Reflections(packageName);
            reflections.getSubTypesOf(Model.class).stream()
                    .filter((modelClass) -> modelClass.isAnnotationPresent(ModelInfo.class))
                    .map(ModelDescriptor::new)
                    .forEach(this::registerDescriptor);
        });
        if (map.isEmpty()) {
            logger.warn("There are no models found in the packages: " + String.join(", ", packageNames));
        }
    }

    /**
     * Register the given model descriptor.
     */
    private <M extends Model> void registerDescriptor(ModelDescriptor<M> modelDescriptor) {
        Class<M> modelClass = modelDescriptor.getModelClass();
        String simpleName = modelClass.getSimpleName();
        if (map.containsKey(simpleName)) {
            String template = "Can not add class %s from package %s, " +
                    "as we already know a model from with the same name in package %s.";
            String thisPackage = modelClass.getPackageName();
            String otherPackage = map.get(simpleName).getModelClass().getPackageName();
            throw new MirilaException(template.formatted(simpleName, thisPackage, otherPackage));
        }
        map.put(modelClass.getSimpleName(), modelDescriptor);
    }

    /**
     * Return a stream of all {@link ModelDescriptor}.
     */
    public Stream<ModelDescriptor<? extends Model>> stream() {
        return map.values().stream();
    }

    /**
     * Return the {@link ModelDescriptor} for the given model.
     * <p>
     * @param modelClass The model.
     * @return The model descriptor.
     */
    public <M extends Model> ModelDescriptor<M> getDescriptorForModel(Class<M> modelClass) {
        return getDescriptorForClassName(modelClass.getSimpleName());
    }

    /**
     * Return the {@link ModelDescriptor} for the model with the given name.
     * <p>
     * @param simpleClassName The simple name of the model.
     * @return The model descriptor.
     * @param <M> The type of the model.
     */
    public <M extends Model> ModelDescriptor<M> getDescriptorForClassName(String simpleClassName) {
        ModelDescriptor<?> descriptor = map.get(simpleClassName);
        if (descriptor == null) {
            throw new UnknownModelException(simpleClassName);
        }
        @SuppressWarnings("unchecked")
        ModelDescriptor<M> modelDescriptor = (ModelDescriptor<M>) descriptor;
        return modelDescriptor;
    }

}
