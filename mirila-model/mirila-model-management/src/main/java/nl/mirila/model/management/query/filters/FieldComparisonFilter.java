package nl.mirila.model.management.query.filters;

import nl.mirila.model.management.enums.RelationalOperator;

/**
 * A filter that contains information on comparing fields.
 */
public class FieldComparisonFilter implements Filter {

    private final String fieldName;
    private final RelationalOperator operator;
    private final Object value;

    /**
     * Initialize a new instance.
     */
    public FieldComparisonFilter(String fieldName, RelationalOperator operator, Object value) {
        this.fieldName = fieldName;
        this.operator = operator;
        this.value = value;
    }

    /**
     * Return the field name.
     */
    public String getFieldName() {
        return fieldName;
    }

    /**
     * Return the relational operator.
     */
    public RelationalOperator getOperator() {
        return operator;
    }

    /**
     * Return the value.
     */
    public Object getValue() {
        return value;
    }

}
