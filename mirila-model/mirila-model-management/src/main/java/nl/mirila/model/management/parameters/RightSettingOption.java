package nl.mirila.model.management.parameters;

import nl.mirila.model.management.managers.EntityPersistenceManager;

import java.util.Set;

/**
 * The RightSettingOption represents a list of keys, used for the right settings. This can be used to pass the setting
 * keys that can be used by the {@link EntityPersistenceManager} in its right checks.
 */
public class RightSettingOption implements Option {

    private final Set<String> keys;

    /**
     * Initialize a new instance with the given keys.
     */
    private RightSettingOption(String... settingKeys) {
        keys = Set.of(settingKeys);
    }

    /**
     * Return the rights setting keys.
     */
    public Set<String> getKeys() {
        return keys;
    }

    /**
     * Return an instance of {@link RightSettingOption} with the given keys.
     * <p>
     * @param settingKeys The keys.
     * @return The RightSettingOption.
     */
    public static RightSettingOption of(String... settingKeys) {
        return new RightSettingOption(settingKeys);
    }
}
