package nl.mirila.model.management.hooks;

import com.google.inject.Inject;
import nl.mirila.model.core.references.Key;
import nl.mirila.model.core.references.Model;
import nl.mirila.model.management.parameters.ActionParameters;
import nl.mirila.model.management.query.QueryParameters;
import nl.mirila.model.management.results.Result;
import nl.mirila.model.management.results.Results;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jboss.resteasy.plugins.guice.RequestScoped;

import java.util.Comparator;
import java.util.List;
import java.util.Set;

/**
 * The HooksManager works as a proxy {@link PreHook} and {@link PostHook}, calling all relevant hooks that are provided
 * in the constructor. The order in which hooks are called, is based on their priority.
 * <p>
 * For each request, one new HooksManager is created, using {@link RequestScoped}. This will automatically make all
 * instances of the hooks have the same scope.
 */
@RequestScoped
public class HooksManager {

    private static final Logger logger = LogManager.getLogger(HooksManager.class);
    
    private final List<PreHook> preHooks;
    private final List<PostHook> postHooks;

    /**
     * Initializing a new instance with the provided sets of {@link PreHook} and {@link PostHook}.
     */
    @Inject
    public HooksManager(Set<PreHook> preHooks, Set<PostHook> postHooks) {
        logger.debug("Initializing HooksManager for this request");
        this.preHooks = preHooks.stream()
                .sorted(Comparator.comparingInt(PreHook::getPreHookPriority))
                .peek((hook) -> logger.debug("Registering pre-hook {}", hook.getClass().getSimpleName()))
                .toList();
        this.postHooks = postHooks.stream()
                .sorted(Comparator.comparingInt(PostHook::getPostHookPriority))
                .peek((hook) -> logger.debug("Registering post-hook {}", hook.getClass().getSimpleName()))
                .toList();
    }

    /**
     * Return the list of {@link PreHook}.
     */
    protected List<PreHook> getPreHooks() {
        return preHooks;
    }

    /**
     * Return the list of {@link PostHook}.
     */
    protected List<PostHook> getPostHooks() {
        return postHooks;
    }

    /**
     * Call the {@link PreHook#beforeCount(QueryParameters)} of all pre-hooks.
     * <p>
     * @param queryParameters The query parameters that defines the query.
     * @param <E> The type of the entities
     */
    public <E extends Model> void beforeCount(QueryParameters<E> queryParameters) {
        preHooks.forEach((hook) -> hook.beforeCount(queryParameters));
    }

    /**
     * Call the {@link PreHook#beforeQuery(QueryParameters)} of all pre-hooks.
     * <p>
     * @param queryParameters The query parameters that refine the query.
     * @param <E> The type of the entities
     */
    public <E extends Model> void beforeQuery(QueryParameters<E> queryParameters) {
        preHooks.forEach((hook) -> hook.beforeQuery(queryParameters));
    }

    /**
     * Call the {@link PreHook#beforeRead(Key, ActionParameters)} of all pre-hooks.
     * <p>
     * @param key The key of the entity.
     * @param parameters The run parameter that refine the read.
     * @param <E> The type of the entity.
     */
    public <E extends Model> void beforeRead(Key<E> key, ActionParameters<E> parameters) {
        preHooks.forEach((hook) -> hook.beforeRead(key, parameters));
    }

    /**
     * Call the {@link PreHook#beforeCreate(Model, ActionParameters)} of all pre-hooks.
     * <p>
     * @param entity The entity.
     * @param parameters The run parameter that refine the create.
     * @param <E> The type of the entity.
     */
    public <E extends Model> void beforeCreate(E entity, ActionParameters<E> parameters) {
        preHooks.forEach((hook) -> hook.beforeCreate(entity, parameters));
    }

    /**
     * Call the {@link PreHook#beforeUpdate(Model, ActionParameters)} of all pre-hooks.
     * <p>
     * @param entity The entity.
     * @param parameters The run parameter that refine the read.
     * @param <E> The type of the entity.
     */
    public <E extends Model> void beforeUpdate(E entity, ActionParameters<E> parameters) {
        preHooks.forEach((hook) -> hook.beforeUpdate(entity, parameters));
    }

    /**
     * Call the {@link PreHook#beforeDelete(Key, ActionParameters)} of all pre-hooks.
     * <p>
     * @param key The key of the entity.
     * @param parameters The run parameter that refine the read.
     * @param <E> The type of the entity.
     */
    public <E extends Model> void beforeDelete(Key<E> key, ActionParameters<E> parameters) {
        preHooks.forEach((hook) -> hook.beforeDelete(key, parameters));
    }

    /**
     * Call the {@link PostHook#afterCount(long, QueryParameters)} of all post-hooks.
     * <p>
     * @param count The count.
     * @param queryParameters The query parameters that refine the query.
     * @param <E> The type of the entities.
     */
    public <E extends Model> void afterCount(long count, QueryParameters<E> queryParameters) {
        postHooks.forEach((hook) -> hook.afterCount(count, queryParameters));
    }

    /**
     * Call the {@link PostHook#afterQuery(Results, QueryParameters)} of all post-hooks.
     * <p>
     * @param results The resulting entities.
     * @param queryParameters The query parameters that refine the query.
     * @param <E> The type of the entities.
     */
    public <E extends Model> void afterQuery(Results<E> results, QueryParameters<E> queryParameters) {
        postHooks.forEach((hook) -> hook.afterQuery(results, queryParameters));
    }

    /**
     * Call the {@link PostHook#afterRead(Key, Result, ActionParameters)} of all post-hooks.
     * <p>
     * @param key  The key of the requested entity.
     * @param result The resulting entity.
     * @param parameters The query parameters that refine the read.
     * @param <E> The type of the entity.
     */
    public <E extends Model> void afterRead(Key<E> key, Result<E> result, ActionParameters<E> parameters) {
        postHooks.forEach((hook) -> hook.afterRead(key, result, parameters));
    }

    /**
     * Call the {@link PostHook#afterCreate(Result, ActionParameters)} of all post-hooks.
     * <p>
     * @param result The resulting entity.
     * @param parameters The query parameters that refine the create.
     * @param <E> The type of the entity.
     */
    public <E extends Model> void afterCreate(Result<E> result, ActionParameters<E> parameters) {
        postHooks.forEach((hook) -> hook.afterCreate(result, parameters));
    }

    /**
     * Call the {@link PostHook#afterUpdate(Result, ActionParameters)} of all post-hooks.
     * <p>
     * @param result The resulting entity.
     * @param parameters The query parameters that refine the update.
     * @param <E> The type of the entity.
     */
    public <E extends Model> void afterUpdate(Result<E> result, ActionParameters<E> parameters) {
        postHooks.forEach((hook) -> hook.afterUpdate(result, parameters));
    }

    /**
     * Call the {@link PostHook#afterDelete(Key, ActionParameters)} of all post-hooks.
     * <p>
     * @param key The key of the entity to delete.
     * @param parameters The query parameters that refine the delete.
     * @param <E> The type of the entientityties.
     */
    public <E extends Model> void afterDelete(Key<E> key, ActionParameters<E> parameters) {
        postHooks.forEach((hook) -> hook.afterDelete(key, parameters));
    }

}
