package nl.mirila.model.management.exceptions;

import nl.mirila.core.exceptions.MirilaException;
import nl.mirila.model.core.references.Key;
import nl.mirila.model.core.references.Model;
import nl.mirila.security.rights.ObjectRight;

import static javax.ws.rs.core.Response.Status.FORBIDDEN;

public class NoRightsException extends MirilaException {

    public NoRightsException(ObjectRight right) {

        super(String.format("You have no rights to %s this entity.", right.toString().toLowerCase()),
              FORBIDDEN.getStatusCode());
    }

    public NoRightsException(ObjectRight right, Key<? extends Model> key) {
        super(String.format("You have no rights to %s the entity with key %s.", right.toString().toLowerCase(), key),
              FORBIDDEN.getStatusCode());
    }

}
