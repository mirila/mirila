package nl.mirila.model.management.query;

import nl.mirila.model.core.enums.SortDirection;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * The Sort defines on which field and in which direction a query should sort.
 */
public class Sort {

    private final Map<String, SortDirection> fields;

    /**
     * Initialize a new instance.
     */
    private Sort() {
        fields = new LinkedHashMap<>();
    }

    /**
     * Initialize a new instance with the given parameters.
     */
    private Sort(String fieldName, SortDirection direction) {
        fields = new LinkedHashMap<>();
        thenOn(fieldName, direction);
    }

    /**
     * Add the given field name to sort on in ascending direction, and return the instance for chaining purposes.
     * <p>
     * @param fieldName The field name.
     * @return The Sort.
     */
    public Sort thenOn(String fieldName) {
        return thenOn(fieldName, SortDirection.ASCENDING);
    }

    /**
     * Add the given field name to sort on in the given direction, and return the instance for chaining purposes.
     * <p>
     * @param fieldName The field name.
     * @return The Sort.
     */
    public Sort thenOn(String fieldName, SortDirection direction) {
        fields.put(fieldName, direction);
        return this;
    }

    /**
     * Return a new {@link Sort} with the given field name, in ascending direction (which is the default).
     * <p>
     * @param fieldName The field name.
     * @return The Sort.
     */
    public static Sort on(String fieldName) {
        return on(fieldName, SortDirection.ASCENDING);
    }

    /**
     * Return a new {@link Sort} with the given field name, in the given direction.
     * <p>
     * @param fieldName The field name.
     * @return The Sort.
     */
    public static Sort on(String fieldName, SortDirection direction) {
        return new Sort(fieldName, direction);
    }

    /**
     * Return a map of all sort fields and their directions.
     */
    public Map<String, SortDirection> getFields() {
        return fields;
    }

    /**
     * Return an empty {@link Sort}.
     */
    public static Sort none() {
        return new Sort();
    }

}
