package nl.mirila.model.management.enums;

/**
 * The RelationOperator represents comparison operators.
 */
public enum RelationalOperator {

    EQ,
    NEQ,
    LT,
    LTE,
    GTE,
    GT

}
