package nl.mirila.model.management.query.testdata;

import nl.mirila.model.management.parameters.Option;

public class TestOption implements Option {

    private final TextTransform textTransform;

    private TestOption(TextTransform textTransform) {
        this.textTransform = textTransform;
    }

    public TextTransform getTextTransform() {
        return textTransform;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if ((other == null) || (getClass() != other.getClass())) {
            return false;
        }

        TestOption that = (TestOption) other;
        return textTransform == that.textTransform;
    }

    @Override
    public int hashCode() {
        return (textTransform != null) ? (textTransform.hashCode()) : 0;
    }

    public static TestOption of(TextTransform textTransform) {
        return new TestOption(textTransform);
    }

    public enum TextTransform {
        CAPITALIZE,
        LOWERCASE,
        UNDERLINE
    }

}
