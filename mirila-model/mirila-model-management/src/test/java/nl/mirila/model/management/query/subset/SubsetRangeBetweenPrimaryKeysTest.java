package nl.mirila.model.management.query.subset;

import nl.mirila.model.core.references.Key;
import nl.mirila.model.management.query.testdata.TestUser;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class SubsetRangeBetweenPrimaryKeysTest {

    @Test
    void testAllInRange() {
        SubsetRangeBetweenPrimaryKeys subset = SubsetRange.fromBeginning().toEnd();
        assertThat(subset.getInclusiveStartKey()).isEmpty();
        assertThat(subset.getExclusiveEndKey()).isEmpty();
        assertThat(subset.getMaxSize()).isEmpty();
    }

    @Test
    void testAllInRangeWithSmallSize() {
        SubsetRangeBetweenPrimaryKeys subset = SubsetRange.fromBeginning().toEnd().withMaxSize(25);
        assertThat(subset.getInclusiveStartKey()).isEmpty();
        assertThat(subset.getExclusiveEndKey()).isEmpty();
        assertThat(subset.getMaxSize()).contains(25);
    }

    @Test
    void testInRangeFromCertainKey() {
        Key<?> firstKey = Key.of(TestUser.class, "123");
        SubsetRangeBetweenPrimaryKeys subset = SubsetRange.from(firstKey).toEnd();
        assertThat(subset.getInclusiveStartKey()).contains(firstKey);
        assertThat(subset.getExclusiveEndKey()).isEmpty();
        assertThat(subset.getMaxSize()).isEmpty();
    }

    @Test
    void testInRangeToCertainKey() {
        Key<?> lastKey = Key.of(TestUser.class, 234);
        SubsetRangeBetweenPrimaryKeys subset = SubsetRange.fromBeginning().to(lastKey);
        assertThat(subset.getInclusiveStartKey()).isEmpty();
        assertThat(subset.getExclusiveEndKey()).contains(lastKey);
        assertThat(subset.getMaxSize()).isEmpty();
    }

    @Test
    void testInRangeFromAndToCertainKeyWithMaxSize() {
        Key<?> firstKey = Key.of(TestUser.class, "123");
        Key<?> lastKey = Key.of(TestUser.class, 234);
        SubsetRangeBetweenPrimaryKeys subset = SubsetRange.from(firstKey).to(lastKey).withMaxSize(35);
        assertThat(subset.getInclusiveStartKey()).contains(firstKey);
        assertThat(subset.getExclusiveEndKey()).contains(lastKey);
        assertThat(subset.getMaxSize()).contains(35);
    }

}
