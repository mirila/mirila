package nl.mirila.model.management.descriptor;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.multibindings.Multibinder;
import nl.mirila.model.core.exceptions.InvalidKeyException;
import nl.mirila.model.core.references.Key;
import nl.mirila.model.management.descriptor.models.TestKeyModel;
import nl.mirila.model.management.descriptor.models.TestUser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.HashMap;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

class ModelEntityFactoryTest {

    private ModelEntityFactory<TestKeyModel> keyEntityFactory;
    private ModelEntityFactory<TestUser> userEntityFactory;

    @BeforeEach
    void setUp() {
        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                Multibinder<String> binder = Multibinder.newSetBinder(binder(), String.class, ModelsPackageNames.class);
                binder.addBinding().toInstance(TestUser.class.getPackageName());
            }
        });
        ModelDescriptors descriptors = injector.getInstance(ModelDescriptors.class);

        ModelDescriptor<TestKeyModel> keyDescriptor = descriptors.getDescriptorForModel(TestKeyModel.class);
        ModelDescriptor<TestUser> userDescriptor = descriptors.getDescriptorForModel(TestUser.class);

        keyEntityFactory = new ModelEntityFactory<>(keyDescriptor);
        userEntityFactory = new ModelEntityFactory<>(userDescriptor);
    }

    @Test
    void createInstanceWithKey() {
        LocalDate today = LocalDate.now();
        Key<TestKeyModel> key = Key.of(TestKeyModel.class, "str", 123, true, today);
        TestKeyModel keyModel = keyEntityFactory.createInstanceWithKey(key);

        assertThat(keyModel.getKey1()).isEqualTo("str");
        assertThat(keyModel.getKey2()).isEqualTo(123);
        assertThat(keyModel.getKey3()).isTrue();
        assertThat(keyModel.getKey4()).isEqualTo(today);

        Key<TestKeyModel> incorrectKey1 = Key.of(TestKeyModel.class, "str", 123, "str", today);
        assertThatExceptionOfType(InvalidKeyException.class).isThrownBy(() -> {
            keyEntityFactory.createInstanceWithKey(incorrectKey1);
        });

        Key<TestKeyModel> incorrectKey2 = Key.of(TestKeyModel.class, "str", 123, today);
        assertThatExceptionOfType(InvalidKeyException.class).isThrownBy(() -> {
            keyEntityFactory.createInstanceWithKey(incorrectKey2);
        });
    }

    @Test
    void createInstanceWithValues() {
        HashMap<String, Object> values = new HashMap<>();
        values.put("firstName", "John");
        values.put("lastName", "Doe");
        values.put("emailAddress", "john@doe.com");
        values.put("dateOfBirth", "1999-02-03");
        TestUser user = userEntityFactory.createInstanceWithValues(values);

        assertThat(user.getUserId()).isNull();
        assertThat(user.getFirstName()).isEqualTo("John");
        assertThat(user.getLastName()).isEqualTo("Doe");
        assertThat(user.getEmailAddress()).isEqualTo("john@doe.com");
        assertThat(user.getPhoneNumber()).isNull();
        assertThat(user.getGender()).isNull();
        assertThat(user.getDateOfBirth()).isEqualTo(LocalDate.of(1999, 2, 3));
    }

}
