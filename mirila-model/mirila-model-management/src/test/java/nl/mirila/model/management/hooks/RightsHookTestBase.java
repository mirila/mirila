package nl.mirila.model.management.hooks;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Scopes;
import com.google.inject.multibindings.Multibinder;
import nl.mirila.core.datatype.Id;
import nl.mirila.core.settings.ApplicationSettings;
import nl.mirila.core.settings.Settings;
import nl.mirila.core.settings.modules.KeyValuesSettingsModule;
import nl.mirila.metrics.core.MetricsCoreModule;
import nl.mirila.model.management.descriptor.ModelsPackageNames;
import nl.mirila.model.management.descriptor.models.TestNewsItem;
import nl.mirila.model.management.managers.EntityPersistenceManager;
import nl.mirila.model.management.managers.FakePersistenceManager;
import nl.mirila.model.management.module.ModelManagementModule;
import nl.mirila.security.auth.core.claims.Claims;
import nl.mirila.security.auth.core.contexts.SecurityContextProvider;
import org.jboss.resteasy.plugins.guice.RequestScoped;

import java.util.Map;

import static nl.mirila.core.settings.ApplicationSettings.KEY_REALM;

class RightsHookTestBase {

    public static Injector getInjector(Claims claims, Map<String, String> settings) {
        return Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                settings.put(KEY_REALM, "test-realm");
                ApplicationSettings applicationSettings = new ApplicationSettings(Settings.withMap(settings));
                install(new KeyValuesSettingsModule(settings));
                install(new ModelManagementModule());
                install(new MetricsCoreModule());

                SecurityContextProvider provider = new SecurityContextProvider(applicationSettings);
                provider.generateContextWithClaims(claims);
                bind(SecurityContextProvider.class).toInstance(provider);
                bind(EntityPersistenceManager.class).to(FakePersistenceManager.class);
                bind(FakePersistenceManager.class).asEagerSingleton();
                bindScope(RequestScoped.class, Scopes.SINGLETON);

                Multibinder<String> binder = Multibinder.newSetBinder(binder(), String.class, ModelsPackageNames.class);
                binder.addBinding().toInstance(TestNewsItem.class.getPackageName());
            }
        });
    }

    public TestNewsItem getTestNewsItem(Id newsItemId, Id ownerId, Id groupId, int rights) {
        TestNewsItem item = new TestNewsItem();
        item.setNewsItemId(newsItemId.asString());
        item.setTitle("Just a title");
        item.setContent("Just some content");
        item.getOwnerIds().add(ownerId);
        item.getGroupIds().add(groupId);
        item.setRights(rights);
        return item;
    }

}
