package nl.mirila.model.management.hooks;

import com.google.inject.Injector;
import com.google.inject.TypeLiteral;
import nl.mirila.core.datatype.Id;
import nl.mirila.model.core.references.Key;
import nl.mirila.model.management.descriptor.models.TestNewsItem;
import nl.mirila.model.management.exceptions.NoRightsException;
import nl.mirila.model.management.managers.EntityManager;
import nl.mirila.model.management.managers.FakePersistenceManager;
import nl.mirila.model.management.parameters.CommonFlags;
import nl.mirila.model.management.query.QueryParameters;
import nl.mirila.model.management.results.Results;
import nl.mirila.security.auth.core.claims.Claims;
import nl.mirila.security.auth.core.claims.ClaimsBuilder;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.List;

import static org.assertj.core.api.Assertions.*;

class RightsHookAsGuestTest extends RightsHookTestBase {

    private static EntityManager<TestNewsItem> manager;
    private static FakePersistenceManager persistenceManager;
    private static Id otherOwnerId;
    private static Id otherGroupId;
    private static Id currentOwnerId;
    private static HashMap<String, String> settings;

    @BeforeAll
    static void beforeAll() {
        settings = new HashMap<>();
        otherOwnerId = Id.of(543);
        currentOwnerId = Id.empty();
        otherGroupId = Id.of(456);
        Claims claims = ClaimsBuilder.forRealm("test-realm").getClaims();
        Injector injector = getInjector(claims, settings);

        //noinspection Convert2Diamond
        manager = injector.getInstance(com.google.inject.Key.get(new TypeLiteral<EntityManager<TestNewsItem>>(){}));
        persistenceManager = injector.getInstance(FakePersistenceManager.class);
    }

    @BeforeEach
    void setUp() {
        persistenceManager.clear();
        settings.clear();
    }

    private void fillStorage() {
        persistenceManager.create(getTestNewsItem(Id.of(100), otherOwnerId, otherGroupId, 0x7074));
        persistenceManager.create(getTestNewsItem(Id.of(101), otherOwnerId, otherGroupId, 0x7044));
        persistenceManager.create(getTestNewsItem(Id.of(102), otherOwnerId, otherGroupId, 0x7040));
        persistenceManager.create(getTestNewsItem(Id.of(103), otherOwnerId, otherGroupId, 0x4400));
    }

    @Test
    void testQueryWithRightsCheck() {
        fillStorage();

        QueryParameters<TestNewsItem> queryParameters = QueryParameters.on(TestNewsItem.class);
        Results<TestNewsItem> results = manager.query(queryParameters);
        assertThat(results.isEmpty()).isFalse();
        assertThat(results.get().size()).isEqualTo(2);

        List<Id> foundIds = results.get().stream()
                .map(TestNewsItem::getNewsItemId)
                .map(Id::of)
                .toList();
        assertThat(foundIds).containsExactlyInAnyOrder(Id.of(100), Id.of(101));
    }

    @Test
    void testQueryWithoutRightsCheck() {
        fillStorage();
        QueryParameters<TestNewsItem> queryParameters = QueryParameters
                .on(TestNewsItem.class)
                .withFlag(CommonFlags.SKIP_RIGHTS_CHECK);
        Results<TestNewsItem> results = manager.query(queryParameters);
        assertThat(results.isEmpty()).isFalse();
        assertThat(results.get().size()).isEqualTo(4);
    }

    @Test
    void testReadItemGuestsReadable() {
        TestNewsItem item = getTestNewsItem(Id.of(100), otherOwnerId, otherGroupId, 0x0007);
        persistenceManager.create(item);

        Key<TestNewsItem> itemKey = Key.of(TestNewsItem.class, item.getNewsItemId());
        assertThatNoException().isThrownBy(() -> {
            manager.read(itemKey);
        });
    }

    @Test
    void testReadItemNotGuestsReadable() {
        TestNewsItem item = getTestNewsItem(Id.of(100), otherOwnerId, otherGroupId, 0x0040);
        persistenceManager.create(item);

        Key<TestNewsItem> itemKey = Key.of(TestNewsItem.class, item.getNewsItemId());
        assertThatThrownBy(() -> {
            manager.read(itemKey);
        }).isInstanceOf(NoRightsException.class);
    }

    @Test
    void testUpdateGuestUpdatableItem() {
        TestNewsItem originalItem = getTestNewsItem(Id.of(100), currentOwnerId, otherGroupId, 0x0007);
        TestNewsItem updatedItem = getTestNewsItem(Id.of(100), currentOwnerId, otherGroupId, 0x0070);
        persistenceManager.create(originalItem);

        assertThatNoException().isThrownBy(() -> {
            manager.update(updatedItem);
        });
    }

    @Test
    void testUpdateNotGuestUpdatableItem() {
        TestNewsItem originalItem = getTestNewsItem(Id.of(100), otherOwnerId, otherGroupId, 0x0070);
        TestNewsItem updatedItem = getTestNewsItem(Id.of(100), otherOwnerId, otherGroupId, 0x0007);
        persistenceManager.create(originalItem);

        assertThatThrownBy(() -> {
            manager.update(updatedItem);
        }).isInstanceOf(NoRightsException.class);
    }

    @Test
    void testDeleteGuestDeletableItem() {
        TestNewsItem item = getTestNewsItem(Id.of(101), currentOwnerId, otherGroupId, 0x0007);
        Key<TestNewsItem> itemKey = Key.of(TestNewsItem.class, item.getNewsItemId());
        persistenceManager.create(item);

        assertThatNoException().isThrownBy(() -> {
            manager.delete(itemKey);
        });
    }

    @Test
    void testDeleteNotGuestDeletableItem() {
        TestNewsItem item = getTestNewsItem(Id.of(101), otherOwnerId, otherGroupId, 0x0070);
        Key<TestNewsItem> itemKey = Key.of(TestNewsItem.class, item.getNewsItemId());
        persistenceManager.create(item);

        assertThatThrownBy(() -> {
            manager.delete(itemKey);
        }).isInstanceOf(NoRightsException.class);
    }

}
