package nl.mirila.model.management.query.testdata;

import nl.mirila.model.management.managers.EntitiesProvider;
import nl.mirila.model.management.query.QueryParameters;
import nl.mirila.model.management.results.Results;

import java.util.Arrays;

public class TestUserEntitiesProvider implements EntitiesProvider<TestUser> {

    @Override
    public Results<TestUser> provide(QueryParameters<TestUser> queryParameters) {
        TestUser user1 = new TestUser();
        user1.setUserId("1");
        user1.setFirstName("");
        user1.setLastName("");

        TestUser user2 = new TestUser();
        user2.setUserId("2");
        user2.setFirstName("");
        user2.setLastName("");

        return Results.of(Arrays.asList(user1, user2));
    }

}
