package nl.mirila.model.management.query.testdata;

import nl.mirila.model.management.parameters.Flag;

public enum TestFlag implements Flag {

    NO_CACHE,
    SKIP_RIGHTS_CHECK,
    IGNORE_DELETED

}
