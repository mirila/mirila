package nl.mirila.model.management.query.subset;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class SubsetRangeWithOffsetTest {

    @Test
    void testAllInRange() {
        SubsetRangeWithOffset subset = SubsetRange.withoutOffset();
        assertThat(subset.getOffset()).isZero();
        assertThat(subset.getMaxSize()).isEmpty();
    }

    @Test
    void testRangeFromOffsetWithSensibleMaxSize() {
        SubsetRangeWithOffset subset = SubsetRange.from(25).withSensibleMaxSize();
        assertThat(subset.getOffset()).isEqualTo(25);
        assertThat(subset.getMaxSize()).contains((int) Short.MAX_VALUE);
    }

    @Test
    void testSmallSubsetRange() {
        SubsetRangeWithOffset subset = SubsetRange.from(25).withMaxSize(50);
        assertThat(subset.getOffset()).isEqualTo(25);
        assertThat(subset.getMaxSize()).contains(50);
    }

}
