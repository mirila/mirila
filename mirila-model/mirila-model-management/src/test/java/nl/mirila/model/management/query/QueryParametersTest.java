package nl.mirila.model.management.query;

import com.google.inject.*;
import com.google.inject.multibindings.Multibinder;
import nl.mirila.core.settings.modules.KeyValuesSettingsModule;
import nl.mirila.metrics.core.MetricsCoreModule;
import nl.mirila.model.core.enums.SortDirection;
import nl.mirila.model.core.references.Key;
import nl.mirila.model.core.references.Model;
import nl.mirila.model.management.descriptor.ModelsPackageNames;
import nl.mirila.model.management.enums.RelationalOperator;
import nl.mirila.model.management.managers.EntityManager;
import nl.mirila.model.management.managers.EntityPersistenceManager;
import nl.mirila.model.management.module.ModelManagementModule;
import nl.mirila.model.management.parameters.ActionParameters;
import nl.mirila.model.management.parameters.Flag;
import nl.mirila.model.management.parameters.Option;
import nl.mirila.model.management.query.filters.FieldComparisonFilter;
import nl.mirila.model.management.query.testdata.TestFlag;
import nl.mirila.model.management.query.testdata.TestOption;
import nl.mirila.model.management.query.testdata.TestUser;
import nl.mirila.model.management.query.testdata.TestUserEntitiesProvider;
import nl.mirila.model.management.results.Result;
import nl.mirila.model.management.results.Results;
import nl.mirila.security.auth.core.contexts.GuestSecurityContext;
import nl.mirila.security.auth.core.contexts.SecurityContext;
import org.jboss.resteasy.plugins.guice.RequestScoped;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

import static com.google.inject.Key.get;
import static nl.mirila.core.settings.ApplicationSettings.KEY_APP_NAME;
import static nl.mirila.core.settings.ApplicationSettings.KEY_REALM;
import static nl.mirila.model.management.query.testdata.TestOption.TextTransform.*;
import static org.assertj.core.api.Assertions.assertThat;

class QueryParametersTest {

    private QueryParameters<TestUser> lastQueryParameters;
    private EntityManager<TestUser> manager;

    @BeforeEach
    @SuppressWarnings({"unchecked", "Convert2Diamond"})
    void setUp() {
        lastQueryParameters = null;
        EntityPersistenceManager manager = new EntityPersistenceManager() {

            @Override
            public <E extends Model> long count(QueryParameters<E> queryParameters) {
                return 0;
            }

            @Override
            public <E extends Model> Results<E> query(QueryParameters<E> query) {
                QueryParametersTest.this.lastQueryParameters = (QueryParameters<TestUser>) query;
                return Results.empty();
            }

            @Override
            public <E extends Model> Result<E> create(E entity, ActionParameters<E> parameters) {
                return null;
            }

            @Override
            public <E extends Model> Result<E> read(Key<E> key, ActionParameters<E> parameters) {
                return null;
            }

            @Override
            public <E extends Model> Result<E> update(E entity, ActionParameters<E> parameters) {
                return null;
            }

            @Override
            public <E extends Model> void delete(Key<E> key, ActionParameters<E> parameters) {
            }

        };
        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                Map<String, String> settings = new HashMap<>();
                settings.put(KEY_APP_NAME, "test-app");
                settings.put(KEY_REALM, "test-realm");

                install(new KeyValuesSettingsModule(settings));
                install(new ModelManagementModule());
                install(new MetricsCoreModule());

                bind(SecurityContext.class).toInstance(new GuestSecurityContext("test-realm", "en_EN"));
                bind(EntityPersistenceManager.class).toInstance(manager);
                bindScope(RequestScoped.class, Scopes.NO_SCOPE);

                Multibinder<String> binder = Multibinder.newSetBinder(binder(), String.class, ModelsPackageNames.class);
                binder.addBinding().toInstance(TestUser.class.getPackageName());
            }
        });
        this.manager = injector.getInstance(get(new TypeLiteral<EntityManager<TestUser>>(){}));
    }

    @Test
    void testWithFilter() {
        QueryParameters.on(TestUser.class)
                .withFilter(new FieldComparisonFilter("test", RelationalOperator.EQ, "hello world"))
                .runWith(manager);
        assertThat(lastQueryParameters).isNotNull();
        assertThat(lastQueryParameters.getFilter()).isNotNull();
        assertThat(lastQueryParameters.getFilter().getClass()).isEqualTo(FieldComparisonFilter.class);
    }

    @Test
    void testWithSort() {
        Sort fieldsSort = Sort.on("test1")
                .thenOn("test2", SortDirection.DESCENDING)
                .thenOn("test3");
        QueryParameters.on(TestUser.class)
                .withSort(fieldsSort)
                .runWith(manager);
        assertThat(lastQueryParameters).isNotNull();
        assertThat(lastQueryParameters.getSort()).isNotNull();

        Map<String, SortDirection> fields = lastQueryParameters.getSort().getFields();

        assertThat(lastQueryParameters.getSort().getFields())
                .hasSize(3)
                .containsKeys("test1", "test2", "test3");

        assertThat(fields)
                .containsEntry("test1", SortDirection.ASCENDING)
                .containsEntry("test2", SortDirection.DESCENDING)
                .containsEntry("test3", SortDirection.ASCENDING);
    }

    @Test
    void testWithOption() {
        QueryParameters.on(TestUser.class)
                .withOption(TestOption.of(UNDERLINE))
                .runWith(manager);
        assertThat(lastQueryParameters).isNotNull();
        assertThat(lastQueryParameters.getOptions()).containsOnly(TestOption.of(UNDERLINE));
    }


    @Test
    void testWithOptions() {
        List<Option> options = Arrays.asList(TestOption.of(LOWERCASE), TestOption.of(CAPITALIZE));
        QueryParameters.on(TestUser.class)
                .withOptions(options)
                .runWith(manager);
        assertThat(lastQueryParameters).isNotNull();
        assertThat(lastQueryParameters.getOptions()).containsOnly(TestOption.of(LOWERCASE), TestOption.of(CAPITALIZE));
    }

    @Test
    void testWithFlag() {
        QueryParameters.on(TestUser.class).runWith(manager);
        assertThat(lastQueryParameters).isNotNull();

        assertThat(lastQueryParameters.getFlags()).isEmpty();

        QueryParameters.on(TestUser.class)
                .withFlag(TestFlag.NO_CACHE)
                .runWith(manager);
        assertThat(lastQueryParameters).isNotNull();

        Set<?> flags1 = lastQueryParameters.getFlags();
        assertThat(flags1).hasSize(1);
        assertThat(flags1.contains(TestFlag.NO_CACHE)).isTrue();
        assertThat(flags1.contains(TestFlag.SKIP_RIGHTS_CHECK)).isFalse();
        assertThat(flags1.contains(TestFlag.IGNORE_DELETED)).isFalse();

        QueryParameters.on(TestUser.class)
                .withFlag(TestFlag.NO_CACHE)
                .withFlag(TestFlag.IGNORE_DELETED)
                .runWith(manager);
        assertThat(lastQueryParameters).isNotNull();

        Set<Flag> flags2 = lastQueryParameters.getFlags();
        assertThat(flags2).hasSize(2)
                .containsOnly(TestFlag.NO_CACHE, TestFlag.IGNORE_DELETED);
    }

    @Test
    void testWithFlags() {
        QueryParameters.on(TestUser.class).runWith(manager);
        assertThat(lastQueryParameters).isNotNull();

        Set<?> flags0 = lastQueryParameters.getFlags();
        assertThat(flags0).isEmpty();

        QueryParameters.on(TestUser.class)
                .withFlags(TestFlag.NO_CACHE)
                .runWith(manager);
        assertThat(lastQueryParameters).isNotNull();

        Set<Flag> flags1 = lastQueryParameters.getFlags();
        assertThat(flags1).containsOnly(TestFlag.NO_CACHE);

        QueryParameters.on(TestUser.class)
                .withFlags(TestFlag.NO_CACHE, TestFlag.IGNORE_DELETED)
                .runWith(manager);
        assertThat(lastQueryParameters).isNotNull();

        Set<Flag> flags2 = lastQueryParameters.getFlags();
        assertThat(flags2).hasSize(2)
                .containsOnly(TestFlag.NO_CACHE, TestFlag.IGNORE_DELETED);
    }

    @Test
    void testUsingEntityProvider() {
        QueryParameters.on(TestUser.class).runWith(manager);
        assertThat(lastQueryParameters.getEntitiesProvider()).isEmpty();

        Results<TestUser> results = QueryParameters.on(TestUser.class)
                .usingEntityProvider(new TestUserEntitiesProvider())
                .runWith(manager);
        List<String> userIds = results.stream()
                .map(TestUser::getUserId)
                .toList();
        assertThat(results).hasSize(2);
        assertThat(userIds).hasSize(2).isEqualTo(Arrays.asList("1", "2"));
    }

}
