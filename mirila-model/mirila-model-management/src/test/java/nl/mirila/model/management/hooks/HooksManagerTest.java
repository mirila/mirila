package nl.mirila.model.management.hooks;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Scopes;
import com.google.inject.multibindings.Multibinder;
import com.google.inject.name.Names;
import nl.mirila.model.core.references.Key;
import nl.mirila.model.management.parameters.ActionParameters;
import nl.mirila.model.management.query.QueryParameters;
import nl.mirila.model.management.results.Result;
import nl.mirila.model.management.results.Results;
import org.jboss.resteasy.plugins.guice.RequestScoped;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

class HooksManagerTest {

    private static int counter;
    private static ActionParameters<TestEntity> parameters;

    public static void increment() {
        counter++;
    }

    @Test
    void testHookInjection() {
        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                bind(Runnable.class)
                        .annotatedWith(Names.named("TestRunner"))
                        .toInstance(HooksManagerTest::increment);

                Multibinder<PreHook> preHookBinder = Multibinder.newSetBinder(binder(), PreHook.class);
                preHookBinder.addBinding().to(PreHookExample.class);
                preHookBinder.addBinding().to(PrePostHookExample.class);

                Multibinder<PostHook> postHookBinder = Multibinder.newSetBinder(binder(), PostHook.class);
                postHookBinder.addBinding().to(PostHookExample.class);
                postHookBinder.addBinding().to(PrePostHookExample.class);
                bindScope(RequestScoped.class, Scopes.NO_SCOPE);
            }
        });

        counter = 0;
        parameters = QueryParameters.on(TestEntity.class);

        HooksManager manager = injector.getInstance(HooksManager.class);
        assertThat(counter).isZero();

        // Only PreHookExample
        manager.beforeQuery(QueryParameters.on(TestEntity.class));
        assertThat(counter).isEqualTo(1);

        // Only PreHookExample
        manager.beforeRead(Key.of(TestEntity.class), parameters);
        assertThat(counter).isEqualTo(2);

        // Both PreHookExample and PrePostHookExample
        manager.beforeCreate(new TestEntity(), parameters);
        assertThat(counter).isEqualTo(4);

        // Only PrePostHookExample
        manager.beforeUpdate(new TestEntity(), parameters);
        assertThat(counter).isEqualTo(5);

        // Only PrePostHookExample
        manager.beforeDelete(Key.of(TestEntity.class), parameters);
        assertThat(counter).isEqualTo(6);


        // Only PreHookExample
        manager.afterQuery(Results.empty(), QueryParameters.on(TestEntity.class));
        assertThat(counter).isEqualTo(7);

        // Only PreHookExample
        manager.afterRead(Key.of(TestEntity.class), Result.empty(), parameters);
        assertThat(counter).isEqualTo(8);

        // Both PreHookExample and PrePostHookExample
        manager.afterCreate(Result.empty(), parameters);
        assertThat(counter).isEqualTo(10);

        // Only PrePostHookExample
        manager.afterUpdate(Result.empty(), parameters);
        assertThat(counter).isEqualTo(11);

        // Only PrePostHookExample
        manager.afterDelete(Key.of(TestEntity.class), parameters);
        assertThat(counter).isEqualTo(12);
    }



    @Test
    void testHookWithoutInjection() {
        counter = 0;

        PreHookExample preHook = new PreHookExample(HooksManagerTest::increment);
        PostHookExample postHook = new PostHookExample(HooksManagerTest::increment);
        PrePostHookExample prePostHook = new PrePostHookExample(HooksManagerTest::increment);

        Set<PreHook> preHooks = new HashSet<>(Arrays.asList(preHook, prePostHook));
        Set<PostHook> postHooks = new HashSet<>(Arrays.asList(postHook, prePostHook));
        HooksManager manager = new HooksManager(preHooks, postHooks);
        assertThat(counter).isZero();

        // Only PreHookExample
        manager.beforeQuery(QueryParameters.on(TestEntity.class));
        assertThat(counter).isEqualTo(1);

        // Only PreHookExample
        manager.beforeRead(Key.of(TestEntity.class), parameters);
        assertThat(counter).isEqualTo(2);

        // Both PreHookExample and PrePostHookExample
        manager.beforeCreate(new TestEntity(), parameters);
        assertThat(counter).isEqualTo(4);

        // Only PrePostHookExample
        manager.beforeUpdate(new TestEntity(), parameters);
        assertThat(counter).isEqualTo(5);

        // Only PrePostHookExample
        manager.beforeDelete(Key.of(TestEntity.class), parameters);
        assertThat(counter).isEqualTo(6);


        // Only PreHookExample
        manager.afterQuery(Results.empty(), QueryParameters.on(TestEntity.class));
        assertThat(counter).isEqualTo(7);

        // Only PreHookExample
        manager.afterRead(Key.of(TestEntity.class), Result.empty(), parameters);
        assertThat(counter).isEqualTo(8);

        // Both PreHookExample and PrePostHookExample
        manager.afterCreate(Result.empty(), parameters);
        assertThat(counter).isEqualTo(10);

        // Only PrePostHookExample
        manager.afterUpdate(Result.empty(), parameters);
        assertThat(counter).isEqualTo(11);

        // Only PrePostHookExample
        manager.afterDelete(Key.of(TestEntity.class), parameters);
        assertThat(counter).isEqualTo(12);
    }

    @Test
    void testPreHookPriority() {
        PreHook highHook = new PreHook() {
            @Override
            public int getPreHookPriority() {
                return -1;
            }
        };

        PreHook normalHook = new PreHook() {
            @Override
            public int getPreHookPriority() {
                return 0;
            }
        };

        PreHook lowHook = new PreHook() {
            @Override
            public int getPreHookPriority() {
                return 1;
            }
        };
        HooksManager manager = new HooksManager(Set.of(normalHook, lowHook, highHook), Set.of());
        assertThat(manager.getPreHooks()).containsExactly(highHook, normalHook, lowHook);
    }

    @Test
    void testPostHookPriority() {
        PostHook highHook = new PostHook() {
            @Override
            public int getPostHookPriority() {
                return -1;
            }
        };

        PostHook normalHook = new PostHook() {
            @Override
            public int getPostHookPriority() {
                return 0;
            }
        };

        PostHook lowHook = new PostHook() {
            @Override
            public int getPostHookPriority() {
                return 1;
            }
        };
        HooksManager manager = new HooksManager(Set.of(), Set.of(normalHook, lowHook, highHook));
        assertThat(manager.getPostHooks()).containsExactly(highHook, normalHook, lowHook);
    }

}
