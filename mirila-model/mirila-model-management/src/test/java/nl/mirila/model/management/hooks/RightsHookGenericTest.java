package nl.mirila.model.management.hooks;

import com.google.inject.Injector;
import com.google.inject.TypeLiteral;
import nl.mirila.core.datatype.Id;
import nl.mirila.model.core.references.Key;
import nl.mirila.model.management.descriptor.models.TestNewsItem;
import nl.mirila.model.management.exceptions.EntityAlreadyExistsException;
import nl.mirila.model.management.exceptions.EntityNotFoundException;
import nl.mirila.model.management.exceptions.NoRightsException;
import nl.mirila.model.management.managers.EntityManager;
import nl.mirila.model.management.managers.FakePersistenceManager;
import nl.mirila.model.management.parameters.ActionParameters;
import nl.mirila.model.management.parameters.CommonFlags;
import nl.mirila.model.management.parameters.RightSettingOption;
import nl.mirila.model.management.results.Result;
import nl.mirila.security.auth.core.claims.Claims;
import nl.mirila.security.auth.core.claims.ClaimsBuilder;
import nl.mirila.security.rights.RightsValidator;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.assertj.core.api.Assertions.*;

class RightsHookGenericTest extends RightsHookTestBase {

    private static EntityManager<TestNewsItem> manager;
    private static FakePersistenceManager persistenceManager;
    private static Id otherOwnerId;
    private static Id otherGroupId;
    private static Id currentOwnerId;
    private static HashMap<String, String> settings;

    @BeforeAll
    static void beforeAll() {
        settings = new HashMap<>();
        otherOwnerId = Id.of(543);
        currentOwnerId = Id.of(123);
        otherGroupId = Id.of(456);
        Claims claims = ClaimsBuilder.forRealm("test-realm")
                .withSubjectId(currentOwnerId.asString())
                .getClaims();
        Injector injector = getInjector(claims, settings);

        //noinspection Convert2Diamond
        manager = injector.getInstance(com.google.inject.Key.get(new TypeLiteral<EntityManager<TestNewsItem>>(){}));
        persistenceManager = injector.getInstance(FakePersistenceManager.class);
    }

    @BeforeEach
    void setUp() {
        persistenceManager.clear();
        settings.clear();
    }

    @Test
    void testCreateItemSameItemTwice() {
        TestNewsItem item = getTestNewsItem(Id.of(100), otherOwnerId, otherGroupId, 0x7000);
        manager.create(item, ActionParameters.on(TestNewsItem.class).withFlag(CommonFlags.SKIP_RIGHTS_CHECK));
        assertThatThrownBy(() -> {
            manager.create(item, ActionParameters.on(TestNewsItem.class).withFlag(CommonFlags.SKIP_RIGHTS_CHECK));
        }).isInstanceOf(EntityAlreadyExistsException.class);
    }

    @Test
    void testCreateItemWithSkipRights() {
        TestNewsItem item = getTestNewsItem(Id.of(100), otherOwnerId, otherGroupId, 0x7000);
        assertThatNoException().isThrownBy(() -> {
            manager.create(item, ActionParameters.on(TestNewsItem.class).withFlag(CommonFlags.SKIP_RIGHTS_CHECK));
        });
    }

    @Test
    void testCreateItemWithValidCustomRightsCategory() {
        TestNewsItem item = getTestNewsItem(Id.of(100), otherOwnerId, otherGroupId, 0x7000);
        settings.put(RightsValidator.getSettingKeyForUsers("a-custom-category"), "true");
        assertThatNoException().isThrownBy(() -> {
            ActionParameters<TestNewsItem> parameters = ActionParameters
                    .on(TestNewsItem.class)
                    .withOption(RightSettingOption.of("a-custom-category"));
            manager.create(item, parameters);
        });
    }

    @Test
    void testCreateItemWithInvalidCustomRightsCategory() {
        TestNewsItem item = getTestNewsItem(Id.of(100), otherOwnerId, otherGroupId, 0x7000);
        settings.put(RightsValidator.getSettingKeyForUsers("a-custom-category"), "true");
        assertThatThrownBy(() -> {
            ActionParameters<TestNewsItem> parameters = ActionParameters
                    .on(TestNewsItem.class)
                    .withOption(RightSettingOption.of("an-invalid-category"));
            manager.create(item, parameters);
        }).isInstanceOf(NoRightsException.class);
    }

    @Test
    void testCreateItemForOtherActorWithSettingCreateRights() {
        settings.put(RightsValidator.getSettingKeyForUsers("news.create"), "true");
        TestNewsItem item = getTestNewsItem(Id.of(102), otherOwnerId, otherGroupId, 0xFFFF);
        assertThatThrownBy(() -> {
            manager.create(item);
        }).isInstanceOf(NoRightsException.class);
    }

    @Test
    void testCreateItemWithUnknownOwnerIdWithSettingCreateRights() {
        settings.put(RightsValidator.getSettingKeyForUsers("news.create"), "true");
        TestNewsItem item = getTestNewsItem(Id.of(100), Id.empty(), otherGroupId, 0x7000);
        assertThatNoException().isThrownBy(() -> {
            Result<TestNewsItem> result = manager.create(item);
            assertThat(result.isEmpty())
                    .as("Result should not be empty.")
                    .isFalse();
            assertThat(result.get().getOwnerIds()).containsExactly(currentOwnerId);
        });
    }

    @Test
    void testCreateItemForOtherActorWithSettingMaintainRights() {
        settings.put(RightsValidator.getSettingKeyForUsers("news.maintain"), "true");
        TestNewsItem item = getTestNewsItem(Id.of(102), otherOwnerId, otherGroupId, 0xFFFF);
        assertThatNoException().isThrownBy(() -> {
            Result<TestNewsItem> result = manager.create(item);
            assertThat(result.isEmpty())
                    .as("Result should not be empty.")
                    .isFalse();
            assertThat(result.get().getOwnerIds()).containsExactly(otherOwnerId);
        });
    }

    @Test
    void testReadNonExistingItem() {
        assertThatNoException().isThrownBy(() -> {
            Result<TestNewsItem> result = manager.read(Key.of(TestNewsItem.class, 100));
            assertThat(result.get()).isNull();
        });
    }

    @Test
    void testReadWithSkipRights() {
        TestNewsItem item = getTestNewsItem(Id.of(101), otherOwnerId, otherGroupId, 0x7000);
        Key<TestNewsItem> itemKey = Key.of(TestNewsItem.class, item.getNewsItemId());
        persistenceManager.create(item);
        assertThatNoException().isThrownBy(() -> {
            manager.read(itemKey, ActionParameters.on(TestNewsItem.class).withFlag(CommonFlags.SKIP_RIGHTS_CHECK));
        });
    }

    @Test
    void testReadItemWithValidCustomRightsCategory() {
        TestNewsItem item = getTestNewsItem(Id.of(100), otherOwnerId, otherGroupId, 0x7000);
        Key<TestNewsItem> itemKey = Key.of(TestNewsItem.class, item.getNewsItemId());
        persistenceManager.create(item);
        settings.put(RightsValidator.getSettingKeyForUsers("a-custom-category"), "true");
        assertThatNoException().isThrownBy(() -> {
            ActionParameters<TestNewsItem> parameters = ActionParameters
                    .on(TestNewsItem.class)
                    .withOption(RightSettingOption.of("a-custom-category"));
            manager.read(itemKey, parameters);
        });
    }

    @Test
    void testReadItemWithInvalidCustomRightsCategory() {
        TestNewsItem item = getTestNewsItem(Id.of(100), otherOwnerId, otherGroupId, 0x7000);
        Key<TestNewsItem> itemKey = Key.of(TestNewsItem.class, item.getNewsItemId());
        persistenceManager.create(item);
        settings.put(RightsValidator.getSettingKeyForUsers("a-custom-category"), "true");
        assertThatThrownBy(() -> {
            ActionParameters<TestNewsItem> parameters = ActionParameters
                    .on(TestNewsItem.class)
                    .withOption(RightSettingOption.of("an-invalid-category"));
            manager.read(itemKey, parameters);
        }).isInstanceOf(NoRightsException.class);
    }

    @Test
    void testReadItemWithSettingCreateRights() {
        TestNewsItem item = getTestNewsItem(Id.of(100), otherOwnerId, otherGroupId, 0x7000);
        Key<TestNewsItem> itemKey = Key.of(TestNewsItem.class, item.getNewsItemId());
        persistenceManager.create(item);
        settings.put(RightsValidator.getSettingKeyForUsers("news.create"), "true");
        assertThatThrownBy(() -> {
            manager.read(itemKey);
        }).isInstanceOf(NoRightsException.class);
    }

    @Test
    void testReadItemWithSettingMaintainRights() {
        TestNewsItem item = getTestNewsItem(Id.of(100), otherOwnerId, otherGroupId, 0x7000);
        Key<TestNewsItem> itemKey = Key.of(TestNewsItem.class, item.getNewsItemId());
        persistenceManager.create(item);
        settings.put(RightsValidator.getSettingKeyForUsers("news.maintain"), "true");
        assertThatNoException().isThrownBy(() -> {
            manager.read(itemKey);
        });
    }

    @Test
    void testUpdateNonExistingItem() {
        TestNewsItem item = getTestNewsItem(Id.of(100), otherOwnerId, otherGroupId, 0x0700);
        assertThatThrownBy(() -> {
            manager.update(item);
        }).isInstanceOf(EntityNotFoundException.class);
    }

    @Test
    void testUpdateWithSkipRights() {
        TestNewsItem item = getTestNewsItem(Id.of(100), otherOwnerId, otherGroupId, 0x0700);
        assertThatThrownBy(() -> {
            manager.update(item, ActionParameters.on(TestNewsItem.class).withFlag(CommonFlags.SKIP_RIGHTS_CHECK));
        }).isInstanceOf(EntityNotFoundException.class);
    }

    @Test
    void testUpdateItemWithValidCustomRightsCategory() {
        TestNewsItem originalItem = getTestNewsItem(Id.of(100), otherOwnerId, otherGroupId, 0x0700);
        TestNewsItem updatedItem = getTestNewsItem(Id.of(100), otherOwnerId, otherGroupId, 0x0700);
        persistenceManager.create(originalItem);
        settings.put(RightsValidator.getSettingKeyForUsers("a-custom-category"), "true");

        assertThatNoException().isThrownBy(() -> {
            ActionParameters<TestNewsItem> parameters = ActionParameters
                    .on(TestNewsItem.class)
                    .withOption(RightSettingOption.of("a-custom-category"));
            manager.update(updatedItem, parameters);
        });
    }

    @Test
    void testUpdateItemWithInvalidCustomRightsCategory() {
        TestNewsItem originalItem = getTestNewsItem(Id.of(100), otherOwnerId, otherGroupId, 0x0700);
        TestNewsItem updatedItem = getTestNewsItem(Id.of(100), otherOwnerId, otherGroupId, 0x0700);
        persistenceManager.create(originalItem);
        settings.put(RightsValidator.getSettingKeyForUsers("a-custom-category"), "true");

        assertThatThrownBy(() -> {
            ActionParameters<TestNewsItem> parameters = ActionParameters
                    .on(TestNewsItem.class)
                    .withOption(RightSettingOption.of("an-invalid-category"));
            manager.update(updatedItem, parameters);
        }).isInstanceOf(NoRightsException.class);
    }


    @Test
    void testUpdateItemWithMaintainRights() {
        TestNewsItem originalItem = getTestNewsItem(Id.of(100), otherOwnerId, otherGroupId, 0x0700);
        TestNewsItem updatedItem = getTestNewsItem(Id.of(100), otherOwnerId, otherGroupId, 0x0700);
        persistenceManager.create(originalItem);
        settings.put(RightsValidator.getSettingKeyForUsers("news.maintain"), "true");

        assertThatNoException().isThrownBy(() -> {
            manager.update(updatedItem);
        });
    }

    @Test
    void testUpdateItemWithCreateRights() {
        TestNewsItem originalItem = getTestNewsItem(Id.of(100), otherOwnerId, otherGroupId, 0x0700);
        TestNewsItem updatedItem = getTestNewsItem(Id.of(100), otherOwnerId, otherGroupId, 0x0700);
        persistenceManager.create(originalItem);
        settings.put(RightsValidator.getSettingKeyForUsers("news.create"), "true");

        assertThatThrownBy(() -> {
            manager.update(updatedItem);
        }).isInstanceOf(NoRightsException.class);
    }

    @Test
    void testDeleteNonExistingItem() {
        Key<TestNewsItem> itemKey = Key.of(TestNewsItem.class, Id.of(100).asString());
        assertThatThrownBy(() -> {
            manager.delete(itemKey);
        }).isInstanceOf(EntityNotFoundException.class);
    }

    @Test
    void testDeleteWithSkipRights() {
        TestNewsItem item = getTestNewsItem(Id.of(101), otherOwnerId, otherGroupId, 0x7044);
        Key<TestNewsItem> itemKey = Key.of(TestNewsItem.class, item.getNewsItemId());
        persistenceManager.create(item);

        assertThatNoException().isThrownBy(() -> {
            manager.delete(itemKey, ActionParameters.on(TestNewsItem.class).withFlag(CommonFlags.SKIP_RIGHTS_CHECK));
        });
    }

    @Test
    void testDeleteItemWithValidCustomRightsCategory() {
        TestNewsItem item = getTestNewsItem(Id.of(100), otherOwnerId, otherGroupId, 0x7000);
        Key<TestNewsItem> itemKey = Key.of(TestNewsItem.class, item.getNewsItemId());
        persistenceManager.create(item);

        settings.put(RightsValidator.getSettingKeyForUsers("a-custom-category"), "true");
        assertThatNoException().isThrownBy(() -> {
            ActionParameters<TestNewsItem> parameters = ActionParameters
                    .on(TestNewsItem.class)
                    .withOption(RightSettingOption.of("a-custom-category"));
            manager.delete(itemKey, parameters);
        });
    }

    @Test
    void testDeleteItemWithInvalidCustomRightsCategory() {
        TestNewsItem item = getTestNewsItem(Id.of(100), otherOwnerId, otherGroupId, 0x7000);
        Key<TestNewsItem> itemKey = Key.of(TestNewsItem.class, item.getNewsItemId());
        persistenceManager.create(item);

        settings.put(RightsValidator.getSettingKeyForUsers("a-custom-category"), "true");
        assertThatThrownBy(() -> {
            ActionParameters<TestNewsItem> parameters = ActionParameters
                    .on(TestNewsItem.class)
                    .withOption(RightSettingOption.of("an-invalid-category"));
            manager.delete(itemKey, parameters);
        }).isInstanceOf(NoRightsException.class);
    }

    @Test
    void testDeleteItemWithCreateRights() {
        TestNewsItem item = getTestNewsItem(Id.of(100), otherOwnerId, otherGroupId, 0x7000);
        Key<TestNewsItem> itemKey = Key.of(TestNewsItem.class, item.getNewsItemId());
        persistenceManager.create(item);

        settings.put(RightsValidator.getSettingKeyForUsers("news.create"), "true");
        assertThatThrownBy(() -> {
            manager.delete(itemKey);
        }).isInstanceOf(NoRightsException.class);
    }

    @Test
    void testDeleteItemWithMaintainRights() {
        TestNewsItem item = getTestNewsItem(Id.of(100), otherOwnerId, otherGroupId, 0x7000);
        Key<TestNewsItem> itemKey = Key.of(TestNewsItem.class, item.getNewsItemId());
        persistenceManager.create(item);

        settings.put(RightsValidator.getSettingKeyForUsers("news.maintain"), "true");
        assertThatNoException().isThrownBy(() -> {
            manager.delete(itemKey);
        });
    }

}
