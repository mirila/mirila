package nl.mirila.model.management.hooks;

import com.google.inject.Inject;
import nl.mirila.model.core.references.Key;
import nl.mirila.model.core.references.Model;
import nl.mirila.model.management.parameters.ActionParameters;

import javax.inject.Named;

public class PreHookExample extends RunnableHook implements PreHook {

    @Inject
    public PreHookExample(@Named("TestRunner") Runnable runner) {
        super(runner);
    }

    @Override
    public <E extends Model> void beforeCreate(E entity, ActionParameters<E> parameters) {
        runner.run();
    }

    @Override
    public <E extends Model> void beforeUpdate(E entity, ActionParameters<E> parameters) {
        runner.run();
    }

    @Override
    public <E extends Model> void beforeDelete(Key<E> key, ActionParameters<E> parameters) {
        runner.run();
    }

}
