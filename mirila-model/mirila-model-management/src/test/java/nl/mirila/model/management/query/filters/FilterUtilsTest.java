package nl.mirila.model.management.query.filters;

import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

class FilterUtilsTest {

    @Test
    void testGetFilterForAnyOfValuesWithMultipleValues() {
        Optional<FilterList> optional = FilterUtils.getFilterForAnyOfValues("field", "test1,test2,test3");
        assertThat(optional).isPresent();
        assertThat(optional.get().getFilters()).hasSize(3);
    }

    @Test
    void testGetFilterForAnyOfValuesWithSingleValue() {
        Optional<FilterList> optional = FilterUtils.getFilterForAnyOfValues("field", "test1");
        assertThat(optional).isPresent();
        assertThat(optional.get().getFilters()).hasSize(1);
    }

    @Test
    void testGetFilterForAnyOfValuesWithEmptyValue() {
        Optional<FilterList> optional = FilterUtils.getFilterForAnyOfValues("field", "");
        assertThat(optional).isEmpty();
    }

    @Test
    void testGetFilterForAnyOfValuesWithNull() {
        Optional<FilterList> optional = FilterUtils.getFilterForAnyOfValues("field", null, ",");
        assertThat(optional).isEmpty();
    }

}