package nl.mirila.model.management.descriptor.models;

import nl.mirila.model.core.annotations.ModelInfo;
import nl.mirila.model.core.references.Model;

import java.time.LocalDate;

@ModelInfo(
        singular = "TestKeyModel",
        plural = "TestKeyModels",
        primaryKeyFields = {"key1", "key2", "key3", "key4"}
)
public class TestKeyModel implements Model {

    private String key1;

    private Integer key2;

    private Boolean key3;

    private LocalDate key4;

    public String getKey1() {
        return key1;
    }

    public void setKey1(String key1) {
        this.key1 = key1;
    }

    public Integer getKey2() {
        return key2;
    }

    public void setKey2(Integer key2) {
        this.key2 = key2;
    }

    public Boolean getKey3() {
        return key3;
    }

    public void setKey3(Boolean key3) {
        this.key3 = key3;
    }

    public LocalDate getKey4() {
        return key4;
    }

    public void setKey4(LocalDate key4) {
        this.key4 = key4;
    }

}
