package nl.mirila.model.management.managers;

import com.google.inject.Inject;
import nl.mirila.model.core.references.Key;
import nl.mirila.model.core.references.Model;
import nl.mirila.model.management.descriptor.ModelDescriptor;
import nl.mirila.model.management.descriptor.ModelDescriptors;
import nl.mirila.model.management.exceptions.EntityAlreadyExistsException;
import nl.mirila.model.management.exceptions.EntityNotFoundException;
import nl.mirila.model.management.parameters.ActionParameters;
import nl.mirila.model.management.query.QueryParameters;
import nl.mirila.model.management.results.Result;
import nl.mirila.model.management.results.Results;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FakePersistenceManager implements EntityPersistenceManager {

    private final Map<Key<? extends Model>, Model> entities = new HashMap<>();
    private final ModelDescriptors descriptors;

    @Inject
    public FakePersistenceManager(ModelDescriptors descriptors) {
        this.descriptors = descriptors;
    }

    @Override
    public <E extends Model> long count(QueryParameters<E> queryParameters) {
        return query(queryParameters).size();
    }

    @Override
    public <E extends Model> Results<E> query(QueryParameters<E> queryParameters) {
        List<E> list = entities.values().stream()
                .filter(queryParameters.getModelClass()::isInstance)
                .map(queryParameters.getModelClass()::cast)
                .toList();
        return Results.of(list);
    }

    @Override
    public <E extends Model> Result<E> read(Key<E> key, ActionParameters<E> parameters) {
        Model rawEntity = entities.get(key);
        if (key.getModelClass().isInstance(rawEntity)) {
            E entity = key.getModelClass().cast(rawEntity);
            return Result.of(entity);
        }
        return Result.empty();
    }

    @Override
    public <E extends Model> Result<E> create(E entity, ActionParameters<E> parameters) {
        ModelDescriptor<E> descriptor = descriptors.getDescriptorForModel(parameters.getModelClass());
        Key<E> key = descriptor.getPrimaryKeyForEntity(entity);
        if (entities.containsKey(key)) {
            throw new EntityAlreadyExistsException(key);
        }
        entities.put(key, entity);
        return Result.of(entity);
    }

    @Override
    public <E extends Model> Result<E> update(E entity, ActionParameters<E> parameters) {
        ModelDescriptor<E> descriptor = descriptors.getDescriptorForModel(parameters.getModelClass());
        Key<E> key = descriptor.getPrimaryKeyForEntity(entity);
        if (entities.containsKey(key)) {
            // replace the existing entity.
            entities.put(key, entity);
        } else {
            throw new EntityNotFoundException(key);
        }
        return Result.of(entity);
    }

    @Override
    public <E extends Model> void delete(Key<E> key, ActionParameters<E> parameters) {
        if (!entities.containsKey(key)) {
            throw new EntityNotFoundException(key);
        }
        entities.remove(key);
    }

    public void clear() {
        entities.clear();
    }

}
