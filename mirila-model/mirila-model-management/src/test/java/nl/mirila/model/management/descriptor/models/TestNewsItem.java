package nl.mirila.model.management.descriptor.models;

import nl.mirila.core.datatype.Id;
import nl.mirila.model.core.annotations.DefaultValue;
import nl.mirila.model.core.annotations.MaxLength;
import nl.mirila.model.core.annotations.ModelInfo;
import nl.mirila.model.core.annotations.Required;
import nl.mirila.model.core.references.Model;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@ModelInfo(
        singular = "TestNewsItem",
        plural = "TestNews",
        primaryKeyFields = {"newsItemId"},
        defaultSortFields = {"timestamp"},
        ownerIdsField = "ownerIds",
        groupIdsField = "groupIds",
        rightsField = "rights",
        rightsCategory = "news"
)
public class TestNewsItem implements Model {

    @Required
    @MaxLength(20)
    private String newsItemId;

    @Required
    @MaxLength((150))
    private String title;

    @Required
    LocalDateTime timestamp;

    @Required
    private Set<Id> ownerIds;

    @Required
    private Set<Id> groupIds;

    @Required
    private int rights;

    @DefaultValue("false")
    Boolean hasReadMore;

    @Required
    private String content;

    public TestNewsItem() {
        this.ownerIds = new HashSet<>();
        this.groupIds = new HashSet<>();
        this.rights = 0;
    }

    public String getNewsItemId() {
        return newsItemId;
    }

    public void setNewsItemId(String newsItemId) {
        this.newsItemId = newsItemId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public Set<Id> getOwnerIds() {
        return ownerIds;
    }

    public void setOwnerIds(Set<Id> ownerIds) {
        this.ownerIds = ownerIds;
    }

    public Set<Id> getGroupIds() {
        return groupIds;
    }

    public void setGroupIds(Set<Id> groupIds) {
        this.groupIds = groupIds;
    }

    public Integer getRights() {
        return rights;
    }

    public void setRights(Integer rights) {
        this.rights = rights;
    }

    public Boolean getHasReadMore() {
        return hasReadMore;
    }

    public void setHasReadMore(Boolean hasReadMore) {
        this.hasReadMore = hasReadMore;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}
