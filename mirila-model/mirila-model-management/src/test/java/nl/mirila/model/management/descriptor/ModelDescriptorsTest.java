package nl.mirila.model.management.descriptor;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.multibindings.Multibinder;
import nl.mirila.core.datatype.Id;
import nl.mirila.model.core.annotations.Validations;
import nl.mirila.model.management.descriptor.models.ModelWithoutModelInfo;
import nl.mirila.model.management.descriptor.models.TestGroup;
import nl.mirila.model.management.descriptor.models.TestNewsItem;
import nl.mirila.model.management.descriptor.models.TestUser;
import nl.mirila.model.management.exceptions.UnknownModelException;
import nl.mirila.security.rights.objects.RightsObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.time.LocalDate;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

@SuppressWarnings("ArraysAsListWithZeroOrOneArgument")
class ModelDescriptorsTest {

    private static final String MODEL_PACKAGE_NAME = "nl.mirila.model.management.descriptor.models";

    private ModelDescriptor<TestUser> userDescriptor;
    private ModelDescriptor<TestGroup> groupDescriptor;
    private ModelDescriptor<TestNewsItem> newsDescriptor;
    private ModelDescriptors descriptors;

    @BeforeEach
    void setUp() {
        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                Multibinder<String> binder = Multibinder.newSetBinder(binder(), String.class, ModelsPackageNames.class);
                binder.addBinding().toInstance(MODEL_PACKAGE_NAME);
            }
        });
        descriptors = injector.getInstance(ModelDescriptors.class);
        userDescriptor = descriptors.getDescriptorForModel(TestUser.class);
        groupDescriptor = descriptors.getDescriptorForModel(TestGroup.class);
        newsDescriptor = descriptors.getDescriptorForModel(TestNewsItem.class);
    }

    @Test
    void testKnownDescriptorByClassName() {
        assertThat(userDescriptor).isNotNull();
        assertThat(groupDescriptor).isNotNull();
        assertThat(newsDescriptor).isNotNull();
    }

    @Test
    void testUnknownDescriptorByClassName() {
        assertThatExceptionOfType(UnknownModelException.class).isThrownBy(() -> {
            descriptors.getDescriptorForModel(ModelWithoutModelInfo.class);
        });
    }

    @Test
    void testSingularNames() {
        assertThat(userDescriptor.getSingular()).isEqualTo("TestUser");
        assertThat(groupDescriptor.getSingular()).isEqualTo("TestGroup");
        assertThat(newsDescriptor.getSingular()).isEqualTo("TestNewsItem");
    }

    @Test
    void testPluralNames() {
        assertThat(userDescriptor.getPlural()).isEqualTo("TestUsers");
        assertThat(groupDescriptor.getPlural()).isEqualTo("TestGroups");
        assertThat(newsDescriptor.getPlural()).isEqualTo("TestNews");
    }

    @Test
    void testKeyFields() {
        assertThat(userDescriptor.getPrimaryKeyFields()).isEqualTo(Arrays.asList("userId"));
        assertThat(groupDescriptor.getPrimaryKeyFields()).isEqualTo(Arrays.asList("groupId"));
        assertThat(newsDescriptor.getPrimaryKeyFields()).isEqualTo(Arrays.asList("newsItemId"));
    }

    @Test
    void testDefaultSortFields() {
        assertThat(userDescriptor.getDefaultSortFields()).isEqualTo(Arrays.asList("lastName", "firstName"));
        assertThat(groupDescriptor.getDefaultSortFields()).isEqualTo(Arrays.asList());
        assertThat(newsDescriptor.getDefaultSortFields()).isEqualTo(Arrays.asList("timestamp"));
    }

    @Test
    void testFields() {
        List<String> expected1 = Arrays.asList("userId",
                                               "firstName",
                                               "lastName",
                                               "emailAddress",
                                               "phoneNumber",
                                               "gender",
                                               "dateOfBirth");
        assertThat(userDescriptor.getFields()).isEqualTo(expected1);
        assertThat(groupDescriptor.getFields()).isEqualTo(Arrays.asList("groupId", "name"));
        List<String> expected2 = Arrays.asList("newsItemId",
                                               "title",
                                               "timestamp",
                                               "ownerIds",
                                               "groupIds",
                                               "rights",
                                               "hasReadMore",
                                               "content");
        assertThat(newsDescriptor.getFields()).isEqualTo(expected2);
    }

    @Test
    void testFieldClasses() {
        Map<String, Class<?>> fieldClasses = userDescriptor.getFieldClasses();
        assertThat(fieldClasses)
                .containsEntry("firstName", String.class)
                .containsEntry("dateOfBirth", LocalDate.class);
    }

    @Test
    void testRequiredFields() {
        assertThat(userDescriptor.getRequiredFields()).containsOnly("userId", "lastName");
        assertThat(groupDescriptor.getRequiredFields()).containsOnly("groupId", "name");

        assertThat(newsDescriptor.getRequiredFields())
                .containsOnly("newsItemId", "title", "timestamp", "ownerIds", "groupIds", "rights", "content");
    }

    @Test
    void testMaxLengths() {
        Map<String, Integer> expected = new HashMap<>();
        expected.put("firstName", 25);
        expected.put("lastName", 50);
        expected.put("emailAddress", 255);
        expected.put("phoneNumber", 25);
        expected.put("gender", 25);

        assertThat(userDescriptor.getMaxLengths()).isEqualTo(expected);
    }

    @Test
    void testFieldValidations() {
        Map<String, Validations> validations = userDescriptor.getFieldValidations();
        assertThat(validations).containsOnlyKeys("emailAddress", "phoneNumber");
    }

    @Test
    void testFieldSetters() {
        Map<String, Method> fieldSetters = userDescriptor.getFieldSetters();
        assertThat(fieldSetters.get("firstName")).isNotNull();
        assertThat(fieldSetters.get("dateOfBirth")).isNotNull();
    }

    @Test
    void testRightsObjectForRightsEntity() {
        TestNewsItem item = new TestNewsItem();
        item.setOwnerIds(Set.of(Id.of("user-1"), Id.of("user-2")));
        item.setGroupIds(Set.of(Id.of("group-1"), Id.of("group-2")));
        item.setRights(0xFF44);

        RightsObject rightsObject = newsDescriptor.getRightsObjectForEntity(item);
        assertThat(rightsObject.getOwnerIds()).containsOnly(Id.of("user-1"), Id.of("user-2"));
        assertThat(rightsObject.getGroupIds()).containsOnly(Id.of("group-1"), Id.of("group-2"));
        assertThat(rightsObject.getRights()).isEqualTo(0xFF44);
    }

    @Test
    void testRightsObjectForNonRightsEntity() {
        TestUser user = new TestUser();
        RightsObject rightsObject = userDescriptor.getRightsObjectForEntity(user);
        assertThat(rightsObject.getOwnerIds()).isEmpty();
        assertThat(rightsObject.getGroupIds()).isEmpty();
        assertThat(rightsObject.getRights()).isEqualTo(0x0044);
    }

}
