package nl.mirila.model.management.hooks;

public abstract class RunnableHook {

    protected final Runnable runner;

    public RunnableHook(Runnable runner) {
        this.runner = runner;
    }

}
