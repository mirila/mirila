package nl.mirila.model.management.hooks;

import com.google.inject.Inject;
import nl.mirila.model.core.references.Key;
import nl.mirila.model.core.references.Model;
import nl.mirila.model.management.parameters.ActionParameters;
import nl.mirila.model.management.query.QueryParameters;
import nl.mirila.model.management.results.Result;
import nl.mirila.model.management.results.Results;

import javax.inject.Named;

public class PrePostHookExample extends RunnableHook implements PreHook, PostHook {

    @Inject
    public PrePostHookExample(@Named("TestRunner") Runnable runner) {
        super(runner);
    }

    @Override
    public <E extends Model> void afterQuery(Results<E> results, QueryParameters<E> queryParameters) {
        runner.run();
    }

    @Override
    public <E extends Model> void afterRead(Key<E> key, Result<E> result, ActionParameters<E> parameters) {
        runner.run();
    }

    @Override
    public <E extends Model> void afterCreate(Result<E> result, ActionParameters<E> parameters) {
        runner.run();
    }

    @Override
    public <E extends Model> void beforeQuery(QueryParameters<E> queryParameters) {
        runner.run();
    }

    @Override
    public <E extends Model> void beforeRead(Key<E> key, ActionParameters<E> parameters) {
        runner.run();
    }

    @Override
    public <E extends Model> void beforeCreate(E entity, ActionParameters<E> parameters) {
        runner.run();
    }

}
