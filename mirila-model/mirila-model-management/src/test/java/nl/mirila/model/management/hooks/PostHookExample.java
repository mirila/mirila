package nl.mirila.model.management.hooks;

import com.google.inject.Inject;
import nl.mirila.model.core.references.Key;
import nl.mirila.model.core.references.Model;
import nl.mirila.model.management.parameters.ActionParameters;
import nl.mirila.model.management.results.Result;

import javax.inject.Named;

public class PostHookExample extends RunnableHook implements PostHook {

    @Inject
    public PostHookExample(@Named("TestRunner") Runnable runner) {
        super(runner);
    }

    @Override
    public <E extends Model> void afterCreate(Result<E> result, ActionParameters<E> parameters) {
        runner.run();
    }

    @Override
    public <E extends Model> void afterUpdate(Result<E> result, ActionParameters<E> parameters) {
        runner.run();
    }

    @Override
    public <E extends Model> void afterDelete(Key<E> key, ActionParameters<E> parameters) {
        runner.run();
    }

}
