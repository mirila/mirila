package nl.mirila.model.management.descriptor.models;

import nl.mirila.model.core.annotations.MaxLength;
import nl.mirila.model.core.annotations.ModelInfo;
import nl.mirila.model.core.annotations.Required;
import nl.mirila.model.core.references.Model;

@ModelInfo(
        singular = "TestGroup",
        plural = "TestGroups",
        primaryKeyFields = {"groupId"}
)
public class TestGroup implements Model {

    @Required
    @MaxLength(20)
    private String groupId;

    @Required
    @MaxLength(50)
    private String name;

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
