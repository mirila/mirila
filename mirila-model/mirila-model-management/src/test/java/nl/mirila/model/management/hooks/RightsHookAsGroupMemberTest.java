package nl.mirila.model.management.hooks;

import com.google.inject.Injector;
import com.google.inject.TypeLiteral;
import nl.mirila.core.datatype.Id;
import nl.mirila.model.core.references.Key;
import nl.mirila.model.management.descriptor.models.TestNewsItem;
import nl.mirila.model.management.exceptions.NoRightsException;
import nl.mirila.model.management.managers.EntityManager;
import nl.mirila.model.management.managers.FakePersistenceManager;
import nl.mirila.model.management.parameters.CommonFlags;
import nl.mirila.model.management.query.QueryParameters;
import nl.mirila.model.management.results.Results;
import nl.mirila.security.auth.core.claims.Claims;
import nl.mirila.security.auth.core.claims.ClaimsBuilder;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.List;

import static org.assertj.core.api.Assertions.*;

class RightsHookAsGroupMemberTest extends RightsHookTestBase {

    private static EntityManager<TestNewsItem> manager;
    private static FakePersistenceManager persistenceManager;
    private static Id otherOwnerId;
    private static Id otherGroupId;
    private static Id currentOwnerId;
    private static Id currentGroupId;
    private static HashMap<String, String> settings;

    @BeforeAll
    static void beforeAll() {
        settings = new HashMap<>();
        otherOwnerId = Id.of(543);
        otherGroupId = Id.of(456);
        currentOwnerId = Id.of(123);
        currentGroupId = Id.of(321);
        Claims claims = ClaimsBuilder.forRealm("test-realm")
                .withSubjectId(currentOwnerId.asString())
                .withGroupIds(currentGroupId.asString())
                .getClaims();
        Injector injector = getInjector(claims, settings);

        //noinspection Convert2Diamond
        manager = injector.getInstance(com.google.inject.Key.get(new TypeLiteral<EntityManager<TestNewsItem>>(){}));
        persistenceManager = injector.getInstance(FakePersistenceManager.class);
    }

    @BeforeEach
    void setUp() {
        persistenceManager.clear();
        settings.clear();
    }

    private void fillStorage() {
        persistenceManager.create(getTestNewsItem(Id.of(100), otherOwnerId, otherGroupId, 0x7074));
        persistenceManager.create(getTestNewsItem(Id.of(101), otherOwnerId, otherGroupId, 0x7044));
        persistenceManager.create(getTestNewsItem(Id.of(102), otherOwnerId, otherGroupId, 0x7040));
        persistenceManager.create(getTestNewsItem(Id.of(103), otherOwnerId, otherGroupId, 0x4400));
        persistenceManager.create(getTestNewsItem(Id.of(104), otherOwnerId, currentGroupId, 0x4400));
        persistenceManager.create(getTestNewsItem(Id.of(105), otherOwnerId, currentGroupId, 0x4000));
        persistenceManager.create(getTestNewsItem(Id.of(106), otherOwnerId, currentGroupId, 0x0000));
    }

    @Test
    void testQueryWithRightsCheck() {
        fillStorage();

        QueryParameters<TestNewsItem> queryParameters = QueryParameters.on(TestNewsItem.class);
        Results<TestNewsItem> results = manager.query(queryParameters);
        assertThat(results.isEmpty()).isFalse();
        assertThat(results.get().size()).isEqualTo(4);

        List<Id> foundIds = results.get().stream()
                .map(TestNewsItem::getNewsItemId)
                .map(Id::of)
                .toList();
        assertThat(foundIds).containsExactlyInAnyOrder(Id.of(100), Id.of(101), Id.of(102), Id.of(104));
    }

    @Test
    void testQueryWithoutRightsCheck() {
        fillStorage();
        QueryParameters<TestNewsItem> queryParameters = QueryParameters
                .on(TestNewsItem.class)
                .withFlag(CommonFlags.SKIP_RIGHTS_CHECK);
        Results<TestNewsItem> results = manager.query(queryParameters);
        assertThat(results.isEmpty()).isFalse();
        assertThat(results.get().size()).isEqualTo(7);
    }

    @Test
    void testReadItemGroupMembersReadable() {
        TestNewsItem item = getTestNewsItem(Id.of(100), otherOwnerId, currentGroupId, 0x0400);
        persistenceManager.create(item);

        Key<TestNewsItem> itemKey = Key.of(TestNewsItem.class, item.getNewsItemId());
        assertThatNoException().isThrownBy(() -> {
            manager.read(itemKey);
        });
    }

    @Test
    void testReadItemNotGroupMembersReadable() {
        TestNewsItem item = getTestNewsItem(Id.of(100), otherOwnerId, otherGroupId, 0x0400);
        persistenceManager.create(item);

        Key<TestNewsItem> itemKey = Key.of(TestNewsItem.class, item.getNewsItemId());
        assertThatThrownBy(() -> {
            manager.read(itemKey);
        }).isInstanceOf(NoRightsException.class);
    }

    @Test
    void testUpdateGroupMemberUpdatableItem() {
        TestNewsItem originalItem = getTestNewsItem(Id.of(100), currentOwnerId, currentGroupId, 0x0700);
        TestNewsItem updatedItem = getTestNewsItem(Id.of(100), currentOwnerId, otherGroupId, 0x0700);
        persistenceManager.create(originalItem);

        assertThatNoException().isThrownBy(() -> {
            manager.update(updatedItem);
        });
    }

    @Test
    void testUpdateNotGroupMemberUpdatableItem() {
        TestNewsItem originalItem = getTestNewsItem(Id.of(100), otherOwnerId, otherGroupId, 0x0700);
        TestNewsItem updatedItem = getTestNewsItem(Id.of(100), otherOwnerId, currentGroupId, 0x0700);
        persistenceManager.create(originalItem);

        assertThatThrownBy(() -> {
            manager.update(updatedItem);
        }).isInstanceOf(NoRightsException.class);
    }

    @Test
    void testDeleteGroupMemberDeletableItem() {
        TestNewsItem item = getTestNewsItem(Id.of(101), otherOwnerId, currentGroupId, 0x0700);
        Key<TestNewsItem> itemKey = Key.of(TestNewsItem.class, item.getNewsItemId());
        persistenceManager.create(item);

        assertThatNoException().isThrownBy(() -> {
            manager.delete(itemKey);
        });
    }

    @Test
    void testDeleteNotGroupMemberDeletableItem() {
        TestNewsItem item = getTestNewsItem(Id.of(101), otherOwnerId, otherGroupId, 0x0700);
        Key<TestNewsItem> itemKey = Key.of(TestNewsItem.class, item.getNewsItemId());
        persistenceManager.create(item);

        assertThatThrownBy(() -> {
            manager.delete(itemKey);
        }).isInstanceOf(NoRightsException.class);
    }

}
