package nl.mirila.model.core.exceptions;

import nl.mirila.core.exceptions.MirilaException;

public class InvalidReferenceException extends MirilaException {

    public InvalidReferenceException(String reference) {
        super(String.format("The given reference '%s' is invalid.", reference));
    }

}
