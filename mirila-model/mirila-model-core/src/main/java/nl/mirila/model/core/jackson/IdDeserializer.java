package nl.mirila.model.core.jackson;


import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import nl.mirila.core.datatype.Id;

import java.io.IOException;

/**
 * A {@link StdDeserializer} that deserializes {@link Id}.
 */
public class IdDeserializer extends StdDeserializer<Id> {

    /**
     * Initialize a new instance.
     */
    public IdDeserializer() {
        super(Id.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Id deserialize(JsonParser parser, DeserializationContext context) throws IOException {
        JsonNode node = parser.getCodec().readTree(parser);
        return Id.of(node.asText());
    }

}
