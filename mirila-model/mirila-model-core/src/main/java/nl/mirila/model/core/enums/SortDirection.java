package nl.mirila.model.core.enums;

/**
 * The sort direction.
 */
public enum SortDirection {

    ASCENDING,
    DESCENDING

}
