 package nl.mirila.model.core.references;

 import nl.mirila.model.core.exceptions.InvalidReferenceException;
 import org.apache.commons.lang3.StringUtils;

 import java.util.Objects;

 /**
  * A reference to the field of a model.
  */
public class Reference {

    private static final String REGEX = "^[\\w\\-]+\\.[\\w\\-]+$";

    private String model;
    private String field;

     /**
      * Initialize a new empty instance.
      */
    public Reference() {
        this.model = "";
        this.field = "";
    }

     /**
      * Initialize a new instance with the given reference.
      */
    public Reference(String reference) {
        if (!isValid(reference)) {
            throw new InvalidReferenceException(reference);
        }
        String[] parts = reference.split("\\.", 2);
        this.model = parts[0];
        this.field = parts[1];
    }

     /**
      * Initialize a new instance with the given referenced model and field.
      */
    public Reference(String model, String field) {
        this.model = model;
        this.field = field;
    }

     /**
      * Return the model.
      */
    public String getModel() {
        return model;
    }

     /**
      * Set the model.
      */
    public void setModel(String model) {
        Objects.requireNonNull(model);
        this.model = model;
    }

     /**
      * Return the field.
      */
    public String getField() {
        return field;
    }

     /**
      * Set the field.
      */
    public void setField(String field) {
        Objects.requireNonNull(field);
        this.field = field;
    }

     /**
      * Return true if the given reference is valid.
      */
    public static boolean isValid(String reference) {
        return ((!StringUtils.isBlank(reference)) && (reference.matches(REGEX)));
    }

}
