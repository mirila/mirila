package nl.mirila.model.core.references;

import com.google.common.base.Objects;
import nl.mirila.core.converters.StringConverter;
import nl.mirila.model.core.exceptions.InvalidKeyException;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * The Key represents the values of entity fields to together compose a data key, such as a primary key, or index key.
 */
public class Key<E> {

    private static final String PART_DELIMITER = ".";
    private static final String MODEL_DELIMITER = "::";

    private final Class<E> modelClass;
    private final List<Object> parts;

    /**
     * Initializes the key.
     * <p>
     * The parts may only include null or supported classes. These are similar to the classes supported by the
     * {@link StringConverter}.
     */
    private Key(Class<E> modelClass, List<Object> parts) {
        this.modelClass = modelClass;
        this.parts = parts;
        boolean validClasses = parts.stream()
                .filter(java.util.Objects::nonNull)
                .map(Object::getClass)
                .allMatch(StringConverter::isSupportedClass);
        if (!validClasses) {
            throw new InvalidKeyException(this);
        }
    }

    /**
     * Returns the parts of the key.
     */
    public List<Object> getParts() {
        return parts;
    }

    /**
     * Returns the parts of the key, wrapped in an {@link Optional}.
     */
    public Optional<Object> getPart(int index) {
        return getPart(index, Object.class);
    }

    /**
     * Returns the parts of the key with if it is an instance of the expected class, wrapped in an {@link Optional}.
     */
    public <F> Optional<F> getPart(int index, Class<F> expectedClass) {
        return ((index >= 0) && (index < parts.size()) && (expectedClass.isInstance(parts.get(index))))
                ? Optional.of(expectedClass.cast(parts.get(index)))
                : Optional.empty();
    }

    /**
     * Returns the class of the model for this key, wrapped in an {@link Optional}.
     */
    public Class<E> getModelClass() {
        return modelClass;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }
        Key<?> key = (Key<?>) obj;
        return ((Objects.equal(parts, key.parts)) &&
                (Objects.equal(modelClass, key.modelClass)));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hashCode(parts, modelClass);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        String partsStr = parts.stream()
                .map(Object::toString)
                .collect(Collectors.joining(PART_DELIMITER));
        return (modelClass == null)
                ? partsStr
                : String.format("%s%s%s", modelClass.getSimpleName(), MODEL_DELIMITER, partsStr);
    }

    /**
     * Returns a new key with the same parts, and with the given model class.
     * <p>
     * @param modelClass The model class to use.
     * @return The new key.
     */
    public static <T> Key<T> of(Class<T> modelClass, Object... parts) {
        return of(modelClass, Arrays.asList(parts));
    }

    /**
     * Returns a new key with the same parts, and with the given model class.
     * <p>
     * @param modelClass The model class to use.
     * @return The new key.
     */
    public static <T> Key<T> of(Class<T> modelClass, List<Object> parts) {
        return new Key<>(modelClass, parts);
    }

}
