package nl.mirila.model.core.exceptions;

import nl.mirila.core.exceptions.MirilaException;
import nl.mirila.model.core.references.Key;

public class InvalidKeyException extends MirilaException {

    public InvalidKeyException(Key<?> key) {
        super(String.format("The given key '%s' is invalid.", key));
    }

}
