package nl.mirila.model.core.jackson;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import nl.mirila.core.converters.StringConverter;
import nl.mirila.core.datatype.Id;
import nl.mirila.core.exceptions.InvalidStringConversionException;
import nl.mirila.core.exceptions.UnsupportedStringConversionTypeException;

import java.io.IOException;

/**
 * A {@link StdSerializer} that serializes {@link Id}.
 */
public class IdSerializer extends StdSerializer<Id> {

    private final IdType idType;

    /**
     * Initialize a new instance.
     */
    public IdSerializer(IdType idType) {
        super(Id.class);
        this.idType = idType;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void serialize(Id id, JsonGenerator jsonGenerator, SerializerProvider serializerProvider)
            throws IOException {
        if (id == null) {
            jsonGenerator.writeNull();
        } else {
            if (idType == IdType.INTEGER) {
                writeAsNumber(jsonGenerator, id);
            } else {
                writeAsString(jsonGenerator, id);
            }
        }
    }

    /**
     * Write the given {@link Id} as a number.
     * <p>
     * @param jsonGenerator The json generator.
     * @param id The id.
     */
    private void writeAsNumber(JsonGenerator jsonGenerator, Id id) throws IOException {
        Integer value;
        try {
            value = StringConverter.convert(Integer.class, id.toString());
        } catch (InvalidStringConversionException | UnsupportedStringConversionTypeException e) {
            value = null;
        }
        if (value == null) {
            jsonGenerator.writeString(id.toString());
        } else {
            jsonGenerator.writeNumber(value);
        }
    }

    /**
     * Write the given {@link Id} as a string.
     * <p>
     * @param jsonGenerator The json generator.
     * @param id The id.
     */
    private void writeAsString(JsonGenerator jsonGenerator, Id id) throws IOException {
        jsonGenerator.writeString(id.toString());
    }

}
