package nl.mirila.model.core.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks a pojo as a model.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface ModelInfo {

    /**
     * Return the singular name of the model.
     */
    String singular();

    /**
     * Return the plural name of the model.
     */
    String plural();

    /**
     * Return the fields that compose the primary key.
     */
    String[] primaryKeyFields() default {};

    /**
     * Return the default fields to sort on.
     */
    String[] defaultSortFields() default {};

    /**
     * Return the field name that define the owner ids of the model instances.
     */
    String ownerIdsField() default "";

    /**
     * Return the field name that define the group ids of the model instances.
     */
    String groupIdsField() default "";

    /**
     * Return the field name that define the rights code of the model instances.
     */
    String rightsField() default "";

    /**
     * Return the category of the rights settings.
     */
    String rightsCategory() default "";

}
