package nl.mirila.model.core.jackson;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import nl.mirila.core.datatype.Json;

import java.io.IOException;

/**
 * A {@link StdSerializer} that serializes {@link Json}.
 */
public class JsonSerializer extends StdSerializer<Json> {

    /**
     * Initialize a new instance.
     */
    public JsonSerializer() {
        super(Json.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void serialize(Json json, JsonGenerator jsonGenerator, SerializerProvider serializerProvider)
            throws IOException {
        if (json == null) {
            jsonGenerator.writeNull();
        } else {
            String prefix = (jsonGenerator.getOutputBuffered() > 0) ? ":" : "";
            jsonGenerator.writeRaw(prefix + json.get());
        }
    }

}
