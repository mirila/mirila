package nl.mirila.model.core.enums;

/**
 * A representation of validation types.
 */
public enum ValidationType {

    // Integer validations
    MIN,
    MAX,

    // String validations
    REGEX,
    IS_NUMERIC,

    // Date and time validations
    IN_THE_PAST,
    IN_THE_FUTURE,
    IN_THE_FUTURE_IF_NEW

}
