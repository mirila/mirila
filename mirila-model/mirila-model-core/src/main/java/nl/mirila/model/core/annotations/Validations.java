package nl.mirila.model.core.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Describes basic validations for the field of a model.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Validations {

    /**
     * Return a list of {@link Validation}.
     */
    Validation[] value() default {};

}
