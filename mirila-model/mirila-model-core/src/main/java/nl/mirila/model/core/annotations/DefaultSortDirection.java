package nl.mirila.model.core.annotations;

import nl.mirila.model.core.enums.SortDirection;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks the default sort direction of a field.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface DefaultSortDirection {

    SortDirection value() default SortDirection.ASCENDING;

}
