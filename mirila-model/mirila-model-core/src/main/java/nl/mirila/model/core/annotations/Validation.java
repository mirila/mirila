package nl.mirila.model.core.annotations;

import nl.mirila.model.core.enums.ValidationType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Describes a validation for the field of a model.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.ANNOTATION_TYPE)
public @interface Validation {

    /**
     * Return the type of validation.
     */
    ValidationType type();

    /**
     * Return the related value of the validation.
     */
    String value();

}
