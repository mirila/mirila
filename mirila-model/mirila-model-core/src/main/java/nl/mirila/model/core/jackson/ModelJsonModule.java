package nl.mirila.model.core.jackson;

import com.fasterxml.jackson.databind.module.SimpleModule;
import nl.mirila.core.datatype.Id;

public class ModelJsonModule extends SimpleModule {

    public ModelJsonModule(IdType idType) {
        addSerializer(new JsonSerializer());
        addSerializer(new IdSerializer(idType));
        addDeserializer(Id.class, new IdDeserializer());
    }

}
