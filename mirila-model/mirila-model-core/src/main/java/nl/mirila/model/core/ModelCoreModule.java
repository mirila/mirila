package nl.mirila.model.core;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.AbstractModule;
import nl.mirila.model.core.jackson.ObjectMapperProvider;

public class ModelCoreModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(ObjectMapper.class).toProvider(ObjectMapperProvider.class);
    }

}
