package nl.mirila.model.core.jackson;

import com.google.inject.Inject;
import nl.mirila.core.datatype.Id;

/**
 * The type of the {@link Id} that is used.
 */
public enum IdType {

    STRING(String.class),
    INTEGER(Integer.class);

    private final Class<?> idClass;

    /**
     * Initialize a new instance.
     */
    IdType(Class<?> idClass) {
        this.idClass = idClass;
    }


    /**
     * Return the class of the used id type.
     */
    Class<?> get() {
        return idClass;
    }

    /**
     * A holder for the IdType, which using injection to detect the systems IdType.
     */
    public static class IdTypeHolder {

        @Inject(optional = true)
        IdType idType;

        /**
         * Initialize a new instance.
         */
        public IdTypeHolder() {
        }

        /**
         * Initialize a new instance with the given {@link IdType}.
         */
        public IdTypeHolder(IdType idType) {
            this.idType = idType;
        }

        /**
         * Return the  {@link IdType}.
         */
        public IdType getIdType() {
            return idType;
        }

    }

}
