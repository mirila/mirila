package nl.mirila.model.core.references;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class ReferenceTest {

    @Test
    void testValidReferences() {
        assertThat(Reference.isValid("Example1.Field2")).isTrue();
        assertThat(Reference.isValid("Example_1.Field_2")).isTrue();

        assertThat(Reference.isValid(null)).isFalse();
        assertThat(Reference.isValid("")).isFalse();
        assertThat(Reference.isValid("Example1.Field2.Field3")).isFalse();
    }

}
