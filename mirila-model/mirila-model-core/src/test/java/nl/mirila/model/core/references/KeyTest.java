package nl.mirila.model.core.references;

import nl.mirila.model.core.references.models.TestKeyModel;
import nl.mirila.model.core.references.models.TestUser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class KeyTest {

    private Key<TestKeyModel> key1;
    private Key<TestKeyModel> key2;
    private Key<Object> key3;
    private Key<TestUser> key4;

    @BeforeEach
    void setUp() {
        key1 = Key.of(TestKeyModel.class, "str1", "str2", 123, true);
        key2 = Key.of(TestKeyModel.class,  Arrays.asList("str1", "str2", 123, true));
        key3 = Key.of(Object.class, "strA", "strB", 12, false);
        key4 = Key.of(TestUser.class, "strA", "strB", 12, "false");
    }

    @Test
    void testGetParts() {
        List<Object> parts = key1.getParts();
        assertThat(parts).isEqualTo(Arrays.asList("str1", "str2", 123, true));
    }

    @Test
    void testGetPart() {
        assertThat(key1.getPart(0).orElse("")).isEqualTo("str1");
        assertThat(key1.getPart(1).orElse("")).isEqualTo("str2");
        assertThat(key1.getPart(2).orElse("")).isEqualTo(123);
        assertThat(key1.getPart(3).orElse("")).isEqualTo(true);
    }

    @Test
    void testGetModelClass() {
        assertThat(key1.getModelClass()).isEqualTo(TestKeyModel.class);
        assertThat(key2.getModelClass()).isEqualTo(TestKeyModel.class);
        assertThat(key3.getModelClass()).isEqualTo(Object.class);
        assertThat(key4.getModelClass()).isEqualTo(TestUser.class);
    }

    @Test
    void testEquals() {
        assertThat(key1).isEqualTo(key1);
        assertThat(key2).isEqualTo(key1);
        assertThat(key1).isNotNull();
        assertThat(key3).isNotEqualTo(key1);
    }

    @Test
    void testHashCode() {
        assertThat(key2.hashCode()).hasSameHashCodeAs(key1.hashCode());
        assertThat(key3.hashCode()).isNotEqualTo(key1.hashCode());
        assertThat(key4.hashCode()).isNotEqualTo(key2.hashCode());
        assertThat(key4.hashCode()).isNotEqualTo(key3.hashCode());
    }

    @Test
    void testToString() {
        assertThat(key1).hasToString("TestKeyModel::str1.str2.123.true");
        assertThat(key2).hasToString("TestKeyModel::str1.str2.123.true");
        assertThat(key3).hasToString("Object::strA.strB.12.false");
        assertThat(key4).hasToString("TestUser::strA.strB.12.false");
    }

}
