package nl.mirila.model.core.references.models;

import nl.mirila.core.datatype.Json;
import nl.mirila.model.core.references.Model;

public class TestJsonModel implements Model {

    private String testName;
    private Json testJson;

    private TestJsonModel(String testName) {
        this.testName = testName;
    }

    /**
     * Return the testName.
     */
    public String getTestName() {
        return testName;
    }

    /**
     * Set the given testName and return this instance for chaining purposes.
     */
    public static TestJsonModel withTestName(String testName) {
        return new TestJsonModel(testName);
    }

    /**
     * Return the testJson.
     */
    public Json getTestJson() {
        return testJson;
    }

    /**
     * Set the given testJson and return this instance for chaining purposes.
     */
    public TestJsonModel withTestJson(Json testJson) {
        this.testJson = testJson;
        return this;
    }

}
