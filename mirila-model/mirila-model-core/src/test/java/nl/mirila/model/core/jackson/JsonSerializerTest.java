package nl.mirila.model.core.jackson;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import nl.mirila.core.datatype.Json;
import nl.mirila.model.core.references.models.TestJsonModel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class JsonSerializerTest {

    private ObjectMapper mapper;

    @BeforeEach
    void setUp() {
        ObjectMapperProvider provider = new ObjectMapperProvider(new IdType.IdTypeHolder(IdType.INTEGER));
        mapper = provider.get();
    }

    @Test
    void testWithModelNullJson() throws JsonProcessingException {
        TestJsonModel testModel = TestJsonModel.withTestName("testWithNull").withTestJson(null);
        String json = mapper.writeValueAsString(testModel);
        assertThat(json).isEqualTo("{\"testName\":\"testWithNull\",\"testJson\":null}");
    }

    @Test
    void testWithModelValidJson() throws JsonProcessingException {
        Json testJson = Json.of("{\"testKey\":\"testValue\"}");
        TestJsonModel testModel = TestJsonModel.withTestName("testWithNull").withTestJson(testJson);
        String json = mapper.writeValueAsString(testModel);
        assertThat(json).isEqualTo("{\"testName\":\"testWithNull\",\"testJson\":{\"testKey\":\"testValue\"}}");
    }

    @Test
    void testWithJson() throws JsonProcessingException {
        Json testJson = Json.of("{\"testKey\":\"testValue\"}");
        String json = mapper.writeValueAsString(testJson);
        assertThat(json).isEqualTo("{\"testKey\":\"testValue\"}");
    }

}
