package nl.mirila.model.reader.utils;

import nl.mirila.core.exceptions.MirilaException;
import nl.mirila.model.reader.ModelFileReader;
import nl.mirila.model.reader.models.FieldDefinition;
import nl.mirila.model.reader.models.ModelDefinition;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

class ModelExpanderTest {


    @Test
    void testGetExpandedModels() {
        Map<String, ModelDefinition> originalModels = loadExampleModels("Example1", "Example2", "Example3", "Example4");
        Map<String, ModelDefinition> expandedModels = ModelExpander.getInstance().getExpandedModels(originalModels);

        assertThat(originalModels).hasSize(4);
        assertThat(expandedModels).hasSize(4);

        assertThat(expandedModels)
                .doesNotContainEntry("Example1", originalModels.get("Example1"))
                .containsEntry("Example2", originalModels.get("Example2"))
                .doesNotContainEntry("Example3", originalModels.get("Example3"))
                .doesNotContainEntry("Example4", originalModels.get("Example4"));

        assertThat(expandedModels)
                .containsKey("Example1")
                .containsKey("Example2")
                .containsKey("Example3")
                .containsKey("Example4");

        assertThatExceptionOfType(NoSuchElementException.class).isThrownBy(() -> {
            originalModels.get("Example1").getField("mixinField1");
        });

        assertThat(expandedModels.get("Example1").getField("mixinField1")).isNotNull();

        FieldDefinition mixinField = expandedModels.get("Example1").getField("mixinField1");
        assertThat(mixinField.getParentFieldDefinition()).isPresent();

        assertThat(mixinField.getModel().getSingularName()).isEqualTo("Example1");
        assertThat(mixinField.getParentFieldDefinition().get().getModel().getSingularName()).isEqualTo("Example2");

        assertThat(originalModels.get("Example1").getMixins()).hasSize(2);
        assertThat(expandedModels.get("Example1").getMixins()).hasSize(1);

        assertThat(expandedModels.get("Example2").isAbstract()).isTrue();
        assertThat(expandedModels.get("Example3").isAbstract()).isFalse();

        assertThat(expandedModels.get("Example1").isStorable()).isTrue();
        assertThat(expandedModels.get("Example3").isStorable()).isFalse();

        assertThat(originalModels.get("Example1").getRightsFields()).isEmpty();
        assertThat(originalModels.get("Example2").getRightsFields()).isEmpty();
        assertThat(originalModels.get("Example3").getRightsFields()).isEmpty();
        assertThat(originalModels.get("Example4").getRightsFields()).isPresent();
        assertThat(expandedModels.get("Example1").getRightsFields()).isEmpty();
        assertThat(expandedModels.get("Example2").getRightsFields()).isEmpty();
        assertThat(expandedModels.get("Example3").getRightsFields()).isEmpty();
        assertThat(expandedModels.get("Example4").getRightsFields()).isPresent();

        assertThat(originalModels.get("Example4").getMixins()).hasSize(1);
        assertThat(originalModels.get("Example4").getFields()).hasSize(4);
        assertThat(expandedModels.get("Example4").getFields()).hasSize(4);
    }

    @Test
    void testCircularModelException() {
        Map<String, ModelDefinition> originalModels = loadExampleModels("CircularErrorExample1",
                                                                        "CircularErrorExample2");
        assertThatExceptionOfType(MirilaException.class).isThrownBy(() -> {
            ModelExpander.getInstance().getExpandedModels(originalModels);
        });
    }


    @Test
    void testUnknownMixinsException() {
        Map<String, ModelDefinition> originalModels = loadExampleModels("Example1");
        assertThatExceptionOfType(MirilaException.class).isThrownBy(() -> {
            ModelExpander.getInstance().getExpandedModels(originalModels);
        });
    }

    @Test
    void testDuplicateMixinsException() {
        Map<String, ModelDefinition> originalModels = loadExampleModels("Example1",
                                                                        "Example2",
                                                                        "Example3",
                                                                        "DuplicateMixinExample");
        assertThatExceptionOfType(MirilaException.class).isThrownBy(() -> {
            ModelExpander.getInstance().getExpandedModels(originalModels);
        });
    }


    private Map<String, ModelDefinition> loadExampleModels(String... fileNames) {
        return Stream.of(fileNames)
                .map(this::getModelDefinitionFromExampleFile)
                .collect(Collectors.toMap(ModelDefinition::getSingularName, Function.identity()));
    }


    private ModelDefinition getModelDefinitionFromExampleFile(String fileName) {
        Path yamlFilePath = Paths.get("src", "test", "resources", "examples", fileName + ".yaml");
        ModelFileReader reader = new ModelFileReader();
        Optional<ModelDefinition> optional = reader.readModelDefinition(yamlFilePath);
        return optional.orElseThrow();
    }

}
