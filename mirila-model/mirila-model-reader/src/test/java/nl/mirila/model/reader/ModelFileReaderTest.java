package nl.mirila.model.reader;

import nl.mirila.model.reader.models.ModelDefinition;
import nl.mirila.model.reader.models.RightsDefinition;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

class ModelFileReaderTest {

    private final ModelDefinition model1;
    private final ModelDefinition model2;
    private final ModelDefinition model3;
    private final ModelDefinition model4;

    public ModelFileReaderTest() {
        model1 = getModelDefinitionFromExampleFile("Example1.yaml");
        model2 = getModelDefinitionFromExampleFile("Example2.yaml");
        model3 = getModelDefinitionFromExampleFile("Example3.yaml");
        model4 = getModelDefinitionFromExampleFile("Example4.yaml");
    }

    private ModelDefinition getModelDefinitionFromExampleFile(String fileName) {
        Path yamlFilePath = Paths.get("src", "test", "resources", "examples", fileName);
        ModelFileReader reader = new ModelFileReader();
        Optional<ModelDefinition> optional = reader.readModelDefinition(yamlFilePath);
        return optional.orElseThrow();
    }

    @Test
    void testSingularName() {
        assertThat(model1.getSingularName()).isEqualTo("Example1");
        assertThat(model2.getSingularName()).isEqualTo("Example2");
    }

    @Test
    void testPluralName() {
        assertThat(model1.getPluralName()).isEqualTo("Example1s");
        assertThat(model2.getPluralName()).isNull();
    }

    @Test
    void testIsAbstract() {
        assertThat(model1.isAbstract()).isFalse();
        assertThat(model2.isAbstract()).isTrue();
    }

    @Test
    void testIsStorable() {
        assertThat(model1.isStorable()).isTrue();
        assertThat(model2.isStorable()).isTrue();
        assertThat(model3.isStorable()).isFalse();
    }

    @Test
    void testDescriptions() {
        assertThat(model1.getDescription()).isEqualTo("This is an example of a model definition.");
        assertThat(model2.getDescription()).isNull();
    }

    @Test
    void testMixins() {
        assertThat(model1.getMixins()).isEqualTo(Arrays.asList("Example2", "Example3"));
        assertThat(model2.getMixins()).isEqualTo(Collections.emptyList());
    }

    @Test
    void testRightsFields() {
        assertThat(model1.getRightsFields()).isEmpty();
        assertThat(model2.getRightsFields()).isEmpty();
        assertThat(model3.getRightsFields()).isEmpty();
        assertThat(model4.getRightsFields()).isPresent();

        RightsDefinition rightsFields = model4.getRightsFields().get();
        assertThat(rightsFields.getOwnerIdsFieldName()).isEqualTo("owner-ids");
        assertThat(rightsFields.getGroupsIdsFieldName()).isEqualTo("group-ids");
        assertThat(rightsFields.getRightsFieldName()).isEqualTo("rights");
        assertThat(rightsFields.getRightsCategory()).isEqualTo("examples");
    }

    @Test
    void testFieldsCount() {
        assertThat(model1.getFields()).hasSize(2);
        assertThat(model2.getFields()).hasSize(3);
    }

    @Test
    void testFieldName() {
        assertThat(model1.getFieldNames()).containsOnly("field1", "field2");
        assertThat(model2.getFieldNames()).containsOnly("field1", "field2", "mixinField1");
    }

    @Test
    void testFieldType() {
        assertThat(model3.getField("field1").getType()).isEqualTo("id");
        assertThat(model3.getField("field2").getType()).isEqualTo("string");
        assertThat(model3.getField("field3").getType()).isEqualTo("text");
        assertThat(model3.getField("field4").getType()).isEqualTo("integer");
        assertThat(model3.getField("field5").getType()).isEqualTo("float");
        assertThat(model3.getField("field6").getType()).isEqualTo("boolean");
        assertThat(model3.getField("field7").getType()).isEqualTo("date");
        assertThat(model3.getField("field8").getType()).isEqualTo("time");
        assertThat(model3.getField("field9").getType()).isEqualTo("datetime");
        assertThat(model3.getField("field10").getType()).isEqualTo("list of string");
        assertThat(model3.getField("field11").getType()).isEqualTo("list of integer");
        assertThat(model3.getField("field12").getType()).isEqualTo("map of string, integer");
        assertThat(model3.getField("field13").getType()).isEqualTo("map of string, date");
        assertThat(model3.getField("field14").getType()).isEqualTo("reference to Example1");
        assertThat(model3.getField("field15").getType()).isEqualTo("map of string, list of string");
        assertThat(model3.getField("field17").getType()).isEqualTo("json");
        assertThat(model3.getField("field18").getType()).isEqualTo("long");
        assertThat(model3.getField("field19").getType()).isEqualTo("timestamp");

        String veryLongType = "map of string, map of string, reference to Example2";
        assertThat(model3.getField("field16").getType()).isEqualTo(veryLongType);
    }

    @Test
    void testFieldDefaultValue() {
        assertThat(model3.getField("field1").getDefaultValue()).isNull();
        assertThat(model3.getField("field2").getDefaultValue()).isEqualTo("Hello");
        assertThat(model3.getField("field4").getDefaultValue()).isEqualTo(12);
        assertThat(model3.getField("field5").getDefaultValue()).isEqualTo(0.0);
        assertThat(model3.getField("field6").getDefaultValue()).isEqualTo(true);
        assertThat(model3.getField("field7").getDefaultValue()).isEqualTo("2020-05-16");
        assertThat(model3.getField("field8").getDefaultValue()).isEqualTo("21:49");
        assertThat(model3.getField("field9").getDefaultValue()).isEqualTo("2020-05-16 21:49:13");
        assertThat(model3.getField("field10").getDefaultValue()).isEqualTo(Collections.emptyList());
        assertThat(model3.getField("field12").getDefaultValue()).isEqualTo(Collections.emptyMap());

        assertThat(model1.getField("field2").getDefaultValue()).isEqualTo("hello");
        assertThat(model4.getField("field2").getDefaultValue()).isEqualTo("hello world");
    }

    @Test
    void testFieldOptions() {
        assertThat(model3.getField("field1").getOptions()).isNull();
        assertThat(model3.getField("field10").getOptions()).isEqualTo(Arrays.asList("option1", "option2"));
    }

    @Test
    void testFieldDescription() {
        assertThat(model3.getField("field2").getDescription()).isEqualTo("This is a field description");
    }

    @Test
    void testFieldIsRequired() {
        assertThat(model3.getField("field1").isRequired()).isFalse();
        assertThat(model3.getField("field2").isRequired()).isFalse();
        assertThat(model3.getField("field3").isRequired()).isTrue();
    }

    @Test
    void testFieldIsAutoGenerated() {
        assertThat(model3.getField("field1").isAutoGenerated()).isTrue();
        assertThat(model3.getField("field2").isAutoGenerated()).isFalse();
        assertThat(model3.getField("field3").isAutoGenerated()).isFalse();
    }

    @Test
    void testFieldIsReadOnly() {
        assertThat(model3.getField("field4").isReadOnly()).isFalse();
        assertThat(model3.getField("field5").isReadOnly()).isFalse();
        assertThat(model3.getField("field6").isReadOnly()).isTrue();
    }

    @Test
    void testFieldIsServerOnly() {
        assertThat(model3.getField("field7").isServerOnly()).isFalse();
        assertThat(model3.getField("field8").isServerOnly()).isFalse();
        assertThat(model3.getField("field9").isServerOnly()).isTrue();
    }

    @Test
    void testFieldFormatting() {
        assertThat(model3.getField("field2").getFormatting()).isNull();
        assertThat(model3.getField("field3").getFormatting()).isEqualTo("markdown");
    }

    @Test
    void testFieldMaxLength() {
        assertThat(model3.getField("field2").getMaxLength()).isEqualTo(80);
        assertThat(model3.getField("field3").getMaxLength()).isZero();
    }

    @Test
    void testFieldRegexValue() {
        assertThat(model3.getField("field2").getRegex()).isEqualTo("^.+@.+$");
        assertThat(model3.getField("field3").getRegex()).isNull();
    }

    @Test
    void testFieldMinValue() {
        assertThat(model3.getField("field4").getMinValue()).isEqualTo(0);
        assertThat(model3.getField("field5").getMinValue()).isNull();
    }

    @Test
    void testFieldMaxValue() {
        assertThat(model3.getField("field4").getMaxValue()).isEqualTo(100);
        assertThat(model3.getField("field5").getMaxValue()).isNull();
    }

}
