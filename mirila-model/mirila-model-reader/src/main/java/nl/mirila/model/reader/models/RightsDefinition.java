package nl.mirila.model.reader.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * A representation of a rights definition, within the model definitions.
 */
public class RightsDefinition {

    @JsonProperty("owner-ids")
    private String ownerIdsFieldName;

    @JsonProperty("group-ids")
    private String groupsIdsFieldName;

    @JsonProperty("rights")
    private String rightsFieldName;

    @JsonProperty("category")
    private String rightsCategory;

    /**
     * Return the field name that refers to the owner id.
     */
    public String getOwnerIdsFieldName() {
        return ownerIdsFieldName;
    }

    /**
     * Return the field name that refers to the group ids.
     */
    public String getGroupsIdsFieldName() {
        return groupsIdsFieldName;
    }

    /**
     * Return the field name that refers to the rights code.
     */
    public String getRightsFieldName() {
        return rightsFieldName;
    }

    /**
     * Return the field name that refers to the rights category.
     */
    public String getRightsCategory() {
        return rightsCategory;
    }

}
