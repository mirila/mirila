package nl.mirila.model.reader.utils;

import nl.mirila.model.core.references.Reference;
import nl.mirila.model.reader.exceptions.*;
import nl.mirila.model.reader.models.FieldDefinition;
import nl.mirila.model.reader.models.ModelDefinition;
import org.apache.commons.lang3.StringUtils;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * This class clones {@link ModelDefinition}, and enriches it with the fields and properties of its mixins.
 */
public class ModelExpander {

    private static final ModelExpander instance = new ModelExpander();

    private Map<String, ModelDefinition> originalModels;

    /**
     * Returns the singleton instance.
     */
    public static ModelExpander getInstance() {
        return instance;
    }

    /**
     * Returns a list of expanded models for the given {@link ModelDefinition}.
     * <p>
     * The list contains the abstract models as-is, followed by the non-abstract expanded models.
     * <p>
     * @param models The loaded models.
     * @return The expanded models.
     */
    public Map<String, ModelDefinition> getExpandedModels(Map<String, ModelDefinition> models) {
        originalModels = models;

        // First get all abstract models as-is.
        Map<String, ModelDefinition> result = models.values().stream()
                .filter(ModelDefinition::isAbstract)
                .collect(Collectors.toMap(ModelDefinition::getSingularName, Function.identity()));

        // Add non-abstract, storable expanded models.
        models.values().stream()
                .filter((modelDefinition) -> !modelDefinition.isAbstract())
                .filter(ModelDefinition::isStorable)
                .map(this::getExpandedModel)
                .forEach((modelDefinition) -> result.put(modelDefinition.getSingularName(), modelDefinition));

        // Add non-storable models.
        models.values().stream()
                .filter((modelDefinition) -> !modelDefinition.isAbstract())
                .filter((modelDefinition) -> !modelDefinition.isStorable())
                .map(this::getExpandedModel)
                .forEach((modelDefinition) -> result.put(modelDefinition.getSingularName(), modelDefinition));

        return result;
    }

    /**
     * Returns a list of expanded models for the given {@link ModelDefinition}.
     * <p>
     * @param modelDefinition The model definition.
     * @return The expanded model definition.
     */
    private ModelDefinition getExpandedModel(ModelDefinition modelDefinition) {
        validateMixins(modelDefinition, 0);

        // Get all relevant related models, including the current model definition.
        Map<String, ModelDefinition> mixinModels = getRelevantMixinsRecursively(modelDefinition);
        mixinModels.put(modelDefinition.getSingularName(), modelDefinition);
        Map<String, ModelDefinition> bindModels = getRelevantBindModels(modelDefinition);
        Map<String, ModelDefinition> allRelevantModels = new LinkedHashMap<>(mixinModels);
        allRelevantModels.putAll(bindModels);

        ModelDefinition expandedModel = new ModelDefinition();
        expandedModel.setSingularName(modelDefinition.getSingularName());
        expandedModel.setPluralName(modelDefinition.getPluralName());
        expandedModel.setDescription(modelDefinition.getDescription());
        expandedModel.setStorable(modelDefinition.isStorable());

        modelDefinition.getMixins().forEach((mixinName) -> {
            ModelDefinition mixin = originalModels.get(mixinName);
            if (mixin.isAbstract()) {
                expandedModel.getMixins().add(mixinName);
                expandedModel.getMixinModels().put(mixinName, mixin);
            }
        });

        mixinModels.values().stream()
                .map(ModelDefinition::getRightsFields)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .forEach(expandedModel::setRightsFields);

        mixinModels.values().stream()
                .map(ModelDefinition::getKeys)
                .forEach((keys) -> expandedModel.getKeys().putAll(keys));

        expandedModel.getFields().addAll(getExpandedFields(modelDefinition, allRelevantModels.values()));
        return expandedModel;
    }

    /**
     * Returns a list of relevant fields for the given {@link ModelDefinition} and its mixins.
     * <p>
     * @param modelDefinition The model definition.
     * @param relatedModels The relevant mixins.
     * @return A list of relevant fields.
     */
    private Collection<FieldDefinition> getExpandedFields(ModelDefinition modelDefinition,
                                                          Collection<ModelDefinition> relatedModels) {
        Predicate<ModelDefinition> isRelevant = (model) -> {
            return (model.isAbstract()) || (model.getSingularName().equals(modelDefinition.getSingularName()));
        };

        Map<String, FieldDefinition> expandedFields = new LinkedHashMap<>();
        relatedModels.stream()
                .filter(isRelevant)
                .flatMap((model) -> model.getFields().stream())
                .forEach((field) -> processField(expandedFields, field, modelDefinition, relatedModels));

        // Return all expandable fields.
        return expandedFields.values().stream()
                .filter(this::isExpandableField)
                .sorted((fieldA, fieldB) -> FieldsSorter.sort(modelDefinition, fieldA, fieldB))
                .toList();
    }

    /**
     * Process the given {@link FieldDefinition} for expansion.
     * <p>
     * @param expandedFields All related expanded fields.
     * @param modelField The field definition to expand.
     * @param modelDefinition The model definition
     * @param relatedModels The related models.
     */
    private void processField(Map<String, FieldDefinition> expandedFields,
                              FieldDefinition modelField,
                              ModelDefinition modelDefinition,
                              Collection<ModelDefinition> relatedModels) {

        String fieldName = modelField.getName();
        FieldDefinition expandedField;
        if (!expandedFields.containsKey(fieldName)) {
            expandedField = new FieldDefinition();
            expandedField.setName(fieldName);
            expandedField.setModel(modelDefinition);
            expandedFields.put(fieldName, expandedField);
        } else {
            expandedField = expandedFields.get(fieldName);
            if ((StringUtils.isNotEmpty(modelField.getType())) &&
                    (StringUtils.isNotEmpty(expandedField.getType())) &&
                    (!expandedField.getType().equals(modelField.getType()))) {
                throw new TypeOverrideException(fieldName, modelField.getModel().getSingularName());
            }
        }

        FieldDefinition baseField = (StringUtils.isNotBlank(modelField.getBindsTo()))
                ? getBindsToField(modelField, modelDefinition, relatedModels)
                : modelField;

        setIfNotNull(expandedField::setDefaultValue, baseField::getDefaultValue);
        setIfNotNull(expandedField::setType, baseField::getType);
        setIfNotNull(expandedField::setDescription, baseField::getDescription);
        setIfNotNull(expandedField::setFormatting, baseField::getFormatting);
        setIfNotNull(expandedField::setMaxLength, baseField::getMaxLength);
        setIfNotNull(expandedField::setRegex, baseField::getRegex);
        setIfNotNull(expandedField::setMinValue, baseField::getMinValue);
        setIfNotNull(expandedField::setMaxValue, baseField::getMaxValue);

        if (expandedField.getOptions() != null) {
            if (expandedField.getOptions() == null) {
                expandedField.setOptions(new ArrayList<>());
            }
            baseField.getOptions().stream()
                    .filter((option) -> !expandedField.getOptions().contains(option))
                    .forEach(expandedField.getOptions()::add);
        }

        if ((StringUtils.isNotBlank(modelField.getBindsTo()))) {
            expandedField.setReadOnly(true);
        } else {
            expandedField.setRequired(expandedField.isRequired() || baseField.isRequired());
            expandedField.setAutoGenerated(expandedField.isAutoGenerated() || baseField.isAutoGenerated());
            expandedField.setReadOnly(expandedField.isReadOnly() || baseField.isReadOnly());
        }
        expandedField.setServerOnly(expandedField.isServerOnly() || baseField.isServerOnly());

        if (!modelField.getModel().getSingularName().equals(modelDefinition.getSingularName())) {
            // The field doesn't originate from the original model definition, but from a mixin.
            expandedField.setParentFieldDefinition(baseField);
        }
    }

    /**
     * Return the binds-to field.
     * <p>
     * @param originalField The original field.
     * @param modelDefinition The model definition of the original field.
     * @param relatedModels The related models.
     * @return The field that binds to the original field.
     */
    private FieldDefinition getBindsToField(FieldDefinition originalField,
                                            ModelDefinition modelDefinition,
                                            Collection<ModelDefinition> relatedModels) {
        String bindsTo = originalField.getBindsTo();
        if (!Reference.isValid(bindsTo)) {
            throw new InvalidBindsToException(originalField.getName(), modelDefinition.getSingularName(), bindsTo);
        }
        Reference reference = new Reference(bindsTo);
        return relatedModels.stream()
                .filter((model) -> model.getSingularName().equals(reference.getModel()))
                .flatMap((model) -> model.getFields().stream())
                .filter((field) -> field.getName().equals(reference.getField()))
                .findAny()
                .orElseThrow(() -> new UnknownBindsToException(originalField.getName(),
                                                               modelDefinition.getSingularName(),
                                                               bindsTo));
    }

    /**
     * Validates the mixins recursively, throw an error in case of invalid mixins, which could be:
     * - {@link CircularModelException}, when mixins depend on each other, creating infinite loops.
     * - {@link UnknownMixinException}, when a used mixin doesn't exist.
     * - {@link DuplicateMixinException}, when a mixin is used twice in the chain.
     * <p>
     * @param modelDefinition The model definition of which to validate the mixins.
     * @param ttl The time-to-live, which is used to detect circular dependencies.
     * @return A list of valid mixin names, especially used in the recursion.
     */
    private List<String> validateMixins(ModelDefinition modelDefinition, int ttl) {
        String modelName = modelDefinition.getSingularName();

        if (ttl > 25) {
            throw new CircularModelException(modelName);
        }
        List<String> results = new ArrayList<>();
        modelDefinition.getMixins().forEach((mixinName) -> {
            ModelDefinition parentMixin = originalModels.get(mixinName);
            if (parentMixin == null) {
                throw new UnknownMixinException(mixinName, modelName);
            }
            results.addAll(validateMixins(parentMixin, ttl + 1));
            if (results.contains(mixinName)) {
                throw new DuplicateMixinException(mixinName, modelName);
            }
            results.add(mixinName);
        });

        return results;
    }

    /**
     * Recursively returns a list of all relevant mixins for the given {@link ModelDefinition}.
     * <p>
     * When the current model definition is a concrete model, instead of abstract, its mixins aren't relevant anymore.
     * <p>
     * @param modelDefinition The model definition.
     * @return A map of relevant model definitions.
     */
    private Map<String, ModelDefinition> getRelevantMixinsRecursively(ModelDefinition modelDefinition) {
        Map<String, ModelDefinition> mixins = new LinkedHashMap<>();

        modelDefinition.getMixins().forEach((mixinName) -> {
            if (!originalModels.containsKey(mixinName)) {
                throw new UnknownMixinException(mixinName, modelDefinition.getSingularName());
            }
            ModelDefinition mixin = originalModels.get(mixinName);
            if (mixin.isAbstract()) {
                mixins.putAll(getRelevantMixinsRecursively(mixin));
                mixins.put(mixinName, mixin);
            }
        });
        return mixins;
    }

    /**
     * Returns a list of all relevant bound models for the given {@link ModelDefinition}, via its binds-to directive.
     * <p>
     * @param modelDefinition The model definition.
     * @return A map of relevant model definitions.
     */
    private Map<String, ModelDefinition> getRelevantBindModels(ModelDefinition modelDefinition) {
        return modelDefinition.getFields().stream()
                .map(FieldDefinition::getBindsTo)
                .filter(Reference::isValid)
                .map(Reference::new)
                .map(Reference::getModel)
                .distinct()
                .map(originalModels::get)
                .filter(Objects::nonNull)
                .collect(Collectors.toMap(ModelDefinition::getSingularName, (model) -> model));
    }

    /**
     * Applies the value of the given {@link Supplier} setter to the given {@link Consumer} getter,
     * if the values is not null.
     * <p>
     * @param setter The setter that accepts the value.
     * @param getter The getter that returns the value.
     * @param <E> The type that both getter and setter use.
     */
    private <E> void setIfNotNull(Consumer<E> setter, Supplier<E> getter) {
        E value = getter.get();
        if (value != null) {
            setter.accept(value);
        }
    }

    /**
     * Return true if the given {@link FieldDefinition} should be expanded.
     */
    private boolean isExpandableField(FieldDefinition field) {
        return field.getParentFieldDefinition()
                .map(FieldDefinition::getModel)
                .map(ModelDefinition::isAbstract)
                .orElse(true);
    }

}
