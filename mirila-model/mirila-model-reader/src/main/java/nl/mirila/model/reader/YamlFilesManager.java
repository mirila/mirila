package nl.mirila.model.reader;

import java.io.File;
import java.io.FilenameFilter;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

/**
 * Manager for Yaml files in a specific root folder.
 */
public class YamlFilesManager {

    private final String srcPath;

    /**
     * Initialize instance.
     */
    public YamlFilesManager(String srcPath) {
        this.srcPath = srcPath;
    }

    /**
     * Return a list of all XML files in the source path.
     */
    public List<Path> getSourceFilePaths() {
        return getSourceFilePaths(new File(srcPath));
    }

    /**
     * Recursively return all XML files in the current directory and its subdirectories.
     * <p>
     * @param currentDir The current directory.
     * @return A list of XML files.
     */
    private List<Path> getSourceFilePaths(File currentDir) {
        if (!currentDir.exists()) {
            return Collections.emptyList();
        }

        List<Path> list = new ArrayList<>();

        // List all Yaml files.
        FilenameFilter yamlFilter = (dir, name) -> (name.endsWith(".yaml") || name.endsWith(".yml"));
        File[] files = currentDir.listFiles(yamlFilter);
        if (files != null) {
            Stream.of(files)
                    .map(File::toPath)
                    .forEach(list::add);
        }

        // Recursively check all sub dirs, and add results to the list.
        File[] dirs = currentDir.listFiles(File::isDirectory);
        if ((dirs != null) && (dirs.length > 0)) {
            Stream.of(dirs)
                    .map(this::getSourceFilePaths)
                    .forEach(list::addAll);
        }
        list.sort(Comparator.naturalOrder());
        return list;
    }

}
