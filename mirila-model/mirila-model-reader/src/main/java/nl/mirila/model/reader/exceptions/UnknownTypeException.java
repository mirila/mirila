package nl.mirila.model.reader.exceptions;

import nl.mirila.core.exceptions.MirilaException;

public class UnknownTypeException extends MirilaException {

    public UnknownTypeException(String type) {
        super(String.format("The type %s is unknown.", type));
    }

}
