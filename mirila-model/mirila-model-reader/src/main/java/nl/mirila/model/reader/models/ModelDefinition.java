package nl.mirila.model.reader.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import nl.mirila.core.exceptions.MirilaException;
import nl.mirila.model.reader.utils.FieldsSorter;

import javax.annotation.Nonnull;
import java.util.*;

/**
 * A representation of a single model, within the model definitions.
 */
public class ModelDefinition {

    @JsonIgnore
    private String singularName;

    @JsonIgnore
    private String pluralName;

    @JsonProperty("abstract")
    private boolean isAbstract;

    @JsonProperty("storable")
    private boolean isStorable;

    @JsonProperty("description")
    private String description;

    @JsonProperty("mixins")
    private final List<String> mixins;

    @JsonProperty("keys")
    private final Map<String, List<String>> keys;

    @JsonIgnore
    private final Map<String, ModelDefinition> mixinModels;

    @JsonProperty("rights-fields")
    private RightsDefinition rightsFields = null;

    @JsonIgnore
    private final List<FieldDefinition> fields;

    /**
     * Initialize an instance.
     */
    public ModelDefinition() {
        keys = new HashMap<>();
        mixins = new ArrayList<>();
        fields = new ArrayList<>();
        mixinModels = new HashMap<>();
        isAbstract = false;
        isStorable = true;
    }

    /**
     * Deserializes the name component.
     */
    @JsonProperty("name")
    private void unpackName(Map<String, String> name) {
        singularName = name.get("singular");
        pluralName = name.get("plural");
    }

    /**
     * Deserializes the fields component.
     */
    @JsonProperty("fields")
    private void unpackFields(Map<String, FieldDefinition> fields) {
        Objects.requireNonNull(fields);
        this.fields.clear();
        fields.forEach((name, field) -> {
            field.setName(name);
            field.setModel(this);
            this.fields.add(field);
        });
        this.fields.sort((fieldA, fieldB) -> FieldsSorter.sort(this, fieldA, fieldB));
    }

    /**
     * Return the singular name of the model.
     */
    @Nonnull
    public String getSingularName() {
        if (singularName == null) {
            throw new MirilaException("A singular name may not be null after deserialization.");
        }
        return singularName;
    }

    /**
     * Set the singular name of the model.
     */
    public void setSingularName(String singularName) {
        this.singularName = singularName;
    }

    /**
     * Return the plural name of the model.
     */
    public String getPluralName() {
        return pluralName;
    }

    /**
     * Set the plural name of the model.
     */
    public void setPluralName(String pluralName) {
        this.pluralName = pluralName;
    }

    /**
     * Return the description of the model.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set the description of the model.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Return true if the this model is abstract.
     */
    public boolean isAbstract() {
        return isAbstract;
    }

    /**
     * Return true if the this model is storable.
     */
    public boolean isStorable() {
        return isStorable;
    }

    /**
     * Set to true if this model is storable.
     */
    public void setStorable(boolean storable) {
        isStorable = storable;
    }

    /**
     * Return the list of mixin names of the model.
     */
    public List<String> getMixins() {
        return mixins;
    }

    /**
     * Return a map of mixin names and the related models.
     * <p>
     * This map is not yet filled with the data after serialization.
     */
    public Map<String, ModelDefinition> getMixinModels() {
        return mixinModels;
    }

    /**
     * Return the field names that define the primary key.
     */
    public Map<String, List<String>> getKeys() {
        return keys;
    }

    /**
     * Return the field names that define the primary key.
     */
    public List<String> getPrimaryKeys() {
        return keys.getOrDefault("primary", Collections.emptyList());
    }

    /**
     * Return the rights fields for this model, wrapped in an {@link Optional}.
     */
    public Optional<RightsDefinition> getRightsFields() {
        return Optional.ofNullable(rightsFields);
    }

    /**
     * Set the rights fields for this model.
     */
    public void setRightsFields(RightsDefinition rightsFields) {
        this.rightsFields = rightsFields;
    }

    /**
     * Return the list of {@link FieldDefinition} of the model.
     */
    public List<FieldDefinition> getFields() {
        return fields;
    }

    /**
     * Return the list of field names of the model.
     */
    public List<String> getFieldNames() {
        return fields.stream()
                .sorted((fieldA, fieldB) -> FieldsSorter.sort(this, fieldA, fieldB))
                .map(FieldDefinition::getName)
                .toList();
    }

    /**
     * Return the {@link FieldDefinition} for the given field name.
     * <p>
     * If the field name is unknown, a {@link NoSuchElementException} is thrown.
     * <p>
     * @param name The field name.
     * @return The field definition.
     */
    public FieldDefinition getField(String name) {
        return fields.stream()
                .filter((field) -> field.getName().equals(name))
                .findFirst()
                .orElseThrow();
    }

}
