package nl.mirila.model.reader.exceptions;

import nl.mirila.core.exceptions.MirilaException;
import nl.mirila.model.reader.models.FieldDefinition;

public class InvalidDefaultException extends MirilaException {

    public InvalidDefaultException(FieldDefinition definition) {
        super(generateMessage(definition));
    }

    public InvalidDefaultException(FieldDefinition definition, Throwable cause) {
        super(generateMessage(definition), cause);
    }

    private static String generateMessage(FieldDefinition definition) {
        return String.format("The default value '%s' for table field %s.%s [%s] is invalid.",
                definition.getDefaultValue(),
                definition.getModel().getPluralName(),
                definition.getName(),
                definition.getType());
    }

}
