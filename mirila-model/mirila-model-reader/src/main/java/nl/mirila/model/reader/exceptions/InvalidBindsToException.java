package nl.mirila.model.reader.exceptions;

import nl.mirila.core.exceptions.MirilaException;

public class InvalidBindsToException extends MirilaException {

    public InvalidBindsToException(String fieldName, String modelName, String bindsTo) {
        super(String.format("The binds-to of %s.%s is invalid: %s", modelName, fieldName, bindsTo));
    }

}
