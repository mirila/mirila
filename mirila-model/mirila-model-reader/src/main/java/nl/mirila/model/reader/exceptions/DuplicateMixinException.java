package nl.mirila.model.reader.exceptions;

import nl.mirila.core.exceptions.MirilaException;

public class DuplicateMixinException extends MirilaException {

    public DuplicateMixinException(String mixinName, String modelName) {
        super(String.format("Mixin %s is already included in %s.", mixinName, modelName));
    }

}
