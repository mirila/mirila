package nl.mirila.model.reader.exceptions;

import nl.mirila.core.exceptions.MirilaException;

public class UnknownBindsToException extends MirilaException {

    public UnknownBindsToException(String fieldName, String modelName, String bindsTo) {
        super(String.format("The binds-to of %s.%s is unknown: %s", modelName, fieldName, bindsTo));
    }

}
