package nl.mirila.model.reader.exceptions;

import nl.mirila.core.exceptions.MirilaException;

public class CircularModelException extends MirilaException {

    public CircularModelException(String modelName) {
        super(String.format("Circular model %s found.", modelName));
    }

}
