package nl.mirila.model.reader.exceptions;

import nl.mirila.core.exceptions.MirilaException;

public class MissingTypeException extends MirilaException {

    public MissingTypeException(String type) {
        super(String.format("Missing the type specification for type %s.", type));
    }

}
