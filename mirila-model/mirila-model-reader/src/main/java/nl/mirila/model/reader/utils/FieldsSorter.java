package nl.mirila.model.reader.utils;

import nl.mirila.model.reader.models.FieldDefinition;
import nl.mirila.model.reader.models.ModelDefinition;

import java.util.List;

/**
 * Sorter for fields within a model definition.
 */
public class FieldsSorter {

    /**
     * This utility class may not be instantiated.
     */
    private FieldsSorter() {
        throw new UnsupportedOperationException();
    }

    /**
     * Return the sort integer for the two given {@link FieldDefinition} of the given {@link ModelDefinition}.
     * <p>
     * The sorting makes sure that all primary keys are sorted first. Within the primary keys or non-primary keys, it
     * follows the input sorting.
     * <p>
     * @param modelDefinition The model definition.
     * @param fieldDefinitionA The first field definition.
     * @param fieldDefinitionB The second field definition.
     * @return The sort integer.
     */
    public static int sort(ModelDefinition modelDefinition,
                           FieldDefinition fieldDefinitionA,
                           FieldDefinition fieldDefinitionB) {
        List<String> primaryKeys = modelDefinition.getPrimaryKeys();
        boolean isPrimaryFieldA = primaryKeys.contains(fieldDefinitionA.getName());
        boolean isPrimaryFieldB = primaryKeys.contains(fieldDefinitionB.getName());
        if (isPrimaryFieldA) {
            return -1;
        } else if (isPrimaryFieldB) {
            return 1;
        }
        return 0;
    }

}
