package nl.mirila.model.reader;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import nl.mirila.core.exceptions.MirilaException;
import nl.mirila.model.reader.models.ModelDefinition;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Optional;

/**
 * This reader loads model definition as Yaml files, into {@link ModelDefinition}.
 */
public class ModelFileReader {

    private static final Logger logger = LogManager.getLogger(ModelFileReader.class);

    private final ObjectMapper mapper;

    /**
     * Initialize a new instance.
     */
    public ModelFileReader() {
        mapper = new ObjectMapper(new YAMLFactory());
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    /**
     * Loads and returns a {@link ModelDefinition} based on the Yaml file with the given {@link Path}.
     * <p>
     * @param yamlPath The path of a Yaml file.
     * @return The model definition, wrapped in an {@link Optional}.
     */
    public Optional<ModelDefinition> readModelDefinition(Path yamlPath) {
        ModelDefinition model;
        try {
            model = mapper.readValue(yamlPath.toFile(), ModelDefinition.class);
        } catch (IOException e) {
            String msg = String.format("Error while parsing '%s': %s", yamlPath.getFileName(), e.getMessage());
            logger.fatal(msg);
            throw new MirilaException(msg, e);
        }

        return Optional.of(model);
    }

}
