package nl.mirila.model.reader.exceptions;

import nl.mirila.core.exceptions.MirilaException;

public class UnknownMixinException extends MirilaException {

    public UnknownMixinException(String mixinName, String modelName) {
        super(String.format("Mixin %s in %s does not exist.", mixinName, modelName));
    }

}
