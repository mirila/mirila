package nl.mirila.model.reader.exceptions;

import nl.mirila.core.exceptions.MirilaException;

public class TypeOverrideException extends MirilaException {

    public TypeOverrideException(String fieldName, String modelName) {
        super(String.format("The type of field %s may not be overridden in %s.", fieldName, modelName));
    }

}
