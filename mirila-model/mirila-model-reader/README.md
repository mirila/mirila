# Mirila Model Reader

The Mirila Model Reader reads model definitions written in java. The structure of these definitions are described
on this page. The loaded models can then be used to write pojos, sql or other model related files. 

Example:
```yaml
name:
    singular: Person
    plural: People

type: model

mixins: []

fields:
    first-name:
        type: string
        max-length: 100
        description: The name of the person. 
```

The supported keys are:
  * `name` The names of the model, supporting the following sub keys:
    * `singular` The singular name of the model.
    * `plural` The plural name of the model.
  * `abstract` True if this model is an abstract model, only usable via mixins. Default: false.
  * `storable` True if this model should be stored in a database, using the default mechanics. Default: true.
  * `description` A description of the model.
  * `mixins` A list of mixin (abstract) models that should be included in this model.
  * `keys` A map of several keys.
    * `primary` A list fields that define the primary key.
  * `rights-fields` A set of field names that define the rights system:
    * `owner-ids` The field name that contains the ids of the owners that own the instance.
    * `group-ids` The field name that contains the ids of the groups that are related to the instance.
    * `right` The field name that contains the rights code that defines the rights to this instance.
    * `category` The category that is used in settings for checking creation or maintenance rights. 
  * `fields` The fields of the model. The keys are the names of the field:
    * `[fieldName]` Each field can have the following subfields.
      * `type` The type of the field. See the list below for the supported types.
      * `default` The default value of this field. Default: null.
                  For date and time based fields, the value NOW may be user, or actual ISO strings.
                  For integer fields, a number or the capitalized ChronoUnit type names may also be used, like YEAR.
                  See also: https://docs.oracle.com/javase/8/docs/api/java/time/temporal/ChronoUnit.html
      * `binds-to` Binds to a specific field of another model. This field uses the same values of that field,
                   or override those values.  
      * `required` A boolean to indicate this is a required field. Default: false.
      * `storable` True if this field should be stored in a database, using the default mechanics. Default: true.
      * `auto-generated` A boolean to indicate this is an auto-generated field. Default: false.
      * `max-length` The maximum length that can be expected for this field. Default depends on DBS.
      * `formatting` The formatting of the text: normal, html, markdown, ubb. Default: normal.
      * `read-only` A boolean to indicate this is a read-only field for clients. Default: false.
      * `server-only` A boolean to indicate this field should not be exposed to the outside world. Default: false.
      * `min` The value must be at least this value (inclusive). Only for numeric and date based fields. 
              For date based fields, the values `past`, `now` and `future` may also be used.
      * `max` The value must be at most this value (inclusive). Only for numeric and date based fields.
              For date based fields, the values `past`, `now` and `future` may also be used.
      * `regex` The value must match this regular expression. Only for string based fields. 
      * `options` A list of possible options to expect. 
      * `description` A description of this field.  

The exact implementation may differ per database system (DBS). 

The supported field types are:
  * `id` An id. The database system determines the actual type implementation, for example integers or strings.
  * `string` A string representation.
  * `json` A json representation.
  * `text` A string representation that is used for potential large texts.
  * `integer` A numeric value. Set the `min` to 0 or higher to use unsigned integers.
  * `long` A numeric value for larger numbers, like timestamps.
  * `float` A floating point value.
  * `boolean` A boolean value.
  * `date` A date value, without the time.
  * `time` A local time value, without the date.
  * `datetime` A combination of both date and local time.
  * `timestamp` A timestamp in microseconds.

Some tips:  
- Enums are omitted. They might break your product. Strings can be used instead.
- Always write keys in these YAML files in kebab-case. 

  
   


