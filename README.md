# Mirila framework
The Mirila framework is a set of libraries that can be used for back-end developments. It can be used as a foundation
for Rest API's and microservices.

## Architecture
Mirila is build in Java. The principle of __separation of concern__ is a basis during its development. To enforce this
principle, Mirila embraces the multi-module features that the build tool Maven provides.

For an abstract feature, for example a message bus, you'll find a core module, which contains a high-level
abstraction of the needed feature, without any concrete implementation, or an in-memory or dummy implementation.
In the case of a message bus, you can find an abstract message producer, an abstract message consumer, and a
representation of a message queue. Additionally, the core module may contain some useful methods, but most importantly,
a core module won't have any knowledge of concrete implementations, like RabbitMQ, Redis or Kafka.
Those concrete implementations are implemented in separate modules. Core modules may contain a helpful internal concrete
implementation, which may or may not be really functionally. These implementations can be used for testing purposes.

Mirila is built with a model centric design. In the vision of Mirila, the model definitions are the basis of an
application. Tools, like database, messaging and cache systems, are merely means that in one or other way persist or
process entities of those models. Therefore, Mirila is not aiming a specific set of tools, but rather allow applications
to use different tools for different contexts.

Although model definitions can be written as plain java objects (pojos), the logical choice is to write the model
definitions in YAML. How to write these model defititions can be found in the 
[separate readme of the Mirila Model Reader](mirila-model/mirila-model-reader/README.md).

Based on these models, Mirila provides an entity manager. With properly defined models, Mirila provides default reading
and writing functionality. This will solve most of the use-cases. For specific cases, Mirila adds all kind of functions,
like hooks and alternative data sources. 

```mermaid
graph TD;
    REST[Restful resource] --> EM[EntityManager<Model>];
    EM -->|Pre hooks| EM;
    EM --> EPM[EntityPercistenceManager];
    EM --> EP[EntitiesProvider<Model>];
    EPM --> Db[(Database)];
    Db --> EPM;
    EPM --> EM;
    EP --> EM;
    EM -->|Post hooks| EM;
    EM --> REST;
    EP -.-> AC[(Cache)];
    AC -.-> EP;
    EP -.-> AF[(File)];
    AF -.-> EP;
    EP -.-> AM[(Memory)];
    AM -.-> EP;
    EP -.-> AD[(Datastore)];
    AD -.-> EP;
```

## Maven
Using Maven, we can build the modules of Mirila. Some additional useful Maven commands are:
```bash
# Find which existing dependencies have newer updates.
mvn versions:display-dependency-updates

# Find which existing dependencies have newer updates, and show each unique line only once.
mvn versions:display-dependency-updates | grep "\->" | sort | uniq

# Update the versions in the properties to the latest version.
mvn versions:update-properties

# Generate a dot graph (this requires graphviz) and a png version.
mvn com.github.ferstl:depgraph-maven-plugin:aggregate -DgraphFormat=dot -DcreateImage=true "-DtargetIncludes=nl.mirila.*"
```

## Used frameworks
Mirila ties several frameworks together. In general, the following frameworks are used in most of the modules: 
- Maven: A Java build tool.
- Guice: A dependency Injection framework
- Jackson: A JSON serializer and deserializer.
- JUnit5: A testing framework.
- AssertJ: A fluent assertion framework on top of JUnit5.
- Log4j2: A logging framework.

## Terminology
Words are important, especially in exact worlds. However, some terms seem ambiguous or are used ambiguous. Some synonyms
seem the same, but have slightly different meaning.

### Authentication
For authentication related terms, Mirila uses terms according the following diagram:
http://telicthoughts.blogspot.com/2011/02/authentication-concept-map.html

### Settings
The terms Settings, Options, Configurations, etc. all seem synonyms, though they may have slightly different meanings.  
https://stackoverflow.com/questions/2074384/options-settings-properties-configuration-preferences-when-and-why

We use the term __Settings__ for all configurable values. We define four levels of settings:
1. System settings: stored as file on the system, that contains the basic to run on a system.
   Preferable, this configuration is small, and only contains server specific configuration
   and configuration that is needed to load application configurations.
2. Application settings: application settings that applies to all realms.
3. Realm settings: the settings that applies to a certain realm.
4. User settings: the settings that users may apply in user profiles.

There are dependencies in these settings. 
- The user settings uses the realm settings as fallback.
- The realm settings uses the application settings as fallback.
- The application settings uses the system settings as fallback.

### Rights
The terms Permissions, Rights, Privileges, etc. all seem synonyms, though they may have slightly different meanings.

We use the term __Rights__ for all permissions, rights and privileges. 
- Existing objects have rights based on a right code, __Groups__ and __Owners__.
- Other rights, like object creation, is based on (group) settings and sensible defaults.

### Groups
We use the term __Groups__ for dynamic grouping of users. Rights can dynamically be assigned to groups.
Using Groups is a fine-grained method to determine access to specific objects.

### Roles
We use the term __Roles__ for system level access control. Mostly they are hard coded into the application.
Using Roles is a coarse-grained method to determine access to functionality.   

### Drivers
We use the term _Drivers_ as interfaces between the Mirila framework and third party services, like databases and cache
systems. A driver basically couples third party software, like clients, to Mirila settings, and supplies the connection
or client to other Mirila modules. Some drivers may contain additional services that fit the scope of the driver.  

## The name Mirila
A personal note: The name Mirila consists of the syllables Mi, Ri and La. These are a reference to Lama Michel Rinpoche,
a Buddhist teacher, and my guru. For more information on Lama Michel, please visit https://www.ngalso.org.
