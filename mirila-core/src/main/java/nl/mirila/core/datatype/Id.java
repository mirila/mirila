package nl.mirila.core.datatype;

import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

/**
 * An identification for an entity.
 */
public class Id implements Comparable<Id> {

    private String value;

    /**
     * Do not use this in code; this public constructor is needed for some deserializers.
     */
    public Id() {
    }

    /**
     * Initialize a new instance.
     */
    private Id(String value) {
        this.value = (value == null) ? "" : value;
    }

    /**
     * Return an empty Id.
     */
    public static Id empty() {
        return new Id("");
    }

    /**
     * Return an {@link Id} with the given String value.
     */
    public static Id of(String value) {
        return new Id(value);
    }

    /**
     * Return an {@link Id} with the given integer value.
     */
    public static Id of(int value) {
        return new Id(Integer.toString(value));
    }

    /**
     * Return true if this id is empty.
     */
    public boolean isEmpty() {
        return (StringUtils.isEmpty(value)) || (value.equals("0"));
    }

    /**
     * Return true if this id contains a value.
     */
    public boolean isPresent() {
        return !isEmpty();
    }

    /**
     * Return the value as String.
     */
    public String asString() {
        return toString();
    }

    /**
     * Return the integer value. If the value is null or not a number, 0 is returned.
     */
    public int asInt() {
        return (StringUtils.isNumeric(value)) ? Integer.parseInt(value) : 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return value;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if ((other == null) || (getClass() != other.getClass())) {
            return false;
        }
        Id otherId = (Id) other;
        return Objects.equals(value, otherId.value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return (StringUtils.isNumeric(value)) ? Integer.parseInt(value) : value.hashCode();
    }

    /**
     * This is an alias for {@link Id#of(String)}, used by other libraries like JaxRS.
     */
    public static Id fromString(String value) {
        return Id.of(value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int compareTo(Id other) {
        return hashCode() - other.hashCode();
    }

}
