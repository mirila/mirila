package nl.mirila.core.datatype;

import java.util.Arrays;
import java.util.Objects;

/**
 * The {@link Json} object is a wrapper for JSON strings.
 */
public class Json {

    public static final String EMPTY_HASH = "{}";
    public static final String EMPTY_ARRAY = "[]";
    public static final String NULL_STRING = "null";
    public static final String DEFAULT = EMPTY_HASH;

    private final String json;

    /**
     * Initialize a new instance with the given JSON string.
     * <p>
     * If a null, empty or blank string is provided, the {@value DEFAULT} will be used instead.
     */
    private Json(String json) {
        this.json = (mayBeJson(json)) ? json : DEFAULT;
    }

    /**
     * Return a {@link Json} for the given JSON string.
     */
    public static Json of(String json) {
        return new Json(json);
    }

    /**
     * Return true if the JSON contains an empty hash, array or null-string.
     */
    public boolean isEmpty() {
        return Arrays.asList(EMPTY_HASH, EMPTY_ARRAY, NULL_STRING).contains(json);
    }

    /**
     * Return true if the JSON does data with content.
     */
    public boolean isPresent() {
        return !isEmpty();
    }

    /**
     * Returns the json string.
     */
    public String get() {
        return this.json;
    }

    /**
     * Returns true if the given string is likely to be JSON. It does not validate the actual content of the string.
     * <p>
     * @param str The given string.
     * @return True if the string is likely to be JSON.
     */
    public static boolean mayBeJson(String str) {
        if (str == null) {
            return false;
        }
        str = str.trim();
        return ((str.equals("null")) ||
                ((str.startsWith("{")) && (str.endsWith("}"))) ||
                ((str.startsWith("[")) && (str.endsWith("]"))));
    }

    /**
     * Alias for {@link Json#get()}.
     */
    @Override
    public String toString() {
        return get();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if ((other == null) || (getClass() != other.getClass())) {
            return false;
        }
        Json otherJson = (Json) other;
        return Objects.equals(json, otherJson.json);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(json);
    }

}
