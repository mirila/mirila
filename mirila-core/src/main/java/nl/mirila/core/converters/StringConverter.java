package nl.mirila.core.converters;

import nl.mirila.core.datatype.Id;
import nl.mirila.core.datatype.Json;
import nl.mirila.core.exceptions.InvalidStringConversionException;
import nl.mirila.core.exceptions.UnsupportedStringConversionTypeException;
import nl.mirila.core.recurrence.patterns.RecurrencePattern;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.*;

/**
 * Convert Strings into instances of other classes.
 */
public final class StringConverter {

    private static final List<Class<?>> supportedClasses = Arrays.asList(String.class,
                                                                         Id.class,
                                                                         Integer.class,
                                                                         Float.class,
                                                                         Double.class,
                                                                         BigDecimal.class,
                                                                         Long.class,
                                                                         Boolean.class,
                                                                         LocalDate.class,
                                                                         LocalTime.class,
                                                                         LocalDateTime.class);

    /**
     * This utility class may not be instantiated.
     */
    private StringConverter() {
        throw new UnsupportedOperationException();
    }

    /**
     * Convert the given string value into the requested type.
     * <p>
     * The following types are supported:
     * - String
     * - Integer
     * - Float
     * - Double
     * - Long
     * - Boolean
     * - LocalDate
     * - LocalDateTime
     * If the requested type is not supported an {@link UnsupportedStringConversionTypeException} is thrown.
     * <p>
     * If the given value is blank and the requested type is not a String, a null will be returned.
     * If the given value can not be parsed correctly a {@link InvalidStringConversionException} is thrown.
     * <p>
     * @param resultClass The class of the requested type.
     * @param value The value to convert.
     * @param <T> The requested type.
     * @return The converted value.
     * <p>
     * @throws InvalidStringConversionException The given string is invalid.
     * @throws UnsupportedStringConversionTypeException The requested type is unsupported.
     */
    public static <T> T convert(Class<T> resultClass, String value)
            throws InvalidStringConversionException, UnsupportedStringConversionTypeException {
        if ((StringUtils.isBlank(value)) && (resultClass != String.class)) {
            return null;
        }

        value = value.trim();
        Object object;
        try {
            if (resultClass == String.class) {
                object = value;
            } else if (resultClass == Id.class) {
                object = Id.of(value);
            } else if (resultClass == Integer.class) {
                object = Integer.valueOf(value);
            } else if (resultClass == Float.class) {
                object = Float.valueOf(value);
            } else if (resultClass == Double.class) {
                object = Double.valueOf(value);
            } else if (resultClass == BigDecimal.class) {
                object = new BigDecimal(value);
            } else if (resultClass == Long.class) {
                object = Long.valueOf(value);
            } else if (resultClass == Boolean.class) {
                object = Boolean.valueOf(value);
            } else if (resultClass == Json.class) {
                object = Json.of(value);
            } else if (resultClass == LocalDate.class) {
                object = ((value.equalsIgnoreCase("NOW")) || (value.equalsIgnoreCase("TODAY")))
                        ? LocalDate.now()
                        : LocalDate.parse(value);
            } else if (resultClass == LocalTime.class) {
                if (value.equalsIgnoreCase("NOW")) {
                    object = LocalTime.now();
                } else {
                    if (value.split(":")[0].length() == 1) {
                        // We accept times without leading zero in the hour component.
                        value = "0" + value;
                    }
                    object = LocalTime.parse(value);
                }
            } else if (resultClass == LocalDateTime.class) {
                if (value.equalsIgnoreCase("NOW")) {
                    object = LocalDateTime.now();
                } else {
                    if (value.matches("^[\\d-]+\\s[\\d:]+$")) {
                        // We accept a space between date and time. Replace it with a 'T'.
                        value = value.replace(" ", "T");
                    }
                    object = LocalDateTime.parse(value);
                }
            } else if (resultClass == RecurrencePattern.class) {
                object = RecurrencePattern.fromCodes(value);
            } else {
                throw new UnsupportedStringConversionTypeException(resultClass);
            }
        } catch (NumberFormatException | DateTimeParseException e) {
            throw new InvalidStringConversionException(resultClass, value);
        }
        return resultClass.cast(object);
    }

    /**
     * Convert the given string value into the requested type, using {@link StringConverter#convert(Class, String)},
     * but doesn't throw exceptions, allowing the process to continue.
     * <p>
     * @param resultClass The class of the requested type.
     * @param value The value to convert.
     * @param <T> The requested type.
     * @return The converted value.
     */
    public static <T> T convertLenient(Class<T> resultClass, String value) {
        try {
            return convert(resultClass, value);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Convert the given string value into a {@link List} of the requested type.
     * <p>
     * The given string will be split based on a newline (\n, \r or \r\n).
     * <p>
     * @param resultClass The class of the requested type.
     * @param value The value to convert.
     * @param <T> The requested type.
     * @return The converted values as a list.
     * <p>
     * @throws InvalidStringConversionException The given string is invalid.
     * @throws UnsupportedStringConversionTypeException The requested type is unsupported.
     */
    public static <T> List<T> convertToList(Class<T> resultClass, String value)
            throws InvalidStringConversionException, UnsupportedStringConversionTypeException {
        return convertToList(resultClass, value, "(\n|\r|\r\n)");
    }

    /**
     * Convert the given string value into a {@link List} of the requested type.
     * <p>
     * The given string will be split based on the given regex.
     * <p>
     * @param resultClass The class of the requested type.
     * @param value The value to convert.
     * @param splitRegex The regex to us for the split.
     * @param <T> The requested type.
     * @return The converted value.
     * <p>
     * @throws InvalidStringConversionException The given string is invalid.
     * @throws UnsupportedStringConversionTypeException The requested type is unsupported.
     */
    public static <T> List<T> convertToList(Class<T> resultClass, String value, String splitRegex)
            throws InvalidStringConversionException, UnsupportedStringConversionTypeException {
        String[] parts = value.split(splitRegex);
        List<T> list = new ArrayList<>();
        for (String part : parts) {
            if (!StringUtils.isBlank(part)) {
                list.add(StringConverter.convert(resultClass, part));
            }
        }
        return list;
    }

    /**
     * Convert the given string value into a {@link Map} of the requested Key type and Value type.
     * <p>
     * The entries will be split by a newline (\n, \r or \r\n).
     * Per entry the key and value will be split by the = sign.
     * <p>
     * @param keyClass The class of the Key part of the requested map.
     * @param valueClass The class of the Value part of the requested map.
     * @param value The value to convert.
     * @param <T1> The type of the Key part.
     * @param <T2> The type of the Value part.
     * @return The converted values as a map.
     * <p>
     * @throws InvalidStringConversionException The given string is invalid.
     * @throws UnsupportedStringConversionTypeException The requested type is unsupported.
     */
    public static <T1, T2> Map<T1, T2> convertToMap(Class<T1> keyClass, Class<T2> valueClass, String value)
            throws InvalidStringConversionException, UnsupportedStringConversionTypeException {
        return convertToMap(keyClass, valueClass, value, "\n", "=");
    }

    /**
     * Convert the given string value into a {@link Map} of the requested Key type and Value type.
     * <p>
     * The entries will be split by the given entrySplitRegex.
     * Per entry the key and value will be split by the given keyValueSplitRegex.
     * <p>
     * @param keyClass The class of the Key part of the requested map.
     * @param valueClass The class of the Value part of the requested map.
     * @param value The value to convert.
     * @param entrySplitRegex The regex to split each entry by.
     * @param keyValueSplitRegex The regex to split the entry into a key and value.
     * @param <T1> The type of the Key part.
     * @param <T2> The type of the Value part.
     * @return The converted values as a map.
     * <p>
     * @throws InvalidStringConversionException The given string is invalid.
     * @throws UnsupportedStringConversionTypeException The requested type is unsupported.
     */
    public static <T1, T2> Map<T1, T2> convertToMap(Class<T1> keyClass,
                                                    Class<T2> valueClass,
                                                    String value,
                                                    String entrySplitRegex,
                                                    String keyValueSplitRegex)
            throws InvalidStringConversionException, UnsupportedStringConversionTypeException {
        Map<T1, T2> map = new LinkedHashMap<>();
        String[] entries = value.split(entrySplitRegex);
        for (String entry : entries) {
            if (StringUtils.isBlank(entry)) {
                continue;
            }
            String[] parts = entry.split(keyValueSplitRegex, 2);
            if (parts.length == 2) {
                T1 mapKey = StringConverter.convert(keyClass, parts[0]);
                T2 mapValue = StringConverter.convert(valueClass, parts[1]);
                map.put(mapKey, mapValue);
            }
        }
        return map;
    }

    /**
     * Return true if the given class is supported by the {@link StringConverter#convert(Class, String)}.
     */
    public static boolean isSupportedClass(Class<?> typeClass) {
        return supportedClasses.contains(typeClass);
    }

}
