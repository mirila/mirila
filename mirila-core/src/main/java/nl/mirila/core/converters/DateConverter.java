package nl.mirila.core.converters;

import nl.mirila.core.exceptions.UnsupportedDateConversionTypeException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.*;
import java.util.Date;

import static java.time.format.DateTimeFormatter.ISO_OFFSET_DATE_TIME;

/**
 * Convert dates into instances of other classes.
 */
public final class DateConverter {

    private static final Logger logger = LogManager.getLogger(DateConverter.class);

    /**
     * This utility class may not be instantiated.
     */
    private DateConverter() {
        throw new UnsupportedOperationException();
    }


    /**
     * Convert the given date into the requested class.
     * <p>
     * The following requested classes are supported:
     * - LocalDate
     * - LocalTime
     * - LocalDateTime
     * - Instant
     * - String
     * <p>
     * @param resultClass The class of the resulting instance.
     * @param date The date to convert.
     * @param <T> The type of the resulting instance.
     * @return The instance.
     * <p>
     * @throws UnsupportedDateConversionTypeException When the requested class in not supported.
     */
    public static <T> T convert(Class<T> resultClass, Date date) throws UnsupportedDateConversionTypeException {
        if (date == null) {
            return null;
        }

        Object object;
        if (resultClass == LocalDate.class) {
            object = (date instanceof java.sql.Date sqlDate)
                    ? sqlDate.toLocalDate()
                    : date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        } else if (resultClass == LocalTime.class) {
            object = (date instanceof java.sql.Time sqlTime)
                    ? sqlTime.toLocalTime()
                    : date.toInstant().atZone(ZoneId.systemDefault()).toLocalTime();
        } else if (resultClass == LocalDateTime.class) {
            object = (date instanceof java.sql.Timestamp sqlTimestamp)
                    ? sqlTimestamp.toLocalDateTime()
                    : date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        } else if (resultClass == String.class) {
            if ((date instanceof java.sql.Date) || (date instanceof java.sql.Time)) {
                object = date.toString();
            } else if (date instanceof java.sql.Timestamp) {
                object = date.toString().substring(0, 19);
            } else {
                object = date.toInstant().atZone(ZoneId.systemDefault()).format(ISO_OFFSET_DATE_TIME);
            }
        } else if (resultClass == Instant.class) {
            try {
                object = date.toInstant();
            } catch (UnsupportedOperationException e) {
                logger.error("Can not convert {} to an Instant.", date.getClass().getName());
                throw new UnsupportedDateConversionTypeException(resultClass);
            }
        } else {
            throw new UnsupportedDateConversionTypeException(resultClass);
        }
        return resultClass.cast(object);
    }

}
