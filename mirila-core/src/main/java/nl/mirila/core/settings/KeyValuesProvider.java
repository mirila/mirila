package nl.mirila.core.settings;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * A provider that is used by a {@link Settings} instance, that provides a configuration values, based on keys.
 */
public interface KeyValuesProvider {

    /**
     * Returns the value for the given key, if available, wrapped in an {@link Optional}.
     * <p>
     * @param key The key.
     * @return The value.
     */
    Optional<String> getValue(String key);

    /**
     * Returns the list values for the given key, using a <code>\s*,\s*</code> as separator.
     * <p>
     * @param key The key.
     * @return A list with all values.
     */
    default List<String> getValues(String key) {
        return getValues(key, "\\s*,\\s*");
    }

    /**
     * Returns the list values for the given key, using the given separator.
     * <p>
     * @param key The key.
     * @param separator The separator.
     * @return A list with all values.
     */
    List<String> getValues(String key, String separator);

    /**
     * Returns a list of keys that start with the given prefix, and their values.
     * <p>
     * @param prefix The prefix.
     * @return The keys and values that have the same prefix.
     */
    Map<String, String> getSubset(String prefix);

    /**
     * A method to load all configuration keys and values.
     */
    void load();

    /**
     * A method to reload all configuration keys and values. By default, it will call the load method,
     * but it can be overridden to implement different reload methods.
     */
    default void reload() {
        load();
    }


}
