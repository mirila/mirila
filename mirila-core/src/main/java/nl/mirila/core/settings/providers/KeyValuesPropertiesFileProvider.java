package nl.mirila.core.settings.providers;

import nl.mirila.core.settings.KeyValueMapProvider;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Properties;

/**
 * A KeyValuesProvider that reads its keys and values from a properties file.
 */
public class KeyValuesPropertiesFileProvider extends KeyValueMapProvider {

    private static final Logger logger = LogManager.getLogger(KeyValuesPropertiesFileProvider.class);

    private final Path filePath;
    private boolean isFirstLoad;

    /**
     * Initializes the instance.
     * <p>
     * @param filePath The path to the properties.
     */
    public KeyValuesPropertiesFileProvider(Path filePath) {
        super(new HashMap<>());
        this.filePath = filePath;
        isFirstLoad = true;
        load();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void load() {
        Properties properties = new Properties();
        try (FileInputStream inputStream = new FileInputStream(filePath.toFile())) {
            properties.load(inputStream);
        } catch (IOException e) {
            String msg = (isFirstLoad)
                    ? "Error while loading settings. Reason: {}"
                    : "Error while loading settings. Kept previous settings. Reason: {}";
            logger.warn(msg, e.getMessage());
            return;
        }
        isFirstLoad = false;
        map.clear();
        properties.forEach((key, value) -> map.put(key.toString(), value.toString()));
        logger.info("Loaded settings from {}.", filePath);
    }

}
