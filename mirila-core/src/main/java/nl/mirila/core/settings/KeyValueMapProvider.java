package nl.mirila.core.settings;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * A {@link KeyValuesProvider} that uses a {@link Map} as the container for keys and values.
 */
public class KeyValueMapProvider implements KeyValuesProvider {

    protected final Map<String, String> map;

    /**
     * Initialize an instance with the given key-values map.
     */
    public KeyValueMapProvider(Map<String, String> map) {
        this.map = map;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<String> getValue(String key) {
        return Optional.ofNullable(map.get(key));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> getValues(String key, String separator) {
        // TODO: need to implement this.
        return new ArrayList<>();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, String> getSubset(String prefix) {
        return map.entrySet().stream()
                .filter((entry) -> entry.getKey().startsWith(prefix))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void load() {
        // Idle
    }

}
