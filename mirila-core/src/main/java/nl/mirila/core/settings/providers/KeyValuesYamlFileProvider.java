package nl.mirila.core.settings.providers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import nl.mirila.core.exceptions.MirilaException;
import nl.mirila.core.settings.KeyValueMapProvider;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * A KeyValuesProvider that reads its keys and values from a YAML file.
 */
public class KeyValuesYamlFileProvider extends KeyValueMapProvider {

    private static final Logger logger = LogManager.getLogger(KeyValuesYamlFileProvider.class);

    private final Path filePath;
    private boolean isFirstLoad;

    /**
     * Initializes a new instance.
     * <p>
     * @param filePath The path of the file.
     */
    public KeyValuesYamlFileProvider(Path filePath) {
        super(new HashMap<>());
        this.filePath = filePath;
        isFirstLoad = true;
        load();
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @Override
    public void load() {
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        Map<Object, Object> tmpMap;
        try {
            tmpMap = mapper.readValue(filePath.toFile(), Map.class);
        } catch (IOException e) {
            if (isFirstLoad) {
                String msg = "Error while loading configuration, and aborting. Reason: " + e.getMessage();
                throw new MirilaException(msg);
            } else {
                String msg = "Error while loading configuration, and keeping previous configuration. Reason: {}";
                logger.error(msg, e.getMessage());
            }
            return;
        }
        isFirstLoad = false;
        map.clear();
        recursivelyAddKeys(tmpMap, "");
    }

    /**
     * Recursively adds keys and the values from the layered Yaml map. The layered keys are stored as dotted keys.
     * <p>
     * @param tmpMap The map from Yaml.
     * @param prefix The prefix of parent nodes.
     */
    @SuppressWarnings("unchecked")
    private void recursivelyAddKeys(Map<Object, Object> tmpMap, String prefix) {
        tmpMap.forEach((key, value) -> {
            String prefixedKey = (StringUtils.isEmpty(prefix)) ? key.toString() : prefix + "." + key;
            if (value instanceof Map map) {
                recursivelyAddKeys(map, prefixedKey);
            } else {
                if (value instanceof List list) {
                    value = list.stream()
                            .map(Object::toString)
                            .collect(Collectors.joining("\\n"));
                }
                map.put(prefixedKey, value.toString());
            }
        });
    }

}
