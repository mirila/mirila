package nl.mirila.core.settings;

import com.google.inject.Inject;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Settings represents all settings within configurations of all kind.
 * It can be chained, for inheritance-like structures within configurations.
 * <p>
 * It is basically a wrapper around all kind of configurations, and it uses the {@link KeyValuesProvider} as a mean
 * to provide configuration data.
 */
public class Settings implements SettingConversions {

    protected final KeyValuesProvider provider;
    protected final Settings parentSettings;

    /**
     * Initialize an instance of Settings, without parent settings.
     * <p>
     * @param provider The provider that supplies
     */
    @Inject
    public Settings(KeyValuesProvider provider) {
        this.provider = provider;
        this.parentSettings = null;
    }

    /**
     * Initialize an instance of Settings, with parent settings.
     * <p>
     * @param provider The provider that supplies
     */
    protected Settings(KeyValuesProvider provider, Settings parentSettings) {
        this.provider = provider;
        this.parentSettings = parentSettings;
    }

    /**
     * Return the value for the given key, wrapped in an {@link Optional}.
     */
    @Override
    public Optional<String> getString(String key) {
        return provider.getValue(key).or(()-> {
            return (parentSettings != null)
                    ? parentSettings.getString(key)
                    : Optional.empty();
        });
    }

    /**
     * Return a map of key values, based on the given prefix.
     */
    @Override
    public Map<String, String> getStringMap(String prefix) {
        Map<String, String> map = (parentSettings != null)
                ? parentSettings.getStringMap(prefix)
                : new HashMap<>();

        Map<String, String> subMap = provider.getSubset(prefix);
        if ((subMap != null) && (!subMap.isEmpty())) {
            map.putAll(subMap);
        }
        return map;
    }

    /**
     * Returns a Settings instance, using the given map as key and values. This method is especially useful with
     * unit tests.
     * <p>
     * @param map The map with keys and values.
     * @return A Settings instance.
     */
    public static Settings withMap(Map<String, String> map) {
        return new Settings(new KeyValueMapProvider(map));
    }

    /**
     * Returns a Settings instance, using the given map as key and values, and the given parent Settings.
     * This method is especially useful with unit tests.
     * <p>
     * @param map The map with keys and values.
     * @return A Settings instance.
     */
    public static Settings withMapAndParentSettings(Map<String, String> map, Settings parentSettings) {
        return new Settings(new KeyValueMapProvider(map), parentSettings);
    }

}
