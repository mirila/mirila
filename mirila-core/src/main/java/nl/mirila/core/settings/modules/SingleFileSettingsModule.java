package nl.mirila.core.settings.modules;

import com.google.inject.AbstractModule;
import nl.mirila.core.settings.KeyValuesProvider;
import nl.mirila.core.settings.Settings;
import nl.mirila.core.settings.providers.KeyValuesPropertiesFileProvider;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * File based {@link Settings} using a single file.
 */
public class SingleFileSettingsModule extends AbstractModule {

    private final String fileName;

    public SingleFileSettingsModule(String fileName) {
        this.fileName = fileName;
    }

    @Override
    protected void configure() {
        Path systemConfigPath = Paths.get(fileName);
        KeyValuesPropertiesFileProvider settingsProvider = new KeyValuesPropertiesFileProvider(systemConfigPath);
        bind(KeyValuesProvider.class).toInstance(settingsProvider);
    }

}
