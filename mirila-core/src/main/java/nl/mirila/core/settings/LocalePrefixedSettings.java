package nl.mirila.core.settings;

import com.google.inject.Inject;
import org.apache.commons.lang3.LocaleUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class LocalePrefixedSettings extends Settings {

    private static final Logger logger = LogManager.getLogger(LocalePrefixedSettings.class);

    private String localeId;

    /**
     * Initialize a new instance.
     */
    @Inject
    public LocalePrefixedSettings(KeyValuesProvider keyValuesProvider) {
        super(keyValuesProvider);
        this.localeId = Locale.getDefault().toString();
    }

    /**
     * Set the given locale id.
     */
    public void setLocaleId(String localeId) {
        this.localeId = localeId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<String> getString(String key) {
        return getOrderedKeyVariants(key, localeId)
                .stream()
                .map(super::getString)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .findFirst();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, String> getStringMap(String prefix) {
        Map<String, String> map = new HashMap<>();
        List<String> keyVariants = getOrderedKeyVariants(prefix, localeId);
        Collections.reverse(keyVariants);
        keyVariants.forEach((variantPrefix) -> {
                    super.getStringMap(variantPrefix).forEach((key, value) -> {
                        String actualKey = key.replaceAll(variantPrefix, prefix);
                        map.put(actualKey, value);
                    });
                });
        return map;
    }

    /**
     * Return an ordered list of variants for the given key, using the given locale id.
     * <p>
     * @param key The key for which variants are needed.
     * @param localeId The id of the locale.
     * @return The ordered list of key variants.
     */
    protected static List<String> getOrderedKeyVariants(String key, String localeId) {
        List<String> list = new ArrayList<>();
        if ((StringUtils.isNotBlank(localeId))) {
            try {
                Locale locale = LocaleUtils.toLocale(localeId.replaceAll("-", "_"));
                list.add(locale + "." + key);
                list.add(locale.getLanguage() + "." + key);
            } catch (IllegalArgumentException e) {
                logger.warn("Error while parsing locale id '{}': {}", localeId, e.getMessage());
            }
        }
        list.add(key);
        return list;
    }

}
