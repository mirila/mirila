package nl.mirila.core.settings.modules;

import com.google.inject.AbstractModule;
import nl.mirila.core.settings.KeyValueMapProvider;
import nl.mirila.core.settings.KeyValuesProvider;

import java.util.Map;

/**
 * Bind all settings types to a single map of key values.
 * This is especially useful for testing purposes.
 */
public class KeyValuesSettingsModule extends AbstractModule {

    private final Map<String, String> keyValues;

    /**
     * Initialize a new instance.
     */
    public KeyValuesSettingsModule(Map<String, String> keyValues) {
        this.keyValues = keyValues;
    }

    @Override
    protected void configure() {
        KeyValueMapProvider mapProvider = new KeyValueMapProvider(keyValues);
        bind(KeyValuesProvider.class).toInstance(mapProvider);
    }

}
