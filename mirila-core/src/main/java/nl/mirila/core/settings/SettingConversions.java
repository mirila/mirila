package nl.mirila.core.settings;

import nl.mirila.core.converters.StringConverter;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;

/**
 * Utility interface that adds settings conversions to other types.
 * <p>
 * When implementing this interface, only two the first two methods need to be overridden. The other methods
 * use these one of these two methods.
 */
interface SettingConversions {
    
    /**
     * Return the value for the given key, wrapped in an {@link Optional}.
     */
    Optional<String> getString(String key);

    /**
     * Return a map of key values, based on the given prefix.
     */
    Map<String, String> getStringMap(String prefix);

    /**
     * Return a list of {@link String} values for the given key, using the <code>\\s*,\\s*</code> as separator.
     */
    default List<String> getStringList(String key) {
        return getStringList(key, "\\s*,\\s*");
    }

    /**
     * Return a list of {@link String} values for the given key, using the given as separator.
     */
    default List<String> getStringList(String key, String separatorRegex) {
        return getString(key)
                .map((value) -> Arrays.asList(value.split(separatorRegex)))
                .orElse(Collections.emptyList());
    }


    /**
     * Return the value of a setting in the expected value class, wrapped in an {@link Optional}.
     * <p>
     * @param valueClass The expected value class.
     * @param key The key of the setting.
     * @param <T> The type of the expected value.
     * @return The value.
     */
    default <T> Optional<T> getConvertedString(Class<T> valueClass, String key) {
        return getString(key).map((value) -> StringConverter.convertLenient(valueClass, value));
    }

    /**
     * Return a list of values of a setting in the expected value class.
     * <p>
     * @param valueClass The expected value class.
     * @param key The key of the setting.
     * @param separatorRegex The separator.
     * @param <T> The type of the expected value.
     * @return The value.
     */
    default <T> List<T> getConvertedStringList(Class<T> valueClass, String key, String separatorRegex) {
        return getStringList(key, separatorRegex).stream()
                .map((value) -> StringConverter.convertLenient(valueClass, value))
                .filter(Objects::nonNull)
                .toList();
    }

    /**
     * Return the {@link Integer} value for the given key, wrapped in an {@link Optional}.
     */
    default Optional<Integer> getInteger(String key) {
        return getConvertedString(Integer.class, key);
    }

    /**
     * Return a list of {@link Integer} values for the given key, using the <code>\\s*,\\s*</code> as separator.
     */
    default List<Integer> getIntegerList(String key) {
        return getIntegerList(key, "\\s*,\\s*");
    }

    /**
     * Return a list of {@link Integer} values for the given key, using the given as separator.
     */
    default List<Integer> getIntegerList(String key, String separatorRegex) {
        return getConvertedStringList(Integer.class, key, separatorRegex);
    }

    /**
     * Return the {@link Long} value for the given key, wrapped in an {@link Optional}.
     */
    default Optional<Long> getLong(String key) {
        return getConvertedString(Long.class, key);
    }

    /**
     * Return a list of {@link Long} values for the given key, using the <code>\\s*,\\s*</code> as separator.
     */
    default List<Long> getLongList(String key) {
        return getLongList(key, "\\s*,\\s*");
    }

    /**
     * Return a list of {@link Long} values for the given key, using the given as separator.
     */
    default List<Long> getLongList(String key, String separatorRegex) {
        return getConvertedStringList(Long.class, key, separatorRegex);
    }

    /**
     * Return the {@link BigDecimal} value for the given key, wrapped in an {@link Optional}.
     */
    default Optional<BigDecimal> getBigDecimal(String key) {
        return getConvertedString(BigDecimal.class, key);
    }

    /**
     * Return a list of {@link BigDecimal} values for the given key, using the <code>\\s*,\\s*</code> as separator.
     */
    default List<BigDecimal> getBigDecimalList(String key) {
        return getBigDecimalList(key, "\\s*,\\s*");
    }

    /**
     * Return a list of {@link BigDecimal} values for the given key, using the given as separator.
     */
    default List<BigDecimal> getBigDecimalList(String key, String separatorRegex) {
        return getConvertedStringList(BigDecimal.class, key, separatorRegex);
    }

    /**
     * Return the {@link Boolean} value for the given key, wrapped in an {@link Optional}.
     */
    default Optional<Boolean> getBoolean(String key) {
        return getConvertedString(Boolean.class, key);
    }

    /**
     * Return a list of {@link Boolean} values for the given key, using the <code>\\s*,\\s*</code> as separator.
     */
    default List<Boolean> getBooleanList(String key) {
        return getBooleanList(key, "\\s*,\\s*");
    }

    /**
     * Return a list of {@link Boolean} values for the given key, using the given as separator.
     */
    default List<Boolean> getBooleanList(String key, String separatorRegex) {
        return getConvertedStringList(Boolean.class, key, separatorRegex);
    }

    /**
     * Return the {@link LocalDate} value for the given key, wrapped in an {@link Optional}.
     */
    default Optional<LocalDate> getLocalDate(String key) {
        return getConvertedString(LocalDate.class, key);
    }

    /**
     * Return a list of {@link LocalDate} values for the given key, using the <code>\\s*,\\s*</code> as separator.
     */
    default List<LocalDate> getLocalDateList(String key) {
        return getLocalDateList(key, "\\s*,\\s*");
    }

    /**
     * Return a list of {@link LocalDate} values for the given key, using the given as separator.
     */
    default List<LocalDate> getLocalDateList(String key, String separatorRegex) {
        return getConvertedStringList(LocalDate.class, key, separatorRegex);
    }

    /**
     * Return the {@link LocalTime} value for the given key, wrapped in an {@link Optional}.
     */
    default Optional<LocalTime> getLocalTime(String key) {
        return getConvertedString(LocalTime.class, key);
    }

    /**
     * Return a list of {@link LocalTime} values for the given key, using the <code>\\s*,\\s*</code> as separator.
     */
    default List<LocalTime> getLocalTimeList(String key) {
        return getLocalTimeList(key, "\\s*,\\s*");
    }

    /**
     * Return a list of {@link LocalTime} values for the given key, using the given as separator.
     */
    default List<LocalTime> getLocalTimeList(String key, String separatorRegex) {
        return getConvertedStringList(LocalTime.class, key, separatorRegex);
    }

    /**
     * Return the {@link LocalDateTime} value for the given key, wrapped in an {@link Optional}.
     */
    default Optional<LocalDateTime> getLocalDateTime(String key) {
        return getConvertedString(LocalDateTime.class, key);
    }

    /**
     * Return a list of {@link LocalDateTime} values for the given key, using the <code>\\s*,\\s*</code> as separator.
     */
    default List<LocalDateTime> getLocalDateTimeList(String key) {
        return getLocalDateTimeList(key, "\\s*,\\s*");
    }

    /**
     * Return a list of {@link LocalDateTime} values for the given key, using the given as separator.
     */
    default List<LocalDateTime> getLocalDateTimeList(String key, String separatorRegex) {
        return getConvertedStringList(LocalDateTime.class, key, separatorRegex);

    }

}
