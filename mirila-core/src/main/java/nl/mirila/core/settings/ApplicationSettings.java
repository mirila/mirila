package nl.mirila.core.settings;

import com.google.inject.Inject;
import nl.mirila.core.exceptions.MissingSettingException;

import java.util.Locale;

/**
 * A convenience class to easily get application settings.
 */
public class ApplicationSettings {

    public static final String KEY_REALM = "app.realm";
    public static final String KEY_LOCALE = "app.locale";
    public static final String KEY_APP_NAME = "app.name";

    protected final Settings settings;

    /**
     * Initialize a new instance.
     */
    @Inject
    public ApplicationSettings(Settings settings) {
        this.settings = settings;
    }

    /**
     * Return the realm.
     * <p>
     * This setting will throw a {@link MissingSettingException} if it is not configured.
     * <p>
     * Setting: {@value KEY_REALM}.
     */
    public String getRealm() {
        return settings.getString(KEY_REALM).orElseThrow(() -> new MissingSettingException(KEY_REALM));
    }

    /**
     * Return the locale from the settings, or falls back to the default locale of the JVM.
     * <p>
     * This setting will throw a {@link MissingSettingException} if it is not configured.
     * <p>
     * Setting: {@value KEY_LOCALE}.
     */
    public String getLocale() {
        return settings.getString(KEY_LOCALE).orElseGet(() -> Locale.getDefault().toString());
    }

    /**
     * Return the name of the application.
     * <p>
     * This setting will throw a {@link MissingSettingException} if it is not configured.
     * <p>
     * Setting: {@value KEY_APP_NAME}.
     */
    public String getAppName() {
        return settings.getString(KEY_APP_NAME).orElseThrow(() -> new MissingSettingException(KEY_APP_NAME));
    }

}
