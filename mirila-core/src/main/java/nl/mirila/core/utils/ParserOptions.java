package nl.mirila.core.utils;

/**
 * The {@link ParserOptions} that can be used for converting a String to a map of String, String.
 */
public class ParserOptions {

    private String lineSeparator;
    private String valueSeparator;
    private boolean trimKeys;
    private boolean trimValues;

    /**
     * Initialize a new instance.
     */
    public ParserOptions() {
        lineSeparator = "\n";
        valueSeparator = "=";
        trimKeys = false;
        trimValues = false;
    }

    /**
     * Return the {@link ParserOptions} with the sensible defaults.
     */
    public static ParserOptions defaults() {
        return new ParserOptions();
    }

    /**
     * Set the line separator and return this instance for chaining purposes.
     **/
    public ParserOptions withLineSeparator(String lineSeparator) {
        this.lineSeparator = lineSeparator;
        return this;
    }

    /**
     * Set value separator and return this instance for chaining purposes.
     **/
    public ParserOptions withValueSeparator(String valueSeparator) {
        this.valueSeparator = valueSeparator;
        return this;
    }

    /**
     * Mark the options to make sure the keys must be trimmed and return this instance for chaining purposes.
     **/
    public ParserOptions withTrimmedKeys() {
        this.trimKeys = true;
        return this;
    }

    /**
     * Mark the options to make sure the values must be trimmed and return this instance for chaining purposes.
     **/
    public ParserOptions withTrimmedValues() {
        this.trimValues = true;
        return this;
    }

    /**
     * Return line separator.
     */
    public String getLineSeparator() {
        return lineSeparator;
    }

    /**
     * Return value separator.
     */
    public String getValueSeparator() {
        return valueSeparator;
    }

    /**
     * Return true if keys must be trimmed.
     */
    public boolean mustTrimKeys() {
        return trimKeys;
    }

    /**
     * Return true if values must be trimmed.
     */
    public boolean mustTrimValues() {
        return trimValues;
    }

}
