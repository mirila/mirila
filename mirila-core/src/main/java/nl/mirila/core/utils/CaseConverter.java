package nl.mirila.core.utils;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Convert a string from one case to another.
 * <p>
 * Each enum has its own converter to convert a string to a list of lowercase tokens,
 * and all enums have converters to convert a string into all possible cases.
 */
public enum CaseConverter {

    CAMEL_CASE(CaseConverter::splitAtCapitals),
    KEBAB_CASE((s) -> splitByDelimiter(s, "-")),
    PASCAL_CASE(CaseConverter::splitAtCapitals),
    SCREAMING_SNAKE_CASE((s) -> splitByDelimiter(s, "_")),
    SENTENCE_CASE((s) -> splitByDelimiter(s, " ")),
    SNAKE_CASE((s) -> splitByDelimiter(s, "_")),
    DOT_CASE((s) -> splitByDelimiter(s, "\\."));

    /**
     * Regular expression to split a string on capitals, based on:
     * https://stackoverflow.com/questions/2559759/how-do-i-convert-camelcase-into-human-readable-names-in-java
     */
    private static final String CAPITAL_REGEX = String.format("%s|%s|%s",
                                                              "(?<=[A-Z])(?=[A-Z][a-z])",
                                                              "(?<=[^A-Z])(?=[A-Z])",
                                                              "(?<=[A-Za-z])(?=[^A-Za-z])");


    /**
     * Method to convert a string to a list of lowercase tokens.
     */
    private final Function<String, List<String>> toLowerCaseTokens;

    /**
     * Initialize an enum instance.
     */
    CaseConverter(Function<String, List<String>> toLowerCaseTokens) {
        this.toLowerCaseTokens = toLowerCaseTokens;
    }

    /**
     * Splits the given string using a specific delimiter into lower case words.
     */
    private static List<String> splitByDelimiter(String str, String delimiter) {
        if (StringUtils.isBlank(str)) {
            return Collections.emptyList();
        }
        return Arrays.stream(str.split(delimiter))
                .map(String::trim)
                .filter(StringUtils::isNotBlank)
                .map(String::toLowerCase)
                .toList();
    }

    /**
     * Splits the given string on each capital into lower case words.
     */
    private static List<String> splitAtCapitals(String str) {
        if (StringUtils.isBlank(str)) {
            return Collections.emptyList();
        }


        return Arrays.stream(str.split(CAPITAL_REGEX))
                .map(String::toLowerCase)
                .toList();
    }

    /**
     * Capitalize the first letter of the given string.
     */
    protected static String capitalizeFirstLetter(String str) {
        return (StringUtils.isNotBlank(str))
                ? str.substring(0, 1).toUpperCase() + str.substring(1)
                : str;
    }

    /**
     * Lowercase the first letter of the given string.
     */
    protected static String lowercaseFirstLetter(String str) {
        return (StringUtils.isNotBlank(str))
                ? str.substring(0, 1).toLowerCase() + str.substring(1)
                : str;
    }

    /**
     * Return the individual, lower cased tokens in the given string as a string list.
     * <p>
     * @param str The input string.
     * @return The list of tokens.
     */
    public List<String> getTokens(String str) {
        return toLowerCaseTokens.apply(str);
    }

    /**
     * Convert the given string into a <code>camelCase</code> string.
     */
    public String toCamelCase(String str) {
        if (str == null) {
            return null;
        }
        return lowercaseFirstLetter(this.toPascalCase(str));
    }

    /**
     * Convert the given string into a <code>kebab-case</code> string.
     */
    public String toKebabCase(String str) {
        if (str == null) {
            return null;
        }
        return String.join("-", toLowerCaseTokens.apply(str));
    }

    /**
     * Convert the given string into a <code>PascalCase</code> string.
     */
    public String toPascalCase(String str) {
        if (str == null) {
            return null;
        }
        return toLowerCaseTokens.apply(str).stream()
                .map(CaseConverter::capitalizeFirstLetter)
                .collect(Collectors.joining());
    }

    /**
     * Convert the given string into a <code>SCREAMING_SNAKE_CASE</code> string.
     */
    public String toScreamingSnakeCase(String str) {
        if (str == null) {
            return null;
        }
        return String.join("_", toLowerCaseTokens.apply(str)).toUpperCase();
    }

    /**
     * Convert the given string into a <code>Sentence case</code> string.
     */
    public String toSentenceCase(String str) {
        if (str == null) {
            return null;
        }
        return CaseConverter.capitalizeFirstLetter(String.join(" ", toLowerCaseTokens.apply(str)));
    }

    /**
     * Convert the given string into a <code>snake_case</code> string.
     */
    public String toSnakeCase(String str) {
        if (str == null) {
            return null;
        }
        return String.join("_", toLowerCaseTokens.apply(str));
    }

    /**
     * Convert the given string into a <code>dot.case</code> string.
     */
    public String toDotCase(String str) {
        if (str == null) {
            return null;
        }
        return String.join(".", toLowerCaseTokens.apply(str));
    }

}
