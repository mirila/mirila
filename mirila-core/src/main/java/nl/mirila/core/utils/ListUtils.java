package nl.mirila.core.utils;

import java.util.*;
import java.util.stream.Collectors;

import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.apache.commons.lang3.StringUtils.trim;

public class ListUtils {

    /**
     * Return the value in the given list. If the value is null or the index out of bounds, an empty {@link Optional}
     * returns.
     *
     * @param list The list that contains the elements.
     * @param index The index of the element to return.
     * @param <T> The type of the element.
     * @return The element wrapped in an Optional.
     */
    public static <T> Optional<T> get(List<T> list, int index) {
        return Optional.ofNullable(getOrDefault(list, index, null));
    }

    /**
     * Return the value in the given list. If the value is null or the index out of bounds, the default value returns.
     *
     * @param list The list that contains the elements.
     * @param index The index of the element to return.
     * @param defaultValue The default value if value in the list is null, or the index out of bounds.
     * @param <T> The type of the element.
     * @return The element or the default value.
     */
    public static <T> T getOrDefault(List<T> list, int index, T defaultValue) {
        T value = ((index >= 0) && (index < list.size())) ? list.get(index) : defaultValue;
        return (value != null) ? value : defaultValue;
    }

    /**
     * Convert the given string into a {@code Map<String, String>}.
     *
     * @param string The string to process.
     * @return The map.
     */
    public static Map<String, String> toMap(String string) {
        return toMap(string, ParserOptions.defaults());
    }

    /**
     * Convert the given string into a {@code Map<String, String>} using the given {@link ParserOptions}.
     *
     * @param string The string to process.
     * @param options The options to use within the process.
     * @return The map.
     */
    public static Map<String, String> toMap(String string, ParserOptions options) {
        String lineSepChar = options.getValueSeparator();
        boolean trimKeys = options.mustTrimKeys();
        boolean trimValues = options.mustTrimValues();
        return split(string, options.getLineSeparator()).stream()
                .filter((line) -> line.contains(lineSepChar))
                .map((line) -> line.split(lineSepChar, 2))
                .filter((parts) -> parts.length == 2)
                .filter((parts) -> isNotBlank(parts[0]))
                .collect(Collectors.toMap((parts) -> (trimKeys) ? trim(parts[0]) : parts[0],
                                          (parts) -> (trimValues) ? trim(parts[1]) : parts[1],
                                          (oldValue, newValue) -> newValue));
    }

    /**
     * Split the given value, using {@link String#split(String)}, and return a {@link List} of strings.
     * <p>
     * See also: {@link ListUtils#split(String, String, int)}.
     *
     * @param str The string to split.
     * @param separator The separator to use.
     * @return The list with elements.
     */
    public static List<String> split(String str, String separator) {
        return split(str, separator, 0);
    }

    /**
     * Split the given value, using {@link String#split(String, int)}, and return a {@link List} of strings, with a
     * maximum of elements.
     * <p>
     * If the given string is null or empty, an empty list returns.
     *
     * @param str The string to split.
     * @param separator The separator to use.
     * @return The list with elements.
     */
    public static List<String> split(String str, String separator, int limit) {
        return ((str == null) || (str.isEmpty()))
            ? new ArrayList<>()
            : Arrays.stream(str.split(separator, limit)).toList();
    }
}
