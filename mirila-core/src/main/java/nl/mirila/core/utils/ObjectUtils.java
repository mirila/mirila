package nl.mirila.core.utils;

import java.util.function.Consumer;
import java.util.function.Supplier;

public class ObjectUtils {

    /**
     * Call the given setter, using the value supplied by the provider, if the given getter does not supply a valid
     * value (null or empty string).
     * <p>
     * This is useful in case of setting default values.
     * <code>
     *     setIfMissing(pojo::getLink, pojo::setLink, () -> settings.getDefaultLink());
     * </code>
     *
     * @param getter The getter that should contain a value;
     * @param setter The setter that can be used to set a new value;
     * @param provider The provider that supplies a new value;
     * @param <T> The type of the value.
     */
    public static <T> void setIfMissing(Supplier<T> getter, Consumer<T> setter, Supplier<T> provider) {
        T currentValue = getter.get();
        if ((currentValue == null) || ((currentValue instanceof String str) && (str.isEmpty()))) {
            setter.accept(provider.get());
        }
    }

}
