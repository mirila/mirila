package nl.mirila.core.exceptions;

public class InvalidStringConversionException extends Exception {

    public InvalidStringConversionException(Class<?> expectedClass, String value) {
        super(String.format("Can not convert string '%s' to a %s", value, expectedClass.getSimpleName()));
    }

}
