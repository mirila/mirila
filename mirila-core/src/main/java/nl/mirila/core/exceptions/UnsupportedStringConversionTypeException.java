package nl.mirila.core.exceptions;

public class UnsupportedStringConversionTypeException extends Exception {

    public UnsupportedStringConversionTypeException(Class<?> expectedClass) {
        super(String.format("The conversion from a string to a %s is not supported.", expectedClass.getSimpleName()));
    }

}
