package nl.mirila.core.exceptions;

public class UnsupportedDateConversionTypeException extends Exception {

    public UnsupportedDateConversionTypeException(Class<?> expectedClass) {
        super(String.format("The conversion from a date to a %s is not supported.", expectedClass.getSimpleName()));
    }

}
