package nl.mirila.core.exceptions;

public class InvalidCronPatternException extends MirilaException {

    public InvalidCronPatternException(String pattern) {
        super("The pattern '" + pattern + "' is not a valid cron expression.");
    }

}
