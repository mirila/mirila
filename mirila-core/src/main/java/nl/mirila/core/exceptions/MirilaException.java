package nl.mirila.core.exceptions;

public class MirilaException extends RuntimeException {

    private final int code;

    /**
     * Initialize a new instance with the given message. It will use 500 as default code.
     */
    public MirilaException(String message) {
        super(message);
        code = 500;
    }

    /**
     * Initialize a new instance with the given message. It will use 500 as default code.
     */
    public MirilaException(String message, Throwable cause) {
        super(message, cause);
        code = 500;
    }

    /**
     * Initialize a new instance with the given message and code.
     */
    public MirilaException(String message, int code) {
        super(message);
        this.code = code;
    }

    /**
     * Return the code. Most codes will follow the http code standard.
     */
    public int getCode() {
        return code;
    }

}
