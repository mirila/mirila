package nl.mirila.core.exceptions;

public class MissingSettingException extends RuntimeException {

    public MissingSettingException(String setting) {
        super(String.format("The setting %s is missing", setting));
    }

}
