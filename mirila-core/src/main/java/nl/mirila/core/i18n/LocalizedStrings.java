package nl.mirila.core.i18n;

import java.util.Optional;

/**
 * The internationalization interface that provides localized strings.
 */
public interface LocalizedStrings {

    /**
     * Returns a localized string for the given key, wrapped in an {@link Optional}.
     * <p>
     * @param key The key.
     * @return The localized string.
     */
    Optional<String> get(String key);

}
