package nl.mirila.core.i18n;

import com.google.inject.Inject;

import java.util.Optional;

/**
 * A {@link LocalizedStrings} dummy, that return the key or null in its Optional return value.
 * <p>
 * This method is especially useful in test cases.
 */
public class LocalizedStringsDummy implements LocalizedStrings {

    private boolean returningKeys;

    /**
     * Initializes a new instance.
     */
    @Inject
    public LocalizedStringsDummy() {
        this.returningKeys = false;
    }

    /**
     * Initializes a new instance with the given returningKeys parameter.
     */
    public LocalizedStringsDummy(boolean returningKeys) {
        this.returningKeys = returningKeys;
    }

    /**
     * Return true if the get() method should return the key or null in its Optional return value.
     */
    public boolean isReturningKeys() {
        return returningKeys;
    }

    /**
     * Set to true if the get() method should return the key or null in its Optional return value.
     */
    public void setReturningKeys(boolean returningKeys) {
        this.returningKeys = returningKeys;
    }

    /**
     * Always return the key, instead of the value.
     */
    @Override
    public Optional<String> get(String key) {
        return (returningKeys) ? Optional.of(key) : Optional.empty();
    }

}
