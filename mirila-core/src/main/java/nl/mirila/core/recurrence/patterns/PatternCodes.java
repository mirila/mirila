package nl.mirila.core.recurrence.patterns;

/**
 * The {@link PatternCodes} defines short codes to use for converting the pattern to string and back.
 */
class PatternCodes {

    protected static final String CODE_KEY_TYPE = "T";
    protected static final String CODE_KEY_TIME_UNIT = "U";
    protected static final String CODE_KEY_INTERVAL = "I";
    protected static final String CODE_KEY_REPEAT = "R";
    protected static final String CODE_KEY_FIRST_OCCURRENCE = "F";
    protected static final String CODE_KEY_SEQUENCE = "S";
    protected static final String CODE_KEY_PATTERN = "P";

    protected static final String CODE_TYPE_CRON = "C";
    protected static final String CODE_TYPE_FIXED_DELAY = "D";
    protected static final String CODE_TYPE_FIXED_RATE = "R";
    protected static final String CODE_TYPE_SEQUENCE = "S";

}
