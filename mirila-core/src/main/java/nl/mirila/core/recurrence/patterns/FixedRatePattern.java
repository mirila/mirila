package nl.mirila.core.recurrence.patterns;

import nl.mirila.core.recurrence.enums.TimeUnit;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.Optional;

import static nl.mirila.core.recurrence.patterns.PatternCodes.CODE_KEY_FIRST_OCCURRENCE;
import static nl.mirila.core.recurrence.patterns.PatternCodes.CODE_TYPE_FIXED_RATE;

/**
 * The {@link FixedRatePattern} calculates the next occurrence, based the first occurrence.
 */
public class FixedRatePattern extends RecurrencePattern
        implements Periodic<FixedRatePattern>, Repeatable<FixedRatePattern>, TimeUnitBased<FixedRatePattern> {

    private TimeUnit timeUnit;
    private int interval;
    private int repeat;
    private LocalDateTime firstOccurrence;

    /**
     * Initialize a new instance.
     */
    FixedRatePattern() {
        timeUnit = TimeUnit.MINUTELY;
        interval = 1;
        repeat = 0;
        firstOccurrence = LocalDateTime.now();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getTypeCode() {
        return CODE_TYPE_FIXED_RATE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TimeUnit getTimeUnit() {
        return timeUnit;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FixedRatePattern withTimeUnit(TimeUnit timeUnit) {
        this.timeUnit = timeUnit;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getInterval() {
        return interval;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FixedRatePattern withInterval(int interval) {
        this.interval = interval;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getRepeat() {
        return repeat;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FixedRatePattern withRepeat(int repeat) {
        this.repeat = repeat;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LocalDateTime getFirstOccurrence() {
        return firstOccurrence;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FixedRatePattern withFirstOccurrence(LocalDateTime firstOccurrence) {
        this.firstOccurrence = firstOccurrence;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<LocalDateTime> getNextOccurrence(LocalDateTime offset) {
        LocalDateTime result = getFirstOccurrence();
        int repeat = getRepeat();
        long interval = getInterval();
        int counter = 1;
        while (!result.isAfter(offset)) {
            long increment;
            if (repeat > 0) {
                if (counter > repeat) {
                    return Optional.empty();
                }
                increment = interval * counter;
            } else {
                if (counter > 1) {
                    throw new RuntimeException("Potential infinite loop detected.");
                }
                increment = getTimeUnit().getTemporalUnit().between(firstOccurrence, offset) + 1;
                increment = (increment % interval != 0)
                        ? (((increment / interval) * interval) + interval)
                        : increment;
            }
            result = getFirstOccurrence().plus(increment, getTimeUnit().getTemporalUnit());
            counter++;
        }
        return Optional.of(result);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void toCodeMap(Map<String, String> codeMap) {
        super.toCodeMap(codeMap);
        if (!codeMap.containsKey(CODE_KEY_FIRST_OCCURRENCE)) {
            // Make sure this first occurrence is added, even if the repeat is not added.
            codeMap.put(CODE_KEY_FIRST_OCCURRENCE, firstOccurrence.format(formatter));
        }
    }

}
