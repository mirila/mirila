package nl.mirila.core.recurrence.utils;

import nl.mirila.core.exceptions.InvalidCronPatternException;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.lang.Integer.parseInt;

/**
 * Utility class for cron related patterns.
 */
public class CronPatternUtils {

    private static final List<String> MONTHS = List.of("JAN", "FEB", "MAR", "APR", "MAY", "JUN",
                                                      "JUL", "AUG", "SEP", "OCT", "NOV", "DEC");

    private static final List<String> DAYS = List.of("SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT");


    /**
     * Private constructor as this is a static helper class.
     */
    private CronPatternUtils() {
        // No-op
    }

    /**
     * Returns true if the given string is a valid cron expression.
     */
    public static boolean isValidCronPattern(String pattern) {
        return pattern.matches("^((\\d+|\\*)/\\d+|\\d+(-\\d+(/\\d+)?|#[1-6])?|\\*)" +
                               "(,((\\d+|\\*)/\\d+|\\d+(-\\d+(/\\d+)?|#[1-6])?|\\*))*$");
    }

    /**
     * Return the set of seconds in the range of 0 to 59, matching the given pattern.
     */
    public static Set<Integer> getSetForSeconds(String pattern) {
        return getSetForPattern(pattern, 0, 59);
    }

    /**
     * Return the set of minutes in the range of 0 to 59, matching the given pattern.
     */
    public static Set<Integer> getSetForMinutes(String pattern) {
        return getSetForPattern(pattern, 0, 59);
    }

    /**
     * Return the set of hours in the range of 0 to 23, matching the given pattern.
     */
    public static Set<Integer> getSetForHours(String pattern) {
        return getSetForPattern(pattern, 0, 23);
    }

    /**
     * Return the set of days of the month in the range of 1 to 31, matching the given pattern.
     */
    public static Set<Integer> getSetForDaysOfMonth(String pattern) {
        return getSetForPattern(pattern, 1, 31);
    }

    /**
     * Return the set of months in the range of 1 to 12, or JAN-DEC, matching the given pattern.
     */
    public static Set<Integer> getSetForMonths(String pattern) {
        return getSetForPattern(replaceAbbreviations(pattern, MONTHS, 1), 1, MONTHS.size());
    }

    /**
     * Return the set of days of the week in the range of 0 to 6, or SUN-SAT, matching the given pattern.
     */
    public static Set<Integer> getSetForDaysOfWeek(String pattern) {
        return getSetForPattern(replaceAbbreviations(pattern, DAYS, 0), 0, DAYS.size() - 1);
    }

    /**
     * Return the set of integers in the range of min to max, matching the pattern.
     *
     * @param pattern The pattern to match.
     * @param min The minimal value of the range.
     * @param max The maximum value of the range.
     * @return The set of integers.
     */
    public static Set<Integer> getSetForPattern(String pattern, int min, int max) {
        if (!isValidCronPattern(pattern)) {
            throw new InvalidCronPatternException(pattern);
        }
        if (pattern.equals("*")) {
            return Set.of();
        }
        List<String> parts = Stream.of(pattern.split(",")).toList();
        return parts.stream()
                .flatMap((part) -> getStreamForTimeUnit(part, min, max))
                .filter((i) -> (i >= min) && (i <= max))
                .collect(Collectors.toCollection(TreeSet::new));
    }

    /**
     * Return a string for the pattern in which the abbreviations are replaced by their numeral values.
     *
     * @param pattern The pattern with abbreviations.
     * @param abbreviations The abbreviations to replace.
     * @param offset The offset to add to the index of the abbreviation in the list, to add to the number.
     * @return The pattern without abbreviations.
     */
    private static String replaceAbbreviations(String pattern, List<String> abbreviations, int offset) {
        for (int i = 0; i < abbreviations.size(); i++) {
            pattern = pattern.replace(abbreviations.get(i), Integer.toString(i + offset));
        }
        return pattern;
    }

    /**
     * Return the stream of integers for the given part, limited the values to only the range from min to max.
     *
     * @param pattern The pattern of time unit.
     * @param min The minimal value.
     * @param max The maximum value.
     * @return The stream of integers.
     */
    private static Stream<Integer> getStreamForTimeUnit(String pattern, int min, int max) {
        if (pattern.equals("*")) {
          return IntStream.rangeClosed(min, max).boxed();
        } else if (pattern.contains("/")) {
            return getIntervalStream(pattern, min, max);
        } else if (pattern.contains("-")) {
            return getRangeStream(pattern);
        } else if (pattern.contains("#")) {
            // Relative days are allowed, so we allow #, but ignore it here.
            return IntStream.empty().boxed();
        } else {
            return Stream.of(pattern).map(Integer::parseInt);
        }
    }

    /**
     * Return a stream of integers matching all integers in the given pattern.
     * <p>
     * For example. The pattern `3-5` will return a stream of 3, 4, 5 and 6.
     *
     * @param pattern The pattern.
     * @return The stream of integers.
     */
    private static Stream<Integer> getRangeStream(String pattern) {
        String[] fromTo = pattern.split("-");
        return IntStream.rangeClosed(parseInt(fromTo[0]), parseInt(fromTo[1])).boxed();
    }

    /**
     * Return a stream of integers matching all integers in the given pattern.
     * <p>
     * For example: `2/3` with a min and max of 1-12, will return a stream of 2, 5, 8, 11.
     *
     * @param pattern The pattern.
     * @return The stream of integers.
     */
    private static Stream<Integer> getIntervalStream(String pattern, int min, int max) {
        String[] parts = pattern.split("/");
        int interval = parseInt(parts[1]);
        int offset;
        if (parts[0].contains("-")) {
            String[] minMax = parts[0].split("-");
            offset = Math.max(min, parseInt(minMax[0]));
            max = Math.min(max, parseInt(minMax[1]));
        } else {
            offset = (parts[0].equals("*")) ? min : Math.max(min, parseInt(parts[0]));
        }

        return IntStream.rangeClosed(offset, max)
                .boxed()
                .filter((i) -> (i - offset) % interval == 0);
    }

    /**
     * Return a set of integers matching all integers in the given pattern. As this pattern may include relative
     * indexes, like third wednesday, the current date is needed. The month is also used for the maximum range.
     *
     * @param pattern The pattern.
     * @param current The date for which the relative.
     * @return The set of integers.
     */
    public static Set<Integer> getDaysOfWeekForMonth(String pattern, LocalDate current) {
        pattern = replaceAbbreviations(pattern, DAYS, 0);
        if (!isValidCronPattern(pattern)) {
            throw new IllegalArgumentException("Invalid cron pattern '" + pattern + "'.");
        }
        if (pattern.equals("*")) {
            return Set.of();
        }
        Set<Integer> result = new TreeSet<>();
        Set<Integer> fixedDays = getSetForDaysOfWeek(pattern);

        // Process all days
        LocalDate lastDayOfMonth = current.with(TemporalAdjusters.lastDayOfMonth());
        IntStream.rangeClosed(1, lastDayOfMonth.getDayOfMonth())
                .boxed()
                .filter((i) -> fixedDays.contains(current.withDayOfMonth(i).getDayOfWeek().getValue() % 7))
                .forEach(result::add);

        Arrays.stream(pattern.split(","))
                .filter((part) -> part.contains("#"))
                .map((part) -> part.split("#"))
                .forEach((parts) -> {
                    DayOfWeek day = cronDayOfWeekAsJavaDayOfWeek(Integer.parseInt(parts[0]));
                    int index = Integer.parseInt(parts[1]);
                    getRelativeDay(current, day, index).ifPresent(result::add);
                });
        return result;
    }

    /**
     * Return the day of month, for the relative day in the current month. If the month does not contain that day,
     * and empty {@link Optional} returns.
     *
     * @param currentMonth The current month.
     * @param dayOfWeek The day of the week.
     * @param nth The nth value. Use 1 for the first to 5 for the fifth. Number 6 is regarded as last of the month.
     * @return The day of the month, if available.
     */
    private static Optional<Integer> getRelativeDay(LocalDate currentMonth, DayOfWeek dayOfWeek, int nth) {
        LocalDate day;
        if (nth == 6) {
            day = currentMonth.with(TemporalAdjusters.lastDayOfMonth())
                    .with(TemporalAdjusters.previousOrSame(dayOfWeek));
        } else {
            day = currentMonth.withDayOfMonth(1)
                    .with(TemporalAdjusters.nextOrSame(dayOfWeek))
                    .plusWeeks(nth - 1);
        }
        return (day.getMonthValue() == currentMonth.getMonthValue())
                ? Optional.of(day.getDayOfMonth())
                : Optional.empty();
    }

    /**
     * Convert the given cron day of the week number (0-6: SUN-SAT), to a Java day of week numeral (1-7: MON-SUN).
     *
     * @param cronDay The number for a day of the week according cron.
     * @return The number for a day of the week according Java.
     */
    private static DayOfWeek cronDayOfWeekAsJavaDayOfWeek(int cronDay) {
        int correctedDay = 1 + ((cronDay + 6) % 7);
        return DayOfWeek.of(correctedDay);
    }

}
