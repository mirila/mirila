package nl.mirila.core.recurrence.patterns;

import nl.mirila.core.exceptions.InvalidCronPatternException;
import nl.mirila.core.recurrence.enums.TimeUnit;
import nl.mirila.core.utils.ListUtils;
import nl.mirila.core.utils.ParserOptions;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import static nl.mirila.core.recurrence.patterns.PatternCodes.*;

/**
 * The {@link RecurrencePattern} is a base class for other patterns.
 */
public abstract class RecurrencePattern {

    private static final Logger logger = LogManager.getLogger(RecurrencePattern.class);

    private static final String LINE_SEPARATOR = ";";
    private static final String KEY_VALUE_SEPARATOR = "=";

    protected static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");

    private static final ParserOptions options = ParserOptions.defaults()
            .withLineSeparator(LINE_SEPARATOR)
            .withValueSeparator(KEY_VALUE_SEPARATOR)
            .withTrimmedKeys()
            .withTrimmedValues();

    /**
     * Return the code for this pattern type.
     */
    protected abstract String getTypeCode();

    /**
     * Return the first possible next occurrence, using the current time as offset.
     * <p>
     * For more information: {@link RecurrencePattern#getNextOccurrence(LocalDateTime)}.
     */
    public Optional<LocalDateTime> getNextOccurrence() {
        return getNextOccurrence(LocalDateTime.now());
    }

    /**
     * Return the first possible next occurrence, after the given offset, wrapped in an {@link Optional}.
     * <p>
     * If there's no new occurrence, an empty Optional returns.
     *
     * @param offset The offset timestamp.
     * @return The next occurrence, wrapped in an Optional.
     */
    public abstract Optional<LocalDateTime> getNextOccurrence(LocalDateTime offset);

    /**
     * Return a list of {@link LocalDateTime} for the next occurrences as of now.
     */
    public List<LocalDateTime> getNextOccurrences() {
        return getNextOccurrences(LocalDateTime.now());
    }

    /**
     * Return a list of {@link LocalDateTime} for the next occurrences as of the given offset, with maximum 1000 values.
     */
    public List<LocalDateTime> getNextOccurrences(LocalDateTime offset) {
        return getNextOccurrences(offset, 1000);
    }

    /**
     * Return a list of {@link LocalDateTime} for the next occurrences as of the given offset.
     *
     * @param offset The offset date to use.
     * @param limit The maximum amount of occurrences in the list.
     * @return The list of occurrences.
     */
    public List<LocalDateTime> getNextOccurrences(LocalDateTime offset, int limit) {
        Optional<LocalDateTime> optional = getNextOccurrence(offset);
        List<LocalDateTime> list = new ArrayList<>();
        while ((optional.isPresent()) && limit-- > 0) {
            LocalDateTime current = optional.get();
            list.add(current);
            optional = getNextOccurrence(current);
        }
        return list;
    }

    /**
     * Set fields based on their values in the given code map.
     *
     * @param codeMap String map with codes.
     */
    protected void fromCodeMap(Map<String, String> codeMap) {
        if (this instanceof TimeUnitBased<?> timeUnitBased) {
            timeUnitBased.withTimeUnit(TimeUnit.findByCode(codeMap.get(CODE_KEY_TIME_UNIT)).orElse(TimeUnit.MINUTELY));
        }
        if (this instanceof Periodic<?> periodic) {
            periodic.withInterval(NumberUtils.toInt(codeMap.get(CODE_KEY_INTERVAL), 1));
        }
        if (this instanceof Repeatable<?> repeatable) {
            repeatable.withRepeat(NumberUtils.toInt(codeMap.get(CODE_KEY_REPEAT), 0));
            try {
                repeatable.withFirstOccurrence(LocalDateTime.parse(codeMap.get(CODE_KEY_FIRST_OCCURRENCE)));
            } catch (Exception ignore) {
            }
        }

    }

    /**
     * Add fields to the given code map.
     *
     * @param codeMap String map with codes.
     */
    protected void toCodeMap(Map<String, String> codeMap) {
        codeMap.put(CODE_KEY_TYPE, getTypeCode());
        if (this instanceof TimeUnitBased<?> timeUnitBased) {
            codeMap.put(CODE_KEY_TIME_UNIT, timeUnitBased.getTimeUnit().getCode());
        }
        if (this instanceof Periodic<?> periodic) {
            int interval = periodic.getInterval();
            if (interval > 1) {
                codeMap.put(CODE_KEY_INTERVAL, Integer.toString(interval));
            }
        }
        if (this instanceof Repeatable<?> repeatable) {
            int repeat = repeatable.getRepeat();
            if (repeat > 0) {
                codeMap.put(CODE_KEY_REPEAT, Integer.toString(repeat));
                codeMap.put(CODE_KEY_FIRST_OCCURRENCE, repeatable.getFirstOccurrence().format(formatter));
            }
        }
    }

    /**
     * Return a {@link FixedRatePattern}.
     *
     * @return The fixed rate pattern.
     */
    public static FixedRatePattern forRate() {
        return new FixedRatePattern();
    }

    /**
     * Return a {@link FixedDelayPattern}.
     *
     * @return The fixed rate pattern.
     */
    public static FixedDelayPattern forDelay() {
        return new FixedDelayPattern();
    }

    /**
     * Return a {@link CronPattern}.
     *
     * @return The fixed rate pattern.
     */
    public static CronPattern forCronPattern() {
        return new CronPattern();
    }

    /**
     * Return a {@link SequencePattern}.
     *
     * @return The fixed rate pattern.
     */
    public static SequencePattern forSequence() {
        return new SequencePattern();
    }

    /**
     * Return a code based string representation of the pattern.
     */
    public String toCodes() {
        Map<String, String> map = new LinkedHashMap<>();
        toCodeMap(map);
        return map.entrySet().stream()
                .map((entry) -> entry.getKey() + KEY_VALUE_SEPARATOR + entry.getValue())
                .collect(Collectors.joining(LINE_SEPARATOR));
    }

    /**
     * Return a pattern based on the given string representation..
     */
    public static Optional<? extends RecurrencePattern> fromCodes(String codes) {
        if (StringUtils.isBlank(codes)) {
            return Optional.empty();
        }
        Map<String, String> codeMap = ListUtils.toMap(codes, options);
        String type = codeMap.getOrDefault(CODE_KEY_TYPE, "").toUpperCase();
        Optional<? extends RecurrencePattern> optional = switch (type) {
            case CODE_TYPE_CRON -> Optional.of(forCronPattern());
            case CODE_TYPE_FIXED_DELAY -> Optional.of(forDelay());
            case CODE_TYPE_FIXED_RATE -> Optional.of(forRate());
            case CODE_TYPE_SEQUENCE -> Optional.of(forSequence());
            default -> Optional.empty();
        };
        try {
            optional.ifPresent((pattern) -> pattern.fromCodeMap(codeMap));
            return optional;
        } catch (InvalidCronPatternException e) {
            logger.warn(e.getMessage() + " Ignoring this recurrence pattern.");
            return Optional.empty();
        }
    }

}
