package nl.mirila.core.recurrence.enums;

import nl.mirila.core.utils.ListUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.stream.Stream;

/**
 * The {@link SequenceFunction} is on enumeration of sequence function.
 */
public enum SequenceFunction {

    /**
     * This method returns the values for the first 45 elements of the
     * <a href="https://en.wikipedia.org/wiki/Fibonacci_number">Fibonacci numbers</a>.
     **/
    FIBONACCI("fibonacci", "FIB", (n, sequence) -> {
        if (sequence.isEmpty()) {
            int previous = 0;
            int current = 1;
            sequence.add(previous); // For N=0
            sequence.add(current); // For N=1
            for (int i = 2; i <= 45; i++) {
                int next = current + previous;
                previous = current;
                current = next;
                sequence.add(current);
            }
        }
        return ListUtils.get(sequence, n);
    }),

    /**
     * This method returns the value of one of the first 28 elements of the
     * <a href="https://en.wikipedia.org/wiki/Lucas_number">Lucas numbers</a>.
     **/
    LUCAS("lucas", "LUC", (n, sequence) -> {
        if (sequence.isEmpty()) {
            int previous = 2;
            int current = 1;
            sequence.add(previous); // For N=0;
            sequence.add(current); // For N=1;
            for (int i = 2; i <= 28; i++) {
                int next = current + previous;
                previous = current;
                current = next;
                sequence.add(current);
            }
        }
        return ListUtils.get(sequence, n);
    }),

    /**
     * This method returns a product of one of the first 12 elements of
     * <a href="https://en.wikipedia.org/wiki/Factorial">factorial numbers</a>.
     **/
    FACTORIAL("factorial", "FACT", (n, sequence) -> {
        if (sequence.isEmpty()) {
            int sum = 1;
            sequence.add(sum); // For N=0;
            for (int i = 1; i <= 12; i++) {
                sum *= i;
                sequence.add(sum);
            }
        }
        return ListUtils.get(sequence, n);
    });

    private final String name;
    private final String code;
    private final BiFunction<Integer, List<Integer>, Optional<Integer>> function;
    private final List<Integer> sequence;

    /**
     * Initialize a new instance.
     */
    SequenceFunction(String name, String code, BiFunction<Integer, List<Integer>, Optional<Integer>> function) {
        this.name = name;
        this.code = code;
        this.function = function;
        this.sequence = new ArrayList<>();
    }

    /**
     * Return name.
     */
    public String getName() {
        return name;
    }

    /**
     * Return code.
     */
    public String getCode() {
        return code;
    }

    /**
     * Return the value for the given Nth element.
     */
    public Optional<Integer> getValue(int n) {
        return function.apply(n, sequence);
    }

    /**
     * Find the {@link SequenceFunction}, using the given name. If not found, an empty {@link Optional} is returned.
     *
     * @param name The name of the SequenceName to find.
     * @return The SequenceName, wrapped in an {@link Optional}.
     */
    public static Optional<SequenceFunction> findByName(String name) {
        return Stream.of(SequenceFunction.values())
                .filter((type) -> type.getName().equalsIgnoreCase(name))
                .findFirst();
    }

    /**
     * Find the {@link SequenceFunction}, using the given code. If not found, an empty {@link Optional} is returned.
     *
     * @param code The name of the SequenceName to find.
     * @return The SequenceName, wrapped in an {@link Optional}.
     */
    public static Optional<SequenceFunction> findByCode(String code) {
        return Stream.of(SequenceFunction.values())
                .filter((type) -> type.getCode().equalsIgnoreCase(code))
                .findFirst();
    }
}
