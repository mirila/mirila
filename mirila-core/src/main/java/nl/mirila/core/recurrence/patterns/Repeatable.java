package nl.mirila.core.recurrence.patterns;

import java.time.LocalDateTime;

/**
 * The {@link Repeatable} implements a repeat and first occurrence.
 */
public interface Repeatable<T extends RecurrencePattern> {

    /**
     * Return the amount of repeats.
     */
    int getRepeat();

    /**
     * Set the amount of repeats and return this instance for chaining purposes.
     **/
    T withRepeat(int repeat);

    /**
     * Return the first occurrence.
     */
    LocalDateTime getFirstOccurrence();

    /**
     * Set the first occurrence and return this instance for chaining purposes.
     **/
    T withFirstOccurrence(LocalDateTime firstOccurrence);

}
