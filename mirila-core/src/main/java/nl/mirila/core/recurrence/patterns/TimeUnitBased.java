package nl.mirila.core.recurrence.patterns;

import nl.mirila.core.recurrence.enums.TimeUnit;

/**
 * The {@link TimeUnitBased} implements a time unit.
 */
public interface TimeUnitBased<T> {

    /**
     * Return the {@link TimeUnit} to use.
     */
    TimeUnit getTimeUnit();

    /**
     * Set the {@link TimeUnit} to use and return this instance for chaining purposes.
     **/
    T withTimeUnit(TimeUnit timeUnit);

}
