# Recurrence pattern

A pattern describes generally how an event should repeat.

## Types

### Fixed rate
Events with a regular recurrence pattern have repeat at fixed time unit.
The next occurrence is calculated based on the first occurrence, meaning the start timestamps of the recurrence match
the same interval.

### Fixed delay
Events with a delayed recurrence pattern have a interval of exactly the .
The next occurrence is calculated based on a given offset (often 'now'), meaning the timestamp the last
occurrence ended, has a gap in size o
Example: A task runs with 5 minutes gaps in between.  

### Cron
This pattern supports cron expressions, as followed:
```text
*  *  *  *  *  *
|  |  |  |  |  |
|  |  |  |  |  +----- Day of the week (0-6 or SUN-SAT)
|  |  |  |  +-------- Month (1-12 or JAN-DEC)
|  |  |  +----------- Day of the month (1-31)
|  |  +-------------- Hour (0-23)
|  +----------------- Minute (0-59)
+-------------------- Second (0-59)
```

| Example    | Name                | Description                                                         |
|:-----------|:--------------------|:--------------------------------------------------------------------|
| *          | Asterisk            | Each possible value.                                                |
| 5          | Single              | The fifth value.                                                    |
| 2,3,14     | List                | The second, third and fourteenth value for that unit.               |
| 4-8,9-13/2 | Range               | The fourth to the eight value for that unit.                        |
| 2/3        | Interval            | The second value and then every 3rd value after that.               |
| 1#3        | Relative days       | The third Monday of the month. Use 6 for the last day of the month. |
| MON        | Day abbreviations   | The days, from MON to SUN, only for day of the week.                |   
| JAN        | Month abbreviations | The months, from JAN to DEC, only for the month.                    |   

- The list may contain a combination of asterisk, single values, ranges, intervals and abbreviations.
- Intervals may be combined with asterisks, single values, ranges and abbreviations.
- Relative days may only be used on day of the week.

### Sequence
The occurrence is based on a predefined sequence, like Fibonnaci, Lucas numbers or prima numbers.


## Attributes

|                  | Fixed rate | Fixed delay | Cron | Sequence |
|------------------|:----------:|:-----------:|:----:|:--------:|
| Time unit        |     ✔      |      ✔      |  -   |    ✔     |
| Interval         |     ✔      |      ✔      |  -   |    -     |
| Repeat           |     ✔      |      -      |  ✔   |    ✔     |
| First Occurrence |     ✔      |      -      |  ✔   |    ✔     |
| Type             |     -      |      -      |  -   |    ✔     |




