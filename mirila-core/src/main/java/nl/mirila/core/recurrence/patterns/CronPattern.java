package nl.mirila.core.recurrence.patterns;

import nl.mirila.core.exceptions.InvalidCronPatternException;
import nl.mirila.core.recurrence.utils.CronPatternUtils;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDateTime;
import java.time.temporal.TemporalAdjusters;
import java.util.*;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static nl.mirila.core.recurrence.patterns.PatternCodes.CODE_KEY_PATTERN;
import static nl.mirila.core.recurrence.patterns.PatternCodes.CODE_TYPE_CRON;
import static nl.mirila.core.recurrence.utils.CronPatternUtils.*;

/**
 *
 *
 * This pattern supports cron expressions, as followed:
 * <pre>
 * *  *  *  *  *  *
 * |  |  |  |  |  |
 * |  |  |  |  |  +----- Day of the week (0-6 or SUN-SAT)
 * |  |  |  |  +-------- Month (1-12 or JAN-DEC)
 * |  |  |  +----------- Day of the month (1-31)
 * |  |  +-------------- Hour (0-23)
 * |  +----------------- Minute (0-59)
 * +-------------------- Second (0-59)
 * </pre>
 *
 * <table border="1">
 *   <thead>
 *     <tr>
 *       <th>Example</th>
 *       <th>Name</th>
 *       <th>Description</th>
 *     </tr>
 *   </thead>
 *   <tbody>
 *     <tr>
 *       <td>*</td>
 *       <td>Wildcard</td>
 *       <td>Each possible value.</td>
 *     </tr>
 *     <tr>
 *       <td>5</td>
 *       <td>Single</td>
 *       <td>The fifth value.</td>
 *     </tr>
 *     <tr>
 *       <td>2,3,14</td>
 *       <td>List</td>
 *       <td>The second, third and fourteenth value.</td>
 *     </tr>
 *     <tr>
 *       <td>4-8,9-13/2</td>
 *       <td>Range</td>
 *       <td>The fourth to the eighth value, and odd numbers between 9 and 13.</td>
 *     </tr>
 *     <tr>
 *       <td>2/3</td>
 *       <td>Interval</td>
 *       <td>The second value and then every 3rd subsequent value.</td>
 *     </tr>
 *     <tr>
 *       <td>1#3</td>
 *       <td>Days</td>
 *       <td>The third Monday of the month.</td>
 *     </tr>
 *     <tr>
 *       <td>MON</td>
 *       <td>Day abbreviations</td>
 *       <td>The days, from MON to SUN, only for day of the week.</td>
 *     </tr>
 *     <tr>
 *       <td>JAN</td>
 *       <td>Month abbreviations</td>
 *       <td>The months, from JAN to DEC, only for the month.</td>
 *     </tr>
 *   </tbody>
 * </table>
 *
 * <ul>
 *   <li>The list may contain a combination of asterisk, single values, ranges, intervals and abbreviations.</li>
 *   <li>Intervals may be combined with asterisks, single values, ranges and abbreviations.</li>
 *   <li>Relative days may only be used on day of the week. 1 for first, 2 for second, etc. 6 is used for 'last'.</li>
 * </ul>
 */
public class CronPattern extends RecurrencePattern implements Repeatable<CronPattern> {

    private Set<Integer> secondsSet;
    private Set<Integer> minutesSet;
    private Set<Integer> hoursSet;
    private Set<Integer> daysOfMonthSet;
    private Set<Integer> monthsSet;

    private String seconds;
    private String minutes;
    private String hours;
    private String daysOfMonth;
    private String months;
    private String daysOfWeek;

    private int repeat;
    private LocalDateTime firstOccurrence;

    /**
     * Initialize a new instance.
     */
    CronPattern() {
        super();
        firstOccurrence = LocalDateTime.now();
        withSecondsPattern("0");
        withMinutesPattern("*");
        withHoursPattern("*");
        withDaysOfMonthPattern("*");
        withMonthsPattern("*");
        withDaysOfWeekPattern("*");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getTypeCode() {
        return CODE_TYPE_CRON;
    }

    /**
     * Configure the pattern with the given cron expression.
     */
    public CronPattern using(String pattern) {
        if (StringUtils.isNotBlank(pattern)) {
            List<String> parts = Arrays.stream(pattern.split("\\s+"))
                    .collect(Collectors.toCollection(ArrayList::new));
            if (parts.size() == 5) {
                parts.add(0, "0");
            }
            if (parts.size() == 6) {
                withSecondsPattern(parts.get(0));
                withMinutesPattern(parts.get(1));
                withHoursPattern(parts.get(2));
                withDaysOfMonthPattern(parts.get(3));
                withMonthsPattern(parts.get(4));
                withDaysOfWeekPattern(parts.get(5));
            } else {
                throw new InvalidCronPatternException(pattern);
            }

        }
        return this;
    }

    /**
     * Return the cron pattern.
     */
    public String getPattern() {
        return StringUtils.joinWith(" ", seconds, minutes, hours, daysOfMonth, months, daysOfWeek);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getRepeat() {
        return repeat;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CronPattern withRepeat(int repeat) {
        this.repeat = repeat;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LocalDateTime getFirstOccurrence() {
        return firstOccurrence;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CronPattern withFirstOccurrence(LocalDateTime firstOccurrence) {
        this.firstOccurrence = firstOccurrence;
        return this;
    }

    /**
     * Return the seconds pattern.
     */
    public String getSeconds() {
        return seconds;
    }

    /**
     * Set the seconds pattern and return this instance for chaining purposes.
     **/
    public CronPattern withSecondsPattern(String secondsPattern) {
        this.seconds = secondsPattern;
        this.secondsSet = getSetForSeconds(secondsPattern);
        return this;
    }

    /**
     * Return the minutes pattern.
     */
    public String getMinutes() {
        return minutes;
    }

    /**
     * Set the minutes pattern and return this instance for chaining purposes.
     **/
    public CronPattern withMinutesPattern(String minutesPattern) {
        this.minutes = minutesPattern;
        this.minutesSet = getSetForMinutes(minutesPattern);
        return this;
    }

    /**
     * Return the hours pattern.
     */
    public String getHours() {
        return hours;
    }

    /**
     * Set the hours pattern and return this instance for chaining purposes.
     **/
    public CronPattern withHoursPattern(String hoursPattern) {
        this.hours = hoursPattern;
        this.hoursSet = getSetForHours(hoursPattern);
        return this;
    }

    /**
     * Return the days of month pattern.
     */
    public String getDaysOfMonth() {
        return daysOfMonth;
    }

    /**
     * Set the days of month pattern and return this instance for chaining purposes.
     **/
    public CronPattern withDaysOfMonthPattern(String daysOfMonthPattern) {
        this.daysOfMonth = daysOfMonthPattern;
        this.daysOfMonthSet = getSetForDaysOfMonth(daysOfMonthPattern);
        return this;
    }

    /**
     * Return the months pattern.
     */
    public String getMonths() {
        return months;
    }

    /**
     * Set the months pattern and return this instance for chaining purposes.
     **/
    public CronPattern withMonthsPattern(String monthsPattern) {
        this.months = monthsPattern;
        this.monthsSet = getSetForMonths(monthsPattern);
        return this;
    }

    /**
     * Return the days of week pattern.
     */
    public String getDaysOfWeek() {
        return daysOfWeek;
    }

    /**
     * Set the days of week pattern and return this instance for chaining purposes.
     **/
    public CronPattern withDaysOfWeekPattern(String daysOfWeekPattern) {
        this.daysOfWeek = daysOfWeekPattern;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<LocalDateTime> getNextOccurrence(LocalDateTime offset) {
        int repeat = getRepeat();
        LocalDateTime first = getFirstOccurrence();
        LocalDateTime next = (repeat == 0) ? offset : first;

        int counter = 0;
        while (!next.isAfter(offset)) {
            if ((repeat > 0) && (counter >= repeat)) {
                return Optional.empty();
            } else if ((repeat == 0) && (counter > 0)) {
                throw new RuntimeException("Potential infinite loop detected.");
            }
            counter++;
            next = increaseSecond(next);
            next = increaseIfNeeded(next, minutesSet, next::getMinute, this::increaseMinute);
            next = increaseIfNeeded(next, hoursSet, next::getHour, this::increaseHour);
            Set<Integer> days = combineDays(next);
            next = increaseIfNeeded(next, days, next::getDayOfMonth, (dt) -> increaseDayOfMonth(dt, days));
            next = increaseIfNeeded(next, monthsSet, next::getMonthValue, this::increaseMonth);
        }
        return Optional.of(next);
    }

    /**
     * Return the set of all possible days in the month of the current {@link LocalDateTime}, using the patterns of
     * both the days of the week, and the days of the months.
     *
     * @param current The current date-time
     * @return The set with possible days in the current month.
     */
    private Set<Integer> combineDays(LocalDateTime current) {
        Set<Integer> days = new TreeSet<>(daysOfMonthSet);
        days.addAll(CronPatternUtils.getDaysOfWeekForMonth(daysOfWeek, current.toLocalDate()));
        return days;
    }

    /**
     * Helper method that increase the value of a time unit, if this is needed, based on the possible values.
     * In this method, we don't know the actual time unit.
     *
     * @param current The current datetime.
     * @param set The set that may contain the possible values.
     * @param getter The supplier that provides the current value,
     * @param incrementer The function to invoke if the unit needs to be incremented.
     * @return The new or current datetime.
     */
    private LocalDateTime increaseIfNeeded(LocalDateTime current,
                                           Set<Integer> set,
                                           Supplier<Integer> getter,
                                           Function<LocalDateTime, LocalDateTime> incrementer) {
        return ((!set.isEmpty()) && (!set.contains(getter.get()))) ? incrementer.apply(current) : current;
    }

    /**
     * Helper method that resets the time unit to the lowest possible value.
     * In this method, we don't know the actual time unit.
     *
     * @param set The set with possible values.
     * @param setter The function that sets the lowest possible value on the datetime.
     * @param min The minimal default value, in case the set is empty.
     * @param resetter The chained reset method for the next smaller time unit.
     * @return The reset datetime.
     */
    private LocalDateTime reset(Set<Integer> set,
                                Function<Integer, LocalDateTime> setter,
                                int min,
                                Function<LocalDateTime, LocalDateTime> resetter) {
        Integer resetValue = set.stream().findFirst().orElse(min);
        return (resetter == null)
                ? setter.apply(resetValue)
                : resetter.apply(setter.apply(resetValue));
    }

    /**
     * Return a new {@link LocalDateTime} based on the current datetime, of which the second is reset.
     */
    private LocalDateTime resetSecond(LocalDateTime current) {
        return reset(secondsSet, current::withSecond, 0, null);
    }

    /**
     * Return a new {@link LocalDateTime} based on the current datetime, of which the minute and second are reset.
     */
    private LocalDateTime resetMinute(LocalDateTime current) {
        return reset(minutesSet, current::withMinute, 0, this::resetSecond);
    }

    /**
     * Return a new {@link LocalDateTime} based on the current datetime, of which the hour, minute and second are reset.
     */
    private LocalDateTime resetHour(LocalDateTime current) {
        return reset(hoursSet, current::withHour, 0, this::resetMinute);
    }

    /**
     * Return a new {@link LocalDateTime} based on the current datetime, of which the day index and time are reset.
     */
    private LocalDateTime resetDayOfMonth(LocalDateTime current) {
        return reset(combineDays(current), current::withDayOfMonth, 1, this::resetHour);
    }

    /**
     * Return a new {@link LocalDateTime} based on the current datetime, of which the month, day and time are reset.
     */
    private LocalDateTime resetMonth(LocalDateTime current) {
        return reset(monthsSet, current::withMonth, 1, this::resetDayOfMonth);
    }

    /**
     * Helper method that increases the value of a time unit. In case of overflowing, the next time unit will be
     * incremented, and the smaller time units will be reset.
     *
     * @param current The current datetime.
     * @param getter The supplier that provides the value of the related time unit.
     * @param setter The function that updates the value of the related time unit.
     * @param set The set of possible values.
     * @param max The maximum value for the related time unit.
     * @param incrementer The function that increments the next larger time unit.
     * @param resetter The function that resets the smaller time units.
     * @return The update datetime.
     */
    private LocalDateTime increase(LocalDateTime current,
                                   Supplier<Integer> getter,
                                   Function<Integer, LocalDateTime> setter,
                                   Set<Integer> set,
                                   int max,
                                   Function<LocalDateTime, LocalDateTime> incrementer,
                                   Function<LocalDateTime, LocalDateTime> resetter) {
        int currentValue = getter.get();
        int newValue = (set.isEmpty())
                ? currentValue + 1
                : set.stream().filter((i) -> i > currentValue).findFirst().orElse(max);
        return (newValue >= max)
                ? incrementer.apply(current)
                : resetter.apply(setter.apply(newValue));
    }

    /**
     * Return a new {@link LocalDateTime} in which the second is incremented.
     */
    private LocalDateTime increaseSecond(LocalDateTime old) {
        return increase(old, old::getSecond, old::withSecond, secondsSet, 60, this::increaseMinute, (dt) -> dt);
    }

    /**
     * Return a new {@link LocalDateTime} in which the minute is incremented.
     */
    private LocalDateTime increaseMinute(LocalDateTime old) {
        return increase(old, old::getMinute, old::withMinute, minutesSet, 60, this::increaseHour, this::resetSecond);
    }

    /**
     * Return a new {@link LocalDateTime} in which the hour is incremented.
     */
    private LocalDateTime increaseHour(LocalDateTime old) {
        return increase(old, old::getHour, old::withHour, hoursSet, 24, this::increaseDayOfMonth, this::resetMinute);
    }

    /**
     * Return a new {@link LocalDateTime} in which the day of the month is incremented.
     */
    private LocalDateTime increaseDayOfMonth(LocalDateTime old) {
        return increaseDayOfMonth(old, combineDays(old));
    }

    /**
     * Return a new {@link LocalDateTime} in which the day of the month is incremented, using the given set.
     */
    private LocalDateTime increaseDayOfMonth(LocalDateTime old, Set<Integer> combinedSet) {
        int max = old.with(TemporalAdjusters.lastDayOfMonth()).getDayOfMonth();
        return increase(old, old::getDayOfMonth, old::withDayOfMonth, combinedSet, max, this::increaseMonth, this::resetHour);
    }

    /**
     * Return a new {@link LocalDateTime} in which the month is incremented.
     */
    private LocalDateTime increaseMonth(LocalDateTime old) {
        return increase(old, old::getMonthValue, old::withMonth,
                        monthsSet, 12, (dt) -> resetMonth(dt.plusYears(1)), this::resetDayOfMonth);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void fromCodeMap(Map<String, String> codeMap) {
        super.fromCodeMap(codeMap);
        using(codeMap.get(CODE_KEY_PATTERN));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void toCodeMap(Map<String, String> codeMap) {
        super.toCodeMap(codeMap);
        codeMap.put(CODE_KEY_PATTERN, getPattern());
    }

}