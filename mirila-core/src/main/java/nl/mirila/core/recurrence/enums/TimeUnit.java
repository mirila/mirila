package nl.mirila.core.recurrence.enums;

import java.time.temporal.TemporalUnit;
import java.util.Optional;
import java.util.stream.Stream;

import static java.time.temporal.ChronoUnit.*;

/**
 * The enumeration of recurrence types.
 */
public enum TimeUnit {
    SECONDLY("secondly", "S", SECONDS),
    MINUTELY("minutely", "I", MINUTES),
    HOURLY("hourly", "H", HOURS),
    DAILY("daily", "D", DAYS),
    WEEKLY("weekly", "W", WEEKS),
    MONTHLY("monthly", "M", MONTHS),
    YEARLY("yearly", "Y", YEARS);

    private final String name;
    private final String code;
    private final TemporalUnit temporalUnit;

    /**
     * Initialize a new instance.
     */
    TimeUnit(String name, String code, TemporalUnit temporalUnit) {
        this.name = name;
        this.code = code;
        this.temporalUnit = temporalUnit;
    }

    /**
     * Return the human-friendly name.
     */
    public String getName() {
        return name;
    }

    /**
     * Return code.
     */
    public String getCode() {
        return code;
    }

    /**
     * Return temporalUnit.
     */
    public TemporalUnit getTemporalUnit() {
        return temporalUnit;
    }

    /**
     * Return the name of the enum.
     */
    @Override
    public String toString() {
        return name;
    }

    /**
     * Find the RecurrenceType, using the given name. If not found, an empty {@link Optional} is returned.
     *
     * @param name The name of the RecurrenceType to find.
     * @return The RecurrenceType, wrapped in an {@link Optional}.
     */
    public static Optional<TimeUnit> findByName(String name) {
        return Stream.of(TimeUnit.values())
                .filter((type) -> type.getName().equalsIgnoreCase(name))
                .findFirst();
    }

    /**
     * Find the RecurrenceType, using the given code. If not found, an empty {@link Optional} is returned.
     *
     * @param code The code of the RecurrenceType to find.
     * @return The RecurrenceType, wrapped in an {@link Optional}.
     */
    public static Optional<TimeUnit> findByCode(String code) {
        return Stream.of(TimeUnit.values())
                .filter((type) -> type.getCode().equalsIgnoreCase(code))
                .findFirst();
    }

    /**
     * Find the RecurrenceType, using the given {@link TemporalUnit}. If not found, an empty {@link Optional} is
     * returned.
     *
     * @param unit The temporal unit of the RecurrenceType to find.
     * @return The RecurrenceType, wrapped in an {@link Optional}.
     */
    public static Optional<TimeUnit> findByTemporalUnit(TemporalUnit unit) {
        return Stream.of(TimeUnit.values())
                .filter((type) -> type.getTemporalUnit().equals(unit))
                .findFirst();
    }

}
