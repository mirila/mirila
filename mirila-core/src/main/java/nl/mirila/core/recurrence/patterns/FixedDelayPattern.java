package nl.mirila.core.recurrence.patterns;

import nl.mirila.core.recurrence.enums.TimeUnit;

import java.time.LocalDateTime;
import java.util.Optional;

import static nl.mirila.core.recurrence.patterns.PatternCodes.CODE_TYPE_FIXED_DELAY;

/**
 * The {@link FixedDelayPattern} calculates the next occurrence, based on the given offset.
 */
public class FixedDelayPattern extends RecurrencePattern
        implements Periodic<FixedDelayPattern>, TimeUnitBased<FixedDelayPattern> {

    private int interval;
    private TimeUnit timeUnit;

    /**
     * Initialize a new instance.
     */
    FixedDelayPattern() {
        interval = 1;
        timeUnit = TimeUnit.MINUTELY;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getTypeCode() {
        return CODE_TYPE_FIXED_DELAY;
    }

    /**
     * Return the interval.
     */
    public int getInterval() {
        return interval;
    }

    /**
     * Set the interval and return this instance for chaining purposes.
     **/
    public FixedDelayPattern withInterval(int interval) {
        this.interval = interval;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TimeUnit getTimeUnit() {
        return timeUnit;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FixedDelayPattern withTimeUnit(TimeUnit timeUnit) {
        this.timeUnit = timeUnit;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<LocalDateTime> getNextOccurrence(LocalDateTime offset) {
        return Optional.of(offset.plus(interval, timeUnit.getTemporalUnit()));
    }

}
