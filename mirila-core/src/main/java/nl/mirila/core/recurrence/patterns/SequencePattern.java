package nl.mirila.core.recurrence.patterns;

import nl.mirila.core.recurrence.enums.SequenceFunction;
import nl.mirila.core.recurrence.enums.TimeUnit;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static nl.mirila.core.recurrence.patterns.PatternCodes.CODE_KEY_SEQUENCE;
import static nl.mirila.core.recurrence.patterns.PatternCodes.CODE_TYPE_SEQUENCE;

/**
 * The {@link SequencePattern} calculates the next occurrence, based on sequence.
 */
public class SequencePattern extends RecurrencePattern
        implements Repeatable<SequencePattern>, TimeUnitBased<SequencePattern> {

    private TimeUnit timeUnit;
    private int repeat;
    private LocalDateTime firstOccurrence;

    private final List<Integer> sequenceList;
    private SequenceFunction sequenceFunction;

    /**
     * Initialize a new instance.
     */
    SequencePattern() {
        timeUnit = TimeUnit.MINUTELY;
        repeat = 0;
        firstOccurrence = LocalDateTime.now();
        sequenceList = new ArrayList<>();
        sequenceFunction = null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getTypeCode() {
        return CODE_TYPE_SEQUENCE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TimeUnit getTimeUnit() {
        return timeUnit;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SequencePattern withTimeUnit(TimeUnit timeUnit) {
        this.timeUnit = timeUnit;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getRepeat() {
        return repeat;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SequencePattern withRepeat(int repeat) {
        this.repeat = repeat;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LocalDateTime getFirstOccurrence() {
        return firstOccurrence;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SequencePattern withFirstOccurrence(LocalDateTime firstOccurrence) {
        this.firstOccurrence = firstOccurrence;
        return this;
    }

    /**
     * Return sequence.
     */
    public List<Integer> getSequenceList() {
        return sequenceList;
    }

    /**
     * Set sequence and return this instance for chaining purposes.
     **/
    public SequencePattern withSequence(int... sequence) {
        Arrays.stream(sequence).forEach(this.sequenceList::add);
        return this;
    }

    /**
     * Set sequence and return this instance for chaining purposes.
     **/
    public SequencePattern withSequence(List<Integer> sequence) {
        this.sequenceList.addAll(sequence);
        return this;
    }

    /**
     * Return sequenceName.
     */
    public Optional<SequenceFunction> getSequenceFunction() {
        return Optional.ofNullable(sequenceFunction);
    }

    /**
     * Set sequenceName and return this instance for chaining purposes.
     **/
    public SequencePattern withSequenceFunction(SequenceFunction sequenceFunction) {
        this.sequenceFunction = sequenceFunction;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<LocalDateTime> getNextOccurrence(LocalDateTime offset) {
        LocalDateTime result = getFirstOccurrence();
        int repeat = getRepeat();
        int counter = 0;
        Integer interval = 0;
        while (!result.isAfter(offset)) {
            if ((repeat > 0) && (counter >= repeat)) {
                return Optional.empty();
            }
            if (sequenceFunction != null) {
                Optional<Integer> optional = sequenceFunction.getValue(counter + 1);
                if (optional.isEmpty()) {
                    return Optional.empty();
                }
                interval += optional.get();
            } else if (counter < sequenceList.size()) {
                interval += sequenceList.get(counter);
            } else {
                return Optional.empty();
            }
            result = getFirstOccurrence().plus(interval, getTimeUnit().getTemporalUnit());
            counter++;
        }
        return Optional.of(result);    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void fromCodeMap(Map<String, String> codeMap) {
        super.fromCodeMap(codeMap);
        String value = codeMap.getOrDefault(CODE_KEY_SEQUENCE, "");
        SequenceFunction.findByCode(value)
                .ifPresentOrElse(this::withSequenceFunction, () -> this.stringToSequence(value));
    }

    /**
     * Add the all numbers in the given value string to the sequence list.
     */
    private void stringToSequence(String value) {
        List<Integer> list = Arrays.stream(value.split(","))
                .map(StringUtils::trim)
                .filter(StringUtils::isNumeric)
                .map(Integer::parseInt)
                .collect(Collectors.toList());
        withSequence(list);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void toCodeMap(Map<String, String> codeMap) {
        super.toCodeMap(codeMap);
        if (sequenceFunction != null) {
            codeMap.put(CODE_KEY_SEQUENCE, sequenceFunction.getCode());
        } else {
            String sequenceString = sequenceList.stream()
                    .map(Object::toString)
                    .collect(Collectors.joining(","));
            codeMap.put(CODE_KEY_SEQUENCE, sequenceString);
        }
    }

}
