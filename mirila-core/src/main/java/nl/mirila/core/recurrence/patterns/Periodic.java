package nl.mirila.core.recurrence.patterns;

/**
 * The {@link Periodic} implements an interval.
 */
public interface Periodic<T extends RecurrencePattern> {

    /**
     * Return the interval.
     */
    int getInterval();

    /**
     * Set the interval and return this instance for chaining purposes.
     **/
    T withInterval(int interval);

}
