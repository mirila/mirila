package nl.mirila.core.converters;

import nl.mirila.core.exceptions.InvalidStringConversionException;
import nl.mirila.core.exceptions.UnsupportedStringConversionTypeException;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static nl.mirila.core.converters.StringConverter.convert;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

class StringConverterTest {

    @Test
    void testNull() throws InvalidStringConversionException, UnsupportedStringConversionTypeException {
        assertThat(convert(Float.class, null)).isNull();
        assertThat(convert(Float.class, " ")).isNull();
        assertThat(convert(Float.class, " \n ")).isNull();
    }

    @Test
    void testString() throws InvalidStringConversionException, UnsupportedStringConversionTypeException {
        assertThat(convert(String.class, "test")).isEqualTo("test");
    }

    @Test
    void testInteger() throws InvalidStringConversionException, UnsupportedStringConversionTypeException {
        assertThat(convert(Integer.class, "-2147483648")).isEqualTo(Integer.MIN_VALUE);
        assertThat(convert(Integer.class, "-1")).isEqualTo(-1);
        assertThat(convert(Integer.class, "0")).isZero();
        assertThat(convert(Integer.class, "1")).isEqualTo(1);
        assertThat(convert(Integer.class, "2147483647")).isEqualTo(Integer.MAX_VALUE);

        assertThrows(InvalidStringConversionException.class, () -> convert(Integer.class, "test"));
    }

    @Test
    void testFloat() throws InvalidStringConversionException, UnsupportedStringConversionTypeException {
        assertThat(convert(Float.class, "-1.2")).isEqualTo((float) -1.2);
        assertThat(convert(Float.class, "0")).isEqualTo((float) 0);
        assertThat(convert(Float.class, "1.5")).isEqualTo((float) 1.5);

        assertThrows(InvalidStringConversionException.class, () -> convert(Float.class, "test"));
    }

    @Test
    void testDouble() throws InvalidStringConversionException, UnsupportedStringConversionTypeException {
        assertThat(convert(Double.class, "-1.2")).isEqualTo(-1.2);
        assertThat(convert(Double.class, "0")).isZero();
        assertThat(convert(Double.class, "1.5")).isEqualTo(1.5);

        assertThrows(InvalidStringConversionException.class, () -> convert(Double.class, "test"));
    }

    @Test
    void testLong() throws InvalidStringConversionException, UnsupportedStringConversionTypeException {
        assertThat(convert(Long.class, "-9223372036854775808")).isEqualTo(-9_223_372_036_854_775_808L);
        assertThat(convert(Long.class, "-0")).isZero();
        assertThat(convert(Long.class, "0")).isZero();
        assertThat(convert(Long.class, "0000")).isZero();
        assertThat(convert(Long.class, "9223372036854775807")).isEqualTo(9_223_372_036_854_775_807L);

        assertThrows(InvalidStringConversionException.class, () -> convert(Long.class, "test"));
    }

    @SuppressWarnings("SpellCheckingInspection")
    @Test
    void testBoolean() throws InvalidStringConversionException, UnsupportedStringConversionTypeException {
        assertThat(convert(Boolean.class, "false")).isFalse();
        assertThat(convert(Boolean.class, "FALSE")).isFalse();
        assertThat(convert(Boolean.class, "fALSE")).isFalse();
        assertThat(convert(Boolean.class, "fAL")).isFalse();
        assertThat(convert(Boolean.class, "")).isNull();
        assertThat(convert(Boolean.class, null)).isNull();
        assertThat(convert(Boolean.class, "true")).isTrue();
        assertThat(convert(Boolean.class, "TRUE")).isTrue();
        assertThat(convert(Boolean.class, "tRUE")).isTrue();
    }

    @Test
    void testLocalDate() throws InvalidStringConversionException, UnsupportedStringConversionTypeException {
        //assertThat(29)).as("2019-12-29").isCloseTo(LocalDate.of(2019, 12, within(convert(LocalDate.class)));
        //assertThat(29)).as("2020-02-29").isCloseTo(LocalDate.of(2020, 2, within(convert(LocalDate.class)));

        LocalDate currentDate = convert(LocalDate.class, "TODAY");
        assertThat(currentDate).isNotNull();
        assertThat(currentDate.isAfter(LocalDate.now().minusDays(1))).isTrue();
        assertThat(currentDate.isBefore(LocalDate.now().plusDays(1))).isTrue();

        currentDate = convert(LocalDate.class, "NOW");
        assertThat(currentDate).isNotNull();
        assertThat(currentDate.isAfter(LocalDate.now().minusDays(1))).isTrue();
        assertThat(currentDate.isBefore(LocalDate.now().plusDays(1))).isTrue();

        assertThrows(InvalidStringConversionException.class, () -> convert(LocalDate.class, "test"));
    }


    @Test
    void testLocalTime() throws InvalidStringConversionException, UnsupportedStringConversionTypeException {
        assertThat(convert(LocalTime.class, "12:34:56")).isEqualTo(LocalTime.of(12, 34, 56));
        assertThat(convert(LocalTime.class, "12:34")).isEqualTo(LocalTime.of(12, 34, 0));
        assertThat(convert(LocalTime.class, "12:00")).isEqualTo(LocalTime.of(12, 0, 0));
        assertThat(convert(LocalTime.class, "5:00")).isEqualTo(LocalTime.of(5, 0, 0));

        LocalTime currentTime = convert(LocalTime.class, "NOW");
        LocalTime now = LocalTime.now();
        assertThat(currentTime).isNotNull();

        // Work around the 23:59 - 00:01 period.
        if (now.toString().startsWith("23:59")) {
            assertThat(currentTime).isBetween("23:59:00", "23:59:59.999");
        } else if (now.toString().startsWith("00:00")) {
            assertThat(currentTime).isBetween("00:00:00", "00:01:00.000");
        } else {
            assertThat(currentTime).isBetween(now.minusMinutes(1), now.plusMinutes(1));
        }
    }

    @Test
    void testLocalDateTime() throws InvalidStringConversionException, UnsupportedStringConversionTypeException {
        assertThat(convert(LocalDateTime.class, "2019-12-29T20:21:36"))
                .isEqualTo(LocalDateTime.of(2019, 12, 29, 20, 21, 36));

        assertThat(convert(LocalDateTime.class, "2020-02-29T07:01:06"))
                .isEqualTo(LocalDateTime.of(2020, 2, 29, 7, 1, 6));

        assertThat(convert(LocalDateTime.class, "2020-02-29T07:01"))
                .isEqualTo(LocalDateTime.of(2020, 2, 29, 7, 1, 0));

        assertThat(convert(LocalDateTime.class, "2020-02-29 07:01"))
                .isEqualTo(LocalDateTime.of(2020, 2, 29, 7, 1, 0));

        LocalDateTime currentDateTime = convert(LocalDateTime.class, "NOW");
        assertThat(currentDateTime)
                .isNotNull()
                .isBetween(LocalDateTime.now().minusMinutes(1), LocalDateTime.now().plusMinutes(1));

        assertThrows(InvalidStringConversionException.class, () -> convert(LocalDateTime.class, "test"));
    }

    @Test
    void testUnknownClass() {
        assertThrows(UnsupportedStringConversionTypeException.class, () -> convert(StringConverterTest.class, "test"));
    }

    @Test
    void testIntegerList() throws InvalidStringConversionException, UnsupportedStringConversionTypeException {
        List<Integer> list1 = StringConverter.convertToList(Integer.class, "1\n2\n3\n");

        assertThat(list1).isEqualTo(Arrays.asList(1, 2, 3));

        List<Integer> list2 = StringConverter.convertToList(Integer.class, "7|6| |5||", "\\|");
        assertThat(list2).isEqualTo(Arrays.asList(7, 6, 5));
    }

    @Test
    void testBooleanList() throws InvalidStringConversionException, UnsupportedStringConversionTypeException {
        List<Boolean> list = StringConverter.convertToList(Boolean.class, "true, false, true", ",");
        assertThat(list).isEqualTo(Arrays.asList(true, false, true));
    }


    @Test
    void testStringMap() throws InvalidStringConversionException, UnsupportedStringConversionTypeException {
        String value = "test1=testA\ntest2=testB\n test3 = testC";
        Map<String, String> map = StringConverter.convertToMap(String.class, String.class, value);
        assertThat(new ArrayList<>(map.keySet())).isEqualTo(Arrays.asList("test1", "test2", "test3"));
        assertThat(new ArrayList<>(map.values())).isEqualTo(Arrays.asList("testA", "testB", "testC"));
    }

    @Test
    void testLocalDateIntegerMap() throws InvalidStringConversionException, UnsupportedStringConversionTypeException {
        String value = "2019-12-29>12|2019-12-30> 13 || 2019-12-31 >14";
        Map<LocalDate, Integer> map = StringConverter.convertToMap(LocalDate.class, Integer.class, value, "\\|", ">");
        List<LocalDate> expectedKeys = Arrays.asList(LocalDate.of(2019, 12, 29),
                                                     LocalDate.of(2019, 12, 30),
                                                     LocalDate.of(2019, 12, 31));
        List<Integer> expectedValues= Arrays.asList(12, 13, 14);
        assertThat(new ArrayList<>(map.keySet())).isEqualTo(expectedKeys);
        assertThat(new ArrayList<>(map.values())).isEqualTo(expectedValues);
    }

}
