package nl.mirila.core.converters;

import nl.mirila.core.exceptions.UnsupportedDateConversionTypeException;
import org.junit.jupiter.api.Test;

import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

class DateConverterTest {

    private final static Instant testInstant = LocalDateTime.parse("2007-10-22T20:29:00")
            .atZone(ZoneId.systemDefault())
            .toInstant();

    private final static java.util.Date testDate = java.util.Date.from(testInstant);

    private final static Date testSqlDate = Date.valueOf(LocalDate.of(2007, 10, 22));
    private final static Time testSqlTime = Time.valueOf(LocalTime.of(20, 29));
    private final static Timestamp testSqlTimestamp = Timestamp.valueOf(LocalDateTime.of(2007, 10, 22, 20, 29));

    @Test
    void testConvertNullToInstant() throws UnsupportedDateConversionTypeException {
        assertThat(DateConverter.convert(LocalDate.class, null)).isNull();
    }

    @Test
    void testConvertDateToInstant() throws UnsupportedDateConversionTypeException {
        assertThat(DateConverter.convert(Instant.class, testDate))
                .isEqualTo(testInstant);
    }

    @Test
    void testConvertDateToLocaleDate() throws UnsupportedDateConversionTypeException {
        assertThat(DateConverter.convert(LocalDate.class, testDate))
                .isEqualTo(LocalDate.of(2007, 10, 22));
    }

    @Test
    void testConvertDateToLocaleDateTime() throws UnsupportedDateConversionTypeException {
        assertThat(DateConverter.convert(LocalDateTime.class, testDate))
                .isEqualTo(LocalDateTime.of(2007, 10, 22, 20, 29));
    }

    @Test
    void testConvertDateToLocaleTime() throws UnsupportedDateConversionTypeException {
        assertThat(DateConverter.convert(LocalTime.class, testDate))
                .isEqualTo(LocalTime.of(20, 29));
    }

    @Test
    void testConvertDateToString() throws UnsupportedDateConversionTypeException {
        assertThat(DateConverter.convert(String.class, testDate))
                .startsWith("2007-10-22T20:29:00");
    }

    @Test
    void testConvertSqlDateToInstant() {
        assertThatExceptionOfType(UnsupportedDateConversionTypeException.class).isThrownBy(() -> {
            DateConverter.convert(Instant.class, testSqlDate);
        });
    }

    @Test
    void testConvertSqlDateToLocaleDate() throws UnsupportedDateConversionTypeException {
        assertThat(DateConverter.convert(LocalDate.class, testSqlDate))
                .isEqualTo(LocalDate.of(2007, 10, 22));
    }

    @Test
    void testConvertSqlDateToString() throws UnsupportedDateConversionTypeException {
        assertThat(DateConverter.convert(String.class, testSqlDate))
                .isEqualTo("2007-10-22");
    }

    @Test
    void testConvertSqlTimestampToInstant() throws UnsupportedDateConversionTypeException {
        assertThat(DateConverter.convert(Instant.class, testSqlTimestamp))
                .isEqualTo(testInstant);
    }

    @Test
    void testConvertSqlTimestampToLocaleDateTime() throws UnsupportedDateConversionTypeException {
        assertThat(DateConverter.convert(LocalDateTime.class, testSqlTimestamp))
                .isEqualTo(LocalDateTime.parse("2007-10-22T20:29:00"));
    }

    @Test
    void testConvertSqlTimestampToString() throws UnsupportedDateConversionTypeException {
        assertThat(DateConverter.convert(String.class, testSqlTimestamp))
                .isEqualTo("2007-10-22 20:29:00");
    }

    @Test
    void testConvertSqlTimeToInstant() {
        assertThatExceptionOfType(UnsupportedDateConversionTypeException.class).isThrownBy(() -> {
            DateConverter.convert(Instant.class, testSqlTime);
        });
    }

    @Test
    void testConvertSqlTimeToLocaleTime() throws UnsupportedDateConversionTypeException {
        assertThat(DateConverter.convert(LocalTime.class, testSqlTime))
                .isEqualTo(LocalTime.of(20, 29));
    }

    @Test
    void testConvertSqlTimeToString() throws UnsupportedDateConversionTypeException {
        assertThat(DateConverter.convert(String.class, testSqlTime))
                .isEqualTo("20:29:00");
    }

}
