package nl.mirila.core.settings.providers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.nio.file.Paths;

import static org.assertj.core.api.Assertions.assertThat;

class KeyValuesPropertiesFileProviderTest {

    private KeyValuesPropertiesFileProvider provider;

    @BeforeEach
    void setUp() {
        Path path = Paths.get("src", "test", "resources", "example-configuration.properties");
        provider = new KeyValuesPropertiesFileProvider(path);
    }

    @Test
    void testLoad() {
        assertThat(provider.getValue("key1")).contains("value1");
        assertThat(provider.getValue("smtp.server")).contains("localhost");
        assertThat(provider.getValue("smtp.port")).contains("25");
        assertThat(provider.getValue("smtp.options")).contains("OptionA, OptionB");
        assertThat(provider.getValue("int-options1")).contains("1, 2, 3");
        assertThat(provider.getValue("int-options2")).contains("1, 2, 3, 4, 5");
    }

}
