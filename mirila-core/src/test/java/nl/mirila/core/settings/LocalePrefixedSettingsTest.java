package nl.mirila.core.settings;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import nl.mirila.core.settings.modules.KeyValuesSettingsModule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static nl.mirila.core.settings.LocalePrefixedSettings.getOrderedKeyVariants;
import static org.assertj.core.api.Assertions.assertThat;

class LocalePrefixedSettingsTest {

    private static final String KEY = "my-key";
    private static final String PREFIX = "prefix";

    private LocalePrefixedSettings settings;

    @BeforeEach
    void setUp() {
        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                Map<String, String> keyValues = new HashMap<>();
                keyValues.put(KEY, "my-value");
                keyValues.put("en." + KEY, "en.my-value");
                keyValues.put("nl." + KEY, "nl.my-value");
                keyValues.put("nl_NL." + KEY, "nl_NL.my-value");

                keyValues.put(PREFIX + ".sub1", "value1");
                keyValues.put("en." + PREFIX + ".sub1", "en.value1");
                keyValues.put("nl." + PREFIX + ".sub1", "nl.value1");
                keyValues.put("nl_NL." + PREFIX + ".sub1", "nl_NL.value1");

                keyValues.put(PREFIX + ".sub2", "value2");
                keyValues.put("en." + PREFIX + ".sub2", "en.value2");
                keyValues.put("nl." + PREFIX + ".sub2", "nl.value2");

                keyValues.put(PREFIX + ".sub3", "value3");
                keyValues.put("en." + PREFIX + ".sub3", "en.value3");

                install(new KeyValuesSettingsModule(keyValues));
            }
        });
        KeyValuesProvider keyValuesProvider = injector.getInstance(KeyValuesProvider.class);
        settings = new LocalePrefixedSettings(keyValuesProvider);
    }

    @Test
    void testGetStringWithoutLocale() {
        settings.setLocaleId(null);
        assertThat(settings.getString(KEY)).contains("my-value");
    }

    @Test
    void testGetStringWithDutchLocale() {
        settings.setLocaleId("nl_NL");
        assertThat(settings.getString(KEY)).contains("nl_NL.my-value");
    }

    @Test
    void testGetStringWithFlemishLocale() {
        settings.setLocaleId("nl_BE");
        assertThat(settings.getString(KEY)).contains("nl.my-value");
    }

    @Test
    void testGetStringWithEnglishLocale() {
        settings.setLocaleId("en_US");
        assertThat(settings.getString(KEY)).contains("en.my-value");
    }

    @Test
    void testGetStringWithItalianLocale() {
        settings.setLocaleId("it_IT");
        assertThat(settings.getString(KEY)).contains("my-value");
    }


    @Test
    void testGetStringMapWithoutLocale() {
        settings.setLocaleId(null);
        assertThat(settings.getStringMap(PREFIX))
                .containsOnlyKeys("prefix.sub1", "prefix.sub2", "prefix.sub3")
                .containsValues("value1", "value2", "value3");
    }

    @Test
    void testGetStringMapWithDutchLocale() {
        settings.setLocaleId("nl_NL");
        assertThat(settings.getStringMap(PREFIX))
                .containsOnlyKeys("prefix.sub1", "prefix.sub2", "prefix.sub3")
                .containsValues("nl_NL.value1", "nl.value2", "value3");
    }

    @Test
    void testGetStringMapWithFlemishLocale() {
        settings.setLocaleId("nl_BE");
        assertThat(settings.getStringMap(PREFIX))
                .containsOnlyKeys("prefix.sub1", "prefix.sub2", "prefix.sub3")
                .containsValues("nl.value1", "nl.value2", "value3");
    }

    @Test
    void testGetStringMapWithEnglishLocale() {
        settings.setLocaleId("en_US");
        assertThat(settings.getStringMap(PREFIX))
                .containsOnlyKeys("prefix.sub1", "prefix.sub2", "prefix.sub3")
                .containsValues("en.value1", "en.value2", "en.value3");
    }

    @Test
    void testGetStringMapWithItalianLocale() {
        settings.setLocaleId("it_IT");
        assertThat(settings.getStringMap(PREFIX))
                .containsOnlyKeys("prefix.sub1", "prefix.sub2", "prefix.sub3")
                .containsValues("value1", "value2", "value3");
    }

    @Test
    void getOrderedKeyVariantsWithNullLocaleId() {
        List<String> variants = getOrderedKeyVariants("my-key", null);
        assertThat(variants).hasSize(1).containsExactly("my-key");
    }

    @Test
    void getOrderedKeyVariantsWithEmptyLocaleId() {
        List<String> variants = getOrderedKeyVariants("my-key", "");
        assertThat(variants).hasSize(1).containsExactly("my-key");
    }

    @Test
    void getOrderedKeyVariantsWithinValidLocaleId() {
        List<String> variants = getOrderedKeyVariants("my-key", "nl_NL");
        assertThat(variants).hasSize(3).containsExactly("nl_NL.my-key",  "nl.my-key", "my-key");
    }

    @Test
    void getOrderedKeyVariantsWithinValidDashedLocaleId() {
        List<String> variants = getOrderedKeyVariants("my-key", "nl-NL");
        assertThat(variants).hasSize(3).containsExactly("nl_NL.my-key",  "nl.my-key", "my-key");
    }

    @Test
    void getOrderedKeyVariantsWithinInvalidLocaleId() {
        List<String> variants = getOrderedKeyVariants("my-key", "not_a_valid_locale");
        assertThat(variants).hasSize(1).containsExactly("my-key");
    }

}