package nl.mirila.core.settings;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.within;

class SettingsTest {

    private final HashMap<String, String> settingValues = new HashMap<>();

    private Settings parentSettings;
    private Settings settings;

    @BeforeEach
    void setUp() {
        Map<String, String> parentSettingValues = new HashMap<>();
        parentSettingValues.put("key1", "value1");
        parentSettingValues.put("key2", "value2");
        parentSettingValues.put("key3", "value3a");
        parentSettings = Settings.withMap(parentSettingValues);

        settingValues.put("key3", "value3b");
        settingValues.put("key4", "value4");
        settingValues.put("key5", "value5");
        settings = Settings.withMapAndParentSettings(settingValues, parentSettings);
    }

    @Test
    void testGetString() {
        assertThat(settings.getString("key4")).contains("value4");
        assertThat(settings.getString("key5")).contains("value5");
        assertThat(settings.getString("unknown-key")).isEmpty();
        assertThat(settings.getString("")).isEmpty();
        assertThat(settings.getString(null)).isEmpty();
    }

    @Test
    void testGetStringWithInheritance() {
        assertThat(settings.getString("key1")).contains("value1");
        assertThat(settings.getString("key2")).contains("value2");

        assertThat(settings.getString("key3")).contains("value3b");
        assertThat(parentSettings.getString("key3")).contains("value3a");

        assertThat(settings.getString("key4")).contains("value4");
        assertThat(settings.getString("key5")).contains("value5");

        assertThat(parentSettings.getString("key4")).isEmpty();
        assertThat(parentSettings.getString("key5")).isEmpty();
    }

    @Test
    void testGetStringList() {
        settingValues.put("ints1", "123,234,345");
        settingValues.put("ints2", "123|234|345");

        assertThat(settings.getStringList("ints1")).isEqualTo(Arrays.asList("123", "234", "345"));
        assertThat(settings.getStringList("ints2", "\\|")).isEqualTo(Arrays.asList("123", "234", "345"));

        assertThat(settings.getStringList("unknown")).isEmpty();
        assertThat(settings.getStringList(null)).isEmpty();
    }

    @Test
    void testGetInteger() {
        settingValues.put("int", "123");

        assertThat(settings.getInteger("int")).contains(123);
        assertThat(parentSettings.getInteger("key1")).isEmpty();
    }

    @Test
    void testGetIntegerList() {
        settingValues.put("ints1", "123,234,345");
        settingValues.put("ints2", "123|234|345");

        assertThat(settings.getIntegerList("ints1")).isEqualTo(Arrays.asList(123, 234, 345));
        assertThat(settings.getIntegerList("ints2", "\\|")).isEqualTo(Arrays.asList(123, 234, 345));
    }

    @Test
    void testGetLong() {
        settingValues.put("long1", "0");
        settingValues.put("long2", "123");
        settingValues.put("long3", "-123");
        settingValues.put("long4", "-9223372036854775808");
        settingValues.put("long5", "9223372036854775807");
        settingValues.put("long6", "9223372036854775807");
        settingValues.put("long7", "123123123123123231");

        assertThat(settings.getLong("long1")).contains(0L);
        assertThat(settings.getLong("long2")).contains(123L);
        assertThat(settings.getLong("long3")).contains(-123L);
        assertThat(settings.getLong("long4")).contains(Long.MIN_VALUE);
        assertThat(settings.getLong("long5")).contains(Long.MAX_VALUE);
        assertThat(settings.getLong("long6")).contains(Long.MAX_VALUE);
        assertThat(settings.getLong("long7")).contains(123123123123123231L);
    }

    @Test
    void testGetLongList() {
        settingValues.put("longs", "-123, 0, 123, 12, 123123123123123231");

        assertThat(settings.getLongList("longs")).isEqualTo(Arrays.asList(-123L, 0L, 123L, 12L, 123123123123123231L));
    }

    @Test
    void testGetBigDecimal() {
        settingValues.put("big-decimal1", "0.12");
        settingValues.put("big-decimal2", "12.3");

        assertThat(settings.getBigDecimal("big-decimal1")).contains(BigDecimal.valueOf(0.12));
        assertThat(settings.getBigDecimal("big-decimal2")).contains(BigDecimal.valueOf(12.3));
    }

    @Test
    void testGetBigDecimalList() {
        settingValues.put("big-decimals1", "12.3, 123.345, -1.123");
        settingValues.put("big-decimals2", "123.345, not-numeric, -1.123, 12.3");

        List<BigDecimal> expected1 = Arrays.asList(BigDecimal.valueOf(12.3),
                                                   BigDecimal.valueOf(123.345),
                                                   BigDecimal.valueOf(-1.123));
        assertThat(settings.getBigDecimalList("big-decimals1")).isEqualTo(expected1);

        List<BigDecimal> expected2 = Arrays.asList(BigDecimal.valueOf(123.345),
                                                   BigDecimal.valueOf(-1.123),
                                                   BigDecimal.valueOf(12.3));
        assertThat(settings.getBigDecimalList("big-decimals2")).isEqualTo(expected2);
    }

    @Test
    void testGetBoolean() {
        settingValues.put("boolean1", "true");
        settingValues.put("boolean2", "false");

        // We don't allow these values.
        settingValues.put("boolean3", "0");
        settingValues.put("boolean4", "1");
        settingValues.put("boolean5", "yes");
        settingValues.put("boolean6", "no");

        assertThat(settings.getBoolean("boolean1")).contains(true);
        assertThat(settings.getBoolean("boolean2")).contains(false);
        assertThat(settings.getBoolean("boolean3")).contains(false);
        assertThat(settings.getBoolean("boolean4")).contains(false);
        assertThat(settings.getBoolean("non-existing-key")).isEmpty();
    }

    @Test
    void testGetBooleanList() {
        settingValues.put("booleans", "true, false, true, false, true");

        List<Boolean> expected = Arrays.asList(true, false, true, false, true);
        assertThat(settings.getBooleanList("booleans")).isEqualTo(expected);
    }

    @Test
    void testGetLocalDate() {
        settingValues.put("local-date", "2020-06-11");
        assertThat(settings.getLocalDate("local-date")).contains(LocalDate.of(2020, 6, 11));
    }

    @Test
    void testGetLocalDateList() {
        settingValues.put("local-dates", "2020-06-11, 2020-06-12");

        List<LocalDate> expected = Arrays.asList(LocalDate.of(2020, 6, 11), LocalDate.of(2020, 6, 12));
        assertThat(settings.getLocalDateList("local-dates")).isEqualTo(expected);
    }

    @Test
    void testGetLocalTime() {
        settingValues.put("localtime1", "12:34:56");
        settingValues.put("localtime2", "12:34");
        settingValues.put("localtime3", "12:00");
        settingValues.put("localtime4", "5:00");
        settingValues.put("localtime5", "NOW");

        assertThat(settings.getLocalTime("localtime1")).contains(LocalTime.of(12, 34, 56));
        assertThat(settings.getLocalTime("localtime2")).contains(LocalTime.of(12, 34, 0));
        assertThat(settings.getLocalTime("localtime3")).contains(LocalTime.of(12, 0, 0));
        assertThat(settings.getLocalTime("localtime4")).contains(LocalTime.of(5, 0, 0));

        LocalTime currentTime = settings.getLocalTime("localtime5").orElse(null);
        assertThat(currentTime)
                .isNotNull()
                .isCloseTo(LocalTime.now(), within(5, ChronoUnit.SECONDS));
    }

    @Test
    void testGetLocalTimeList() {
        settingValues.put("local-times", "5:00, 12:34:56, 12:34");

        List<Object> expected = Arrays.asList(LocalTime.of(5, 0), LocalTime.of(12, 34, 56), LocalTime.of(12, 34, 0));
        assertThat(settings.getLocalTimeList("local-times")).isEqualTo(expected);
    }

    @Test
    void testGetLocalDateTime() {
        settingValues.put("local-date-time1", "2020-06-12T00:35:05");
        settingValues.put("local-date-time2", "2020-06-12 00:35:05");
        settingValues.put("local-date-time3", "NOW");

        assertThat(settings.getLocalDateTime("local-date-time1"))
                .contains(LocalDateTime.of(2020, 6, 12, 0, 35, 5));

        assertThat(settings.getLocalDateTime("local-date-time1"))
                .contains(LocalDateTime.of(2020, 6, 12, 0, 35, 5));


        LocalDateTime currentTime = settings.getLocalDateTime("local-date-time3").orElse(null);
        assertThat(currentTime)
                .isNotNull()
                .isCloseTo(LocalDateTime.now(), within(5, ChronoUnit.SECONDS));
    }

    @Test
    void testGetLocalDateTimeList() {
        settingValues.put("local-date-times", "2020-06-12T00:35:05, 2020-06-13 12:34");

        List<LocalDateTime> expected = Arrays.asList(LocalDateTime.of(2020, 6, 12, 0, 35, 5),
                                                     LocalDateTime.of(2020, 6, 13, 12, 34, 0));
        assertThat(settings.getLocalDateTimeList("local-date-times")).isEqualTo(expected);
    }

}
