package nl.mirila.core.datatype;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class JsonTest {

    @Test
    void testIsEmptyWithNull() {
        assertThat(Json.of(null).isEmpty()).isTrue();
    }

    @Test
    void testIsEmptyWithNullString() {
        assertThat(Json.of("null").isEmpty()).isTrue();
    }

    @Test
    void testIsEmptyWithEmpyString() {
        assertThat(Json.of("").isEmpty()).isTrue();
    }

    @Test
    void testIsEmptyWithBlankString() {
        assertThat(Json.of(" ").isEmpty()).isTrue();
    }

    @Test
    void testIsEmptyWithEmptyArray() {
        assertThat(Json.of("[]").isEmpty()).isTrue();
    }

    @Test
    void testIsEmptyWithEmptyHash() {
        assertThat(Json.of("{}").isEmpty()).isTrue();
    }

    @Test
    void testIsEmptyWithDataInJson() {
        assertThat(Json.of("{\"id\": \"an-id\"}").isEmpty()).isFalse();
    }

    @Test
    void testMaybeWithNull() {
        assertThat(Json.mayBeJson(null)).isFalse();
    }

    @Test
    void testMaybeWithNullString() {
        assertThat(Json.mayBeJson("null")).isTrue();
    }

    @Test
    void testMaybeWithEmptyString() {
        assertThat(Json.mayBeJson("")).isFalse();
    }

    @Test
    void testMaybeWithBlankString() {
        assertThat(Json.mayBeJson("  ")).isFalse();
    }

    @Test
    void testMaybeWithEmptyArray() {
        assertThat(Json.mayBeJson("[]")).isTrue();
    }

    @Test
    void testMaybeWithEmptyHash() {
        assertThat(Json.mayBeJson("{}")).isTrue();
    }

    @Test
    void testMaybeWithDataInJson() {
        assertThat(Json.mayBeJson("{\"id\": \"an-id\"}")).isTrue();
    }

    @Test
    void testGetWithNull() {
        assertThat(Json.of(null).get()).isEqualTo(Json.DEFAULT);
    }

    @Test
    void testGetWithNullString() {
        assertThat(Json.of("null").get()).isEqualTo(Json.NULL_STRING);
    }

    @Test
    void testGetWithEmptyString() {
        assertThat(Json.of("").get()).isEqualTo(Json.DEFAULT);
    }

    @Test
    void testGetWithBlankString() {
        assertThat(Json.of("  ").get()).isEqualTo(Json.DEFAULT);
    }

    @Test
    void testGetWithEmptyArray() {
        assertThat(Json.of("[]").get()).isEqualTo(Json.EMPTY_ARRAY);
    }

    @Test
    void testGetWithEmptyHash() {
        assertThat(Json.of("{}").get()).isEqualTo(Json.EMPTY_HASH);
    }

    @Test
    void testGetWithDataInJson() {
        assertThat(Json.of("{\"id\": \"an-id\"}").get()).isEqualTo("{\"id\": \"an-id\"}");
    }

}
