package nl.mirila.core.recurrence.utils;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Set;

import static nl.mirila.core.recurrence.utils.CronPatternUtils.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatException;

class CronPatternUtilsTest {

    @Test
    void testGetSetForPatternWithInvalidPattern() {
        assertThatException().isThrownBy(() -> getSetForPattern("invalid", 1, 12));
        assertThatException().isThrownBy(() -> getSetForPattern("-1", 1, 12));
        assertThatException().isThrownBy(() -> getSetForPattern("1/invalid", 1, 12));
        assertThatException().isThrownBy(() -> getSetForPattern("1/*", 1, 12));
        assertThatException().isThrownBy(() -> getSetForPattern("*/invalid", 1, 12));
    }

    @Test
    void testGetSetForPatternWithAsterisk() {
        assertThat(getSetForPattern("*", 0, 3)).isEmpty();
    }

    @Test
    void testGetSetForPatternWithNumeric() {
        assertThat(getSetForPattern("2", 1, 12)).containsExactly(2);
    }

    @Test
    void testGetSetForPatternWithNumerics() {
        assertThat(getSetForPattern("2,3,5,6", 1, 12)).containsExactly(2, 3, 5, 6);
    }

    @Test
    void testGetSetForPatternWithRange() {
        assertThat(getSetForPattern("2-6", 1, 12)).containsExactly(2, 3, 4, 5, 6);
    }

    @Test
    void testGetSetForPatternWithNumericsAndRanges() {
        assertThat(getSetForPattern("2,4-6,8,9-11", 1, 12)).containsExactly(2, 4, 5, 6, 8, 9, 10, 11);
    }

    @Test
    void testGetSetForPatternWithOverflowingNumerics() {
        assertThat(getSetForPattern("0,3,6,15,18", 1, 12)).containsExactly(3, 6);
    }

    @Test
    void testGetSetForPatternWithOverflowingRange() {
        assertThat(getSetForPattern("9-14", 1, 12)).containsExactly(9, 10, 11, 12);
    }

    @Test
    void testGetSetForPatternWithIntervalAndAsterisk() {
        assertThat(getSetForPattern("*/3", 1, 12)).containsExactly(1, 4, 7, 10);
    }

    @Test
    void testGetSetForPatternWithIntervalAndOffset1() {
        assertThat(getSetForPattern("1/3", 1, 12)).containsExactly(1, 4, 7, 10);
    }

    @Test
    void testGetSetForPatternWithIntervalAndOffset2() {
        assertThat(getSetForPattern("2/3", 1, 12)).containsExactly(2, 5, 8, 11);
    }

    @Test
    void testGetSetForPatternWithIntervalAndOffset9() {
        assertThat(getSetForPattern("9/3", 1, 12)).containsExactly(9, 12);
    }

    @Test
    void testGetSetForPatternWithIntervalAndRange() {
        assertThat(getSetForPattern("5-10/2", 1, 12)).containsExactly(5, 7, 9);
    }

    @Test
    void testGetSetForPatternWithNumericRangeAndInterval() {
        assertThat(getSetForPattern("9/2,1,3-5", 1, 12)).containsExactly(1, 3, 4, 5, 9, 11);
    }

    @Test
    void testGetSetForPatternWithNumericRangeAndStartWithAsterisk() {
        assertThat(getSetForPattern("*,1,3-5", 1, 8)).containsExactly(1, 2, 3, 4, 5, 6, 7, 8);
    }

    @Test
    void testGetSetForPatternWithNumericRangeAndContainsAsterisk() {
        assertThat(getSetForPattern("1,3-5,*", 1, 8)).containsExactly(1, 2, 3, 4, 5, 6, 7, 8);
    }

    @Test
    void testGetSetForPatternWithNumericRangeAndEndsWithAsterisk() {
        assertThat(getSetForPattern("1,3-5,*", 1, 8)).containsExactly(1, 2, 3, 4, 5, 6, 7, 8);
    }

    @Test
    void testGetSetForSeconds() {
        assertThat(getSetForSeconds("0-3,58-62")).containsExactly(0, 1, 2, 3, 58, 59);
    }

    @Test
    void testGetSetForMinutes() {
        assertThat(getSetForMinutes("0-3,58-62")).containsExactly(0, 1, 2, 3, 58, 59);
    }

    @Test
    void testGetSetForHours() {
        assertThat(getSetForHours("0-3,22-25")).containsExactly(0, 1, 2, 3, 22, 23);
    }

    @Test
    void testGetSetForDaysOfMonth() {
        assertThat(getSetForDaysOfMonth("0-3,30-33")).containsExactly(1, 2, 3, 30, 31);
    }

    @Test
    void testGetSetForDaysOfWeek() {
        assertThat(getSetForDaysOfWeek("MON,WED,FRI")).containsExactly(1, 3, 5);
    }

    @Test
    void testGetSetForDaysOfWeekWithInterval() {
        assertThat(getSetForDaysOfWeek("MON-FRI/2")).containsExactly(1, 3, 5);
    }

    @Test
    void testGetSetForMonths() {
        assertThat(getSetForMonths("JAN-MAR,JUL,SEP-NOV")).containsExactly(1, 2, 3, 7, 9, 10, 11);
    }

    @Test
    void getDaysOfWeekForMonthWithSundaysInFebruary() {
        Set<Integer> set = getDaysOfWeekForMonth("SUN", LocalDate.of(2023, 2, 1));
        assertThat(set).containsExactly(5, 12, 19, 26);

    }

    @Test
    void getDaysOfWeekForMonthWithFridaysInFebruary() {
        Set<Integer> set = getDaysOfWeekForMonth("MON", LocalDate.of(2023, 2, 1));
        assertThat(set).containsExactly(6, 13, 20, 27);
    }

    @Test
    void getDaysOfWeekForMonthWithMondaysToWednesdaysInFebruary() {
        Set<Integer> set = getDaysOfWeekForMonth("MON-WED", LocalDate.of(2023, 2, 1));
        assertThat(set).containsExactly(1, 6, 7, 8, 13, 14, 15, 20, 21, 22, 27, 28);
    }

    @Test
    void getDaysOfWeekForMonthWithFirstSundayInFebruary() {
        Set<Integer> set = getDaysOfWeekForMonth("SUN#1", LocalDate.of(2023, 2, 1));
        assertThat(set).containsExactly(5);
    }

    @Test
    void getDaysOfWeekForMonthWithThirdMondayInFebruary() {
        Set<Integer> set = getDaysOfWeekForMonth("MON#3", LocalDate.of(2023, 2, 1));
        assertThat(set).containsExactly(20);
    }

    @Test
    void getDaysOfWeekForMonthWithThirdMondayAdIndexInFebruary() {
        Set<Integer> set = getDaysOfWeekForMonth("1#3", LocalDate.of(2023, 2, 1));
        assertThat(set).containsExactly(20);
    }

}