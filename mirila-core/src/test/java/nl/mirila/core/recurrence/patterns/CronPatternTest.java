package nl.mirila.core.recurrence.patterns;

import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatException;

class CronPatternTest {

    @Test
    void testShortPattern() {
        CronPattern pattern = RecurrencePattern.forCronPattern()
                .using("1 2 3 4 5");
        assertThat(pattern.getMinutes()).isEqualTo("1");
        assertThat(pattern.getHours()).isEqualTo("2");
        assertThat(pattern.getDaysOfMonth()).isEqualTo("3");
        assertThat(pattern.getMonths()).isEqualTo("4");
        assertThat(pattern.getDaysOfWeek()).isEqualTo("5");
    }

    @Test
    void testLongPattern() {
        CronPattern pattern = RecurrencePattern.forCronPattern()
                .using("1 2 3 4 5 6");
        assertThat(pattern.getSeconds()).isEqualTo("1");
        assertThat(pattern.getMinutes()).isEqualTo("2");
        assertThat(pattern.getHours()).isEqualTo("3");
        assertThat(pattern.getDaysOfMonth()).isEqualTo("4");
        assertThat(pattern.getMonths()).isEqualTo("5");
        assertThat(pattern.getDaysOfWeek()).isEqualTo("6");
    }

    @Test
    void testGetNextOccurrenceToNextSecond() {
        CronPattern pattern = RecurrencePattern.forCronPattern()
                .using("* * * * * *");

        LocalDateTime offset = LocalDateTime.of(2023, 3, 2, 12, 34, 56);
        assertThat(pattern.getNextOccurrence(offset)).contains(LocalDateTime.of(2023, 3, 2, 12, 34, 57));
    }

    @Test
    void testGetNextOccurrenceToNextMinute() {
        CronPattern pattern = RecurrencePattern.forCronPattern()
                .using("0 * * * * *");

        LocalDateTime offset = LocalDateTime.of(2023, 3, 2, 12, 34, 56);
        assertThat(pattern.getNextOccurrence(offset)).contains(LocalDateTime.of(2023, 3, 2, 12, 35, 0));
    }

    @Test
    void testGetNextOccurrenceToNextMinuteWithShortPattern() {
        CronPattern pattern = RecurrencePattern.forCronPattern()
                .using("* * * * *");

        LocalDateTime offset = LocalDateTime.of(2023, 3, 2, 12, 34, 56);
        assertThat(pattern.getNextOccurrence(offset)).contains(LocalDateTime.of(2023, 3, 2, 12, 35, 0));
    }

    @Test
    void testGetNextOccurrenceToNextHour() {
        CronPattern pattern = RecurrencePattern.forCronPattern()
                .using("0 0 * * * *");

        LocalDateTime offset = LocalDateTime.of(2023, 3, 2, 12, 34, 56);
        assertThat(pattern.getNextOccurrence(offset)).contains(LocalDateTime.of(2023, 3, 2, 13, 0, 0));
    }


    @Test
    void testGetNextOccurrenceToNextHourWithRange() {
        CronPattern pattern = RecurrencePattern.forCronPattern()
                .using("0 0 10-15 * * *");

        LocalDateTime offset = LocalDateTime.of(2023, 3, 2, 12, 34, 56);
        assertThat(pattern.getNextOccurrence(offset)).contains(LocalDateTime.of(2023, 3, 2, 13, 0, 0));
    }

    @Test
    void testGetNextOccurrenceToNextDay() {
        CronPattern pattern = RecurrencePattern.forCronPattern()
                .using("0 0 0 * * *");

        LocalDateTime offset = LocalDateTime.of(2023, 3, 2, 12, 34, 56);
        assertThat(pattern.getNextOccurrence(offset)).contains(LocalDateTime.of(2023, 3, 3, 0, 0, 0));
    }

    @Test
    void testGetNextOccurrenceToNextMonth() {
        CronPattern pattern = RecurrencePattern.forCronPattern()
                .using("0 0 0 1 * *");

        LocalDateTime offset = LocalDateTime.of(2023, 3, 2, 12, 34, 56);
        assertThat(pattern.getNextOccurrence(offset)).contains(LocalDateTime.of(2023, 4, 1, 0, 0, 0));
    }

    @Test
    void testGetNextOccurrenceToNextYear() {
        CronPattern pattern = RecurrencePattern.forCronPattern()
                .using("0 0 0 1 1 *");

        LocalDateTime offset = LocalDateTime.of(2023, 3, 2, 12, 34, 56);
        assertThat(pattern.getNextOccurrence(offset)).contains(LocalDateTime.of(2024, 1, 1, 0, 0, 0));
    }

    @Test
    void testGetNextOccurrenceToNextSecondWithInterval() {
        CronPattern pattern = RecurrencePattern.forCronPattern()
                .using("3/15 * * * * *");

        LocalDateTime offset = LocalDateTime.of(2023, 3, 2, 12, 34, 56);
        assertThat(pattern.getNextOccurrence(offset)).contains(LocalDateTime.of(2023, 3, 2, 12, 35, 3));
    }

    @Test
    void testGetNextOccurrenceToNextMinuteWithIntervals() {
        CronPattern pattern = RecurrencePattern.forCronPattern()
                .using("58 */10 * * * *");

        LocalDateTime offset = LocalDateTime.of(2023, 3, 2, 12, 34, 56);
        assertThat(pattern.getNextOccurrence(offset)).contains(LocalDateTime.of(2023, 3, 2, 12, 40, 58));
    }

    @Test
    void testGetNextOccurrenceToNextHourWithIntervals() {
        CronPattern pattern = RecurrencePattern.forCronPattern()
                .using("1/2 5/10 2/3 * * *");

        LocalDateTime offset = LocalDateTime.of(2023, 3, 2, 12, 34, 56);
        assertThat(pattern.getNextOccurrence(offset)).contains(LocalDateTime.of(2023, 3, 2, 14, 5, 1));
    }

    @Test
    void testGetNextOccurrenceToNextDayWithIntervals() {
        CronPattern pattern = RecurrencePattern.forCronPattern()
                .using("33 23,45 9-17 */7 * *");

        LocalDateTime offset = LocalDateTime.of(2023, 3, 2, 12, 34, 56);
        assertThat(pattern.getNextOccurrence(offset)).contains(LocalDateTime.of(2023, 3, 8, 9, 23, 33));
    }

    @Test
    void testGetNextOccurrenceToNextMonthWithIntervals() {
        CronPattern pattern = RecurrencePattern.forCronPattern()
                .using("33 23,45 9-17 23 */6 *");

        LocalDateTime offset = LocalDateTime.of(2023, 3, 2, 12, 34, 56);
        assertThat(pattern.getNextOccurrence(offset)).contains(LocalDateTime.of(2023, 7, 23, 9, 23, 33));
    }

    @Test
    void testGetNextOccurrenceToNextMonthWithIntervalsWithShortPattern() {
        CronPattern pattern = RecurrencePattern.forCronPattern()
                .using("23,45 9-17 23 */6 *");

        LocalDateTime offset = LocalDateTime.of(2023, 3, 2, 12, 34, 56);
        assertThat(pattern.getNextOccurrence(offset)).contains(LocalDateTime.of(2023, 7, 23, 9, 23));
    }

    @Test
    void testGetNextOccurrenceToThirdMondayOfThisMonth() {
        CronPattern pattern = RecurrencePattern.forCronPattern()
                .using("0 0 7 * * 1#3");

        LocalDateTime offset = LocalDateTime.of(2023, 3, 2, 12, 34, 56);
        assertThat(pattern.getNextOccurrence(offset)).contains(LocalDateTime.of(2023, 3, 20, 7, 0, 0));
    }


    @Test
    void testGetNextOccurrenceToThirdMondayOrSecondFriday() {
        CronPattern pattern = RecurrencePattern.forCronPattern()
                .using("0 0 7 * * 1#3,5#2");

        assertThat(pattern.getNextOccurrence(LocalDateTime.of(2023, 3, 1, 12, 34, 56)))
                .contains(LocalDateTime.of(2023, 3, 10, 7, 0, 0));

        assertThat(pattern.getNextOccurrence(LocalDateTime.of(2023, 3, 15, 12, 34, 56)))
                .contains(LocalDateTime.of(2023, 3, 20, 7, 0, 0));
    }


    @Test
    void testGetNextOccurrenceToRepeat() {
        LocalDateTime first = LocalDateTime.of(2023, 3, 2, 12, 34, 56);
        CronPattern pattern = RecurrencePattern.forCronPattern()
                .using("* * * * * *")
                .withFirstOccurrence(first)
                .withRepeat(2);

        Optional<LocalDateTime> next = pattern.getNextOccurrence(first);
        assertThat(next).isPresent().contains(LocalDateTime.of(2023, 3, 2, 12, 34, 57));

        next = pattern.getNextOccurrence(next.get());
        assertThat(next).isPresent().contains(LocalDateTime.of(2023, 3, 2, 12, 34, 58));

        next = pattern.getNextOccurrence(next.get());
        assertThat(next).isEmpty();
    }


    @Test
    void testGetNextOccurrenceToRepeatInFuture() {
        LocalDateTime first = LocalDateTime.of(2000, 3, 2, 12, 34, 56);
        LocalDateTime offset = LocalDateTime.of(2023, 4, 2, 12, 34, 56);
        CronPattern pattern = RecurrencePattern.forCronPattern()
                .using("* * * * * *")
                .withFirstOccurrence(first)
                .withRepeat(100);
        assertThat(pattern.getNextOccurrence(offset)).isEmpty();
    }

    @Test
    void testToCodeStringWithRepeatAndInterval() {
        CronPattern pattern = RecurrencePattern.forCronPattern()
                .withRepeat(3)
                .withFirstOccurrence(LocalDateTime.of(2023, 3, 1, 6, 7, 8));
        assertThat(pattern.toCodes()).isEqualTo("T=C;R=3;F=2023-03-01T06:07:08;P=0 * * * * *");
    }

    @Test
    void testEmptyWithoutPattern() {
        assertThat(RecurrencePattern.forCronPattern().getPattern()).isEqualTo("0 * * * * *");
    }

    @Test
    void testEmptyPattern() {
        assertThat(RecurrencePattern.forCronPattern().using("").getPattern()).isEqualTo("0 * * * * *");
    }

    @Test
    void testInvalid() {
        assertThatException().isThrownBy(() -> RecurrencePattern.forCronPattern().using("invalid"));
    }


    @Test
    void testToCodeString() {
        CronPattern pattern = RecurrencePattern.forCronPattern()
                .using("* 0 23 * * MON-SUN");

        assertThat(pattern.getRepeat()).isZero();
        assertThat(pattern.getPattern()).isEqualTo("* 0 23 * * MON-SUN");
    }

    @Test
    void testFromCodeString() {
        String codes = "T=C;R=5;F=2023-03-01T06:07:08;P=* 0 1/12 * * MON";
        Optional<? extends RecurrencePattern> optional = RecurrencePattern.fromCodes(codes);
        assertThat(optional)
                .isPresent()
                .containsInstanceOf(CronPattern.class);

        CronPattern pattern = (CronPattern) optional.get();
        assertThat(pattern.getPattern()).isEqualTo("* 0 1/12 * * MON");
        assertThat(pattern.getRepeat()).isEqualTo(5);
        assertThat(pattern.getFirstOccurrence()).isEqualTo(LocalDateTime.of(2023, 3, 1, 6, 7, 8));
    }

    @Test
    void testFromCodesWithInvalidPattern() {
        assertThat(RecurrencePattern.fromCodes("T=C;P=invalid")).isEmpty();
    }

}