package nl.mirila.core.recurrence.patterns;

import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.Optional;

import static nl.mirila.core.recurrence.enums.TimeUnit.*;
import static org.assertj.core.api.Assertions.assertThat;

class FixedDelayPatternTest {

    private static final LocalDateTime offset = LocalDateTime.of(2023, 3, 1, 6, 7, 8);

    @Test
    void testNextOccurrenceSecondly() {
        FixedDelayPattern pattern = RecurrencePattern.forDelay().withTimeUnit(SECONDLY);
        assertThat(pattern.getNextOccurrence(offset)).contains(LocalDateTime.of(2023, 3, 1, 6, 7, 9));
    }

    @Test
    void testNextOccurrenceSecondlyWithInterval() {
        FixedDelayPattern pattern = RecurrencePattern.forDelay().withTimeUnit(SECONDLY).withInterval(3);
        assertThat(pattern.getNextOccurrence(offset)).contains(LocalDateTime.of(2023, 3, 1, 6, 7, 11));
    }

    @Test
    void testNextOccurrenceMinutely() {
        FixedDelayPattern pattern = RecurrencePattern.forDelay().withTimeUnit(MINUTELY);
        assertThat(pattern.getNextOccurrence(offset)).contains(LocalDateTime.of(2023, 3, 1, 6, 8, 8));
    }

    @Test
    void testNextOccurrenceMinutelyWithInterval() {
        FixedDelayPattern pattern = RecurrencePattern.forDelay().withTimeUnit(MINUTELY).withInterval(3);
        assertThat(pattern.getNextOccurrence(offset)).contains(LocalDateTime.of(2023, 3, 1, 6, 10, 8));
    }

    @Test
    void testNextOccurrenceHourly() {
        FixedDelayPattern pattern = RecurrencePattern.forDelay().withTimeUnit(HOURLY);
        assertThat(pattern.getNextOccurrence(offset)).contains(LocalDateTime.of(2023, 3, 1, 7, 7, 8));
    }

    @Test
    void testNextOccurrenceHourlyWithInterval() {
        FixedDelayPattern pattern = RecurrencePattern.forDelay().withTimeUnit(HOURLY).withInterval(3);
        assertThat(pattern.getNextOccurrence(offset)).contains(LocalDateTime.of(2023, 3, 1, 9, 7, 8));
    }

    @Test
    void testNextOccurrenceDaily() {
        FixedDelayPattern pattern = RecurrencePattern.forDelay().withTimeUnit(DAILY);
        assertThat(pattern.getNextOccurrence(offset)).contains(LocalDateTime.of(2023, 3, 2, 6, 7, 8));
    }

    @Test
    void testNextOccurrenceDailyWithInterval() {
        FixedDelayPattern pattern = RecurrencePattern.forDelay().withTimeUnit(DAILY).withInterval(3);
        assertThat(pattern.getNextOccurrence(offset)).contains(LocalDateTime.of(2023, 3, 4, 6, 7, 8));
    }

    @Test
    void testNextOccurrenceWeekly() {
        FixedDelayPattern pattern = RecurrencePattern.forDelay().withTimeUnit(WEEKLY);
        assertThat(pattern.getNextOccurrence(offset)).contains(LocalDateTime.of(2023, 3, 8, 6, 7, 8));
    }

    @Test
    void testNextOccurrenceWeeklyWithInterval() {
        FixedDelayPattern pattern = RecurrencePattern.forDelay().withTimeUnit(WEEKLY).withInterval(3);
        assertThat(pattern.getNextOccurrence(offset)).contains(LocalDateTime.of(2023, 3, 22, 6, 7, 8));
    }

    @Test
    void testNextOccurrenceMonthly() {
        FixedDelayPattern pattern = RecurrencePattern.forDelay().withTimeUnit(MONTHLY);
        assertThat(pattern.getNextOccurrence(offset)).contains(LocalDateTime.of(2023, 4, 1, 6, 7, 8));
    }

    @Test
    void testNextOccurrenceMonthlyWithInterval() {
        FixedDelayPattern pattern = RecurrencePattern.forDelay().withTimeUnit(MONTHLY).withInterval(3);
        assertThat(pattern.getNextOccurrence(offset)).contains(LocalDateTime.of(2023, 6, 1, 6, 7, 8));
    }

    @Test
    void testNextOccurrenceYearly() {
        FixedDelayPattern pattern = RecurrencePattern.forDelay().withTimeUnit(YEARLY);
        assertThat(pattern.getNextOccurrence(offset)).contains(LocalDateTime.of(2024, 3, 1, 6, 7, 8));
    }

    @Test
    void testNextOccurrenceYearlyWithInterval() {
        FixedDelayPattern pattern = RecurrencePattern.forDelay().withTimeUnit(YEARLY).withInterval(3);
        assertThat(pattern.getNextOccurrence(offset)).contains(LocalDateTime.of(2026, 3, 1, 6, 7, 8));
    }

    @Test
    void testToCodeString() {
        FixedDelayPattern pattern = RecurrencePattern.forDelay().withTimeUnit(DAILY).withInterval(5);
        assertThat(pattern.toCodes()).isEqualTo("T=D;U=D;I=5");
    }

    @Test
    void testFromCodeString() {
        Optional<? extends RecurrencePattern> optional = RecurrencePattern.fromCodes("T=D;U=W;I=3");
        assertThat(optional)
                .isPresent()
                .containsInstanceOf(FixedDelayPattern.class);

        FixedDelayPattern pattern = (FixedDelayPattern) optional.get();
        assertThat(pattern.getTimeUnit()).isEqualTo(WEEKLY);
        assertThat(pattern.getInterval()).isEqualTo(3L);
    }

}