package nl.mirila.core.recurrence.patterns;

import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.Optional;

import static nl.mirila.core.recurrence.enums.TimeUnit.*;
import static org.assertj.core.api.Assertions.assertThat;

class FixedRatePatternTest {

    private static final LocalDateTime offset = LocalDateTime.of(2023, 3, 1, 6, 7, 8);

    @Test
    void testNextOccurrenceSecondly() {
        FixedRatePattern pattern = RecurrencePattern.forRate().withTimeUnit(SECONDLY)
                .withFirstOccurrence(LocalDateTime.of(2023, 3, 1, 6, 0, 0));
        assertThat(pattern.getNextOccurrence(offset)).contains(LocalDateTime.of(2023, 3, 1, 6, 7, 9));
    }

    @Test
    void testNextOccurrenceSecondlyWithInterval() {
        FixedRatePattern pattern = RecurrencePattern.forRate().withTimeUnit(SECONDLY)
                .withFirstOccurrence(LocalDateTime.of(2023, 3, 1, 6, 0, 0))
                .withInterval(5);

        assertThat(pattern.getNextOccurrence(offset)).contains(LocalDateTime.of(2023, 3, 1, 6, 7, 10));
    }

    @Test
    void testNextOccurrenceMinutely() {
        FixedRatePattern pattern = RecurrencePattern.forRate().withTimeUnit(MINUTELY)
                .withFirstOccurrence(LocalDateTime.of(2023, 3, 1, 6, 0, 0));
        assertThat(pattern.getNextOccurrence(offset)).contains(LocalDateTime.of(2023, 3, 1, 6, 8, 0));
    }

    @Test
    void testNextOccurrenceMinutelyWithInterval() {
        FixedRatePattern pattern = RecurrencePattern.forRate().withTimeUnit(MINUTELY)
                .withFirstOccurrence(LocalDateTime.of(2023, 3, 1, 6, 0, 0))
                .withInterval(5);
        assertThat(pattern.getNextOccurrence(offset)).contains(LocalDateTime.of(2023, 3, 1, 6, 10, 0));
    }

    @Test
    void testNextOccurrenceHourly() {
        FixedRatePattern pattern = RecurrencePattern.forRate().withTimeUnit(HOURLY)
                .withFirstOccurrence(LocalDateTime.of(2023, 3, 1, 5, 1, 9));
        assertThat(pattern.getNextOccurrence(offset)).contains(LocalDateTime.of(2023, 3, 1, 7, 1, 9));
    }

    @Test
    void testNextOccurrenceHourlyWithInterval() {
        FixedRatePattern pattern = RecurrencePattern.forRate().withTimeUnit(HOURLY)
                .withFirstOccurrence(LocalDateTime.of(2023, 3, 1, 5, 1, 9))
                .withInterval(5);
        assertThat(pattern.getNextOccurrence(offset)).contains(LocalDateTime.of(2023, 3, 1, 10, 1, 9));
    }

    @Test
    void testNextOccurrenceDaily() {
        FixedRatePattern pattern = RecurrencePattern.forRate().withTimeUnit(DAILY)
                .withFirstOccurrence(LocalDateTime.of(2023, 2, 21, 5, 1, 9));
        assertThat(pattern.getNextOccurrence(offset)).contains(LocalDateTime.of(2023, 3, 2, 5, 1, 9));
    }

    @Test
    void testNextOccurrenceDailyWithInterval() {
        FixedRatePattern pattern = RecurrencePattern.forRate().withTimeUnit(DAILY)
                .withFirstOccurrence(LocalDateTime.of(2023, 2, 21, 5, 1, 9))
                .withInterval(5);
        assertThat(pattern.getNextOccurrence(offset)).contains(LocalDateTime.of(2023, 3, 3, 5, 1, 9));
    }

    @Test
    void testNextOccurrenceWeekly() {
        FixedRatePattern pattern = RecurrencePattern.forRate().withTimeUnit(WEEKLY)
                .withFirstOccurrence(LocalDateTime.of(2023, 2, 21, 5, 1, 9));
        assertThat(pattern.getNextOccurrence(offset)).contains(LocalDateTime.of(2023, 3, 7, 5, 1, 9));
    }

    @Test
    void testNextOccurrenceWeeklyWithInterval() {
        FixedRatePattern pattern = RecurrencePattern.forRate().withTimeUnit(WEEKLY)
                .withFirstOccurrence(LocalDateTime.of(2023, 2, 21, 5, 1, 9))
                .withInterval(5);
        assertThat(pattern.getNextOccurrence(offset)).contains(LocalDateTime.of(2023, 3, 28, 5, 1, 9));
    }

    @Test
    void testNextOccurrenceWeeklyWithRepeatReturningEmpty() {
        FixedRatePattern pattern = RecurrencePattern.forRate().withTimeUnit(WEEKLY)
                .withFirstOccurrence(LocalDateTime.of(2023, 2, 14, 5, 1, 9))
                .withRepeat(2);
        assertThat(pattern.getNextOccurrence(offset)).isEmpty();
    }

    @Test
    void testNextOccurrenceWeeklyWithRepeatReturningDate() {
        FixedRatePattern pattern = RecurrencePattern.forRate().withTimeUnit(WEEKLY)
                .withFirstOccurrence(LocalDateTime.of(2023, 2, 14, 5, 1, 9))
                .withRepeat(3);
        assertThat(pattern.getNextOccurrence(offset)).contains(LocalDateTime.of(2023, 3, 7, 5, 1, 9));
    }

    @Test
    void testNextOccurrenceMonthly() {
        FixedRatePattern pattern = RecurrencePattern.forRate().withTimeUnit(MONTHLY)
                .withFirstOccurrence(LocalDateTime.of(2023, 1, 31, 5, 1, 9));
        assertThat(pattern.getNextOccurrence(offset)).contains(LocalDateTime.of(2023, 3, 31, 5, 1, 9));
    }

    @Test
    void testNextOccurrenceMonthlyWithInterval() {
        FixedRatePattern pattern = RecurrencePattern.forRate().withTimeUnit(MONTHLY)
                .withFirstOccurrence(LocalDateTime.of(2023, 1, 13, 5, 1, 9))
                .withInterval(3);
        assertThat(pattern.getNextOccurrence(offset)).contains(LocalDateTime.of(2023, 4, 13, 5, 1, 9));
    }

    @Test
    void testNextOccurrenceYearly() {
        FixedRatePattern pattern = RecurrencePattern.forRate().withTimeUnit(YEARLY)
                .withFirstOccurrence(LocalDateTime.of(2022, 1, 13, 5, 1, 9));
        assertThat(pattern.getNextOccurrence(offset)).contains(LocalDateTime.of(2024, 1, 13, 5, 1, 9));
    }


    @Test
    void testNextOccurrenceYearlyWithLargeDifferenceBetweenFirstAndOffset() {
        FixedRatePattern pattern = RecurrencePattern.forRate().withTimeUnit(YEARLY)
                .withFirstOccurrence(LocalDateTime.of(2005, 1, 13, 5, 1, 9));
        assertThat(pattern.getNextOccurrence(offset)).contains(LocalDateTime.of(2024, 1, 13, 5, 1, 9));
    }

    @Test
    void testNextOccurrenceYearlyWithInterval() {
        FixedRatePattern pattern = RecurrencePattern.forRate().withTimeUnit(YEARLY)
                .withFirstOccurrence(LocalDateTime.of(2022, 1, 13, 5, 1, 9))
                .withInterval(3);
        assertThat(pattern.getNextOccurrence(offset)).contains(LocalDateTime.of(2025, 1, 13, 5, 1, 9));
    }

    @Test
    void testToCodeStringWithRepeat() {
        FixedRatePattern pattern = RecurrencePattern.forRate().withTimeUnit(HOURLY)
                .withRepeat(5)
                .withFirstOccurrence(LocalDateTime.of(2023, 3, 1, 6, 7, 8));
        assertThat(pattern.toCodes()).isEqualTo("T=R;U=H;R=5;F=2023-03-01T06:07:08");
    }

    @Test
    void testToCodeStringWithFirstOccurrence() {
        FixedRatePattern pattern = RecurrencePattern.forRate().withTimeUnit(DAILY)
                .withFirstOccurrence(LocalDateTime.of(2023, 3, 1, 6, 7, 8));
        assertThat(pattern.toCodes()).isEqualTo("T=R;U=D;F=2023-03-01T06:07:08");
    }


    @Test
    void testToCodeStringWithRepeatAndInterval() {
        FixedRatePattern pattern = RecurrencePattern.forRate().withTimeUnit(DAILY)
                .withInterval(5)
                .withRepeat(3)
                .withFirstOccurrence(LocalDateTime.of(2023, 3, 1, 6, 7, 8));
        assertThat(pattern.toCodes()).isEqualTo("T=R;U=D;I=5;R=3;F=2023-03-01T06:07:08");
    }

    @Test
    void testFromCodeString() {
        Optional<? extends RecurrencePattern> optional = RecurrencePattern.fromCodes("T=R;U=W;I=5;R=3;F=2023-03-01T06:07:08");
        assertThat(optional)
                .isPresent()
                .containsInstanceOf(FixedRatePattern.class);

        FixedRatePattern pattern = (FixedRatePattern) optional.get();
        assertThat(pattern.getTimeUnit()).isEqualTo(WEEKLY);
        assertThat(pattern.getInterval()).isEqualTo(5);
        assertThat(pattern.getRepeat()).isEqualTo(3);
        assertThat(pattern.getFirstOccurrence()).isEqualTo(LocalDateTime.of(2023, 3, 1, 6, 7, 8));
    }

}