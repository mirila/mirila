package nl.mirila.core.recurrence.enums;

import org.junit.jupiter.api.Test;

import static nl.mirila.core.recurrence.enums.SequenceFunction.*;
import static org.assertj.core.api.Assertions.assertThat;

class SequenceFunctionTest {

    @Test
    void testFibonacciWithN0() {
        assertThat(FIBONACCI.getValue(0)).contains(0);
    }

    @Test
    void testFibonacciWithN1() {
        assertThat(FIBONACCI.getValue(1)).contains(1);
    }

    @Test
    void testFibonacciWithN2() {
        assertThat(FIBONACCI.getValue(2)).contains(1);
    }

    @Test
    void testFibonacciWithN3() {
        assertThat(FIBONACCI.getValue(3)).contains(2);
    }

    @Test
    void testFibonacciWithN4() {
        assertThat(FIBONACCI.getValue(4)).contains(3);
    }

    @Test
    void testFibonacciWithN5() {
        assertThat(FIBONACCI.getValue(5)).contains(5);
    }

    @Test
    void testFibonacciWithN10() {
        assertThat(FIBONACCI.getValue(10)).contains(55);
    }

    @Test
    void testLucasWithN0() {
        assertThat(LUCAS.getValue(0)).contains(2);
    }

    @Test
    void testLucasWithN1() {
        assertThat(LUCAS.getValue(1)).contains(1);
    }

    @Test
    void testLucasWithN2() {
        assertThat(LUCAS.getValue(2)).contains(3);
    }

    @Test
    void testLucasWithN3() {
        assertThat(LUCAS.getValue(3)).contains(4);
    }

    @Test
    void testLucasWithN4() {
        assertThat(LUCAS.getValue(4)).contains(7);
    }

    @Test
    void testLucasWithN5() {
        assertThat(LUCAS.getValue(5)).contains(11);
    }

    @Test
    void testLucasWithN10() {
        assertThat(LUCAS.getValue(10)).contains(123);
    }

    @Test
    void testFactorialWithN0() {
        assertThat(FACTORIAL.getValue(0)).contains(1);
    }

    @Test
    void testFactorialWithN1() {
        assertThat(FACTORIAL.getValue(1)).contains(1);
    }

    @Test
    void testFactorialWithN2() {
        assertThat(FACTORIAL.getValue(2)).contains(2);
    }

    @Test
    void testFactorialWithN3() {
        assertThat(FACTORIAL.getValue(3)).contains(6);
    }

    @Test
    void testFactorialWithN4() {
        assertThat(FACTORIAL.getValue(4)).contains(24);
    }

    @Test
    void testFactorialWithN5() {
        assertThat(FACTORIAL.getValue(5)).contains(120);
    }

    @Test
    void testFactorialWithN10() {
        assertThat(FACTORIAL.getValue(10)).contains(3_628_800);
    }


}