package nl.mirila.core.recurrence.patterns;

import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static nl.mirila.core.recurrence.enums.SequenceFunction.*;
import static nl.mirila.core.recurrence.enums.TimeUnit.*;
import static org.assertj.core.api.Assertions.assertThat;

class SequencePatternTest {

    @Test
    void testNextOccurrencesWithLimitedListFromNow() {
        SequencePattern pattern = RecurrencePattern.forSequence()
                .withTimeUnit(DAILY)
                .withSequence(1, 2, 3, 4, 5);
        assertThat(pattern.getNextOccurrences()).hasSize(5);
        assertThat(pattern.getSequenceList()).containsExactly(1, 2, 3, 4, 5);
    }

    @Test
    void testNextOccurrencesWithSimpleSequence() {
        SequencePattern pattern = RecurrencePattern.forSequence()
                .withTimeUnit(DAILY)
                .withSequence(1, 2, 3, 4, 5)
                .withFirstOccurrence(LocalDateTime.of(2023, 2, 27, 12, 34, 56));
        List<LocalDateTime> occurrences = pattern.getNextOccurrences(pattern.getFirstOccurrence());

        assertThat(occurrences).containsExactly(LocalDateTime.of(2023, 2, 28, 12, 34, 56),
                                                LocalDateTime.of(2023, 3, 2, 12, 34, 56),
                                                LocalDateTime.of(2023, 3, 5, 12, 34, 56),
                                                LocalDateTime.of(2023, 3, 9, 12, 34, 56),
                                                LocalDateTime.of(2023, 3, 14, 12, 34, 56));
    }

    @Test
    void testNextOccurrenceWithSimpleSequenceReturningDate() {
        SequencePattern pattern = RecurrencePattern.forSequence()
                .withTimeUnit(DAILY)
                .withSequence(1, 2, 3, 4, 5)
                .withFirstOccurrence(LocalDateTime.of(2023, 2, 27, 12, 34, 56));

        LocalDateTime offset = LocalDateTime.of(2023, 3, 9, 12, 34, 56);
        Optional<LocalDateTime> optional = pattern.getNextOccurrence(offset);
        assertThat(optional).contains(LocalDateTime.of(2023, 3, 14, 12, 34, 56));
    }

    @Test
    void testNextOccurrenceWithSimpleSequenceReturningEmpty() {
        SequencePattern pattern = RecurrencePattern.forSequence()
                .withTimeUnit(DAILY)
                .withSequence(1, 2, 3, 4, 5)
                .withFirstOccurrence(LocalDateTime.of(2023, 2, 27, 12, 34, 56));

        LocalDateTime offset = LocalDateTime.of(2023, 3, 14, 12, 34, 56);
        Optional<LocalDateTime> optional = pattern.getNextOccurrence(offset);
        assertThat(optional).isEmpty();
    }

    @Test
    void testNextOccurrencesWithSimpleSequenceAndRepeat() {
        SequencePattern pattern = RecurrencePattern.forSequence()
                .withTimeUnit(DAILY)
                .withSequence(1, 2, 3, 4, 5)
                .withFirstOccurrence(LocalDateTime.of(2023, 2, 27, 12, 34, 56));
        List<LocalDateTime> occurrences = pattern.getNextOccurrences(pattern.getFirstOccurrence(), 3);

        assertThat(occurrences).containsExactly(LocalDateTime.of(2023, 2, 28, 12, 34, 56),
                                                LocalDateTime.of(2023, 3, 2, 12, 34, 56),
                                                LocalDateTime.of(2023, 3, 5, 12, 34, 56));
    }

    @Test
    void testNextOccurrencesWithoutSequence() {
        SequencePattern pattern = RecurrencePattern.forSequence().withTimeUnit(DAILY)
                .withFirstOccurrence(LocalDateTime.of(2023, 2, 27, 12, 34, 56));
        List<LocalDateTime> occurrences = pattern.getNextOccurrences(pattern.getFirstOccurrence());
        assertThat(occurrences).isEmpty();
    }


    @Test
    void testNextOccurrencesWithFibonacciSequenceAndRepeat() {
        SequencePattern pattern = RecurrencePattern.forSequence()
                .withTimeUnit(DAILY)
                .withSequenceFunction(FIBONACCI)
                .withRepeat(5)
                .withFirstOccurrence(LocalDateTime.of(2023, 2, 27, 12, 34, 56));
        List<LocalDateTime> occurrences = pattern.getNextOccurrences(pattern.getFirstOccurrence());

        assertThat(pattern.getSequenceFunction()).contains(FIBONACCI);
        assertThat(occurrences).containsExactly(LocalDateTime.of(2023, 2, 28, 12, 34, 56),
                                                LocalDateTime.of(2023, 3, 1, 12, 34, 56),
                                                LocalDateTime.of(2023, 3, 3, 12, 34, 56),
                                                LocalDateTime.of(2023, 3, 6, 12, 34, 56),
                                                LocalDateTime.of(2023, 3, 11, 12, 34, 56));
    }

    @Test
    void testNextOccurrencesWithFactorialSequence() {
        SequencePattern pattern = RecurrencePattern.forSequence()
                .withTimeUnit(DAILY)
                .withSequenceFunction(FACTORIAL)
                .withFirstOccurrence(LocalDateTime.of(2023, 2, 27, 12, 34, 56));
        List<LocalDateTime> occurrences = pattern.getNextOccurrences(pattern.getFirstOccurrence());

        assertThat(occurrences)
                .hasSize(12)
                .containsSubsequence(LocalDateTime.of(2023, 2, 28, 12, 34, 56),
                                     LocalDateTime.of(2023, 3, 2, 12, 34, 56),
                                     LocalDateTime.of(2023, 3, 8, 12, 34, 56),
                                     LocalDateTime.of(2023, 4, 1, 12, 34, 56));
    }

    @Test
    void testToCodeStringWithSequenceList() {
        SequencePattern pattern = SequencePattern.forSequence()
                .withTimeUnit(DAILY)
                .withRepeat(3)
                .withSequence(1, 2, 3, 4)
                .withFirstOccurrence(LocalDateTime.of(2023, 3, 1, 6, 7, 8));
        assertThat(pattern.toCodes()).isEqualTo("T=S;U=D;R=3;F=2023-03-01T06:07:08;S=1,2,3,4");
    }

    @Test
    void testToCodeStringWithSequenceFunction() {
        SequencePattern pattern = SequencePattern.forSequence()
                .withTimeUnit(HOURLY)
                .withSequenceFunction(FACTORIAL);
        assertThat(pattern.toCodes()).isEqualTo("T=S;U=H;S=FACT");
    }

    @Test
    void testFromCodeStringWithSequenceList() {
        String codes = "T=S;U=S;R=2;F=2023-03-01T06:07:08;S=8,16,32,64,128";
        Optional<? extends RecurrencePattern> optional = RecurrencePattern.fromCodes(codes);
        assertThat(optional)
                .isPresent()
                .containsInstanceOf(SequencePattern.class);

        SequencePattern pattern = (SequencePattern) optional.get();
        assertThat(pattern.getTimeUnit()).isEqualTo(SECONDLY);
        assertThat(pattern.getRepeat()).isEqualTo(2);
        assertThat(pattern.getFirstOccurrence()).isEqualTo(LocalDateTime.of(2023, 3, 1, 6, 7, 8));
        assertThat(pattern.getSequenceFunction()).isEmpty();
        assertThat(pattern.getSequenceList()).containsExactly(8, 16, 32, 64, 128);
    }

    @Test
    void testFromCodeStringWithSequenceFunction() {
        String codes = "T=S;U=S;S=LUC";
        Optional<? extends RecurrencePattern> optional = RecurrencePattern.fromCodes(codes);
        assertThat(optional)
                .isPresent()
                .containsInstanceOf(SequencePattern.class);

        SequencePattern pattern = (SequencePattern) optional.get();
        assertThat(pattern.getTimeUnit()).isEqualTo(SECONDLY);
        assertThat(pattern.getSequenceFunction()).contains(LUCAS);
        assertThat(pattern.getSequenceList()).isEmpty();
    }

}