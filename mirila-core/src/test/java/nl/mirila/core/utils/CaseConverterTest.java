package nl.mirila.core.utils;

import org.junit.jupiter.api.Test;

import static nl.mirila.core.utils.CaseConverter.*;
import static org.assertj.core.api.Assertions.assertThat;

class CaseConverterTest {

    @Test
    void testBlankCapitalizeFirstLetter() {
        assertThat(CaseConverter.capitalizeFirstLetter(null)).isNull();
        assertThat(CaseConverter.capitalizeFirstLetter("")).isEmpty();
        assertThat(CaseConverter.capitalizeFirstLetter(" ")).isEqualTo(" ");
    }

    @Test
    void testCapitalizeFirstLetter() {
        assertThat(CaseConverter.capitalizeFirstLetter("test")).isEqualTo("Test");
        assertThat(CaseConverter.capitalizeFirstLetter("testTest")).isEqualTo("TestTest");
    }

    @Test
    void testBlankLowercaseFirstLetter() {
        assertThat(lowercaseFirstLetter(null)).isNull();
        assertThat(lowercaseFirstLetter("")).isEmpty();
        assertThat(lowercaseFirstLetter(" ")).isEqualTo(" ");
    }

    @Test
    void testLowercaseFirstLetter() {
        assertThat(lowercaseFirstLetter("test")).isEqualTo("test");
        assertThat(lowercaseFirstLetter("testTest")).isEqualTo("testTest");
        assertThat(lowercaseFirstLetter("Test")).isEqualTo("test");
        assertThat(lowercaseFirstLetter("TestTest")).isEqualTo("testTest");
    }

    @Test
    void testNullStrings() {
        assertThat(CAMEL_CASE.toCamelCase(null)).isNull();
        assertThat(KEBAB_CASE.toKebabCase(null)).isNull();
        assertThat(PASCAL_CASE.toPascalCase(null)).isNull();
        assertThat(SCREAMING_SNAKE_CASE.toScreamingSnakeCase(null)).isNull();
        assertThat(SENTENCE_CASE.toSentenceCase(null)).isNull();
        assertThat(SNAKE_CASE.toSnakeCase(null)).isNull();
        assertThat(DOT_CASE.toDotCase(null)).isNull();
    }

    @Test
    void testBlankStrings() {
        assertThat(CAMEL_CASE.toCamelCase("")).isEmpty();
        assertThat(CAMEL_CASE.toCamelCase(" ")).isEmpty();
        assertThat(KEBAB_CASE.toKebabCase("")).isEmpty();
        assertThat(KEBAB_CASE.toKebabCase(" ")).isEmpty();
        assertThat(PASCAL_CASE.toPascalCase("")).isEmpty();
        assertThat(PASCAL_CASE.toPascalCase(" ")).isEmpty();
        assertThat(SCREAMING_SNAKE_CASE.toScreamingSnakeCase("")).isEmpty();
        assertThat(SCREAMING_SNAKE_CASE.toScreamingSnakeCase(" ")).isEmpty();
        assertThat(SENTENCE_CASE.toSentenceCase("")).isEmpty();
        assertThat(SENTENCE_CASE.toSentenceCase(" ")).isEmpty();
        assertThat(SNAKE_CASE.toSnakeCase("")).isEmpty();
        assertThat(SNAKE_CASE.toSnakeCase(" ")).isEmpty();
        assertThat(DOT_CASE.toSnakeCase("")).isEmpty();
        assertThat(DOT_CASE.toSnakeCase(" ")).isEmpty();
    }

    @Test
    void testToCamelCase() {
        assertThat(CAMEL_CASE.toCamelCase("helloWorld")).isEqualTo("helloWorld");
        assertThat(KEBAB_CASE.toCamelCase("hello-world")).isEqualTo("helloWorld");
        assertThat(PASCAL_CASE.toCamelCase("HelloWorld")).isEqualTo("helloWorld");
        assertThat(SCREAMING_SNAKE_CASE.toCamelCase("HELLO_WORLD")).isEqualTo("helloWorld");
        assertThat(SENTENCE_CASE.toCamelCase("Hello world")).isEqualTo("helloWorld");
        assertThat(SNAKE_CASE.toCamelCase("hello_world")).isEqualTo("helloWorld");
        assertThat(DOT_CASE.toCamelCase("hello.world")).isEqualTo("helloWorld");
    }

    @Test
    void testToKebabCase() {
        assertThat(CAMEL_CASE.toKebabCase("helloWorld")).isEqualTo("hello-world");
        assertThat(KEBAB_CASE.toKebabCase("hello-world")).isEqualTo("hello-world");
        assertThat(PASCAL_CASE.toKebabCase("HelloWorld")).isEqualTo("hello-world");
        assertThat(SCREAMING_SNAKE_CASE.toKebabCase("HELLO_WORLD")).isEqualTo("hello-world");
        assertThat(SENTENCE_CASE.toKebabCase("Hello world")).isEqualTo("hello-world");
        assertThat(SNAKE_CASE.toKebabCase("hello_world")).isEqualTo("hello-world");
        assertThat(DOT_CASE.toKebabCase("hello.world")).isEqualTo("hello-world");
    }

    @Test
    void testToPascalCase() {
        assertThat(CAMEL_CASE.toPascalCase("helloWorld")).isEqualTo("HelloWorld");
        assertThat(KEBAB_CASE.toPascalCase("hello-world")).isEqualTo("HelloWorld");
        assertThat(PASCAL_CASE.toPascalCase("HelloWorld")).isEqualTo("HelloWorld");
        assertThat(SCREAMING_SNAKE_CASE.toPascalCase("HELLO_WORLD")).isEqualTo("HelloWorld");
        assertThat(SENTENCE_CASE.toPascalCase("Hello world")).isEqualTo("HelloWorld");
        assertThat(SNAKE_CASE.toPascalCase("hello_world")).isEqualTo("HelloWorld");
        assertThat(DOT_CASE.toPascalCase("hello.world")).isEqualTo("HelloWorld");
    }

    @Test
    void testToScreamingSnakeCase() {
        assertThat(CAMEL_CASE.toScreamingSnakeCase("helloWorld")).isEqualTo("HELLO_WORLD");
        assertThat(KEBAB_CASE.toScreamingSnakeCase("hello-world")).isEqualTo("HELLO_WORLD");
        assertThat(PASCAL_CASE.toScreamingSnakeCase("HelloWorld")).isEqualTo("HELLO_WORLD");
        assertThat(SCREAMING_SNAKE_CASE.toScreamingSnakeCase("HELLO_WORLD")).isEqualTo("HELLO_WORLD");
        assertThat(SENTENCE_CASE.toScreamingSnakeCase("Hello world")).isEqualTo("HELLO_WORLD");
        assertThat(SNAKE_CASE.toScreamingSnakeCase("hello_world")).isEqualTo("HELLO_WORLD");
        assertThat(DOT_CASE.toScreamingSnakeCase("hello.world")).isEqualTo("HELLO_WORLD");
    }
    @Test
    void testToSentenceCase() {
        assertThat(CAMEL_CASE.toSentenceCase("helloWorld")).isEqualTo("Hello world");
        assertThat(KEBAB_CASE.toSentenceCase("hello-world")).isEqualTo("Hello world");
        assertThat(PASCAL_CASE.toSentenceCase("HelloWorld")).isEqualTo("Hello world");
        assertThat(SCREAMING_SNAKE_CASE.toSentenceCase("HELLO_WORLD")).isEqualTo("Hello world");
        assertThat(SENTENCE_CASE.toSentenceCase("Hello world")).isEqualTo("Hello world");
        assertThat(SNAKE_CASE.toSentenceCase("hello_world")).isEqualTo("Hello world");
        assertThat(DOT_CASE.toSentenceCase("hello.world")).isEqualTo("Hello world");
    }

    @Test
    void testToSnakeCase() {
        assertThat(CAMEL_CASE.toSnakeCase("helloWorld")).isEqualTo("hello_world");
        assertThat(KEBAB_CASE.toSnakeCase("hello-world")).isEqualTo("hello_world");
        assertThat(PASCAL_CASE.toSnakeCase("HelloWorld")).isEqualTo("hello_world");
        assertThat(SCREAMING_SNAKE_CASE.toSnakeCase("HELLO_WORLD")).isEqualTo("hello_world");
        assertThat(SENTENCE_CASE.toSnakeCase("Hello world")).isEqualTo("hello_world");
        assertThat(SNAKE_CASE.toSnakeCase("hello_world")).isEqualTo("hello_world");
        assertThat(DOT_CASE.toSnakeCase("hello.world")).isEqualTo("hello_world");
    }

    @Test
    void testToDotCase() {
        assertThat(CAMEL_CASE.toDotCase("helloWorld")).isEqualTo("hello.world");
        assertThat(KEBAB_CASE.toDotCase("hello-world")).isEqualTo("hello.world");
        assertThat(PASCAL_CASE.toDotCase("HelloWorld")).isEqualTo("hello.world");
        assertThat(SCREAMING_SNAKE_CASE.toDotCase("HELLO_WORLD")).isEqualTo("hello.world");
        assertThat(SENTENCE_CASE.toDotCase("Hello world")).isEqualTo("hello.world");
        assertThat(SNAKE_CASE.toDotCase("hello_world")).isEqualTo("hello.world");
        assertThat(DOT_CASE.toDotCase("hello.world")).isEqualTo("hello.world");
    }

    @Test
    void testWithNumbers() {
        assertThat(CAMEL_CASE.toKebabCase("helloWorld123")).isEqualTo("hello-world-123");
        assertThat(SNAKE_CASE.toKebabCase("hello_world_123")).isEqualTo("hello-world-123");
        assertThat(DOT_CASE.toKebabCase("hello.world.123")).isEqualTo("hello-world-123");
    }

    @Test
    void testWithIDs() {
        assertThat(PASCAL_CASE.toKebabCase("MyID")).isEqualTo("my-id");
        assertThat(PASCAL_CASE.toKebabCase("MyIDs")).isEqualTo("my-i-ds");
        assertThat(SENTENCE_CASE.toKebabCase("My ID")).isEqualTo("my-id");
        assertThat(PASCAL_CASE.toKebabCase("MySQL")).isEqualTo("my-sql");
        assertThat(PASCAL_CASE.toKebabCase("MySQLConverter")).isEqualTo("my-sql-converter");
    }

}
