package nl.mirila.core.utils;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class ListUtilsTest {

    private static final List<String> list = new ArrayList<>();

    static {
        list.add("A");
        list.add("B");
        list.add("C");
        list.add(null);
        list.add("D");
        list.add("E");
    }

    @Test
    void testGetWithValidValue() {
        assertThat(ListUtils.get(list, 2)).contains("C");
    }

    @Test
    void testGetWithNull() {
        assertThat(ListUtils.get(list, 3)).isEmpty();
    }

    @Test
    void testGetWithOutOfBounds() {
        assertThat(ListUtils.get(list, 123)).isEmpty();
    }

    @Test
    void testGetOrDefaultWithValidValue() {
        assertThat(ListUtils.getOrDefault(list, 2, "DEFAULT")).contains("C");
    }

    @Test
    void testGetOrDefaultWithNull() {
        assertThat(ListUtils.getOrDefault(list, 3, "DEFAULT")).contains("DEFAULT");
    }

    @Test
    void testGetOrDefaultWithOutOfBounds() {
        assertThat(ListUtils.getOrDefault(list, 123, "DEFAULT")).contains("DEFAULT");
    }

}