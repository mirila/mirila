package nl.mirila.core.utils;

import org.junit.jupiter.api.Test;

import static nl.mirila.core.utils.ObjectUtils.setIfMissing;
import static org.assertj.core.api.Assertions.assertThat;

class ObjectUtilsTest {

    private String name;

    @Test
    void testSetIfMissingWithNullAndLambda() {
        name = null;
        setIfMissing(this::getName, this::setName, this::provideNewName);
        assertThat(name).isEqualTo("Lama Michel Rinpoche");
    }

    @Test
    void testSetIfMissingWithValueAndLambda() {
        name = "Lama Gangchen Rinpoche";
        setIfMissing(this::getName, this::setName, this::provideNewName);
        assertThat(name).isEqualTo("Lama Gangchen Rinpoche");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String provideNewName() {
        return "Lama Michel Rinpoche";
    }

}