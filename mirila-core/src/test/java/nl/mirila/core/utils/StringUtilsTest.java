package nl.mirila.core.utils;

import org.junit.jupiter.api.Test;

import java.util.Map;

import static nl.mirila.core.utils.ParserOptions.defaults;
import static org.assertj.core.api.Assertions.assertThat;

class StringUtilsTest {

    @Test
    void testToMap() {
        Map<String, String> map = ListUtils.toMap("A=1\nB=2\nC=3");
        assertThat(map)
                .containsOnlyKeys("A", "B", "C")
                .containsValues("1", "2", "3");
    }

    @Test
    void testToMapWithNull() {
        Map<String, String> map = ListUtils.toMap(null);
        assertThat(map).isEmpty();
    }

    @Test
    void testToMapWithEmptyString() {
        Map<String, String> map = ListUtils.toMap("");
        assertThat(map).isEmpty();
    }

    @Test
    void testToMapWithNonKeyValueString() {
        Map<String, String> map = ListUtils.toMap("ABCDEFG\nHIJKLMNOP");
        assertThat(map).isEmpty();
    }

    @Test
    void testToMapWithPartlyNonKeyValueString() {
        Map<String, String> map = ListUtils.toMap("ABC=DEF\nGHIJK\n=\nLMN=OPQ");
        assertThat(map)
                .containsOnlyKeys("ABC", "LMN")
                .containsValues("DEF", "OPQ");
    }

    @Test
    void testToMapWithCustomSeparators() {
        Map<String, String> map = ListUtils.toMap("ABC:DEF;GHI=JK;:;LMN:OPQ",
                                                  defaults().withLineSeparator(";").withValueSeparator(":"));
        assertThat(map)
                .containsOnlyKeys("ABC", "LMN")
                .containsValues("DEF", "OPQ");
    }

    @Test
    void testToMapWithoutTrimmedKeys() {
        Map<String, String> map = ListUtils
                .toMap(" ABC = DEF \nGHI =  \n LMN= OPQ ");
        assertThat(map)
                .containsOnlyKeys(" ABC ", "GHI ", " LMN")
                .containsValues(" DEF ", "  ", " OPQ ");
    }

    @Test
    void testToMapWithTrimmedKeys() {
        Map<String, String> map = ListUtils.toMap(" ABC = DEF \nGHI=  \n LMN= OPQ ",
                                                  defaults().withTrimmedKeys());
        assertThat(map)
                .containsOnlyKeys("ABC", "GHI", "LMN")
                .containsValues(" DEF ", "  ", " OPQ ");
    }

    @Test
    void testToMapWithTrimmedValues() {
        Map<String, String> map = ListUtils.toMap(" ABC = DEF \nGHI =  \n LMN= OPQ ",
                                                  defaults().withTrimmedValues());
        assertThat(map)
                .containsOnlyKeys(" ABC ", "GHI ", " LMN")
                .containsValues("DEF", "", "OPQ");
    }

    @Test
    void testToMapWithTrimmedKeysAndValues() {
        Map<String, String> map = ListUtils.toMap(" ABC = DEF \nGHI=  \n LMN= OPQ ",
                                                  defaults().withTrimmedKeys().withTrimmedValues());
        assertThat(map)
                .containsOnlyKeys("ABC", "GHI", "LMN")
                .containsValues("DEF", "", "OPQ");
    }

    @Test
    void testSplitValues() {
        assertThat(ListUtils.split("abc,def,ghi", ","))
                .hasSize(3)
                .containsExactly("abc", "def", "ghi");
    }

    @Test
    void testSplitValuesSingleValue() {
        assertThat(ListUtils.split("abc", ","))
                .hasSize(1)
                .containsExactly("abc");
    }

    @Test
    void testSplitWithEmpty() {
        assertThat(ListUtils.split("", ",")).hasSize(0);
    }

    @Test
    void testSplitWithNull() {
        assertThat(ListUtils.split(null, ",")).hasSize(0);
    }

    @Test
    void testSplitWithLimitValues() {
        assertThat(ListUtils.split("abc,def,ghi", ",", 2))
                .hasSize(2)
                .containsExactly("abc", "def,ghi");
    }

}