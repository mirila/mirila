package nl.mirila.cache.memcached;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import nl.mirila.cache.core.Cache;
import nl.mirila.core.settings.modules.KeyValuesSettingsModule;
import nl.mirila.model.core.jackson.IdType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;


class MemcachedCacheServiceIT {

    private Cache cache;

    @BeforeEach
    void setUp() {
        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                install(new KeyValuesSettingsModule(Collections.emptyMap()));
                bind(IdType.class).toInstance(IdType.STRING);
                install(new MemcachedCacheModule());
            }
        });
        cache = injector.getInstance(Cache.class);
    }

    @Test
    void testCacheAndGet() {
        cache.set("key1", "value1");
        cache.set("key2", "value2");
        cache.set("key3", 123);

        assertThat(cache.getString("key1")).contains("value1");
        assertThat(cache.getString("key2")).contains("value2");
        assertThat(cache.get("key3", Integer.class)).contains(123);
        assertThat(cache.getString("key4")).isEmpty();
    }

    @Test
    void testDelete() {
        cache.set("key.delete", "value");
        assertThat(cache.getString("key.delete")).contains("value");

        cache.delete("key.delete");
        assertThat(cache.getString("key.delete")).isEmpty();
    }

}
