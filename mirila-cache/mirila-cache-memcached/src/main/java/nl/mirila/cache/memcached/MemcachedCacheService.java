package nl.mirila.cache.memcached;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import nl.mirila.cache.core.services.CacheService;
import nl.mirila.drivers.memcached.MemcachedDriverService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Optional;

/**
 * A {@link CacheService} using a Memcached Server.
 */
public class MemcachedCacheService implements CacheService {

    private static final Logger logger = LogManager.getLogger(MemcachedCacheService.class);

    private final MemcachedDriverService service;
    private final ObjectMapper mapper;

    /**
     * Initialize a new instance.
     */
    @Inject
    public MemcachedCacheService(MemcachedDriverService service, ObjectMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void set(String key, Object value, int timeToLive) {
        if ((key == null) || (value == null)) {
            return;
        }
        String strValue;
        if (value instanceof String) {
            strValue = value.toString();
        } else {
            try {
                strValue = mapper.writeValueAsString(value);
            } catch (JsonProcessingException e) {
                logger.warn(e.getMessage(), e);
                return;
            }
        }
        service.add(key, strValue, timeToLive);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T> Optional<T> get(String key, Class<T> valueClass) {
        Optional<Object> optional = service.get(key);
        if (optional.isEmpty()) {
            return Optional.empty();
        }

        Object value = optional.get();
        if (valueClass.equals(String.class)) {
            return Optional.of(valueClass.cast(value.toString()));
        }

        if (value instanceof String) {
            try {
                return Optional.of(mapper.readValue(value.toString(), valueClass));
            } catch (JsonProcessingException e) {
                String msg = "Error while parsing key '{}' from Redis cache: {}.\n{}}";
                logger.warn(msg, key, e.getMessage(), optional.get());
            }
        } else if (valueClass.isInstance(value)) {
            return Optional.of(valueClass.cast(value));
        }
        return Optional.empty();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void delete(List<String> keys) {
        keys.forEach(service::delete);
    }

}
