package nl.mirila.cache.memcached;

import com.google.inject.AbstractModule;
import com.google.inject.multibindings.OptionalBinder;
import nl.mirila.cache.core.CacheCoreModule;
import nl.mirila.cache.core.services.CacheService;

public class MemcachedCacheModule extends AbstractModule {

    @Override
    protected void configure() {
        install(new CacheCoreModule());

        OptionalBinder.newOptionalBinder(binder(), CacheService.class)
                .setBinding()
                .to(MemcachedCacheService.class);
    }

}
