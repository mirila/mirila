package nl.mirila.cache.core;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.name.Names;
import nl.mirila.cache.core.services.CacheService;
import nl.mirila.cache.core.services.InMemoryCacheService;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class RealmBasedCacheTest {

    @Test
    void testPrefixKeyWithRealm() {
        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                bind(String.class).annotatedWith(Names.named("realm")).toInstance("my-realm");
                bind(CacheService.class).to(InMemoryCacheService.class);
            }
        });
        RealmBasedCache cache = injector.getInstance(RealmBasedCache.class);
        assertThat(cache.prefixKey("key")).isEqualTo("my-realm:key");

        // Cache should work without specifying the realm.
        cache.set("test-key", "test-value");
        assertThat(cache.getString("test-key")).contains("test-value");

        // The CacheService should work with specifying the realm.
        CacheService service = injector.getInstance(CacheService.class);
        assertThat(service.getString("test-key")).isEmpty();
        assertThat(service.getString("my-realm:test-key")).contains("test-value");

        cache.delete("test-key");
        assertThat(cache.getString("test-key")).isEmpty();
    }

    @Test
    void testPrefixKeyWithoutRealm() {
        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                bind(CacheService.class).to(InMemoryCacheService.class);
            }
        });
        RealmBasedCache cache = injector.getInstance(RealmBasedCache.class);
        assertThat(cache.prefixKey("key")).isEqualTo("key");

        // Cache should work without specifying the realm, which is actually not set.
        cache.set("test-key", "test-value");
        assertThat(cache.getString("test-key")).contains("test-value");

        // The CacheService should also work without the realm.
        CacheService service = injector.getInstance(CacheService.class);
        assertThat(service.getString("a-realm:test-key")).isEmpty();
        assertThat(service.getString("test-key")).contains("test-value");

        cache.delete("test-key");
        assertThat(cache.getString("test-key")).isEmpty();
    }

}
