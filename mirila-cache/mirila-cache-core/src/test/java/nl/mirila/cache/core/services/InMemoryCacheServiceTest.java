package nl.mirila.cache.core.services;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;

class InMemoryCacheServiceTest {

    private InMemoryCacheService cache;

    @BeforeEach
    void setUp() {
        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                install(new InMemoryCacheModule());
            }
        });
        cache = injector.getInstance(InMemoryCacheService.class);
    }

    @Test
    void testCacheAndGet() {
        cache.set("key1", "value1");
        cache.set("key2", "value2");
        cache.set("key3", "value3", -10);
        cache.set("null-key", null);

        assertThat(cache.getString("key1")).isPresent();
        assertThat(cache.getString("key2")).isPresent();
        assertThat(cache.getString("key3")).isEmpty();
        assertThat(cache.getString("key4")).isEmpty();

        assertThat(cache.get("key1", Integer.class)).isNotPresent();

        assertThat(cache.getString("key1")).contains("value1");
        assertThat(cache.getString("key2")).contains("value2");
    }

    @Test
    void testDelete() {
        String key = "key.delete";
        String value = "value";

        cache.set(key, value);
        assertThat(cache.getString(key)).isPresent();
        assertThat(cache.getString(key)).contains(value);

        cache.delete(key);
        assertThat(cache.getString(key)).isEmpty();
    }

    @Test
    void testPurgeOnExpiredAfter() throws InterruptedException {
        cache.set("key1", "value1", 3600);
        cache.set("key2", "value2", 3600);
        cache.set("key3", "value3", 1);
        cache.set("key4", "value4", 1);
        cache.set("key5", "value5", 1);

        assertThat(cache.getString("key1")).isPresent();
        assertThat(cache.getString("key2")).isPresent();
        assertThat(cache.getString("key3")).isPresent();
        assertThat(cache.getString("key4")).isPresent();
        assertThat(cache.getString("key5")).isPresent();

        Thread.sleep(TimeUnit.SECONDS.toMillis(1));
        cache.purge();

        assertThat(cache.getString("key1")).isPresent();
        assertThat(cache.getString("key2")).isPresent();
        assertThat(cache.getString("key3")).isEmpty();
        assertThat(cache.getString("key4")).isEmpty();
        assertThat(cache.getString("key5")).isEmpty();
    }

    @Test
    void testPurgeMedian() {
        int max = InMemoryCacheService.MAX_SIZE;
        IntStream.range(0, max)
                .forEach((i) -> cache.set("key" + i, "value" + i));
        assertThat(cache.getSize()).isEqualTo(max);
        cache.set("key" + max, "value" + max); // Adding this also forces a purge.
        assertThat(cache.getSize()).isLessThanOrEqualTo(max / 2); // The cache should now maximum be half full.
    }

}
