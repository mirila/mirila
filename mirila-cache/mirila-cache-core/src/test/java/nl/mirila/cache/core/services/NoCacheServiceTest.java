package nl.mirila.cache.core.services;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import nl.mirila.cache.core.Cache;
import nl.mirila.cache.core.CacheCoreModule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNoException;

public class NoCacheServiceTest {

    private Cache cache;

    @BeforeEach
    void setUp() {
        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                install(new CacheCoreModule());
            }
        });
        cache = injector.getInstance(Cache.class);
    }

    @Test
    void testNoCacheSet() {
        assertThatNoException().isThrownBy(() -> cache.set("key", "value"));
    }

    @Test
    void testNoCacheGet() {
        assertThat(cache.getString("key")).isNotPresent();
    }

    @Test
    void testNoCacheDelete() {
        assertThatNoException().isThrownBy(() -> cache.delete("key"));
    }

}
