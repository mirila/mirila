package nl.mirila.cache.core.services;

import com.google.inject.AbstractModule;
import nl.mirila.cache.core.Cache;

public class NoCacheModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(Cache.class).to(NoCacheService.class);
        bind(CacheService.class).to(NoCacheService.class);
    }

}
