package nl.mirila.cache.core;

import com.google.inject.Inject;
import nl.mirila.cache.core.services.CacheService;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Named;
import java.util.List;
import java.util.Optional;

/**
 * A {@link Cache} that adds a realm to the given keys, and uses a {@link CacheService} for the actual caching.
 */
public class RealmBasedCache implements Cache {

    private final CacheService service;

    /**
     * The realm.
     * <p>
     * We apply field injection, because we need realm to be an optional injection, but CacheService not.
     */
    @Inject(optional = true)
    @Named("realm")
    private String realm;

    /**
     * Initialize a new instance.
     */
    @Inject
    public RealmBasedCache(CacheService service) {
        this.service = service;
    }

    /**
     * Return a key that adds the realm as prefix, if available.
     * <p>
     * @param key The original key.
     * @return The key, potentially prefixed with the realm.
     */
    protected String prefixKey(String key) {
        return StringUtils.isBlank(realm) ? key : realm + ":" + key;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void set(String key, Object value, int expiresAfter) {
        service.set(prefixKey(key), value, expiresAfter);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T> Optional<T> get(String key, Class<T> valueClass) {
        return service.get(prefixKey(key), valueClass);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void delete(List<String> keys) {
        List<String> prefixedKeys = keys.stream()
                .map(this::prefixKey)
                .toList();
        service.delete(prefixedKeys);
    }

}
