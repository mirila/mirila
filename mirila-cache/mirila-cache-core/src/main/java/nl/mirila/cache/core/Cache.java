package nl.mirila.cache.core;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 * An interface for a cache.
 */
public interface Cache {

    /**
     * Caches the given string key with the given value for 24 hours.
     * <p>
     * @param key The key to cache.
     * @param value The value to cache.
     */
    default void set(String key, Object value) {
        set(key, value, (int) TimeUnit.DAYS.toSeconds(1));
    }

    /**
     * Caches the given string key with the given value.
     * <p>
     * @param key The key to cache.
     * @param value The value to cache.
     * @param expiresAfter The amount of seconds after which this key expires.
     */
    void set(String key, Object value, int expiresAfter);

    /**
     * Return the string value from an object in cache.
     * <p>
     * @param key The key.
     * @return The value, wrapped in a {@link Optional}.
     */
    default Optional<String> getString(String key) {
        return get(key, String.class)
                .stream()
                .findFirst();
    }

    /**
     * Return the value from cache, if available.
     * <p>
     * If the type of the value do not match, an empty {@link Optional} is returned.
     * <p>
     * @param key The key.
     * @param valueClass The class of the value.
     * @return The value, wrapped in a {@link Optional}.
     * @param <T> The type of the value to return.
     */
    <T> Optional<T> get(String key, Class<T> valueClass);

    /**
     * Delete the given key from the cache.
     * <p>
     * @param key The key.
     */
    default void delete(String key) {
        delete(Collections.singletonList(key));
    }

    /**
     * Delete the given keys from the cache.
     * <p>
     * @param keys The keys.
     */
    void delete(List<String> keys);

}
