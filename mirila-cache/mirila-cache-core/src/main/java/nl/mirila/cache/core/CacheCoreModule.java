package nl.mirila.cache.core;

import com.google.inject.AbstractModule;
import com.google.inject.multibindings.OptionalBinder;
import nl.mirila.cache.core.services.CacheService;
import nl.mirila.cache.core.services.NoCacheService;

public class CacheCoreModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(Cache.class).to(RealmBasedCache.class);

        OptionalBinder.newOptionalBinder(binder(), CacheService.class)
                .setDefault()
                .to(NoCacheService.class);
    }

}
