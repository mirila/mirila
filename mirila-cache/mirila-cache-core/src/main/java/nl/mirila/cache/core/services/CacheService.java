package nl.mirila.cache.core.services;

import nl.mirila.cache.core.Cache;

/**
 * An interface for a cache service, which actually performs the caching and fetching.
 * <p>
 * Use this interface for implementation. To use cache, use {@link Cache} instead.
 */
public interface CacheService extends Cache {

}
