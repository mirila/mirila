package nl.mirila.cache.core.services;

import javax.inject.Singleton;
import java.util.List;
import java.util.Optional;

/**
 * A {@link CacheService} that actually doesn't cache anything. This may be useful for situations in which caching
 * is not wanted, but in which the interface is needed, like testing.
 */
@Singleton
public class NoCacheService implements CacheService {

    /**
     * {@inheritDoc}
     */
    @Override
    public void set(String key, Object value, int expiresAfter) {
        // no-op
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T> Optional<T> get(String key, Class<T> valueClass) {
        return Optional.empty();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void delete(List<String> keys) {
        // no-op
    }

}
