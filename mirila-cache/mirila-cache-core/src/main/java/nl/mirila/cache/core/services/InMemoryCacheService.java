package nl.mirila.cache.core.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * A simple cache service, using limited application memory. This is useful for small applications or for
 * testing purposes.
 */
public class InMemoryCacheService implements CacheService {

    private static final Logger logger = LogManager.getLogger(InMemoryCacheService.class);
    private static final HashMap<String, InMemoryCacheItem> cache = new HashMap<>();

    protected static final int MAX_SIZE = 5_000;

    /**
     * Return the number of items in the cache.
     */
    protected int getSize() {
        return cache.size();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void set(String key, Object value, int timeToLive) {
        if (value == null) {
            logger.warn("Invalid value null for key {}. Ignoring this value.", key);
            return;
        }
        cache.put(key, new InMemoryCacheItem(value, timeToLive));
        if (cache.size() > MAX_SIZE) {
            purge();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T> Optional<T> get(String key, Class<T> valueClass) {
        if (!cache.containsKey(key)) {
            return Optional.empty();
        }
        InMemoryCacheItem item = cache.get(key);
        if (item.getExpiresAt() < System.currentTimeMillis()) {
            // The item is expired, so it should be deleted.
            cache.remove(key);
            return Optional.empty();
        }
        Object value = item.getValue();
        return (valueClass.isInstance(value))
                ? Optional.of(valueClass.cast(value))
                : Optional.empty();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void delete(List<String> keys) {
        keys.forEach(cache::remove);
    }

    /**
     * Purge expired items from the cache. If that doesn't free up enough space, remove to the oldest half of the cache
     * based on the expires date of the median item.
     */
    public void purge() {
        long now = System.currentTimeMillis();
        List<String> expiredKeys = getKeysBeforeDate(now);
        expiredKeys.forEach(cache::remove);
        logger.debug("Purged {} expired keys from the cache.", expiredKeys.size());

        if (cache.size() > MAX_SIZE) {
            long expiresAfterOfMedian = cache.values().stream()
                    .map(InMemoryCacheItem::getExpiresAt)
                    .sorted()
                    .skip(MAX_SIZE / 2)
                    .findFirst()
                    .orElse(0L);
            List<String> keysBeforeMedian = getKeysBeforeDate(expiresAfterOfMedian);
            keysBeforeMedian.forEach(cache::remove);
            logger.debug("Additionally purged all keys before {}.", expiresAfterOfMedian);
        }
    }

    /**
     * Return all cache keys that expired before the given timestamp.
     * <p>
     * @param timestamp The timestamp.
     * @return A list of keys.
     */
    private List<String> getKeysBeforeDate(long timestamp) {
        return cache.entrySet().stream()
                .filter((entry) -> entry.getValue().getExpiresAt() <= timestamp)
                .map(Map.Entry::getKey)
                .toList();
    }

}
