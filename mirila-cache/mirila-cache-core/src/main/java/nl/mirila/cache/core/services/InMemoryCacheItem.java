package nl.mirila.cache.core.services;

import javax.validation.constraints.NotNull;

/**
 * Represents a single item within the {@link InMemoryCacheService}.
 */
class InMemoryCacheItem {

    private final Object value;
    private final long expiresAt;

    /**
     * Initialize a new instance.
     * <p>
     * @param value The value.
     * @param expiresAfter The amount of seconds after which this item expires.
     */
    public InMemoryCacheItem(@NotNull Object value, long expiresAfter) {
        this.value = value;
        this.expiresAt = System.currentTimeMillis() + (expiresAfter * 1000);
    }

    /**
     * Return the value of a cached item.
     */
    @NotNull
    public Object getValue() {
        return value;
    }

    /**
     * Return from which epoch this cache expires.
     */
    public long getExpiresAt() {
        return expiresAt;
    }

}
