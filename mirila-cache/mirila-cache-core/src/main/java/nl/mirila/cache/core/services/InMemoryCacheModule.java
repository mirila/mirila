package nl.mirila.cache.core.services;

import com.google.inject.AbstractModule;
import com.google.inject.multibindings.OptionalBinder;
import nl.mirila.cache.core.CacheCoreModule;

public class InMemoryCacheModule extends AbstractModule {

    @Override
    protected void configure() {
        install(new CacheCoreModule());

        OptionalBinder.newOptionalBinder(binder(), CacheService.class)
                .setBinding()
                .to(InMemoryCacheService.class);
    }

}
