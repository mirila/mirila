package nl.mirila.cache.redis;

import com.google.inject.AbstractModule;
import com.google.inject.multibindings.OptionalBinder;
import nl.mirila.cache.core.CacheCoreModule;
import nl.mirila.cache.core.services.CacheService;

public class RedisCacheModule extends AbstractModule {

    @Override
    protected void configure() {
        install(new CacheCoreModule());

        OptionalBinder.newOptionalBinder(binder(), CacheService.class)
                .setBinding()
                .to(RedisCacheService.class);
    }

}
