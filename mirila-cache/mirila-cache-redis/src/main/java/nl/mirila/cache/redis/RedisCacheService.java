package nl.mirila.cache.redis;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import nl.mirila.cache.core.services.CacheService;
import nl.mirila.drivers.redis.RedisDriverService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Optional;

/**
 * A {@link CacheService} using a Redis Server.
 */
public class RedisCacheService implements CacheService {

    private static final Logger logger = LogManager.getLogger(RedisCacheService.class);

    private final ObjectMapper mapper;
    private final RedisDriverService service;

    /**
     * Initialize a new instance.
     */
    @Inject
    public RedisCacheService(RedisDriverService service, ObjectMapper mapper) {
        this.mapper = mapper;
        this.service = service;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void set(String key, Object value, int timeToLive) {
        if ((key == null) || (value == null)) {
            return;
        }
        String strValue;
        if (value instanceof String) {
            strValue = value.toString();
        } else {
            try {
                strValue = mapper.writeValueAsString(value);
            } catch (JsonProcessingException e) {
                logger.warn(e.getMessage(), e);
                return;
            }
        }
        service.set(key, strValue, timeToLive);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T> Optional<T> get(String key, Class<T> valueClass) {
        Optional<String> optional = service.get(key);
        if (optional.isEmpty()) {
            return Optional.empty();
        }

        String value = optional.get();
        if (valueClass.equals(String.class)) {
            return Optional.of(valueClass.cast(value));
        }

        try {
            return Optional.of(mapper.readValue(value, valueClass));
        } catch (JsonProcessingException e) {
            String msg = "Error while parsing key '{}' from Redis cache: {}.\n{}";
            logger.warn(msg, key, e.getMessage(), value);
            return Optional.empty();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void delete(List<String> keys) {
        service.del(keys);
    }

}
