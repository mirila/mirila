package nl.mirila.cache.redis;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import nl.mirila.cache.core.Cache;
import nl.mirila.core.settings.modules.KeyValuesSettingsModule;
import nl.mirila.model.core.jackson.IdType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class RedisCacheServiceIT {

    private Cache cache;

    @BeforeEach
    void setUp() {
        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                install(new KeyValuesSettingsModule(Collections.emptyMap()));
                bind(IdType.class).toInstance(IdType.STRING);
                install(new RedisCacheModule());
            }
        });
        cache = injector.getInstance(Cache.class);
    }

    @Test
    void testCacheAndGet() {
        cache.set("key1", "value1");
        cache.set("key2", "value2");

        assertTrue(cache.getString("key1").isPresent());
        assertTrue(cache.getString("key2").isPresent());
        assertTrue(cache.getString("key3").isEmpty());
        assertTrue(cache.getString("key4").isEmpty());

        assertEquals("value1", cache.getString("key1").orElse(""));
        assertEquals("value2", cache.getString("key2").orElse(""));
    }

    @Test
    void testDelete() {
        String key = "key.delete";
        String value = "value";

        cache.set(key, value);
        assertTrue(cache.getString(key).isPresent());
        assertEquals(value, cache.getString(key).orElse(""));

        cache.delete(key);
        assertTrue(cache.getString(key).isEmpty());
    }

}
