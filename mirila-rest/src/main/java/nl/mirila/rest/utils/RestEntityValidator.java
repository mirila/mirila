package nl.mirila.rest.utils;

import nl.mirila.model.core.references.Model;
import nl.mirila.model.management.results.Result;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.core.Response;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Validator for REST entities.
 */
public final class RestEntityValidator {

    /**
     * This utility class may not be instantiated.
     */
    private RestEntityValidator() {
        throw new UnsupportedOperationException();
    }

    /**
     * Validate the value that is possible in the given result, and compare it with the expected value.
     * <p>
     * If the result is empty, which is allowed, further validation is skipped.
     * <p>
     * If these values do not match a {@link ClientErrorException} will be thrown.
     * <p>
     * @param result The result that may contain the actual value.
     * @param mapper The mapper that can be used to fetch actual value.
     * @param expectedValue The expected value.
     * @param fieldName The name of the field.
     * @param <E> The type of the entity.
     * @param <T> The type of the expected value.
     */
    public static <E extends Model, T> void validateValue(String fieldName,
                                                          Result<E> result,
                                                          Function<E, T> mapper,
                                                          T expectedValue) {
        result.ifPresent((entity) -> validateValue(fieldName, mapper.apply(entity), expectedValue));
    }

    /**
     * Validate the value via the given {@link Supplier}, and compare it with the expected value. If these values
     * do not match a {@link ClientErrorException} will be thrown.
     * <p>
     * @param fieldName The name of the field.
     * @param supplier The supplier that can be used to fetch actual value.
     * @param expectedValue The expected value.
     * @param <T> The type of the expected value.
     */
    public static <T> void validateValue(String fieldName, Supplier<T> supplier, T expectedValue) {
        validateValue(fieldName, supplier.get(), expectedValue);
    }

    /**
     * Validate the expected and actual value for the given field name. If the objects are not equal a
     * {@link ClientErrorException} will be thrown.
     * <p>
     * @param fieldName The name of the field.
     * @param expected The expected value for the field.
     * @param actual The actual value for the field.
     * @param <E> The type of the expected and actual value.
     */
    public static <E> void validateValue(String fieldName, E expected, E actual) {
        if (!Objects.equals(expected, actual)) {
            String template = "There's a mismatch in the expected and actual value for field %s.";
            String message = String.format(template, fieldName);
            throw new ClientErrorException(message, Response.Status.CONFLICT);
        }
    }

    /**
     * Validate the expected and actual value for the given field name. If the actual value is null, the setter will
     * be called which should be used to set the expected value onto the actual value. Otherwise, if the objects are not
     * equal a {@link ClientErrorException} will be thrown.
     * <p>
     * @param fieldName The name of the field.
     * @param expected The expected value for the field.
     * @param actual The actual value for the field.
     * @param actualSetter The setter for the actual value.
     * @param <E> The type of the expected and actual value.
     */
    public static <E> void validateValueOrSetIfNull(String fieldName, E expected, E actual, Consumer<E> actualSetter) {
        if ((expected != null) && (actual == null)) {
            actualSetter.accept(expected);
        } else {
            validateValue(fieldName, expected, actual);
        }
    }

}
