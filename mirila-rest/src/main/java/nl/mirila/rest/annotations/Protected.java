package nl.mirila.rest.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation can be used on resource methods, to indicate that only authenticated actors may access the resource.
 * Using the roles, this can even be narrowed to specific groups.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Protected {

    /**
     * Return the roles which are allowed to call a certain resources method.
     */
    String[] roles() default {};

    /**
     * Return a list of {@link ClaimCheck} which are must be checked when calling a certain resources method.
     */
    ClaimCheck[] claimChecks() default {};

}
