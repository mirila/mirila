package nl.mirila.rest.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This nested annotation can be used to indicate that a specific claim should match a specific value, or a value in a
 * specific query parameter.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.ANNOTATION_TYPE)
public @interface ClaimCheck {

    /**
     * Return the key of the claim to check.
     */
    String claimKey();


    /**
     * Return the expected value for the claim.
     * <p>
     * Use either this queryParamName or the claimValue.
     */
    String claimValue() default "";

    /**
     * Return the name of the query parameter, that contains the expected value for the claim.
     * <p>
     * Use either this queryParamName or the claimValue.
     */
    String queryParamName() default "";

}
