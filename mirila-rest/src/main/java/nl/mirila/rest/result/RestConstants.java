package nl.mirila.rest.result;

/**
 * Constants that are useful within REST resources.
 */
public class RestConstants {

    public static final String JSON_WITH_UTF8 = "application/json; charset=utf-8";

    /**
     * This utility class may not be instantiated.
     */
    private RestConstants() {
        throw new UnsupportedOperationException();
    }

}
