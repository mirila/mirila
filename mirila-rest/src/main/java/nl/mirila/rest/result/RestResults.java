package nl.mirila.rest.result;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import nl.mirila.model.core.references.Model;
import nl.mirila.model.management.results.Results;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Represent the list of results for the Rest API.
 * <p>
 * @param <E> The type of the entities that is included in this RestResult.
 */
public class RestResults<E> implements Iterable<E> {

    @JsonProperty("links")
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private final List<E> links;

    @JsonProperty("data")
    private final List<E> data;

    @JsonIgnore
    private final int statusCode;

    @JsonIgnore
    private final String statusMessage;

    /**
     * Initialize an empty instance without objects.
     */
    private RestResults() {
        this.statusCode = 200;
        this.statusMessage = "";
        this.links = new ArrayList<>();
        this.data = new ArrayList<>();
    }

    /**
     * Initialize an empty instance without objects.
     */
    private RestResults(int statusCode, String statusMessage) {
        this.statusCode = statusCode;
        this.statusMessage = statusMessage;
        links = new ArrayList<>();
        data = new ArrayList<>();
    }

    /**
     * Initialize a new instance with the given entities.
     */
    private RestResults(List<E> entities) {
        this.statusCode = 200;
        this.statusMessage = "";
        this.data = entities;
        links = new ArrayList<>();
    }


    /**
     * Return the list of additional links.
     */
    public List<E> getLinks() {
        return links;
    }

    /**
     * Return the data.
     */
    public List<E> getData() {
        return data;
    }

    /**
     * Return the http status code.
     */
    public int getStatusCode() {
        return statusCode;
    }

    /**
     * Return the status message.
     */
    public String getStatusMessage() {
        return statusMessage;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Iterator<E> iterator() {
        return data.iterator();
    }

    /**
     * Create a new {@link RestResults} wrapper with the given entities.
     * <p>
     * @param entities The entities.
     * @param <E> The type of the entities.
     * @return The results wrapper.
     */
    public static <E> RestResults<E> with(List<E> entities) {
        return new RestResults<>(entities);
    }

    /**
     * Create a new {@link RestResults} wrapper with the given entities in the {@link Results} wrapper.
     * <p>
     * @param results The entities in the {@link Results} wrapper.
     * @param <M> The type of the entities.
     * @return The results wrapper.
     */
    public static <M extends Model> RestResults<M> with(Results<M> results) {
        return (results.isEmpty())
                ? empty()
                : with(results.get());
    }

    /**
     * Create an empty {@link RestResults} wrapper.
     * <p>
     * @param <E> The type of the entities, if there were results.
     * @return The results wrapper, without entities.
     */
    public static <E> RestResults<E> empty() {
        return new RestResults<>();
    }

    /**
     * Create an empty {@link RestResults} wrapper, with the given http status code.
     * <p>
     * @param statusCode The http status code.
     * @return The results wrapper, without entities.
     * @param <E> The type of the entities, if there were results.
     */
    public static <E> RestResults<E> withStatus(int statusCode) {
        return new RestResults<>(statusCode, "");
    }

    /**
     * Create an empty {@link RestResults} wrapper, with the given http status code and status message.
     * <p>
     * @param statusCode The http status code.
     * @return The results wrapper, without entities.
     * @param <E> The type of the entities, if there were results.
     */
    public static <E> RestResults<E> withStatus(int statusCode, String statusMessage) {
        return new RestResults<>(statusCode, statusMessage);
    }

}
