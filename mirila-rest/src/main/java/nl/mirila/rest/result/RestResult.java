package nl.mirila.rest.result;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import nl.mirila.model.core.references.Model;
import nl.mirila.model.management.results.Result;

import java.util.Optional;

import static javax.ws.rs.core.Response.Status.ACCEPTED;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;

/**
 * Represent the result for the Rest API.
 * <p>
 * @param <E> The type of the entity that is included in this RestResult.
 */
public class RestResult<E> {

    @JsonProperty("data")
    private final E entity;

    @JsonIgnore
    private final int statusCode;

    @JsonIgnore
    private final String statusMessage;

    /**
     * Initialize a new instance without an entity.
     */
    private RestResult() {
        this(NOT_FOUND.getStatusCode(), "Not Found");
    }

    /**
     * Initialize a new instance with the given entity.
     */
    private RestResult(E entity) {
        this.entity = entity;
        this.statusCode = ACCEPTED.getStatusCode();
        this.statusMessage = "";
    }

    /**
     * Initialize a new instance without an entity, but with the given status values.
     */
    private RestResult(int statusCode, String statusMessage) {
        entity = null;
        this.statusCode = statusCode;
        this.statusMessage = statusMessage;
    }

    /**
     * Return the entity, wrapped in an {@link Optional}.
     */
    public Optional<Object> get() {
        return Optional.ofNullable(entity);
    }

    /**
     * Return the http status code.
     */
    public int getStatusCode() {
        return statusCode;
    }

    /**
     * Return the status message.
     */
    public String getStatusMessage() {
        return statusMessage;
    }

    /**
     * Create a new {@link RestResult} with the given object.
     */
    public static <E> RestResult<E> with(E object) {
        return new RestResult<>(object);
    }

    /**
     * Create a new {@link RestResult} with the entity in the given {@link Result}. If that result is empty, the
     * {@link RestResult} will also be empty.
     */
    public static<M extends Model> RestResult<M> with(Result<M> result) {
        return (result.isEmpty())
                ? empty()
                : with(result.get());
    }

    /**
     * Create an empty {@link RestResult}.
     */
    public static <E> RestResult<E> empty() {
        return new RestResult<>();
    }

    /**
     * Create an empty {@link RestResult}, with the given status code.
     */
    public static <E> RestResult<E> withStatus(int statusCode) {
        return new RestResult<>(statusCode, "");
    }

    /**
     * Create an empty {@link RestResult}, with the given status code and status message.
     */
    public static <E> RestResult<E> withStatus(int statusCode, String statusMessage) {
        return new RestResult<>(statusCode, statusMessage);
    }

}
