package nl.mirila.rest;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.google.inject.AbstractModule;
import nl.mirila.model.management.module.ModelManagementModule;
import nl.mirila.rest.filters.*;
import nl.mirila.rest.mapper.ResteasyJacksonProvider;
import org.jboss.resteasy.plugins.guice.ext.RequestScopeModule;

import javax.inject.Singleton;

public class RestModule extends AbstractModule {

    @Override
    @SuppressWarnings("PointlessBinding") // Some bindings are required, and picked up by Guice using reflection.
    protected void configure() {
        install(new RequestScopeModule());
        install(new ModelManagementModule());

        bind(JacksonJsonProvider.class).to(ResteasyJacksonProvider.class).in(Singleton.class);

        bind(ExceptionFilter.class);
        bind(MirilaExceptionFilter.class);
        bind(ClientErrorExceptionFilter.class);
        bind(PersistenceConnectionFilter.class);

        bind(RestResultFilter.class);
        bind(RestResultsFilter.class);
    }

}
