package nl.mirila.rest;

import com.google.inject.Inject;
import nl.mirila.core.settings.Settings;

/**
 * Settings for the Rest API.
 */
public class RestSettings {

    public static final String KEY_USE_JSON_API = "rest.json.api";
    public static final String KEY_EXPOSE_SERVER_ERRORS = "app.expose-server-errors";

    private static final boolean DEFAULT_USE_JSON_API = false;
    private static final boolean DEFAULT_EXPOSE_SERVER_ERRORS = false;

    private final Settings settings;

    /**
     * Initialize a new instance.
     */
    @Inject
    public RestSettings(Settings settings) {
        this.settings = settings;
    }

    /**
     * Return true if the Rest API uses the JSON:API protocol.
     * <p>
     * Setting: {@value KEY_USE_JSON_API}. Default: {@value DEFAULT_USE_JSON_API}.
     */
    public boolean usesJsonApi() {
        return settings.getBoolean(KEY_USE_JSON_API).orElse(DEFAULT_USE_JSON_API);
    }

    /**
     * Return true if the Rest API should expose the exception message on internal server errors.
     * <p>
     * Setting: {@value KEY_EXPOSE_SERVER_ERRORS}. Default: {@value DEFAULT_EXPOSE_SERVER_ERRORS}.
     */
    public boolean exposeServerErrors() {
        return settings.getBoolean(KEY_EXPOSE_SERVER_ERRORS).orElse(DEFAULT_EXPOSE_SERVER_ERRORS);
    }

}
