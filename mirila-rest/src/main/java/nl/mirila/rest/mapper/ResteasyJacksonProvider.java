package nl.mirila.rest.mapper;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import com.google.inject.Inject;
import nl.mirila.model.core.jackson.ObjectMapperProvider;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.ext.Provider;


/**
 * A provider that provides {@link ObjectMapper}, configured with Mirila related (de)serializers.
 */
@Provider
@Consumes({"application/json", "application/*+json", "text/json"})
@Produces({"application/json", "application/*+json", "text/json"})
public class ResteasyJacksonProvider extends JacksonJaxbJsonProvider {

    /**
     * Initialize a new instance.
     */
    @Inject
    public ResteasyJacksonProvider(ObjectMapperProvider provider) {
        super(provider.get(), DEFAULT_ANNOTATIONS);
    }

}
