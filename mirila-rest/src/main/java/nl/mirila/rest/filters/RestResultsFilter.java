package nl.mirila.rest.filters;

import com.google.inject.Inject;
import nl.mirila.rest.RestSettings;
import nl.mirila.rest.result.RestResults;

import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;

/**
 * A filter that returns the content of a {@link RestResults}.
 */
@Provider
@Priority(Priorities.ENTITY_CODER)
public class RestResultsFilter implements ContainerResponseFilter {

    private final RestSettings settings;

    /**
     * Initialize a new instance.
     */
    @Inject
    public RestResultsFilter(RestSettings settings) {
        this.settings = settings;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void filter(ContainerRequestContext requestContext,
                       ContainerResponseContext responseContext) {
        if ((responseContext.hasEntity()) && (responseContext.getEntity() instanceof RestResults<?> restResults)) {
            responseContext.setStatus(restResults.getStatusCode());

            Object resultingObject = (settings.usesJsonApi()) ? restResults : restResults.getData();
            responseContext.setEntity(resultingObject);
        }
    }

}
