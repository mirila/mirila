package nl.mirila.rest.filters;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * The ClientErrorExceptionFilter logs the ClientErrorException and returns a plain text response.
 */
@Provider
public class ClientErrorExceptionFilter implements ExceptionMapper<ClientErrorException> {

    private static final Logger logger = LogManager.getLogger(ClientErrorExceptionFilter.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public Response toResponse(ClientErrorException exception) {
        logger.warn(exception.getMessage());
        return Response.status(exception.getResponse().getStatus())
                .type(MediaType.TEXT_PLAIN_TYPE)
                .entity(exception.getMessage())
                .build();
    }

}
