package nl.mirila.rest.filters;

import com.google.inject.Inject;
import nl.mirila.rest.RestSettings;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * The ExceptionFilter logs the exception and sends a response. Depending on the application settings, the message
 * of the exception may be exposed in the response.
 */
@Provider
public class ExceptionFilter implements ExceptionMapper<Exception> {

    private static final Logger logger = LogManager.getLogger(ExceptionFilter.class);
    private final RestSettings settings;

    /**
     * Initialize a new instance.
     */
    @Inject
    public ExceptionFilter(RestSettings settings) {
        this.settings = settings;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Response toResponse(Exception exception) {
        logger.error(exception.getMessage(), exception);
        String message = (settings.exposeServerErrors())
                ? exception.getMessage()
                : "An unexpected error occurred on our server. Please, contact the system administrators.";
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .type(MediaType.TEXT_PLAIN_TYPE)
                .entity(message)
                .build();
    }

}
