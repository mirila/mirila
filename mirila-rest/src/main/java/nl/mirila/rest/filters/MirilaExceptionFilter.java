package nl.mirila.rest.filters;

import nl.mirila.core.exceptions.MirilaException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * A filter that outputs a response based on a {@link MirilaException}.
 */
@Provider
public class MirilaExceptionFilter implements ExceptionMapper<MirilaException> {

    private static final Logger logger = LogManager.getLogger(MirilaExceptionFilter.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public Response toResponse(MirilaException exception) {
        if (exception.getCode() < 500) {
            logger.error(exception.getMessage());
        } else {
            logger.error(exception.getMessage(), exception);
        }
        return Response.status(exception.getCode())
                .type(MediaType.TEXT_PLAIN_TYPE)
                .entity(exception.getMessage())
                .build();
    }

}
