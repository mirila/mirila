package nl.mirila.rest.filters;

import com.google.inject.Inject;
import nl.mirila.rest.RestSettings;
import nl.mirila.rest.result.RestResult;

import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;

/**
 * A filter that returns the content of a {@link RestResult}.
 */
@Provider
@Priority(Priorities.ENTITY_CODER)
public class RestResultFilter implements ContainerResponseFilter {

    private final RestSettings settings;

    /**
     * Initialize a new instance.
     */
    @Inject
    public RestResultFilter(RestSettings settings) {
        this.settings = settings;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) {
        if ((responseContext.hasEntity()) && (responseContext.getEntity() instanceof RestResult<?> restResult)) {
            responseContext.setStatus(restResult.getStatusCode());

            Object resultingObject = (settings.usesJsonApi()) ? restResult : restResult.get().orElse(null);
            responseContext.setEntity(resultingObject);
        }
    }

}
