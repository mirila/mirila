package nl.mirila.rest.filters;

import com.google.inject.Inject;
import com.google.inject.Injector;
import nl.mirila.model.management.managers.EntityPersistenceManager;

import javax.annotation.Priority;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;

/**
 * The {@link PersistenceConnectionFilter} closes the connection the {@link EntityPersistenceManager} may have used.
 */
@Provider
@Priority(100)
public class PersistenceConnectionFilter implements ContainerResponseFilter {

    private final Injector injector;

    /**
     * Initialize a new instance.
     */
    @Inject
    public PersistenceConnectionFilter(Injector injector) {
        this.injector = injector;
    }

    /**
     * Close the connection after the request is processed.
     * <p>
     * @param requestContext The request context.
     * @param responseContext The response context.
     */
    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) {
        EntityPersistenceManager manager = injector.getInstance(EntityPersistenceManager.class);
        manager.close();
    }

}
