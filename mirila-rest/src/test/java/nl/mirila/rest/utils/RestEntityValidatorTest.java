package nl.mirila.rest.utils;

import org.junit.jupiter.api.Test;

import javax.ws.rs.ClientErrorException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

class RestEntityValidatorTest {

    @Test
    void testValidateIds() {
        assertDoesNotThrow(() -> {
            RestEntityValidator.validateValue("test", "abc", "abc");
        });

        assertThrows(ClientErrorException.class, () -> {
            RestEntityValidator.validateValue("test", "abc", "cba");
        });
    }

    @Test
    void testTestValidateIds() {
        assertDoesNotThrow(() -> {
            RestEntityValidator.validateValueOrSetIfNull("test", "abc", "abc", (newId) -> {});
            RestEntityValidator.validateValueOrSetIfNull("test", "abc", null, (newId) -> {
                assertThat(newId).isEqualTo("abc");
            });
        });

        assertThrows(ClientErrorException.class, () -> {
            RestEntityValidator.validateValueOrSetIfNull("test", "abc", "cba", (newId) -> {});
        });
    }

}
