package nl.mirila.rest.result;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import nl.mirila.core.datatype.Id;
import nl.mirila.model.core.jackson.IdType;
import nl.mirila.model.core.jackson.ObjectMapperProvider;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class RestResultTest {

    @Test
    void testWith() {
        RestResult<String> result = RestResult.with("Hello world!");
        assertThat(result).isNotNull();
        assertThat(result.get()).contains("Hello world!");
    }

    @Test
    void testEmpty() {
        RestResult<String> result = RestResult.empty();
        assertThat(result).isNotNull();
        assertThat(result.get()).isEmpty();
    }

    @Test
    void testWithStatus() {
        RestResult<String> result1 = RestResult.withStatus(123);
        assertThat(result1).isNotNull();
        assertThat(result1.getStatusCode()).isEqualTo(123);
        assertThat(result1.getStatusMessage()).isEmpty();

        RestResult<String> result2 = RestResult.withStatus(234, "Hello world!");
        assertThat(result2).isNotNull();
        assertThat(result2.getStatusCode()).isEqualTo(234);
        assertThat(result2.getStatusMessage()).isEqualTo("Hello world!");
    }

    @Test
    void testMapper() throws JsonProcessingException {
        ObjectMapperProvider provider = new ObjectMapperProvider(new IdType.IdTypeHolder(IdType.INTEGER));
        ObjectMapper mapper = provider.get();

        RestResult<String> emptyResult = RestResult.empty();
        String emptyJson = mapper.writeValueAsString(emptyResult);
        assertThat(emptyJson).isEqualTo("{\"data\":null}");

        RestResult<String> simpleResult = RestResult.with("Hello world!");
        String simpleJson = mapper.writeValueAsString(simpleResult);
        assertThat(simpleJson).isEqualTo("{\"data\":\"Hello world!\"}");

        TestUser user = new TestUser(Id.of(1), "obiwan", "Obi-Wan", "Kenobi", 123);
        RestResult<TestUser> userResult = RestResult.with(user);
        String userJon = mapper.writeValueAsString(userResult);
        assertThat(userJon).isEqualTo("{\"data\":{\"userId\":1,\"username\":\"obiwan\"," +
                                              "\"firstName\":\"Obi-Wan\",\"lastName\":\"Kenobi\",\"age\":123}}");
    }

}
