package nl.mirila.rest.result;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import nl.mirila.core.datatype.Id;
import nl.mirila.model.core.jackson.IdType;
import nl.mirila.model.core.jackson.ObjectMapperProvider;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

class RestResultsTest {

    @Test
    void testWith() {
        RestResults<Integer> results = RestResults.with(Arrays.asList(1, 2, 3));
        assertThat(results).isNotNull().hasSize(3);
        assertThat(results.getData()).containsOnly(1, 2, 3);
    }

    @Test
    void testEmpty() {
        RestResults<Integer> results = RestResults.empty();
        assertThat(results).isNotNull().isEmpty();
    }

    @Test
    void testWithStatus() {
        RestResults<Integer> results1 = RestResults.withStatus(123);
        assertThat(results1).isNotNull();
        assertThat(results1.getData()).isEmpty();
        assertThat(results1.getStatusCode()).isEqualTo(123);
        assertThat(results1.getStatusMessage()).isEmpty();

        RestResults<Integer> results2 = RestResults.withStatus(234, "Hello world!");
        assertThat(results2).isNotNull();
        assertThat(results2.getData()).isEmpty();
        assertThat(results2.getStatusCode()).isEqualTo(234);
        assertThat(results2.getStatusMessage()).isEqualTo("Hello world!");
    }

    @Test
    void testMapper() throws JsonProcessingException {
        ObjectMapperProvider provider = new ObjectMapperProvider(new IdType.IdTypeHolder(IdType.INTEGER));
        ObjectMapper mapper = provider.get();

        RestResults<TestUser> emptyResult = RestResults.empty();
        String emptyJson = mapper.writeValueAsString(emptyResult);
        assertThat(emptyJson).isEqualTo("{\"data\":[]}");

        TestUser user1 = new TestUser(Id.of(1), "obiwan", "Obi-Wan", "Kenobi", 123);
        TestUser user2 = new TestUser(Id.of(2), "luke", "Luke", "Skywalker", 56);

        RestResults<TestUser> results = RestResults.with(Arrays.asList(user1, user2));
        String json = mapper.writeValueAsString(results);

        assertThat(json).isEqualTo("{\"data\":[" +
            "{\"userId\":1,\"username\":\"obiwan\",\"firstName\":\"Obi-Wan\",\"lastName\":\"Kenobi\",\"age\":123}," +
            "{\"userId\":2,\"username\":\"luke\",\"firstName\":\"Luke\",\"lastName\":\"Skywalker\",\"age\":56}]}");
    }

}
