package nl.mirila.files.core;

import nl.mirila.core.datatype.Id;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Optional;

/**
 * The FileManager interface provides methods to work with files, regardless of the actual storage.
 */
public interface FileManager {

    /**
     * Create a file with the given file name, using the given container id. The temporary contains the file data.
     * <p>
     * @param containerId The id of the container.
     * @param fileName The name of the file.
     * @param temporaryFile The file that contains the file data.
     */
    void create(Id containerId, String fileName, File temporaryFile);

    /**
     * Create a file with the given file name, using the given container id. The {@link OutputStream} contains the
     * file data.
     * <p>
     * @param containerId The id of the container.
     * @param fileName The name of the file.
     * @param inputStream The input stream.
     */
    void create(Id containerId, String fileName, InputStream inputStream);

    /**
     * Read the file with the given file name, to be found in the given container id, as an {@link InputStream}.
     * <p>
     * @param containerId The id of the container.
     * @param fileName The name of the file.
     * @return The input stream.
     */
    Optional<InputStream> read(Id containerId, String fileName);

    /**
     * Update the file with the given file name, using the given container id. The temporary contains the file data.
     * <p>
     * @param containerId The id of the container.
     * @param fileName The name of the file.
     * @param temporaryFile The file that contains the file data.
     */
    void update(Id containerId, String fileName, File temporaryFile);


    /**
     * Update the file with the given file name, using the given container id. The {@link OutputStream} the file data.
     * <p>
     * @param containerId The id of the container.
     * @param fileName The name of the file.
     * @param inputStream The input stream.
     */
    void update(Id containerId, String fileName, InputStream inputStream);

    /**
     * Delete the file with the given file name, to be found in the given container id.
     * <p>
     * @param containerId The id of the container.
     * @param fileName The name of the file.
     */
    void delete(Id containerId, String fileName);

}
