package nl.mirila.files.local;

import com.google.inject.Inject;
import nl.mirila.core.datatype.Id;
import nl.mirila.core.exceptions.MirilaException;
import nl.mirila.files.core.FileManager;
import org.apache.commons.lang3.StringUtils;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Optional;

/**
 * The LocalFileManager manages files on the local file system.
 */
public class LocalFileManager implements FileManager {

    private final LocalFileManagerSettings settings;

    /**
     * Initialize a new instance.
     */
    @Inject
    public LocalFileManager(LocalFileManagerSettings settings) {
        this.settings = settings;
    }

    /**
     * Return the real path of the file on the local file system with the given container id and file name.
     * <p>
     * @param containerId The id of the container.
     * @param fileName The name of the file.
     * @return The real path of the file.
     */
    private Path getRealFilePath(Id containerId, String fileName) {
        if (StringUtils.isBlank(fileName)) {
            throw new MirilaException("Filename may not be blank.");
        }
        String basePath;
        String path;
        try {
            basePath = Path.of(settings.getBasePath()).toFile().getCanonicalPath();
            path = Path.of(basePath, containerId.asString(), fileName).toFile().getCanonicalPath();
        } catch (IOException e) {
            throw new MirilaException(e.getMessage());
        }

        if (!path.startsWith(basePath)) {
            throw new PathViolationException(Path.of(basePath), Path.of(path));
        }
        return Path.of(path);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void create(Id containerId, String fileName, File temporaryFile) {
        update(containerId, fileName, temporaryFile);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void create(Id containerId, String fileName, InputStream inputStream) {
        update(containerId, fileName, inputStream);
    }

    /**
     * {@inheritDoc}
     * @return
     */
    @Override
    public Optional<InputStream> read(Id containerId, String fileName) {
        Path filePath = getRealFilePath(containerId, fileName);
        if (!Files.exists(filePath)) {
            throw new MirilaException(String.format("Filename %s does not exist.", fileName));
        }
        try {
            return Optional.of(new FileInputStream(filePath.toString()));
        } catch (FileNotFoundException e) {
            return Optional.empty();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void update(Id containerId, String fileName, File temporaryFile) {
        Path filePath = getRealFilePath(containerId, fileName);
        try {
            if (!Files.isDirectory(filePath.getParent())) {
                Files.createDirectories(filePath.getParent());
            }
            Files.copy(temporaryFile.toPath(), filePath, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new MirilaException(e.getMessage());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void update(Id containerId, String fileName, InputStream inputStream) {
        Path filePath = getRealFilePath(containerId, fileName);
        try {
            if (!Files.isDirectory(filePath.getParent())) {
                Files.createDirectories(filePath.getParent());
            }
            Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new MirilaException(e.getMessage());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void delete(Id containerId, String fileName) {
        Path filePath = getRealFilePath(containerId, fileName);
        try {
            Files.deleteIfExists(filePath);
        } catch (IOException e) {
            throw new MirilaException(e.getMessage());
        }
    }

}
