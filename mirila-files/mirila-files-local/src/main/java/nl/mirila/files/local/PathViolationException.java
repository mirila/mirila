package nl.mirila.files.local;

import nl.mirila.core.exceptions.MirilaException;

import java.nio.file.Path;

public class PathViolationException extends MirilaException {

    public PathViolationException(Path basePath, Path path) {
        super(String.format("Path '%s' not allowed to leave the scope of '%s'.", path.toString(), basePath.toString()));
    }

}
