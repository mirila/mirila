package nl.mirila.files.local;

import com.google.inject.AbstractModule;
import nl.mirila.files.core.FileManager;

public class LocalFileManagerModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(FileManager.class).to(LocalFileManager.class);
    }

}
