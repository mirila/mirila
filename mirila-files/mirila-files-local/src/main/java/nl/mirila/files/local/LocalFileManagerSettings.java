package nl.mirila.files.local;

import com.google.inject.Inject;
import nl.mirila.core.settings.Settings;

public class LocalFileManagerSettings {

    public static final String KEY_BASE_PATH = "files.path";

    private static final String DEFAULT_BASE_PATH = "/var/uploads/";

    private final Settings settings;

    /**
     * Initialize a new instance.
     */
    @Inject
    public LocalFileManagerSettings(Settings settings) {
        this.settings = settings;
    }

    /**
     * Return the base for all local files.
     * <p>
     * Setting: {@value KEY_BASE_PATH}. Default: {@value DEFAULT_BASE_PATH}.
     */
    public String getBasePath() {
        return settings.getString(KEY_BASE_PATH).orElse(DEFAULT_BASE_PATH);
    }

}

