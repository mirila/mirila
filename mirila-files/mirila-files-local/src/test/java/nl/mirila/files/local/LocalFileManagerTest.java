package nl.mirila.files.local;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import nl.mirila.core.datatype.Id;
import nl.mirila.core.settings.modules.KeyValuesSettingsModule;
import nl.mirila.files.core.FileManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

import static nl.mirila.files.local.LocalFileManagerSettings.KEY_BASE_PATH;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class LocalFileManagerTest {

    private FileManager manager;
    private LocalFileManagerSettings settings;

    @BeforeEach
    void setUp() throws URISyntaxException {
        Path resourcePath = Path.of(LocalFileManagerTest.class.getResource(File.separator).toURI());
        Path basePath = Path.of(resourcePath.getParent().toAbsolutePath().toString(), "uploads");

        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                Map<String, String> settings = new HashMap<>();
                settings.put(KEY_BASE_PATH, basePath.toString());

                install(new KeyValuesSettingsModule(settings));
                install(new LocalFileManagerModule());
            }
        });
        manager = injector.getInstance(FileManager.class);
        settings = injector.getInstance(LocalFileManagerSettings.class);
    }

    @Test
    void testFileManager() throws IOException {
        Path fileA = Path.of(this.getClass().getResource(File.separator + "fileA.txt").getFile());
        Path fileB = Path.of(this.getClass().getResource(File.separator + "fileB.txt").getFile());
        Path destPath = Path.of(settings.getBasePath(), "folder1", "folder2", "file1.txt");

        Id testFolderId = Id.of("folder1" + File.separator + "folder2");
        String testFileName = "file1.txt";

        assertThat(Files.exists(fileA)).isTrue();
        assertThat(Files.exists(fileB)).isTrue();

        // Creating a file.
        manager.create(testFolderId, testFileName, fileA.toFile());
        assertThat(Files.exists(destPath)).isTrue();
        assertThat(Files.readString(destPath)).contains("This is test file A.");

        // Reading a file.
        manager.read(testFolderId, testFileName);

        // Updating a file.
        manager.update(testFolderId, testFileName, fileB.toFile());
        assertThat(Files.exists(destPath)).isTrue();
        assertThat(Files.readString(destPath)).contains("This is test file B.");

        // Deleting a file.
        manager.delete(testFolderId, testFileName);
        assertThat(Files.exists(destPath)).isFalse();
    }

    @Test()
    void testPathViolationException() {
        Id testFolderId = Id.of(Path.of("..", "etc").toString());
        String testFileName = "passwd";

        assertThatThrownBy(() -> manager.read(testFolderId, testFileName))
                .isInstanceOf(PathViolationException.class);
    }

}
