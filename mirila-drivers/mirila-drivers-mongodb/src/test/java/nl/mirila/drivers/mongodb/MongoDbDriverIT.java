package nl.mirila.drivers.mongodb;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import nl.mirila.core.settings.modules.KeyValuesSettingsModule;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static nl.mirila.drivers.core.DriverStatus.HEALTHY;
import static nl.mirila.drivers.mongodb.MongoDbSettings.KEY_DATABASE;
import static org.assertj.core.api.Assertions.assertThat;

class MongoDbDriverIT {

    @Test
    void testGetDatabase() throws InterruptedException {
        final Map<String, String> settings = new HashMap<>();
        settings.put(KEY_DATABASE, "mirila");

        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                install(new KeyValuesSettingsModule(settings));
                install(new MongoDbDriverModule());
            }
        });
        MongoDbDriver driver = injector.getInstance(MongoDbDriver.class);
        assertThat(driver.getDriverInformation().getStatus()).isEqualTo(HEALTHY);
        assertThat(driver.getDatabase("mirila")).isPresent();
    }

}
