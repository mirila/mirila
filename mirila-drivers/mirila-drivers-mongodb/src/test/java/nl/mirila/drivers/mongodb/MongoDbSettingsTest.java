package nl.mirila.drivers.mongodb;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import nl.mirila.core.settings.modules.KeyValuesSettingsModule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static nl.mirila.drivers.mongodb.MongoDbSettings.*;
import static org.assertj.core.api.Assertions.assertThat;

class MongoDbSettingsTest {

    private MongoDbSettings mongoDbSettings;

    @BeforeEach
    void setUp() {
        final Map<String, String> settings = new HashMap<>();
        settings.put(KEY_HOST, "my.local.host");
        settings.put(KEY_PORT, "27017");
        settings.put(KEY_DATABASE, "my-database");
        settings.put(KEY_USERNAME, "my-username");
        settings.put(KEY_PASSWORD, "my-password");

        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                install(new KeyValuesSettingsModule(settings));
            }
        });
        mongoDbSettings = injector.getInstance(MongoDbSettings.class);
    }

    @Test
    void testDatabaseSetting() {
        assertThat(mongoDbSettings.getDatabase()).isEqualTo("my-database");
    }

    @Test
    void testUsernameSettings() {
        assertThat(mongoDbSettings.getUsername()).isEqualTo("my-username");
    }

    @Test
    void testPasswordSettings() {
        assertThat(mongoDbSettings.getPassword()).isEqualTo("my-password");
    }

    @Test
    void testUriSettings() {
        assertThat(mongoDbSettings.getUri())
                .isEqualTo("mongodb://my-username:my-password@my.local.host:27017/my-database");
    }

}
