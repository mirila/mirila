package nl.mirila.drivers.mongodb;

import com.google.inject.Inject;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;
import com.mongodb.event.ServerMonitorListener;
import nl.mirila.drivers.core.Driver;
import nl.mirila.drivers.core.DriverInformation;
import nl.mirila.drivers.core.DriverStatus;
import nl.mirila.drivers.core.DriverUri;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.inject.Singleton;
import java.util.Optional;

/**
 * The MongoDbDriver makes a connection to a MongoDB server.
 */
@Singleton
public class MongoDbDriver implements Driver, ServerMonitorListener {

    private static final Logger logger = LogManager.getLogger(MongoDbDriver.class);
    private final MongoDbSettings settings;

    private MongoClient mongoClient;

    /**
     * Initialize a new instance.
     */
    @Inject
    public MongoDbDriver(MongoDbSettings settings) {
        this.settings = settings;
        String uri = settings.getUri();
        logger.info("Connecting to MongoDb: {}", DriverUri.obfuscateUri(uri));
        try {
            mongoClient = MongoClients.create(uri);
            // The following simple database action – listing collection names – forces to have a valid connection.
            mongoClient.getDatabase(settings.getDatabase()).listCollectionNames().spliterator();
            logger.info("Successfully connected to MongoDb");
        } catch (Exception e) {
            mongoClient = null;
            logger.fatal(e.getMessage());
        }
    }

    /**
     * Return the name of this MongoDB driver.
     */
    @Override
    public String getDriverName() {
        return "MongoDB";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DriverInformation getDriverInformation() {
        return (mongoClient == null)
                ? new DriverInformation(DriverStatus.ERROR, "There's no connection.")
                : new DriverInformation(DriverStatus.HEALTHY);
    }

    /**
     * Return the {@link MongoDatabase} for the configured database.
     * <p>
     * @return The MongoDatabase.
     */
    public Optional<MongoDatabase> getDatabase() {
        return getDatabase(settings.getDatabase());
    }


    /**
     * Return the {@link MongoDatabase} for the given database name.
     * <p>
     * @param database The name of the database.
     * @return The MongoDatabase.
     */
    public Optional<MongoDatabase> getDatabase(String database) {
        return (mongoClient != null)
                ? Optional.of(mongoClient.getDatabase(database))
                : Optional.empty();
    }
}
