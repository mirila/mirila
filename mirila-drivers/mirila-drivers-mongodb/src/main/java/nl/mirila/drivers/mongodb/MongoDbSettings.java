package nl.mirila.drivers.mongodb;

import com.google.inject.Inject;
import nl.mirila.core.settings.Settings;
import nl.mirila.drivers.core.DriverUri;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Named;
import javax.validation.constraints.NotNull;

/**
 * The settings that are relevant for MongoDB.
 */
public class MongoDbSettings {

    public static final String KEY_HOST = "mongodb.host";
    public static final String KEY_PORT = "mongodb.port";
    public static final String KEY_DATABASE = "mongodb.database";
    public static final String KEY_USERNAME = "mongodb.username";
    public static final String KEY_PASSWORD = "mongodb.password";
    public static final String KEY_URI = "mongodb.uri";

    private static final String DEFAULT_HOST = "localhost";
    private static final int DEFAULT_PORT = 27017;
    private static final String DEFAULT_DATABASE = "mirila";
    private static final String DEFAULT_USERNAME = "";
    private static final String DEFAULT_PASSWORD = "";

    private final Settings settings;

    /**
     * The realm.
     * <p>
     * We apply field injection, because we need realm to be an optional injection, but Settings not.
     */
    @Inject(optional = true)
    @Named("realm")
    private String realm;

    /**
     * Initialize a new instance.
     */
    @Inject
    MongoDbSettings(Settings settings) {
        this.settings = settings;
    }

    /**
     * Return the host of MongoDB.
     * <p>
     * Setting: {@value KEY_HOST}. Default: {@value DEFAULT_HOST}.
     */
    @NotNull
    public String getHost() {
        return settings.getString(KEY_HOST).orElse(DEFAULT_HOST);
    }

    /**
     * Return the port of MongoDB.
     * <p>
     * Setting: {@value KEY_PORT}. Default: {@value DEFAULT_PORT}.
     */
    public int getPort() {
        return settings.getInteger(KEY_PORT).orElse(DEFAULT_PORT);
    }

    /**
     * Return the database name.
     * <p>
     * Setting: {@value KEY_DATABASE}. Default: realm, if available or {@value DEFAULT_DATABASE}.
     */
    public String getDatabase() {
        return settings.getString(KEY_DATABASE).orElseGet(() -> {
            return StringUtils.defaultIfEmpty(realm, DEFAULT_DATABASE);
        });
    }

    /**
     * Return the username that must be used to access MongoDB.
     * <p>
     * Setting: {@value KEY_USERNAME}. Default: {@value DEFAULT_USERNAME}.
     */
    public String getUsername() {
        return settings.getString(KEY_USERNAME).orElse(DEFAULT_USERNAME);
    }

    /**
     * Return the password that must be used to access MongoDB.
     * <p>
     * Setting: {@value KEY_PASSWORD}. Default: {@value DEFAULT_PASSWORD}.
     */
    @NotNull
    public String getPassword() {
        return settings.getString(KEY_PASSWORD).orElse(DEFAULT_PASSWORD);
    }

    /**
     * Return the MongoDb URI.
     */
    public String getUri() {
        return settings.getString(KEY_URI).orElseGet(this::constructUri);
    }

    /**
     * Construct an uri for the connection.
     */
    private String constructUri() {
        return DriverUri.forProtocol("mongodb")
                .withUsernameAndPassword(getUsername(), getPassword())
                .withHost(getHost())
                .withPort(getPort())
                .withPath(getDatabase())
                .toUriString();
    }

}
