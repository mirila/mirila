package nl.mirila.drivers.mongodb;

import com.google.inject.AbstractModule;
import com.google.inject.multibindings.Multibinder;
import nl.mirila.drivers.core.Driver;

public class MongoDbDriverModule extends AbstractModule {

    @Override
    protected void configure() {
        Multibinder<Driver> driversBinding = Multibinder.newSetBinder(binder(), Driver.class);
        driversBinding.addBinding().to(MongoDbDriver.class).asEagerSingleton();
    }

}
