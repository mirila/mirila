package nl.mirila.drivers.mongodb;

import nl.mirila.drivers.core.MissingConnectionException;

/**
 * The exception can be used if the connection to the MongoDB server is missing, but required.
 */
public class MissingMongoDbConnectionException extends MissingConnectionException {

    public MissingMongoDbConnectionException() {
        super("MongoDB");
    }

}
