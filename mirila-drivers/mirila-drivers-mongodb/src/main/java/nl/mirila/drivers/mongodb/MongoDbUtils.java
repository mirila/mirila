package nl.mirila.drivers.mongodb;

import com.mongodb.client.MongoDatabase;

import static com.google.common.collect.Streams.stream;

public final class MongoDbUtils {

    /**
     * This utility class may not be instantiated.
     */
    private MongoDbUtils() {
        throw new UnsupportedOperationException();
    }

    /**
     * Return true if the collection with the given name exists at the given database.
     * <p>
     * @param database The database that may contain the requested collection.
     * @param collectionName The name of the collection.
     * @return True if the collection exists.
     */
    public static boolean doesCollectionExist(MongoDatabase database, String collectionName) {
        return stream(database.listCollectionNames()).anyMatch(collectionName::equalsIgnoreCase);
    }

}
