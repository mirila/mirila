package nl.mirila.drivers.mollie;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import nl.mirila.core.settings.modules.KeyValuesSettingsModule;
import nl.mirila.drivers.core.DriverInformation;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static nl.mirila.drivers.core.DriverStatus.ERROR;
import static nl.mirila.drivers.core.DriverStatus.HEALTHY;
import static org.assertj.core.api.Assertions.assertThat;

class MollieDriverTest {

    @Test
    void getDriverInformationWithApiKey() {
        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                Map<String, String> settingsMap = new HashMap<>();
                settingsMap.put(MollieSettings.KEY_API_KEY, "test_3UEED8xaPeqrgBGfjCD4u6jMR6teuv");
                install(new KeyValuesSettingsModule(settingsMap));
            }
        });
        MollieDriver driver = injector.getInstance(MollieDriver.class);
        DriverInformation information = driver.getDriverInformation();
        assertThat(information).isNotNull();
        assertThat(information.getStatus()).isEqualTo(HEALTHY);
    }

    @Test
    void getDriverInformationWithoutApiKey() {
        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                Map<String, String> settingsMap = new HashMap<>();
                settingsMap.put(MollieSettings.KEY_API_KEY, "");
                install(new KeyValuesSettingsModule(settingsMap));
            }
        });
        MollieDriver driver = injector.getInstance(MollieDriver.class);
        DriverInformation information = driver.getDriverInformation();
        assertThat(information).isNotNull();
        assertThat(information.getStatus()).isEqualTo(ERROR);
    }

}