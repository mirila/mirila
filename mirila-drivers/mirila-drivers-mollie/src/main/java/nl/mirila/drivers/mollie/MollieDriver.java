package nl.mirila.drivers.mollie;

import be.woutschoovaerts.mollie.Client;
import be.woutschoovaerts.mollie.ClientBuilder;
import com.google.inject.Inject;
import nl.mirila.drivers.core.Driver;
import nl.mirila.drivers.core.DriverInformation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static nl.mirila.drivers.core.DriverStatus.ERROR;
import static nl.mirila.drivers.core.DriverStatus.HEALTHY;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

public class MollieDriver implements Driver {

    private static final Logger logger = LogManager.getLogger(MollieDriver.class);

    private final Client client;

    /**
     * Initialize a new instance.
     */
    @Inject
    public MollieDriver(MollieSettings settings) {
        String apiKey = settings.getApiKey();
        if (isNotBlank(apiKey)) {
            client = new ClientBuilder()
                    .withApiKey(apiKey)
                    .withTestMode(settings.isInTestMode())
                    .build();
        } else {
            logger.error("Missing configuration " + MollieSettings.KEY_API_KEY);
            client = null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DriverInformation getDriverInformation() {
        return (client != null)
            ? new DriverInformation(HEALTHY, "The client is configured successfully.")
            : new DriverInformation(ERROR, "The client is not configured correctly. Restart is required.");
    }

    /**
     * Return the Mollie client.
     */
    public Client getClient() {
        return client;
    }
}
