package nl.mirila.drivers.mollie;

import com.google.inject.Inject;
import nl.mirila.core.settings.Settings;

import javax.validation.constraints.NotNull;

public class MollieSettings {

    public static final String KEY_API_KEY = "mollie.api-key";
    protected static final String KEY_IN_TEST_MODE = "mollie.test-mode";

    private final Settings settings;

    /**
     * Initialize a new instance.
     */
    @Inject
    public MollieSettings(Settings settings) {
        this.settings = settings;
    }

    /**
     * Return the API key to use with Mollie.
     * <p>
     * Setting: {@value KEY_API_KEY}.
     */
    @NotNull
    public String getApiKey() {
        return settings.getString(KEY_API_KEY).orElse("");
    }

    /**
     * Return true if the client should be in test mode. If not provided, it will check whether the key starts with the
     * prefix 'test_'.
     * <p>
     * Setting: {@value KEY_IN_TEST_MODE}.
     */
    @NotNull
    public boolean isInTestMode() {
        return settings.getBoolean(KEY_IN_TEST_MODE).orElse(getApiKey().startsWith("test_"));
    }


}
