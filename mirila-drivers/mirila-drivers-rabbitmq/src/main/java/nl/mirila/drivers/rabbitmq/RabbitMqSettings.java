package nl.mirila.drivers.rabbitmq;

import com.google.inject.Inject;
import nl.mirila.core.settings.Settings;

import javax.validation.constraints.NotNull;

/**
 * The settings that are relevant for RabbitMQ.
 */
public class RabbitMqSettings {

    public static final String KEY_HOST = "rabbitmq.host";
    public static final String KEY_PORT = "rabbitmq.port";
    public static final String KEY_USERNAME = "rabbitmq.username";
    public static final String KEY_PASSWORD = "rabbitmq.password";

    private static final String DEFAULT_HOST = "localhost";
    private static final int DEFAULT_PORT = 5672;
    private static final String DEFAULT_USERNAME = "guest";
    private static final String DEFAULT_PASSWORD = "guest";

    private final Settings settings;

    /**
     * Initialize a new instance.
     */
    @Inject
    RabbitMqSettings(Settings settings) {
        this.settings = settings;
    }

    /**
     * Return the host of RabbitMQ.
     * <p>
     * Setting: {@value KEY_HOST}. Default: {@value DEFAULT_HOST}.
     */
    @NotNull
    public String getHost() {
        return settings.getString(KEY_HOST).orElse(DEFAULT_HOST);
    }

    /**
     * Return the port of RabbitMQ.
     * <p>
     * Setting: {@value KEY_PORT}. Default: {@value DEFAULT_PORT}.
     */
    public int getPort() {
        return settings.getInteger(KEY_PORT).orElse(DEFAULT_PORT);
    }

    /**
     * Return the username that must be used to access RabbitMQ.
     * <p>
     * Setting: {@value KEY_USERNAME}. Default: {@value DEFAULT_USERNAME}.
     */
    public String getUsername() {
        return settings.getString(KEY_USERNAME).orElse(DEFAULT_USERNAME);
    }

    /**
     * Return the password that must be used to access RabbitMQ.
     * <p>
     * Setting: {@value KEY_PASSWORD}. Default: {@value DEFAULT_PASSWORD}.
     */
    @NotNull
    public String getPassword() {
        return settings.getString(KEY_PASSWORD).orElse(DEFAULT_PASSWORD);
    }

}
