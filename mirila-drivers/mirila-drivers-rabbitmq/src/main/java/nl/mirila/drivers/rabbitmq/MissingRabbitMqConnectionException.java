package nl.mirila.drivers.rabbitmq;

import nl.mirila.drivers.core.MissingConnectionException;

/**
 * This exception can be used if the connection to the RabbitMQ server is missing, but required.
 */
public class MissingRabbitMqConnectionException extends MissingConnectionException {

    public MissingRabbitMqConnectionException() {
        super("RabbitMQ");
    }

}
