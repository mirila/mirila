package nl.mirila.drivers.rabbitmq;

import com.google.inject.Inject;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import nl.mirila.drivers.core.Driver;
import nl.mirila.drivers.core.DriverInformation;
import nl.mirila.drivers.core.DriverStatus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.inject.Singleton;
import java.util.Optional;

/**
 * The RabbitMqDriver makes a connection to a RabbitMQ server.
 */
@Singleton
public class RabbitMqDriver implements Driver {

    private static final Logger logger = LogManager.getLogger(RabbitMqDriver.class);

    private Connection connection;

    /**
     * Initialize a new instance.
     */
    @Inject
    public RabbitMqDriver(RabbitMqSettings settings) {
        ConnectionFactory factory = new ConnectionFactory();
        try {
            factory.setUsername(settings.getUsername());
            factory.setPassword(settings.getPassword());
            factory.setVirtualHost("/");
            factory.setHost(settings.getHost());
            factory.setPort(settings.getPort());
            connection = factory.newConnection();
        } catch (Exception e) {
            logger.fatal(e.getMessage(), e);
            connection = null;
        }
    }

    /**
     * Return the name of this RabbitMQ driver.
     */
    @Override
    public String getDriverName() {
        return "RabbitMQ";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DriverInformation getDriverInformation() {
        return ((connection != null) && (connection.isOpen()))
                ? new DriverInformation(DriverStatus.HEALTHY)
                : new DriverInformation(DriverStatus.ERROR, "The connection is closed.");
    }

    /**
     * Return the connection.
     */
    public Optional<Connection> getConnection() {
        return Optional.ofNullable(connection);
    }

}
