package nl.mirila.drivers.rabbitmq;

import nl.mirila.core.exceptions.MirilaException;

/**
 * This exception can be used if a channel to the RabbitMQ server is missing.
 */
public class MissingRabbitMqChannel extends MirilaException {

    public MissingRabbitMqChannel() {
        super("Missing RabbitMQ channel for the current thread.");
    }

    public MissingRabbitMqChannel(String name) {
        super("Missing RabbitMQ channel '" + name + "'");
    }

}
