package nl.mirila.drivers.rabbitmq;

import com.google.inject.Inject;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.util.Strings;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Representation of RabbitMQ channels.
 */
public class RabbitMqChannels {

    private static final Logger logger = LogManager.getLogger(RabbitMqChannels.class);

    private final RabbitMqDriver driver;
    private final Map<String, Channel> channels;

    /**
     * Initialize a new instance.
     */
    @Inject
    public RabbitMqChannels(RabbitMqDriver driver) {
        this.driver = driver;
        channels = new HashMap<>();
    }

    /**
     * Return the RabbitMQ {@link Channel} that is used for the current thread (by name), or an empty {@link Optional}
     * if it wasn't possible to get a proper connection.
     * <p>
     * @return The channel.
     */
    public Optional<Channel> getCurrentChannel() {
        return getChannel(Thread.currentThread().getName());
    }


    /**
     * Return the RabbitMQ {@link Channel} that is used for the given name, or an empty {@link Optional} if it wasn't
     * possible to get a proper connection.
     * <p>
     * @param name The name.
     * @return The channel.
     */
    public Optional<Channel> getChannel(String name) {
        if (Strings.isBlank(name)) {
            throw new IllegalArgumentException("Parameter name may not be blank.");
        }
        if ((!channels.containsKey(name)) || (!channels.get(name).isOpen())) {
            Optional<Connection> optionalConnection = driver.getConnection();
            if (optionalConnection.isEmpty()) {
                return Optional.empty();
            }
            Channel channel;
            try {
                channel = optionalConnection.get().createChannel();
            } catch (IOException e) {
                logger.error(e.getMessage());
                return Optional.empty();
            }
            channels.put(name, channel);
        }
        return Optional.of(channels.get(name));
    }

}
