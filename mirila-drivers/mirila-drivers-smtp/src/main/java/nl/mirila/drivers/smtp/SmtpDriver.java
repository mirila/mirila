package nl.mirila.drivers.smtp;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import nl.mirila.drivers.core.Driver;
import nl.mirila.drivers.core.DriverInformation;
import nl.mirila.drivers.core.DriverStatus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.simplejavamail.api.mailer.Mailer;
import org.simplejavamail.mailer.MailerBuilder;

import java.util.Optional;

/**
 * The SmtpDriver makes it possible to send emails via SMTP server.
 * <p>
 * This driver uses <a href='http://www.simplejavamail.org'>Simple Java Mail library</a>.
 */
@Singleton
public class SmtpDriver implements Driver {

    private static final Logger logger = LogManager.getLogger(SmtpDriver.class);

    private final Mailer mailer;

    /**
     * Initialize a new instance.
     */
    @Inject
    public SmtpDriver(SmtpSettings settings) {
        mailer = MailerBuilder
                .withSMTPServerHost(settings.getHost())
                .withSMTPServerPort(settings.getPort())
                .withSMTPServerUsername(settings.getUsername())
                .withSMTPServerPassword(settings.getPassword())
                .withSessionTimeout(settings.getSessionTimeout())
                .buildMailer();
        testConnection();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getDriverName() {
        return "SMTP";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DriverInformation getDriverInformation() {
        return (testConnection())
                ? new DriverInformation(DriverStatus.HEALTHY)
                : new DriverInformation(DriverStatus.ERROR, "The connection is closed.");
    }

    /**
     * Return true if the connection is working.
     */
    private boolean testConnection() {
        try {
            mailer.testConnection();
            return true;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return false;
        }
    }

    /**
     * Return the {@link Mailer}, in an {@link Optional}. If the connection should be tested, and it failed,
     * an empty {@link Optional} is returned.
     * <p>
     * @param shouldTestConnection Set to true to test the connection.
     * @return The mailer or an empty Optional.
     */
    public Optional<Mailer> getMailer(boolean shouldTestConnection) {
        return ((!shouldTestConnection) || (testConnection()))
                ? Optional.of(mailer)
                : Optional.empty();
    }

}
