package nl.mirila.drivers.smtp;

import nl.mirila.drivers.core.MissingConnectionException;

/**
 * This exception can be used if the connection to the SMTP server is missing, but required.
 */
public class MissingSmtpConnectionException extends MissingConnectionException {

    /**
     * Initialize a new instance.
     */
    public MissingSmtpConnectionException() {
        super("SMTP");
    }

}
