package nl.mirila.drivers.smtp;

import com.google.inject.Inject;
import nl.mirila.core.settings.Settings;

/**
 * The settings that are related to SMTP.
 */
public class SmtpSettings {

    public static final String KEY_HOST = "smtp.host";
    public static final String KEY_PORT = "smtp.port";
    public static final String KEY_USERNAME = "smtp.username";
    public static final String KEY_PASSWORD = "smtp.password";
    public static final String KEY_DEFAULT_FROM_ADDRESS = "smtp.from";
    public static final String KEY_SESSION_TIMEOUT_MILLIS = "smtp.timeout";

    private static final String DEFAULT_HOST = "localhost";
    private static final int DEFAULT_PORT = 25;
    private static final String DEFAULT_USERNAME = "";
    private static final String DEFAULT_PASSWORD = "";
    private static final String DEFAULT_FROM_ADDRESS = "example@example.com";
    private static final int DEFAULT_SESSION_TIMEOUT_MILLIS = 60_000;

    private final Settings settings;

    /**
     * Initialize a new instance.
     */
    @Inject
    public SmtpSettings(Settings settings) {
        this.settings = settings;
    }

    /**
     * Return the host of the SMTP server.
     * <p>
     * Setting: {@value KEY_HOST}. Default: {@value DEFAULT_HOST}.
     */
    public String getHost() {
        return settings.getString(KEY_HOST).orElse(DEFAULT_HOST);
    }

    /**
     * Return the port of the SMTP server.
     * <p>
     * Setting: {@value KEY_PORT}. Default: {@value DEFAULT_PORT}.
     */
    public int getPort() {
        return settings.getInteger(KEY_PORT).orElse(DEFAULT_PORT);
    }

    /**
     * Return the username to access the SMTP server.
     * <p>
     * Setting: {@value KEY_USERNAME}. Default: {@value DEFAULT_USERNAME}.
     */
    public String getUsername() {
        return settings.getString(KEY_USERNAME).orElse(DEFAULT_USERNAME);
    }

    /**
     * Return the password to access the SMTP server.
     * <p>
     * Setting: {@value KEY_PASSWORD}. Default: {@value DEFAULT_PASSWORD}.
     */
    public String getPassword() {
        return settings.getString(KEY_PASSWORD).orElse(DEFAULT_PASSWORD);
    }

    /**
     * Return the default from email address that can be used if no from is given.
     * <p>
     * Setting: {@value KEY_DEFAULT_FROM_ADDRESS}. Default: {@value DEFAULT_FROM_ADDRESS}.
     */
    public String getDefaultFromAddress() {
        return settings.getString(KEY_DEFAULT_FROM_ADDRESS).orElse(DEFAULT_FROM_ADDRESS);
    }

    /**
     * Return the timeout in seconds of the SMTP server.
     * <p>
     * Setting: {@value KEY_SESSION_TIMEOUT_MILLIS}. Default: {@value DEFAULT_SESSION_TIMEOUT_MILLIS}.
     */
    public int getSessionTimeout() {
        return settings.getInteger(KEY_SESSION_TIMEOUT_MILLIS).orElse(DEFAULT_SESSION_TIMEOUT_MILLIS);
    }

}
