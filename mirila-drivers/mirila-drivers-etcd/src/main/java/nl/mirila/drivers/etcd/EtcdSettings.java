package nl.mirila.drivers.etcd;

import com.google.inject.Inject;
import nl.mirila.core.settings.Settings;
import nl.mirila.drivers.core.DriverUri;

import javax.validation.constraints.NotNull;

/**
 * The settings that are relevant for Etcd.
 */
public class EtcdSettings {

    public static final String KEY_HOST = "etcd.host";
    public static final String KEY_PORT = "etcd.port";
    public static final String KEY_PROTOCOL = "etcd.protocol";
    public static final String KEY_USERNAME = "etcd.username";
    public static final String KEY_PASSWORD = "etcd.password";
    public static final String KEY_URI = "etcd.uri";

    private static final String DEFAULT_HOST = "localhost";
    private static final int DEFAULT_PORT = 2379;
    private static final String DEFAULT_PROTOCOL = "http";
    private static final String DEFAULT_USERNAME = "";
    private static final String DEFAULT_PASSWORD = "";

    private final Settings settings;

    /**
     * Initialize a new instance.
     */
    @Inject
    public EtcdSettings(Settings settings) {
        this.settings = settings;
    }

    /**
     * Return the host of the Etcd database.
     * <p>
     * Setting: {@value KEY_HOST}. Default: {@value DEFAULT_HOST}.
     */
    @NotNull
    public String getHost() {
        return settings.getString(KEY_HOST).orElse(DEFAULT_HOST);
    }

    /**
     * Return the port of the Etcd server.
     * <p>
     * Setting: {@value KEY_PORT}. Default: {@value DEFAULT_PORT}.
     */
    public int getPort() {
        return settings.getInteger(KEY_PORT).orElse(DEFAULT_PORT);
    }

    /**
     * Return the protocol of the Etcd server.
     * <p>
     * Setting: {@value KEY_PORT}. Default: {@value DEFAULT_PORT}.
     */
    public String getProtocol() {
        return settings.getString(KEY_PROTOCOL).orElse(DEFAULT_PROTOCOL);
    }

    /**
     * Return the username that must be used to access Etcd.
     * <p>
     * Setting: {@value KEY_USERNAME}. Default: {@value DEFAULT_USERNAME}.
     */
    public String getUsername() {
        return settings.getString(KEY_USERNAME).orElse(DEFAULT_USERNAME);
    }

    /**
     * Return the password that must be used to access Etcd.
     * <p>
     * Setting: {@value KEY_PASSWORD}. Default: {@value DEFAULT_PASSWORD}.
     */
    @NotNull
    public String getPassword() {
        return settings.getString(KEY_PASSWORD).orElse(DEFAULT_PASSWORD);
    }

    /**
     * Return the Etcd URI directly from the config or construct a new one.
     */
    @NotNull
    public String getUri() {
        return settings.getString(KEY_URI).orElseGet(this::constructUri);
    }

    /**
     * Construct an uri for the connection.
     */
    private String constructUri() {
        return DriverUri.forProtocol(getProtocol())
                .withUsernameAndPassword(getUsername(), getPassword())
                .withHost(getHost())
                .withPort(getPort())
                .toUriString();
    }
}
