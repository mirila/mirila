package nl.mirila.drivers.etcd;

import nl.mirila.drivers.core.MissingConnectionException;

public class MissingEtcdConnectionException extends MissingConnectionException {

    public MissingEtcdConnectionException() {
        super("Etcd");
    }

}
