package nl.mirila.drivers.etcd;

import com.google.inject.Inject;
import com.google.protobuf.ByteString;
import com.ibm.etcd.api.RangeResponse;
import com.ibm.etcd.client.EtcdClient;
import com.ibm.etcd.client.kv.KvClient;
import nl.mirila.drivers.core.Driver;
import nl.mirila.drivers.core.DriverInformation;
import nl.mirila.drivers.core.DriverStatus;
import nl.mirila.drivers.core.DriverUri;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.Instant;
import java.util.Optional;

/**
 * The EtcdDriver makes a connection to an Etcd server.
 */
public class EtcdDriver implements Driver {

    private static final Logger logger = LogManager.getLogger(EtcdDriver.class);

    private EtcdClient client;

    /**
     * Initialize a new instance.
     */
    @Inject
    public EtcdDriver(EtcdSettings settings) {
        try {
            EtcdClient.Builder builder = EtcdClient.forEndpoint(settings.getHost(), settings.getPort())
                    .withPlainText();
            if (StringUtils.isNotEmpty(settings.getUsername())) {
                builder.withCredentials(settings.getUsername(), settings.getPassword());
            }
            client = builder.build();
        } catch (Exception e) {
            client = null;
            logger.error("Error while connecting to the Etcd server via {}. Reason: {}",
                         DriverUri.obfuscateUri(settings.getUri()),
                         e.getMessage());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DriverInformation getDriverInformation() {
        if (client == null) {
            return new DriverInformation(DriverStatus.ERROR, "There's no connection.");
        }
        try {
            long now = Instant.now().toEpochMilli();
            ByteString key = ByteString.copyFromUtf8("mirila.etcd.driver.health.check");
            ByteString value = ByteString.copyFromUtf8(Long.toString(now));
            KvClient kvClient = client.getKvClient();
            kvClient.put(key, value).sync();
            RangeResponse response = kvClient.get(key).sync();
            kvClient.delete(key).sync();
            if (response.getCount() > 0) {
                long responseValue = Long.parseLong(new String(response.getKvs(0).getValue().toByteArray()));
                return (responseValue >= now)
                        ? new DriverInformation(DriverStatus.HEALTHY)
                        : new DriverInformation(DriverStatus.WARNING, "The stored timestamp is invalid.");
            }
            return new DriverInformation(DriverStatus.WARNING, "The timestamp is not stored correctly.");
        } catch (Exception e) {
            return new DriverInformation(DriverStatus.ERROR, e.getMessage());
        }
    }

    /**
     * Return the {@link EtcdClient} for the Etcd server.
     */
    public Optional<EtcdClient> getClient() {
        return Optional.ofNullable(client);
    }

}
