package nl.mirila.drivers.etcd;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import nl.mirila.core.settings.modules.KeyValuesSettingsModule;
import nl.mirila.drivers.core.DriverStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.assertj.core.api.Assertions.assertThat;

class EtcdDriverIT {

    private EtcdDriver driver;

    @BeforeEach
    void setUp() {
        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                install(new KeyValuesSettingsModule(new HashMap<>()));
            }
        });
        driver = injector.getInstance(EtcdDriver.class);
    }

    @Test
    void testGetDriverInformation() {
        assertThat(driver.getDriverInformation().getStatus())
                .isEqualTo(DriverStatus.HEALTHY);
    }

}
