package nl.mirila.drivers.sql;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import nl.mirila.core.settings.modules.KeyValuesSettingsModule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static nl.mirila.drivers.sql.SqlSettings.*;
import static org.assertj.core.api.Assertions.assertThat;

class SqlSettingsTest {

    private SqlSettings sqlSettings;

    @BeforeEach
    void setUp() {
        final Map<String, String> settings = new HashMap<>();
        settings.put(KEY_HOST, "my.local.host");
        settings.put(KEY_PORT, "6033");
        settings.put(KEY_DATABASE, "my-database");
        settings.put(KEY_USERNAME, "my-username");
        settings.put(KEY_PASSWORD, "my-password");

        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                install(new KeyValuesSettingsModule(settings));
            }
        });
        sqlSettings = injector.getInstance(SqlSettings.class);
    }

    @Test
    void testDatabaseSetting() {
        assertThat(sqlSettings.getDatabase()).isEqualTo("my-database");
    }

    @Test
    void testUsernameSettings() {
        assertThat(sqlSettings.getUsername()).isEqualTo("my-username");
    }

    @Test
    void testPasswordSettings() {
        assertThat(sqlSettings.getPassword()).isEqualTo("my-password");
    }

    @Test
    void testUriSettings() {
        assertThat(sqlSettings.getUri())
                .isEqualTo("jdbc:mysql://my-username:my-password@my.local.host:6033/my-database");
    }

}
