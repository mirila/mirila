package nl.mirila.drivers.sql;

import com.google.inject.Inject;
import nl.mirila.core.settings.ApplicationSettings;
import nl.mirila.core.settings.Settings;
import nl.mirila.drivers.core.DriverUri;

import static java.util.concurrent.TimeUnit.MINUTES;
import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * The settings that are relevant for SQL.
 */
public class SqlSettings {

    public static final String KEY_DRIVER = "sql.driver";
    public static final String KEY_HOST = "sql.host";
    public static final String KEY_PORT = "sql.port";
    public static final String KEY_DIALECT = "sql.dialect";
    public static final String KEY_DATABASE = "sql.database";
    public static final String KEY_USERNAME = "sql.username";
    public static final String KEY_PASSWORD = "sql.password";
    public static final String KEY_POOL_NAME = "sql.pool.name";
    public static final String KEY_POOL_SIZE_MIN = "sql.pool.min";
    public static final String KEY_POOL_SIZE_MAX = "sql.pool.max";
    public static final String KEY_MAX_LIFETIME = "sql.max.lifetime";
    public static final String KEY_CONNECTION_TIMEOUT = "sql.timeout.connection";
    public static final String KEY_IDLE_TIMEOUT = "sql.timeout.idle";
    public static final String KEY_URI = "sql.uri";

    private static final String DEFAULT_DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String DEFAULT_HOST = "localhost";
    private static final int DEFAULT_PORT = 3306;
    private static final String DEFAULT_DATABASE = "REALM";
    private static final String DEFAULT_DIALECT = "mysql";
    private static final String DEFAULT_USERNAME = "root";
    private static final String DEFAULT_PASSWORD = "";
    private static final String DEFAULT_POOL_NAME = "sql-connection";
    private static final int DEFAULT_MAX_POOL_SIZE = 10;
    private static final long DEFAULT_MAX_LIFETIME = MINUTES.toMillis(30);
    private static final long DEFAULT_CONNECTION_TIMEOUT = SECONDS.toMillis(30);
    private static final long DEFAULT_IDLE_TIMEOUT = MINUTES.toMillis(10);

    private final Settings settings;
    private final ApplicationSettings applicationSettings;

    /**
     * Initialize a new instance.
     */
    @Inject
    SqlSettings(Settings settings, ApplicationSettings applicationSettings) {
        this.settings = settings;
        this.applicationSettings = applicationSettings;
    }

    /**
     * Return the driver for the JDBC connection.
     * <p>
     * Setting: {@value KEY_DRIVER}. Default: {@value DEFAULT_DRIVER}.
     */
    public String getDriver() {
        return settings.getString(KEY_DRIVER).orElse(DEFAULT_DRIVER);
    }

    /**
     * Return the host of the SQL database.
     * <p>
     * Setting: {@value KEY_HOST}. Default: {@value DEFAULT_HOST}.
     */
    public String getHost() {
        return settings.getString(KEY_HOST).orElse(DEFAULT_HOST);
    }

    /**
     * Return the port of the SQL database.
     * <p>
     * Setting: {@value KEY_PORT}. Default: {@value DEFAULT_PORT}.
     */
    public int getPort() {
        return settings.getInteger(KEY_PORT).orElse(DEFAULT_PORT);
    }

    /**
     * Return the dialect of the SQL database.
     * <p>
     * Setting: {@value KEY_DIALECT}. Default: {@value DEFAULT_DIALECT}.
     */
    public String getDialect() {
        return settings.getString(KEY_DIALECT).orElse(DEFAULT_DIALECT);
    }

    /**
     * Return the database name.
     * <p>
     * Setting: {@value KEY_DATABASE}. Default: {@value DEFAULT_DATABASE}.
     */
    public String getDatabase() {
        String database = settings.getString(KEY_DATABASE).orElse(DEFAULT_DATABASE);
        return (database.equals("REALM"))
                ? applicationSettings.getRealm()
                : database;
    }

    /**
     * Return the username that must be used to access the database.
     * <p>
     * Setting: {@value KEY_USERNAME}. Default: {@value DEFAULT_USERNAME}.
     */
    public String getUsername() {
        return settings.getString(KEY_USERNAME).orElse(DEFAULT_USERNAME);
    }

    /**
     * Return the password that must be used to access the database.
     * <p>
     * Setting: {@value KEY_PASSWORD}. Default: {@value DEFAULT_PASSWORD}.
     */
    public String getPassword() {
        return settings.getString(KEY_PASSWORD).orElse(DEFAULT_PASSWORD);
    }

    /**
     * Return the jdbc URI directly from the config or construct a new one.
     */
    public String getUri() {
        return settings.getString(KEY_URI).orElseGet(this::constructUri);
    }

    /**
     * Return the name of the connection pool, which is also used a prefix for connection pool workers.
     */
    public String getPoolName() {
        return settings.getString(KEY_POOL_NAME).orElse(DEFAULT_POOL_NAME);
    }

    /**
     * Return the minimal amount of workers in the connection pool.
     * <p>
     * Setting: {@value KEY_POOL_SIZE_MIN}. Default: 0.
     */
    public int getMinPoolSize() {
        return settings.getInteger(KEY_POOL_SIZE_MIN).orElse(0);
    }

    /**
     * Return the maximum amount of workers in the connection pool.
     * <p>
     * Setting: {@value KEY_POOL_SIZE_MAX}. Default: {@value DEFAULT_MAX_POOL_SIZE}.
     */
    public int getMaxPoolSize() {
        return settings.getInteger(KEY_POOL_SIZE_MAX).orElse(DEFAULT_MAX_POOL_SIZE);
    }

    /**
     * Return the maximum lifetime of a connection.
     * <p>
     * Setting: {@value KEY_MAX_LIFETIME}. Default: 30 seconds (in millis).
     */
    public long getMaxLifetime() {
        return settings.getLong(KEY_MAX_LIFETIME).orElse(DEFAULT_MAX_LIFETIME);
    }

    /**
     * Return the timeout in milliseconds to set up a connection.
     * <p>
     * Setting: {@value KEY_CONNECTION_TIMEOUT}. Default: 30 seconds (in millis).
     */
    public long getConnectionTimeout() {
        return settings.getLong(KEY_CONNECTION_TIMEOUT).orElse(DEFAULT_CONNECTION_TIMEOUT);
    }

    /**
     * Return the timeout in milliseconds for a thread to be idle.
     * <p>
     * Setting: {@value KEY_IDLE_TIMEOUT}. Default: 10 minutes (in millis).
     */
    public long getIdleTimeout() {
        return settings.getLong(KEY_IDLE_TIMEOUT).orElse(DEFAULT_IDLE_TIMEOUT);
    }

    /**
     * Construct an uri for the connection.
     */
    private String constructUri() {
        return DriverUri.forProtocol("jdbc")
                .withDialect(getDialect())
                .withUsernameAndPassword(getUsername(), getPassword())
                .withHost(getHost())
                .withPort(getPort())
                .withPath(getDatabase())
                .toUriString();
    }

}
