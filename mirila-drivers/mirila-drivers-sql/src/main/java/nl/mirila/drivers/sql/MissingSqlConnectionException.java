package nl.mirila.drivers.sql;

import nl.mirila.drivers.core.MissingConnectionException;

/**
 * The exception can be used if the connection to the SQL server is missing, but required.
 */
public class MissingSqlConnectionException extends MissingConnectionException {

    public MissingSqlConnectionException() {
        super("SQL");
    }

}
