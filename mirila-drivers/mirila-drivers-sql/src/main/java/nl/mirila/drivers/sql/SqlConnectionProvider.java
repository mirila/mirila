package nl.mirila.drivers.sql;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.zaxxer.hikari.HikariDataSource;
import nl.mirila.core.exceptions.MirilaException;

import java.sql.Connection;
import java.sql.SQLException;

public class SqlConnectionProvider implements Provider<Connection> {

    private final SqlDriver sqlDriver;

    @Inject
    public SqlConnectionProvider(SqlDriver sqlDriver) {
        this.sqlDriver = sqlDriver;
    }

    @Override
    public Connection get() {
        HikariDataSource dataSource = sqlDriver.getDataSource().orElseThrow();
        try {
            return dataSource.getConnection();
        } catch (SQLException e) {
            throw new MirilaException(e.getMessage(), e);
        }
    }

}
