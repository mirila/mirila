package nl.mirila.drivers.sql;

import com.google.inject.AbstractModule;
import com.google.inject.multibindings.Multibinder;
import nl.mirila.drivers.core.Driver;
import org.jboss.resteasy.plugins.guice.RequestScoped;

import java.sql.Connection;

public class SqlDriverModule extends AbstractModule {

    @Override
    protected void configure() {
        Multibinder<Driver> driversBinding = Multibinder.newSetBinder(binder(), Driver.class);
        driversBinding.addBinding().to(SqlDriver.class).asEagerSingleton();

        bind(Connection.class).toProvider(SqlConnectionProvider.class).in(RequestScoped.class);
    }

}
