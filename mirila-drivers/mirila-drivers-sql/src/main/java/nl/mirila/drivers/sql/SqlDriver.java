package nl.mirila.drivers.sql;

import com.google.inject.Inject;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import nl.mirila.core.exceptions.MirilaException;
import nl.mirila.drivers.core.Driver;
import nl.mirila.drivers.core.DriverInformation;
import nl.mirila.drivers.core.DriverStatus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.inject.Singleton;
import java.util.Optional;

import static nl.mirila.drivers.core.DriverUri.obfuscateUri;

/**
 * The SqlDriver makes a connection to a SQL database.
 */
@Singleton
public class SqlDriver implements Driver {

    private static final Logger logger = LogManager.getLogger(SqlDriver.class);

    private final SqlSettings settings;
    private HikariDataSource dataSource;

    /**
     * Initialize a new instance, and make a connection.
     */
    @Inject
    public SqlDriver(SqlSettings settings) {
        this.settings = settings;
        connect();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getDriverName() {
        return "SQL with Hikari Connection Pool";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DriverInformation getDriverInformation() {
        if (dataSource == null) {
            return new DriverInformation(DriverStatus.INITIALIZING, "The connection is initializing");
        } else if (dataSource.isClosed()) {
            return new DriverInformation(DriverStatus.FATAL, "The connection is closed.");
        } else if (dataSource.isReadOnly()) {
            return new DriverInformation(DriverStatus.WARNING, "The connection is read-only.");
        } else if (dataSource.isRunning()) {
            return new DriverInformation(DriverStatus.HEALTHY);
        } else {
            return new DriverInformation(DriverStatus.ERROR, "The data source is in an unknown, unexpected state.");
        }
    }

    /**
     * Make a connection to a SQL database.
     */
    private void connect() {
        HikariConfig config = new HikariConfig();
        config.setDriverClassName(settings.getDriver());
        config.setJdbcUrl(settings.getUri());
        config.setUsername(settings.getUsername());
        config.setPassword(settings.getPassword());

        config.setPoolName(settings.getPoolName());
        config.setMinimumIdle(settings.getMinPoolSize());
        config.setMaximumPoolSize(settings.getMaxPoolSize());

        config.setMaxLifetime(settings.getMaxLifetime());
        config.setConnectionTimeout(settings.getConnectionTimeout());
        config.setIdleTimeout(settings.getIdleTimeout());

        try {
            dataSource = new HikariDataSource(config);
        } catch (Exception e) {
            logger.fatal(e.getMessage(), e);
            throw new MirilaException(e.getMessage(), e);
        }

        logger.info("Setting up connection pool to {} with username {}, using pool size of {}.",
                    obfuscateUri(config.getJdbcUrl()),
                    config.getUsername(),
                    config.getMaximumPoolSize());
    }

    /**
     * Return the {@link HikariDataSource} to a SQL database.
     */
    public Optional<HikariDataSource> getDataSource() {
        return Optional.ofNullable(dataSource);
    }

}
