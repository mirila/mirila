package nl.mirila.drivers.redis;

import com.google.inject.Inject;
import io.lettuce.core.SetArgs;

import java.util.List;
import java.util.Optional;

/**
 * The RedisDriverService communicates with a Redis server.
 */
public class RedisDriverService {

    private final RedisDriver driver;

    /**
     * Initialize a new instance.
     */
    @Inject
    public RedisDriverService(RedisDriver driver) {
        this.driver = driver;
    }

    /**
     * Perform a SET command to Redis, which sets the given key and value.
     * <p>
     * @param key The key.
     * @param value The value.
     * @param expiresAfter The amount of seconds after which this key expires.
     */
    public void set(String key, String value, long expiresAfter) {
        driver.getSyncConnection()
                .ifPresent((connection) -> connection.set(key, value, SetArgs.Builder.ex(expiresAfter)));
    }

    /**
     * Perform a GET command to Redis, which returns the value for the given key.
     * <p>
     * The result is wrapped in an {@link Optional}.
     * <p>
     * @param key The key.
     * @return The value.
     */
    public Optional<String> get(String key) {
        return driver.getSyncConnection().map((connection) -> connection.get(key));
    }

    /**
     * Perform a DEL command to Redis, which deletes the given key.
     * <p>
     * @param key The key to delete.
     */
    public void del(String key) {
        driver.getSyncConnection().ifPresent((connection) -> connection.del(key));
    }

    /**
     * Perform a DEL command to Redis, which deletes the given keys.
     * <p>
     * @param keys The keys to delete.
     */
    public void del(List<String> keys) {
        driver.getSyncConnection().ifPresent((connection) -> connection.del(keys.toArray(new String[0])));
    }

}
