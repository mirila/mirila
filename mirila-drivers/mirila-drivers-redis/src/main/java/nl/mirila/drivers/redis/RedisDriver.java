package nl.mirila.drivers.redis;

import com.google.inject.Inject;
import io.lettuce.core.RedisClient;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.sync.RedisCommands;
import io.lettuce.core.pubsub.StatefulRedisPubSubConnection;
import nl.mirila.drivers.core.Driver;
import nl.mirila.drivers.core.DriverInformation;
import nl.mirila.drivers.core.DriverStatus;
import nl.mirila.drivers.core.DriverUri;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.inject.Singleton;
import java.util.Optional;

/**
 * The RedisDriver makes a connection to a Redis server.
 */
@Singleton
public class RedisDriver implements Driver {

    private static final Logger logger = LogManager.getLogger(RedisDriver.class);
    private final StatefulRedisPubSubConnection<String, String> pubSubConnection;

    private StatefulRedisConnection<String, String> connection;

    /**
     * Initialize a new instance.
     */
    @Inject
    public RedisDriver(RedisSettings settings) {
        RedisClient client = RedisClient.create(settings.getUri());
        pubSubConnection = client.connectPubSub();
        try {
            connection = client.connect();
        } catch (Exception e) {
            connection = null;
            logger.error("Error while connecting to the Redis server via {}. Reason: {}",
                         DriverUri.obfuscateUri(settings.getUri()),
                         e.getMessage());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DriverInformation getDriverInformation() {
        return ((connection != null) && (connection.isOpen()))
                ? new DriverInformation(DriverStatus.HEALTHY)
                : new DriverInformation(DriverStatus.ERROR, "The connection is closed.");
    }

    /**
     * Return the synchronous connection to the Redis Server.
     */
    public Optional<RedisCommands<String, String>> getSyncConnection() {
        return ((connection != null) && (connection.isOpen()))
                ? Optional.of(connection.sync())
                : Optional.empty();
    }


    /**
     * Return the pub/sub connection for the Redis Server.
     */
    public Optional<StatefulRedisPubSubConnection<String, String>> getPubSubConnection() {
        return ((connection != null) && (connection.isOpen()) && (pubSubConnection != null))
                ? Optional.of(pubSubConnection)
                : Optional.empty();
    }

}
