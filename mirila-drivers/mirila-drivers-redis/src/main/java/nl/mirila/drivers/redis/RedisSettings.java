package nl.mirila.drivers.redis;

import com.google.inject.Inject;
import nl.mirila.core.exceptions.MissingSettingException;
import nl.mirila.core.settings.Settings;
import nl.mirila.drivers.core.DriverUri;

import javax.validation.constraints.NotNull;

/**
 * The settings that are relevant for Redis.
 */
public class RedisSettings {

    public static final String KEY_HOST = "redis.host";
    public static final String KEY_PORT = "redis.port";
    public static final String KEY_DATABASE_NUMBER = "redis.database_number";
    public static final String KEY_USERNAME = "redis.username";
    public static final String KEY_PASSWORD = "redis.password";
    public static final String KEY_URI = "redis.uri";

    private static final String DEFAULT_HOST = "localhost";
    private static final int DEFAULT_PORT = 6379;
    private static final int DEFAULT_DATABASE_NUMBER = 0;
    private static final String DEFAULT_USERNAME = "";
    private static final String DEFAULT_PASSWORD = "";

    private final Settings settings;

    /**
     * Initialize a new instance.
     */
    @Inject
    RedisSettings(Settings settings) {
        this.settings = settings;
    }

    /**
     * Return the host of the Redis database.
     * <p>
     * Setting: {@value KEY_HOST}. Default: {@value DEFAULT_HOST}.
     */
    @NotNull
    public String getHost() {
        return settings.getString(KEY_HOST).orElse(DEFAULT_HOST);
    }

    /**
     * Return the port of the Redis database.
     * <p>
     * Setting: {@value KEY_PORT}. Default: {@value DEFAULT_PORT}.
     */
    public int getPort() {
        return settings.getInteger(KEY_PORT).orElse(DEFAULT_PORT);
    }

    /**
     * Return the database number. If it is not configured, a {@link MissingSettingException} will be thrown.
     * <p>
     * <p>
     * Setting: {@value KEY_DATABASE_NUMBER}. Default: {@value DEFAULT_DATABASE_NUMBER}.
     */
    public int getDatabaseNumber() {
        return settings.getInteger(KEY_DATABASE_NUMBER).orElse(DEFAULT_DATABASE_NUMBER);
    }

    /**
     * Return the username that must be used to access the database.
     * <p>
     * Setting: {@value KEY_USERNAME}. Default: {@value DEFAULT_USERNAME}.
     */
    public String getUsername() {
        return settings.getString(KEY_USERNAME).orElse(DEFAULT_USERNAME);
    }

    /**
     * Return the password that must be used to access the database.
     * <p>
     * Setting: {@value KEY_PASSWORD}. Default: {@value DEFAULT_PASSWORD}.
     */
    @NotNull
    public String getPassword() {
        return settings.getString(KEY_PASSWORD).orElse(DEFAULT_PASSWORD);
    }

    /**
     * Return the Redis URI directly from the config or construct a new one.
     */
    @NotNull
    public String getUri() {
        return settings.getString(KEY_URI).orElseGet(this::constructUri);
    }

    /**
     * Construct an uri for the connection.
     */
    private String constructUri() {
        return DriverUri.forProtocol("redis")
                .withUsernameAndPassword(getUsername(), getPassword())
                .withHost(getHost())
                .withPort(getPort())
                .withPath(Integer.toString(getDatabaseNumber()))
                .toUriString();
    }

}
