package nl.mirila.drivers.redis;

import nl.mirila.drivers.core.MissingConnectionException;

/**
 * The exception can be used if the connection to the Redis server is missing, but required.
 */
public class MissingRedisConnectionException extends MissingConnectionException {

    public MissingRedisConnectionException() {
        super("Redis");
    }

}
