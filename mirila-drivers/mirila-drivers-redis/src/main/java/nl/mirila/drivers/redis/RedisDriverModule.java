package nl.mirila.drivers.redis;

import com.google.inject.AbstractModule;
import com.google.inject.multibindings.Multibinder;
import nl.mirila.drivers.core.Driver;

public class RedisDriverModule extends AbstractModule {

    @Override
    protected void configure() {
        Multibinder<Driver> driversBinding = Multibinder.newSetBinder(binder(), Driver.class);
        driversBinding.addBinding().to(RedisDriver.class).asEagerSingleton();
    }

}
