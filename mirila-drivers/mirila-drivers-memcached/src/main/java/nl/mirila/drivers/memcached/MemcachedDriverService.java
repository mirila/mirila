package nl.mirila.drivers.memcached;

import com.google.inject.Inject;

import java.util.Optional;

/**
 * The MemcachedDriverService exposes useful methods based on the {@link MemcachedDriver}.
 */
public class MemcachedDriverService {

    private final MemcachedDriver driver;

    /**
     * Initialize a new instance.
     */
    @Inject
    public MemcachedDriverService(MemcachedDriver driver) {
        this.driver = driver;
    }

    /**
     * Perform a ADD command to Memcached, which adds the given key and value.
     * <p>
     * @param key The key.
     * @param value The value.
     * @param expiresAfter The amount of seconds after which this key expires.
     */
    public void add(String key, Object value, int expiresAfter) {
        driver.getClient().ifPresent((client) -> client.add(key, expiresAfter, value));
    }

    /**
     * Perform a GET command to Memcached, which returns the value for the given key.
     * <p>
     * The result is wrapped in an {@link Optional}.
     * <p>
     * @param key The key.
     * @return The value.
     */
    public Optional<Object> get(String key) {
        return driver.getClient().map((client) -> client.get(key));
    }

    /**
     * Perform a DELETE command to Memcached, which deletes the key from Memcached.
     * <p>
     * @param key The key.
     */
    public void delete(String key) {
        driver.getClient().ifPresent((client) -> client.delete(key));
    }

}
