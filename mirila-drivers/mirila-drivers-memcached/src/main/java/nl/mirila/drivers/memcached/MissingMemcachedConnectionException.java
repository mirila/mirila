package nl.mirila.drivers.memcached;

import nl.mirila.drivers.core.MissingConnectionException;

/**
 * The exception can be used if the connection to the Memcached server is missing, but required.
 */
public class MissingMemcachedConnectionException extends MissingConnectionException {

    public MissingMemcachedConnectionException() {
        super("Memcached");
    }

}
