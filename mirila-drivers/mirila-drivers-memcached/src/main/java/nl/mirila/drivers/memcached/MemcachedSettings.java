package nl.mirila.drivers.memcached;

import com.google.inject.Inject;
import nl.mirila.core.settings.Settings;

import javax.validation.constraints.NotNull;

/**
 * The settings that are relevant for Memcached.
 */
public class MemcachedSettings {

    public static final String KEY_HOST = "memcached.host";
    public static final String KEY_PORT = "memcached.port";

    private static final String DEFAULT_HOST = "localhost";
    private static final int DEFAULT_PORT = 11211;

    private final Settings settings;

    /**
     * Initialize a new instance.
     */
    @Inject
    MemcachedSettings(Settings settings) {
        this.settings = settings;
    }

    /**
     * Return the host of the Memcached database.
     * <p>
     * Setting: {@value KEY_HOST}. Default: {@value DEFAULT_HOST}.
     */
    @NotNull
    public String getHost() {
        return settings.getString(KEY_HOST).orElse(DEFAULT_HOST);
    }

    /**
     * Return the port of the Memcached database.
     * <p>
     * Setting: {@value KEY_PORT}. Default: {@value DEFAULT_PORT}.
     */
    public int getPort() {
        return settings.getInteger(KEY_PORT).orElse(DEFAULT_PORT);
    }

}
