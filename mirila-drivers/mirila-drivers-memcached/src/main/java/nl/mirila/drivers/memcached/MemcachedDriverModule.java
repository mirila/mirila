package nl.mirila.drivers.memcached;

import com.google.inject.AbstractModule;
import com.google.inject.multibindings.Multibinder;
import nl.mirila.drivers.core.Driver;

public class MemcachedDriverModule extends AbstractModule {

    @Override
    protected void configure() {
        Multibinder<Driver> driversBinding = Multibinder.newSetBinder(binder(), Driver.class);
        driversBinding.addBinding().to(MemcachedDriver.class).asEagerSingleton();
    }

}
