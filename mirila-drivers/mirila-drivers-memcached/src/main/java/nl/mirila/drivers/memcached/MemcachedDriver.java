package nl.mirila.drivers.memcached;

import com.google.inject.Inject;
import net.spy.memcached.MemcachedClient;
import net.spy.memcached.MemcachedNode;
import nl.mirila.drivers.core.Driver;
import nl.mirila.drivers.core.DriverInformation;
import nl.mirila.drivers.core.DriverStatus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.inject.Singleton;
import java.net.InetSocketAddress;
import java.util.Optional;

/**
 * The MemcachedDriver makes a connection to a Memcached server.
 */
@Singleton
public class MemcachedDriver implements Driver {

    private static final Logger logger = LogManager.getLogger(MemcachedDriver.class);

    private final MemcachedSettings settings;
    private MemcachedClient client;

    /**
     * Initialize a new instance.
     */
    @Inject
    public MemcachedDriver(MemcachedSettings settings) {
        this.settings = settings;
        this.client = null;
        connect();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DriverInformation getDriverInformation() {
        if (!hasValidConnection()) {
            return new DriverInformation(DriverStatus.ERROR, "Not connected to Memcached.");
        }
        boolean isActive = client.getNodeLocator().getAll().stream().allMatch(MemcachedNode::isActive);
        return (isActive)
                ? new DriverInformation(DriverStatus.HEALTHY)
                : new DriverInformation(DriverStatus.ERROR, client.getConnection().connectionsStatus());
    }

    /**
     * Make a connection to Memcached.
     */
    private void connect() {
        InetSocketAddress address = new InetSocketAddress(settings.getHost(), settings.getPort());
        try {
            client = new MemcachedClient(address);
        } catch (Exception e) {
            client = null;
            logger.error("Error while connecting to the Redis server on {}:{}. Reason: {}",
                         settings.getHost(),
                         settings.getPort(),
                         e.getMessage());

            String msg = "Unable to make a connection to memcached {}:{}. Reason: {}";
            logger.error(msg, settings.getHost(), settings.getPort(), e.getMessage());
        }
    }

    /**
     * Return true if there is a connection to Memcached. If there isn't, try to make a connection.
     */
    private boolean hasValidConnection() {
        if (client == null) {
            connect();
        }
        return (client != null);
    }

    /**
     * Return the Memcached client, wrapped in an {@link Optional}. If there's no connection, an empty Optional is
     * returned.
     */
    public Optional<MemcachedClient> getClient() {
        return (hasValidConnection()) ? Optional.of(client) : Optional.empty();
    }

}
