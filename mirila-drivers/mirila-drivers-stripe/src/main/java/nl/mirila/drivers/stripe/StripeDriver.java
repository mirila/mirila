package nl.mirila.drivers.stripe;

import com.google.inject.Inject;
import com.stripe.Stripe;
import nl.mirila.drivers.core.Driver;
import nl.mirila.drivers.core.DriverInformation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static nl.mirila.drivers.core.DriverStatus.ERROR;
import static nl.mirila.drivers.core.DriverStatus.HEALTHY;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

public class StripeDriver implements Driver {

    private static final Logger logger = LogManager.getLogger(StripeDriver.class);

    /**
     * Initialize a new instance.
     */
    @Inject
    public StripeDriver(StripeSettings settings) {
        Stripe.apiKey = settings.getApiKey();
        if (isBlank(Stripe.apiKey)) {
            logger.error("Missing configuration " + StripeSettings.KEY_API_KEY);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DriverInformation getDriverInformation() {
        return (isNotBlank(Stripe.apiKey))
                ? new DriverInformation(HEALTHY, "The client is configured successfully.")
                : new DriverInformation(ERROR, "The client is not configured correctly. Restart is required.");
    }


}
