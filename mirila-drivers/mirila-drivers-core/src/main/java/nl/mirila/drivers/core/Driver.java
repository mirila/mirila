package nl.mirila.drivers.core;

/**
 * A Driver serves as an interface to an external service or hardware.
 * <p>
 * Each new instance register itself to the {@link DriverManager}, and adds an method interface to share the
 * status of the driver.
 */
public interface Driver {

    /**
     * Return the name of the driver.
     * <p>
     * By default this returns the simple name of the class, without the word 'Driver'.
     */
    default String getDriverName() {
        return getClass().getSimpleName().replace("Driver", "");
    }

    /**
     * Return {@link DriverInformation} about this driver.
     */
    DriverInformation getDriverInformation();

}
