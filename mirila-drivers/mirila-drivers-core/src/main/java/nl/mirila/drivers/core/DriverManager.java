package nl.mirila.drivers.core;

import com.google.inject.Inject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.inject.Singleton;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Manager for drivers.
 */
@Singleton
public class DriverManager {

    private static final Logger logger = LogManager.getLogger(DriverManager.class);

    private final Set<Driver> drivers;

    /**
     * Initialize a new instance.
     */
    @Inject
    public DriverManager(Set<Driver> drivers) {
        this.drivers = drivers;
    }

    /**
     * Returns a map with all drivers and their status information.
     */
    public Map<String, DriverInformation> getDriversInformation() {
        return drivers.stream()
                .collect(Collectors.toMap(Driver::getDriverName, this::getDriverInformation));
    }

    /**
     * Return the {@link DriverInformation} of the given driver.
     * <p>
     * If an exception is thrown it is logged, and a DriverInformation with {@link DriverStatus#FATAL} is returned.
     * <p>
     * @param driver The driver.
     * @return The driver information.
     */
    private DriverInformation getDriverInformation(Driver driver) {
        try {
            return driver.getDriverInformation();
        } catch (Exception e) {
            String msg = "An error occurred while fetching driver information for {}. Reason: {}";
            logger.fatal(msg, driver.getDriverName(), e.getMessage());
            return new DriverInformation(DriverStatus.FATAL, "The driver failed. See logging for details.");
        }
    }

}
