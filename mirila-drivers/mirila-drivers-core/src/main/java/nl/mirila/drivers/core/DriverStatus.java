package nl.mirila.drivers.core;

/**
 * The status of a driver.
 */
public enum DriverStatus {

    /**
     * The driver is in a initializing. This should not last long.
     */
    INITIALIZING,

    /**
     * The driver is in a healthy state.
     */
    HEALTHY,

    /**
     * The driver is in a healthy state, but there is a warning.
     */
    WARNING,

    /**
     * The driver is not connected to the service or interface.
     */
    ERROR,

    /**
     * The driver itself is not working. This should only be used by the {@link DriverManager}.
     */
    FATAL

}
