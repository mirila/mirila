package nl.mirila.drivers.core;

import org.apache.commons.lang3.StringUtils;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;

/**
 * Representation of a driver uri, using the protocol, host, port number and other information.
 */
public class DriverUri {

    private final String protocol;
    private String dialect;
    private String username;
    private String password;
    private String host;
    private int port;
    private String path;

    /**
     * Initialize a new instance using the given protocol.
     */
    private DriverUri(String protocol) {
        this.protocol = protocol;
        dialect = null;
        username = null;
        password = null;
        host = null;
        port = 0;
        path = null;
    }

    /**
     * Return a new instance using the given protocol.
     */
    public static DriverUri forProtocol(String protocol) {
        return new DriverUri(protocol);
    }

    /**
     * Set the dialect and return this instance for chaining.
     * <p>
     * @param dialect The dialect.
     * @return This instance.
     */
    public DriverUri withDialect(String dialect) {
        this.dialect = dialect;
        return this;
    }

    /**
     * Set the username and return this instance for chaining.
     * <p>
     * @param username The username.
     * @return This instance.
     */
    public DriverUri withUsername(String username) {
        this.username = username;
        return this;
    }

    /**
     * Set the username and password, and return this instance for chaining.
     * <p>
     * @param username The username.
     * @param password The password.
     * @return This instance.
     */
    public DriverUri withUsernameAndPassword(String username, String password) {
        this.username = username;
        this.password = password;
        return this;
    }

    /**
     * Set the host and return this instance for chaining.
     * <p>
     * @param host The host.
     * @return This instance.
     */
    public DriverUri withHost(String host) {
        this.host = host;
        return this;
    }

    /**
     * Set the port and return this instance for chaining.
     * <p>
     * @param port The port.
     * @return This instance.
     */
    public DriverUri withPort(Integer port) {
        this.port = port;
        return this;
    }

    /**
     * Set the path and return this instance for chaining.
     * <p>
     * @param path The path.
     * @return This instance.
     */
    public DriverUri withPath(String path) {
        this.path = path;
        return this;
    }

    /**
     * Return the URI string.
     */
    public String toUriString() {
        String uri = protocol;
        if (isNotEmpty(dialect)) {
            uri += ":" + dialect;
        }
        uri += "://";

        if (isNotEmpty(username)) {
            uri += username;
            if (isNotEmpty(password)) {
                uri += ":" + password;
            }
            uri += "@";
        }

        uri += (isNotEmpty(host)) ? host : "localhost";
        if (port > 0) {
            uri += ":" + port;
        }

        if (isNotEmpty(path)) {
            uri += "/" + path;
        }
        return uri;
    }

    /**
     * Obfuscate the password part in the given uri, by replacing the password with 5 asterisks.
     * <p>
     * @param uri The uri.
     * @return The obfuscated uri.
     */
    public static String obfuscateUri(String uri) {
        return (StringUtils.isNotEmpty(uri))
                ? uri.replaceFirst(":[^:]*@", ":*****@")
                : uri;
    }

}
