package nl.mirila.drivers.core;

/**
 * The DriverInformation contains information about the current status of the driver.
 * <p>
 * We expect at least the status. Each {@link Driver} should use this DriverStatus, or use an child class,
 * to add additional information.
 */
public class DriverInformation {

    private DriverStatus status;
    private String statusMessage;

    /**
     * Initialize a new instance.
     */
    public DriverInformation(DriverStatus status) {
        this.status = status;
        statusMessage = "";
    }

    /**
     * Initialize a new instance, with the given parameters.
     */
    public DriverInformation(DriverStatus status, String statusMessage) {
        this.status = status;
        this.statusMessage = statusMessage;
    }

    /**
     * Return the status.
     */
    public DriverStatus getStatus() {
        return status;
    }

    /**
     * Set the status.
     */
    public void setStatus(DriverStatus status) {
        this.status = status;
    }

    /**
     * Return the status message.
     */
    public String getStatusMessage() {
        return statusMessage;
    }

    /**
     * Set the status message.
     */
    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

}
