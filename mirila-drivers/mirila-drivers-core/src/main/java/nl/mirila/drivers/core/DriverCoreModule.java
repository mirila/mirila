package nl.mirila.drivers.core;

import com.google.inject.AbstractModule;
import com.google.inject.multibindings.Multibinder;

public class DriverCoreModule extends AbstractModule {

    @Override
    protected void configure() {
        // Ensure the Driver.class is bound to a Set.
        Multibinder.newSetBinder(binder(), Driver.class);
    }

}
