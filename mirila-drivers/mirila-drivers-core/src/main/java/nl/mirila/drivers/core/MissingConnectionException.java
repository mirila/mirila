package nl.mirila.drivers.core;

import nl.mirila.core.exceptions.MirilaException;

public class MissingConnectionException extends MirilaException {

    public MissingConnectionException(String serviceName) {
        super(String.format("The connection to %s is invalid or missing.", serviceName));
    }

}
