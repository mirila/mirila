package nl.mirila.drivers.core.drivers;

import nl.mirila.drivers.core.Driver;
import nl.mirila.drivers.core.DriverInformation;

public class FatalDriver implements Driver {

    @Override
    public String getDriverName() {
        return getClass().getSimpleName();
    }

    @Override
    public DriverInformation getDriverInformation() {
        throw new RuntimeException("Faking a fatal exception.");
    }

}
