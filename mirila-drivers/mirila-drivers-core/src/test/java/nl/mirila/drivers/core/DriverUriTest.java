package nl.mirila.drivers.core;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class DriverUriTest {

    @Test
    void testBasicUrl() {
        String uri = DriverUri.forProtocol("http").toUriString();
        assertThat(uri).isEqualTo("http://localhost");
    }

    @Test
    void testDialect() {
        String uri = DriverUri.forProtocol("jdbc")
                .withDialect("mysql")
                .toUriString();
        assertThat(uri).isEqualTo("jdbc:mysql://localhost");
    }

    @Test
    void testUsername() {
        String uri = DriverUri.forProtocol("jdbc")
                .withUsername("root")
                .toUriString();
        assertThat(uri).isEqualTo("jdbc://root@localhost");
    }

    @Test
    void testPassword() {
        String uri = DriverUri.forProtocol("jdbc")
                .withUsernameAndPassword("root", "password123")
                .toUriString();
        assertThat(uri).isEqualTo("jdbc://root:password123@localhost");
    }

    @Test
    void testHost() {
        String uri = DriverUri.forProtocol("https")
                .withHost("www.mirila.nl")
                .toUriString();
        assertThat(uri).isEqualTo("https://www.mirila.nl");
    }

    @Test
    void testPort() {
        String uri = DriverUri.forProtocol("http")
                .withPort(8080)
                .toUriString();
        assertThat(uri).isEqualTo("http://localhost:8080");
    }

    @Test
    void testPath() {
        String uri = DriverUri.forProtocol("http")
                .withPath("path")
                .toUriString();
        assertThat(uri).isEqualTo("http://localhost/path");
    }

    @Test
    void testFullUri() {
        String uri = DriverUri.forProtocol("jdbc")
                .withDialect("mysql")
                .withUsernameAndPassword("root", "password123")
                .withHost("localhost")
                .withPort(3306)
                .withPath("database")
                .toUriString();
        assertThat(uri).isEqualTo("jdbc:mysql://root:password123@localhost:3306/database");
    }


}
