package nl.mirila.drivers.core;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.multibindings.Multibinder;
import nl.mirila.drivers.core.drivers.ErrorDriver;
import nl.mirila.drivers.core.drivers.FatalDriver;
import nl.mirila.drivers.core.drivers.HealthyDriver;
import nl.mirila.drivers.core.drivers.WarningDriver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

class DriverManagerTest {

    private Map<String, DriverInformation> map;

    @BeforeEach
    void setUp() {
        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                Multibinder<Driver> driversBinding = Multibinder.newSetBinder(binder(), Driver.class);
                driversBinding.addBinding().to(HealthyDriver.class);
                driversBinding.addBinding().to(WarningDriver.class);
                driversBinding.addBinding().to(ErrorDriver.class);
                driversBinding.addBinding().to(FatalDriver.class);
            }
        });
        DriverManager manager = injector.getInstance(DriverManager.class);
        map = manager.getDriversInformation();
    }

    @Test
    void testInformation() {
        assertThat(map).containsOnlyKeys("HealthyDriver", "WarningDriver", "ErrorDriver", "FatalDriver");
    }

    @Test
    void testHealthy() {
        DriverInformation info = map.get("HealthyDriver");
        assertThat(info.getStatus()).isEqualTo(DriverStatus.HEALTHY);
        assertThat(info.getStatusMessage()).isEmpty();
    }

    @Test
    void testWarning() {
        DriverInformation info = map.get("WarningDriver");
        assertThat(info.getStatus()).isEqualTo(DriverStatus.WARNING);
        assertThat(info.getStatusMessage()).isEqualTo("This is a warning.");
    }

    @Test
    void testError() {
        DriverInformation info = map.get("ErrorDriver");
        assertThat(info.getStatus()).isEqualTo(DriverStatus.ERROR);
        assertThat(info.getStatusMessage()).isEqualTo("This is an error.");
    }

    @Test
    void testFatal() {
        DriverInformation info = map.get("FatalDriver");
        assertThat(info.getStatus()).isEqualTo(DriverStatus.FATAL);
        assertThat(info.getStatusMessage()).isEqualTo("The driver failed. See logging for details.");
    }

}
