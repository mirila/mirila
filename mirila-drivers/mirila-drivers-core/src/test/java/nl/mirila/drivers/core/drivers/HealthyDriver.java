package nl.mirila.drivers.core.drivers;

import nl.mirila.drivers.core.Driver;
import nl.mirila.drivers.core.DriverInformation;
import nl.mirila.drivers.core.DriverStatus;

public class HealthyDriver implements Driver {

    @Override
    public String getDriverName() {
        return getClass().getSimpleName();
    }

    @Override
    public DriverInformation getDriverInformation() {
        return new DriverInformation(DriverStatus.HEALTHY);
    }

}
