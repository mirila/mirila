package nl.mirila.audit.core;

import com.google.inject.AbstractModule;
import com.google.inject.multibindings.OptionalBinder;

/**
 * The {@link AuditCoreModule} adds support of audit logs to the application.
 */
public class AuditCoreModule extends AbstractModule {

    /**
     * {@inheritDoc}
     */
    @Override
    protected void configure() {
        OptionalBinder.newOptionalBinder(binder(), AuditService.class)
                .setDefault()
                .to(NoAuditService.class);

        bind(AuditRestFilter.class);
    }

}
