package nl.mirila.audit.core.log4j2;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import nl.mirila.audit.core.AuditRecord;
import nl.mirila.audit.core.AuditService;
import nl.mirila.model.core.jackson.ObjectMapperProvider;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Optional;

/**
 * The {@link Log4j2AuditService} outputs each {@link AuditRecord} to a log.
 */
public class Log4j2AuditService implements AuditService {

    private static final Logger logger = LogManager.getLogger(Log4j2AuditService.class);

    private final ObjectMapper mapper;

    /**
     * Initialize a new instance.
     */
    @Inject
    public Log4j2AuditService(ObjectMapperProvider provider) {
        this.mapper = provider.get();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void log(AuditRecord auditRecord) {
        getJson(auditRecord).ifPresent((json) -> {
            logger.info("Audit log -> {}", json);
        });
    }

    /**
     * Return the JSON representation for the given {@link AuditRecord}, wrapped in an {@link Optional}.
     * <p>
     * @param auditRecord The audit record.
     * @return The JSON string.
     */
    protected Optional<String> getJson(AuditRecord auditRecord) {
        try {
            return Optional.of(mapper.writeValueAsString(auditRecord));
        } catch (JsonProcessingException e) {
            return Optional.empty();
        }
    }

}
