package nl.mirila.audit.core;

/**
 * The {@link NoAuditService} is a dummy {@link AuditService} that does not actually log anything. This is useful when
 * needing to bind a non-functional audit service.
 */
public class NoAuditService implements AuditService {

    /**
     * Doesn't actually log anything.
     */
    @Override
    public void log(AuditRecord auditRecord) {
        // No-op
    }

}
