package nl.mirila.audit.core;

/**
 * The {@link AuditService} is responsible for logging a {@link AuditRecord}.
 */
public interface AuditService {

    /**
     * Log the given {@link AuditRecord}.
     * <p>
     * @param auditRecord The audit record.
     */
    void log(AuditRecord auditRecord);

}
