package nl.mirila.audit.core;

import com.google.inject.Inject;
import com.google.inject.Injector;
import nl.mirila.core.settings.ApplicationSettings;
import nl.mirila.model.core.references.Key;
import nl.mirila.model.core.references.Model;
import nl.mirila.model.management.descriptor.ModelDescriptor;
import nl.mirila.model.management.descriptor.ModelDescriptors;
import nl.mirila.rest.annotations.Protected;
import nl.mirila.rest.result.RestResult;
import nl.mirila.rest.result.RestResults;
import nl.mirila.security.auth.core.contexts.SecurityContext;
import nl.mirila.security.auth.core.contexts.SecurityContextProvider;

import javax.annotation.Priority;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;
import java.lang.reflect.Method;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * The {@link AuditRestFilter} audit logs all activities on {@link Protected} endpoints.
 */
@Provider
@Priority((Priorities.ENTITY_CODER + Priorities.USER) / 2)
public class AuditRestFilter implements ContainerResponseFilter {

    private final Injector injector;
    private final ModelDescriptors descriptors;

    @Context
    private HttpServletRequest servletRequest;

    @Context
    private ResourceInfo resourceInfo;

    /**
     * Initialize a new instance.
     */
    @Inject
    public AuditRestFilter(Injector injector) {
        this.injector = injector;
        descriptors = injector.getInstance(ModelDescriptors.class);
    }

    /**
     * {@inheritDoc}
     * <p>
     * This filter checks whether this request/response should be included in the audit log. If so, the
     * {@link AuditRestFilter#logActivity(ContainerResponseContext)} is called.
     */
    @Override
    public void filter(ContainerRequestContext containerRequestContext,
                       ContainerResponseContext containerResponseContext) {
        Method method = resourceInfo.getResourceMethod();
        if ((method != null) && (method.getAnnotation(Protected.class) != null)) {
            // We only log protected endpoints, as that data needs protection.
            logActivity(containerResponseContext);
        }
    }

    /**
     * Log the request and details of the response on the current endpoint in an {@link AuditRecord}.
     * <p>
     * @param containerResponseContext The container that wraps the response object.
     */
    private void logActivity(ContainerResponseContext containerResponseContext) {
        AuditService service = injector.getInstance(AuditService.class);
        ApplicationSettings settings = injector.getInstance(ApplicationSettings.class);
        SecurityContextProvider contextProvider = injector.getInstance(SecurityContextProvider.class);
        SecurityContext context = contextProvider.get();

        AuditRecord record = new AuditRecord();
        record.setAppId(settings.getAppName());
        record.setRealm(context.getRealm());
        record.setCredentialId(context.getCredentialId());
        record.setIdentity(context.getCredentialName().orElse(null));

        record.setSubjectId(context.getCredentialId());
        record.setSubjectName(context.getSubjectName().orElse(null));

        record.setRequestPath(servletRequest.getRequestURI());
        record.setRequestQuery(servletRequest.getQueryString());
        record.setRequestMethod(servletRequest.getMethod());
        record.setIpAddress(servletRequest.getRemoteAddr());
        record.setServerName(servletRequest.getServerName());

        record.setResponseStatus(containerResponseContext.getStatus());
        record.setResponseSummary("");
        processEntity(record, containerResponseContext.getEntity());

        service.log(record);
    }

    /**
     * Process the given object and store audit information in the given {@link AuditRecord}.
     * <p>
     * Currently, we only process information of {@link RestResult} and {@link RestResults}.
     * <p>
     * @param record The record that will contain the audit information.
     * @param object The object that contains the resulting entity.
     * @param <M> The type of the expected model, if applicable.
     */
    @SuppressWarnings("unchecked")
    private <M extends Model> void processEntity(AuditRecord record, Object object) {
        if (object instanceof RestResult result) {
            processRestResult(record, result);
        } else if (object instanceof RestResults results) {
            processRestResults(record, results);
        }
    }

    /**
     * Process the given {@link RestResult} and store audit information in the given {@link AuditRecord}.
     * <p>
     * @param record The record that will contain the audit information.
     * @param result The result that contains an instance of the model.
     * @param <M> The type of the model.
     */
    private <M extends Model> void processRestResult(AuditRecord record, RestResult<M> result) {
        int statusCode = result.getStatusCode();
        if ((statusCode >= 200) && (statusCode < 300)) {
            if (result.get().isEmpty()) {
                return;
            }
            if (result.get().isPresent()) {
                @SuppressWarnings("unchecked")
                M entity = (M) result.get().get();
                Key<M> key = getEntityKey(entity);
                record.setResponseSummary(key.toString());
            } else {
                record.setResponseSummary("");
            }
        } else {
            record.setResponseSummary(result.getStatusMessage());
        }
    }

    /**
     * Process the given {@link RestResult} and store audit information in the given {@link AuditRecord}.
     * <p>
     * @param record The record that will contain the audit information.
     * @param results The list of results that contains instances of the model.
     * @param <M> The type of the model.
     */
    private <M extends Model> void processRestResults(AuditRecord record, RestResults<?> results) {
        int statusCode = results.getStatusCode();
        if ((statusCode >= 200) && (statusCode < 300)) {
            @SuppressWarnings("unchecked")
            String summary = results.getData().stream()
                    .filter(Objects::nonNull)
                    .map((entity) -> getEntityKey((M) entity))
                    .map(Key::toString)
                    .collect(Collectors.joining("⁙"));
            record.setResponseSummary(summary);
        } else {
            record.setResponseSummary(results.getStatusMessage());
        }
    }

    /**
     * Return the {@link Key} of the given entity.
     * <p>
     * @param entity The entity.
     * @param <M> The type of the entity.
     * @return The related key.
     */
    @SuppressWarnings("unchecked")
    private <M extends Model> Key<M> getEntityKey(M entity) {
        ModelDescriptor<M> descriptor = (ModelDescriptor<M>) descriptors.getDescriptorForModel(entity.getClass());
        return descriptor.getPrimaryKeyForEntity(entity);
    }

}
