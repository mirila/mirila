package nl.mirila.audit.core;

import com.fasterxml.jackson.annotation.JsonFormat;
import nl.mirila.core.datatype.Id;

import java.time.Instant;

/**
 * The {@link AuditRecord} represents a single action of an account.
 */
public class AuditRecord {

    private Instant timestamp;
    private String appId;

    private String realm;
    private Id credentialId;
    private String identity;
    private Id subjectId;
    private String subjectName;

    private String ipAddress;
    private String serverName;
    private String requestPath;
    private String requestQuery;
    private String requestMethod;

    private int responseStatus;
    private String responseSummary;

    /**
     * Initialize a new instance.
     */
    public AuditRecord() {
        timestamp = Instant.now();
    }

    /**
     * Return the timestamp.
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss'Z'", timezone = "UTC")
    public Instant getTimestamp() {
        return timestamp;
    }

    /**
     * Set the given timestamp.
     */
    public void setTimestamp(Instant timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * Return the appId.
     */
    public String getAppId() {
        return appId;
    }

    /**
     * Set the given appId.
     */
    public void setAppId(String appId) {
        this.appId = appId;
    }

    /**
     * Return the realm.
     */
    public String getRealm() {
        return realm;
    }

    /**
     * Set the given realm.
     */
    public void setRealm(String realm) {
        this.realm = realm;
    }

    /**
     * Return the credentialId.
     */
    public Id getCredentialId() {
        return credentialId;
    }

    /**
     * Set the given credentialId.
     */
    public void setCredentialId(Id credentialId) {
        this.credentialId = credentialId;
    }

    /**
     * Return the identity.
     */
    public String getIdentity() {
        return identity;
    }

    /**
     * Set the given identity.
     */
    public void setIdentity(String identity) {
        this.identity = identity;
    }

    /**
     * Return the server name.
     */
    public String getServerName() {
        return serverName;
    }

    /**
     * Set server name.
     **/
    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    /**
     * Return the subjectId.
     */
    public Id getSubjectId() {
        return subjectId;
    }

    /**
     * Set the given subjectId.
     */
    public void setSubjectId(Id subjectId) {
        this.subjectId = subjectId;
    }

    /**
     * Return the subjectName.
     */
    public String getSubjectName() {
        return subjectName;
    }

    /**
     * Set the given subjectName.
     */
    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    /**
     * Return the ipAddress.
     */
    public String getIpAddress() {
        return ipAddress;
    }

    /**
     * Set the given ipAddress.
     */
    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    /**
     * Return the requestPath.
     */
    public String getRequestPath() {
        return requestPath;
    }

    /**
     * Set the given requestPath.
     */
    public void setRequestPath(String requestPath) {
        this.requestPath = requestPath;
    }

    /**
     * Return the requestQuery.
     */
    public String getRequestQuery() {
        return requestQuery;
    }

    /**
     * Set the given requestQuery.
     */
    public void setRequestQuery(String requestQuery) {
        this.requestQuery = requestQuery;
    }

    /**
     * Return the requestMethod.
     */
    public String getRequestMethod() {
        return requestMethod;
    }

    /**
     * Set the given requestMethod.
     */
    public void setRequestMethod(String requestMethod) {
        this.requestMethod = requestMethod;
    }

    /**
     * Return the responseStatus.
     */
    public int getResponseStatus() {
        return responseStatus;
    }

    /**
     * Set the given responseStatus.
     */
    public void setResponseStatus(int responseStatus) {
        this.responseStatus = responseStatus;
    }

    /**
     * Return the responseSummary.
     */
    public String getResponseSummary() {
        return responseSummary;
    }

    /**
     * Set the given responseSummary.
     */
    public void setResponseSummary(String responseSummary) {
        this.responseSummary = responseSummary;
    }

    @Override
    public String toString() {
        return "{ timestamp=" + timestamp +
                ", appId='" + appId + '\'' +
                ", realm='" + realm + '\'' +
                ", credentialId=" + credentialId +
                ", identity='" + identity + '\'' +
                ", subjectId=" + subjectId +
                ", subjectName='" + subjectName + '\'' +
                ", ipAddress='" + ipAddress + '\'' +
                ", requestPath='" + requestPath + '\'' +
                ", requestQuery='" + requestQuery + '\'' +
                ", requestMethod='" + requestMethod + '\'' +
                ", responseStatus=" + responseStatus +
                ", responseSummary='" + responseSummary + '\'' +
                '}';
    }

}
