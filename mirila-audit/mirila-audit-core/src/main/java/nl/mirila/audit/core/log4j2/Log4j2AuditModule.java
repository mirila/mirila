package nl.mirila.audit.core.log4j2;

import com.google.inject.AbstractModule;
import com.google.inject.multibindings.OptionalBinder;
import nl.mirila.audit.core.AuditCoreModule;
import nl.mirila.audit.core.AuditService;

/**
 * The {@link Log4j2AuditModule} adds support of audit logging via Log4j2 to the application.
 */
public class Log4j2AuditModule extends AbstractModule {

    @Override
    protected void configure() {
        install(new AuditCoreModule());

        OptionalBinder.newOptionalBinder(binder(), AuditService.class)
                .setBinding()
                .to(Log4j2AuditService.class);
    }

}
