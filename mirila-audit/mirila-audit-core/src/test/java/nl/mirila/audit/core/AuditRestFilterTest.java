package nl.mirila.audit.core;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Scopes;
import com.google.inject.multibindings.Multibinder;
import nl.mirila.core.settings.modules.KeyValuesSettingsModule;
import nl.mirila.dbs.fakedb.FakeDbEntityManager;
import nl.mirila.model.management.descriptor.ModelsPackageNames;
import nl.mirila.model.management.managers.EntityPersistenceManager;
import nl.mirila.model.management.module.ModelManagementModule;
import nl.mirila.security.auth.core.contexts.GuestSecurityContext;
import nl.mirila.security.auth.core.contexts.SecurityContext;
import org.jboss.resteasy.plugins.guice.RequestScoped;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static nl.mirila.core.settings.ApplicationSettings.KEY_REALM;
import static org.assertj.core.api.Assertions.assertThatNoException;

class AuditRestFilterTest {

    @Test
    void testConstruction() {
        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                Map<String, String> settings = new HashMap<>();
                settings.put(KEY_REALM, "test-realm");
                install(new KeyValuesSettingsModule(settings));
                install(new ModelManagementModule());
                install(new AuditCoreModule());
                install(new ModelManagementModule());

                Multibinder<String> binder = Multibinder.newSetBinder(binder(), String.class, ModelsPackageNames.class);
                binder.addBinding().toInstance(this.getClass().getPackageName());

                bind(SecurityContext.class).toInstance(new GuestSecurityContext("test-realm", "en_EN"));
                bind(EntityPersistenceManager.class).to(FakeDbEntityManager.class);
                bind(FakeDbEntityManager.class).asEagerSingleton();
                bindScope(RequestScoped.class, Scopes.SINGLETON);
            }
        });
        assertThatNoException().isThrownBy(() -> injector.getInstance(AuditRestFilter.class));
    }

}
