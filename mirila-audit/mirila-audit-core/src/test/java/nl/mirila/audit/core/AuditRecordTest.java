package nl.mirila.audit.core;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import nl.mirila.core.datatype.Id;
import nl.mirila.model.core.ModelCoreModule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Instant;

import static org.assertj.core.api.Assertions.assertThat;

class AuditRecordTest {

    private Instant beforeCreation;
    private Instant afterCreation;
    private AuditRecord record;

    @BeforeEach
    void setUp() {
        beforeCreation = Instant.now();
        record = new AuditRecord();
        record.setAppId("myAppId");
        record.setRealm("my-realm");
        record.setCredentialId(Id.of("123"));
        record.setIdentity("test-account-123");
        record.setSubjectId(Id.of(456));
        record.setSubjectName("Test Account");
        record.setRequestQuery("/users");
        record.setRequestMethod("GET");
        record.setIpAddress("127.0.0.1");
        record.setServerName("localhost");
        record.setResponseStatus(200);
        record.setResponseSummary("{\"userIds\": [1, 2, 3, 4]}");
        record.setTimestamp(Instant.now());
        afterCreation = Instant.now();
    }

    @Test
    void testGetAppId() {
        assertThat(record.getAppId()).isEqualTo("myAppId");
    }

    @Test
    void testGetRealm() {
        assertThat(record.getRealm()).isEqualTo("my-realm");
    }

    @Test
    void testGetAccountId() {
        assertThat(record.getCredentialId().asInt()).isEqualTo(123);
    }

    @Test
    void testGetIdentity() {
        assertThat(record.getIdentity()).isEqualTo("test-account-123");
    }

    @Test
    void testGetSubjectId() {
        assertThat(record.getSubjectId().asInt()).isEqualTo(456);
    }

    @Test
    void testGetSubjectName() {
        assertThat(record.getSubjectName()).isEqualTo("Test Account");
    }

    @Test
    void testGetTimestamp() {
        assertThat(record.getTimestamp())
                .isAfterOrEqualTo(beforeCreation)
                .isBeforeOrEqualTo(afterCreation);
    }

    @Test
    void testGetIpAddress() {
        assertThat(record.getIpAddress()).isEqualTo("127.0.0.1");
    }

    @Test
    void testGetServerName() {
        assertThat(record.getServerName()).isEqualTo("localhost");
    }

    @Test
    void testGetRequestQuery() {
        assertThat(record.getRequestQuery()).isEqualTo("/users");
    }

    @Test
    void testGetRequestMethod() {
        assertThat(record.getRequestMethod()).isEqualTo("GET");
    }

    @Test
    void testGetResponseStatus() {
        assertThat(record.getResponseStatus()).isEqualTo(200);
    }

    @Test
    void testGetResponseSummary() {
        assertThat(record.getResponseSummary()).isEqualTo("{\"userIds\": [1, 2, 3, 4]}");
    }

    @Test
    void testJson() throws JsonProcessingException {
        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                install(new ModelCoreModule());
            }
        });
        ObjectMapper mapper = injector.getInstance(ObjectMapper.class);
        String json = mapper.writeValueAsString(record);
        System.out.println(json);
        assertThat(json)
                .startsWith("{")
                .endsWith("}");
    }

}
