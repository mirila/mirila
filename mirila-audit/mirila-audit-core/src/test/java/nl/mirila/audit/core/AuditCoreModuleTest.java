package nl.mirila.audit.core;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import nl.mirila.audit.core.log4j2.Log4j2AuditModule;
import nl.mirila.audit.core.log4j2.Log4j2AuditService;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class AuditCoreModuleTest {

    @Test
    void testConfigureDefaultService() {
        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                install(new Log4j2AuditModule());
            }
        });
        AuditService service = injector.getInstance(AuditService.class);
        assertThat(service).isInstanceOf(Log4j2AuditService.class);
    }

    @Test
    void testConfigureNoAuditService() {
        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                install(new AuditCoreModule());
            }
        });
        AuditService service = injector.getInstance(AuditService.class);
        assertThat(service).isInstanceOf(NoAuditService.class);
    }

}
