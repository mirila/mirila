package nl.mirila.example.war;

import com.google.inject.AbstractModule;
import nl.mirila.app.web.WebAppModule;
import nl.mirila.core.settings.modules.KeyValuesSettingsModule;
import nl.mirila.dbs.fakedb.FakeDbModule;
import nl.mirila.dbs.sql.SqlModule;
import nl.mirila.example.api.modules.ExampleModule;
import nl.mirila.example.pojos.ExamplePojosModule;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

import static nl.mirila.core.settings.ApplicationSettings.KEY_REALM;
import static nl.mirila.security.auth.jwt.JwtSettings.KEY_JWT_SECRET;

public class ExampleApiModule extends AbstractModule {

    private static final Logger logger = LogManager.getLogger(ExampleApiModule.class);

    public ExampleApiModule() {
        logger.info("Starting ExampleApiModule");
    }

    @Override
    protected void configure() {
        Map<String, String> settings = new HashMap<>();
        settings.put(KEY_REALM, "example-app");
        settings.put(KEY_REALM, "Example Application");
        settings.put(KEY_JWT_SECRET, "This is my example secret.");
        install(new KeyValuesSettingsModule(settings));

        install(new WebAppModule());

        install(new ExamplePojosModule());
        install(new ExampleModule());

        install(new SqlModule());
        install(new FakeDbModule());
    }

}
