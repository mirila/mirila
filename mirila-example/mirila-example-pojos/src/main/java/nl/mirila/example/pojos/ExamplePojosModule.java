package nl.mirila.example.pojos;

import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import com.google.inject.multibindings.Multibinder;
import nl.mirila.model.management.descriptor.ModelsPackageNames;
import nl.mirila.model.management.managers.EntityManager;
import nl.mirila.model.management.module.ModelManagementModule;

public class ExamplePojosModule extends AbstractModule {

    @Override
    protected void configure() {
        install(new ModelManagementModule());

        bind(new TypeLiteral<EntityManager<User>>(){});

        Multibinder<String> binder = Multibinder.newSetBinder(binder(), String.class, ModelsPackageNames.class);
        binder.addBinding().toInstance(this.getClass().getPackageName());
    }

}
