package nl.mirila.example.api.modules;

import com.google.inject.AbstractModule;
import com.google.inject.multibindings.Multibinder;
import nl.mirila.core.i18n.LocalizedStrings;
import nl.mirila.example.api.i18n.LocalizedResourceStrings;
import nl.mirila.example.api.i18n.ResourceNames;
import nl.mirila.example.api.resources.StatusResource;
import nl.mirila.example.api.resources.UserResource;
import nl.mirila.example.api.security.ExampleAuthenticator;
import nl.mirila.security.auth.core.claims.Authenticator;

public class ExampleModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(Authenticator.class).to(ExampleAuthenticator.class);

        bind(UserResource.class);
        bind(StatusResource.class);

        bind(LocalizedStrings.class).to(LocalizedResourceStrings.class);
        Multibinder<String> resourceNameBinder = Multibinder.newSetBinder(binder(), String.class, ResourceNames.class);
        resourceNameBinder.addBinding().toInstance("FirstExamples");
        resourceNameBinder.addBinding().toInstance("SecondExamples");
    }

}
