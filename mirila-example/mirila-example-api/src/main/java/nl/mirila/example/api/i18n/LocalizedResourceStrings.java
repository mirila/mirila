package nl.mirila.example.api.i18n;

import com.google.inject.Inject;
import nl.mirila.core.i18n.LocalizedStrings;
import nl.mirila.security.auth.core.contexts.SecurityContext;
import org.apache.commons.lang3.LocaleUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.*;

public class LocalizedResourceStrings implements LocalizedStrings {

    private final List<ResourceBundle> bundles;

    @Inject
    public LocalizedResourceStrings(SecurityContext securityContext, @ResourceNames Set<String> resourceNames) {
        String localeId = securityContext.getLocaleId();
        final Locale locale = LocaleUtils.toLocale(localeId.replaceAll("-", "_"));
        bundles = resourceNames.stream()
                .map((resourceName) -> ResourceBundle.getBundle(resourceName, locale))
                .filter(Objects::nonNull)
                .toList();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<String> get(String key) {
        return bundles.stream()
                .filter((bundle) -> bundle.containsKey(key))
                .map((bundle) -> bundle.getString(key))
                .filter(StringUtils::isNotEmpty)
                .findFirst();
    }

}
