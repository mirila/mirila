package nl.mirila.example.api.security;

import nl.mirila.security.auth.core.authorization.Credentials;
import nl.mirila.security.auth.core.claims.Authenticator;
import nl.mirila.security.auth.core.claims.Claims;
import nl.mirila.security.auth.core.claims.ClaimsBuilder;
import nl.mirila.security.auth.core.exceptions.AbstractAuthenticationFailedException;
import nl.mirila.security.auth.core.exceptions.InvalidCredentialException;

public class ExampleAuthenticator implements Authenticator {

    /**
     * Authenticate a subject, using the given {@link Credentials}, and return the related {@link Claims}.
     * <p>
     * @param credentials The credentials.
     * @return The claims that are related to these credentials.
     * <p>
     * @throws InvalidCredentialException Will be thrown if the authentication fails.
     */
    @Override
    public Claims authenticate(Credentials credentials) throws AbstractAuthenticationFailedException {
        if (!isValid(credentials)) {
            throw new InvalidCredentialException();
        }
        return ClaimsBuilder.forRealm("test")
                .asSuperUser()
                .withIssuer("nl.mirila.example-api")
                .withLocaleId("nl_NL")
                .withSubject(credentials.getIdentity())
                .withGroupIds("1", "123")
                .withSubjectName("Mirila Test account")
                .withSubjectId("34")
                .getClaims();
    }

    /**
     * Return true if the given {@link Credentials} are valid.
     */
    private boolean isValid(Credentials credentials) {
        String hash = String.format("%s:%s", credentials.getIdentity(), credentials.getRealm());
        return credentials.getSecret().equals(hash);
    }

}
