package nl.mirila.example.api.resources;

import com.google.inject.Inject;
import nl.mirila.drivers.core.DriverInformation;
import nl.mirila.drivers.core.DriverManager;
import nl.mirila.rest.annotations.Public;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.Map;

import static nl.mirila.rest.result.RestConstants.JSON_WITH_UTF8;

@Path("/status")
public class StatusResource {

    private final DriverManager driverManager;

    @Inject
    public StatusResource(DriverManager driverManager) {
        this.driverManager = driverManager;
    }

    @GET
    @Path("/drivers")
    @Produces(JSON_WITH_UTF8)
    @Public
    public Map<String, DriverInformation> getDriversStatus() {
        return driverManager.getDriversInformation();
    }

}
