package nl.mirila.example.api.resources;

import com.google.inject.Inject;
import nl.mirila.example.pojos.User;
import nl.mirila.model.core.references.Key;
import nl.mirila.model.management.managers.EntityManager;
import nl.mirila.model.management.query.QueryParameters;
import nl.mirila.model.management.results.Result;
import nl.mirila.model.management.results.Results;
import nl.mirila.rest.annotations.Public;
import nl.mirila.rest.result.RestResult;
import nl.mirila.rest.result.RestResults;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import static nl.mirila.rest.result.RestConstants.JSON_WITH_UTF8;

@Path("/users")
public class UserResource {

    private static boolean isLoaded = false;

    private final EntityManager<User> userManager;

    @Inject
    public UserResource(EntityManager<User> userManager) {
        this.userManager = userManager;

        if (!isLoaded) {
            User user1 = new User();
            user1.setFirstName("Obi-wan");
            user1.setLastName("Kenobi");
            userManager.create(user1);

            User user2 = new User();
            user2.setFirstName("Luke");
            user2.setLastName("Skywalker");
            userManager.create(user2);

            User user3 = new User();
            user3.setFirstName("Leia");
            user3.setLastName("Organa");
            userManager.create(user3);
            isLoaded = true;
        }
    }

    @GET
    @Path("/")
    @Produces(JSON_WITH_UTF8)
    @Public
    public RestResults<User> getAll() {
        Results<User> results = QueryParameters.on(User.class).runWith(userManager);
        return RestResults.with(results.get());
    }

    @GET
    @Path("/{userId}")
    @Produces(JSON_WITH_UTF8)
    @Public
    public RestResult<User> getUser(@PathParam("userId") int userId) {
        Result<User> result = userManager.read(Key.of(User.class, userId));
        return RestResult.with(result);
    }



}
