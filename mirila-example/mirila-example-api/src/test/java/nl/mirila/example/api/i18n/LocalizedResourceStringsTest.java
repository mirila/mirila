package nl.mirila.example.api.i18n;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.multibindings.Multibinder;
import nl.mirila.core.datatype.Id;
import nl.mirila.core.i18n.LocalizedStrings;
import nl.mirila.security.auth.core.contexts.SecurityContext;
import nl.mirila.security.auth.core.contexts.ServiceSecurityContext;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class LocalizedResourceStringsTest {

    @SuppressWarnings({"OptionalGetWithoutIsPresent", "SpellCheckingInspection"})
    @Test
    void testGet() {
        ServiceSecurityContext context = new ServiceSecurityContext("realm", "nl_NL", Id.of("1"), "service-id", "name");
        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                //install(new ExampleModule());
                bind(SecurityContext.class).toInstance(context);
                bind(LocalizedStrings.class).to(LocalizedResourceStrings.class);
                Multibinder<String> resourceNameBinder = Multibinder.newSetBinder(binder(),
                                                                                  String.class,
                                                                                  ResourceNames.class);
                resourceNameBinder.addBinding().toInstance("FirstExamples");
                resourceNameBinder.addBinding().toInstance("SecondExamples");
            }
        });
        LocalizedStrings strings = injector.getInstance(LocalizedStrings.class);

        assertThat(strings.get("example")).contains("Voorbeeld 1");
        assertThat(strings.get("example1")).contains("Voorbeeld 1");
        assertThat(strings.get("example2")).contains("Voorbeeld 2");
    }

}
