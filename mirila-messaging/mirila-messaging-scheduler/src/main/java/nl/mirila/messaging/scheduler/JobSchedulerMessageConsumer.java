package nl.mirila.messaging.scheduler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.inject.Inject;
import com.google.inject.Injector;
import nl.mirila.messaging.core.Message;
import nl.mirila.messaging.core.MessageConsumer;
import nl.mirila.messaging.core.MessageMapper;
import nl.mirila.messaging.core.MessagePayload;
import nl.mirila.scheduling.core.HandlesJobs;
import nl.mirila.scheduling.core.Job;
import nl.mirila.scheduling.core.JobHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static nl.mirila.messaging.scheduler.JobSchedulerMessageProducer.MESSAGE_BUS_GROUP;

@HandlesJobs(group = MESSAGE_BUS_GROUP)
public class JobSchedulerMessageConsumer extends MessageConsumer implements JobHandler {

    private static final Logger logger = LogManager.getLogger(JobSchedulerMessageConsumer.class);
    private final MessageMapper mapper;

    /**
     * Initialize a new instance.
     */
    @Inject
    protected JobSchedulerMessageConsumer(Injector injector, MessageMapper mapper) {
        super(injector);
        this.mapper = mapper;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void handle(Job job) {
        if (job.getData().isEmpty()) {
            logger.warn("Trying to handle a job that does not contain data.");
            return;
        }
        try {
            Message<? extends MessagePayload> message = mapper.fromJson(job.getData().get());
            consume(message);
        } catch (JsonProcessingException e) {
            logger.error("Error while convert the JSON to a message. Reason: {}", e.getMessage(), e);
        }
    }

}
