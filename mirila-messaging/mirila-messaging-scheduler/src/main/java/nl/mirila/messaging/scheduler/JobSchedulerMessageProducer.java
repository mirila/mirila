package nl.mirila.messaging.scheduler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.inject.Inject;
import nl.mirila.core.settings.ApplicationSettings;
import nl.mirila.messaging.core.*;
import nl.mirila.scheduling.core.Job;
import nl.mirila.scheduling.core.JobScheduler;
import nl.mirila.security.auth.core.contexts.SecurityContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * The {@link JobSchedulerMessageProducer} uses a {@link JobScheduler} as a messages bus system. Fitting most messaging
 * systems, this class does not actually schedule a job at a specific time, with a certain recurrence pattern, but uses
 * its infrastructure for the same purpose, as scheduling and messaging, though different setups, do have similarities.
 */
public class JobSchedulerMessageProducer implements MessageProducer {

    private static final Logger logger = LogManager.getLogger(JobSchedulerMessageProducer.class);

    static final String MESSAGE_BUS_GROUP = "message-bus";

    private final SecurityContext securityContext;
    private final ApplicationSettings applicationSettings;
    private final JobScheduler scheduler;
    private final MessageMapper mapper;

    /**
     * Initialize a new instance.
     */
    @Inject
    public JobSchedulerMessageProducer(SecurityContext securityContext,
                                       ApplicationSettings applicationSettings,
                                       JobScheduler scheduler,
                                       MessageMapper mapper) {
        this.securityContext = securityContext;
        this.applicationSettings = applicationSettings;
        this.scheduler = scheduler;
        this.mapper = mapper;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SecurityContext getSecurityContext() {
        return securityContext;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ApplicationSettings getApplicationSettings() {
        return applicationSettings;
    }

    /**
     * Schedules the given {@link Message} as a job into the {@link JobScheduler}.
     *
     * The group of the job will be set to {@link JobSchedulerMessageProducer#MESSAGE_BUS_GROUP}, and the name of the
     * job will be set to the name of the {@link MessageQueue}, suffixed with the @ and realm, if a realm is specified
     * in the queue.
     *
     * @param message The message.
     * @param queue The queue.
     */
    @Override
    public void sendMessage(Message<MessagePayload> message, MessageQueue queue) {
        String json;
        try {
            json = mapper.toJson(message);
        } catch (JsonProcessingException e) {
            logger.error("Unable convert this message payload to json. Reason: {}", e.getMessage(), e);
            return;
        }
        String name = queue.getName() + queue.getRealm().map((realm) -> "@" + realm).orElse("");
        Job job = Job.of(MESSAGE_BUS_GROUP, name).withData(json);
        scheduler.schedule(job);
    }

}
