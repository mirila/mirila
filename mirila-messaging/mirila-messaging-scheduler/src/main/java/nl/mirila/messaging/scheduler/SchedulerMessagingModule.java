package nl.mirila.messaging.scheduler;

import com.google.inject.AbstractModule;
import com.google.inject.multibindings.OptionalBinder;
import nl.mirila.messaging.core.MessageConsumer;
import nl.mirila.messaging.core.MessageProducer;

public class SchedulerMessagingModule extends AbstractModule {

    @Override
    protected void configure() {
        OptionalBinder.newOptionalBinder(binder(), MessageConsumer.class)
                .setBinding()
                .to(JobSchedulerMessageConsumer.class);

        OptionalBinder.newOptionalBinder(binder(), MessageProducer.class)
                .setBinding()
                .to(JobSchedulerMessageProducer.class);

        bind(JobSchedulerMessageConsumer.class);
    }

}
