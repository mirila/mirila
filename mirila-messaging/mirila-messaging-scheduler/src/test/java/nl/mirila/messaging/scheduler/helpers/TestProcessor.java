package nl.mirila.messaging.scheduler.helpers;

import nl.mirila.messaging.core.Message;
import nl.mirila.messaging.core.MessageProcessor;
import nl.mirila.messaging.core.MessageQueue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TestProcessor implements MessageProcessor<TestPayload> {

    private static volatile List<String> processedPayloadNames = Collections.synchronizedList(new ArrayList<>());
    private static volatile List<String> processedMessageIds = Collections.synchronizedList(new ArrayList<>());

    @Override
    public MessageQueue getQueue() {
        return MessageQueue.withName("test-queue");
    }

    @Override
    public Class<TestPayload> getPayloadClass() {
        return TestPayload.class;
    }

    @Override
    public void process(Message<TestPayload> message) {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ignore) {
        }
        processedMessageIds.add(message.getId().asString());
        processedPayloadNames.add(message.getPayload().getName());
    }

    public static List<String> getProcessedPayloadNames() {
        return processedPayloadNames;
    }

    public static List<String> getProcessedMessageIds() {
        return processedMessageIds;
    }

}
