package nl.mirila.messaging.scheduler;

import com.google.inject.*;
import com.google.inject.multibindings.Multibinder;
import nl.mirila.core.settings.modules.KeyValuesSettingsModule;
import nl.mirila.dbs.sql.SqlEntityManager;
import nl.mirila.dbs.sql.SqlModule;
import nl.mirila.messaging.core.MessagePayload;
import nl.mirila.messaging.core.MessageProcessor;
import nl.mirila.messaging.core.MessageProducer;
import nl.mirila.messaging.core.MessageQueue;
import nl.mirila.messaging.scheduler.helpers.TestPayload;
import nl.mirila.messaging.scheduler.helpers.TestProcessor;
import nl.mirila.metrics.core.MetricsCoreModule;
import nl.mirila.model.management.module.ModelManagementModule;
import nl.mirila.model.management.query.QueryParameters;
import nl.mirila.scheduling.dbs.DatabaseSchedulingModule;
import nl.mirila.scheduling.dbs.ScheduledJob;
import nl.mirila.security.auth.core.contexts.GuestSecurityContext;
import nl.mirila.security.auth.core.contexts.SecurityContext;
import org.jboss.resteasy.plugins.guice.RequestScoped;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static nl.mirila.core.settings.ApplicationSettings.KEY_APP_NAME;
import static nl.mirila.core.settings.ApplicationSettings.KEY_REALM;
import static nl.mirila.scheduling.core.SchedulerSettings.KEY_INTERVAL;
import static nl.mirila.scheduling.core.SchedulerSettings.KEY_LOG_JOB_COUNT;
import static org.assertj.core.api.Assertions.assertThat;

class JobSchedulerMessageProducerTest {

    private MessageProducer producer;
    private MessageQueue queue;
    private SqlEntityManager persistenceManager;

    @BeforeEach
    void setUp() {

        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                Map<String, String> settings = new HashMap<>();
                settings.put(KEY_APP_NAME, "Mirila");
                settings.put(KEY_REALM, "mirila");
                settings.put(KEY_INTERVAL, "1");
                settings.put(KEY_LOG_JOB_COUNT, "true");

                install(new KeyValuesSettingsModule(settings));
                install(new SchedulerMessagingModule());
                install(new SqlModule());
                install(new ModelManagementModule());
                install(new MetricsCoreModule());
                install(new DatabaseSchedulingModule());

                bind(SecurityContext.class).toInstance(new GuestSecurityContext("mirila", "en_EN"));
                bindScope(RequestScoped.class, Scopes.SINGLETON);

                Multibinder<MessageProcessor<? extends MessagePayload>> consumerBinder = Multibinder
                        .newSetBinder(binder(), new TypeLiteral<>() {});
                consumerBinder.addBinding().to(TestProcessor.class);
            }
        });

        producer = injector.getInstance(MessageProducer.class);
        queue = MessageQueue.withName("test-queue").withRealm("mirila");
        persistenceManager = injector.getInstance(SqlEntityManager.class);
        persistenceManager.runSqlStatement("truncate scheduled_jobs", List.of());
    }

    @Test
    void testMessaging() throws InterruptedException {
        producer.sendPayload(queue, new TestPayload("test1"));
        producer.sendPayload(queue, new TestPayload("test2"));
        producer.sendPayload(queue, new TestPayload("test3"));

        long count1 = persistenceManager.count(QueryParameters.on(ScheduledJob.class));
        assertThat(count1).isEqualTo(3);

        long counter = (TimeUnit.SECONDS.toMillis(10) / 10);
        while (TestProcessor.getProcessedPayloadNames().size() < 3) {
            //noinspection BusyWait
            Thread.sleep(10);
            if (counter-- <= 0) {
                throw new RuntimeException("Breaking from loop to prevent an infinite loop...");
            }
        }
        Thread.sleep(100); // Little extra sleep for the JobDispatcher to finish it's work.

        assertThat(TestProcessor.getProcessedPayloadNames())
                .containsOnly("test1", "test2", "test3");

    }

}