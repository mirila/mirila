package nl.mirila.messaging.scheduler.helpers;

import com.fasterxml.jackson.annotation.JsonProperty;
import nl.mirila.messaging.core.MessagePayload;

public class TestPayload implements MessagePayload {

    @JsonProperty("name")
    private String name;

    public TestPayload() {
    }

    public TestPayload(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
