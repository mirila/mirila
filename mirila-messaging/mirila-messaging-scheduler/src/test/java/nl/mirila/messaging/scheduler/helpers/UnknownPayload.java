package nl.mirila.messaging.scheduler.helpers;

import com.fasterxml.jackson.annotation.JsonProperty;
import nl.mirila.messaging.core.MessagePayload;

public class UnknownPayload implements MessagePayload {

    @JsonProperty
    private String name;

    public UnknownPayload() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
