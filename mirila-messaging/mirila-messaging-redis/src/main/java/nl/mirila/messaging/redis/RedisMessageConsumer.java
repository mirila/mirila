package nl.mirila.messaging.redis;

import com.google.inject.Inject;
import com.google.inject.Injector;
import io.lettuce.core.api.sync.RedisCommands;
import io.lettuce.core.pubsub.StatefulRedisPubSubConnection;
import nl.mirila.drivers.redis.MissingRedisConnectionException;
import nl.mirila.drivers.redis.RedisDriver;
import nl.mirila.messaging.core.MessageConsumer;
import nl.mirila.messaging.core.MessageMapper;

/**
 * A Redis implementation of a {@link MessageConsumer}.
 */
public class RedisMessageConsumer extends MessageConsumer {

    private final RedisDriver redisDriver;
    private final MessageMapper messageMapper;

    private final StatefulRedisPubSubConnection<String, String> pubSubConnection;
    private final RedisCommands<String, String> commands;

    /**
     * Initialize a new instance, using the given processors.
     */
    @Inject
    public RedisMessageConsumer(Injector injector,
                                RedisDriver redisDriver,
                                MessageMapper messageMapper) {
        super(injector);
        this.redisDriver = redisDriver;
        this.messageMapper = messageMapper;

        commands = redisDriver.getSyncConnection()
                .orElseThrow(MissingRedisConnectionException::new);

        pubSubConnection = redisDriver.getPubSubConnection()
                .orElseThrow(MissingRedisConnectionException::new);
    }

}
