package nl.mirila.messaging.redis;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.inject.Inject;
import io.lettuce.core.api.sync.RedisCommands;
import nl.mirila.core.settings.ApplicationSettings;
import nl.mirila.drivers.redis.MissingRedisConnectionException;
import nl.mirila.drivers.redis.RedisDriver;
import nl.mirila.messaging.core.*;
import nl.mirila.security.auth.core.contexts.SecurityContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * A Redis implementation of a {@link MessageProducer}.
 */
public class RedisMessageProducer implements MessageProducer {

    private static final Logger logger = LogManager.getLogger(RedisMessageProducer.class);

    private final SecurityContext context;
    private final ApplicationSettings settings;
    private final RedisDriver redisDriver;
    private final MessageMapper messageMapper;

    /**
     * Initialize a new instance.
     */
    @Inject
    public RedisMessageProducer(SecurityContext context,
                                ApplicationSettings settings,
                                RedisDriver redisDriver,
                                MessageMapper messageMapper) {
        this.context = context;
        this.settings = settings;
        this.redisDriver = redisDriver;
        this.messageMapper = messageMapper;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SecurityContext getSecurityContext() {
        return context;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ApplicationSettings getApplicationSettings() {
        return settings;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void sendMessage(Message<MessagePayload> message, MessageQueue queue) {
        RedisCommands<String, String> commands = redisDriver.getSyncConnection()
                .orElseThrow(MissingRedisConnectionException::new);

        try {
            String json = messageMapper.toJson(message);
            commands.publish(queue.getName(), json);
        } catch (JsonProcessingException e) {
            logger.error(e.getMessage(), e);
        }
    }

}
