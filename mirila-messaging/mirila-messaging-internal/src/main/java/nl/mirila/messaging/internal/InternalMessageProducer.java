package nl.mirila.messaging.internal;

import com.google.inject.Inject;
import nl.mirila.core.settings.ApplicationSettings;
import nl.mirila.messaging.core.Message;
import nl.mirila.messaging.core.MessagePayload;
import nl.mirila.messaging.core.MessageProducer;
import nl.mirila.messaging.core.MessageQueue;
import nl.mirila.security.auth.core.contexts.SecurityContext;

/**
 * An internal implementation of a {@link MessageProducer}.
 */
public class InternalMessageProducer implements MessageProducer {

    private final InternalMessageConsumer bus;
    private final SecurityContext context;
    private final ApplicationSettings settings;

    /**
     * Initialize a new instance.
     */
    @Inject
    public InternalMessageProducer(InternalMessageConsumer bus, SecurityContext context, ApplicationSettings settings) {
        this.bus = bus;
        this.context = context;
        this.settings = settings;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SecurityContext getSecurityContext() {
        return context;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ApplicationSettings getApplicationSettings() {
        return settings;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void sendMessage(Message<MessagePayload> message, MessageQueue queue) {
        bus.queueMessage(message);
    }

}
