package nl.mirila.messaging.internal;

import com.google.inject.Inject;
import nl.mirila.core.settings.Settings;

/**
 * Settings that are needed for the internal messaging system.
 */
public class InternalMessagingSettings {

    public static final String KEY_POOL_SIZE = "messaging.internal.pool.size";

    private static final int DEFAULT_POOL_SIZE = 3;

    private final Settings settings;

    /**
     * Initialize a new instance.
     */
    @Inject
    public InternalMessagingSettings(Settings settings) {
        this.settings = settings;
    }

    /**
     * Return the threads pool size of the internal message bus.
     * <p>
     * Setting: {@value KEY_POOL_SIZE}. Default: {@value DEFAULT_POOL_SIZE}.
     */
    public int getPoolSize() {
        return settings.getInteger(KEY_POOL_SIZE).orElse(DEFAULT_POOL_SIZE);
    }

}
