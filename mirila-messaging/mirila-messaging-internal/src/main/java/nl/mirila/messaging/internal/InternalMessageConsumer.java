package nl.mirila.messaging.internal;

import com.google.inject.Inject;
import com.google.inject.Injector;
import nl.mirila.messaging.core.Message;
import nl.mirila.messaging.core.MessageConsumer;
import nl.mirila.messaging.core.MessagePayload;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * The internal representation of a message consumer.
 */
public class InternalMessageConsumer extends MessageConsumer {

    private final ExecutorService bus;

    /**
     * Initialize a new instance.
     */
    @Inject
    public InternalMessageConsumer(Injector injector, InternalMessagingSettings settings) {
        super(injector);
        String namePrefix = "message-bus-worker-";
        bus = Executors.newFixedThreadPool(settings.getPoolSize(), (runnable) -> new Thread(runnable, namePrefix));
    }

    /**
     * Queue the given message in the pool.
     * <p>
     * @param message The message to queue.
     */
    public <P extends MessagePayload> void queueMessage(Message<P> message) {
        bus.execute(new InternalMessageBusRunnable<>(this, message));
    }

}
