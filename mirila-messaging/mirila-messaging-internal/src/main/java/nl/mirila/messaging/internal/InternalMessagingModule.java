package nl.mirila.messaging.internal;

import com.google.inject.AbstractModule;
import com.google.inject.multibindings.OptionalBinder;
import nl.mirila.messaging.core.MessageConsumer;
import nl.mirila.messaging.core.MessageProducer;

public class InternalMessagingModule extends AbstractModule {

    @Override
    protected void configure() {
        OptionalBinder.newOptionalBinder(binder(), MessageConsumer.class)
                .setBinding()
                .to(InternalMessageConsumer.class);

        OptionalBinder.newOptionalBinder(binder(), MessageProducer.class)
                .setBinding()
                .to(InternalMessageProducer.class);
    }

}
