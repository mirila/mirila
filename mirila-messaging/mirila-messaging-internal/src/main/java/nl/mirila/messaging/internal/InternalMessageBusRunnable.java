package nl.mirila.messaging.internal;

import nl.mirila.messaging.core.Message;
import nl.mirila.messaging.core.MessageConsumer;
import nl.mirila.messaging.core.MessagePayload;
import nl.mirila.messaging.core.MessageProcessor;

/**
 * A queueable item that contains a message and the resulted processor, that start consuming the message as soon as
 * this {@link Runnable} is started.
 * <p>
 * @param <P> The type of {@link MessagePayload} of the {@link Message} and {@link MessageProcessor}.
 */
public class InternalMessageBusRunnable<P extends MessagePayload> implements Runnable {

    private final MessageConsumer consumer;
    private final Message<P> message;

    /**
     * Initialize a new instance.
     */
    public InternalMessageBusRunnable(MessageConsumer consumer, Message<P> message) {
        this.consumer = consumer;
        this.message = message;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void run() {
        consumer.consume(message);
    }

}
