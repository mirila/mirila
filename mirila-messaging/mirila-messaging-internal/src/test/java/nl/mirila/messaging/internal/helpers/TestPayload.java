package nl.mirila.messaging.internal.helpers;

import nl.mirila.messaging.core.MessagePayload;

public class TestPayload implements MessagePayload {

    private final String name;

    public TestPayload(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
