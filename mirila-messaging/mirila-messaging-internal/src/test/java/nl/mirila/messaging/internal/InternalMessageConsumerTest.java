package nl.mirila.messaging.internal;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.TypeLiteral;
import com.google.inject.multibindings.Multibinder;
import nl.mirila.core.settings.modules.KeyValuesSettingsModule;
import nl.mirila.messaging.core.MessagePayload;
import nl.mirila.messaging.core.MessageProcessor;
import nl.mirila.messaging.core.MessageProducer;
import nl.mirila.messaging.core.MessageQueue;
import nl.mirila.messaging.internal.helpers.TestPayload;
import nl.mirila.messaging.internal.helpers.TestProcessor;
import nl.mirila.security.auth.core.contexts.GuestSecurityContext;
import nl.mirila.security.auth.core.contexts.SecurityContext;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static nl.mirila.core.settings.ApplicationSettings.KEY_APP_NAME;
import static org.assertj.core.api.Assertions.assertThat;

class InternalMessageConsumerTest {

    private static MessageProducer producer;
    private static MessageQueue queue;

    @BeforeAll
    static void beforeAll() {
        Map<String, String> settings = new HashMap<>();
        settings.put(KEY_APP_NAME, "test-app");

        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                install(new KeyValuesSettingsModule(settings));
                install(new InternalMessagingModule());
                bind(SecurityContext.class).toInstance(new GuestSecurityContext("test-realm", "en_EN"));

                Multibinder<MessageProcessor<? extends MessagePayload>> consumerBinder = Multibinder
                        .newSetBinder(binder(), new TypeLiteral<>() {});
                consumerBinder.addBinding().to(TestProcessor.class);
            }
        });

        producer = injector.getInstance(MessageProducer.class);
        queue = MessageQueue.withName("test-queue");
    }

    @Test
    void testMessaging() throws InterruptedException {
        producer.sendPayload(queue, new TestPayload("test1"));
        producer.sendPayload(queue, new TestPayload("test2"));
        producer.sendPayload(queue, new TestPayload("test3"));

        long counter = (TimeUnit.SECONDS.toMillis(10) / 5);
        while (TestProcessor.getProcessedPayloadNames().size() < 3) {
            //noinspection BusyWait
            Thread.sleep(10);
            if (counter-- <= 0) {
                throw new RuntimeException("Breaking from loop to prevent an infinite loop...");
            }
        }
        assertThat(TestProcessor.getProcessedPayloadNames())
                .containsOnly("test1", "test2", "test3");
    }

}
