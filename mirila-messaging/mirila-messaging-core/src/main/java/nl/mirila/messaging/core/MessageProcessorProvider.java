package nl.mirila.messaging.core;

import com.google.inject.Inject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * The {@link MessageProcessorProvider} is a custom provider and a custom provider for {@link MessageProcessor}, to
 * allow fetching new, fresh instances of the processors each time a message needs to be processed.
 */
public class MessageProcessorProvider {

    private static final Logger logger = LogManager.getLogger(MessageProcessorProvider.class);

    private final Set<MessageProcessor<? extends MessagePayload>> processors;

    /**
     * Initialize a new instance.
     */
    @Inject
    public MessageProcessorProvider(Set<MessageProcessor<? extends MessagePayload>> processors) {
        this.processors = processors;
    }

    /**
     * Return a set of all {@link MessageQueue} for which processors are available.
     * <p>
     * @return A set of queues.
     */
    public Set<MessageQueue> getProcessableQueues() {
        return processors.stream()
                .map(MessageProcessor::getQueue)
                .collect(Collectors.toSet());
    }


    /**
     * Return the {@link MessageProcessor} that can process the given {@link Message}, wrapped in an {@link Optional}.
     * <p>
     * @param message The message.
     * @param <P> The type of the payload.
     * @return The message processor.
     */
    @SuppressWarnings("unchecked")
    public <P extends MessagePayload> Optional<MessageProcessor<P>> getMessageProcessor(Message<P> message) {
        List<MessageProcessor<? extends MessagePayload>> list = processors.stream()
                .filter((c) -> c.canProcess(message))
                .toList();

        if (list.size() > 1) {
            String processorNames = list.stream()
                    .map(Object::getClass)
                    .map(Class::getSimpleName)
                    .collect(Collectors.joining(", "));
            logger.error("More than one processors processing payload {} is not allowed ({}).",
                         message.getPayload().getClass().getSimpleName(),
                         processorNames);
            return Optional.empty();
        }

        return list.stream()
                .findAny()
                .map(MessageProcessor.class::cast);
    }



}
