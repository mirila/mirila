package nl.mirila.messaging.core;

import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import com.google.inject.multibindings.Multibinder;
import com.google.inject.multibindings.OptionalBinder;
import nl.mirila.messaging.core.bypass.BypassMessageConsumer;
import nl.mirila.messaging.core.bypass.BypassMessageProducer;

/**
 * Binds the {@link BypassMessageProducer} and {@link BypassMessageConsumer} as default messaging handlers.
 * <p>
 * This bypasses messaging systems or internal threading, making it useful for testing purposes.
 * <p>
 * Small applications may use this as well, but it's advised to use the internal messaging module, which uses
 * separate thread pools.
 */
public class MessagingCoreModule extends AbstractModule {

    /**
     * {@inheritDoc}
     */
    @Override
    protected void configure() {

        // Ensure the following binder is created.
        Multibinder.newSetBinder(binder(), new TypeLiteral<MessageProcessor<? extends MessagePayload>>() {});

        OptionalBinder.newOptionalBinder(binder(), MessageConsumer.class)
                .setDefault()
                .to(BypassMessageConsumer.class);

        OptionalBinder.newOptionalBinder(binder(), MessageProducer.class)
                .setDefault()
                .to(BypassMessageProducer.class);
    }

}
