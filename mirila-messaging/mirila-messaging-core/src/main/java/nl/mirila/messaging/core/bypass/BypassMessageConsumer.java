package nl.mirila.messaging.core.bypass;

import com.google.inject.Inject;
import com.google.inject.Injector;
import nl.mirila.messaging.core.MessageConsumer;

/**
 * The BypassMessageConsumer consumes messages without using an external message bus system or internal threading.
 * <p>
 * This is useful for testing purposes.
 */
public class BypassMessageConsumer extends MessageConsumer {

    /**
     * Initialize a new instance, using the given processors.
     */
    @Inject
    public BypassMessageConsumer(Injector injector) {
        super(injector);
    }

}
