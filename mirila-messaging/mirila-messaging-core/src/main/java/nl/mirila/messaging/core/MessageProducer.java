package nl.mirila.messaging.core;

import nl.mirila.core.datatype.Id;
import nl.mirila.core.settings.ApplicationSettings;
import nl.mirila.security.auth.core.contexts.SecurityContext;

import java.util.UUID;

/**
 * The MessageProducer creates and sends a {@link Message} to a message bus system.
 */
public interface MessageProducer {

    /**
     * Return the current {@link SecurityContext}.
     */
    SecurityContext getSecurityContext();

    /**
     * Return the current {@link ApplicationSettings}.
     */
    ApplicationSettings getApplicationSettings();

    /**
     * Send the given {@link Message} to the given {@link MessageQueue}.
     * <p>
     * @param queue The queue.
     * @param message The message.
     */
    void sendMessage(Message<MessagePayload> message, MessageQueue queue);

    /**
     * Return the {@link Id} for the given {@link MessagePayload}, depending on the actual message bus system.
     * <p>
     * @param payload The payload.
     * @return The id.
     */
    default Id getNewId(MessagePayload payload) {
        return Id.of(UUID.randomUUID().toString());
    }


    /**
     * Send the given {@link MessagePayload} as a {@link Message} to the given {@link MessageQueue}.
     * <p>
     * @param queue The queue.
     * @param payload The payload.
     * @param <P> The type of the payload.
     * @return The message that is send.
     */
    default <P extends MessagePayload> Message<P> sendPayload(MessageQueue queue, P payload) {
        return sendPayload(queue, payload, null);
    }

    /**
     * Send the given {@link MessagePayload} as a {@link Message} to the given {@link MessageQueue}.
     * <p>
     * @param queue The queue.
     * @param payload The payload.
     * @param batchId The id of the batch.
     * @param <P> The type of the payload.
     * @return The message that is send.
     */
    @SuppressWarnings("unchecked")
    default <P extends MessagePayload> Message<P> sendPayload(MessageQueue queue, P payload, Id batchId) {
        SecurityContext context = getSecurityContext();
        ApplicationSettings settings = getApplicationSettings();
        Message<P> message = Message.forPayload(payload)
                .withId(getNewId(payload))
                .withQueue(queue.getName())
                .withRealm(context.getRealm())
                .withBatchId(batchId)
                .withIdentity(context.getCredentialName().orElse(null))
                .withSourceId(settings.getAppName())
                .withLocale(context.getLocaleId());
        sendMessage((Message<MessagePayload>) message, queue);
        return message;
    }

    /**
     * Send the given {@link MessagePayload} as a new {@link Message} to the given {@link MessageQueue}, as a follow up
     * message of the given previous {@link Message}.
     * <p>
     * @param queue The queue.
     * @param payload The payload.
     * @param previous The message
     * @param <P> The type of the payload.
     * @return The message that is send.
     */
    default <P extends MessagePayload> Message<P> sendFollowUpPayload(MessageQueue queue,
                                                                     P payload,
                                                                     Message<MessagePayload> previous) {
        return sendFollowUpPayload(queue, payload, previous, null);
    }

    /**
     * Send the given {@link MessagePayload} as a new {@link Message} to the given {@link MessageQueue}, as a follow up
     * message of the given previous {@link Message}.
     * <p>
     * @param queue The queue.
     * @param payload The payload.
     * @param previous The message
     * @param batchId The id of the batch.
     * @param <P> The type of the payload.
     * @return The message that is send.
     */
    @SuppressWarnings("unchecked")
    default <P extends MessagePayload> Message<P> sendFollowUpPayload(MessageQueue queue,
                                                                     P payload,
                                                                     Message<MessagePayload> previous,
                                                                     Id batchId) {
        SecurityContext context = getSecurityContext();
        ApplicationSettings settings = getApplicationSettings();
        Message<P> message = Message.forPayload(payload)
                .withId(getNewId(payload))
                .withQueue(previous.getRealm())
                .withRealm(previous.getRealm())
                .withFirstId(previous.getFirstId())
                .withPreviousId(previous.getId())
                .withBatchId(batchId)
                .withProcessedAt(previous.getProcessedAt())
                .withIdentity(context.getCredentialName().orElse(null))
                .withSourceId(settings.getAppName())
                .withLocale(previous.getLocaleId());
        sendMessage((Message<MessagePayload>) message, queue);
        return message;
    }

}
