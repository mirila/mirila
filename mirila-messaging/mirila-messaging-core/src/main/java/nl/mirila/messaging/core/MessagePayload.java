package nl.mirila.messaging.core;

/**
 * An interface that represents the payload of a {@link Message}.
 * <p>
 * While the {@link Message} defines data that is relevant for each message, this payload defines fields that are
 * specific for a certain context.
 */
public interface MessagePayload {

}
