package nl.mirila.messaging.core.bypass;

import com.google.inject.Inject;
import nl.mirila.core.settings.ApplicationSettings;
import nl.mirila.messaging.core.Message;
import nl.mirila.messaging.core.MessagePayload;
import nl.mirila.messaging.core.MessageProducer;
import nl.mirila.messaging.core.MessageQueue;
import nl.mirila.security.auth.core.contexts.SecurityContext;

/**
 * The BypassMessageProducer sends message directly and synchronously to the {@link BypassMessageConsumer}, without using
 * an external message bus system or internal threading.
 * <p>
 * This is useful for testing purposes.
 */
public class BypassMessageProducer implements MessageProducer {

    private final SecurityContext context;
    private final ApplicationSettings settings;
    private final BypassMessageConsumer consumer;

    /**
     * Initialize a new instance.
     */
    @Inject
    public BypassMessageProducer(SecurityContext context,
                                 ApplicationSettings settings,
                                 BypassMessageConsumer consumer) {
        this.context = context;
        this.settings = settings;
        this.consumer = consumer;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SecurityContext getSecurityContext() {
        return context;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ApplicationSettings getApplicationSettings() {
        return settings;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void sendMessage(Message<MessagePayload> message, MessageQueue queue) {
        consumer.consume(message);
    }

}
