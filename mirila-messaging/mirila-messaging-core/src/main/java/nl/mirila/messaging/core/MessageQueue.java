package nl.mirila.messaging.core;

import java.util.Objects;
import java.util.Optional;

/**
 * The representation of a message queue.
 */
public class MessageQueue {

    private final String name;
    private String realm;

    /**
     * Initialize a new instance with the given name.
     */
    protected MessageQueue(String name) {
        this.name = name;
        this.realm = null;
    }

    /**
     * Return the name of this queue.
     */
    public String getName() {
        return name;
    }

    /**
     * Return the realm for this given, if the realm is relevant. Otherwise an empty {@link Optional} is returned.
     */
    public Optional<String> getRealm() {
        return Optional.ofNullable(realm);
    }

    /**
     * Create a {@link MessageQueue} with the given name and return this new instance for chaining.
     * <p>
     * @param name The name.
     * @return This instance.
     */
    public static MessageQueue withName(String name) {
        return new MessageQueue(name);
    }

    /**
     * Set the given realm and return this instance for chaining.
     * <p>
     * @param realm The realm.
     * @return This instance.
     */
    public MessageQueue withRealm(String realm) {
        this.realm = realm;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }
        MessageQueue other = (MessageQueue) obj;
        return ((name.equals(other.name)) &&
                (Objects.equals(realm, other.realm)));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(name, realm);
    }

}
