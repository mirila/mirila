package nl.mirila.messaging.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import nl.mirila.core.datatype.Id;

import static com.fasterxml.jackson.annotation.JsonTypeInfo.As.EXTERNAL_PROPERTY;
import static com.fasterxml.jackson.annotation.JsonTypeInfo.Id.CLASS;

/**
 * The basis for each message on a message bus system.
 * <p>
 * @param <P> The type of the payload.
 */
public class Message<P extends MessagePayload> {

    @JsonProperty
    private String queue;

    @JsonProperty
    private String realm;

    @JsonProperty
    private Id id;

    @JsonProperty
    private Id firstId;

    @JsonProperty
    private Id previousId;

    @JsonProperty
    private Id batchId;

    @JsonProperty
    private Long createdAt;

    @JsonProperty
    private Long processedAt;

    @JsonProperty
    private String identity;

    @JsonProperty
    private String sourceId;

    @JsonProperty
    private String localeId;

    @JsonProperty
    @JsonTypeInfo(use = CLASS, include = EXTERNAL_PROPERTY, property = "payloadType")
    private P payload;

    /**
     * Initialize a new instance.
     */
    protected Message() {
        // no-op
    }

    /**
     * Initialize a new instance.
     */
    private Message(P payload) {
        this.payload = payload;
        this.createdAt = System.currentTimeMillis();
    }

    /**
     * Return the queue.
     */
    public String getQueue() {
        return queue;
    }

    /**
     * Return the realm.
     */
    public String getRealm() {
        return realm;
    }

    /**
     * Return the id.
     */
    public Id getId() {
        return id;
    }

    /**
     * Return the firstId.
     */
    public Id getFirstId() {
        return firstId;
    }

    /**
     * Return the previousId.
     */
    public Id getPreviousId() {
        return previousId;
    }

    /**
     * Return the batchId.
     */
    public Id getBatchId() {
        return batchId;
    }

    /**
     * Return the createdAt.
     */
    public Long getCreatedAt() {
        return createdAt;
    }

    /**
     * Return the processedAt.
     */
    public Long getProcessedAt() {
        return processedAt;
    }

    /**
     * Return the identity.
     */
    public String getIdentity() {
        return identity;
    }

    /**
     * Return the sourceId.
     */
    public String getSourceId() {
        return sourceId;
    }

    /**
     * Return the localeId.
     */
    public String getLocaleId() {
        return localeId;
    }

    /**
     * Return the payload.
     */
    public P getPayload() {
        return payload;
    }

    /**
     * Set the given queue and return this instance for chaining purposes.
     * <p>
     * @param queue The queue.
     * @return This instance.
     */
    public Message<P> withQueue(String queue) {
        this.queue = queue;
        return this;
    }

    /**
     * Set the given realm and return this instance for chaining purposes.
     * <p>
     * @param realm The realm.
     * @return This instance.
     */
    public Message<P> withRealm(String realm) {
        this.realm = realm;
        return this;
    }

    /**
     * Set the given id and return this instance for chaining purposes.
     * <p>
     * @param id The id.
     * @return This instance.
     */
    public Message<P> withId(Id id) {
        this.id = id;
        return this;
    }

    /**
     * Set the given firstId and return this instance for chaining purposes.
     * <p>
     * @param firstId The firstId.
     * @return This instance.
     */
    public Message<P> withFirstId(Id firstId) {
        this.firstId = firstId;
        return this;
    }

    /**
     * Set the given previousId and return this instance for chaining purposes.
     * <p>
     * @param previousId The previousId.
     * @return This instance.
     */
    public Message<P> withPreviousId(Id previousId) {
        this.previousId = previousId;
        return this;
    }

    /**
     * Set the given batchId and return this instance for chaining purposes.
     * <p>
     * @param batchId The batchId.
     * @return This instance.
     */
    public Message<P> withBatchId(Id batchId) {
        this.batchId = batchId;
        return this;
    }

    /**
     * Set the given processedAt and return this instance for chaining purposes.
     * <p>
     * @param processedAt The processedAt.
     * @return This instance.
     */
    public Message<P> withProcessedAt(Long processedAt) {
        this.processedAt = processedAt;
        return this;
    }

    /**
     * Set the given identity and return this instance for chaining purposes.
     * <p>
     * @param identity The identity.
     * @return This instance.
     */
    public Message<P> withIdentity(String identity) {
        this.identity = identity;
        return this;
    }

    /**
     * Set the given sourceId and return this instance for chaining purposes.
     * <p>
     * @param sourceId The sourceId.
     * @return This instance.
     */
    public Message<P> withSourceId(String sourceId) {
        this.sourceId = sourceId;
        return this;
    }

    /**
     * Set the given localeId and return this instance for chaining purposes.
     * <p>
     * @param localeId The localeId.
     * @return This instance.
     */
    public Message<P> withLocale(String localeId) {
        this.localeId = localeId;
        return this;
    }

    /**
     * Create a new message for the given {@link MessagePayload}.
     * <p>
     * @param payload The payload.
     * @param <R> The type of the payload.
     * @return The new message instance.
     */
    public static <R extends MessagePayload> Message<R> forPayload(R payload) {
        return new Message<>(payload);
    }

}
