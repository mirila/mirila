package nl.mirila.messaging.core;

import com.google.inject.Injector;
import nl.mirila.messaging.core.exceptions.MissingMessageProcessor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * A MessageConsumer fetches or gets a {@link Message} from a message bus system, and passes it to the related
 * {@link MessageProcessor}.
 */
public abstract class MessageConsumer {

    private static final Logger logger = LogManager.getLogger(MessageConsumer.class);

    private final Injector injector;

    /**
     * Initialize a new instance, using the given injector to fetch processors.
     */
    protected MessageConsumer(Injector injector) {
        this.injector = injector;
    }

    /**
     * Consume the given message.
     * <p>
     * For the given message, the related processor will be searched.
     * <p>
     * @param message The message.
     * @param <P> The type of the payload.
     */
    public <P extends MessagePayload> void consume(Message<P> message) {
        MessageProcessorProvider provider = injector.getInstance(MessageProcessorProvider.class);
        MessageProcessor<P> processor = provider.getMessageProcessor(message)
                .orElseThrow(() -> new MissingMessageProcessor(message.getPayload().getClass()));

        try {
            processor.process(message);
        } catch (Exception e) {
            logger.error("Error while processing message: {}", message.toString(), e);
        }
    }

}
