package nl.mirila.messaging.core.exceptions;

import nl.mirila.core.exceptions.MirilaException;
import nl.mirila.messaging.core.MessagePayload;

public class MissingMessageProcessor extends MirilaException {

    public MissingMessageProcessor(Class<? extends MessagePayload> payloadClass) {
        super("There's no MessageProcessor for message of " + payloadClass.getSimpleName());
    }

}
