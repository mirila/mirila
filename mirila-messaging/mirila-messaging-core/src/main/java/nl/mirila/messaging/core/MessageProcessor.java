package nl.mirila.messaging.core;

import java.util.Objects;

/**
 * The MessageProcessor is able to process a {@link Message} with specific {@link MessagePayload}.
 * <p>
 * @param <P> The type of the {@link MessagePayload}.
 */
public interface MessageProcessor<P extends MessagePayload> {

    /**
     * Return the queue for which this MessageConsumer operates.
     */
    MessageQueue getQueue();

    /**
     * Return the concrete class of the {@link MessagePayload} this consumer can process.
     */
    Class<P> getPayloadClass();

    /**
     * Return true if the given message is processable by this processor.
     */
    default boolean canProcess(Message<? extends MessagePayload> message) {
        MessageQueue queue = getQueue();
        Objects.requireNonNull(message);
        Objects.requireNonNull(message.getPayload());
        Objects.requireNonNull(queue);
        return ((Objects.equals(message.getQueue(), queue.getName())) &&
                (Objects.equals(message.getPayload().getClass(), getPayloadClass())) &&
                (queue.getRealm().map(message.getRealm()::equals).orElse(true)));
    }

    /**
     * Processes the given {@link Message}.
     * <p>
     * @param message The message.
     */
    void process(Message<P> message);

}
