package nl.mirila.messaging.core;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;

/**
 * The MessageMapper uses an {@link ObjectMapper} to convert a {@link Message} to JSON, and vice versa.
 */
public class MessageMapper {

    private final ObjectMapper mapper;

    /**
     * Initialize a new instance.
     */
    @Inject
    public MessageMapper(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    /**
     * Convert the given {@link Message} into JSON.
     * <p>
     * @param message The message to convert.
     * @return The JSON.
     * <p>
     * @throws JsonProcessingException Thrown if the JSON conversion failed.
     */
    public String toJson(Message<? extends MessagePayload> message) throws JsonProcessingException {
        return mapper.writeValueAsString(message);
    }

    /**
     * Convert the given JSON into a {@link Message}.
     * <p>
     * @param json The JSON to convert.
     * @return The message.
     * <p>
     * @throws JsonProcessingException Thrown if the JSON conversion failed.
     */
    public Message<? extends MessagePayload> fromJson(String json) throws JsonProcessingException {
        return (Message<? extends MessagePayload>) mapper.readValue(json, Message.class);
    }

}
