package nl.mirila.messaging.core.helpers;

import com.google.inject.Inject;
import nl.mirila.messaging.core.Message;
import nl.mirila.messaging.core.MessageProcessor;
import nl.mirila.messaging.core.MessageQueue;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FirstProcessor implements MessageProcessor<FirstExamplePayload> {

    private static final Logger logger = LogManager.getLogger(FirstProcessor.class);
    private final ExampleService exampleService;

    @Inject
    public FirstProcessor(ExampleService exampleService) {
        this.exampleService = exampleService;
    }

    @Override
    public MessageQueue getQueue() {
        return MessageQueue.withName("test-queue-1");
    }

    @Override
    public Class<FirstExamplePayload> getPayloadClass() {
        return FirstExamplePayload.class;
    }

    @Override
    public void process(Message<FirstExamplePayload> message) {
        exampleService.doNothing();
        logger.info("Processed fake message using {} with payload info: {}",
                    this.getClass().getSimpleName(),
                    message.getPayload().getInformation());
    }




}
