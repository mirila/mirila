package nl.mirila.messaging.core.helpers;

import com.fasterxml.jackson.annotation.JsonProperty;
import nl.mirila.messaging.core.MessagePayload;

public class TestPayload implements MessagePayload {

    @JsonProperty
    private String name;

    public TestPayload() {
    }

    public TestPayload(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
