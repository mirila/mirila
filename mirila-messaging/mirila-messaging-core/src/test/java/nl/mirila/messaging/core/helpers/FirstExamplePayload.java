package nl.mirila.messaging.core.helpers;

import nl.mirila.messaging.core.MessagePayload;

public class FirstExamplePayload implements MessagePayload {

    private String information;

    /**
     * Return the information.
     */
    public String getInformation() {
        return information;
    }

    /**
     * Set the given information.
     */
    public void setInformation(String information) {
        this.information = information;
    }

}
