package nl.mirila.messaging.core;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.TypeLiteral;
import com.google.inject.multibindings.Multibinder;
import nl.mirila.core.settings.modules.KeyValuesSettingsModule;
import nl.mirila.messaging.core.helpers.*;
import nl.mirila.security.auth.core.contexts.GuestSecurityContext;
import nl.mirila.security.auth.core.contexts.SecurityContext;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static nl.mirila.core.settings.ApplicationSettings.KEY_REALM;
import static org.assertj.core.api.Assertions.assertThat;

class MessageProcessorProviderTest {

    private Injector injector;

    @BeforeEach
    void setUp() {
        injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                Map<String, String> settings = new HashMap<>();
                settings.put(KEY_REALM, "test-app");

                install(new KeyValuesSettingsModule(settings));
                install(new MessagingCoreModule());
                bind(SecurityContext.class).toInstance(new GuestSecurityContext("test-realm", "en_EN"));

                Multibinder<MessageProcessor<? extends MessagePayload>> binder =
                        Multibinder.newSetBinder(binder(), new TypeLiteral<>() {});
                binder.addBinding().to(FirstProcessor.class);
                binder.addBinding().to(SecondProcessor.class);
                binder.addBinding().to(ThirdProcessor.class);
            }
        });
    }

    @Test
    void testGetFirstExample() {
        FirstExamplePayload payload = new FirstExamplePayload();
        payload.setInformation("Hello world");
        Message<FirstExamplePayload> message = Message.forPayload(payload)
                .withQueue("test-queue-1")
                .withRealm("fake-realm");

        MessageProcessorProvider provider = injector.getInstance(MessageProcessorProvider.class);
        MessageProcessor<FirstExamplePayload> processor = provider.getMessageProcessor(message).orElse(null);

        assertThat(processor).isOfAnyClassIn(FirstProcessor.class);
    }

    @Test
    void testGetSecondExample() {
        SecondExamplePayload payload = new SecondExamplePayload();
        payload.setInformation("Hello world");
        Message<SecondExamplePayload> message = Message.forPayload(payload)
                .withQueue("test-queue-2")
                .withRealm("fake-realm");

        MessageProcessorProvider provider = injector.getInstance(MessageProcessorProvider.class);
        // Should be empty, as both SecondProcessor and ThirdProcessor process SecondExamplePayload. That's not allowed.
        assertThat(provider.getMessageProcessor(message)).isEmpty();
    }

    @Test
    void testGetFirstExampleTwice() {
        ExampleService.INSTANCES.clear();

        FirstExamplePayload payload = new FirstExamplePayload();
        payload.setInformation("Hello world");
        Message<FirstExamplePayload> message = Message.forPayload(payload)
                .withQueue("test-queue-1")
                .withRealm("fake-realm");

        MessageProcessorProvider provider1 = injector.getInstance(MessageProcessorProvider.class);
        MessageProcessorProvider provider2 = injector.getInstance(MessageProcessorProvider.class);

        MessageProcessor<FirstExamplePayload> processor1 = provider1.getMessageProcessor(message).orElse(null);
        MessageProcessor<FirstExamplePayload> processor2 = provider2.getMessageProcessor(message).orElse(null);

        assertThat(processor1).isOfAnyClassIn(FirstProcessor.class);
        assertThat(processor2).isOfAnyClassIn(FirstProcessor.class);
        assertThat(processor1).isNotEqualTo(processor2);
        assertThat(ExampleService.INSTANCES).hasSize(2);
    }

}
