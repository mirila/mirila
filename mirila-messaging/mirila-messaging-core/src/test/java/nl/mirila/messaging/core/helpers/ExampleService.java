package nl.mirila.messaging.core.helpers;

import java.util.HashSet;
import java.util.Set;

public class ExampleService {

    public volatile static Set<ExampleService> INSTANCES = new HashSet<>();

    public ExampleService() {
        INSTANCES.add(this);
    }

    public void doNothing() {

    }

}
