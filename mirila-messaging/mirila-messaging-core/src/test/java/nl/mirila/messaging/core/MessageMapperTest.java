package nl.mirila.messaging.core;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import nl.mirila.core.datatype.Id;
import nl.mirila.messaging.core.helpers.FirstExamplePayload;
import nl.mirila.messaging.core.helpers.TestPayload;
import nl.mirila.model.core.ModelCoreModule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class MessageMapperTest {

    private MessageMapper mapper;

    @BeforeEach
    void setUp() {
        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                install(new ModelCoreModule());
            }
        });
        mapper = injector.getInstance(MessageMapper.class);
    }

    @Test
    void testFromJson() throws JsonProcessingException {
        String json = "{\"queue\":\"test-queue\",\"realm\":\"test-realm\",\"id\":\"ecf9e22d\",\"firstId\":null," +
                "\"previousId\":null,\"batchId\":null,\"createdAt\":1603919731802,\"processedAt\":null," +
                "\"identity\":null,\"sourceId\":\"test-app\",\"localeId\":\"en_EN\",\"payload\":{\"name\":\"test3\"}," +
                "\"payloadType\":\"nl.mirila.messaging.core.helpers.TestPayload\"}";

        @SuppressWarnings("unchecked")
        Message<TestPayload> message = (Message<TestPayload>) mapper.fromJson(json);

        assertThat(message.getRealm()).isEqualTo("test-realm");
        assertThat(message.getId()).isEqualTo(Id.of("ecf9e22d"));
        assertThat(message.getPayload().getName()).isEqualTo("test3");
    }

    @Test
    void testToJson() throws JsonProcessingException {
        FirstExamplePayload payload = new FirstExamplePayload();
        payload.setInformation("Hello world");

        Message<FirstExamplePayload> message = Message.forPayload(payload)
                .withId(Id.of("test-123456"))
                .withQueue("test-queue-1")
                .withRealm("fake-realm");

        String json = mapper.toJson(message);
        assertThat(json)
                .contains("\"test-123456\"")
                .contains("\"test-queue-1\"")
                .contains("\"fake-realm\"");
    }
}
