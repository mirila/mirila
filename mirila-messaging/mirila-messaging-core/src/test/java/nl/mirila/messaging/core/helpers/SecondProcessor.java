package nl.mirila.messaging.core.helpers;

import nl.mirila.messaging.core.Message;
import nl.mirila.messaging.core.MessageProcessor;
import nl.mirila.messaging.core.MessageQueue;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SecondProcessor implements MessageProcessor<SecondExamplePayload> {

    private static final Logger logger = LogManager.getLogger(SecondProcessor.class);

    @Override
    public MessageQueue getQueue() {
        return MessageQueue.withName("test-queue-2");
    }

    @Override
    public Class<SecondExamplePayload> getPayloadClass() {
        return SecondExamplePayload.class;
    }

    @Override
    public void process(Message<SecondExamplePayload> message) {
        logger.info("Processed fake message using {} with payload info: {}",
                    this.getClass().getSimpleName(),
                    message.getPayload().getInformation());
    }

}
