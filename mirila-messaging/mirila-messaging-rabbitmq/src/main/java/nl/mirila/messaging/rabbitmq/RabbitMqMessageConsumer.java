package nl.mirila.messaging.rabbitmq;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Delivery;
import nl.mirila.drivers.rabbitmq.MissingRabbitMqChannel;
import nl.mirila.drivers.rabbitmq.RabbitMqChannels;
import nl.mirila.messaging.core.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * A representation of a {@link MessageConsumer} using RabbitMQ.
 */
public class RabbitMqMessageConsumer extends MessageConsumer {

    private static final Logger logger = LogManager.getLogger(RabbitMqMessageConsumer.class);

    private final MessageMapper mapper;

    /**
     * Initialize a new instance, using the given processors.
     */
    @Inject
    public RabbitMqMessageConsumer(Injector injector, RabbitMqChannels channels, MessageMapper mapper) {
        super(injector);
        this.mapper = mapper;

        Channel channel = channels.getCurrentChannel().orElseThrow(MissingRabbitMqChannel::new);
        MessageProcessorProvider provider = injector.getInstance(MessageProcessorProvider.class);
        provider.getProcessableQueues()
                .stream()
                .map(MessageQueue::getName)
                .forEach((queueName) -> {
                    try {
                        channel.queueDeclare(queueName, false, false, false, null);
                        channel.basicConsume(queueName, true, this::processRawData, this::processCancellation);
                    } catch (IOException e) {
                        logger.error(e.getMessage(), e);
                    }
                });
    }

    /**
     * Process the incoming raw data.
     * <p>
     * @param consumerTag The tag of the consumer.
     * @param delivery The delivery data.
     * @param <P> The type of the payload.
     */
    @SuppressWarnings("unchecked")
    private <P extends MessagePayload> void processRawData(String consumerTag, Delivery delivery) {
        String json = new String(delivery.getBody(), StandardCharsets.UTF_8);
        if (StringUtils.isBlank(json)) {
            return;
        }
        Message<P> message;
        try {
            message = (Message<P>) mapper.fromJson(json);
        } catch (JsonProcessingException e) {
            logger.error(e.getMessage());
            return;
        }
        consume(message);
    }

    /**
     * Process the cancellation.
     * <p>
     * @param consumerTag The consumer tag that is cancelled.
     */
    private void processCancellation(String consumerTag) {
        logger.warn("Received cancel for {}", consumerTag);
    }

}
