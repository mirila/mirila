package nl.mirila.messaging.rabbitmq;

import com.google.inject.Inject;
import com.rabbitmq.client.Channel;
import nl.mirila.core.settings.ApplicationSettings;
import nl.mirila.drivers.rabbitmq.MissingRabbitMqChannel;
import nl.mirila.drivers.rabbitmq.RabbitMqChannels;
import nl.mirila.messaging.core.*;
import nl.mirila.security.auth.core.contexts.SecurityContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * A representation of a {@link MessageProducer} using RabbitMQ.
 */
public class RabbitMqMessageProducer implements MessageProducer {

    private static final Logger logger = LogManager.getLogger(RabbitMqMessageProducer.class);

    private final SecurityContext context;
    private final ApplicationSettings settings;
    private final RabbitMqChannels channels;
    private final MessageMapper mapper;

    /**
     * Initialize a new instance.
     */
    @Inject
    public RabbitMqMessageProducer(SecurityContext context,
                                   ApplicationSettings settings,
                                   RabbitMqChannels channels,
                                   MessageMapper mapper) {
        this.context = context;
        this.settings = settings;
        this.channels = channels;
        this.mapper = mapper;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SecurityContext getSecurityContext() {
        return context;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ApplicationSettings getApplicationSettings() {
        return settings;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void sendMessage(Message<MessagePayload> message, MessageQueue queue) {
        Channel channel = channels.getCurrentChannel().orElseThrow(() -> new MissingRabbitMqChannel(queue.getName()));
        try {
            String json = mapper.toJson(message);
            logger.info("Got json: {}", json);
            channel.basicPublish("", queue.getName(), null, json.getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

}
