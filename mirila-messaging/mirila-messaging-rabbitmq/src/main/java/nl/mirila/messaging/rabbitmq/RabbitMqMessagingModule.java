package nl.mirila.messaging.rabbitmq;

import com.google.inject.AbstractModule;
import com.google.inject.multibindings.OptionalBinder;
import nl.mirila.drivers.rabbitmq.RabbitMqDriverModule;
import nl.mirila.messaging.core.MessageConsumer;
import nl.mirila.messaging.core.MessageProducer;

public class RabbitMqMessagingModule extends AbstractModule {

    @Override
    protected void configure() {
        install(new RabbitMqDriverModule());

        OptionalBinder.newOptionalBinder(binder(), MessageConsumer.class)
                .setBinding()
                .to(RabbitMqMessageConsumer.class)
                .asEagerSingleton();

        OptionalBinder.newOptionalBinder(binder(), MessageProducer.class)
                .setBinding()
                .to(RabbitMqMessageProducer.class);
    }

}
