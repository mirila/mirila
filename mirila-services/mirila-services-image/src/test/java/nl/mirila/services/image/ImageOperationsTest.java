package nl.mirila.services.image;

import nl.mirila.services.image.actions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

import static nl.mirila.services.image.enums.FlipType.HORIZONTAL;
import static nl.mirila.services.image.enums.FlipType.VERTICAL;
import static nl.mirila.services.image.enums.RotationType.*;
import static nl.mirila.services.image.enums.ThumbnailType.KEEP_RATIO;
import static org.assertj.core.api.Assertions.assertThat;

class ImageOperationsTest {

    private ImageOperations operations;
    private Path landscapeTestImagePath;
    private Path portraitTestImagePath;
    private Path outputPath;

    @BeforeEach
    void setUp() throws IOException, URISyntaxException {
        operations = new ImageOperations();
        landscapeTestImagePath = Path.of(ImageService.class.getResource(File.separator + "landscape-test.jpg").toURI());
        portraitTestImagePath = Path.of(ImageService.class.getResource(File.separator + "portrait-test.png").toURI());
        outputPath = Path.of(landscapeTestImagePath.getParent().getParent().toString(), "test-output");
        if (!Files.exists(outputPath)) {
            Files.createDirectory(outputPath);
        }
    }

    @Test
    void testCropImage() throws IOException {
        BufferedImage source = ImageIO.read(landscapeTestImagePath.toFile());
        CropAction cropAction = CropAction.withLeft(10).withTop(10).withWidth(128).withHeight(168);
        BufferedImage image = operations.performImageAction(source, cropAction);
        File outputFile = Path.of(outputPath.toString(), "test-cropped.jpg").toFile();
        ImageIO.write(image, "jpg", outputFile);

        assertThat(image.getWidth()).isEqualTo(128);
        assertThat(image.getHeight()).isEqualTo(168);
        assertThat(Files.exists(outputFile.toPath())).isTrue();
    }

    @Test
    void testFlipHorizontalImage() throws IOException {
        BufferedImage source = ImageIO.read(landscapeTestImagePath.toFile());
        BufferedImage image = operations.performImageAction(source, FlipAction.withType(HORIZONTAL));
        File outputFile = Path.of(outputPath.toString(), "test-flipped-horizontal.jpg").toFile();
        ImageIO.write(image, "jpg", outputFile);

        assertThat(image.getWidth()).isEqualTo(1280);
        assertThat(image.getHeight()).isEqualTo(960);
        assertThat(Files.exists(outputFile.toPath())).isTrue();
    }

    @Test
    void testFlipVerticalImage() throws IOException {
        BufferedImage source = ImageIO.read(landscapeTestImagePath.toFile());
        BufferedImage image = operations.performImageAction(source, FlipAction.withType(VERTICAL));
        File outputFile = Path.of(outputPath.toString(), "test-flipped-vertical.jpg").toFile();
        ImageIO.write(image, "jpg", outputFile);

        assertThat(image.getWidth()).isEqualTo(1280);
        assertThat(image.getHeight()).isEqualTo(960);
        assertThat(Files.exists(outputFile.toPath())).isTrue();
    }

    @Test
    void testResizeImage() throws IOException {
        BufferedImage source = ImageIO.read(landscapeTestImagePath.toFile());
        BufferedImage image = operations.performImageAction(source, ResizeAction.withMaxSize(256));
        File outputFile = Path.of(outputPath.toString(), "test-resized.jpg").toFile();
        ImageIO.write(image, "jpg", outputFile);

        assertThat(image.getWidth()).isEqualTo(256);
        assertThat(image.getHeight()).isEqualTo(192);
        assertThat(Files.exists(outputFile.toPath())).isTrue();
    }

    @Test
    void testResizeAndCropImage() throws IOException {
        BufferedImage source = ImageIO.read(landscapeTestImagePath.toFile());
        List<ImageAction> actions = Arrays.asList(ResizeAction.withMaxSize(256),
                                                  CropAction.withLeft(10).withTop(10).withWidth(100).withHeight(100));
        BufferedImage image = operations.performImageAction(source, actions);
        File outputFile = Path.of(outputPath.toString(), "test-cropped-resized.jpg").toFile();
        ImageIO.write(image, "jpg", outputFile);

        assertThat(image.getWidth()).isEqualTo(100);
        assertThat(image.getHeight()).isEqualTo(100);
        assertThat(Files.exists(outputFile.toPath())).isTrue();
    }

    @Test
    void testRotate90Image() throws IOException {
        BufferedImage source = ImageIO.read(landscapeTestImagePath.toFile());
        BufferedImage image = operations.performImageAction(source, RotateAction.withType(ROTATE_90));
        File outputFile = Path.of(outputPath.toString(), "test-rotated-90.jpg").toFile();
        ImageIO.write(image, "jpg", outputFile);

        assertThat(image.getWidth()).isEqualTo(960);
        assertThat(image.getHeight()).isEqualTo(1280);
        assertThat(Files.exists(outputFile.toPath())).isTrue();
    }

    @Test
    void testRotate180Image() throws IOException {
        BufferedImage source = ImageIO.read(landscapeTestImagePath.toFile());
        BufferedImage image = operations.performImageAction(source, RotateAction.withType(ROTATE_180));
        File outputFile = Path.of(outputPath.toString(), "test-rotated-180.jpg").toFile();
        ImageIO.write(image, "jpg", outputFile);

        assertThat(image.getWidth()).isEqualTo(1280);
        assertThat(image.getHeight()).isEqualTo(960);
        assertThat(Files.exists(outputFile.toPath())).isTrue();
    }

    @Test
    void testRotate270Image() throws IOException {
        BufferedImage source = ImageIO.read(landscapeTestImagePath.toFile());
        BufferedImage image = operations.performImageAction(source, RotateAction.withType(ROTATE_270));
        File outputFile = Path.of(outputPath.toString(), "test-rotated-270.jpg").toFile();
        ImageIO.write(image, "jpg", outputFile);

        assertThat(image.getWidth()).isEqualTo(960);
        assertThat(image.getHeight()).isEqualTo(1280);
        assertThat(Files.exists(outputFile.toPath())).isTrue();
    }

    @Test
    void testSquareThumbnail() throws IOException {
        BufferedImage source = ImageIO.read(landscapeTestImagePath.toFile());
        BufferedImage image = operations.performImageAction(source, ThumbnailAction.withSize(256));
        File outputFile = Path.of(outputPath.toString(), "test-thumbnail-256-square.jpg").toFile();
        ImageIO.write(image, "jpg", outputFile);

        assertThat(image.getWidth()).isEqualTo(256);
        assertThat(image.getHeight()).isEqualTo(256);
        assertThat(Files.exists(outputFile.toPath())).isTrue();
    }

    @Test
    void testKeepRatioThumbnail() throws IOException {
        BufferedImage source = ImageIO.read(landscapeTestImagePath.toFile());
        BufferedImage image = operations.performImageAction(source, ThumbnailAction.withSize(256).withType(KEEP_RATIO));
        File outputFile = Path.of(outputPath.toString(), "test-thumbnail-256-ratio.jpg").toFile();
        ImageIO.write(image, "jpg", outputFile);

        assertThat(image.getWidth()).isEqualTo(256);
        assertThat(image.getHeight()).isEqualTo(192);
        assertThat(Files.exists(outputFile.toPath())).isTrue();
    }


    @Test
    void testKeepRatioPortraitThumbnail() throws IOException {
        BufferedImage source = ImageIO.read(portraitTestImagePath.toFile());
        BufferedImage image = operations.performImageAction(source, ThumbnailAction.withSize(256).withType(KEEP_RATIO));
        File outputFile = Path.of(outputPath.toString(), "test-thumbnail-256-ratio-portait.png").toFile();
        ImageIO.write(image, "png", outputFile);

        assertThat(image.getWidth()).isEqualTo(183);
        assertThat(image.getHeight()).isEqualTo(256);
        assertThat(Files.exists(outputFile.toPath())).isTrue();
    }

}
