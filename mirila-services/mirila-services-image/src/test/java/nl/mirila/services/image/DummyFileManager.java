package nl.mirila.services.image;

import nl.mirila.core.datatype.Id;
import nl.mirila.files.core.FileManager;

import java.io.*;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

public class DummyFileManager implements FileManager {

    private final Path basePath;
    private final Path outputPath;

    public DummyFileManager() {
        try {
            basePath = Path.of(this.getClass().getResource(File.separator).toURI());
        } catch (URISyntaxException e) {
            throw new RuntimeException(e.getMessage());
        }
        outputPath = Path.of(basePath.getParent().toString(), "test-output");
        if (!Files.exists(outputPath)) {
            try {
                Files.createDirectory(outputPath);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void create(Id containerId, String fileName, File temporaryFile) {
        // no-op
    }

    @Override
    public void create(Id containerId, String fileName, InputStream inputStream) {
        Path filePath = Path.of(outputPath.toString(), fileName);
        try {
            Files.copy(inputStream, filePath, REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Optional<InputStream> read(Id containerId, String fileName) {
        // We ignore containerId.
        Path filePath = Path.of(basePath.toString(), fileName);
        try {
            return Optional.of(new FileInputStream(filePath.toFile()));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }

    @Override
    public void update(Id containerId, String fileName, File temporaryFile) {
        // no-op
    }

    @Override
    public void update(Id containerId, String fileName, InputStream inputStream) {
        // no-op
    }

    @Override
    public void delete(Id containerId, String fileName) {
        // no-op
    }

}
