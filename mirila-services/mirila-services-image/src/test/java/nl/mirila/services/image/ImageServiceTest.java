package nl.mirila.services.image;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import nl.mirila.core.datatype.Id;
import nl.mirila.core.settings.modules.KeyValuesSettingsModule;
import nl.mirila.files.core.FileManager;
import nl.mirila.messaging.core.MessagingCoreModule;
import nl.mirila.security.auth.core.contexts.GuestSecurityContext;
import nl.mirila.security.auth.core.contexts.SecurityContext;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.util.HashMap;

import static nl.mirila.core.settings.ApplicationSettings.KEY_APP_NAME;

class ImageServiceTest {

    private ImageService service;

    private Path testFile;
    private Path outputPath;

    @BeforeEach
    void setUp() throws URISyntaxException, IOException {

        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                HashMap<String, String> settings = new HashMap<>();
                settings.put(KEY_APP_NAME, "test-app");
                install(new KeyValuesSettingsModule(settings));
                install(new MessagingCoreModule());
                install(new ImageServiceModule());
                bind(SecurityContext.class).toInstance(new GuestSecurityContext("test-realm", "en_EN"));
                bind(FileManager.class).to(DummyFileManager.class);
            }
        });
        service = injector.getInstance(ImageService.class);
    }

    @Test
    void testSendImageAction() {
        service.createThumbnails(Id.of("dummy"), "landscape-test.jpg");

        //Path fileA = Path.of(this.getClass().getResource(File.separator + "landscape-test.jpg").getFile());
        //assertThat(Files.exists(fileA)).isTrue();
    }

}
