package nl.mirila.services.image;

import nl.mirila.services.image.actions.*;
import nl.mirila.services.image.enums.ThumbnailType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.imgscalr.Scalr;

import java.awt.image.BufferedImage;
import java.util.List;

/**
 * ImageOperations performs simple image processing operations onto images, based {@link ImageAction}.
 */
public class ImageOperations {

    private static final Logger logger = LogManager.getLogger(ImageOperations.class);

    /**
     * Perform multiple {@link ImageAction}, using the given {@link BufferedImage} as source. The resulted image is
     * returned.
     * <p>
     * @param image The source image.
     * @param actions The actions to perform.
     * @return The new image.
     */
    public BufferedImage performImageAction(BufferedImage image, List<ImageAction> actions) {
        for (ImageAction action : actions) {
            image = performImageAction(image, action);
        }
        return image;
    }

    /**
     * Perform a given {@link ImageAction}, using the given {@link BufferedImage} as source. The resulted image is
     * returned.
     * <p>
     * @param image The source image.
     * @param action The action to perform.
     * @return The new image.
     */
    public BufferedImage performImageAction(BufferedImage image, ImageAction action) {
        if (action instanceof CropAction cropAction) {
            return crop(image, cropAction);
        }
        if (action instanceof FlipAction flipAction) {
            return flip(image, flipAction);
        }
        if (action instanceof ResizeAction resizeAction) {
            return resize(image, resizeAction);
        }
        if (action instanceof RotateAction rotateAction) {
            return rotate(image, rotateAction);
        }
        if (action instanceof ThumbnailAction thumbnailAction) {
            return createThumbnail(image, thumbnailAction);
        }
        logger.warn("Unknown action {}. Skipping ...", action.getClass().getSimpleName());
        return image;
    }

    /**
     * Crop the given {@link BufferedImage} using the {@link CropAction}.
     * <p>
     * @param image The image.
     * @param action The crop information.
     * @return The cropped image.
     */
    private BufferedImage crop(BufferedImage image, CropAction action) {
        return Scalr.crop(image, action.getLeft(), action.getTop(), action.getWidth(), action.getHeight());
    }

    /**
     * Flip the given {@link BufferedImage} using the {@link FlipAction}.
     * <p>
     * @param image The image.
     * @param action The flip information.
     * @return The flipped image.
     */
    private BufferedImage flip(BufferedImage image, FlipAction action) {
        return action.getType().asScalrRotation()
                .map((rotation) -> Scalr.rotate(image, rotation))
                .orElse(image);
    }

    /**
     * Resize the given {@link BufferedImage} using the {@link ResizeAction}.
     * <p>
     * @param image The image.
     * @param action The resize information.
     * @return The resized image.
     */
    private BufferedImage resize(BufferedImage image, ResizeAction action) {
        return Scalr.resize(image, action.getMaxSize());
    }

    /**
     * Rotate the given {@link BufferedImage} using the {@link RotateAction}.
     * <p>
     * @param image The image.
     * @param action The rotate information.
     * @return The rotated image.
     */
    private BufferedImage rotate(BufferedImage image, RotateAction action) {
        return action.getType().asScalrRotation()
                .map((rotation) -> Scalr.rotate(image, rotation))
                .orElse(image);
    }

    /**
     * Create a thumbnail for the given {@link BufferedImage} using the {@link ThumbnailAction}.
     * <p>
     * @param image The image.
     * @param action The thumbnail information.
     * @return The thumbnail.
     */
    private BufferedImage createThumbnail(BufferedImage image, ThumbnailAction action) {
        int size = action.getSize();
        if (action.getType() == ThumbnailType.KEEP_RATIO) {
            return Scalr.resize(image, size);
        } else {
            boolean isHorizontal = (image.getWidth() > image.getHeight());
            Scalr.Mode mode = (isHorizontal) ? Scalr.Mode.FIT_TO_HEIGHT : Scalr.Mode.FIT_TO_WIDTH;
            BufferedImage resizedImage = Scalr.resize(image, mode, size);
            int left = (resizedImage.getWidth() - size) / 2;
            int top = (resizedImage.getHeight() - size) / 2;
            return Scalr.crop(resizedImage, left, top, size, size);
        }
    }

}
