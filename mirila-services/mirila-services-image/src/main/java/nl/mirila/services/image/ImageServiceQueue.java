package nl.mirila.services.image;

import nl.mirila.messaging.core.MessageQueue;

/**
 * The ImageServiceQueue represents a queue on the messaging system for that is used for the image service.
 */
public class ImageServiceQueue extends MessageQueue {

    /**
     * Initialize a new instance.
     */
    protected ImageServiceQueue() {
        super("image-service");
    }

}
