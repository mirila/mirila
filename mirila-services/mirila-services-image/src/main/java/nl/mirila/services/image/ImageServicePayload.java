package nl.mirila.services.image;

import com.fasterxml.jackson.annotation.JsonProperty;
import nl.mirila.core.datatype.Id;
import nl.mirila.messaging.core.MessagePayload;

import java.util.ArrayList;
import java.util.List;

/**
 * The information that is needed in the {@link MessagePayload} for the {@link ImageServiceProcessor}.
 */
public class ImageServicePayload implements MessagePayload {

    @JsonProperty
    private Id sourceContainerId;

    @JsonProperty
    private String sourceFileName;

    @JsonProperty
    private final List<ImageServiceTarget> targets;

    /**
     * Initialize a new instance.
     */
    protected ImageServicePayload() {
        targets = new ArrayList<>();
    }

    /**
     * Return the container id of the source file.
     */
    public Id getSourceContainerId() {
        return sourceContainerId;
    }

    /**
     * Set the given container id of the source file and return this instance for chaining purposes.
     */
    public ImageServicePayload withContainerId(Id containerId) {
        this.sourceContainerId = containerId;
        return this;
    }

    /**
     * Return the name of the source file.
     */
    public String getSourceFileName() {
        return sourceFileName;
    }

    /**
     * Set the given name of the source file and return this instance for chaining purposes.
     */
    public static ImageServicePayload withFileName(String fileName) {
        ImageServicePayload payload = new ImageServicePayload();
        payload.sourceFileName = fileName;
        return payload;
    }

    /**
     * Return the imaging target actions.
     */
    public List<ImageServiceTarget> getTargets() {
        return targets;
    }

    /**
     * Add the given actions.
     */
    public ImageServicePayload withTargets(List<ImageServiceTarget> targets) {
        this.targets.addAll(targets);
        return this;
    }

    /**
     * Add the given action.
     */
    public ImageServicePayload withTarget(ImageServiceTarget target) {
        this.targets.add(target);
        return this;
    }

}
