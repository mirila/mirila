package nl.mirila.services.image.enums;

import org.imgscalr.Scalr;

import java.util.Optional;
import java.util.stream.Stream;

/**
 * The flip types that are supported.
 */
public enum FlipType {

    HORIZONTAL("horizontal"),
    VERTICAL("vertical");

    private final String name;

    /**
     * Initialize a new instance.
     */
    FlipType(String name) {
        this.name = name;
    }

    /**
     * Return the name of the enum.
     */
    @Override
    public String toString() {
        return name;
    }

    /**
     * Find the FlipType, using the given name. If not found, an empty {@link Optional} is returned.
     * <p>
     * @param name The name of the RotationType to find.
     * @return The FlipType, wrapped in an {@link Optional}.
     */
    public static Optional<FlipType> findByName(String name) {
        return Stream.of(FlipType.values())
                .filter((type) -> type.toString().equals(name))
                .findFirst();
    }

    /**
     * Return the {@link Scalr.Rotation} that is related to this enum.
     */
    public Optional<Scalr.Rotation> asScalrRotation() {
        return switch (findByName(name).orElseThrow()) {
            case HORIZONTAL -> Optional.of(Scalr.Rotation.FLIP_HORZ);
            case VERTICAL -> Optional.of(Scalr.Rotation.FLIP_VERT);
            default -> Optional.empty();
        };
    }

}
