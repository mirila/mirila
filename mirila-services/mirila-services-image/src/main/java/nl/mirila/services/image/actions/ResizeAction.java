package nl.mirila.services.image.actions;

import com.fasterxml.jackson.annotation.JsonProperty;

import static nl.mirila.services.image.enums.ActionType.RESIZE;

/**
 * A wrapper for the data that is needed for resizing images.
 */
public class ResizeAction extends ImageAction {

    @JsonProperty
    private int maxSize;

    /**
     * Initialize a new instance.
     */
    protected ResizeAction() {
        super(RESIZE);
    }

    /**
     * Return the maxSize.
     */
    public int getMaxSize() {
        return maxSize;
    }

    /**
     * Set the given maxSize and return this instance for chaining purposes.
     */
    public static ResizeAction withMaxSize(int maxSize) {
        ResizeAction action = new ResizeAction();
        action.maxSize = maxSize;
        return action;
    }

}
