package nl.mirila.services.image.actions;

import nl.mirila.services.image.enums.ThumbnailType;

import static nl.mirila.services.image.enums.ActionType.THUMBNAIL;

/**
 * A wrapper for the data that is needed for create thumbnails of images.
 */
public class ThumbnailAction extends ImageAction {

    private int size;
    private ThumbnailType type;

    /**
     * Initialize a new instance.
     */
    protected ThumbnailAction() {
        super(THUMBNAIL);
        size = 512;
        type = ThumbnailType.SQUARE;
    }

    /**
     * Return the size.
     */
    public int getSize() {
        return size;
    }

    /**
     * Set the given size.
     */
    public static ThumbnailAction withSize(int size) {
        ThumbnailAction action = new ThumbnailAction();
        action.size = size;
        return action;
    }

    /**
     * Return the thumbnail type.
     */
    public ThumbnailType getType() {
        return type;
    }

    /**
     * Set the given thumbnail type and return this instance for chaining purposes.
     */
    public ThumbnailAction withType(ThumbnailType type) {
        this.type = type;
        return this;
    }

}
