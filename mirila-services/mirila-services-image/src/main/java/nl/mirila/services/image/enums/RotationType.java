package nl.mirila.services.image.enums;

import org.imgscalr.Scalr;

import java.util.Optional;
import java.util.stream.Stream;

/**
 * The rotation types that are supported.
 */
public enum RotationType {

    NONE("none"),

    ROTATE_90("rotate-90"),
    ROTATE_180("rotate-180"),
    ROTATE_270("rotate-270");

    private final String name;

    /**
     * Initialize a new instance.
     */
    RotationType(String name) {
        this.name = name;
    }

    /**
     * Return the name of the enum.
     */
    @Override
    public String toString() {
        return name;
    }

    /**
     * Find the RotationType, using the given name. If not found, an empty {@link Optional} is returned.
     * <p>
     * @param name The name of the RotationType to find.
     * @return The RotationType, wrapped in an {@link Optional}.
     */
    public static Optional<RotationType> findByName(String name) {
        return Stream.of(RotationType.values())
                .filter((value) -> value.toString().equals(name))
                .findFirst();
    }

    /**
     * Return the {@link Scalr.Rotation} that is related to this enum.
     */
    public Optional<Scalr.Rotation> asScalrRotation() {
        return switch (findByName(name).orElseThrow()) {
            case ROTATE_90 -> Optional.of(Scalr.Rotation.CW_90);
            case ROTATE_180 -> Optional.of(Scalr.Rotation.CW_180);
            case ROTATE_270 -> Optional.of(Scalr.Rotation.CW_270);
            default -> Optional.empty();
        };
    }

}

