package nl.mirila.services.image.actions;

import com.fasterxml.jackson.annotation.JsonProperty;

import static nl.mirila.services.image.enums.ActionType.CROP;

/**
 * A wrapper for the data that is needed for cropping images.
 */
public class CropAction extends ImageAction {

    @JsonProperty
    private int left;

    @JsonProperty
    private int top;

    @JsonProperty
    private int width;

    @JsonProperty
    private int height;

    /**
     * Initialize a new instance.
     */
    protected CropAction() {
        super(CROP);
    }

    /**
     * Return a new crop action, with the given top.
     */
    public static CropAction withLeft(int left) {
        CropAction action = new CropAction();
        action.left = left;
        return action;
    }

    /**
     * Return the left.
     */
    public int getLeft() {
        return left;
    }

    /**
     * Return the top.
     */
    public int getTop() {
        return top;
    }

    /**
     * Set the given left and return this instance for chaining purposes.
     */
    public CropAction withTop(int top) {
        this.top = top;
        return this;
    }

    /**
     * Return the width.
     */
    public int getWidth() {
        return width;
    }

    /**
     * Set the given width and return this instance for chaining purposes.
     */
    public CropAction withWidth(int width) {
        this.width = width;
        return this;
    }

    /**
     * Return the height.
     */
    public int getHeight() {
        return height;
    }

    /**
     * Set the given height and return this instance for chaining purposes.
     */
    public CropAction withHeight(int height) {
        this.height = height;
        return this;
    }

}
