package nl.mirila.services.image;

import com.google.inject.Inject;
import nl.mirila.core.exceptions.MirilaException;
import nl.mirila.files.core.FileManager;
import nl.mirila.messaging.core.Message;
import nl.mirila.messaging.core.MessageProcessor;
import nl.mirila.messaging.core.MessageQueue;
import org.apache.commons.io.FilenameUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * The ImageServiceProcessor processes messages from the messaging system, fetching the images from the
 * {@link FileManager} an
 */
public class ImageServiceProcessor implements MessageProcessor<ImageServicePayload> {

    private final ImageServiceQueue queue;
    private final ImageOperations operations;
    private final FileManager manager;

    /**
     * Initialize a new instance.
     */
    @Inject
    public ImageServiceProcessor(ImageServiceQueue queue, ImageOperations operations, FileManager manager) {
        this.queue = queue;
        this.operations = operations;
        this.manager = manager;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MessageQueue getQueue() {
        return queue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Class<ImageServicePayload> getPayloadClass() {
        return ImageServicePayload.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void process(Message<ImageServicePayload> message) {
        ImageServicePayload payload = message.getPayload();
        InputStream inputStream = manager.read(payload.getSourceContainerId(), payload.getSourceFileName())
                .orElseThrow(() -> new MirilaException("File not found"));

        List<ImageServiceTarget> targets = payload.getTargets();
        if (targets.size() == 1) {
            processTarget(inputStream, targets.get(0));
        } else if (!targets.isEmpty()) {
            processTargets(inputStream, targets);
        }
    }

    /**
     * Processes the list of {@link ImageServiceTarget}, using the given {@link InputStream} as the source of the image.
     * <p>
     * Each target represents a different file. This makes it possible to create multiple copies of one source image,
     * for example, when creating multiple thumbnails with different sizes. In order to do this, the given input stream
     * is buffered in memory.
     * <p>
     * @param inputStream The input stream, containing the image.
     * @param targets The targets and their actions.
     */
    private void processTargets(InputStream inputStream, List<ImageServiceTarget> targets) {
        try (ByteArrayOutputStream cache = new ByteArrayOutputStream()) {
            inputStream.transferTo(cache);
            inputStream.close();

            targets.forEach((target) -> {
                try (InputStream cachedInputStream = new ByteArrayInputStream(cache.toByteArray())) {
                    processTarget(cachedInputStream, target);
                } catch (IOException e) {
                    throw new MirilaException(e.getMessage());
                }
            });
        } catch (IOException e) {
            throw new MirilaException(e.getMessage());
        }
    }

    /**
     * Processes the given {@link ImageServiceTarget}, using the given {@link InputStream} as the source of the image.
     * <p>
     * The target represents a different file target. This makes it possible to create multiple copies of one source
     * image, for example, when creating multiple thumbnails with different sizes.
     * <p>
     * @param inputStream The input stream, containing the image.
     * @param target The target and its actions.
     */
    private void processTarget(InputStream inputStream, ImageServiceTarget target) {
        String extension = FilenameUtils.getExtension(target.getFileName());
        try (ByteArrayOutputStream buffer = new ByteArrayOutputStream()){
            BufferedImage processedImage = operations.performImageAction(ImageIO.read(inputStream), target.getActions());
            ImageIO.write(processedImage, extension, buffer);
            createFileTarget(buffer, target);
        } catch (IOException e) {
            throw new MirilaException(e.getMessage());
        }
    }

    /**
     * Create the file based on the given target.
     * <p>
     * @param buffer The buffer that contains the file data.
     * @param target The target and its actions.
     */
    private void createFileTarget(ByteArrayOutputStream buffer, ImageServiceTarget target) {
        try (ByteArrayInputStream processedInputStream = new ByteArrayInputStream(buffer.toByteArray())) {
            manager.create(target.getContainerId(), target.getFileName(), processedInputStream);
        } catch (IOException e) {
            throw new MirilaException(e.getMessage());
        }
    }

}
