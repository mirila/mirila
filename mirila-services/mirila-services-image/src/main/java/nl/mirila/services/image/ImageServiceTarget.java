package nl.mirila.services.image;

import com.fasterxml.jackson.annotation.JsonProperty;
import nl.mirila.core.datatype.Id;
import nl.mirila.services.image.actions.ImageAction;

import java.util.ArrayList;
import java.util.List;

public class ImageServiceTarget {

    @JsonProperty
    private Id containerId;

    @JsonProperty
    private String fileName;

    @JsonProperty
    private final List<ImageAction> actions;

    /**
     * Initialize a new instance.
     */
    protected ImageServiceTarget() {
        actions = new ArrayList<>();
    }

    /**
     * Return the container id of the target file.
     */
    public Id getContainerId() {
        return containerId;
    }

    /**
     * Set the given container id of the target file and return this instance for chaining purposes.
     */
    public ImageServiceTarget withContainerId(Id containerId) {
        this.containerId = containerId;
        return this;
    }

    /**
     * Return the name of the target file.
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * Set the given name of the target file and return this instance for chaining purposes.
     */
    public static ImageServiceTarget withFileName(String fileName) {
        ImageServiceTarget targetActions = new ImageServiceTarget();
        targetActions.fileName = fileName;
        return targetActions;
    }

    /**
     * Return the imaging actions.
     */
    public List<ImageAction> getActions() {
        return actions;
    }

    /**
     * Add the given actions.
     */
    public ImageServiceTarget withActions(List<ImageAction> actions) {
        this.actions.addAll(actions);
        return this;
    }

    /**
     * Add the given action.
     */
    public ImageServiceTarget withAction(ImageAction action) {
        this.actions.add(action);
        return this;
    }

}
