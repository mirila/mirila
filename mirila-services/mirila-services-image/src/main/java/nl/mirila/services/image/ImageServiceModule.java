package nl.mirila.services.image;

import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import com.google.inject.multibindings.Multibinder;
import nl.mirila.messaging.core.MessagePayload;
import nl.mirila.messaging.core.MessageProcessor;

/**
 * The ImageServiceModule binds the modules that are needed for the image service, especially related to the messaging
 * system.
 */
public class ImageServiceModule extends AbstractModule {

    @Override
    protected void configure() {
        Multibinder<MessageProcessor<? extends MessagePayload>> consumerBinder = Multibinder
                .newSetBinder(binder(), new TypeLiteral<>() {});
        consumerBinder.addBinding().to(ImageServiceProcessor.class);
    }

}
