package nl.mirila.services.image.actions;

import com.fasterxml.jackson.annotation.JsonProperty;
import nl.mirila.services.image.enums.FlipType;

import static nl.mirila.services.image.enums.ActionType.ROTATE;

/**
 * A wrapper for the data that is needed for rotating images.
 */
public class FlipAction extends ImageAction {

    @JsonProperty
    private FlipType type;

    /**
     * Initialize a new instance.
     */
    protected FlipAction() {
        super(ROTATE);
    }

    /**
     * Return the type.
     */
    public FlipType getType() {
        return type;
    }

    /**
     * Return a new {@link FlipAction} with the given type.
     */
    public static FlipAction withType(FlipType type) {
        FlipAction action = new FlipAction();
        action.type = type;
        return action;
    }

}
