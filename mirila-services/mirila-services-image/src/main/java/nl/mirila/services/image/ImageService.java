package nl.mirila.services.image;

import com.google.inject.Inject;
import nl.mirila.core.datatype.Id;
import nl.mirila.messaging.core.MessageProducer;
import nl.mirila.services.image.actions.ImageAction;
import nl.mirila.services.image.actions.ThumbnailAction;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;

import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;

/**
 * The ImagingService performs image processing, either directly, or via a messaging service.
 */
public class ImageService {

    private final MessageProducer producer;
    private final ImageServiceQueue queue;
    private final ImageOperations operations;

    /**
     * Initialize a new instance.
     */
    @Inject
    public ImageService(MessageProducer producer,
                        ImageServiceQueue queue,
                        ImageOperations operations) {
        this.producer = producer;
        this.queue = queue;
        this.operations = operations;
    }

    /**
     * Send the given {@link ImageServicePayload} to the messaging system.
     * <p>
     * @param payload The payload.
     */
    public void sendPayload(ImageServicePayload payload) {
        producer.sendPayload(queue, payload);
    }

    /**
     * Change the image with the given file name, in the given container, using the list of {@link ImageAction}.
     * <p>
     * @param containerId The id of the container.
     * @param fileName The name of the file.
     * @param actions The list of actions.
     */
    public void changeImage(Id containerId, String fileName, List<ImageAction> actions) {
       ImageServiceTarget target = ImageServiceTarget.withFileName(fileName)
               .withContainerId(containerId)
               .withActions(actions);

        ImageServicePayload payload = ImageServicePayload.withFileName(fileName)
                .withContainerId(containerId)
                .withTarget(target);

        sendPayload(payload);
    }

    /**
     * Change the image with the given file name, in the given container, using the list of {@link ImageAction}.
     * <p>
     * @param sourceContainerId The container id of the source image.
     * @param sourceFileName The file name of the source image.
     * @param targets The targets.
     */
    public void changeImageCopy(Id sourceContainerId,
                                String sourceFileName,
                                List<ImageServiceTarget> targets) {
        ImageServicePayload payload = ImageServicePayload.withFileName(sourceFileName)
                .withContainerId(sourceContainerId)
                .withTargets(targets);

        sendPayload(payload);
    }

    /**
     * Change the image with the given file name, in the given container, using the list of {@link ImageAction}.
     * <p>
     * @param sourceContainerId The container id of the source image.
     * @param sourceFileName The file name of the source image.
     * @param targetContainerId The container id of the target image.
     * @param targetFileName The file name of the target image.
     * @param actions The list of actions.
     */
    public void changeImageCopy(Id sourceContainerId,
                                String sourceFileName,
                                Id targetContainerId,
                                String targetFileName,
                                List<ImageAction> actions) {
        ImageServiceTarget target = ImageServiceTarget.withFileName(targetFileName)
                .withContainerId(targetContainerId)
                .withActions(actions);

        changeImageCopy(sourceContainerId, sourceFileName, Collections.singletonList(target));
    }

    /**
     * Create thumbnails for the image with the given source file name, in the source container id. The target files
     * are stored in the target container id, with a postfix of the size in the thumbnail file names.
     * <p>
     * @param sourceContainerId The container id of the source file.
     * @param sourceFileName The name of the source file.
     */
    public void createThumbnails(Id sourceContainerId, String sourceFileName) {
        createThumbnails(sourceContainerId, sourceFileName, sourceContainerId);
    }

    /**
     * Create thumbnails for the image with the given source file name, in the source container id. The target files
     * are stored in the target container id, with a postfix of the size in the thumbnail file names.
     * <p>
     * @param sourceContainerId The container id of the source file.
     * @param sourceFileName The name of the source file.
     * @param targetContainerId The container id of the target file.
     */
    public void createThumbnails(Id sourceContainerId, String sourceFileName, Id targetContainerId) {
        List<ImageServiceTarget> targets = IntStream.of(128, 256)
                .mapToObj((size) -> new ImmutablePair<>(size, getThumbnailName(sourceFileName, size)))
                .map((pair) -> ImageServiceTarget.withFileName(pair.getRight())
                        .withContainerId(targetContainerId)
                        .withAction(ThumbnailAction.withSize(pair.getLeft())))
                .toList();

        changeImageCopy(sourceContainerId, sourceFileName, targets);
    }

    /**
     * Return the file name for a thumbnail with the given size.
     * <p>
     * @param fileName The file name.
     * @param size The size.
     * @return The thumbnail file name.
     */
    private String getThumbnailName(String fileName, int size) {
        String extension = FilenameUtils.getExtension(fileName);
        String baseName = FilenameUtils.getBaseName(fileName);
        return baseName + "." + size + "x" + size + "." + extension;
    }

}
