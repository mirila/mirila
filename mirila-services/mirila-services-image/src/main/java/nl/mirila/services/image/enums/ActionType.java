package nl.mirila.services.image.enums;

import java.util.Optional;
import java.util.stream.Stream;

/**
 * The types of action that are implemented.
 */
public enum ActionType {

    CROP("crop"),
    FLIP("flip"),
    RESIZE("resize"),
    ROTATE("rotate"),
    THUMBNAIL("thumbnail");

    private final String name;

    /**
     * Initialize a new instance.
     */
    ActionType(String name) {
        this.name = name;
    }

    /**
     * Return the name of the enum.
     */
    @Override
    public String toString() {
        return name;
    }

    /**
     * Find the ActionType, using the given name. If not found, an empty {@link Optional} is returned.
     * <p>
     * @param name The name of the ActionType to find.
     * @return The ActionType, wrapped in an {@link Optional}.
     */
    public static Optional<ActionType> findByName(String name) {
        return Stream.of(ActionType.values())
                .filter((type) -> type.toString().equals(name))
                .findFirst();
    }

}
