package nl.mirila.services.image.enums;

/**
 * The flip types that are supported.
 */
public enum ThumbnailType {

    SQUARE("square"),
    KEEP_RATIO("keep-ratio");

    private final String type;

    /**
     * Initialize a new instance.
     */
    ThumbnailType(String type) {
        this.type = type;
    }

    /**
     * Return the name of the enum.
     */
    @Override
    public String toString() {
        return type;
    }

}
