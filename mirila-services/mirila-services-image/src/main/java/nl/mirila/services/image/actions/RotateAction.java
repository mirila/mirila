package nl.mirila.services.image.actions;

import com.fasterxml.jackson.annotation.JsonProperty;
import nl.mirila.services.image.enums.RotationType;

import static nl.mirila.services.image.enums.ActionType.ROTATE;

/**
 * A wrapper for the data that is needed for rotating images.
 */
public class RotateAction extends ImageAction {

    @JsonProperty
    private RotationType type;

    /**
     * Initialize a new instance.
     */
    protected RotateAction() {
        super(ROTATE);
    }

    /**
     * Return the type.
     */
    public RotationType getType() {
        return type;
    }

    /**
     * Return a new {@link RotateAction} with the given type.
     */
    public static RotateAction withType(RotationType type) {
        RotateAction action = new RotateAction();
        action.type = type;
        return action;
    }

}
