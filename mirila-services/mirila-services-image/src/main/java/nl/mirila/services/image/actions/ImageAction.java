package nl.mirila.services.image.actions;

import com.fasterxml.jackson.annotation.JsonProperty;
import nl.mirila.services.image.enums.ActionType;

/**
 * A wrapper for the data that is needed for specific image processing actions.
 */
public abstract class ImageAction {

    @JsonProperty
    private final ActionType actionType;

    /**
     * Initialize a new instance.
     */
    protected ImageAction(ActionType actionType) {
        this.actionType = actionType;
    }

    /**
     * Return the action type.
     */
    public ActionType getActionType() {
        return actionType;
    }

}
