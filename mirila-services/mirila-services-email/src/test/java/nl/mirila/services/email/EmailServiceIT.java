package nl.mirila.services.email;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import nl.mirila.core.settings.modules.KeyValuesSettingsModule;
import nl.mirila.drivers.smtp.SmtpSettings;
import nl.mirila.messaging.core.MessagingCoreModule;
import nl.mirila.security.auth.core.contexts.GuestSecurityContext;
import nl.mirila.security.auth.core.contexts.SecurityContext;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static nl.mirila.core.settings.ApplicationSettings.KEY_APP_NAME;
import static org.assertj.core.api.Assertions.assertThat;

class EmailServiceIT {

    private EmailService sender;
    private SmtpSettings settings;
    private EmailServiceProcessor processor;

    @BeforeEach
    void setUp() {

        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                Map<String, String> settings = new HashMap<>();
                settings.put(KEY_APP_NAME, "test-app");

                install(new KeyValuesSettingsModule(settings));
                install(new MessagingCoreModule());
                install(new EmailServiceProcessorModule());
                bind(SecurityContext.class).toInstance(new GuestSecurityContext("test-realm", "en_EN"));
            }
        });
        sender = injector.getInstance(EmailService.class);
        settings = injector.getInstance(SmtpSettings.class);
        processor = injector.getInstance(EmailServiceProcessor.class);
    }

    @Test
    void testSendMailPayload() {
        String address = settings.getDefaultFromAddress();
        EmailServicePayload payload = EmailServicePayload.withTo(address)
                .withFrom(address)
                .withSubject("MailServiceSender test")
                .withText("This is a test from the MailServiceSender.");

        sender.sendMailPayload(payload);
        assertThat(processor.getLastSentMail())
                .isPresent()
                .get()
                .extracting("subject")
                .isEqualTo("MailServiceSender test");
    }

}
