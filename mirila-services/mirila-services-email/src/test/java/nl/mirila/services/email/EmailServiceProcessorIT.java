package nl.mirila.services.email;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import nl.mirila.core.settings.modules.KeyValuesSettingsModule;
import nl.mirila.drivers.smtp.SmtpSettings;
import nl.mirila.messaging.core.Message;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;

import static org.assertj.core.api.Assertions.assertThat;

class EmailServiceProcessorIT {

    private EmailServiceProcessor processor;
    private SmtpSettings settings;

    @BeforeEach
    void setUp() {
        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                install(new KeyValuesSettingsModule(new HashMap<>()));

            }
        });
        processor = injector.getInstance(EmailServiceProcessor.class);
        settings = injector.getInstance(SmtpSettings.class);
    }

    /**
     * <p>
     */
    @Test
    void testProcess() {
        String address = settings.getDefaultFromAddress();
        EmailServicePayload payload = EmailServicePayload.withTo(address)
                .withFrom(address)
                .withSubject("MailServiceProcessor test")
                .withText("This is a test from the MailServiceProcessor.");

        Message<EmailServicePayload> message = Message.forPayload(payload);
        processor.process(message);

        assertThat(processor.getLastSentMail())
                .isPresent()
                .get()
                .extracting("subject")
                .isEqualTo("MailServiceProcessor test");
    }

    @Test
    void testAttachmentFile() throws URISyntaxException {
        URL resource = Thread.currentThread().getContextClassLoader().getResource("test-pattern.jpg");
        assertThat(resource).isNotNull();

        String address = settings.getDefaultFromAddress();
        EmailServicePayload payload = EmailServicePayload.withTo(address)
                .withFrom(address)
                .withSubject("MailServiceProcessor file attachment test")
                .withText("This is a file attachment test from the MailServiceProcessor.")
                .withAttachment("test.jpg", resource.toURI());

        Message<EmailServicePayload> message = Message.forPayload(payload);
        processor.process(message);

        assertThat(processor.getLastSentMail())
                .isPresent()
                .get()
                .satisfies((mail) -> {
                    assertThat(mail.getSubject()).isEqualTo("MailServiceProcessor file attachment test");
                    assertThat(mail.getPlainText())
                            .isEqualTo("This is a file attachment test from the MailServiceProcessor.");
                    assertThat(mail.getAttachments()).hasSize(1);
                });
    }

    @Test
    void testAttachmentBase64() {
        String base64 = "R0lGODlhEAAQAHcAACwAAAAAEAAQAKf///85OTnclZXKysptbW08PDw2NjZCQkIzMzNISEjLy8tcXFZwcHA/Pz/" +
                "Y2P9cXFycnJzZ2dmzs7Ph4eHe3t7Pz8/a2tp9fX3MzMuysrT+/f/Si5XLhZXV1f/Fxf+Tk5PJyf/v6/+ysqx9fXRbYFu1td" +
                "98fHmamrLk5eahWlrs4P/N0//k5P/17P/ZiZHZkJX89v/w6O6pYmWrbn709P/o6PZZWVZaWlqVlajLy+iFhYXZ3P/l6P/Bw" +
                "b7X1+VcXFlZWVllZWXk5OTCwsnExMTq6urb2///5P//5v//+f+oqMybm5z39/eAgH34+//Q0NBZWVe2tsz+/v7x8f9mZmPH" +
                "x8eioaBqamphYWGhWVnEfX1DQ0Py8vKenp6fn589fLxIQjwAG3cACmvs7OzIyMuuZ2fo5+aSkpIwMDDMzMzJyclNTU2hoaF" +
                "gYGC2trb7+/v9/f1SUlI+Pj7n5+egoKBKSkp5eXkAHnqj2f+h2f/39PFkYWHNzc1jY2Opr7o/PzxIRT/p6enE0eIwUIf4+P" +
                "/+/v+Hh5jo6P9vb20yQFdtq+FCPzkAKWwAHoMSMWIXMV99enZpZl1ZWVOLi4gJjPsAffVPXmYWhOcAAAAAAAAAAAAAAAAAA" +
                "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" +
                "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" +
                "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" +
                "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" +
                "AAAAAAAAAAAAAAAAAAI/wABsElQIIBBgwUOSEgwAIDDAQUqULBgIUKECRcOrDlQAYCbNmjOKFAwYICaNHQQzLHzAECCOnDe" +
                "yAlgoKaBAHG4bPECYECDLhcQYCFAlOiVBwccAtCShcwYAQIcQgVgpkxUh2C+ABAT5k4jRo8cJUKkFMAiRQAqUbpkKRKkSZL" +
                "68FH6xw+APHj26CkEgNAhQwsCOQQ0CIAgKyIctlCxgsUJEwAEpEABYIaMGjReuIgBg8MGDA4LZHA4ogSIEB9IaPDQwYbDBk" +
                "sAMGmixIGDI0iSOJlCxaEEBFWkPIESxcgOHg57QFCqo2CAGwtw5PAxhAiDsgCKUJggZACDH0CCLAMHEBAAOw==";

        String address = settings.getDefaultFromAddress();
        EmailServicePayload payload = EmailServicePayload.withTo(address)
                .withFrom(address)
                .withSubject("MailServiceProcessor Base64 attachment test")
                .withText("This is a Base64 attachment test from the MailServiceProcessor.")
                .withAttachment("test.jpg", base64);

        Message<EmailServicePayload> message = Message.forPayload(payload);
        processor.process(message);

        assertThat(processor.getLastSentMail())
                .isPresent()
                .get()
                .extracting("subject")
                .isEqualTo("MailServiceProcessor Base64 attachment test");
    }

}
