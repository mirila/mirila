package nl.mirila.services.email;

import nl.mirila.messaging.core.MessagePayload;
import org.apache.commons.lang3.StringUtils;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

/**
 * The {@link MessagePayload} that is used by the {@link EmailService}.
 */
public class EmailServicePayload implements MessagePayload {

    private String to;
    private String from;
    private String cc;
    private String bcc;
    private String subject;
    private String text;
    private String html;
    private final Map<String, String> attachments;

    /**
     * Initialize a new instance.
     */
    public EmailServicePayload() {
        attachments = new HashMap<>();
    }

    /**
     * Return the to.
     */
    public String getTo() {
        return to;
    }

    /**
     * Return the from.
     */
    public String getFrom() {
        return from;
    }

    /**
     * Return the cc.
     */
    public String getCc() {
        return cc;
    }

    /**
     * Return the bcc.
     */
    public String getBcc() {
        return bcc;
    }

    /**
     * Return the subject.
     */
    public String getSubject() {
        return subject;
    }

    /**
     * Return the text.
     */
    public String getText() {
        return text;
    }

    /**
     * Return the html.
     */
    public String getHtml() {
        return html;
    }

    /**
     * Return the attachments, keys containing filenames, values containing either a file uri or a Base64
     * string of the file.
     */
    public Map<String, String> getAttachments() {
        return attachments;
    }

    /**
     * Set the to and return this instance for chaining.
     */
    public static EmailServicePayload withTo(String to) {
        EmailServicePayload payload = new EmailServicePayload();
        payload.to = to;
        return payload;
    }

    /**
     * Set the from and return this instance for chaining.
     */
    public EmailServicePayload withFrom(String from) {
        this.from = from;
        return this;
    }

    /**
     * Set the cc and return this instance for chaining.
     */
    public EmailServicePayload withCc(String cc) {
        this.cc = cc;
        return this;
    }

    /**
     * Set the bcc and return this instance for chaining.
     */
    public EmailServicePayload withBcc(String bcc) {
        this.bcc = bcc;
        return this;
    }

    /**
     * Set the subject and return this instance for chaining.
     */
    public EmailServicePayload withSubject(String subject) {
        this.subject = subject;
        return this;
    }

    /**
     * Set the text and return this instance for chaining.
     */
    public EmailServicePayload withText(String text) {
        this.text = text;
        return this;
    }

    /**
     * Set the html and return this instance for chaining.
     */
    public EmailServicePayload withHtml(String html) {
        this.html = html;
        return this;
    }

    /**
     * Add the attachment with the given filename and data, and return this instance for chaining purposes.
     * <p>
     * The data should be either an uri, which is accessible from the processing service or a Base64 string containing
     * the data.
     */
    public EmailServicePayload withAttachment(String filename, String data) {
        if ((StringUtils.isNotBlank(filename)) && (StringUtils.isNotBlank(data))) {
            attachments.put(filename, data);
        }
        return this;
    }

    /**
     * Add the attachment with the given filename and file uri, and return this instance for chaining purposes.
     */
    public EmailServicePayload withAttachment(String filename, URI filePath) {
        return withAttachment(filename, filePath.toString());
    }

}
