package nl.mirila.services.email;

import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import com.google.inject.multibindings.Multibinder;
import nl.mirila.messaging.core.MessagePayload;
import nl.mirila.messaging.core.MessageProcessor;

/**
 * The EmailServiceProcessorModule binds all classes that are needed for processing {@link EmailServicePayload}.
 */
public class EmailServiceProcessorModule extends AbstractModule {

    @Override
    protected void configure() {
        Multibinder<MessageProcessor<? extends MessagePayload>> consumerBinder = Multibinder
                .newSetBinder(binder(), new TypeLiteral<>() {});
        consumerBinder.addBinding().to(EmailServiceProcessor.class);
    }

}
