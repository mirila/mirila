package nl.mirila.services.email;

import com.google.inject.Inject;
import nl.mirila.messaging.core.MessageProducer;

/**
 * The EmailService sends a {@link EmailServicePayload} to a messaging system.
 */
public class EmailService {

    private final MessageProducer producer;
    private final EmailServiceQueue queue;

    /**
     * Initialize a new instance.
     */
    @Inject
    public EmailService(MessageProducer producer, EmailServiceQueue queue) {
        this.producer = producer;
        this.queue = queue;
    }

    /**
     * Sends a basic, plain text email.
     * <p>
     * @param from The sender
     * @param to The recipient.
     * @param subject The subject of the email.
     * @param textBody The body text of the email.
     */
    public void sendSimpleMail(String from, String to, String subject, String textBody) {
        EmailServicePayload payload = EmailServicePayload.withTo(to)
                .withFrom(from)
                .withSubject(subject)
                .withText(textBody);
        sendMailPayload(payload);
    }

    /**
     * Sends the given payload to a messaging system.
     * <p>
     * @param payload The payload.
     */
    public void sendMailPayload(EmailServicePayload payload) {
        producer.sendPayload(queue, payload);
    }

}
