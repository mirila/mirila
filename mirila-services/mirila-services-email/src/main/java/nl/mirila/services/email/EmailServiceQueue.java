package nl.mirila.services.email;

import nl.mirila.messaging.core.MessageQueue;

/**
 * The EmailServiceQueue represents a queue on the messaging system for that is used for the mail service.
 */
public class EmailServiceQueue extends MessageQueue {

    /**
     * Initialize a new instance.
     */
    protected EmailServiceQueue() {
        super("mail-service");
    }

}
