package nl.mirila.services.email;

import com.google.inject.Inject;
import jakarta.activation.DataSource;
import jakarta.mail.util.ByteArrayDataSource;
import nl.mirila.drivers.smtp.MissingSmtpConnectionException;
import nl.mirila.drivers.smtp.SmtpDriver;
import nl.mirila.drivers.smtp.SmtpSettings;
import nl.mirila.messaging.core.Message;
import nl.mirila.messaging.core.MessageProcessor;
import nl.mirila.messaging.core.MessageQueue;
import org.apache.commons.lang3.StringUtils;
import org.simplejavamail.api.email.Email;
import org.simplejavamail.api.email.EmailPopulatingBuilder;
import org.simplejavamail.email.EmailBuilder;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URLConnection;
import java.util.Base64;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

/**
 * The EmailServiceProcessor takes a {@link EmailServicePayload} and sends an email using the {@link SmtpDriver}.
 */
public class EmailServiceProcessor implements MessageProcessor<EmailServicePayload> {

    private static final AtomicReference<Email> lastSentMailReference = new AtomicReference<>();

    private final SmtpDriver driver;
    private final SmtpSettings settings;
    private final MessageQueue queue;


    /**
     * Initialize a new instance.
     */
    @Inject
    public EmailServiceProcessor(SmtpDriver driver, SmtpSettings settings, EmailServiceQueue queue) {
        this.driver = driver;
        this.settings = settings;
        this.queue = queue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MessageQueue getQueue() {
        return queue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Class<EmailServicePayload> getPayloadClass() {
        return EmailServicePayload.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void process(Message<EmailServicePayload> message) {
        lastSentMailReference.set(null);
        EmailServicePayload payload = message.getPayload();

        String from = (StringUtils.isNotBlank(payload.getFrom()))
                ? payload.getFrom()
                : settings.getDefaultFromAddress();

        EmailPopulatingBuilder builder = EmailBuilder.startingBlank()
                .from(from)
                .to(payload.getTo())
                .withSubject(payload.getSubject());

        if (StringUtils.isNotBlank(payload.getCc())) {
            builder.cc(payload.getCc());
        }

        if (StringUtils.isNotBlank(payload.getBcc())) {
            builder.bcc(payload.getBcc());
        }

        if (StringUtils.isNotBlank(payload.getText())) {
            builder.withPlainText(payload.getText());
        }

        if (StringUtils.isNotBlank(payload.getHtml())) {
            builder.withHTMLText(payload.getHtml());
        }
        payload.getAttachments().forEach((filename, data) -> {
            String mimeType = URLConnection.guessContentTypeFromName(filename);
            try {
                builder.withAttachment(filename, getDataSource(data, mimeType));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        Email email = builder.buildEmail();
        driver.getMailer(true)
                .orElseThrow(MissingSmtpConnectionException::new)
                .sendMail(email);
        lastSentMailReference.set(email);
    }

    /**
     * Return the last sent {@link Email}, wrapped in an {@link Optional}. This is ideal for testing purposes.
     * <p>
     * Each time a payload starts to get processed, this
     */
    public Optional<Email> getLastSentMail() {
        return Optional.ofNullable(lastSentMailReference.get());
    }

    /**
     * Return an {@link InputStream} for the given data.
     * <p>
     * The data contains either a filename or a Base64 encode string.
     * <p>
     * @param data The data.
     * @return The input stream.
     * <p>
     * @throws FileNotFoundException If the file can not be found.
     */
    private DataSource getDataSource(String data, String mimeType) throws FileNotFoundException {
        try {
            URI uri = URI.create(data);
            if (uri.getScheme().equals("file")) {
                return new ByteArrayDataSource(new FileInputStream(uri.getPath()), mimeType);
            } else {
                throw new FileNotFoundException(uri.getPath());
            }
        } catch (Exception ignore) {
        }
        return new ByteArrayDataSource(Base64.getDecoder().decode(data), mimeType);
    }

}
