package nl.mirila.dbs.sql.descriptor;

import com.google.inject.Inject;
import nl.mirila.model.core.references.Model;
import nl.mirila.model.management.descriptor.ModelEntityFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * The SqlQueryExecutor executes an sql query.
 * <p>
 * @param <M> The model.
 */
public class SqlQueryExecutor<M extends Model> {

    private static final Logger logger = LogManager.getLogger(SqlQueryExecutor.class);

    private final SqlModelDescriptor<M> sqlModelDescriptor;
    private final ModelEntityFactory<M> entityFactory;

    /**
     * Initialize a new instance.
     */
    @Inject
    public SqlQueryExecutor(SqlModelDescriptor<M> sqlModelDescriptor) {
        this.sqlModelDescriptor = sqlModelDescriptor;
        this.entityFactory = sqlModelDescriptor.getDescriptor().getEntityFactory();
    }

    /**
     * Execute the given statement and return a list of entities from the resulting set.
     * <p>
     * @param statement The statement to execute.
     * @return A list of entities.
     */
    public long executeCountStatement(PreparedStatement statement) {
        try (ResultSet set = statement.executeQuery()) {
            set.next();
            return set.getLong(1);
        } catch (SQLException e) {
            logger.error(e.getMessage());
            return 0;
        }
    }

    /**
     * Execute the given statement and return a list of entities from the resulting set.
     * <p>
     * @param statement The statement to execute.
     * @return A list of entities.
     */
    public List<M> executeStatement(PreparedStatement statement) {
        return executeStatement(statement, Collections.emptyList());
    }

    /**
     * Execute the given statement and return a list of entities from the resulting set, optionally converting less
     * fields, as they are not required.
     * <p>
     * @param statement The statement to execute.
     * @param fieldNames The fields to include in the entity. If the given list is empty, all fields are returned.
     * @return A list of entities.
     */
    public List<M> executeStatement(PreparedStatement statement, List<String> fieldNames) {
        ArrayList<M> list = new ArrayList<>();
        Set<Map.Entry<String, String>> entrySet = sqlModelDescriptor.getFieldNameAndColumnNames().entrySet();
        try (ResultSet set = statement.executeQuery()) {
            while (set.next()) {
                Map<String, Object> values = new HashMap<>();
                for (Map.Entry<String, String> entry : entrySet) {
                    String field = entry.getKey();
                    String column = entry.getValue();
                    if ((fieldNames.isEmpty()) || (fieldNames.contains(field))) {
                        values.put(field, set.getObject(column));
                    }
                }
                M entity = entityFactory.createInstanceWithValues(values);
                list.add(entity);
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        return list;
    }

}
