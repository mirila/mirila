package nl.mirila.dbs.sql.descriptor;

import java.sql.PreparedStatement;
import java.util.List;

/**
 * The SqlAndParameters combines the sql and parameters, that can be used for the {@link PreparedStatement}.
 */
public class SqlAndParameters {

    private final String sql;
    private final List<Object> parameters;

    /**
     * Initialize a new instance with the given parameters.
     * <p>
     * @param sql The sql template.
     * @param parameters The list of parameters.
     */
    public SqlAndParameters(String sql, List<Object> parameters) {
        this.sql = sql;
        this.parameters = parameters;
    }

    /**
     * Return the sql.
     */
    public String getSql() {
        return sql;
    }

    /**
     * Return the parameters.
     */
    public List<Object> getParameters() {
        return parameters;
    }

}
