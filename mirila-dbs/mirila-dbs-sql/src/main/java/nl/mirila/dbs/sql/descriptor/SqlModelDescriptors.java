package nl.mirila.dbs.sql.descriptor;


import com.google.inject.Inject;
import nl.mirila.model.core.references.Model;
import nl.mirila.model.management.descriptor.ModelDescriptor;
import nl.mirila.model.management.descriptor.ModelDescriptors;
import nl.mirila.model.management.exceptions.NonStorableModelException;
import nl.mirila.model.management.exceptions.UnknownModelException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The uses SqlModelDescriptors the {@link ModelDescriptors} to find all relevant models, and creates a
 * {@link SqlModelDescriptor} for each model. Each descriptor is stored in a static map, making sure that only
 * one SqlModelDescriptor is instantiated per model.
 */
public class SqlModelDescriptors {

    private static final Logger logger = LogManager.getLogger(SqlModelDescriptors.class);
    private static final Map<String, SqlModelDescriptor<? extends Model>> map = new HashMap<>();
    private static final List<String> nonStorableModels = new ArrayList<>();

    /**
     * Initializes a new instance, and creates the {@link SqlModelDescriptor} if not yet loaded.
     * <p>
     * @param descriptors The generic model descriptors.
     */
    @Inject
    public SqlModelDescriptors(ModelDescriptors descriptors) {
        if (map.isEmpty()) {
            descriptors.stream()
                    .filter(ModelDescriptor::isStorable)
                    .map(SqlModelDescriptor::new)
                    .forEach(this::registerSqlModel);

            descriptors.stream()
                    .filter((descriptor) -> !descriptor.isStorable())
                    .map(ModelDescriptor::getModelClass)
                    .map(Class::getSimpleName)
                    .forEach(nonStorableModels::add);
        }
    }

    /**
     * Register the given {@link SqlModelDescriptor}.
     */
    private void registerSqlModel(SqlModelDescriptor<? extends Model> sqlModelDescriptor) {
        String modelName = sqlModelDescriptor.getModelClass().getSimpleName();
        logger.info("Registered SQL model for {}.", modelName);
        map.put(modelName, sqlModelDescriptor);
    }

    /**
     * Return the {@link SqlModelDescriptor} for the given model.
     * <p>
     * @param modelClass The model.
     * @return The model descriptor.
     */
    public <M extends Model> SqlModelDescriptor<M> getSqlDescriptorForModel(Class<M> modelClass) {
        return getSqlDescriptorForClassName(modelClass.getSimpleName());
    }

    /**
     * Return the {@link ModelDescriptor} for the model with the given name.
     * <p>
     * @param simpleClassName The simple name of the model.
     * @return The model descriptor.
     * @param <M> The type of the model.
     */
    public <M extends Model> SqlModelDescriptor<M> getSqlDescriptorForClassName(String simpleClassName) {
        SqlModelDescriptor<?> sqlModelDescriptor = map.get(simpleClassName);
        if (sqlModelDescriptor == null) {
            if (nonStorableModels.contains(simpleClassName)) {
                throw new NonStorableModelException(simpleClassName);
            }
            throw new UnknownModelException(simpleClassName);
        }
        @SuppressWarnings("unchecked")
        SqlModelDescriptor<M> model = (SqlModelDescriptor<M>) sqlModelDescriptor;
        return model;
    }

    /**
     * Return the {@link SqlCrudQueryBuilder} for the given model.
     * <p>
     * @param modelClass The model.
     * @return The SQL CRUD query builder.
     */
    public <M extends Model> SqlCrudQueryBuilder<M> getCrudQueryBuilder(Class<M> modelClass) {
        return getSqlDescriptorForModel(modelClass).getCrudQueryBuilder();
    }

    /**
     * Return the {@link SqlSimpleQueryBuilder} for the given model.
     * <p>
     * @param modelClass The model.
     * @return The SQL simple query builder.
     */
    public <M extends Model> SqlSimpleQueryBuilder<M> getSimpleQueryBuilder(Class<M> modelClass) {
        return getSqlDescriptorForModel(modelClass).getSimpleQueryBuilder();
    }

    /**
     * Return the {@link SqlQueryExecutor} for the given model.
     * <p>
     * @param modelClass The model.
     * @return The SQL query executor.
     */
    public <M extends Model> SqlQueryExecutor<M> getQueryExecutor(Class<M> modelClass) {
        return getSqlDescriptorForModel(modelClass).getQueryExecutor();
    }

}
