package nl.mirila.dbs.sql;

import com.google.inject.AbstractModule;
import nl.mirila.dbs.sql.descriptor.SqlModelDescriptors;
import nl.mirila.drivers.sql.SqlDriverModule;
import nl.mirila.model.core.jackson.IdType;
import nl.mirila.model.management.managers.EntityPersistenceManager;
import org.jboss.resteasy.plugins.guice.RequestScoped;

public class SqlModule extends AbstractModule {

    @Override
    protected void configure() {
        install(new SqlDriverModule());

        bind(IdType.class).toInstance(IdType.INTEGER);
        bind(EntityPersistenceManager.class).to(SqlEntityManager.class).in(RequestScoped.class);
        bind(SqlModelDescriptors.class).asEagerSingleton();
    }

}
