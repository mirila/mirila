package nl.mirila.dbs.sql.descriptor;

import nl.mirila.core.exceptions.MirilaException;
import nl.mirila.model.core.enums.SortDirection;
import nl.mirila.model.core.references.Model;
import nl.mirila.model.management.exceptions.UnknownFilterException;
import nl.mirila.model.management.query.QueryParameters;
import nl.mirila.model.management.query.Sort;
import nl.mirila.model.management.query.filters.FieldComparisonFilter;
import nl.mirila.model.management.query.filters.Filter;
import nl.mirila.model.management.query.filters.FilterList;
import nl.mirila.model.management.query.filters.NoFilter;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static nl.mirila.model.management.query.filters.FilterList.MatchCondition.ALL;

/**
 * The SqlSimpleQueryBuilder builds queries for the given {@link Model}.
 * <p>
 * @param <M> The model.
 */
public class SqlSimpleQueryBuilder<M extends Model> {

    private final Map<String, String> fieldAndColumnNames;
    private final String tableName;

    private List<Object> parameters;

    /**
     * Initialize a new instance.
     * <p>
     * @param tableName The name of the table.
     * @param fieldAndColumnNames The map with field names and columns names.
     */
    public SqlSimpleQueryBuilder(String tableName, Map<String, String> fieldAndColumnNames) {
        this.tableName = tableName;
        this.fieldAndColumnNames = fieldAndColumnNames;
    }

    /**
     * Generate the sql for the given query, wrapped in a {@link SqlAndParameters}.
     * <p>
     * @param queryParameters The query
     * @return The query and the parameters.
     */
    public SqlAndParameters generateSql(QueryParameters<M> queryParameters) {
        return generateSql(queryParameters, false);
    }

    /**
     * Generate the sql for the given query, wrapped in a {@link SqlAndParameters}.
     * <p>
     * @param queryParameters The query
     * @param countOnly True if the query should return a count.
     * @return The query and the parameters.
     */
    public SqlAndParameters generateSql(QueryParameters<M> queryParameters, boolean countOnly) {
        parameters = new ArrayList<>();
        String wildcardOrCount = (countOnly) ? "count(*)" : "*";
        String sql = "SELECT " + wildcardOrCount + " FROM " + tableName + " ";

        // Filters
        if (!(queryParameters.getFilter() instanceof NoFilter))  {
            String where = filterToString(queryParameters.getFilter());
            if (StringUtils.isNotBlank(where)) {
                sql += "WHERE " + where + " ";
            }
        }

        // Sorting
        if (!countOnly) {
            String sort = getSortString(queryParameters.getSort());
            if (StringUtils.isNotBlank(sort)) {
                sql += "ORDER BY " + sort;
            }
        }
        return new SqlAndParameters(sql.trim(), parameters);
    }

    /**
     * Convert the given filter into a string of conditionals as can be used in the where clause of the query.
     * <p>
     * @param filter The filter.
     * @return The conditional string.
     */
    protected String filterToString(Filter filter) {
        if (filter instanceof FieldComparisonFilter comparisonFilter) {
            return filterComparisonFilterToString(comparisonFilter);
        } else if (filter instanceof FilterList filterList) {
            String filterString = filterListToString(filterList);
            return (StringUtils.isBlank(filterString)) ? "" : "(" + filterString + ")";
        } else {
            throw new UnknownFilterException(filter.getClass());
        }
    }

    /**
     * Convert the given field comparison filter into a string of conditionals as can be used in the where clause of
     * a query.
     * <p>
     * @param filter The field comparison filter.
     * @return The conditional string.
     */
    private String filterComparisonFilterToString(FieldComparisonFilter filter) {
        String column = "`" + getColumn(filter.getFieldName()) + "`";
        parameters.add(filter.getValue());
        return switch (filter.getOperator()) {
            case NEQ -> column + " NOT ?";
            case LT -> column + " < ?";
            case LTE -> column + " <= ?";
            case GTE -> column + " >= ?";
            case GT -> column + " > ?";
            default -> column + " = ?";
        };
    }

    /**
     * Convert the given field comparison filter into a string of conditionals as can be used in the where clause of
     * a query.
     * <p>
     * @param filter The field comparison filter.
     * @return The conditional string.
     */
    private String filterListToString(FilterList filter) {
        if (filter.getFilters().isEmpty()) {
            return "";
        }
        String operator = (filter.getMatchCondition() == ALL) ? " AND " : " OR ";
        return filter.getFilters().stream()
                .map(this::filterToString)
                .filter(StringUtils::isNotBlank)
                .collect(Collectors.joining(operator));
    }

    /**
     * Return the column for the given field. In case the field is actually a column, it will return that field
     * as column.
     * <p>
     * @param field The field.
     * @return The column.
     */
    private String getColumn(String field) {
        String column = fieldAndColumnNames.get(field);
        if (column != null) {
            return column;
        } else if (fieldAndColumnNames.containsValue(field)) {
            return field;
        }
        throw new MirilaException("Unknown column " + field);
    }

    /**
     * Convert the given {@link Sort} into a string of columns and sort direction, which can be used in the sort
     * clause of a query.
     * <p>
     * @param sort The sort.
     * @return The string.
     */
    private String getSortString(Sort sort) {
        List<String> sortFields = new ArrayList<>();
        sort.getFields().forEach((field, direction) -> {
            String column = "`" + getColumn(field) + "`";
            String directionString = (direction == SortDirection.DESCENDING) ? " desc" : "";
            sortFields.add(column + directionString);
        });
        return String.join(", ", sortFields);
    }

}
