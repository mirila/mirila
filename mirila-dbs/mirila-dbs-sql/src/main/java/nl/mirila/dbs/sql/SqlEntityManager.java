package nl.mirila.dbs.sql;

import com.google.inject.Inject;
import nl.mirila.core.datatype.Id;
import nl.mirila.core.datatype.Json;
import nl.mirila.core.exceptions.MirilaException;
import nl.mirila.dbs.sql.descriptor.*;
import nl.mirila.model.core.references.Key;
import nl.mirila.model.core.references.Model;
import nl.mirila.model.management.descriptor.ModelDescriptor;
import nl.mirila.model.management.descriptor.ModelEntityFactory;
import nl.mirila.model.management.managers.EntityPersistenceManager;
import nl.mirila.model.management.parameters.ActionParameters;
import nl.mirila.model.management.query.QueryParameters;
import nl.mirila.model.management.results.Result;
import nl.mirila.model.management.results.Results;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.sql.Statement.RETURN_GENERATED_KEYS;

/**
 * The {@link EntityPersistenceManager} for SQL databases.
 */
public class SqlEntityManager implements EntityPersistenceManager {

    private static final Logger logger = LogManager.getLogger(SqlEntityManager.class);

    private final SqlModelDescriptors descriptors;
    private final Connection connection;

    /**
     * Initialize an new instance.
     */
    @Inject
    public SqlEntityManager(SqlModelDescriptors sqlDescriptors, Connection connection) {
        this.descriptors = sqlDescriptors;
        this.connection = connection;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <E extends Model> long count(QueryParameters<E> queryParameters) {
        SqlSimpleQueryBuilder<E> builder = descriptors.getSimpleQueryBuilder(queryParameters.getModelClass());
        SqlAndParameters sqlAndParameters = builder.generateSql(queryParameters, true);
        try (PreparedStatement statement = connection.prepareStatement(sqlAndParameters.getSql())) {
            addParametersToStatement(statement, sqlAndParameters.getParameters());
            return descriptors.getQueryExecutor(queryParameters.getModelClass()).executeCountStatement(statement);
        } catch (SQLException e) {
            throw new MirilaException(e.getMessage());
        }
    }

    /**
     * Fetch a list of entities, using the context of the given query.
     * <p>
     * @param queryParameters The query to process.
     * @param <E> The type of the entity.
     * @return A list of entities, wrapped in a {@link Results}.
     */
    @Override
    public <E extends Model> Results<E> query(QueryParameters<E> queryParameters) {
        SqlSimpleQueryBuilder<E> builder = descriptors.getSimpleQueryBuilder(queryParameters.getModelClass());
        SqlAndParameters sqlAndParameters = builder.generateSql(queryParameters, false);
        return query(queryParameters.getModelClass(), sqlAndParameters.getSql(), sqlAndParameters.getParameters());
    }

    /**
     * Fetch a list of entities, using the context of the given query.
     * <p>
     * @param entityClass The class o the entity.
     * @param sql The sql template to run.
     * @param queryParameters The run parameters to use.
     * @param <E> The type of the entity.
     * @return A list of entities, wrapped in a {@link Results}.
     */
    public <E extends Model> Results<E> query(Class<E> entityClass, String sql, List<Object> queryParameters) {
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            addParametersToStatement(statement, queryParameters);
            logQuery(statement);
            List<E> list = descriptors.getQueryExecutor(entityClass).executeStatement(statement);
            return Results.of(list);
        } catch (SQLException e) {
            throw new MirilaException(e.getMessage());
        }
    }

    /**
     * Fetch a single entity, that has the given key as primary key.
     * <p>
     * @param key The key of the expected entity.
     * @param parameters The run parameters to use.
     * @param <E> The model class.
     * @return The entity, wrapped in a {@link Result}.
     */
    @Override
    public <E extends Model> Result<E> read(Key<E> key, ActionParameters<E> parameters) {
        SqlCrudQueryBuilder<E> builder = descriptors.getCrudQueryBuilder(key.getModelClass());
        List<E> list;
        String sqlTemplate = builder.getReadTemplate();

        try (PreparedStatement statement = connection.prepareStatement(sqlTemplate)) {
            addParametersToStatement(statement, key.getParts());
            logQuery(statement);
            list = descriptors.getQueryExecutor(key.getModelClass())
                    .executeStatement(statement);
        } catch (SQLException e) {
            throw new MirilaException(e.getMessage());
        }

        if (list.size() == 0) {
            return Result.empty();
        } else if (list.size() == 1) {
            return Result.of(list.get(0));
        }
        String msg = "Found multiple items for model %s with primary key %s";
        throw new MirilaException(String.format(msg, key.getModelClass().getSimpleName(), key));
    }

    /**
     * Create the given entity, and returned the entity.
     * <p>
     * The returned entity is not the given entity. Instead a {@link #read(Key, ActionParameters)} is performed, to also
     * get the data that may have been auto generated.
     * <p>
     * @param entity The entity.
     * @param parameters The run parameters to use.
     * @param <E> The model class.
     * @return The created entity, wrapped in a {@link Result}.
     */
    @Override
    public <E extends Model> Result<E> create(E entity, ActionParameters<E> parameters) {
        String modelName = entity.getClass().getSimpleName();
        SqlModelDescriptor<E> sqlDescriptor = descriptors.getSqlDescriptorForClassName(modelName);
        SqlCrudQueryBuilder<E> builder = sqlDescriptor.getCrudQueryBuilder();
        String sqlTemplate = builder.getCreateTemplate();

        try (PreparedStatement statement = connection.prepareStatement(sqlTemplate, RETURN_GENERATED_KEYS)) {
            addEntityFieldsToPreparedInsertStatement(sqlDescriptor, statement, entity);
            logQuery(statement);
            statement.executeUpdate();
            ResultSet set = statement.getGeneratedKeys();
            if (set.next()) {
                List<String> autoGeneratedFields = sqlDescriptor.getDescriptor().getAutoGeneratedPrimaryKeyFields();
                if (!autoGeneratedFields.isEmpty()) {
                    Map<String, Object> values = new HashMap<>();
                    for (int i = 0; i < autoGeneratedFields.size(); i++) {
                        values.put(autoGeneratedFields.get(i), set.getString(i + 1));
                    }
                    ModelEntityFactory<E> factory = sqlDescriptor.getDescriptor().getEntityFactory();
                    factory.updateEntityWithValues(entity, values);
                    // Instead of returning the updated entity, do an actual SELECT, as fields may be auto updated.
                    Key<E> key = sqlDescriptor.getDescriptor().getPrimaryKeyForEntity(entity);
                    return read(key, parameters);
                }
            }
        } catch (SQLException e) {
            throw new MirilaException(e.getMessage());
        }
        return null;
    }

    /**
     * Update the given entity, and returned the updated entity.
     * <p>
     * The returned entity is not the given entity. Instead a {@link #read(Key, ActionParameters)} is performed, to also
     * get the data that may have been auto generated.
     * <p>
     * @param entity The entity.
     * @param parameters The run parameters to use.
     * @param <E> The model class.
     * @return The created entity, wrapped in a {@link Result}.
     */
    @Override
    public <E extends Model> Result<E> update(E entity, ActionParameters<E> parameters) {
        String modelName = entity.getClass().getSimpleName();
        SqlModelDescriptor<E> sqlDescriptor = descriptors.getSqlDescriptorForClassName(modelName);
        SqlCrudQueryBuilder<E> builder = sqlDescriptor.getCrudQueryBuilder();
        String sqlTemplate = builder.getUpdateTemplate();

        try (PreparedStatement statement = connection.prepareStatement(sqlTemplate)) {
            addEntityFieldsToPreparedUpdateStatement(sqlDescriptor, statement, entity);
            logQuery(statement);
            statement.execute();
            // Instead of returning the updated entity, do an actual SELECT, as fields may be auto updated.
            Key<E> key = sqlDescriptor.getDescriptor().getPrimaryKeyForEntity(entity);
            return read(key, parameters);
        } catch (SQLException e) {
            throw new MirilaException(e.getMessage());
        }
    }

    /**
     * Delete the entity with the given {@link Key}.
     * <p>
     * @param key The key of the entity.
     * @param parameters The run parameters to use.
     * @param <E> The model class.
     */
    @Override
    public <E extends Model> void delete(Key<E> key, ActionParameters<E> parameters) {
        SqlCrudQueryBuilder<E> builder = descriptors.getCrudQueryBuilder(key.getModelClass());
        String sqlTemplate = builder.getDeleteTemplate();
        runSqlStatement(sqlTemplate, key.getParts());
    }

    /**
     * Runs an sql query that does not return data.
     * <p>
     * @param sqlTemplate The sql template.
     * @param sqlParameters The parameters for the sql template.
     */
    public boolean runSqlStatement(String sqlTemplate, List<Object> sqlParameters) {
        try (PreparedStatement statement = connection.prepareStatement(sqlTemplate)) {
            addParametersToStatement(statement, sqlParameters);
            logQuery(statement);
            return statement.execute();
        } catch (SQLException e) {
            throw new MirilaException(e.getMessage());
        }
    }

    /**
     * Add the given parameters to the statement.
     * <p>
     * @param statement The statement.
     * @param parameters The parameters
     * @throws SQLException The SQL exception in case a parameter could not be set.
     */
    private void addParametersToStatement(PreparedStatement statement, List<Object> parameters) throws SQLException {
        for (int i = 0; i < parameters.size(); i++) {
            Object value = parameters.get(i);
            if ((value instanceof Id) || (value instanceof Json)) {
                value = value.toString();
            }
            statement.setObject(i + 1, value);
        }
    }

    /**
     * Add all values of the given entity, to the INSERT {@link PreparedStatement}. The auto generated columns are not
     * used within this INSERT SQL.
     * <p>
     * @param sqlDescriptor The SQL descriptor that is related to this model.
     * @param statement The prepared statement that need the values.
     * @param entity The entity that contains the values.
     * @param <E> The model class.
     */
    private <E extends Model> void addEntityFieldsToPreparedInsertStatement(SqlModelDescriptor<E> sqlDescriptor,
                                                                            PreparedStatement statement,
                                                                            E entity) throws SQLException {
        ModelDescriptor<E> descriptor = sqlDescriptor.getDescriptor();
        List<String> fields = descriptor.getFields();
        fields.removeAll(descriptor.getAutoGeneratedFields());
        addEntityFieldsToPreparedStatement(descriptor, statement, fields, entity);
    }

    /**
     * Add all values of the given entity, to the UPDATE {@link PreparedStatement}. The primary key columns are added as
     * last, as they are not used for update, but as WHERE condition for the update.
     * <p>
     * @param sqlDescriptor The SQL descriptor that is related to this model.
     * @param statement The prepared statement that need the values.
     * @param entity The entity that contains the values.
     * @param <E> The model class.
     */
    protected <E extends Model> void addEntityFieldsToPreparedUpdateStatement(SqlModelDescriptor<E> sqlDescriptor,
                                                                              PreparedStatement statement,
                                                                              E entity) throws SQLException {
        ModelDescriptor<E> descriptor = sqlDescriptor.getDescriptor();
        List<String> fields = descriptor.getFields();
        fields.removeAll(descriptor.getPrimaryKeyFields());
        fields.removeAll(descriptor.getAutoGeneratedFields());

        // Now add the primary keys to the end of the list.
        fields.addAll(descriptor.getPrimaryKeyFields());
        addEntityFieldsToPreparedStatement(descriptor, statement, fields, entity);
    }

    /**
     * Add the values of the given entity to the SELECT, INSERT, UPDATE or DELETE {@link PreparedStatement}.
     * <p>
     * @param descriptor The model descriptor that is related to this model.
     * @param statement The prepared statement that need the values.
     * @param fields The fields that should be included in the statement, also in this order.
     * @param entity The entity that contains the values.
     * @param <E> The model class.
     */
    private <E extends Model> void addEntityFieldsToPreparedStatement(ModelDescriptor<E> descriptor,
                                                                      PreparedStatement statement,
                                                                      List<String> fields,
                                                                      E entity) throws SQLException {
        for (int i = 0; i < fields.size(); i++) {
            String fieldName = fields.get(i);
            Object value = descriptor.getValue(fieldName, entity);
            addParameterToStatement(statement, i + 1, value);
        }
    }

    /**
     * Adds the given value to the statement at the given index.
     * <p>
     * @param statement The statement.
     * @param index The non-zero index.
     * @param value The value.
     * @throws SQLException if setting the parameter failed.
     */
    private void addParameterToStatement(PreparedStatement statement, int index, Object value) throws SQLException {
        if (value instanceof Id) {
            statement.setObject(index, ((Id) value).asInt());
        } else if (value instanceof Json) {
            statement.setObject(index, value.toString());
        } else {
            statement.setObject(index, value);
        }
    }

    /**
     * Log the query if debugging is enabled.
     * <p>
     * @param statement The statement to log.
     */
    private void logQuery(PreparedStatement statement) {
        if (logger.isDebugEnabled()) {
            String query = statement.toString().split(": ", 2)[1];
            logger.debug(">>> " + query);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void close() {
        logger.debug("Closing connection of SqlEntityManager");
        try {
            connection.close();
        } catch (SQLException e) {
            logger.error(e.getMessage(), e);
        }
    }

}
