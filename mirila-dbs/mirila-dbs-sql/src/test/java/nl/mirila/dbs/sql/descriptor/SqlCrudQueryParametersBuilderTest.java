package nl.mirila.dbs.sql.descriptor;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.multibindings.Multibinder;
import nl.mirila.core.settings.modules.KeyValuesSettingsModule;
import nl.mirila.dbs.sql.models.TestUser;
import nl.mirila.model.management.descriptor.ModelsPackageNames;
import nl.mirila.security.auth.core.contexts.GuestSecurityContext;
import nl.mirila.security.auth.core.contexts.SecurityContext;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.assertj.core.api.Assertions.assertThat;

class SqlCrudQueryParametersBuilderTest {

    private SqlCrudQueryBuilder<TestUser> queryBuilder;

    @BeforeEach
    void setUp() {
        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                install(new KeyValuesSettingsModule(new HashMap<>()));
                bind(SecurityContext.class).toInstance(new GuestSecurityContext("test-realm", "en_EN"));
                Multibinder<String> binder = Multibinder.newSetBinder(binder(), String.class, ModelsPackageNames.class);
                binder.addBinding().toInstance(TestUser.class.getPackageName());
            }
        });
        SqlModelDescriptors descriptors = injector.getInstance(SqlModelDescriptors.class);
        queryBuilder = descriptors.getCrudQueryBuilder(TestUser.class);
    }

    @Test
    void testGetCreateTemplate() {
        String expected = "INSERT INTO test_users (`username`, `first_name`, `last_name`, `age`) " +
                "VALUES (?, ?, ?, ?)";
        assertThat(queryBuilder.getCreateTemplate()).isEqualTo(expected);
    }

    @Test
    void testGetReadTemplate() {
        String expected = "SELECT * FROM test_users WHERE `user_id`=?";
        assertThat(queryBuilder.getReadTemplate()).isEqualTo(expected);
    }

    @Test
    void testGetUpdateTemplate() {
        String expected = "UPDATE test_users SET `username`=?, `first_name`=?, `last_name`=?, `age`=? WHERE `user_id`=?";
        assertThat(queryBuilder.getUpdateTemplate()).isEqualTo(expected);
    }

    @Test
    void testGetDeleteTemplate() {
        String expected = "DELETE FROM test_users WHERE `user_id`=?";
        assertThat(queryBuilder.getDeleteTemplate()).isEqualTo(expected);
    }

}
