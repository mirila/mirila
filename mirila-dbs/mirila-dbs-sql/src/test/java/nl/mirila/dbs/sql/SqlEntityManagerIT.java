package nl.mirila.dbs.sql;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Scopes;
import com.google.inject.multibindings.Multibinder;
import nl.mirila.core.settings.modules.KeyValuesSettingsModule;
import nl.mirila.dbs.sql.models.TestUser;
import nl.mirila.model.core.references.Key;
import nl.mirila.model.management.descriptor.ModelsPackageNames;
import nl.mirila.model.management.managers.EntityPersistenceManager;
import nl.mirila.model.management.results.Result;
import org.jboss.resteasy.plugins.guice.RequestScoped;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static nl.mirila.core.settings.ApplicationSettings.KEY_REALM;
import static org.junit.jupiter.api.Assertions.*;

class SqlEntityManagerIT {

    private static EntityPersistenceManager manager;

    @BeforeAll
    static void beforeAll() {
        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                Map<String, String> settings = new HashMap<>();
                settings.put(KEY_REALM, "mirila");
                install(new KeyValuesSettingsModule(settings));
                install(new SqlModule());
                Multibinder<String> binder = Multibinder.newSetBinder(binder(), String.class, ModelsPackageNames.class);
                binder.addBinding().toInstance(TestUser.class.getPackageName());
                bindScope(RequestScoped.class, Scopes.SINGLETON);
            }
        });
        manager = injector.getInstance(EntityPersistenceManager.class);
    }

    @SuppressWarnings("SpellCheckingInspection")
    @Test
    void testCrudProcess() {
        // This can not be tested in separate tests, as these test relate to each other.

        TestUser user = new TestUser();
        user.setAge(123);
        user.setFirstName("Obi-wan");
        user.setLastName("Kenobi");
        user.setUsername("owkenobi");

        // Create a user based on the given data.
        Result<TestUser> createResult = manager.create(user);
        assertFalse(createResult.isEmpty());

        TestUser createdUser = createResult.get();
        assertTrue(createdUser.getUserId().asInt() > 0);
        assertEquals(user.getUserId(), createdUser.getUserId());
        assertEquals("owkenobi", createdUser.getUsername());
        assertEquals("Obi-wan", createdUser.getFirstName());
        assertEquals("Kenobi", createdUser.getLastName());
        assertEquals(123, createdUser.getAge());

        // Change the created user.
        createdUser.setAge(321);
        createdUser.setFirstName("Luke");
        createdUser.setLastName("Skywalker");

        // Update the created user to the database.
        Result<TestUser> updateResult = manager.update(createdUser);
        assertFalse(updateResult.isEmpty());

        TestUser updatedUser = updateResult.get();
        assertEquals(createdUser.getUserId(), updatedUser.getUserId());
        assertEquals("owkenobi", updatedUser.getUsername());
        assertEquals("Luke", updatedUser.getFirstName());
        assertEquals("Skywalker", updatedUser.getLastName());
        assertEquals(321, updatedUser.getAge());

        // Read result, we should find a user.
        Key<TestUser> key = Key.of(TestUser.class, createdUser.getUserId());
        Result<TestUser> readResult = manager.read(key);
        assertFalse(readResult.isEmpty());

        //assertEquals(1, manager.count(QueryParameters.on(TestUser.class)));

        // Delete the user from the database.
        manager.delete(key);

        //assertEquals(0, manager.count(QueryParameters.on(TestUser.class)));

        // Read the user with the id of the delete user. We should receive an empty result.
        Result<TestUser> anotherReadResult = manager.read(key);
        assertTrue(anotherReadResult.isEmpty());
    }

}
