package nl.mirila.dbs.sql.descriptor;

import nl.mirila.dbs.sql.models.TestUser;
import nl.mirila.model.core.enums.SortDirection;
import nl.mirila.model.management.query.QueryParameters;
import nl.mirila.model.management.query.Sort;
import nl.mirila.model.management.query.filters.FieldComparisonFilter;
import nl.mirila.model.management.query.filters.FilterList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static nl.mirila.model.management.enums.RelationalOperator.EQ;
import static nl.mirila.model.management.enums.RelationalOperator.NEQ;
import static org.assertj.core.api.Assertions.assertThat;

class SqlSimpleQueryParametersBuilderTest {

    private SqlSimpleQueryBuilder<TestUser> builder;

    @BeforeEach
    void setUp() {
        HashMap<String, String> fieldAndColumnNames = new HashMap<>();
        fieldAndColumnNames.put("userId", "user_id");
        fieldAndColumnNames.put("username", "username");
        fieldAndColumnNames.put("firstName", "first_name");
        fieldAndColumnNames.put("lastName", "last_name");
        fieldAndColumnNames.put("age", "age");
        builder = new SqlSimpleQueryBuilder<>("test_users", fieldAndColumnNames);
    }

    @Test
    void testGenerateSqlWithEmptyQuery() {
        QueryParameters<TestUser> queryParameters = QueryParameters.on(TestUser.class);
        SqlAndParameters sqlAndParameters = builder.generateSql(queryParameters);
        assertThat(sqlAndParameters.getSql()).isEqualTo("SELECT * FROM test_users");
    }

    @Test
    void testGenerateSqlWithIdQuery() {
        QueryParameters<TestUser> queryParameters = QueryParameters.on(TestUser.class)
                .withFilter(new FieldComparisonFilter("userId", EQ, 1));
        SqlAndParameters sqlAndParameters = builder.generateSql(queryParameters);
        assertThat(sqlAndParameters.getSql())
                .isEqualTo("SELECT * FROM test_users WHERE `user_id` = ?");
        assertThat(sqlAndParameters.getParameters())
                .containsExactly(1);
    }

    @Test
    void testGenerateSqlWithFilteredListQuery() {
        FilterList filterList = FilterList.of(
                new FieldComparisonFilter("firstName", NEQ, "Lama"),
                new FieldComparisonFilter("lastName", EQ, "Michel")
        );

        QueryParameters<TestUser> queryParameters = QueryParameters.on(TestUser.class).withFilter(filterList);
        SqlAndParameters sqlAndParameters = builder.generateSql(queryParameters);
        assertThat(sqlAndParameters.getSql())
                .isEqualTo("SELECT * FROM test_users WHERE (`first_name` NOT ? AND `last_name` = ?)");
        assertThat(sqlAndParameters.getParameters())
                .containsExactly("Lama", "Michel");
    }

    @Test
    void testGenerateSqlWithComplexFilteredListQuery() {
        FilterList filterList = FilterList.of(
                new FieldComparisonFilter("username", NEQ, "lama_michel"),
                FilterList.of(
                        new FieldComparisonFilter("firstName", NEQ, "Lama"),
                        new FieldComparisonFilter("lastName", EQ, "Michel")
                ).matching(FilterList.MatchCondition.ONE)
        );

        QueryParameters<TestUser> queryParameters = QueryParameters.on(TestUser.class).withFilter(filterList);
        SqlAndParameters sqlAndParameters = builder.generateSql(queryParameters);
        assertThat(sqlAndParameters.getSql())
                .isEqualTo("SELECT * FROM test_users " +
                           "WHERE (`username` NOT ? AND (`first_name` NOT ? OR `last_name` = ?))");
        assertThat(sqlAndParameters.getParameters())
                .containsExactly("lama_michel", "Lama", "Michel");
    }


    @Test
    void testGenerateSqlWithIdAndSimpleSortQuery() {
        QueryParameters<TestUser> queryParameters = QueryParameters.on(TestUser.class)
                .withFilter(new FieldComparisonFilter("userId", EQ, 1))
                .withSort(Sort.on("firstName").thenOn("last_name", SortDirection.DESCENDING));

        SqlAndParameters sqlAndParameters = builder.generateSql(queryParameters);
        assertThat(sqlAndParameters.getSql())
                .isEqualTo("SELECT * FROM test_users WHERE `user_id` = ? ORDER BY `first_name`, `last_name` desc");
        assertThat(sqlAndParameters.getParameters())
                .containsExactly(1);
    }
}
