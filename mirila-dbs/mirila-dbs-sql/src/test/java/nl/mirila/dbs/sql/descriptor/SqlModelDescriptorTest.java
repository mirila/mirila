package nl.mirila.dbs.sql.descriptor;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.multibindings.Multibinder;
import nl.mirila.dbs.sql.models.TestUser;
import nl.mirila.model.management.descriptor.ModelsPackageNames;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.assertj.core.api.Assertions.assertThat;

class SqlModelDescriptorTest {

    private SqlModelDescriptor<TestUser> testUserDescriptor;

    @BeforeEach
    void setUp() {
        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                Multibinder<String> binder = Multibinder.newSetBinder(binder(), String.class, ModelsPackageNames.class);
                binder.addBinding().toInstance(TestUser.class.getPackageName());
            }
        });
        SqlModelDescriptors descriptors = injector.getInstance(SqlModelDescriptors.class);
        testUserDescriptor = descriptors.getSqlDescriptorForModel(TestUser.class);
    }

    @Test
    void testGetModelClass() {
        assertThat(testUserDescriptor.getModelClass()).isEqualTo(TestUser.class);
    }

    @Test
    void testGetTableName() {
        assertThat(testUserDescriptor.getTableName()).isEqualTo("test_users");
    }

    @Test
    void testGetFieldNameAndColumnNames() {
        HashMap<String, String> expectedMap = new HashMap<>();
        expectedMap.put("userId", "user_id");
        expectedMap.put("username", "username");
        expectedMap.put("firstName", "first_name");
        expectedMap.put("lastName", "last_name");
        expectedMap.put("age", "age");

        assertThat(testUserDescriptor.getFieldNameAndColumnNames()).isEqualTo(expectedMap);
    }

    @Test
    void testGetPrimaryKeyNames() {
        HashMap<String, String> expectedMap = new HashMap<>();
        expectedMap.put("userId", "user_id");

        assertThat(testUserDescriptor.getPrimaryKeyColumnNames()).isEqualTo(expectedMap);
    }

}
