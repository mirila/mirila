package nl.mirila.dbs.mongodb;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.multibindings.Multibinder;
import com.mongodb.client.model.Filters;
import nl.mirila.dbs.mongodb.models.TestUser;
import nl.mirila.model.management.descriptor.ModelDescriptors;
import nl.mirila.model.management.descriptor.ModelsPackageNames;
import nl.mirila.model.management.enums.RelationalOperator;
import nl.mirila.model.management.query.filters.FieldComparisonFilter;
import nl.mirila.model.management.query.filters.Filter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class MongoDbFilterBuilderTest {

    private MongoDbFilterBuilder<TestUser> builder;

    @BeforeEach
    void setUp() {
        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                Multibinder<String> binder = Multibinder.newSetBinder(binder(), String.class, ModelsPackageNames.class);
                binder.addBinding().toInstance(TestUser.class.getPackageName());
            }
        });
        ModelDescriptors descriptors = injector.getInstance(ModelDescriptors.class);
        builder = new MongoDbFilterBuilder<>(descriptors.getDescriptorForModel(TestUser.class));
    }

    @Test
    void testFromFilterWithNull() {
        assertThat(builder.fromFilter(null)).isEmpty();
    }

    @Test
    void testFromFilterWithNoFilter() {
        assertThat(builder.fromFilter(Filter.none())).isEmpty();
    }

    @Test
    void testFromFilterWithEqualsFieldComparisonFilter() {
        FieldComparisonFilter filter = new FieldComparisonFilter("username", RelationalOperator.EQ, "test");
        assertThat(builder.fromFilter(filter))
                .isPresent()
                .contains(Filters.eq("username", "test"));
    }

    @Test
    void testFromFilterWithNotEqualsFieldComparisonFilter() {
        FieldComparisonFilter filter = new FieldComparisonFilter("username", RelationalOperator.NEQ, "test");
        assertThat(builder.fromFilter(filter))
                .isPresent()
                .contains(Filters.ne("username", "test"));
    }

    @Test
    void testFromFilterWithLesserOrEqualsThanFieldComparisonFilter() {
        FieldComparisonFilter filter = new FieldComparisonFilter("username", RelationalOperator.LTE, "test");
        assertThat(builder.fromFilter(filter))
                .isPresent()
                .contains(Filters.lte("username", "test"));
    }

}
