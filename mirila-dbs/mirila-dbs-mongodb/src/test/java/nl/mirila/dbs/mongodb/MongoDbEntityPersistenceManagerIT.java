package nl.mirila.dbs.mongodb;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.multibindings.Multibinder;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;
import nl.mirila.core.datatype.Id;
import nl.mirila.core.settings.modules.KeyValuesSettingsModule;
import nl.mirila.dbs.mongodb.models.TestUser;
import nl.mirila.drivers.mongodb.MongoDbDriver;
import nl.mirila.drivers.mongodb.MongoDbUtils;
import nl.mirila.model.core.references.Key;
import nl.mirila.model.management.descriptor.ModelsPackageNames;
import nl.mirila.model.management.enums.RelationalOperator;
import nl.mirila.model.management.managers.EntityPersistenceManager;
import nl.mirila.model.management.query.QueryParameters;
import nl.mirila.model.management.query.filters.FieldComparisonFilter;
import nl.mirila.model.management.results.Result;
import nl.mirila.model.management.results.Results;
import nl.mirila.security.auth.core.contexts.GuestSecurityContext;
import nl.mirila.security.auth.core.contexts.SecurityContext;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.Reader;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

class MongoDbEntityPersistenceManagerIT {

    private static final String COLLECTION_NAME = "test_users";

    private static final Logger logger = LogManager.getLogger(MongoDbEntityPersistenceManagerIT.class);

    private static EntityPersistenceManager manager;
    private static MongoDbDriver driver;

    @BeforeAll
    static void beforeAll() throws URISyntaxException, IOException, CsvException {
        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                install(new KeyValuesSettingsModule(Collections.emptyMap()));
                install(new MongoDbModule());
                bind(SecurityContext.class).toInstance(new GuestSecurityContext("test-realm", "en_EN"));

                Multibinder<String> binder = Multibinder.newSetBinder(binder(), String.class, ModelsPackageNames.class);
                binder.addBinding().toInstance(TestUser.class.getPackageName());
            }
        });
        manager = injector.getInstance(EntityPersistenceManager.class);
        driver = injector.getInstance(MongoDbDriver.class);
        createTestUsersCollection();
    }

    @AfterAll
    static void afterAll() {
        dropTestUsersCollection();
    }

    private static void createTestUsersCollection() throws URISyntaxException, IOException, CsvException {
        MongoDatabase database = driver.getDatabase().orElseThrow();
        if (MongoDbUtils.doesCollectionExist(database, COLLECTION_NAME)) {
            dropTestUsersCollection();
        }
        database.createCollection(COLLECTION_NAME);
        logger.info("Created table " + COLLECTION_NAME + ".");
        MongoCollection<Document> collection = database.getCollection(COLLECTION_NAME);
        getData().forEach(collection::insertOne);
        logger.info("Added documents, total count of documents: {}", collection.countDocuments());
    }

    private static List<Document> getData() throws URISyntaxException, IOException, CsvException {
        Reader reader = Files.newBufferedReader(Paths.get(ClassLoader.getSystemResource("test-data.csv").toURI()));
        CSVReader csvReader = new CSVReader(reader);
        return csvReader.readAll()
                .stream()
                .map(MongoDbEntityPersistenceManagerIT::stringArrayToDocument)
                .filter(Objects::nonNull)
                .toList();
    }

    private static Document stringArrayToDocument(String[] stringArray) {
        if ((stringArray.length >= 5) && (!StringUtils.isNumeric(stringArray[4]))) {
            return null;
        }
        Document document = new Document();
        document.append("_id", new ObjectId(stringArray[0]));
        document.append("username", stringArray[1]);
        document.append("firstName", stringArray[2]);
        document.append("lastName", stringArray[3]);
        document.append("age", Integer.parseInt(stringArray[4]));
        return document;
    }

    private static void dropTestUsersCollection() {
        MongoDatabase database = driver.getDatabase().orElseThrow();
        database.getCollection(COLLECTION_NAME).drop();
        logger.info("Deleted table " + COLLECTION_NAME + ".");
    }

    @Test
    void testQueryAllUsers() {
        QueryParameters<TestUser> queryParameters = QueryParameters.on(TestUser.class);
        Results<TestUser> results = manager.query(queryParameters);
        assertThat(results)
                .isNotEmpty()
                .hasSize(20);
    }

    @Test
    void testQueryNonAdultUsers() {
        QueryParameters<TestUser> queryParameters = QueryParameters.on(TestUser.class)
                .withFilter(new FieldComparisonFilter("age", RelationalOperator.LT, 18));
        Results<TestUser> results = manager.query(queryParameters);
        assertThat(results)
                .isNotEmpty()
                .hasSize(4);
    }

    @Test
    void testCreate() {
        TestUser user = new TestUser();
        user.setAge(123);
        user.setFirstName("Obi-wan");
        user.setLastName("Kenobi");
        user.setUsername("owkenobi");

        Result<TestUser> createResult = manager.create(user);
        assertThat(createResult.isEmpty()).isFalse();
    }

    @Test
    void testRead() {
        // TODO: Add test.
    }

    @Test
    void testUpdate() {
        // TODO: Add test.
    }

    @Test
    void testDelete() {
        // TODO: Add test.
    }

    @SuppressWarnings("SpellCheckingInspection")
    @Test
    void testCrudProcess() {
        // This can not be tested in separate tests, as these test relate to each other.

        TestUser user = new TestUser();
        user.setAge(123);
        user.setFirstName("Obi-wan");
        user.setLastName("Kenobi");
        user.setUsername("owkenobi");

        // Create a user based on the given data.
        Result<TestUser> createResult = manager.create(user);
        assertThat(createResult.isEmpty()).isFalse();

        TestUser createdUser = createResult.get();
        assertThat(createdUser.getUserId()).isNotNull().isNotEqualTo(Id.of(""));
        assertThat(createdUser.getUsername()).isEqualTo("owkenobi");
        assertThat(createdUser.getFirstName()).isEqualTo("Obi-wan");
        assertThat(createdUser.getLastName()).isEqualTo("Kenobi");
        assertThat(createdUser.getAge()).isEqualTo(123);

        // Change the created user.
        createdUser.setAge(321);
        createdUser.setFirstName("Luke");
        createdUser.setLastName("Skywalker");

        // Update the created user to the database.
        Result<TestUser> updateResult = manager.update(createdUser);
        assertThat(updateResult.isEmpty()).isFalse();

        TestUser updatedUser = updateResult.get();
        assertThat(updatedUser.getUserId()).isEqualTo(createdUser.getUserId());
        assertThat(updatedUser.getUsername()).isEqualTo("owkenobi");
        assertThat(updatedUser.getFirstName()).isEqualTo("Luke");
        assertThat(updatedUser.getLastName()).isEqualTo("Skywalker");
        assertThat(updatedUser.getAge()).isEqualTo(321);

        // Read result, we should find a user.
        Key<TestUser> key = Key.of(TestUser.class, createdUser.getUserId());
        Result<TestUser> readResult = manager.read(key);
        assertThat(readResult.isEmpty()).isFalse();

        // Delete the user from the database.
        manager.delete(key);

        // Read the user with the id of the delete user. We should receive an empty result.
        Result<TestUser> anotherReadResult = manager.read(key);
        assertThat(anotherReadResult.isEmpty()).isTrue();
    }

}
