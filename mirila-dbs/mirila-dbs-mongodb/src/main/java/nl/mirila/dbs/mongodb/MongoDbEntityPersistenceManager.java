package nl.mirila.dbs.mongodb;

import com.google.inject.Inject;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.InsertOneResult;
import nl.mirila.core.datatype.Id;
import nl.mirila.drivers.mongodb.MissingMongoDbConnectionException;
import nl.mirila.drivers.mongodb.MongoDbDriver;
import nl.mirila.drivers.mongodb.MongoDbSettings;
import nl.mirila.model.core.exceptions.InvalidKeyException;
import nl.mirila.model.core.references.Key;
import nl.mirila.model.core.references.Model;
import nl.mirila.model.management.descriptor.ModelDescriptor;
import nl.mirila.model.management.descriptor.ModelDescriptors;
import nl.mirila.model.management.descriptor.ModelEntityFactory;
import nl.mirila.model.management.exceptions.NonStorableModelException;
import nl.mirila.model.management.exceptions.UnknownModelException;
import nl.mirila.model.management.managers.EntityPersistenceManager;
import nl.mirila.model.management.parameters.ActionParameters;
import nl.mirila.model.management.query.QueryParameters;
import nl.mirila.model.management.results.Result;
import nl.mirila.model.management.results.Results;
import org.bson.BsonDocument;
import org.bson.BsonObjectId;
import org.bson.BsonValue;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import java.util.*;

import static nl.mirila.core.utils.CaseConverter.PASCAL_CASE;
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

public class MongoDbEntityPersistenceManager implements EntityPersistenceManager {

    private static final Map<String, String> collectionNames = new HashMap<>();
    private static final Map<String, MongoDatabase> databases = new HashMap<>();

    private final MongoDatabase database;
    private final ModelDescriptors descriptors;

    /**
     * Initialize a new instance.
     */
    @Inject
    public MongoDbEntityPersistenceManager(MongoDbDriver driver,
                                           MongoDbSettings settings,
                                           ModelDescriptors descriptors) {
        this.descriptors = descriptors;
        String databaseName = settings.getDatabase();
        if (!databases.containsKey(databaseName)) {
            PojoCodecProvider provider = PojoCodecProvider.builder()
                    .register(Id.class)
                    .automatic(true)
                    .build();
            CodecRegistry pojoCodecRegistry = fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
                                                             fromProviders(provider));
            database = driver.getDatabase(databaseName)
                    .orElseThrow(MissingMongoDbConnectionException::new)
                    .withCodecRegistry(pojoCodecRegistry);
            databases.put(databaseName, database);
        } else {
            database = databases.get(databaseName);
        }
    }

    /**
     * Return the {@link MongoCollection} for the given entity class.
     * <p>
     * @param entityClass The entity class.
     * @param <E> The type of the entity.
     * @return The MongoDb collection.
     */
    private <E extends Model> MongoCollection<E> getCollection(Class<E> entityClass) {
        String entityClassName = entityClass.getSimpleName();
        collectionNames.computeIfAbsent(entityClassName, (name) -> {
            ModelDescriptor<E> descriptor = descriptors.getDescriptorForModel(entityClass);
            if (!descriptor.isStorable()) {
                throw new NonStorableModelException(entityClassName);
            }
            return PASCAL_CASE.toSnakeCase(descriptor.getPlural());
        });
        String collectionName = collectionNames.get(entityClassName);
        return database.getCollection(collectionName, entityClass);
    }

    /**
     * Return the {@link Bson} for the given entity key.
     * <p>
     * @param key The key of the entity.
     * @param <E> The type of the entity.
     * @return The Bson key for the entity.
     */
    private <E extends Model> Bson getBson(Key<E> key) {
        if (key.getParts().size() != 1) {
            throw new InvalidKeyException(key);
        }

        Optional<Object> optional = key.getPart(0);
        if (optional.isPresent()) {
            ObjectId objectId = new ObjectId(optional.get().toString());
            return new BsonDocument("_id", new BsonObjectId(objectId));
        }
        return new BsonDocument("_id", null);
    }

    /**
     * Update the given entity with the given key.
     * <p>
     * @param entity The entity to update.
     * @param key The key related to the entity.
     * @param <E> The type of the entity.
     */
    private <E extends Model> void updateEntityWithKey(E entity, Key<E> key) {
        ModelDescriptor<E> descriptor = descriptors.getDescriptorForModel(key.getModelClass());
        if (descriptor == null) {
            throw new UnknownModelException(key.getModelClass());
        }
        if (descriptor.getAutoGeneratedPrimaryKeyFields().size() == 1) {
            String idField = descriptor.getAutoGeneratedPrimaryKeyFields().get(0);
            ModelEntityFactory<E> factory = descriptor.getEntityFactory();
            Map<String, Object> idMap = new HashMap<>();
            idMap.put(idField, key.getPart(0).orElse(null));
            factory.updateEntityWithValues(entity, idMap);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <E extends Model> long count(QueryParameters<E> queryParameters) {
        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <E extends Model> Results<E> query(QueryParameters<E> queryParameters) {
        MongoCollection<E> collection = getCollection(queryParameters.getModelClass());
        List<E> list = new ArrayList<>();

        Bson filter = MongoDbFilterBuilder.from(queryParameters, descriptors);
        collection.find(filter).forEach(list::add);
        return Results.of(list.stream().distinct().toList());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <E extends Model> Result<E> read(Key<E> key, ActionParameters<E> parameters) {
        MongoCollection<E> collection = getCollection(key.getModelClass());
        Bson bson = getBson(key);
        FindIterable<E> iterable = collection.find(bson);
        if (!iterable.cursor().hasNext()) {
            return Result.empty();
        }

        E entity = iterable.cursor().next();
        updateEntityWithKey(entity, key);
        return Result.of(entity);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <E extends Model> Result<E> create(E entity, ActionParameters<E> parameters) {
        @SuppressWarnings("unchecked")
        Class<E> entityClass = (Class<E>) entity.getClass();
        MongoCollection<E> collection = getCollection(entityClass);
        InsertOneResult result = collection.insertOne(entity);

        BsonValue bson = result.getInsertedId();
        if (bson != null) {
            BsonObjectId objectId = bson.asObjectId();
            Key<E> newKey = Key.of(entityClass, objectId.getValue().toHexString());
            updateEntityWithKey(entity, newKey);
            return read(newKey);
        }
        return Result.empty();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <E extends Model> Result<E> update(E entity, ActionParameters<E> parameters) {
        @SuppressWarnings("unchecked")
        Class<E> entityClass = (Class<E>) entity.getClass();
        MongoCollection<E> collection = getCollection(entityClass);
        ModelDescriptor<E> descriptor = descriptors.getDescriptorForModel(entityClass);
        if (descriptor == null) {
            throw new UnknownModelException(entityClass);
        }
        Key<E> key = descriptor.getPrimaryKeyForEntity(entity);

        collection.findOneAndReplace(getBson(key), entity);
        updateEntityWithKey(entity, key);
        return Result.of(entity);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <E extends Model> void delete(Key<E> key, ActionParameters<E> parameters) {
        MongoCollection<E> collection = getCollection(key.getModelClass());
        Bson bson = getBson(key);
        collection.findOneAndDelete(bson);
    }

}
