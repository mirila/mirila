package nl.mirila.dbs.mongodb;

import com.google.inject.AbstractModule;
import nl.mirila.drivers.mongodb.MongoDbDriverModule;
import nl.mirila.model.core.jackson.IdType;
import nl.mirila.model.management.managers.EntityPersistenceManager;

public class MongoDbModule extends AbstractModule {

    @Override
    protected void configure() {
        install(new MongoDbDriverModule());

        bind(IdType.class).toInstance(IdType.STRING);
        bind(EntityPersistenceManager.class).to(MongoDbEntityPersistenceManager.class);
    }

}
