package nl.mirila.dbs.mongodb;

import com.google.inject.Inject;
import com.mongodb.BasicDBObject;
import com.mongodb.client.model.Filters;
import nl.mirila.model.core.references.Model;
import nl.mirila.model.management.descriptor.ModelDescriptor;
import nl.mirila.model.management.descriptor.ModelDescriptors;
import nl.mirila.model.management.query.QueryParameters;
import nl.mirila.model.management.query.filters.FieldComparisonFilter;
import nl.mirila.model.management.query.filters.Filter;
import nl.mirila.model.management.query.filters.FilterList;
import nl.mirila.model.management.query.filters.NoFilter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.conversions.Bson;

import java.util.List;
import java.util.Optional;

import static nl.mirila.model.management.query.filters.FilterList.MatchCondition.ALL;

/**
 * The {@link MongoDbFilterBuilder} creates MongoDb filters based on the query parameters.
 */
public class MongoDbFilterBuilder<E extends Model> {

    private static final Logger logger = LogManager.getLogger(MongoDbFilterBuilder.class);

    private final ModelDescriptor<E> descriptor;

    /**
     * Initialize a new instance.
     */
    @Inject
    MongoDbFilterBuilder(ModelDescriptor<E> descriptor) {
        this.descriptor = descriptor;
    }

    /**
     * Return the {@link Bson} using the given {@link QueryParameters} and {@link ModelDescriptors}.
     * <p>
     * @param queryParameters The query parameters.
     * @param modelDescriptors The model descriptors.
     * @param <E> The type of the entity.
     * @return A new Bson object.
     */
    public static <E extends Model> Bson from(QueryParameters<E> queryParameters, ModelDescriptors modelDescriptors) {
        ModelDescriptor<E> modelDescriptor = modelDescriptors.getDescriptorForModel(queryParameters.getModelClass());
        MongoDbFilterBuilder<E> mongoDbFilterBuilder = new MongoDbFilterBuilder<>(modelDescriptor);
        return mongoDbFilterBuilder.fromFilter(queryParameters.getFilter()).orElseGet(BasicDBObject::new);
    }

    /**
     * Convert the given {@link Filter} into a filter for MongoDb as a {@link BasicDBObject}.
     * <p>
     * @param filter The filter to convert.
     * @return The MongoDb filter in a BasicDBObject.
     */
    protected Optional<Bson> fromFilter(Filter filter) {
        if (filter instanceof FilterList filterList) {
            return fromFilterList(filterList);
        } else if (filter instanceof FieldComparisonFilter comparisonFilter) {
            return fromFieldComparisonFilter(comparisonFilter);
        } else if ((filter instanceof NoFilter) || (filter == null)) {
            return Optional.empty();
        } else {
            logger.warn("Unknown filter type: {}.", filter.getClass().getSimpleName());
            return Optional.empty();
        }
    }

    /**
     * Return the {@link Bson} for the given {@link FilterList}, wrapped in an {@link Optional}.
     * <p>
     * @param filterList The filter list.
     * @return The bson.
     */
    public Optional<Bson> fromFilterList(FilterList filterList) {
        List<Bson> objects = filterList.getFilters()
                .stream()
                .map(this::fromFilter)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .toList();
        if (objects.isEmpty()) {
            return Optional.empty();
        }
        Bson bson = (filterList.getMatchCondition() == ALL)
                ? Filters.and(objects)
                : Filters.or(objects);
        return Optional.of(bson);
    }

    /**
     * Return the {@link Bson} for the given {@link FieldComparisonFilter}, wrapped in an {@link Optional}.
     * <p>
     * @param filter The filter.
     * @return The bson.
     */
    private Optional<Bson> fromFieldComparisonFilter(FieldComparisonFilter filter) {
        String fieldName = filter.getFieldName();
        Object value = filter.getValue();

        return switch (filter.getOperator()) {
            case NEQ -> Optional.of(Filters.ne(fieldName, value));
            case LT -> Optional.of(Filters.lt(fieldName, value));
            case LTE -> Optional.of(Filters.lte(fieldName, value));
            case GTE -> Optional.of(Filters.gte(fieldName, value));
            case GT -> Optional.of(Filters.gt(fieldName, value));
            default -> Optional.of(Filters.eq(fieldName, value));
        };
    }

}
