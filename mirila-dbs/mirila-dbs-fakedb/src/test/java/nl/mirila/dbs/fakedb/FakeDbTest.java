package nl.mirila.dbs.fakedb;

import com.github.javafaker.Faker;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Scopes;
import com.google.inject.multibindings.Multibinder;
import nl.mirila.core.datatype.Id;
import nl.mirila.core.settings.modules.KeyValuesSettingsModule;
import nl.mirila.dbs.fakedb.testdata.TestUser;
import nl.mirila.metrics.core.MetricsCoreModule;
import nl.mirila.model.core.references.Key;
import nl.mirila.model.management.descriptor.ModelsPackageNames;
import nl.mirila.model.management.module.ModelManagementModule;
import nl.mirila.model.management.query.filters.FieldComparisonFilter;
import nl.mirila.model.management.query.filters.Filter;
import nl.mirila.model.management.results.Result;
import nl.mirila.model.management.results.Results;
import nl.mirila.security.auth.core.contexts.GuestSecurityContext;
import nl.mirila.security.auth.core.contexts.SecurityContext;
import nl.mirila.security.auth.core.contexts.ServiceSecurityContext;
import org.jboss.resteasy.plugins.guice.RequestScoped;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static nl.mirila.core.settings.ApplicationSettings.KEY_REALM;
import static nl.mirila.model.management.enums.RelationalOperator.*;
import static org.assertj.core.api.Assertions.assertThat;

class FakeDbTest {

    private static final String REALM = "test";

    private Faker faker;
    private FakeDb fakeDb;

    @BeforeEach
    void setUp() {
        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                Map<String, String> settings = new HashMap<>();
                settings.put(KEY_REALM, REALM);
                install(new KeyValuesSettingsModule(settings));
                install(new ModelManagementModule());
                install(new FakeDbModule());
                install(new MetricsCoreModule());
                bind(SecurityContext.class).toInstance(new GuestSecurityContext("test-realm", "en_EN"));
                bindScope(RequestScoped.class, Scopes.NO_SCOPE);

                Multibinder<String> binder = Multibinder.newSetBinder(binder(), String.class, ModelsPackageNames.class);
                binder.addBinding().toInstance(this.getClass().getPackageName());
            }
        });
        fakeDb = injector.getInstance(FakeDb.class);
        faker = Faker.instance();
    }

    private TestUser generateUser() {
        LocalDate dateOfBirth = faker.date().birthday().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        TestUser user = new TestUser();
        user.setFirstName(faker.name().firstName());
        user.setLastName(faker.name().lastName());
        user.setEmailAddress(faker.internet().emailAddress());
        user.setGender(faker.dog().gender());
        user.setPhoneNumber(faker.phoneNumber().phoneNumber());
        user.setDateOfBirth(dateOfBirth);
        return user;
    }

    @Test
    void testFakeDb() {
        int size = 100;
        List<TestUser> users = Stream.generate(this::generateUser)
                .limit(size)
                .toList();

        users.forEach((user) -> fakeDb.add(REALM, user));
        assertThat(fakeDb.getCount()).isEqualTo(size);
        assertThat(fakeDb.count(REALM, TestUser.class, Filter.none())).isEqualTo(size);

        assertThat(fakeDb.get(REALM, Key.of(TestUser.class, 1))).isPresent();
        assertThat(fakeDb.get(REALM, Key.of(TestUser.class, 50))).isPresent();
        assertThat(fakeDb.get(REALM, Key.of(TestUser.class, 100))).isPresent();

        assertThat(fakeDb.get(REALM, Key.of(TestUser.class, 200))).isEmpty();
        assertThat(fakeDb.get(REALM, Key.of(TestUser.class))).isEmpty();
        assertThat(fakeDb.get(REALM, Key.of(TestUser.class, 0))).isEmpty();

        IntStream.range(20, 40)
                .boxed()
                .forEach((i) -> fakeDb.remove(REALM, Key.of(TestUser.class, i)));

        assertThat(fakeDb.getCount()).isEqualTo(size - 20);
        assertThat(fakeDb.count(REALM, TestUser.class, Filter.none())).isEqualTo(size - 20);

        assertThat(fakeDb.get(REALM, Key.of(TestUser.class, 19))).isPresent();
        assertThat(fakeDb.get(REALM, Key.of(TestUser.class, 20))).isEmpty();
        assertThat(fakeDb.get(REALM, Key.of(TestUser.class, 39))).isEmpty();
        assertThat(fakeDb.get(REALM, Key.of(TestUser.class, 40))).isPresent();
    }


    @Test
    void testEntityManagerInjection() {
        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                ServiceSecurityContext context;
                context = new ServiceSecurityContext(REALM, "", Id.of(1), "fakedb", "FakeDb Test");
                bind(SecurityContext.class).toInstance(context);
                bindScope(RequestScoped.class, Scopes.NO_SCOPE);

                Map<String, String> settings = new HashMap<>();
                settings.put(KEY_REALM, REALM);

                install(new FakeDbModule());
                install(new KeyValuesSettingsModule(settings));
                install(new ModelManagementModule());
                install(new MetricsCoreModule());

                Multibinder<String> binder = Multibinder.newSetBinder(binder(), String.class, ModelsPackageNames.class);
                binder.addBinding().toInstance(this.getClass().getPackageName());
            }
        });

        InjectionTester tester = injector.getInstance(InjectionTester.class);
        Result<TestUser> result = tester.getTestUser();

        assertThat(result.isEmpty()).isFalse();
        assertThat(result.get().getFirstName()).isEqualTo("Obi-wan");

        Results<TestUser> results = tester.findTestUsers();
        assertThat(results.isEmpty()).isFalse();
    }

    @Test
    void testPassesFilter() {
        TestUser user = new TestUser();
        user.setUserId(Id.of("123"));
        user.setFirstName("Obi-Wan");
        user.setLastName(null);
        user.setAge(43);
        user.setDateOfBirth(LocalDate.of(1977, 9, 1));



        assertThat(fakeDb.passesFilter(user, new FieldComparisonFilter("userId", EQ, Id.of("123")))).isTrue();
        assertThat(fakeDb.passesFilter(user, new FieldComparisonFilter("userId", EQ, Id.of("456")))).isFalse();

        assertThat(fakeDb.passesFilter(user, new FieldComparisonFilter("firstName", EQ, "Obi-Wan"))).isTrue();
        assertThat(fakeDb.passesFilter(user, new FieldComparisonFilter("firstName", EQ, "Luke"))).isFalse();

        assertThat(fakeDb.passesFilter(user, new FieldComparisonFilter("lastName", EQ, null))).isTrue();
        assertThat(fakeDb.passesFilter(user, new FieldComparisonFilter("lastName", EQ, "Luke"))).isFalse();

        assertThat(fakeDb.passesFilter(user, new FieldComparisonFilter("age", EQ, 43))).isTrue();
        assertThat(fakeDb.passesFilter(user, new FieldComparisonFilter("age", EQ, 44))).isFalse();

        assertThat(fakeDb.passesFilter(user, new FieldComparisonFilter("age", NEQ, 43))).isFalse();
        assertThat(fakeDb.passesFilter(user, new FieldComparisonFilter("age", NEQ, 44))).isTrue();

        assertThat(fakeDb.passesFilter(user, new FieldComparisonFilter("age", LT, 50))).isTrue();
        assertThat(fakeDb.passesFilter(user, new FieldComparisonFilter("age", LT, 43))).isFalse();
        assertThat(fakeDb.passesFilter(user, new FieldComparisonFilter("age", LT, 40))).isFalse();

        assertThat(fakeDb.passesFilter(user, new FieldComparisonFilter("age", LTE, 50))).isTrue();
        assertThat(fakeDb.passesFilter(user, new FieldComparisonFilter("age", LTE, 43))).isTrue();
        assertThat(fakeDb.passesFilter(user, new FieldComparisonFilter("age", LTE, 40))).isFalse();

        assertThat(fakeDb.passesFilter(user, new FieldComparisonFilter("age", GTE, 50))).isFalse();
        assertThat(fakeDb.passesFilter(user, new FieldComparisonFilter("age", GTE, 43))).isTrue();
        assertThat(fakeDb.passesFilter(user, new FieldComparisonFilter("age", GTE, 40))).isTrue();

        assertThat(fakeDb.passesFilter(user, new FieldComparisonFilter("age", GT, 50))).isFalse();
        assertThat(fakeDb.passesFilter(user, new FieldComparisonFilter("age", GT, 43))).isFalse();
        assertThat(fakeDb.passesFilter(user, new FieldComparisonFilter("age", GT, 40))).isTrue();


        LocalDate dateOfBirth = LocalDate.of(1977, 9, 1);
        LocalDate dayBefore = dateOfBirth.minusDays(1);
        LocalDate dayAfter = dateOfBirth.plusDays(1);

        assertThat(fakeDb.passesFilter(user, new FieldComparisonFilter("dateOfBirth", EQ, dateOfBirth))).isTrue();
        assertThat(fakeDb.passesFilter(user, new FieldComparisonFilter("dateOfBirth", EQ, dayBefore))).isFalse();

        assertThat(fakeDb.passesFilter(user, new FieldComparisonFilter("dateOfBirth", NEQ, dateOfBirth))).isFalse();
        assertThat(fakeDb.passesFilter(user, new FieldComparisonFilter("dateOfBirth", NEQ, dayBefore))).isTrue();

        assertThat(fakeDb.passesFilter(user, new FieldComparisonFilter("dateOfBirth", LT, dayAfter))).isTrue();
        assertThat(fakeDb.passesFilter(user, new FieldComparisonFilter("dateOfBirth", LT, dateOfBirth))).isFalse();
        assertThat(fakeDb.passesFilter(user, new FieldComparisonFilter("dateOfBirth", LT, dayBefore))).isFalse();

        assertThat(fakeDb.passesFilter(user, new FieldComparisonFilter("dateOfBirth", LTE, dayAfter))).isTrue();
        assertThat(fakeDb.passesFilter(user, new FieldComparisonFilter("dateOfBirth", LTE, dateOfBirth))).isTrue();
        assertThat(fakeDb.passesFilter(user, new FieldComparisonFilter("dateOfBirth", LTE, dayBefore))).isFalse();

        assertThat(fakeDb.passesFilter(user, new FieldComparisonFilter("dateOfBirth", GTE, dayAfter))).isFalse();
        assertThat(fakeDb.passesFilter(user, new FieldComparisonFilter("dateOfBirth", GTE, dateOfBirth))).isTrue();
        assertThat(fakeDb.passesFilter(user, new FieldComparisonFilter("dateOfBirth", GTE, dayBefore))).isTrue();

        assertThat(fakeDb.passesFilter(user, new FieldComparisonFilter("dateOfBirth", GT, dayAfter))).isFalse();
        assertThat(fakeDb.passesFilter(user, new FieldComparisonFilter("dateOfBirth", GT, dateOfBirth))).isFalse();
        assertThat(fakeDb.passesFilter(user, new FieldComparisonFilter("dateOfBirth", GT, dayBefore))).isTrue();
    }

    @Test
    void testNoneIsNull() {
        assertThat(fakeDb.containsNull("Test1", "Test2")).isFalse();
        assertThat(fakeDb.containsNull(1, 2, 3, 4)).isFalse();
        assertThat(fakeDb.containsNull(true, false, true)).isFalse();

        assertThat(fakeDb.containsNull("Test1", null, "Test2")).isTrue();
        assertThat(fakeDb.containsNull(1, 2, null, 3, 4)).isTrue();
        assertThat(fakeDb.containsNull(true, null, false, true)).isTrue();

        assertThat(fakeDb.containsNull()).isFalse();
        assertThat(fakeDb.containsNull((Object) null)).isTrue();
        assertThat(fakeDb.containsNull(null, null)).isTrue();
    }

}
