package nl.mirila.dbs.fakedb;

import com.google.inject.Inject;
import nl.mirila.dbs.fakedb.testdata.TestUser;
import nl.mirila.model.management.managers.EntityManager;
import nl.mirila.model.management.query.QueryParameters;
import nl.mirila.model.management.results.Result;
import nl.mirila.model.management.results.Results;

class InjectionTester {

    private final EntityManager<TestUser> manager;

    @Inject
    public InjectionTester(EntityManager<TestUser> manager) {
        this.manager = manager;
    }

    public Result<TestUser> getTestUser() {
        TestUser user = new TestUser();
        user.setFirstName("Obi-wan");
        user.setLastName("Kenobi");
        return manager.create(user);
    }

    public Results<TestUser> findTestUsers() {
        return QueryParameters.on(TestUser.class).runWith(manager);
    }

}
