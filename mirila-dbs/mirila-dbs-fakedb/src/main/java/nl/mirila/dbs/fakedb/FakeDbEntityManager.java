package nl.mirila.dbs.fakedb;


import com.google.inject.Inject;
import nl.mirila.model.core.references.Key;
import nl.mirila.model.core.references.Model;
import nl.mirila.model.management.managers.EntityPersistenceManager;
import nl.mirila.model.management.parameters.ActionParameters;
import nl.mirila.model.management.query.QueryParameters;
import nl.mirila.model.management.results.Result;
import nl.mirila.model.management.results.Results;
import nl.mirila.security.auth.core.contexts.SecurityContext;

import java.util.List;
import java.util.Optional;

/**
 * The EntityManager implementation using the {@link FakeDb}.
 */
public class FakeDbEntityManager implements EntityPersistenceManager {

    private final FakeDb fakeDb;
    private final SecurityContext context;

    /**
     * Initialize a new instance.
     */
    @Inject
    FakeDbEntityManager(FakeDb fakeDb, SecurityContext context) {
        this.fakeDb = fakeDb;
        this.context = context;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <E extends Model> long count(QueryParameters<E> queryParameters) {
        return fakeDb.count(context.getRealm(), queryParameters.getModelClass(), queryParameters.getFilter());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <E extends Model> Results<E> query(QueryParameters<E> queryParameters) {
        List<E> list = fakeDb.find(context.getRealm(), queryParameters.getModelClass(), queryParameters.getFilter());
        return Results.of(list);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <E extends Model > Result<E> read(Key<E> key, ActionParameters<E> parameters) {
        Optional<E> optional = fakeDb.get(context.getRealm(), key);
        return optional.map(Result::of).orElse(Result.empty());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <E extends Model > Result<E> create(E entity, ActionParameters<E> parameters) {
        entity = fakeDb.add(context.getRealm(), entity);
        return Result.of(entity);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <E extends Model > Result<E> update(E entity, ActionParameters<E> parameters) {
        entity = fakeDb.replace(context.getRealm(), entity);
        return Result.of(entity);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <E extends Model > void delete(Key<E> key, ActionParameters<E> parameters) {
        fakeDb.remove(context.getRealm(), key);
    }

    /**
     * Clear the database and reset the auto increment index.
     */
    public void clearAll() {
        fakeDb.clear();
    }

}
