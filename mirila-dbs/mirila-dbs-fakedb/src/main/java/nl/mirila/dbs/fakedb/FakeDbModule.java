package nl.mirila.dbs.fakedb;

import com.google.inject.AbstractModule;
import com.google.inject.multibindings.Multibinder;
import nl.mirila.model.core.jackson.IdType;
import nl.mirila.model.management.managers.EntityPersistenceManager;

import java.sql.Driver;

public class FakeDbModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(IdType.class).toInstance(IdType.INTEGER);
        bind(FakeDb.class).asEagerSingleton();
        bind(EntityPersistenceManager.class).to(FakeDbEntityManager.class);

        // Ensure the Driver.class is bound to a Set.
        Multibinder.newSetBinder(binder(), Driver.class);
    }

}
