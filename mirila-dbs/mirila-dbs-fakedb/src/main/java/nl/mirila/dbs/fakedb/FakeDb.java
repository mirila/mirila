package nl.mirila.dbs.fakedb;

import com.google.inject.Inject;
import nl.mirila.core.datatype.Id;
import nl.mirila.model.core.references.Key;
import nl.mirila.model.core.references.Model;
import nl.mirila.model.management.descriptor.ModelDescriptor;
import nl.mirila.model.management.descriptor.ModelDescriptors;
import nl.mirila.model.management.enums.RelationalOperator;
import nl.mirila.model.management.exceptions.EntityAlreadyExistsException;
import nl.mirila.model.management.exceptions.NonStorableModelException;
import nl.mirila.model.management.exceptions.UnknownFilterException;
import nl.mirila.model.management.query.filters.FieldComparisonFilter;
import nl.mirila.model.management.query.filters.Filter;
import nl.mirila.model.management.query.filters.FilterList;
import nl.mirila.model.management.query.filters.NoFilter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static nl.mirila.model.management.enums.RelationalOperator.EQ;
import static nl.mirila.model.management.enums.RelationalOperator.NEQ;
import static nl.mirila.model.management.query.filters.FilterList.MatchCondition.ALL;

/**
 * The FakeDb is a simple in-memory database, without persistence.
 */
public class FakeDb {

    private static final Logger logger = LogManager.getLogger(FakeDb.class);

    private static final Map<String, Model> database = new LinkedHashMap<>();

    private static final AtomicInteger autoIncrement = new AtomicInteger(1);

    private final ModelDescriptors modelDescriptors;

    /**
     * Initialize a new instance.
     */
    @Inject
    FakeDb(ModelDescriptors modelDescriptors) {
        this.modelDescriptors = modelDescriptors;
    }

    /**
     * Return the {@link ModelDescriptor} for the model with the given name.
     * <p>
     * If the model name is not known, or not storable, a {@link NonStorableModelException} will be thrown.
     * <p>
     * @param modelName The name of the model.
     * @param <T> The type of the model.
     * @return The model descriptor.
     */
    private <T extends Model> ModelDescriptor<T> getDescriptor(String modelName) {
        ModelDescriptor<T> descriptor = modelDescriptors.getDescriptorForClassName(modelName);
        if (!descriptor.isStorable()) {
            throw new NonStorableModelException(modelName);
        }
        return descriptor;
    }

    /**
     * Return a new auto increment value.
     */
    private static int getAutoIncrement() {
        return autoIncrement.getAndIncrement();
    }

    /**
     * Return the count of records in the database, regardless of model and realm.
     */
    public int getCount() {
        return database.size();
    }

    /**
     * Return the database key for the entity on the given realm.
     * <p>
     * @param realm The realm.
     * @param entityClass The class of the entity.
     * @param key The key of the entity.
     * @return The database key.
     */
    private String getKey(String realm, Class<?> entityClass, Key<?> key) {
        return (key == null)
                ? realm + "::" + entityClass.getSimpleName() + "::"
                : realm + "::" + key;
    }

    /**
     * Add the given entity to the database.
     * <p>
     * @param realm The realm.
     * @param entity The entity.
     * @param <T> The type of the entity.
     * @return The added entity.
     */
    public <T extends Model> T add(String realm, T entity) {
        String modelName = entity.getClass().getSimpleName();
        ModelDescriptor<T> descriptor = getDescriptor(modelName);
        Key<?> primaryKey = descriptor.getPrimaryKeyForEntity(entity, (fieldName) -> {
            return Id.of(String.valueOf(getAutoIncrement()));
        });
        String dbKey = getKey(realm, entity.getClass(), primaryKey);
        if (database.containsKey(dbKey)) {
            throw new EntityAlreadyExistsException(primaryKey);
        }
        database.put(dbKey, entity);
        logger.debug("Added record {}", dbKey);
        return entity;
    }

    /**
     * Replace the given entity to the database.
     * <p>
     * @param realm The realm.
     * @param entity The entity.
     * @param <T> The type of the entity.
     * @return The replaced entity.
     */
    public <T extends Model> T replace(String realm, T entity) {
        String modelName = entity.getClass().getSimpleName();
        ModelDescriptor<T> descriptor = getDescriptor(modelName);
        Key<?> primaryKey = descriptor.getPrimaryKeyForEntity(entity);
        String dbKey = getKey(realm, entity.getClass(), primaryKey);
        database.put(dbKey, entity);
        logger.debug("Replaced record {}", dbKey);
        return entity;
    }

    /**
     * Remove the given entity to the database.
     * <p>
     * @param realm The realm.
     * @param primaryKey The primary key of the entity.
     * @param <T> The type of the entity.
     */
    public <T extends Model> void remove(String realm, Key<T> primaryKey) {
        String modelName = primaryKey.getModelClass().getSimpleName();
        getDescriptor(modelName); // Checks for storable.
        String dbKey = getKey(realm, primaryKey.getModelClass(), primaryKey);
        database.remove(dbKey);
        logger.debug("Removed record {}", dbKey);
    }

    /**
     * Fetch the entity with the given primary key, wrapped in an {@link Optional}.
     * <p>
     * @param realm The realm.
     * @param primaryKey The primary key.
     * @param <T> The type of the entity.
     * @return The entity.
     */
    public <T extends Model> Optional<T> get(String realm, Key<T> primaryKey) {
        getDescriptor(primaryKey.getModelClass().getSimpleName()); // Checks for storable.
        String dbKey = getKey(realm, primaryKey.getModelClass(), primaryKey);
        Object object = database.get(dbKey);
        Class<T> modelClass = primaryKey.getModelClass();
        return (modelClass.isInstance(object)) ? Optional.of(modelClass.cast(object)) : Optional.empty();
    }

    /**
     * Return the number of records in the database for models of the given model class, using the given filter.
     * <p>
     * @param realm The realm.
     * @param modelClass The class of the model.
     * @param filter The filter to use.
     * @param <T> The type of the model.
     * @return The number of related records.
     */
    public <T extends Model> long count(String realm, Class<T> modelClass, Filter filter) {
        getDescriptor(modelClass.getSimpleName()); // Checks for storable.
        String prefixFilter = getKey(realm, modelClass, null);
        return database.entrySet().stream()
                .filter((entry) -> entry.getKey().startsWith(prefixFilter))
                .filter((entry) -> passesFilter(entry.getValue(), filter))
                .count();
    }

    /**
     * Return a list of all entities that matches the given realm and filter.
     * <p>
     * @param realm The realm.
     * @param modelClass The class of the model.
     * @param filter The filter to use.
     * @param <T> The type of the model.
     * @return The list of found entities.
     */
    public <T extends Model> List<T> find(String realm, Class<T> modelClass, Filter filter) {
        getDescriptor(modelClass.getSimpleName()); // Checks for storable.

        String prefixFilter = getKey(realm, modelClass, null);
        return database.entrySet().stream()
                .filter((entry) -> entry.getKey().startsWith(prefixFilter))
                .filter((entry) -> passesFilter(entry.getValue(), filter))
                .map(Map.Entry::getValue)
                .map(modelClass::cast)
                .toList();
    }

    /**
     * Return true if the given entity passes the given filter.
     * <p>
     * @param entity The entity to test.
     * @param filter The filter to pass.
     * @param <T> The type of the entity.
     * @return True if it passes.
     */
    public <T extends Model> boolean passesFilter(T entity, Filter filter) {
        if (filter instanceof NoFilter) {
            return true;
        }  else if (filter instanceof FilterList filterList) {
            return passesFilter(entity, filterList);
        } else if (filter instanceof FieldComparisonFilter comparisonFilter) {
            return passesFilter(entity, comparisonFilter);
        } else {
            throw new UnknownFilterException(filter.getClass());
        }
    }

    /**
     * Return true if the given entity passes the given filter list.
     * <p>
     * @param entity The entity to test.
     * @param list The filter list to pass.
     * @param <T> The type of the entity.
     * @return True if it passes.
     */
    protected <T extends Model> boolean passesFilter(T entity, FilterList list) {
        Stream<Filter> stream = list.getFilters().stream();
        Predicate<Filter> predicate = (filter) -> passesFilter(entity, filter);
        return (list.getMatchCondition() == ALL)
                ? stream.allMatch(predicate)
                : stream.anyMatch(predicate);
    }

    /**
     * Return true if the given entity passes the given field comparison filter.
     * <p>
     * @param entity The entity to test.
     * @param filter The field comparison filter to pass.
     * @param <T> The type of the entity.
     * @return True if it passes.
     */
    protected <T extends Model> boolean passesFilter(T entity, FieldComparisonFilter filter) {
        String modelName = entity.getClass().getSimpleName();
        ModelDescriptor<T> descriptor = getDescriptor(modelName);

        Object actualValue = descriptor.getValue(filter.getFieldName(), entity);
        Object filterValue = filter.getValue();
        RelationalOperator operator = filter.getOperator();

        if (operator == EQ) {
            return (Objects.equals(actualValue, filterValue));
        }
        if (operator == NEQ) {
            return (!Objects.equals(actualValue, filterValue));
        }
        if ((containsNull(actualValue, filterValue)) || (!actualValue.getClass().isInstance(filterValue))) {
            return false;
        }

        if (actualValue instanceof Comparable) {
            @SuppressWarnings({"unchecked", "rawtypes"})
            int compared = ((Comparable) actualValue).compareTo(filterValue);
            return switch (operator) {
                case LT -> (compared < 0);
                case LTE -> (compared <= 0);
                case GTE -> (compared >= 0);
                case GT -> (compared > 0);
                default -> false;
            };
        }
        return false;
    }

    /**
     * Return true if any of the given objects is a null.
     */
    protected boolean containsNull(Object... objects) {
        for (Object object : objects) {
            if (object == null) {
                return true;
            }
        }
        return false;
    }

    /**
     * Remove all records from the database.
     */
    public void clear() {
        database.clear();
        autoIncrement.set(1);
    }

}
