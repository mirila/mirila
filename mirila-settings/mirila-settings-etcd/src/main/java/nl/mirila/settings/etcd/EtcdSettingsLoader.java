package nl.mirila.settings.etcd;

import com.google.inject.Inject;
import com.google.protobuf.ByteString;
import com.ibm.etcd.api.Event;
import com.ibm.etcd.api.KeyValue;
import com.ibm.etcd.api.RangeResponse;
import com.ibm.etcd.client.EtcdClient;
import com.ibm.etcd.client.kv.WatchUpdate;
import io.grpc.stub.StreamObserver;
import nl.mirila.drivers.etcd.EtcdDriver;
import nl.mirila.drivers.etcd.MissingEtcdConnectionException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

/**
 * The EcdSettingsLoader fetches all required settings from Etcd, and sets watchers to those settings.
 */
public class EtcdSettingsLoader {

    private static final Logger logger = LogManager.getLogger(EtcdSettingsLoader.class);

    public static final String PREFIX_SYSTEM = "settings.system.";
    public static final String PREFIX_APPLICATIONS = "settings.apps." + System.getProperty("app.id", "unknown") + ".";
    public static final String PREFIX_REALMS = "settings.realms.";
    public static final String PREFIX_USERS = "settings.users.";

    private final Map<String, String> settings;
    private final EtcdDriver driver;

    /**
     * Initialize a new instance.
     */
    @Inject
    public EtcdSettingsLoader(EtcdDriver driver) {
        this.driver = driver;
        settings = new HashMap<>();
        load();
    }

    /**
     * Load the driver, fetch the settings, and set watchers on these settings, in case those they change.
     */
    public void load() {
        EtcdClient client = driver.getClient().orElseThrow(MissingEtcdConnectionException::new);

        List<String> prefixes = Arrays.asList(PREFIX_SYSTEM, PREFIX_APPLICATIONS, PREFIX_REALMS, PREFIX_USERS);
        prefixes.forEach((prefix) -> {
            ByteString keyPrefix = ByteString.copyFrom(prefix.getBytes(StandardCharsets.UTF_8));

            client.getKvClient()
                    .watch(keyPrefix)
                    .asPrefix()
                    .start(new SettingsObserver());

            RangeResponse response = client.getKvClient()
                    .get(keyPrefix)
                    .asPrefix()
                    .sync();

            response.getKvsList().forEach(this::cacheKeyValue);
        });
    }

    /**
     * Cache the settings key and value in the given {@link KeyValue}.
     * <p>
     * @param keyValue The key value.
     */
    private void cacheKeyValue(KeyValue keyValue) {
        String key = new String(keyValue.getKey().toByteArray(), StandardCharsets.UTF_8);
        String value = new String(keyValue.getValue().toByteArray(), StandardCharsets.UTF_8);
        settings.put(key, value);
    }

    /**
     * Remove the settings key in the given {@link KeyValue} from cache.
     * <p>
     * @param keyValue The key value.
     */
    private void removeKeyFromCache(KeyValue keyValue) {
        String key = new String(keyValue.getKey().toByteArray(), StandardCharsets.UTF_8);
        settings.remove(key);
    }

    /**
     * Return the cached string value for the given key. If the key doesn't exist, an empty {@link Optional} returns.
     * <p>
     * @param key The key.
     * @return The value.
     */
    public Optional<String> getSetting(String key) {
        return Optional.ofNullable(settings.get(key));
    }

    /**
     * Return all settings with the given prefix.
     * <p>
     * @param prefix The prefix.
     * @return All related key values.
     */
    public Map<String, String> getSettings(String prefix) {
        return settings.entrySet().stream()
                .filter((entry) -> entry.getKey().startsWith(prefix))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    /**
     * The SettingsObserver is used as watch event handler when there's an update on the Etcd server or cluster.
     */
    private class SettingsObserver implements StreamObserver<WatchUpdate> {

        /**
         * Callback when an update event occurred on the Etcd server or cluster.
         */
        @Override
        public void onNext(WatchUpdate value) {
            value.getEvents().forEach((event) -> {
                if (event.getType().equals(Event.EventType.PUT)) {
                    EtcdSettingsLoader.this.cacheKeyValue(event.getKv());
                } else if (event.getType().equals(Event.EventType.DELETE)) {
                    EtcdSettingsLoader.this.removeKeyFromCache(event.getKv());
                }
            });
        }

        /**
         * Callback when an error occurred while watching on the Etcd server or cluster.
         */
        @Override
        public void onError(Throwable t) {
            logger.error("Error while watching Etcd: {}", t.getMessage());
        }

        /**
         * Callback when an error occurred while watching the Etcd server or cluster.
         */
        @Override
        public void onCompleted() {
            logger.info("Completed watching.");
        }

    }

}
