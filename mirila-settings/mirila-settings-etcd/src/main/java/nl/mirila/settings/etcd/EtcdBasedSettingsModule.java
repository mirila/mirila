package nl.mirila.settings.etcd;

import com.google.inject.AbstractModule;
import com.google.inject.Scopes;
import nl.mirila.core.settings.KeyValueMapProvider;
import nl.mirila.core.settings.KeyValuesProvider;
import nl.mirila.core.settings.Settings;
import nl.mirila.drivers.etcd.EtcdSettings;
import nl.mirila.settings.core.annotations.ApplicationLevelSettingsKeyValuesProvider;
import nl.mirila.settings.core.annotations.RealmLevelSettingsKeyValuesProvider;
import nl.mirila.settings.core.annotations.SystemLevelSettingsKeyValuesProvider;
import nl.mirila.settings.core.annotations.UserLevelSettingsKeyValuesProvider;
import nl.mirila.settings.core.levels.RealmLevelSettings;
import nl.mirila.settings.etcd.levels.ApplicationLevelEtcdKeyValuesProvider;
import nl.mirila.settings.etcd.levels.RealmLevelEtcdKeyValuesProvider;
import nl.mirila.settings.etcd.levels.SystemLevelEtcdKeyValuesProvider;
import nl.mirila.settings.etcd.levels.UserLevelEtcdKeyValuesProvider;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

/**
 * The EtcdBasedSettingsModule adds support to leveled based settings using an Etcd server or cluster.
 * <p>
 * We need minimal Etcd settings, but because settings are handled by Etcd, we're using system properties and sensible
 * defaults to configure the Etcd driver itself.
 */
public class EtcdBasedSettingsModule extends AbstractModule {

    private final Map<String, String> defaultEtcdSettings;

    /**
     * Initialize a new instance.
     */
    public EtcdBasedSettingsModule() {
        defaultEtcdSettings = new HashMap<>();
        defaultEtcdSettings.put("etcd.host", "localhost");
        defaultEtcdSettings.put("etcd.port", "2379");
        defaultEtcdSettings.put("etcd.protocol", "http");
        defaultEtcdSettings.put("etcd.username", "");
        defaultEtcdSettings.put("etcd.password", "");
    }

    /**
     * Configure the needed binds.
     */
    @Override
    protected void configure() {
        // Use the properties or the default Etcd settings.
        Properties properties = System.getProperties();
        Map<String, String> actualEtcdSettings = defaultEtcdSettings.entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey,
                                          (entry) -> properties.getProperty(entry.getKey(), entry.getValue())));
        KeyValueMapProvider provider = new KeyValueMapProvider(actualEtcdSettings);
        new Settings(provider);

        bind(EtcdSettings.class).toInstance(new EtcdSettings(new Settings(provider)));

        bind(KeyValuesProvider.class)
                .annotatedWith(SystemLevelSettingsKeyValuesProvider.class)
                .to(SystemLevelEtcdKeyValuesProvider.class)
                .in(Scopes.SINGLETON);

        bind(KeyValuesProvider.class)
                .annotatedWith(ApplicationLevelSettingsKeyValuesProvider.class)
                .to(ApplicationLevelEtcdKeyValuesProvider.class)
                .in(Scopes.SINGLETON);

        bind(KeyValuesProvider.class)
                .annotatedWith(RealmLevelSettingsKeyValuesProvider.class)
                .to(RealmLevelEtcdKeyValuesProvider.class)
                .in(Scopes.SINGLETON);

        bind(KeyValuesProvider.class)
                .annotatedWith(UserLevelSettingsKeyValuesProvider.class)
                .to(UserLevelEtcdKeyValuesProvider.class)
                .in(Scopes.SINGLETON);

        bind(Settings.class).to(RealmLevelSettings.class);
    }

}
