package nl.mirila.settings.etcd.levels;

import com.google.inject.Inject;
import nl.mirila.settings.etcd.EtcdKeyValuesProvider;
import nl.mirila.settings.etcd.EtcdSettingsLoader;


/**
 * The {@link EtcdKeyValuesProvider} for application level settings.
 */
public class ApplicationLevelEtcdKeyValuesProvider extends EtcdKeyValuesProvider {

    /**
     * Initialize a new instance.
     */
    @Inject
    public ApplicationLevelEtcdKeyValuesProvider(EtcdSettingsLoader loader) {
        super(loader, EtcdSettingsLoader.PREFIX_APPLICATIONS);
    }

}
