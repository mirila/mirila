package nl.mirila.settings.etcd.levels;

import com.google.inject.Inject;
import nl.mirila.security.auth.core.contexts.SecurityContext;
import nl.mirila.settings.etcd.EtcdKeyValuesProvider;
import nl.mirila.settings.etcd.EtcdSettingsLoader;

import static nl.mirila.settings.etcd.EtcdSettingsLoader.PREFIX_USERS;

/**
 * The {@link EtcdKeyValuesProvider} for user level settings.
 */
public class UserLevelEtcdKeyValuesProvider extends EtcdKeyValuesProvider {

    /**
     * Initialize a new instance.
     */
    @Inject
    public UserLevelEtcdKeyValuesProvider(EtcdSettingsLoader loader, SecurityContext context) {
        super(loader, PREFIX_USERS + context.getRealm() + "." + context.getCredentialName().orElse("0") + ".");
    }

}
