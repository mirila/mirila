package nl.mirila.settings.etcd.levels;

import com.google.inject.Inject;
import nl.mirila.security.auth.core.contexts.SecurityContext;
import nl.mirila.settings.etcd.EtcdKeyValuesProvider;
import nl.mirila.settings.etcd.EtcdSettingsLoader;

/**
 * The {@link EtcdKeyValuesProvider} for realm level settings.
 */
public class RealmLevelEtcdKeyValuesProvider extends EtcdKeyValuesProvider {

    /**
     * Initialize a new instance.
     */
    @Inject
    public RealmLevelEtcdKeyValuesProvider(EtcdSettingsLoader loader, SecurityContext context) {
        super(loader, EtcdSettingsLoader.PREFIX_REALMS + context.getRealm() + ".");
    }

}
