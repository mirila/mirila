package nl.mirila.settings.etcd.levels;


import com.google.inject.Inject;
import nl.mirila.settings.etcd.EtcdKeyValuesProvider;
import nl.mirila.settings.etcd.EtcdSettingsLoader;

/**
 * The {@link EtcdKeyValuesProvider} for system level settings.
 */
public class SystemLevelEtcdKeyValuesProvider extends EtcdKeyValuesProvider {

    /**
     * Initialize a new instance.
     */
    @Inject
    public SystemLevelEtcdKeyValuesProvider(EtcdSettingsLoader loader) {
        super(loader, EtcdSettingsLoader.PREFIX_SYSTEM);
    }

}
