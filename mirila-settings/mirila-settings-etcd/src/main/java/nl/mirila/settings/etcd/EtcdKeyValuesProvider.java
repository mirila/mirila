package nl.mirila.settings.etcd;

import nl.mirila.core.settings.KeyValuesProvider;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * The EtcdKeyValuesProvider provides settings using
 */
public abstract class EtcdKeyValuesProvider implements KeyValuesProvider {

    private final EtcdSettingsLoader loader;
    private final String levelPrefix;

    /**
     * Initialize a new instance.
     */
    protected EtcdKeyValuesProvider(EtcdSettingsLoader loader, String levelPrefix) {
        this.loader = loader;
        this.levelPrefix = levelPrefix;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<String> getValue(String key) {
        return loader.getSetting(levelPrefix + key);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> getValues(String key, String separator) {
        return Arrays.asList(loader.getSetting(levelPrefix + key).orElse("").split(separator));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, String> getSubset(String prefix) {
        return loader.getSettings(levelPrefix + prefix);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void load() {
        // No-op
    }

}
