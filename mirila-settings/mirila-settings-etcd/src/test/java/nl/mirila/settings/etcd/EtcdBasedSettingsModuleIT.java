package nl.mirila.settings.etcd;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.protobuf.ByteString;
import com.ibm.etcd.api.DeleteRangeRequest;
import com.ibm.etcd.api.PutRequest;
import com.ibm.etcd.client.EtcdClient;
import nl.mirila.core.settings.Settings;
import nl.mirila.drivers.etcd.EtcdDriver;
import nl.mirila.security.auth.core.contexts.GuestSecurityContext;
import nl.mirila.security.auth.core.contexts.SecurityContext;
import nl.mirila.settings.core.levels.UserLevelSettings;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class EtcdBasedSettingsModuleIT {

    private static final String WATCHED_PREFIX = "watched-";
    
    private static String systemKey;
    private static String applicationKey;
    private static String realmKey;
    private static String userKey;

    private static String systemValue;
    private static String applicationValue;
    private static String realmValue;
    private static String userValue;

    private static ByteString rawSystemKey;
    private static ByteString rawApplicationKey;
    private static ByteString rawRealmKey;
    private static ByteString rawUserKey;

    private static ByteString rawSystemValue;
    private static ByteString rawApplicationValue;
    private static ByteString rawRealmValue;
    private static ByteString rawUserValue;

    private Settings settings;
    private EtcdClient client;

    @BeforeAll
    static void beforeAll() {
        systemKey = "test-system-key";
        applicationKey = "test-application-key";
        realmKey = "test-realm-key";
        userKey = "test-user-key";

        systemValue = systemKey.replace("-key", "-value");
        applicationValue = applicationKey.replace("-key", "-value");
        realmValue = realmKey.replace("-key", "-value");
        userValue = userKey.replace("-key", "-value");

        rawSystemKey = ByteString.copyFromUtf8("settings.system." + systemKey);
        rawApplicationKey = ByteString.copyFromUtf8("settings.apps.test-app." + applicationKey);
        rawRealmKey = ByteString.copyFromUtf8("settings.realms.test-realm." + realmKey);
        rawUserKey = ByteString.copyFromUtf8("settings.users.test-realm.0." + userKey);

        rawSystemValue = ByteString.copyFromUtf8(systemValue);
        rawApplicationValue = ByteString.copyFromUtf8(applicationValue);
        rawRealmValue = ByteString.copyFromUtf8(realmValue);
        rawUserValue = ByteString.copyFromUtf8(userValue);
    }

    @BeforeEach
    void setUp() {
        System.setProperty("app.id", "test-app");
        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                bind(SecurityContext.class).toInstance(new GuestSecurityContext("test-realm", "en_EN"));
                install(new EtcdBasedSettingsModule());
            }
        });
        EtcdDriver driver = injector.getInstance(EtcdDriver.class);
        client = driver.getClient().orElseThrow();
        client.getKvClient().batch()
                .put(PutRequest.newBuilder().setKey(rawSystemKey).setValue(rawSystemValue))
                .put(PutRequest.newBuilder().setKey(rawApplicationKey).setValue(rawApplicationValue))
                .put(PutRequest.newBuilder().setKey(rawRealmKey).setValue(rawRealmValue))
                .put(PutRequest.newBuilder().setKey(rawUserKey).setValue(rawUserValue))
                .sync();

        settings = injector.getInstance(UserLevelSettings.class);
    }

    @AfterEach
    void tearDown() {
        client.getKvClient().batch()
                .delete(DeleteRangeRequest.newBuilder().setKey(rawSystemKey))
                .delete(DeleteRangeRequest.newBuilder().setKey(rawApplicationKey))
                .delete(DeleteRangeRequest.newBuilder().setKey(rawRealmKey))
                .delete(DeleteRangeRequest.newBuilder().setKey(rawUserKey))
                .sync();
    }

    @Test
    void testEtcdSettings() throws InterruptedException {
        assertThat(settings.getString(systemKey)).isPresent().hasValue(systemValue);
        assertThat(settings.getString(applicationKey)).isPresent().hasValue(applicationValue);
        assertThat(settings.getString(realmKey)).isPresent().hasValue(realmValue);
        assertThat(settings.getString(userKey)).isPresent().hasValue(userValue);

        ByteString watchedSystemValue = ByteString.copyFromUtf8(WATCHED_PREFIX + systemValue);
        ByteString watchedApplicationValue = ByteString.copyFromUtf8(WATCHED_PREFIX + applicationValue);
        ByteString watchedRealmValue = ByteString.copyFromUtf8(WATCHED_PREFIX + realmValue);
        ByteString watchedUserValue = ByteString.copyFromUtf8(WATCHED_PREFIX + userValue);

        client.getKvClient().batch()
                .put(PutRequest.newBuilder().setKey(rawSystemKey).setValue(watchedSystemValue))
                .put(PutRequest.newBuilder().setKey(rawApplicationKey).setValue(watchedApplicationValue))
                .put(PutRequest.newBuilder().setKey(rawRealmKey).setValue(watchedRealmValue))
                .put(PutRequest.newBuilder().setKey(rawUserKey).setValue(watchedUserValue))
                .sync();

        // Wait for Etcd to notify all watchers.
        Thread.sleep(500);

        assertThat(settings.getString(systemKey)).isPresent().hasValue(WATCHED_PREFIX + systemValue);
        assertThat(settings.getString(applicationKey)).isPresent().hasValue(WATCHED_PREFIX + applicationValue);
        assertThat(settings.getString(realmKey)).isPresent().hasValue(WATCHED_PREFIX + realmValue);
        assertThat(settings.getString(userKey)).isPresent().hasValue(WATCHED_PREFIX + userValue);
    }

}
