package nl.mirila.settings.core;

import nl.mirila.core.settings.KeyValueMapProvider;

import java.util.HashMap;
import java.util.Map;

public class ExampleKeyValuesProvider extends KeyValueMapProvider {

    private static int constructionCount = 0;

    public ExampleKeyValuesProvider() {
        super(new HashMap<>());
        constructionCount++;
        map.put("example1", "value1");
        map.put("example2", "value2");
        map.put("example3", "value3");
    }

    public ExampleKeyValuesProvider(Map<String, String> map) {
        super(map);
        constructionCount++;
    }

    public static int getConstructionCount() {
        return constructionCount;
    }

    public static void clearConstructionCount() {
        constructionCount = 0;
    }

}
