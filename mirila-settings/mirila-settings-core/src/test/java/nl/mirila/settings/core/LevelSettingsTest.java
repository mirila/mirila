package nl.mirila.settings.core;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import nl.mirila.core.settings.KeyValuesProvider;
import nl.mirila.core.settings.Settings;
import nl.mirila.settings.core.annotations.ApplicationLevelSettingsKeyValuesProvider;
import nl.mirila.settings.core.annotations.RealmLevelSettingsKeyValuesProvider;
import nl.mirila.settings.core.annotations.SystemLevelSettingsKeyValuesProvider;
import nl.mirila.settings.core.annotations.UserLevelSettingsKeyValuesProvider;
import nl.mirila.settings.core.levels.ApplicationLevelSettings;
import nl.mirila.settings.core.levels.RealmLevelSettings;
import nl.mirila.settings.core.levels.SystemLevelSettings;
import nl.mirila.settings.core.levels.UserLevelSettings;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

class LevelSettingsTest {

    @Test
    void testGuiceInstantiationWithSingletonProviders() {
        ExampleKeyValuesProvider.clearConstructionCount();

        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                Map<String, String> systemMap = new HashMap<>();
                systemMap.put("type", "system");

                Map<String, String> applicationMap = new HashMap<>();
                applicationMap.put("type", "application");

                Map<String, String> realmMap = new HashMap<>();
                realmMap.put("type", "realm");

                Map<String, String> userMap = new HashMap<>();
                userMap.put("type", "user");

                // Set the correct KeyValueProviders for the different settings.
                bind(KeyValuesProvider.class)
                        .annotatedWith(SystemLevelSettingsKeyValuesProvider.class)
                        .toInstance(new ExampleKeyValuesProvider(systemMap));

                bind(KeyValuesProvider.class)
                        .annotatedWith(ApplicationLevelSettingsKeyValuesProvider.class)
                        .toInstance(new ExampleKeyValuesProvider(applicationMap));

                bind(KeyValuesProvider.class)
                        .annotatedWith(RealmLevelSettingsKeyValuesProvider.class)
                        .toInstance(new ExampleKeyValuesProvider(realmMap));

                bind(KeyValuesProvider.class)
                        .annotatedWith(UserLevelSettingsKeyValuesProvider.class)
                        .toInstance(new ExampleKeyValuesProvider(userMap));

                // Set the default settings to realm settings.
                bind(Settings.class).to(RealmLevelSettings.class);
            }
        });

        Settings systemSettings1 = injector.getInstance(SystemLevelSettings.class);
        Settings applicationSettings1 = injector.getInstance(ApplicationLevelSettings.class);
        Settings realmSettings1 = injector.getInstance(RealmLevelSettings.class);
        Settings userSettings1 = injector.getInstance(UserLevelSettings.class);
        Settings genericSettings1 = injector.getInstance(Settings.class);

        Settings systemSettings2 = injector.getInstance(SystemLevelSettings.class);
        Settings applicationSettings2 = injector.getInstance(ApplicationLevelSettings.class);
        Settings realmSettings2 = injector.getInstance(RealmLevelSettings.class);
        Settings userSettings2 = injector.getInstance(UserLevelSettings.class);
        Settings genericSettings2 = injector.getInstance(Settings.class);

        assertThat(systemSettings1.getString("type")).contains("system");
        assertThat(applicationSettings1.getString("type")).contains("application");
        assertThat(realmSettings1.getString("type")).contains("realm");
        assertThat(userSettings1.getString("type")).contains("user");
        assertThat(genericSettings1.getString("type")).contains("realm");

        assertThat(systemSettings2.getString("type")).contains("system");
        assertThat(applicationSettings2.getString("type")).contains("application");
        assertThat(realmSettings2.getString("type")).contains("realm");
        assertThat(userSettings2.getString("type")).contains("user");
        assertThat(genericSettings2.getString("type")).contains("realm");

        assertThat(ExampleKeyValuesProvider.getConstructionCount()).isEqualTo(4);
    }

    @Test
    void testGuiceInstantiationWithPartlySingletonProviders() {
        ExampleKeyValuesProvider.clearConstructionCount();

        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                Map<String, String> systemMap = new HashMap<>();
                systemMap.put("type", "system");

                Map<String, String> applicationMap = new HashMap<>();
                applicationMap.put("type", "application");

                // Set the correct KeyValueProviders for the different settings.
                bind(KeyValuesProvider.class)
                        .annotatedWith(SystemLevelSettingsKeyValuesProvider.class)
                        .toInstance(new ExampleKeyValuesProvider(systemMap));

                bind(KeyValuesProvider.class)
                        .annotatedWith(ApplicationLevelSettingsKeyValuesProvider.class)
                        .toInstance(new ExampleKeyValuesProvider(applicationMap));

                bind(KeyValuesProvider.class)
                        .annotatedWith(RealmLevelSettingsKeyValuesProvider.class)
                        .to(ExampleKeyValuesProvider.class);

                bind(KeyValuesProvider.class)
                        .annotatedWith(UserLevelSettingsKeyValuesProvider.class)
                        .to(ExampleKeyValuesProvider.class);

                // Set the default settings to realm settings.
                bind(Settings.class).to(RealmLevelSettings.class);
            }
        });

        assertThat(ExampleKeyValuesProvider.getConstructionCount()).isEqualTo(2);

        // The following 5 statements will create new ExampleKeyValuesProvider instances
        Settings systemSettings1 = injector.getInstance(SystemLevelSettings.class);
        assertThat(systemSettings1).isNotNull();
        assertThat(ExampleKeyValuesProvider.getConstructionCount()).isEqualTo(2);

        Settings applicationSettings1 = injector.getInstance(ApplicationLevelSettings.class);
        assertThat(applicationSettings1).isNotNull();
        assertThat(ExampleKeyValuesProvider.getConstructionCount()).isEqualTo(2);

        Settings realmSettings1 = injector.getInstance(RealmLevelSettings.class);
        assertThat(realmSettings1).isNotNull();
        assertThat(ExampleKeyValuesProvider.getConstructionCount()).isEqualTo(3);

        // This creates two ExampleKeyValuesProvider, one for UserSettings, and one for RealmSettings.
        Settings userSettings1 = injector.getInstance(UserLevelSettings.class);
        assertThat(userSettings1).isNotNull();
        assertThat(ExampleKeyValuesProvider.getConstructionCount()).isEqualTo(5);

        // This creates one ExampleKeyValuesProvider for RealmSettings.
        Settings genericSettings1 = injector.getInstance(Settings.class);
        assertThat(ExampleKeyValuesProvider.getConstructionCount()).isEqualTo(6);
        assertThat(genericSettings1).isNotNull();

        // The following 2 statements will use existing singletons for ExampleKeyValuesProvider instances
        Settings systemSettings2 = injector.getInstance(SystemLevelSettings.class);
        Settings applicationSettings2 = injector.getInstance(ApplicationLevelSettings.class);
        assertThat(systemSettings2).isNotNull();
        assertThat(applicationSettings2).isNotNull();
        assertThat(ExampleKeyValuesProvider.getConstructionCount()).isEqualTo(6);

        // This creates one ExampleKeyValuesProvider for RealmSettings.
        Settings realmSettings2 = injector.getInstance(RealmLevelSettings.class);
        assertThat(realmSettings2).isNotNull();
        assertThat(ExampleKeyValuesProvider.getConstructionCount()).isEqualTo(7);

        // This creates two ExampleKeyValuesProvider, one for UserSettings, and one for RealmSettings.
        Settings userSettings2 = injector.getInstance(UserLevelSettings.class);
        assertThat(userSettings2).isNotNull();
        assertThat(ExampleKeyValuesProvider.getConstructionCount()).isEqualTo(9);

        // This creates one ExampleKeyValuesProvider for RealmSettings.
        Settings genericSettings2 = injector.getInstance(Settings.class);
        assertThat(genericSettings2).isNotNull();
        assertThat(ExampleKeyValuesProvider.getConstructionCount()).isEqualTo(10);
    }

}
