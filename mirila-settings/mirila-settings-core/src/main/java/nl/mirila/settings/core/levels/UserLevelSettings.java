package nl.mirila.settings.core.levels;

import com.google.inject.Inject;
import nl.mirila.core.settings.KeyValuesProvider;
import nl.mirila.core.settings.Settings;
import nl.mirila.settings.core.annotations.UserLevelSettingsKeyValuesProvider;

/**
 * Represents settings on the user level, using a fallback to {@link RealmLevelSettings}.
 */
public class UserLevelSettings extends Settings {

    /**
     * Initialize a new instance.
     */
    @Inject
    UserLevelSettings(@UserLevelSettingsKeyValuesProvider KeyValuesProvider provider,
                      RealmLevelSettings realmLevelSettings) {
        super(provider, realmLevelSettings);
    }

}
