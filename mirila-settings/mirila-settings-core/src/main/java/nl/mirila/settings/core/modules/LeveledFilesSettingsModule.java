package nl.mirila.settings.core.modules;

import com.google.inject.AbstractModule;
import nl.mirila.core.settings.KeyValuesProvider;
import nl.mirila.core.settings.Settings;
import nl.mirila.core.settings.providers.KeyValuesPropertiesFileProvider;
import nl.mirila.settings.core.annotations.ApplicationLevelSettingsKeyValuesProvider;
import nl.mirila.settings.core.annotations.RealmLevelSettingsKeyValuesProvider;
import nl.mirila.settings.core.annotations.SystemLevelSettingsKeyValuesProvider;
import nl.mirila.settings.core.annotations.UserLevelSettingsKeyValuesProvider;
import nl.mirila.settings.core.levels.RealmLevelSettings;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

/**
 * A {@link Settings} module that has a settings file per level.
 */
public class LeveledFilesSettingsModule extends AbstractModule {

    private final String mask;

    public LeveledFilesSettingsModule(String path) {
        if (!path.endsWith("/")) {
            path += "/";
        }
        mask = path + "%s";
    }

    @Override
    protected void configure() {
        Properties properties = System.getProperties();

        String systemSettings = properties.getProperty("mirila.system.settings", "system-settings.properties");
        Path systemConfigPath = Paths.get(String.format(mask, systemSettings));
        bind(KeyValuesProvider.class)
                .annotatedWith(SystemLevelSettingsKeyValuesProvider.class)
                .toInstance(new KeyValuesPropertiesFileProvider(systemConfigPath));

        String appSettings = properties.getProperty("mirila.application.settings", "application-settings.properties");
        Path appConfigPath = Paths.get(String.format(mask, appSettings));
        bind(KeyValuesProvider.class)
                .annotatedWith(ApplicationLevelSettingsKeyValuesProvider.class)
                .toInstance(new KeyValuesPropertiesFileProvider(appConfigPath));

        String realmSettings = properties.getProperty("mirila.realm.settings", "realm-settings.properties");
        Path realmConfigPath = Paths.get(String.format(mask, realmSettings));
        bind(KeyValuesProvider.class)
                .annotatedWith(RealmLevelSettingsKeyValuesProvider.class)
                .toInstance(new KeyValuesPropertiesFileProvider(realmConfigPath));

        String userSettings = properties.getProperty("mirila.user.settings", "user-settings.properties");
        Path userConfigPath = Paths.get(String.format(mask, userSettings));
        bind(KeyValuesProvider.class)
                .annotatedWith(UserLevelSettingsKeyValuesProvider.class)
                .toInstance(new KeyValuesPropertiesFileProvider(userConfigPath));

        // Set the default settings to realm settings.
        bind(Settings.class).to(RealmLevelSettings.class);
    }

}
