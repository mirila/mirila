package nl.mirila.settings.core.levels;

import com.google.inject.Inject;
import nl.mirila.core.settings.KeyValuesProvider;
import nl.mirila.core.settings.Settings;
import nl.mirila.settings.core.annotations.ApplicationLevelSettingsKeyValuesProvider;

/**
 * Represents settings on the application level, using a fall back to {@link SystemLevelSettings}.
 */
public class ApplicationLevelSettings extends Settings {

    /**
     * Initialize a new instance.
     */
    @Inject
    ApplicationLevelSettings(@ApplicationLevelSettingsKeyValuesProvider KeyValuesProvider provider,
                             SystemLevelSettings systemLevelSettings) {
        super(provider, systemLevelSettings);
    }

}
