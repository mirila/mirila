package nl.mirila.settings.core.levels;

import com.google.inject.Inject;
import nl.mirila.core.settings.KeyValuesProvider;
import nl.mirila.core.settings.Settings;
import nl.mirila.settings.core.annotations.SystemLevelSettingsKeyValuesProvider;

/**
 * Represents settings on the system level, without using a fallback.
 */
public class SystemLevelSettings extends Settings {

    /**
     * Initialize a new instance.
     */
    @Inject
    SystemLevelSettings(@SystemLevelSettingsKeyValuesProvider KeyValuesProvider provider) {
        super(provider);
    }

}
