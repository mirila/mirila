package nl.mirila.settings.core.annotations;

import nl.mirila.core.settings.KeyValuesProvider;

import javax.inject.Qualifier;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Use this annotation for a class that implements a {@link KeyValuesProvider} that provides configurations
 * on the realm level.
 */
@Qualifier
@Target({ FIELD, PARAMETER, METHOD })
@Retention(RUNTIME)
public @interface RealmLevelSettingsKeyValuesProvider {
}
