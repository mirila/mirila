package nl.mirila.settings.core.levels;

import com.google.inject.Inject;
import nl.mirila.core.settings.KeyValuesProvider;
import nl.mirila.core.settings.Settings;
import nl.mirila.settings.core.annotations.RealmLevelSettingsKeyValuesProvider;

/**
 * Represents settings on the realm, using a fallback to {@link ApplicationLevelSettings}.
 */
public class RealmLevelSettings extends Settings {

    /**
     * Initialize a new instance.
     */
    @Inject
    RealmLevelSettings(@RealmLevelSettingsKeyValuesProvider KeyValuesProvider provider,
                       ApplicationLevelSettings applicationLevelSettings) {
        super(provider, applicationLevelSettings);
    }

}
