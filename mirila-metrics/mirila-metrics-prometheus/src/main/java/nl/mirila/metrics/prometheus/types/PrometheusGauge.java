package nl.mirila.metrics.prometheus.types;

import io.prometheus.client.Gauge;
import nl.mirila.metrics.core.MetricInfo;
import nl.mirila.metrics.core.types.Metric;

import static nl.mirila.metrics.prometheus.PrometheusUtils.*;

/**
 * A {@link PrometheusGauge} represents a {@link Gauge} within the Prometheus metrics system.
 */
public class PrometheusGauge implements nl.mirila.metrics.core.types.Gauge {

    private final Gauge gauge;

    /**
     * Initialize a new instance.
     */
    public <T extends Metric> PrometheusGauge(MetricInfo<T> info) {
        gauge = Gauge.build()
                .name(getNormalizedKey(info))
                .help(getNormalizedDescription(info))
                .labelNames(getLabelsAsArray(info))
                .register();
    }


}
