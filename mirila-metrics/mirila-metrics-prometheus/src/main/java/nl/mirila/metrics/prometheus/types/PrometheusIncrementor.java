package nl.mirila.metrics.prometheus.types;

import io.prometheus.client.Counter;
import nl.mirila.metrics.core.MetricInfo;
import nl.mirila.metrics.core.types.Incrementor;
import nl.mirila.metrics.core.types.Metric;

import static nl.mirila.metrics.prometheus.PrometheusUtils.*;

/**
 * A {@link PrometheusIncrementor} represents a {@link Incrementor} within the Prometheus metrics system.
 */
public class PrometheusIncrementor implements Incrementor {

    private final Counter counter;

    /**
     * Initialize a new instance.
     */
    public <T extends Metric> PrometheusIncrementor(MetricInfo<T> info) {
        counter = Counter.build()
                .name(getNormalizedKey(info))
                .help(getNormalizedDescription(info))
                .labelNames(getLabelsAsArray(info))
                .register();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void increment(int step) {
        counter.inc(step);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long getValue() {
        return (long) counter.get();
    }

}
