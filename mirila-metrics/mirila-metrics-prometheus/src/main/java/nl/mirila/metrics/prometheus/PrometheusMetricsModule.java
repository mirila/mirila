package nl.mirila.metrics.prometheus;

import com.google.inject.AbstractModule;
import com.google.inject.multibindings.OptionalBinder;
import nl.mirila.metrics.core.MetricProvider;

public class PrometheusMetricsModule extends AbstractModule {

    @Override
    protected void configure() {
        OptionalBinder.newOptionalBinder(binder(), MetricProvider.class)
                .setBinding()
                .to(PrometheusMetricsProvider.class);
    }

}
