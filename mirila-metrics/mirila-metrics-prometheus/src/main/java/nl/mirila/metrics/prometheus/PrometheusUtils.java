package nl.mirila.metrics.prometheus;

import io.prometheus.client.Collector;
import io.prometheus.client.CollectorRegistry;
import io.prometheus.client.exporter.common.TextFormat;
import nl.mirila.metrics.core.MetricInfo;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Enumeration;

/**
 * The {@link PrometheusUtils} provides convenient methods for working with Prometheus metrics system.
 */
public abstract class PrometheusUtils {

    private static final Logger logger = LogManager.getLogger(PrometheusUtils.class);

    /**
     * Return a normalized key, mapping all invalid Prometheus key characters into underscores.
     * <p>
     * @param info The metric info containing the key.
     * @return The normalized key.
     */
    public static String getNormalizedKey(MetricInfo<?> info) {
        return info.getKey().replaceAll("[^a-zA-Z0-9_:]", "_");
    }

    /**
     * Return a normalized description, or a default description if missing.
     * <p>
     * @param info The metric info containing the description.
     * @return The normalized description.
     */
    public static String getNormalizedDescription(MetricInfo<?> info) {
        return StringUtils.defaultIfBlank(info.getDescription(), "Unknown description for key " + info.getKey());
    }

    /**
     * Return a array of String with all labels in the given {@link MetricInfo}.
     * <p>
     * @param info The metric info containing the labels.
     * @return The string array.
     */
    public static String[] getLabelsAsArray(MetricInfo<?> info) {
        return info.getLabels().toArray(String[]::new);
    }

    /**
     * Return all Prometheus methods as string. This can be used to exposed the metrics via a servlet.
     */
    public static String getMetricsAsString() {
        StringWriter writer = new StringWriter();
        Enumeration<Collector.MetricFamilySamples> samples = CollectorRegistry.defaultRegistry.metricFamilySamples();
        try {
            TextFormat.writeFormat(TextFormat.CONTENT_TYPE_004, writer, samples);
            return writer.getBuffer().toString();
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            return "";
        }
    }

}
