package nl.mirila.metrics.prometheus.types;

import io.prometheus.client.Gauge;
import nl.mirila.metrics.core.MetricInfo;
import nl.mirila.metrics.core.types.Counter;
import nl.mirila.metrics.core.types.Metric;

import static nl.mirila.metrics.prometheus.PrometheusUtils.*;

/**
 * A {@link PrometheusCounter} represents a {@link Counter} within the Prometheus metrics system.
 */
public class PrometheusCounter implements Counter {

    private final Gauge gauge;

    /**
     * Initialize a new instance.
     */
    public <T extends Metric> PrometheusCounter(MetricInfo<T> info) {
        gauge = Gauge.build()
                .name(getNormalizedKey(info))
                .help(getNormalizedDescription(info))
                .labelNames(getLabelsAsArray(info))
                .register();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void decrement(int step) {
        gauge.dec(step);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void increment(int step) {
        gauge.inc(step);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long getValue() {
        return (long) gauge.get();
    }

}
