package nl.mirila.metrics.prometheus;

import com.google.inject.Inject;
import nl.mirila.metrics.core.MetricInfo;
import nl.mirila.metrics.core.MetricProvider;
import nl.mirila.metrics.core.MetricsRegistry;
import nl.mirila.metrics.core.types.Counter;
import nl.mirila.metrics.core.types.Gauge;
import nl.mirila.metrics.core.types.Incrementor;
import nl.mirila.metrics.prometheus.types.PrometheusCounter;
import nl.mirila.metrics.prometheus.types.PrometheusGauge;
import nl.mirila.metrics.prometheus.types.PrometheusIncrementor;

/**
 * A {@link PrometheusMetricsProvider} represents the {@link MetricProvider} for the Prometheus metrics system.
 */
public class PrometheusMetricsProvider implements MetricProvider {

    private final MetricsRegistry registry;

    /**
     * Initialize a new instance.
     */
    @Inject
    public PrometheusMetricsProvider(MetricsRegistry registry) {
        this.registry = registry;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MetricsRegistry getRegistry() {
        return registry;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Counter getCounter(MetricInfo<? extends Counter> metricInfo) {
        return new PrometheusCounter(metricInfo);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Gauge getGauge(MetricInfo<? extends Gauge> metricInfo) {
        return new PrometheusGauge(metricInfo);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Incrementor getIncrementor(MetricInfo<? extends Incrementor> metricInfo) {
        return new PrometheusIncrementor(metricInfo);
    }

}
