package nl.mirila.metrics.prometheus;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import nl.mirila.metrics.core.MetricProvider;
import nl.mirila.metrics.core.types.Counter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;

public class PrometheusMetricsTest {

    private MetricProvider provider;

    @BeforeEach
    void setUp() {
        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                install(new PrometheusMetricsModule());
            }
        });
        provider = injector.getInstance(MetricProvider.class);
    }

    @Test
    void testPrometheus() {
        Counter counter1 = provider.getCounter("it_test_counter_1");
        IntStream.range(0, 10).forEach((i) -> counter1.increment());

        Counter counter2 = provider.getCounter("it_test_counter_2");
        IntStream.range(0, 20).forEach((i) -> counter2.increment());

        Counter counter3 = provider.getCounter("it_test_counter_3");
        IntStream.range(0, 30).forEach((i) -> counter3.increment());

        String metricsString = PrometheusUtils.getMetricsAsString();

        assertThat(metricsString)
                .contains("it_test_counter_1 10.0")
                .contains("it_test_counter_2 20.0")
                .contains("it_test_counter_3 30.0");
    }


}
