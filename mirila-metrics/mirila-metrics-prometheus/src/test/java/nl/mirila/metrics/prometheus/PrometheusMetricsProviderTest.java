package nl.mirila.metrics.prometheus;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import nl.mirila.core.exceptions.MirilaException;
import nl.mirila.metrics.core.MetricInfo;
import nl.mirila.metrics.core.MetricProvider;
import nl.mirila.metrics.core.types.Counter;
import nl.mirila.metrics.core.types.Gauge;
import nl.mirila.metrics.core.types.Incrementor;
import nl.mirila.metrics.prometheus.types.PrometheusCounter;
import nl.mirila.metrics.prometheus.types.PrometheusGauge;
import nl.mirila.metrics.prometheus.types.PrometheusIncrementor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

class PrometheusMetricsProviderTest {

    private MetricProvider provider;

    @BeforeEach
    void setUp() {
        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                install(new PrometheusMetricsModule());
            }
        });
        provider = injector.getInstance(MetricProvider.class);
    }

    @Test
    void testGetCounter() {
        MetricInfo<Counter> metricInfo = MetricInfo.forCounter("counter-test");
        assertThat(provider.getCounter(metricInfo)).isInstanceOf(PrometheusCounter.class);
    }

    @Test
    void testGetGauge() {
        MetricInfo<Gauge> metricInfo = MetricInfo.forGauge("gauge-test");
        assertThat(provider.getGauge(metricInfo)).isInstanceOf(PrometheusGauge.class);
    }

    @Test
    void testGetIncrementor() {
        MetricInfo<Incrementor> metricInfo = MetricInfo.forIncrementor("incrementor-test");
        assertThat(provider.getIncrementor(metricInfo)).isInstanceOf(PrometheusIncrementor.class);
    }

    @Test
    void testGetExistingMetric() {
        String singleMetricId = "one_single_metric_id";
        MetricInfo<Counter> counterInfo1 = MetricInfo.forCounter(singleMetricId);
        Counter counter1 = provider.get(counterInfo1);

        MetricInfo<Counter> counterInfo2 = MetricInfo.forCounter(singleMetricId);
        Counter counter2 = provider.get(counterInfo2);

        assertThat(counter2).isEqualTo(counter1);
    }

    @Test
    void testGetExistingMetricWithIncorrectType() {
        String singleMetricId = "one_single_metric_id_with_different_types";
        MetricInfo<Counter> counterInfo1 = MetricInfo.forCounter(singleMetricId);
        Counter counter = provider.get(counterInfo1);
        assertThat(counter).isNotNull();

        // Creating new info, but with different type.
        MetricInfo<Gauge> counterInfo2 = MetricInfo.forGauge(singleMetricId);
        assertThatExceptionOfType(MirilaException.class).isThrownBy(() -> {
            provider.get(counterInfo2);
        });
    }

}
