package nl.mirila.metrics.core;

import nl.mirila.metrics.core.types.Counter;
import nl.mirila.metrics.core.types.Gauge;
import nl.mirila.metrics.core.types.Incrementor;
import nl.mirila.metrics.core.types.Metric;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * The {@link MetricInfo} is a wrapper for a {@link Metric}, that contains metadata about that metric.
 */
public class MetricInfo<T extends Metric> {

    private final Class<T> metricType;
    private final String key;
    private String description;
    private final List<String> labels;
    private final List<MetricOption> options;

    /**
     * Initialize a new builder fot the given {@link Metric} and the given key.
     * <p>
     * @param metricType The class of the metric type.
     * @param key The unique key that should be used.
     */
    MetricInfo(Class<T> metricType, String key) {
        this.metricType = metricType;
        this.key = key;
        description = "";
        labels = new ArrayList<>();
        options = new ArrayList<>();
    }

    /**
     * Return the metricType.
     */
    public Class<T> getMetricType() {
        return metricType;
    }

    /**
     * Return the key.
     */
    public String getKey() {
        return key;
    }

    /**
     * Return the description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Create a new instance of a {@link Counter}, related to the given {@link Metric} using the given key.
     * <p>
     * @param key The key of the metric.
     * @return The metric info.
     */
    public static MetricInfo<Counter> forCounter(String key) {
        return new MetricInfo<>(Counter.class, key);
    }

    /**
     * Create a new instance of a {@link Gauge}, related to the given {@link Metric} using the given key.
     * <p>
     * @param key The key of the metric.
     * @return The metric info.
     */
    public static MetricInfo<Gauge> forGauge(String key) {
        return new MetricInfo<>(Gauge.class, key);
    }

    /**
     * Create a new instance of a {@link Incrementor}, related to the given {@link Metric} using the given key.
     * <p>
     * @param key The key of the metric.
     * @return The metric info.
     */
    public static MetricInfo<Incrementor> forIncrementor(String key) {
        return new MetricInfo<>(Incrementor.class, key);
    }

    /**
     * Set the given description and return this instance for chaining purposes.
     */
    public MetricInfo<T> withDescription(String description) {
        this.description = description;
        return this;
    }

    /**
     * Return the labels.
     */
    public List<String> getLabels() {
        return labels;
    }

    /**
     * Set the given label.
     */
    public MetricInfo<T> withLabel(String label) {
        labels.add(label);
        return this;
    }

    /**
     * Set the given labels.
     */
    public MetricInfo<T> withLabels(String... labels) {
        this.labels.addAll(Arrays.asList(labels));
        return this;
    }

    /**
     * Return the options.
     */
    public List<MetricOption> getOptions() {
        return options;
    }

    /**
     * Add the given option.
     */
    public MetricInfo<T> addOption(MetricOption option) {
        options.add(option);
        return this;
    }

    /**
     * Add the given option.
     */
    public MetricInfo<T> withOptions(MetricOption... options) {
        this.options.addAll(Arrays.asList(options));
        return this;
    }

}
