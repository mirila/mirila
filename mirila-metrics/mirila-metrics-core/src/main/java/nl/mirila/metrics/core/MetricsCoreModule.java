package nl.mirila.metrics.core;

import com.google.inject.AbstractModule;
import com.google.inject.multibindings.OptionalBinder;

public class MetricsCoreModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(MetricsRegistry.class).asEagerSingleton();

        OptionalBinder.newOptionalBinder(binder(), MetricProvider.class)
                .setDefault()
                .to(NoMetricsProvider.class);
    }

}
