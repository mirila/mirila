package nl.mirila.metrics.core;

import com.google.inject.Inject;
import nl.mirila.core.exceptions.MirilaException;
import nl.mirila.metrics.core.types.Metric;

import javax.inject.Singleton;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

/**
 * The {@link MetricsRegistry} contains a list of all created metrics.
 */
@Singleton
public class MetricsRegistry {

    private final Map<String, Metric> registry;

    /**
     * Initialize a new instance.
     */
    @Inject
    public MetricsRegistry() {
        registry = new HashMap<>();
    }

    /**
     * Return the existing {@link Metric} related to the given {@link MetricInfo} from the registry,
     * wrapped in an {@link Optional}.
     * <p>
     * @param info The information about the metric.
     * @param <T> The type of the metric.
     * @return The metric, if available.
     */
    public <T extends Metric> Optional<T> getMetric(MetricInfo<T> info) {
        if (!registry.containsKey(info.getKey())) {
            return Optional.empty();
        }
        Metric metric = registry.get(info.getKey());
        try {
            return Optional.of(info.getMetricType().cast(metric));
        } catch (ClassCastException e) {
            String template = "Metric with key %s is of type %s, but type %s was requested.";
            String message = String.format(template, info.getKey(), metric.getClass(), info.getMetricType());
            throw new MirilaException(message);
        }
    }

    /**
     * Return the existing {@link Metric} related to the given {@link MetricInfo} from the registry,
     * or create a new metric using the given metric supplier. A newly created metric is directly registered to the
     * registry.
     * <p>
     * @param info The information about the metric.
     * @param metricSupplier The supplier that creates new metrics.
     * @param <T> The type of the metric.
     * @return The existing or newly created metric.
     */
    public <T extends Metric> T getMetric(MetricInfo<T> info, Function<MetricInfo<T>, T> metricSupplier) {
        Optional<T> optional = getMetric(info);
        T metric;
        if (optional.isPresent()) {
            metric = optional.get();
        } else {
            metric = metricSupplier.apply(info);
            registry.put(info.getKey(), metric);
        }
        return metric;
    }

}
