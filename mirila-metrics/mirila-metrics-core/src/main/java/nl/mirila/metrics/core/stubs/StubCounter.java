package nl.mirila.metrics.core.stubs;

import nl.mirila.metrics.core.types.Counter;

/**
 * A stub that represents a {@link Counter}.
 */
public class StubCounter extends StubIncrementor implements Counter {

    /**
     * Stubbing a decrement.
     */
    @Override
    public void decrement(int step) {
        // no-op
    }

}
