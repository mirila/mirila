package nl.mirila.metrics.core;

import nl.mirila.core.exceptions.MirilaException;
import nl.mirila.metrics.core.stubs.StubCounter;
import nl.mirila.metrics.core.stubs.StubGauge;
import nl.mirila.metrics.core.stubs.StubIncrementor;
import nl.mirila.metrics.core.types.Counter;
import nl.mirila.metrics.core.types.Gauge;
import nl.mirila.metrics.core.types.Incrementor;
import nl.mirila.metrics.core.types.Metric;

/**
 * The {@link MetricProvider} provides a metric.
 */
public interface MetricProvider {

    /**
     * Return a metric based on the information in the given {@link MetricInfo}.
     * <p>
     * @param metricInfo The metric info.
     * @param <T> The type of the metric.
     * @return The metric.
     */
    default <T extends Metric> T get(MetricInfo<T> metricInfo) {
        return getRegistry().getMetric(metricInfo, (info) -> {
            Metric metric;
            if (metricInfo.getMetricType() == Incrementor.class) {
                metric = new StubIncrementor();
            } else if (metricInfo.getMetricType() == Counter.class) {
                metric = new StubCounter();
            } else if (metricInfo.getMetricType() == Gauge.class) {
                metric = new StubGauge();
            } else {
                throw new MirilaException("Unknown metric type");
            }
            return metricInfo.getMetricType().cast(metric);
        });
    }

    /**
     * Return the registry to use.
     */
    MetricsRegistry getRegistry();

    /**
     * Return a counter based metric, using the given {@link MetricInfo}. By default, a stub is returned. Override
     * this method to return a concrete implementation.
     * <p>
     * @param metricInfo The metric information.
     * @return The counter.
     */
    default Counter getCounter(MetricInfo<? extends Counter> metricInfo) {
        return new StubCounter();
    }

    /**
     * Return a counter based metric, using the given key. By default, a stub is returned. Override this method to
     * return a concrete implementation.
     * <p>
     * @param key The key to use for the metric.
     * @return The counter.
     */
    default Counter getCounter(String key) {
        return getCounter(MetricInfo.forCounter(key));
    }

    /**
     * Return a gauge based metric, using the given {@link MetricInfo}. By default, a stub is returned. Override
     * this method to return a concrete implementation.
     * <p>
     * @param metricInfo The metric information.
     * @return The counter.
     */
    default Gauge getGauge(MetricInfo<? extends Gauge> metricInfo) {
        return new StubGauge();
    }

    /**
     * Return a gauge based metric, using the given key. By default, a stub is returned. Override this method to return
     * a concrete implementation.
     * <p>
     * @param key The key to use for the metric.
     * @return The counter.
     */
    default Gauge getGauge(String key) {
        return getGauge(MetricInfo.forGauge(key));
    }

    /**
     * Return a incrementor based metric, using the given {@link MetricInfo}. By default, a stub is returned. Override
     * this method to return a concrete implementation.
     * <p>
     * @param metricInfo The metric information.
     * @return The counter.
     */
    default Incrementor getIncrementor(MetricInfo<? extends Incrementor> metricInfo) {
        return new StubIncrementor();
    }

    /**
     * Return a incrementor based metric, using the given key. By default, a stub is returned. Override this method to
     * return a concrete implementation.
     * <p>
     * @param key The key to use for the metric.
     * @return The counter.
     */
    default Incrementor getIncrementor(String key) {
        return getIncrementor(MetricInfo.forIncrementor(key));
    }

}
