package nl.mirila.metrics.core;

import com.google.inject.Inject;

/**
 * This {@link MetricProvider} does not override any of the metrics getters, resulting in only providing metric stubs.
 */
public class NoMetricsProvider implements MetricProvider {

    private final MetricsRegistry registry;

    /**
     * Initialize a new instance.
     */
    @Inject
    public NoMetricsProvider(MetricsRegistry registry) {
        this.registry = registry;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MetricsRegistry getRegistry() {
        return registry;
    }

}
