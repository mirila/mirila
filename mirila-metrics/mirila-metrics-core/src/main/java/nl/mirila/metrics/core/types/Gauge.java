package nl.mirila.metrics.core.types;

/**
 * A {@link Gauge} represents a type of metric that can increase and decrease its value.
 */
public interface Gauge extends Metric {

}
