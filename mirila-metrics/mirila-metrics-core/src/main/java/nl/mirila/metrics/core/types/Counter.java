package nl.mirila.metrics.core.types;

/**
 * The {@link Counter} is a metric that allows incrementing and decrementing a metric..
 */
public interface Counter extends Incrementor {

    /**
     * Decrement the metric by the given step.
     * <p>
     * @param step The step by which to decrement.
     */
    void decrement(int step);

    /**
     * Decrement the metric by 1.
     */
    default void decrement() {
        decrement(1);
    }

    /**
     * Return the counter value.
     */
    long getValue();

}
