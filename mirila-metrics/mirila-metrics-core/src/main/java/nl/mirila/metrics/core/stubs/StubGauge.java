package nl.mirila.metrics.core.stubs;

import nl.mirila.metrics.core.types.Gauge;

/**
 * A stub that represents a {@link Gauge}.
 */
public class StubGauge implements Gauge {

}
