package nl.mirila.metrics.core.stubs;

import nl.mirila.metrics.core.types.Incrementor;

/**
 * A stub that represents a {@link StubIncrementor}.
 */
public class StubIncrementor implements Incrementor {

    /**
     * Stubbing an increment.
     */
    @Override
    public void increment(int step) {
        // no-op
    }

    /**
     * Stubbing returning a value.
     */
    @Override
    public long getValue() {
        return 0;
    }

}
