package nl.mirila.metrics.core.types;

/**
 * The {@link Incrementor} is a counter that only allows incrementing.
 */
public interface Incrementor extends Metric {

    /**
     * Increment the metric by the given step.
     * <p>
     * @param step The step by which to increment.
     */
    void increment(int step);

    /**
     * Increment the metric by 1.
     */
    default void increment() {
        increment(1);
    }

    /**
     * Return the counter value.
     */
    long getValue();


}
