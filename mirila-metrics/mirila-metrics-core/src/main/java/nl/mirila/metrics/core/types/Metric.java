package nl.mirila.metrics.core.types;

/**
 * A {@link Metric} represents a measurable value for monitoring purposes.
 */
public interface Metric {

}
