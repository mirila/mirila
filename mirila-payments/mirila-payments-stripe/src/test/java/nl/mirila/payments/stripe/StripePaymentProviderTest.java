package nl.mirila.payments.stripe;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.stripe.param.checkout.SessionCreateParams;
import nl.mirila.core.settings.modules.KeyValuesSettingsModule;
import nl.mirila.payments.core.Payment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Locale;

import static nl.mirila.payments.core.PaymentStatus.PAID;
import static nl.mirila.payments.core.PaymentStatus.PENDING;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

class StripePaymentProviderTest {

    private StripePaymentProvider provider;

    @BeforeEach
    void setUp() {
        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                install(new KeyValuesSettingsModule(new HashMap<>()));
            }
        });
        provider = injector.getInstance(StripePaymentProvider.class);
    }

    @Test
    void testConvertPaymentToSessionCreateParams() {
        Payment payment = Payment.withAmount("12.34")
                .withDescription("This is a payment description.")
                .withSuccessUrl("https://www.example.com/success")
                .withLocale(new Locale("nl", "NL"));

        SessionCreateParams params = provider.convertPaymentToSessionCreateParams(payment);

        SessionCreateParams.LineItem.PriceData priceData = params.getLineItems().get(0).getPriceData();

        assertThat(priceData)
                .extracting("unitAmountDecimal", "currency")
                .contains(new BigDecimal("1234"), "EUR");

        assertThat(priceData.getProductData().getName())
                .isEqualTo(payment.getDescription());

        assertThat(params.getSuccessUrl()).contains("https://www.example.com/success");
        assertThat(params.getLocale())
                .isNotNull()
                .isEqualTo(SessionCreateParams.Locale.NL);
    }

    @Test
    void testConvertStripeStatusToPaymentStatusWithPaid() {
        assertThat(provider.convertStripeStatusToPaymentStatus("complete", "paid"))
                .isEqualTo(PAID);
    }

    @Test
    void testConvertStripeStatusToPaymentStatusWithProcessing() {
        assertThat(provider.convertStripeStatusToPaymentStatus("open", "processing"))
                .isEqualTo(PENDING);
    }

    @Test
    void testConvertStripeStatusToPaymentStatusWithNull() {
        assertThatExceptionOfType(NullPointerException.class)
                .isThrownBy(() -> provider.convertStripeStatusToPaymentStatus("open", null));
    }
}