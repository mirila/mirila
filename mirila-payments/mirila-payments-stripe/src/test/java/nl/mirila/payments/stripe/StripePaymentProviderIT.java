package nl.mirila.payments.stripe;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import nl.mirila.core.settings.modules.KeyValuesSettingsModule;
import nl.mirila.drivers.stripe.StripeDriver;
import nl.mirila.drivers.stripe.StripeSettings;
import nl.mirila.payments.core.Payment;
import nl.mirila.payments.core.PaymentStatus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

class StripePaymentProviderIT {

    private static final Logger logger = LogManager.getLogger(StripePaymentProviderIT.class);

    private StripePaymentProvider provider;
    private Payment payment;
    private StripeDriver driver;

    @BeforeEach
    void setUp() {
        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                String key = "sk_test_51K22jQIQ3eBKphWD0upfeByhRKOzGYrH6UI5wVGz94tnj1aIA2FW6N38NoE4SZlFCE2O1HJQJtwm4" +
                        "ENhT3ZntV8800j1vQsvTK";
                Map<String, String> settingsMap = new HashMap<>();
                settingsMap.put(StripeSettings.KEY_API_KEY, key);
                install(new KeyValuesSettingsModule(settingsMap));
            }
        });
        provider = injector.getInstance(StripePaymentProvider.class);
        payment = Payment.withAmount("12.34")
                .withDescription("This is a payment description.")
                .withSuccessUrl("https://www.example.com/success")
                .withLocale(new Locale("nl", "NL"));
    }

    /**
     * This test is disabled, to prevent excessive use of the external API of Stripe.
     */
    @Test
    @Disabled
    void testCreatePaymentProcess() {
        String redirectUrl = provider.createPaymentRedirect(payment);
        assertThat(redirectUrl).startsWith("https://checkout.stripe.com/c/pay");
        logger.info("Redirect url: {}", redirectUrl);
    }

    /**
     * This test is disabled, as it requires manual interaction. Use the first test to generate a redirect url.
     * After faking a successful payment, run this test.
     */
    @Test
    @Disabled
    void testPaymentStatus() {
        payment.setPaymentId("cs_test_a1I0i9VeK3nUuvYuvZpWoih5DXoE4CNayAG6hZLR7LzbXO2xJ1QezLr0yY");
        assertThat(provider.checkStatus(payment)).isEqualTo(PaymentStatus.PAID);
    }

}