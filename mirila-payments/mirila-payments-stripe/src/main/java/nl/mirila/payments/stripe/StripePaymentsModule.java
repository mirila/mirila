package nl.mirila.payments.stripe;

import com.google.inject.AbstractModule;
import nl.mirila.payments.core.PaymentProvider;

public class StripePaymentsModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(PaymentProvider.class).to(StripePaymentProvider.class);
    }

}
