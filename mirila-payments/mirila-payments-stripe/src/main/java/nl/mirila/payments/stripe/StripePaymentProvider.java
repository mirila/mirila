package nl.mirila.payments.stripe;

import com.google.inject.Inject;
import com.stripe.exception.StripeException;
import com.stripe.model.checkout.Session;
import com.stripe.param.checkout.SessionCreateParams;
import com.stripe.param.checkout.SessionCreateParams.LineItem.PriceData;
import nl.mirila.core.exceptions.MirilaException;
import nl.mirila.drivers.core.DriverInformation;
import nl.mirila.drivers.core.DriverStatus;
import nl.mirila.drivers.stripe.StripeDriver;
import nl.mirila.payments.core.Payment;
import nl.mirila.payments.core.PaymentProvider;
import nl.mirila.payments.core.PaymentStatus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.util.Objects;

import static org.apache.commons.lang3.StringUtils.isBlank;


/**
 * The type Stripe payment provider.
 */
public class StripePaymentProvider implements PaymentProvider {

    private static final Logger logger = LogManager.getLogger(StripePaymentProvider.class);

    private StripeDriver driver;

    /**
     * Initialize a new instance.
     *
     * @param driver the driver
     */
    @Inject
    public StripePaymentProvider(StripeDriver driver) {
        this.driver = driver;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String createPaymentRedirect(Payment payment) {
        DriverInformation information = driver.getDriverInformation();
        if (information.getStatus() != DriverStatus.HEALTHY) {
            logger.error(information.getStatusMessage());
            throw new MirilaException(information.getStatusMessage());
        }
        SessionCreateParams params = convertPaymentToSessionCreateParams(payment);
        try {
            Session session = Session.create(params);
            logger.info("Created a Stripe payment with id {}.", session.getId());
            return session.getUrl();
        } catch (StripeException e) {
            logger.error(e.getMessage());
            throw new MirilaException(e.getMessage());
        }
    }

    /**
     * Convert to given {@link Payment} into a {@link SessionCreateParams} of Stripe.
     *
     * @param payment The payment.
     * @return The session create parameters.
     */
    protected SessionCreateParams convertPaymentToSessionCreateParams(Payment payment) {
        // Compensate the amount for used decimals in the currency.
        BigDecimal amount = payment.getAmount().movePointRight(payment.getCurrency().getDefaultFractionDigits());

        PriceData.ProductData productData = PriceData.ProductData.builder()
                .setName(payment.getDescription())
                .build();

        PriceData priceData = PriceData.builder()
                .setCurrency(payment.getCurrency().getCurrencyCode())
                .setUnitAmountDecimal(amount)
                .setProductData(productData)
                .build();

        SessionCreateParams.LineItem lineItem = SessionCreateParams.LineItem.builder()
                .setQuantity(1L)
                .setPriceData(priceData)
                .build();

        SessionCreateParams.Locale locale;
        try {
            locale = SessionCreateParams.Locale.valueOf(payment.getLocale().getLanguage().toUpperCase());
        } catch (Exception ignore) {
            locale = SessionCreateParams.Locale.AUTO;
        }

        return SessionCreateParams.builder()
                .setMode(SessionCreateParams.Mode.PAYMENT)
                .setLocale(locale)
                .setSuccessUrl(payment.getSuccessUrl().orElse(null))
                .setCancelUrl(payment.getCancelUrl().orElse(null))
                .addLineItem(lineItem)
                .build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PaymentStatus checkStatus(Payment payment) {
        if (isBlank(payment.getPaymentId())) {
            logger.error("Can not check the payment, because the payment id is missing.");
            return PaymentStatus.FAILED;
        }
        try {
            Session session = Session.retrieve(payment.getPaymentId());
            payment.setStatus(convertStripeStatusToPaymentStatus(session.getStatus(), session.getPaymentStatus()));
            return payment.getStatus();
        } catch (StripeException e) {
            logger.error(e.getMessage());
            return PaymentStatus.FAILED;
        }
    }

    /**
     * Return the {@link PaymentStatus} for the given Stripe status.
     *
     * @param sessionStatus the session status.
     * @param paymentStatus the payment status.
     * @return The internal payment status.
     */
    protected PaymentStatus convertStripeStatusToPaymentStatus(String sessionStatus, String paymentStatus) {
        Objects.requireNonNull(sessionStatus);
        Objects.requireNonNull(paymentStatus);
        return switch (sessionStatus) {
            case "open" -> PaymentStatus.PENDING;
            case "expired" -> PaymentStatus.FAILED;
            default -> switch (paymentStatus) {
                case "unpaid" -> PaymentStatus.CANCELED;
                case "no_payment_required", "paid" -> PaymentStatus.PAID;
                default -> PaymentStatus.FAILED;
            };
        };
    }

}
