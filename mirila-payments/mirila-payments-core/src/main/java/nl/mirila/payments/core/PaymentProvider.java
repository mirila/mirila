package nl.mirila.payments.core;

/**
 * The interface PaymentProvider represents any Payment Service Provider that can be used.
 */
public interface PaymentProvider {

    /**
     * Create a payment redirect using the given {@link Payment}.
     *
     * @param payment The payment to use.
     * @return The redirect url to send to the payer.
     */
    String createPaymentRedirect(Payment payment);

    /**
     * Fetch the status of the payment at the Payment Service Provider for the given {@link Payment}.
     * <p>
     * The status is returned and also updated in the given payment.
     *
     * @param payment The payment to use.
     * @return The payment status.
     */
    PaymentStatus checkStatus(Payment payment);

}
