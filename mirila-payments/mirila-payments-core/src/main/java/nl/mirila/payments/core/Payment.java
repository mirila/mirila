package nl.mirila.payments.core;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Locale;
import java.util.Optional;

public class Payment {

    private final BigDecimal amount;
    private Currency currency;
    private Locale locale;

    private PaymentStatus status;
    private String paymentId;

    private String description;

    private String successUrl;
    private String cancelUrl;
    private String errorUrl;

    /**
     * Initialize a new instance.
     */
    private Payment(BigDecimal amount) {
        this.amount = amount;
        locale = Locale.getDefault();
        currency = Currency.getInstance(Locale.getDefault());
        status = PaymentStatus.STARTED;
    }

    /**
     * Create a new instance of {@link Payment} with the given amount.
     **/
    public static Payment withAmount(BigDecimal amount) {
        return new Payment(amount);
    }

    /**
     * Create a new instance of {@link Payment} with the given amount.
     **/
    public static Payment withAmount(String amount) {
        return withAmount(new BigDecimal(amount));
    }

    /**
     * Return amount.
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Return currency.
     */
    public Currency getCurrency() {
        return currency;
    }

    /**
     * Set currency and return this instance for chaining purposes.
     **/
    public Payment withCurrency(Currency currency) {
        this.currency = currency;
        return this;
    }

    /**
     * Return locale.
     */
    public Locale getLocale() {
        return locale;
    }

    /**
     * Return status.
     */
    public PaymentStatus getStatus() {
        return status;
    }

    /**
     * Set status.
     **/
    public void setStatus(PaymentStatus status) {
        this.status = status;
    }

    /**
     * Set status and return this instance for chaining purposes.
     **/
    public Payment withStatus(PaymentStatus status) {
        this.status = status;
        return this;
    }

    /**
     * Return the external id of the payment.
     */
    public String getPaymentId() {
        return paymentId;
    }

    /**
     * Set the external id of the payment.
     **/
    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    /**
     * Set the external id of the payment and return this instance for chaining purposes.
     **/
    public Payment withPaymentId(String paymentId) {
        this.paymentId = paymentId;
        return this;
    }

    /**
     * Return description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set description and return this instance for chaining purposes.
     **/
    public Payment withDescription(String description) {
        this.description = description;
        return this;
    }

    /**
     * Set locale and return this instance for chaining purposes.
     **/
    public Payment withLocale(Locale locale) {
        this.locale = locale;
        return this;
    }

    /**
     * Return success url.
     */
    public Optional<String> getSuccessUrl() {
        return Optional.ofNullable(successUrl);
    }

    /**
     * Set success url and return this instance for chaining purposes.
     **/
    public Payment withSuccessUrl(String successUrl) {
        this.successUrl = successUrl;
        return this;
    }

    /**
     * Return cancel url.
     */
    public Optional<String> getCancelUrl() {
        return Optional.ofNullable(cancelUrl);
    }

    /**
     * Set cancel url and return this instance for chaining purposes.
     **/
    public Payment withCancelUrl(String cancelUrl) {
        this.cancelUrl = cancelUrl;
        return this;
    }

    /**
     * Return error url.
     */
    public Optional<String> getErrorUrl() {
        return Optional.ofNullable(errorUrl);
    }

    /**
     * Set error url and return this instance for chaining purposes.
     **/
    public Payment withErrorUrl(String errorUrl) {
        this.errorUrl = errorUrl;
        return this;
    }

}
