package nl.mirila.payments.core;


import java.util.Optional;
import java.util.stream.Stream;

public enum PaymentStatus {
    STARTED("started"),
    PENDING("pending"),
    PAID("paid"),
    CANCELED("canceled"),
    FAILED("failed");

    private final String name;

    /**
     * Initialize a new instance.
     */
    PaymentStatus(String name) {
        this.name = name;
    }

    /**
     * Return the name of the enum.
     */
    @Override
    public String toString() {
        return name;
    }

    /**
     * Find the PaymentStatus, using the given name. If not found, an empty {@link Optional} is returned.
     *
     * @param name The name of the PaymentStatus to find.
     * @return The PaymentStatus, wrapped in an {@link Optional}.
     */
    public static Optional<PaymentStatus> findByName(String name) {
        return Stream.of(PaymentStatus.values())
                .filter((type) -> type.toString().equalsIgnoreCase(name))
                .findFirst();
    }

}
