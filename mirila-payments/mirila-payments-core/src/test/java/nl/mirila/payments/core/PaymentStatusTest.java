package nl.mirila.payments.core;

import org.junit.jupiter.api.Test;

import static nl.mirila.payments.core.PaymentStatus.PAID;
import static org.assertj.core.api.Assertions.assertThat;

class PaymentStatusTest {

    @Test
    void testFindByNameWithValidName() {
        assertThat(PaymentStatus.findByName("paid")).contains(PAID);
    }

    @Test
    void testFindByNameWithInvalidName() {
        assertThat(PaymentStatus.findByName("invalid name")).isEmpty();
    }

    @Test
    void testFindByNameWithBlankString() {
        assertThat(PaymentStatus.findByName(" ")).isEmpty();
    }

    @Test
    void testFindByNameWithEmptyString() {
        assertThat(PaymentStatus.findByName("")).isEmpty();
    }

    @Test
    void testFindByNameWithNull() {
        assertThat(PaymentStatus.findByName(null)).isEmpty();
    }

}