package nl.mirila.payments.mollie;

import be.woutschoovaerts.mollie.Client;
import be.woutschoovaerts.mollie.data.common.Amount;
import be.woutschoovaerts.mollie.data.common.Locale;
import be.woutschoovaerts.mollie.data.payment.PaymentRequest;
import be.woutschoovaerts.mollie.data.payment.PaymentResponse;
import be.woutschoovaerts.mollie.exception.MollieException;
import com.google.inject.Inject;
import nl.mirila.core.exceptions.MirilaException;
import nl.mirila.drivers.mollie.MollieDriver;
import nl.mirila.payments.core.Payment;
import nl.mirila.payments.core.PaymentProvider;
import nl.mirila.payments.core.PaymentStatus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.Optional;

import static org.apache.commons.lang3.StringUtils.isBlank;

public class MolliePaymentProvider implements PaymentProvider {

    private static final Logger logger = LogManager.getLogger(MolliePaymentProvider.class);
    private final Client client;

    /**
     * Initialize a new instance.
     */
    @Inject
    public MolliePaymentProvider(MollieDriver driver) {
        client = driver.getClient();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String createPaymentRedirect(Payment payment) {
        PaymentRequest request = convertPaymentToMolliePaymentRequest(payment);
        try {
            PaymentResponse response = client.payments().createPayment(request);
            payment.setStatus(PaymentStatus.PENDING);
            payment.setPaymentId(response.getId());
            logger.info("Created a Mollie payment with id {}.", response.getId());
            return response.getLinks().getCheckout().getHref();
        } catch (MollieException e) {
            logger.error(e.getMessage());
            payment.setStatus(PaymentStatus.FAILED);
            throw new MirilaException(e.getMessage());
        }
    }

    /**
     * Converts the given {@link Payment} into a {@link PaymentRequest} suitable for the Mollie client.
     *
     * @param payment The payment to convert.
     * @return The payment request.
     */
    protected PaymentRequest convertPaymentToMolliePaymentRequest(Payment payment) {
        Amount amount = Amount.builder()
                .value(payment.getAmount())
                .currency(payment.getCurrency().getCurrencyCode())
                .build();

        Locale locale = null;
        try {
            locale = Locale.valueOf(payment.getLocale().toString());
        } catch (IllegalArgumentException e) {
            logger.warn("Error while converting system locale to Mollie locale: {}", e.getMessage());
        }

        return PaymentRequest.builder()
                .amount(amount)
                .locale(Optional.ofNullable(locale))
                .redirectUrl(payment.getSuccessUrl().orElse(null))
                .description(payment.getDescription())
                .build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PaymentStatus checkStatus(Payment payment) {
        if (isBlank(payment.getPaymentId())) {
            logger.error("Can not check the payment, because the payment id is missing.");
            return PaymentStatus.FAILED;
        }
        try {
            PaymentResponse response = client.payments().getPayment(payment.getPaymentId());
            payment.setStatus(convertMollieStatusToPaymentStatus(response.getStatus()));
            return payment.getStatus();
        } catch (MollieException e) {
            logger.error(e.getMessage());
            return PaymentStatus.FAILED;
        }
    }

    /**
     * Return the {@link PaymentStatus} for the given Mollie status.
     *
     * @param status The Mollie status.
     * @return The internal payment status.
     */
    protected PaymentStatus convertMollieStatusToPaymentStatus(
            @NotNull be.woutschoovaerts.mollie.data.payment.PaymentStatus status
    ) {
        Objects.requireNonNull(status);
        return switch (status) {
            case CANCELED -> PaymentStatus.CANCELED;
            case EXPIRED, FAILED -> PaymentStatus.FAILED;
            case PAID -> PaymentStatus.PAID;
            default -> PaymentStatus.PENDING;
        };
    }

}
