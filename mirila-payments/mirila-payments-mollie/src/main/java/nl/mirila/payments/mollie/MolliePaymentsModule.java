package nl.mirila.payments.mollie;

import com.google.inject.AbstractModule;
import nl.mirila.payments.core.PaymentProvider;

public class MolliePaymentsModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(PaymentProvider.class).to(MolliePaymentProvider.class);
    }

}
