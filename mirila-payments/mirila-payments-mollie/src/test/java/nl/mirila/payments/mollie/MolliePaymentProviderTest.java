package nl.mirila.payments.mollie;

import be.woutschoovaerts.mollie.data.payment.PaymentRequest;
import be.woutschoovaerts.mollie.data.payment.PaymentStatus;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import nl.mirila.core.settings.modules.KeyValuesSettingsModule;
import nl.mirila.payments.core.Payment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.HashMap;
import java.util.Locale;

import static be.woutschoovaerts.mollie.data.common.Locale.nl_NL;
import static nl.mirila.payments.core.PaymentStatus.PAID;
import static nl.mirila.payments.core.PaymentStatus.PENDING;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

class MolliePaymentProviderTest {

    private MolliePaymentProvider provider;

    @BeforeEach
    void setUp() {
        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                install(new KeyValuesSettingsModule(new HashMap<>()));
            }
        });
        provider = injector.getInstance(MolliePaymentProvider.class);
    }

    @Test
    void testConvertPaymentToMolliePaymentRequest() {
        Payment payment = Payment.withAmount("12.34")
                .withCurrency(Currency.getInstance("EUR"))
                .withDescription("This is a payment description.")
                .withSuccessUrl("https://www.example.com/success")
                .withLocale(new Locale("nl", "NL"));

        PaymentRequest request = provider.convertPaymentToMolliePaymentRequest(payment);

        assertThat(request.getAmount().getValue()).isEqualTo(new BigDecimal("12.34"));
        assertThat(request.getAmount().getCurrency()).isEqualTo("EUR");
        assertThat(request.getDescription()).isEqualTo("This is a payment description.");
        assertThat(request.getRedirectUrl()).contains("https://www.example.com/success");
        assertThat(request.getLocale()).contains(nl_NL);
    }

    @Test
    void testConvertMollieStatusToPaymentStatusWithPaid() {
        assertThat(provider.convertMollieStatusToPaymentStatus(PaymentStatus.PAID))
                .isEqualTo(PAID);
    }

    @Test
    void testConvertMollieStatusToPaymentStatusWithAuthorized() {
        assertThat(provider.convertMollieStatusToPaymentStatus(PaymentStatus.AUTHORIZED))
                .isEqualTo(PENDING);
    }

    @Test
    void testConvertMollieStatusToPaymentStatusWithNull() {
        assertThatExceptionOfType(NullPointerException.class)
                .isThrownBy(() -> provider.convertMollieStatusToPaymentStatus(null));
    }

}