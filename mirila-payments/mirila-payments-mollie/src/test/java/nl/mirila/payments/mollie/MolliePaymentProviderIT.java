package nl.mirila.payments.mollie;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import nl.mirila.core.settings.modules.KeyValuesSettingsModule;
import nl.mirila.drivers.mollie.MollieSettings;
import nl.mirila.payments.core.Payment;
import nl.mirila.payments.core.PaymentStatus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

class MolliePaymentProviderIT {

    private static final Logger logger = LogManager.getLogger(MolliePaymentProviderIT.class);

    private MolliePaymentProvider provider;
    private Payment payment;

    @BeforeEach
    void setUp() {
        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                Map<String, String> settingsMap = new HashMap<>();
                settingsMap.put(MollieSettings.KEY_API_KEY, "test_EwhCgjNQ6zjct7tc2pcKFagwhbfcVC");
                install(new KeyValuesSettingsModule(settingsMap));
            }
        });
        provider = injector.getInstance(MolliePaymentProvider.class);
        payment = Payment.withAmount("12.34")
                .withDescription("This is a payment description.")
                .withSuccessUrl("https://www.example.com/success")
                .withLocale(new Locale("nl", "NL"));
    }

    /**
     * This test is disabled, to prevent excessive use of the external API of Mollie.
     */
    @Test
    @Disabled
    void testCreatePaymentProcess() {
        String redirectUrl = provider.createPaymentRedirect(payment);
        assertThat(redirectUrl).startsWith("https://www.mollie.com/checkout/select-method/");
        logger.info("Redirect url: {}", redirectUrl);
    }

    /**
     * This test is disabled, as it requires manual interaction. Use the first test to generate a redirect url.
     * After faking a successful payment, run this test.
     */
    @Test
    @Disabled
    void testPaymentStatus() {
        payment.setPaymentId("tr_TbDNwSLgMP");
        assertThat(provider.checkStatus(payment)).isEqualTo(PaymentStatus.PAID);
    }

}