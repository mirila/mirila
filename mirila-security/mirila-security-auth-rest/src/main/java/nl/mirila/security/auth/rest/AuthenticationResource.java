package nl.mirila.security.auth.rest;

import com.google.inject.Inject;
import nl.mirila.rest.annotations.Protected;
import nl.mirila.rest.annotations.Public;
import nl.mirila.rest.result.RestConstants;
import nl.mirila.security.auth.core.authorization.AuthorizationTokenProvider;
import nl.mirila.security.auth.core.authorization.Credentials;
import nl.mirila.security.auth.core.claims.Claims;
import nl.mirila.security.auth.core.contexts.ClaimsBasedSecurityContext;
import nl.mirila.security.auth.core.contexts.SecurityContext;
import nl.mirila.security.auth.core.exceptions.AbstractAuthenticationFailedException;

import javax.ws.rs.*;

/**
 * The resource that provides authentication.
 */
@Path("auth")
@Consumes(RestConstants.JSON_WITH_UTF8)
@Produces(RestConstants.JSON_WITH_UTF8)
public class AuthenticationResource {

    private final AuthorizationTokenProvider provider;
    private final SecurityContext securityContext;

    /**
     * Initialize a new instance.
     */
    @Inject
    public AuthenticationResource(AuthorizationTokenProvider provider, SecurityContext securityContext) {
        this.provider = provider;
        this.securityContext = securityContext;
    }

    @POST
    @Public
    public TokenResponse authenticate(Credentials credentials) {
        try {
            String token = provider.generateToken(credentials);
            return new TokenResponse(token);
        } catch (AbstractAuthenticationFailedException e) {
            throw new ForbiddenException(e.getMessage());
        }
    }

    @GET
    @Protected
    @Path("refresh")
    public TokenResponse refresh() {
        if (!(securityContext instanceof ClaimsBasedSecurityContext claimsContext)) {
            throw new BadRequestException("The security context is not claims based.");
        }
        Claims claims = claimsContext.getClaims();
        String token = provider.generateToken(claims);
        return new TokenResponse(token);
    }

    @GET
    @Public
    @Path("context")
    public SecurityContext getContext() {
        return securityContext;
    }

}
