package nl.mirila.security.auth.rest;

import com.fasterxml.jackson.annotation.JsonRootName;

/**
 * A response object that contains the generated token.
 */
@JsonRootName("")
public class TokenResponse {

    private final String token;

    /**
     * Initialize a new instance.
     */
    public TokenResponse(String token) {
        this.token = token;
    }

    /**
     * Return the token.
     */
    public String getToken() {
        return token;
    }

}
