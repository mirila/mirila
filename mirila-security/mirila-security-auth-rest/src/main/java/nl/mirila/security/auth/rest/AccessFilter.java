package nl.mirila.security.auth.rest;

import com.google.inject.Inject;
import com.google.inject.Injector;
import nl.mirila.core.exceptions.MirilaException;
import nl.mirila.rest.annotations.ClaimCheck;
import nl.mirila.rest.annotations.Protected;
import nl.mirila.rest.annotations.Public;
import nl.mirila.security.auth.core.claims.ClaimUtils;
import nl.mirila.security.auth.core.claims.Claims;
import nl.mirila.security.auth.core.contexts.ClaimsBasedSecurityContext;
import nl.mirila.security.auth.core.contexts.SecurityContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jboss.resteasy.core.interception.jaxrs.PostMatchContainerRequestContext;

import javax.annotation.Priority;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static javax.ws.rs.core.Response.Status.FORBIDDEN;
import static javax.ws.rs.core.Response.Status.UNAUTHORIZED;

/**
 * The AccessFilter checks whether the actor meets the accessibility requirements as specified in the resource method
 * that is being called.
 */
@Provider
@Priority(Priorities.AUTHORIZATION)
public class AccessFilter implements ContainerRequestFilter {

    private static final Logger logger = LogManager.getLogger(AccessFilter.class);

    private static final String ANNOTATION_MISSING = "Annotation @Public or @Protected is missing on endpoint {}.";
    private static final String ANNOTATION_CONFLICT = "Both @Public and @Protected are present on endpoint {}.";
    private static final String SECURITY_CONTEXT_CONFLICT =
            "Expected a ClaimsBasedSecurityContext for claim checking, but got a {}.";

    private static final String NOT_AUTHENTICATED = "You need to be authenticated to use this endpoint.";
    private static final String NO_RIGHTS = "You do not have sufficient rights to access this endpoint.";

    private final Injector injector;

    @Context
    private ResourceInfo resourceInfo;

    /**
     * Initialize a new instance.
     */
    @Inject
    public AccessFilter(Injector injector) {
        this.injector = injector;
    }

    /**
     * {@inheritDoc}
     * <p>
     * This filter checks for the presence of either the {@link Public} and {@link Protected} on the endpoint method.
     * If both annotations are present on the endpoint method, {@link Protected} will be used.
     */
    @Override
    public void filter(ContainerRequestContext requestContext) {
        Public publicAnnotation = resourceInfo.getResourceMethod().getAnnotation(Public.class);
        Protected protectedAnnotation = resourceInfo.getResourceMethod().getAnnotation(Protected.class);

        if ((protectedAnnotation != null) && (publicAnnotation != null)) {
            logger.warn(ANNOTATION_CONFLICT, requestContext.getUriInfo().getPath());
        } else if (publicAnnotation != null) {
            // Public endpoint, no other access checks required.
            return;
        } else if (protectedAnnotation == null) {
            logger.error(ANNOTATION_MISSING, requestContext.getUriInfo().getPath());
            throw new MirilaException("Missing endpoint annotations.");
        }

        SecurityContext securityContext = injector.getInstance(SecurityContext.class);
        if (!securityContext.isAuthenticated()) {
            String msg = "You need to be authenticated to use this endpoint.";
            throw new ClientErrorException(msg, UNAUTHORIZED);
        } else if (securityContext.isSuperUser()) {
            // Super users have access, no other checks required in this filter.
            return;
        }

        checkProtectedScope(requestContext, securityContext, protectedAnnotation);
    }

    /**
     * Check whether the {@link Protected} annotation may lead to blocking the request due to not having sufficient
     * rights. In those cases this method throws a {@link ClientErrorException}.
     * <p>
     * @param requestContext The request context.
     * @param securityContext The security context.
     * @param protectedAnnotation The Protected annotation.
     */
    private void checkProtectedScope(ContainerRequestContext requestContext,
                                     SecurityContext securityContext,
                                     Protected protectedAnnotation) {
        List<String> requiredRoles = Arrays.asList(protectedAnnotation.roles());
        List<ClaimCheck> claimChecks = Arrays.asList(protectedAnnotation.claimChecks());

        if (!requiredRoles.isEmpty()) {
            List<String> roles = securityContext.getRoles();
            boolean hasValidRole = roles.stream().anyMatch(requiredRoles::contains);
            if ((!hasValidRole) && (claimChecks.isEmpty())) {
                throw new ClientErrorException(NO_RIGHTS, FORBIDDEN);
            }
        }

        if (!claimChecks.isEmpty()) {
            if (!(securityContext instanceof ClaimsBasedSecurityContext claimsContext)) {
                logger.warn(SECURITY_CONTEXT_CONFLICT, securityContext.getClass().getSimpleName());
                throw new ClientErrorException(NO_RIGHTS, FORBIDDEN);
            }
            Claims claims = claimsContext.getClaims();
            claimChecks.forEach((claimCheck) -> {
                String value = claimCheck.claimValue();
                if (value != null) {
                    String paramName = claimCheck.queryParamName();
                    List<String> parameter = ((PostMatchContainerRequestContext) requestContext).getHttpRequest()
                            .getUri()
                            .getPathParameters()
                            .get(paramName);
                    if ((parameter != null) && (!parameter.isEmpty())) {
                        value = parameter.get(0);
                    }
                }
                Optional<String> optional = ClaimUtils.getClaimByKey(claims, claimCheck.claimKey());
                if ((optional.isEmpty()) || (!optional.get().equals(value))) {
                    throw new ClientErrorException(NO_RIGHTS, FORBIDDEN);
                }
            });
        }
    }

}
