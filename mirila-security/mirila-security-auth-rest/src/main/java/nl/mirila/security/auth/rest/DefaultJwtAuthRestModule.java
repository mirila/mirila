package nl.mirila.security.auth.rest;

import com.auth0.jwt.algorithms.Algorithm;
import com.google.inject.AbstractModule;
import nl.mirila.security.auth.core.SecurityCoreModule;
import nl.mirila.security.auth.core.authorization.AuthorizationTokenProvider;
import nl.mirila.security.auth.core.contexts.SecurityContextFilter;
import nl.mirila.security.auth.jwt.HMAC512AlgorithmProvider;
import nl.mirila.security.auth.jwt.JwtProvider;

public class DefaultJwtAuthRestModule extends AbstractModule {

    @SuppressWarnings("PointlessBinding") // Some bindings need to be added, to be seen during reflection.
    @Override
    protected void configure() {
        install(new SecurityCoreModule());

        bind(Algorithm.class).toProvider(HMAC512AlgorithmProvider.class);
        bind(AuthorizationTokenProvider.class).to(JwtProvider.class);
        bind(AuthenticationResource.class);
        bind(SecurityContextFilter.class);
        bind(AccessFilter.class);
    }

}
