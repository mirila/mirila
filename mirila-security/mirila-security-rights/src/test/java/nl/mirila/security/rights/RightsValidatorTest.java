package nl.mirila.security.rights;

import nl.mirila.core.datatype.Id;
import nl.mirila.core.settings.ApplicationSettings;
import nl.mirila.core.settings.Settings;
import nl.mirila.security.auth.core.contexts.SecurityContextProvider;
import nl.mirila.security.rights.objects.RightsActor;
import nl.mirila.security.rights.objects.RightsObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static nl.mirila.core.settings.ApplicationSettings.KEY_REALM;
import static nl.mirila.security.auth.core.authorization.Roles.ADMIN;
import static nl.mirila.security.auth.core.authorization.Roles.SUPER_USER;
import static nl.mirila.security.rights.objects.RightsMasks.*;
import static org.assertj.core.api.Assertions.assertThat;

class RightsValidatorTest {

    private RightsValidator validator;

    @BeforeEach
    void setUp() {
        Map<String, String> settingsMap = new HashMap<>();
        settingsMap.put(KEY_REALM, "test-realm");
        settingsMap.put("rights.all.may-revoke-access", "true");
        settingsMap.put("rights.guests.may-update-something", "true");
        settingsMap.put("rights.users.may-grant-access", "true");
        settingsMap.put("rights.groups.soccer-players.may-update-anything", "true");
        settingsMap.put("rights.groups.baseball-players.may-update-anything", "true");
        Settings settings = Settings.withMap(settingsMap);
        ApplicationSettings applicationSettings = new ApplicationSettings(settings);

        SecurityContextProvider securityContextProvider = new SecurityContextProvider(applicationSettings);
        CurrentRightsActorProvider rightsActorProvider = new CurrentRightsActorProvider(securityContextProvider);
        validator = new RightsValidator(settings, rightsActorProvider);
    }

    @Test
    void testGuest() {
        RightsActor guest = new RightsActor(null, null, null);

        RightsObject object = new RightsObject(null, null, RIGHT_GUEST_CREATE_MASK);
        assertThat(validator.hasRight(guest, object, ObjectRight.CREATE)).isTrue();
        assertThat(validator.hasRight(guest, object, ObjectRight.READ)).isFalse();
        assertThat(validator.hasRight(guest, object, ObjectRight.UPDATE)).isFalse();
        assertThat(validator.hasRight(guest, object, ObjectRight.DELETE)).isFalse();

        object = new RightsObject(null, null, RIGHT_GUEST_READ_MASK);
        assertThat(validator.hasRight(guest, object, ObjectRight.CREATE)).isFalse();
        assertThat(validator.hasRight(guest, object, ObjectRight.READ)).isTrue();
        assertThat(validator.hasRight(guest, object, ObjectRight.UPDATE)).isFalse();
        assertThat(validator.hasRight(guest, object, ObjectRight.DELETE)).isFalse();

        object = new RightsObject(null, null, RIGHT_GUEST_UPDATE_MASK);
        assertThat(validator.hasRight(guest, object, ObjectRight.CREATE)).isFalse();
        assertThat(validator.hasRight(guest, object, ObjectRight.READ)).isFalse();
        assertThat(validator.hasRight(guest, object, ObjectRight.UPDATE)).isTrue();
        assertThat(validator.hasRight(guest, object, ObjectRight.DELETE)).isFalse();

        object = new RightsObject(null, null, RIGHT_GUEST_DELETE_MASK);
        assertThat(validator.hasRight(guest, object, ObjectRight.CREATE)).isFalse();
        assertThat(validator.hasRight(guest, object, ObjectRight.READ)).isFalse();
        assertThat(validator.hasRight(guest, object, ObjectRight.UPDATE)).isFalse();
        assertThat(validator.hasRight(guest, object, ObjectRight.DELETE)).isTrue();

        object = new RightsObject(null, null, RIGHT_GUEST_DELETE_MASK + RIGHT_GROUP_READ_MASK);
        assertThat(validator.hasRight(guest, object, ObjectRight.CREATE)).isFalse();
        assertThat(validator.hasRight(guest, object, ObjectRight.READ)).isFalse();
        assertThat(validator.hasRight(guest, object, ObjectRight.UPDATE)).isFalse();
        assertThat(validator.hasRight(guest, object, ObjectRight.DELETE)).isTrue();
    }

    @Test
    void testOwner() {
        Id ownerId = Id.of("owner-id");
        RightsActor owner = new RightsActor(ownerId, null, null);

        RightsObject object = new RightsObject(Set.of(ownerId), null, RIGHT_OWNER_CREATE_MASK);
        assertThat(validator.hasRight(owner, object, ObjectRight.CREATE)).isTrue();
        assertThat(validator.hasRight(owner, object, ObjectRight.READ)).isFalse();
        assertThat(validator.hasRight(owner, object, ObjectRight.UPDATE)).isFalse();
        assertThat(validator.hasRight(owner, object, ObjectRight.DELETE)).isFalse();

        object = new RightsObject(Set.of(ownerId), null, RIGHT_OWNER_READ_MASK);
        assertThat(validator.hasRight(owner, object, ObjectRight.CREATE)).isFalse();
        assertThat(validator.hasRight(owner, object, ObjectRight.READ)).isTrue();
        assertThat(validator.hasRight(owner, object, ObjectRight.UPDATE)).isFalse();
        assertThat(validator.hasRight(owner, object, ObjectRight.DELETE)).isFalse();

        object = new RightsObject(Set.of(ownerId), null, RIGHT_OWNER_UPDATE_MASK);
        assertThat(validator.hasRight(owner, object, ObjectRight.CREATE)).isFalse();
        assertThat(validator.hasRight(owner, object, ObjectRight.READ)).isFalse();
        assertThat(validator.hasRight(owner, object, ObjectRight.UPDATE)).isTrue();
        assertThat(validator.hasRight(owner, object, ObjectRight.DELETE)).isFalse();

        object = new RightsObject(Set.of(ownerId), null, RIGHT_OWNER_DELETE_MASK);
        assertThat(validator.hasRight(owner, object, ObjectRight.CREATE)).isFalse();
        assertThat(validator.hasRight(owner, object, ObjectRight.READ)).isFalse();
        assertThat(validator.hasRight(owner, object, ObjectRight.UPDATE)).isFalse();
        assertThat(validator.hasRight(owner, object, ObjectRight.DELETE)).isTrue();

        object = new RightsObject(Set.of(ownerId), null, RIGHT_OWNER_ALL_MASK);
        assertThat(validator.hasRight(owner, object, ObjectRight.CREATE)).isTrue();
        assertThat(validator.hasRight(owner, object, ObjectRight.READ)).isTrue();
        assertThat(validator.hasRight(owner, object, ObjectRight.UPDATE)).isTrue();
        assertThat(validator.hasRight(owner, object, ObjectRight.DELETE)).isTrue();


        RightsActor notOwner = new RightsActor(Id.of("not-" + ownerId), null, null);
        object = new RightsObject(Set.of(ownerId), null, RIGHT_OWNER_ALL_MASK);
        assertThat(validator.hasRight(notOwner, object, ObjectRight.CREATE)).isFalse();
        assertThat(validator.hasRight(notOwner, object, ObjectRight.READ)).isFalse();
        assertThat(validator.hasRight(notOwner, object, ObjectRight.UPDATE)).isFalse();
        assertThat(validator.hasRight(notOwner, object, ObjectRight.DELETE)).isFalse();
    }

    @Test
    void testGroup() {
        RightsActor groupMember = new RightsActor(Id.of("user-1"), null, Set.of(Id.of("group-1")));

        Set<Id> groupIds = Set.of(Id.of("group-0"), Id.of("group-1"), Id.of("group-2"));
        RightsObject object = new RightsObject(null, groupIds, RIGHT_GROUP_CREATE_MASK);
        assertThat(validator.hasRight(groupMember, object, ObjectRight.CREATE)).isTrue();
        assertThat(validator.hasRight(groupMember, object, ObjectRight.READ)).isFalse();
        assertThat(validator.hasRight(groupMember, object, ObjectRight.UPDATE)).isFalse();
        assertThat(validator.hasRight(groupMember, object, ObjectRight.DELETE)).isFalse();

        object = new RightsObject(null, groupIds, RIGHT_GROUP_READ_MASK);
        assertThat(validator.hasRight(groupMember, object, ObjectRight.CREATE)).isFalse();
        assertThat(validator.hasRight(groupMember, object, ObjectRight.READ)).isTrue();
        assertThat(validator.hasRight(groupMember, object, ObjectRight.UPDATE)).isFalse();
        assertThat(validator.hasRight(groupMember, object, ObjectRight.DELETE)).isFalse();

        object = new RightsObject(null, groupIds, RIGHT_GROUP_UPDATE_MASK);
        assertThat(validator.hasRight(groupMember, object, ObjectRight.CREATE)).isFalse();
        assertThat(validator.hasRight(groupMember, object, ObjectRight.READ)).isFalse();
        assertThat(validator.hasRight(groupMember, object, ObjectRight.UPDATE)).isTrue();
        assertThat(validator.hasRight(groupMember, object, ObjectRight.DELETE)).isFalse();

        object = new RightsObject(null, groupIds, RIGHT_GROUP_DELETE_MASK);
        assertThat(validator.hasRight(groupMember, object, ObjectRight.CREATE)).isFalse();
        assertThat(validator.hasRight(groupMember, object, ObjectRight.READ)).isFalse();
        assertThat(validator.hasRight(groupMember, object, ObjectRight.UPDATE)).isFalse();
        assertThat(validator.hasRight(groupMember, object, ObjectRight.DELETE)).isTrue();

        object = new RightsObject(null, groupIds, RIGHT_GROUP_DELETE_MASK + RIGHT_GROUP_READ_MASK);
        assertThat(validator.hasRight(groupMember, object, ObjectRight.CREATE)).isFalse();
        assertThat(validator.hasRight(groupMember, object, ObjectRight.READ)).isTrue();
        assertThat(validator.hasRight(groupMember, object, ObjectRight.UPDATE)).isFalse();
        assertThat(validator.hasRight(groupMember, object, ObjectRight.DELETE)).isTrue();
    }


    @Test
    void testSuperUser() {
        RightsActor superUser = new RightsActor(Id.of("user-1"), Set.of(SUPER_USER), null);

        Set<Id> groupIds = Set.of(Id.of("group-0"), Id.of("group-1"), Id.of("group-2"));
        RightsObject object = new RightsObject(null, groupIds, RIGHT_GUEST_CREATE_MASK);
        assertThat(validator.hasRight(superUser, object, ObjectRight.CREATE)).isTrue();
        assertThat(validator.hasRight(superUser, object, ObjectRight.READ)).isTrue();
        assertThat(validator.hasRight(superUser, object, ObjectRight.UPDATE)).isTrue();
        assertThat(validator.hasRight(superUser, object, ObjectRight.DELETE)).isTrue();

        object = new RightsObject(null, groupIds, RIGHT_GUEST_READ_MASK);
        assertThat(validator.hasRight(superUser, object, ObjectRight.CREATE)).isTrue();
        assertThat(validator.hasRight(superUser, object, ObjectRight.READ)).isTrue();
        assertThat(validator.hasRight(superUser, object, ObjectRight.UPDATE)).isTrue();
        assertThat(validator.hasRight(superUser, object, ObjectRight.DELETE)).isTrue();

        object = new RightsObject(null, groupIds, RIGHT_GUEST_UPDATE_MASK);
        assertThat(validator.hasRight(superUser, object, ObjectRight.CREATE)).isTrue();
        assertThat(validator.hasRight(superUser, object, ObjectRight.READ)).isTrue();
        assertThat(validator.hasRight(superUser, object, ObjectRight.UPDATE)).isTrue();
        assertThat(validator.hasRight(superUser, object, ObjectRight.DELETE)).isTrue();

        object = new RightsObject(null, groupIds, RIGHT_GUEST_DELETE_MASK);
        assertThat(validator.hasRight(superUser, object, ObjectRight.CREATE)).isTrue();
        assertThat(validator.hasRight(superUser, object, ObjectRight.READ)).isTrue();
        assertThat(validator.hasRight(superUser, object, ObjectRight.UPDATE)).isTrue();
        assertThat(validator.hasRight(superUser, object, ObjectRight.DELETE)).isTrue();

        object = new RightsObject(null, groupIds, RIGHT_GUEST_DELETE_MASK + RIGHT_GROUP_READ_MASK);
        assertThat(validator.hasRight(superUser, object, ObjectRight.CREATE)).isTrue();
        assertThat(validator.hasRight(superUser, object, ObjectRight.READ)).isTrue();
        assertThat(validator.hasRight(superUser, object, ObjectRight.UPDATE)).isTrue();
        assertThat(validator.hasRight(superUser, object, ObjectRight.DELETE)).isTrue();
    }

    @Test
    void testAdministrator() {
        RightsActor administrator = new RightsActor(Id.of("user-1"), Set.of(ADMIN), null);

        Set<Id> groupIds = Set.of(Id.of("group-0"), Id.of("group-1"), Id.of("group-2"));
        RightsObject object = new RightsObject(null, groupIds, RIGHT_GUEST_CREATE_MASK);
        assertThat(validator.hasRight(administrator, object, ObjectRight.CREATE)).isTrue();
        assertThat(validator.hasRight(administrator, object, ObjectRight.READ)).isTrue();
        assertThat(validator.hasRight(administrator, object, ObjectRight.UPDATE)).isTrue();
        assertThat(validator.hasRight(administrator, object, ObjectRight.DELETE)).isTrue();

        object = new RightsObject(null, groupIds, RIGHT_GUEST_READ_MASK);
        assertThat(validator.hasRight(administrator, object, ObjectRight.CREATE)).isTrue();
        assertThat(validator.hasRight(administrator, object, ObjectRight.READ)).isTrue();
        assertThat(validator.hasRight(administrator, object, ObjectRight.UPDATE)).isTrue();
        assertThat(validator.hasRight(administrator, object, ObjectRight.DELETE)).isTrue();

        object = new RightsObject(null, groupIds, RIGHT_GUEST_UPDATE_MASK);
        assertThat(validator.hasRight(administrator, object, ObjectRight.CREATE)).isTrue();
        assertThat(validator.hasRight(administrator, object, ObjectRight.READ)).isTrue();
        assertThat(validator.hasRight(administrator, object, ObjectRight.UPDATE)).isTrue();
        assertThat(validator.hasRight(administrator, object, ObjectRight.DELETE)).isTrue();

        object = new RightsObject(null, groupIds, RIGHT_GUEST_DELETE_MASK);
        assertThat(validator.hasRight(administrator, object, ObjectRight.CREATE)).isTrue();
        assertThat(validator.hasRight(administrator, object, ObjectRight.READ)).isTrue();
        assertThat(validator.hasRight(administrator, object, ObjectRight.UPDATE)).isTrue();
        assertThat(validator.hasRight(administrator, object, ObjectRight.DELETE)).isTrue();

        object = new RightsObject(null, groupIds, RIGHT_GUEST_DELETE_MASK + RIGHT_GROUP_READ_MASK);
        assertThat(validator.hasRight(administrator, object, ObjectRight.CREATE)).isTrue();
        assertThat(validator.hasRight(administrator, object, ObjectRight.READ)).isTrue();
        assertThat(validator.hasRight(administrator, object, ObjectRight.UPDATE)).isTrue();
        assertThat(validator.hasRight(administrator, object, ObjectRight.DELETE)).isTrue();
    }

    @Test
    void testHasRightBySetting() {
        RightsActor guest = new RightsActor(null, null, null);
        assertThat(validator.hasRight(guest, "may-update-something")).isTrue();
        assertThat(validator.hasRight(guest, "may-revoke-access")).isTrue();
        assertThat(validator.hasRight(guest, "may-grant-access")).isFalse();
        assertThat(validator.hasRight(guest, "may-update-anything")).isFalse();
        assertThat(validator.hasRight(guest, "may-update-nothing")).isFalse();

        RightsActor user = new RightsActor(Id.of("user-1"), null, null);
        assertThat(validator.hasRight(user, "may-update-something")).isFalse();
        assertThat(validator.hasRight(user, "may-grant-access")).isTrue();
        assertThat(validator.hasRight(user, "may-revoke-access")).isTrue();
        assertThat(validator.hasRight(user, "may-update-anything")).isFalse();
        assertThat(validator.hasRight(user, "may-update-nothing")).isFalse();

        RightsActor member = new RightsActor(Id.of("user-1"), null, Set.of(Id.of("soccer-players")));
        assertThat(validator.hasRight(member, "may-update-something")).isFalse();
        assertThat(validator.hasRight(member, "may-grant-access")).isTrue();
        assertThat(validator.hasRight(member, "may-revoke-access")).isTrue();
        assertThat(validator.hasRight(member, "may-update-anything")).isTrue();
        assertThat(validator.hasRight(member, "may-update-nothing")).isFalse();

        RightsActor superUser = new RightsActor(Id.of("user-1"), Set.of(SUPER_USER), null);
        assertThat(validator.hasRight(superUser, "may-update-something")).isTrue();
        assertThat(validator.hasRight(superUser, "may-grant-access")).isTrue();
        assertThat(validator.hasRight(superUser, "may-revoke-access")).isTrue();
        assertThat(validator.hasRight(superUser, "may-update-anything")).isTrue();
        assertThat(validator.hasRight(superUser, "may-update-nothing")).isTrue();

        RightsActor admin = new RightsActor(Id.of("user-1"), Set.of(ADMIN), null);
        assertThat(validator.hasRight(admin, "may-update-something")).isTrue();
        assertThat(validator.hasRight(admin, "may-grant-access")).isTrue();
        assertThat(validator.hasRight(admin, "may-revoke-access")).isTrue();
        assertThat(validator.hasRight(admin, "may-update-anything")).isTrue();
        assertThat(validator.hasRight(admin, "may-update-nothing")).isTrue();
    }

    @Test
    void testHasRightByObjectOrSetting() {
        Id ownerId = Id.of("owner-id");
        RightsActor owner = new RightsActor(ownerId, null, null);
        RightsObject object = new RightsObject(Set.of(ownerId), null, RIGHT_OWNER_CREATE_MASK);

        // Has right by both object and settings
        assertThat(validator.hasRight(owner, object, ObjectRight.CREATE, "may-grant-access")).isTrue();

        // Has right by object, but not by settings
        assertThat(validator.hasRight(owner, object, ObjectRight.READ, "may-grant-access")).isTrue();

        // Has no right by object, nor by settings
        assertThat(validator.hasRight(owner, object, ObjectRight.READ, "may-do-nothing")).isFalse();

        // Has no right by object, by has it by settings
        assertThat(validator.hasRight(owner, object, ObjectRight.READ, "may-grant-access")).isTrue();
    }

}
