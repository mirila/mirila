package nl.mirila.security.rights;

import com.google.inject.Inject;
import nl.mirila.core.datatype.Id;
import nl.mirila.core.settings.Settings;
import nl.mirila.security.rights.objects.RightsActor;
import nl.mirila.security.rights.objects.RightsObject;

import java.util.Set;

/**
 * Validates the rights. There are two types of rights:
 * - model based rights.
 * - settings based rights.
 */
public class RightsValidator {

    public static final String SETTING_KEY_POSTFIX_CREATE = ".create";
    public static final String SETTING_KEY_POSTFIX_MAINTAIN = ".maintain";

    private final Settings settings;
    private final CurrentRightsActorProvider currentRightsActorProvider;

    /**
     * Initialize a new instance.
     */
    @Inject
    RightsValidator(Settings settings, CurrentRightsActorProvider currentRightsActorProvider) {
        this.settings = settings;
        this.currentRightsActorProvider = currentRightsActorProvider;
    }

    /**
     * Return true if the current actor has the specific right to the given object.
     * <p>
     * @param object The object for which the right is requested.
     * @param rightToApply The specific right that is requested.
     * @return True if the actor has the right to the object.
     */
    public boolean hasRight(RightsObject object, ObjectRight rightToApply) {
        return hasRight(currentRightsActorProvider.get(), object, rightToApply);
    }

    /**
     * Return true if the given actor has the specific right to the given object.
     * <p>
     * @param actor The actor for whom the right is requested.
     * @param object The object for which the right is requested.
     * @param rightToApply The specific right that is requested.
     * @return True if the actor has the right to the object.
     */
    public boolean hasRight(RightsActor actor, RightsObject object, ObjectRight rightToApply) {
        if (actor.getActorId().isEmpty()) {
            int mask = rightToApply.getRightCode();
            return ((object.getRights() & mask) > 0);
        }

        if (actor.isSuperUser()) {
            // Super users have all rights.
            return true;
        }

        if (object.getOwnerIds().contains(actor.getActorId())) {
            // The actor is the owner of the object.
            int mask = rightToApply.getRightCode() << 12;
            if ((object.getRights() & mask) > 0) {
                return true;
            }
        }

        boolean hasSameGroupId = actor.getGroupIds().stream().anyMatch(object.getGroupIds()::contains);
        if (hasSameGroupId) {
            // The actor is a group member of a group that has rights on this object.
            int mask = rightToApply.getRightCode() << 8;
            if ((object.getRights() & mask) > 0) {
                return true;
            }
        }

        // The actor, that isn't a guest, owner or group member, is a user.
        int mask = rightToApply.getRightCode() << 4;
        return ((object.getRights() & mask) > 0);
    }

    /**
     * Return true if the current actor has a specific right according to the settings.
     * <p>
     * @param rightSettingKey The right key that is used in the settings.
     * <p>
     * @return True if the actor has the right according to the setting.
     */
    public boolean hasRight(String rightSettingKey) {
        return hasRight(currentRightsActorProvider.get(), Set.of(rightSettingKey));
    }

    /**
     * Return true if the current actor has at least one of the given setting rights.
     * <p>
     * @param rightSettingKeys The rights keys that is used in the settings.
     * <p>
     * @return True if the actor has the right according to the setting.
     */
    public boolean hasRight(Set<String> rightSettingKeys) {
        return hasRight(currentRightsActorProvider.get(), rightSettingKeys);
    }

    /**
     * Return true if the given actor has a specific setting right.
     * <p>
     * @param actor The actor for whom the right is requested.
     * @param rightSettingKey The right key that is used in the settings.
     * <p>
     * @return True if the actor has the right according to the setting.
     */
    public boolean hasRight(RightsActor actor, String rightSettingKey) {
        return hasRight(actor, Set.of(rightSettingKey));
    }

    /**
     * Return true if the given actor has at least one of the given setting rights.
     * <p>
     * @param actor The actor for whom the right is requested.
     * @param rightSettingKeys The rights keys that are used in the settings.
     * <p>
     * @return True if the actor has the right according to the setting.
     */
    public boolean hasRight(RightsActor actor, Set<String> rightSettingKeys) {
         if (actor.isSuperUser()) {
            // Super users have all object rights.
            return true;
        }
        
        for (String rightSettingKey : rightSettingKeys) {
            String settingsKey = getSettingKeyForAll(rightSettingKey);
            if (settings.getBoolean(settingsKey).orElse(false)) {
                return true;
            } else if (actor.getActorId().isEmpty()) {
                settingsKey = getSettingKeyForGuests(rightSettingKey);
                if (settings.getBoolean(settingsKey).orElse(false)) {
                    return true;
                }
            } else {
                settingsKey = getSettingKeyForUsers(rightSettingKey);
                if (settings.getBoolean(settingsKey).orElse(false)) {
                    return true;
                }
                boolean hasRightViaGroup = actor.getGroupIds().stream()
                        .map((groupId) -> getSettingKeyForGroupMembers(rightSettingKey, groupId))
                        .anyMatch((groupKey) -> settings.getBoolean(groupKey).orElse(false));
                if (hasRightViaGroup) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Return true if the current actor either has the right on the given object, or a specific right according to the
     * settings.
     * <p>
     * @param object The object for which the right is requested.
     * @param rightToApply The specific right that is requested.
     * @param rightSettingKey The right key that is used in the settings.
     * @return True if the actor has the right according to the setting.
     */
    public boolean hasRight(RightsObject object, ObjectRight rightToApply, String rightSettingKey) {
        return hasRight(currentRightsActorProvider.get(), object, rightToApply, Set.of(rightSettingKey));
    }

    /**
     * Return true if the given actor either has the right on the given object, or at least one of the given right
     * setting keys.
     * <p>
     * @param actor The actor for whom the right is requested.
     * @param object The object for which the right is requested.
     * @param rightToApply The specific right that is requested.
     * @param rightSettingKey The right key that is used in the settings.
     * @return True if the actor has the right according to the setting.
     */
    public boolean hasRight(RightsActor actor, RightsObject object, ObjectRight rightToApply, String rightSettingKey) {
        return hasRight(actor, object, rightToApply, Set.of(rightSettingKey));
    }

    /**
     * Return true if the given actor either has the right on the given object, or a specific right according to the
     * settings.
     * <p>
     * @param object The object for which the right is requested.
     * @param rightToApply The specific right that is requested.
     * @param rightSettingKeys The right keys that are used in the settings.
     * @return True if the actor has the right according to the setting.
     */
    public boolean hasRight(RightsObject object, ObjectRight rightToApply, Set<String> rightSettingKeys) {
        return hasRight(currentRightsActorProvider.get(), object, rightToApply, rightSettingKeys);
    }

    /**
     * Return true if the given actor either has the right on the given object, or at least one of the given right
     * setting keys.
     * <p>
     * @param actor The actor for whom the right is requested.
     * @param object The object for which the right is requested.
     * @param rightToApply The specific right that is requested.
     * @param rightSettingKeys The right keys that are used in the settings.
     * @return True if the actor has the right according to the setting.
     */
    public boolean hasRight(RightsActor actor,
                            RightsObject object,
                            ObjectRight rightToApply,
                            Set<String> rightSettingKeys) {
        return (hasRight(actor, object, rightToApply)) || (hasRight(actor, rightSettingKeys));
    }

    /**
     * Return the setting key for the given rightSettingKey for anyone.
     */
    public static String getSettingKeyForAll(String rightSettingKey) {
        return "rights.all." + rightSettingKey;
    }

    /**
     * Return the setting key for the given rightSettingKey for guests.
     */
    public static String getSettingKeyForGuests(String rightSettingKey) {
        return "rights.guests." + rightSettingKey;
    }

    /**
     * Return the setting key for the given rightSettingKey for users.
     */
    public static String getSettingKeyForUsers(String rightSettingKey) {
        return "rights.users." + rightSettingKey;
    }

    /**
     * Return the setting key for the given rightSettingKey for group members.
     */
    public static String getSettingKeyForGroupMembers(String rightSettingKey, Id groupId) {
        return "rights.groups." + groupId.asString() + "." + rightSettingKey;
    }


}
