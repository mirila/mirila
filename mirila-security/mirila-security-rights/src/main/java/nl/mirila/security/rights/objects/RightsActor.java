package nl.mirila.security.rights.objects;

import nl.mirila.core.datatype.Id;
import nl.mirila.security.auth.core.authorization.Roles;
import nl.mirila.security.auth.core.contexts.SecurityContext;

import javax.annotation.Nonnull;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * A simple actor representation that is required for rights validation.
 */
public class RightsActor {

    private final Id actorId;
    private final Set<String> roles;
    private final Set<Id> groupIds;

    /**
     * Initialize an instance
     */
    public RightsActor(Id actorId, Set<String> roles, Set<Id> groupIds) {
        this.actorId = (actorId == null) ? Id.empty() : actorId;
        this.roles = (roles == null) ? new HashSet<>() : new HashSet<>(roles);
        this.groupIds = (groupIds == null) ? new HashSet<>() : new HashSet<>(groupIds);
    }

    /**
     * Return the actor id.
     */
    @Nonnull
    public Id getActorId() {
        return actorId;
    }

    /**
     * Return a list of roles.
     */
    @Nonnull
    public Set<String> getRoles() {
        return roles;
    }

    /**
     * Return a list of group ids.
     */
    @Nonnull
    public Set<Id> getGroupIds() {
        return groupIds;
    }

    /**
     * Return true if this actor is member of the group with the given id.
     * <p>
     * @param groupId The id of the group.
     * @return True if this actor is a group member.
     */
    public boolean isGroupMember(Id groupId) {
        return groupIds.contains(groupId);
    }

    /**
     * Return true if this actor has the given role.
     * <p>
     * @param role The role.
     * @return True if this actor has the role.
     */
    public boolean hasRole(String role) {
        return roles.contains(role);
    }

    /**
     * Return true if the actor is an administrator.
     */
    public boolean isAdministrator() {
        return hasRole(Roles.ADMIN);
    }

    /**
     * Return true if the actor is a super user.
     * <p>
     * NB: Administrators are also regarded as super users.
     */
    public boolean isSuperUser() {
        return roles.stream().anyMatch(Arrays.asList(Roles.ADMIN, Roles.SUPER_USER)::contains);
    }

    /**
     * Return the {@link RightsActor} that can be used for guests.
     */
    public static RightsActor forGuest() {
        return new RightsActor(null, null, null);
    }

    /**
     * Create an instance of a {@link RightsActor}, based on the given {@link SecurityContext}.
     */
    public static RightsActor fromSecurityContext(SecurityContext securityContext) {
        Set<Id> groupIds = securityContext.getGroupIds().stream()
                .map(Id::of)
                .collect(Collectors.toSet());
        return new RightsActor(securityContext.getCredentialId(),
                               new HashSet<>(securityContext.getRoles()),
                               groupIds);
    }

}
