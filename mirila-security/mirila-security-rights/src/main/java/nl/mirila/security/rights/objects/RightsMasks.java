package nl.mirila.security.rights.objects;

/**
 * Commonly used rights mask constants.
 */
public final class RightsMasks {

    public static final int RIGHT_OWNER_CREATE_MASK = 0x8000;
    public static final int RIGHT_GROUP_CREATE_MASK = 0x0800;
    public static final int RIGHT_USER_CREATE_MASK = 0x0080;
    public static final int RIGHT_GUEST_CREATE_MASK = 0x0008;

    public static final int RIGHT_OWNER_READ_MASK = 0x4000;
    public static final int RIGHT_GROUP_READ_MASK = 0x0400;
    public static final int RIGHT_USER_READ_MASK = 0x0040;
    public static final int RIGHT_GUEST_READ_MASK = 0x0004;

    public static final int RIGHT_OWNER_UPDATE_MASK = 0x2000;
    public static final int RIGHT_GROUP_UPDATE_MASK = 0x0200;
    public static final int RIGHT_USER_UPDATE_MASK = 0x0020;
    public static final int RIGHT_GUEST_UPDATE_MASK = 0x0002;

    public static final int RIGHT_OWNER_DELETE_MASK = 0x1000;
    public static final int RIGHT_GROUP_DELETE_MASK = 0x0100;
    public static final int RIGHT_USER_DELETE_MASK = 0x0010;
    public static final int RIGHT_GUEST_DELETE_MASK = 0x0001;

    public static final int RIGHT_ALL_CREATE_MASK = 0x8888;
    public static final int RIGHT_ALL_READ_MASK = 0x4444;
    public static final int RIGHT_ALL_UPDATE_MASK = 0x2222;
    public static final int RIGHT_ALL_DELETE_MASK = 0x1111;

    public static final int RIGHT_OWNER_ALL_MASK = 0xF000;
    public static final int RIGHT_GROUP_ALL_MASK = 0x0F00;
    public static final int RIGHT_USER_ALL_MASK = 0x00F0;
    public static final int RIGHT_GUEST_ALL_MASK = 0x000F;

    public static final int SENSIBLE_DEFAULT = 0xFF44;

    /**
     * This utility class may not be instantiated.
     */
    private RightsMasks() {
        throw new UnsupportedOperationException();
    }

}
