package nl.mirila.security.rights;

/**
 * The right that can be applied to an object.
 */
public enum ObjectRight {

    CREATE(8),
    READ(4),
    UPDATE(2),
    DELETE(1);

    private final int rightCode;

    /**
     * Initialize a new instance.
     */
    ObjectRight(int rightCode) {
        this.rightCode = rightCode;
    }

    /**
     * Return the right code.
     */
    public int getRightCode() {
        return rightCode;
    }

}
