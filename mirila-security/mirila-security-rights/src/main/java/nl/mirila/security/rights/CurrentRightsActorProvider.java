package nl.mirila.security.rights;

import com.google.inject.Inject;
import nl.mirila.security.auth.core.contexts.SecurityContextProvider;
import nl.mirila.security.rights.objects.RightsActor;
import org.jboss.resteasy.plugins.guice.RequestScoped;

import javax.inject.Provider;

/**
 * A provider that provides the {@link RightsActor} of the current actor.
 */
@RequestScoped
public class CurrentRightsActorProvider implements Provider<RightsActor> {

    private final SecurityContextProvider provider;
    private RightsActor rightsActor;

    /**
     * Initialize a new instance.
     */
    @Inject
    public CurrentRightsActorProvider(SecurityContextProvider provider) {
        this.provider = provider;
        rightsActor = null;
    }

    /**
     * Return the current {@link RightsActor}.
     */
    @Override
    public RightsActor get() {
        if (rightsActor == null) {
            rightsActor = RightsActor.fromSecurityContext(provider.get());
        }
        return rightsActor;
    }

}
