package nl.mirila.security.rights.objects;

import nl.mirila.core.datatype.Id;

import java.util.HashSet;
import java.util.Set;

/**
 * An object that contains the fields that are required for the rights system.
 */
public class RightsObject {

    private final Set<Id> ownerIds;
    private final Set<Id> groupIds;
    private final Integer rights;

    /**
     * Initialize a new instance.
     */
    public RightsObject(Set<Id> ownerIds, Set<Id> groupIds, Integer rights) {
        this.ownerIds = (ownerIds == null) ? new HashSet<>() : new HashSet<>(ownerIds);
        this.groupIds = (groupIds == null) ? new HashSet<>() : new HashSet<>(groupIds);
        this.rights = (rights == null) ? 0 : rights;
    }

    /**
     * Return the owner ids.
     */
    public Set<Id> getOwnerIds() {
        return ownerIds;
    }

    /**
     * Return the group ids.
     */
    public Set<Id> getGroupIds() {
        return groupIds;
    }

    /**
     * Return the rights code.
     */
    public Integer getRights() {
        return rights;
    }

}
