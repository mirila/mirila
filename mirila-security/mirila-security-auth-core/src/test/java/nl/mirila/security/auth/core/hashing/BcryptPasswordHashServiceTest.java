package nl.mirila.security.auth.core.hashing;

import nl.mirila.core.settings.Settings;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class BcryptPasswordHashServiceTest {


    private BcryptPasswordHashService service;

    @BeforeEach
    void setUp() {
        Settings settings = Settings.withMap(new HashMap<>());
        service = new BcryptPasswordHashService(new BcryptSettings(settings));
    }

    @Test
    void testService() {
        String validPassword = "This is my passphrase";
        String validHash = service.hashPassword(validPassword);

        assertThat(service.verifyPassword(validPassword, validHash)).isTrue();
        assertThat(service.verifyPassword("invalidPassword", validHash)).isFalse();
        assertThat(service.verifyPassword("", validHash)).isFalse();
        assertThat(service.verifyPassword(null, validHash)).isFalse();
        assertThat(service.verifyPassword(validPassword, "")).isFalse();
        assertThat(service.verifyPassword(validPassword, null)).isFalse();
    }

}
