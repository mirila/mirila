package nl.mirila.security.auth.core.claims;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

class ClaimsTest {

    @Test
    void testSetIssuer() {
        Claims claims = new Claims();
        claims.setIssuer("test-app");
        assertThat(claims.getIssuer()).isEqualTo("test-app");
    }

    @Test
    void testSetSubject() {
        Claims claims = new Claims();
        claims.setSubject("my-subject");
        assertThat(claims.getSubject()).isEqualTo("my-subject");
    }

    @Test
    void testGetAudience() {
        Claims claims = new Claims();
        Set<String> audience = claims.getAudience();
        audience.addAll(Arrays.asList("aud1", "aud2", "aud2")); // Deliberate duplicate.

        assertThat(audience).hasSize(2).containsOnly("aud1", "aud2");
    }

    @Test
    void testSetExpirationTime() {
        Claims claims = new Claims();
        claims.setExpirationTime(158539788L);
        assertThat(claims.getExpirationTime()).isEqualTo(158539788L);
    }

    @Test
    void testSetSubjectName() {
        Claims claims = new Claims();
        claims.setSubjectName("my-subject-name");
        assertThat(claims.getSubjectName()).isEqualTo("my-subject-name");
    }

    @Test
    void testGetGroupIds() {
        Claims claims = new Claims();
        Set<String> groupIds = claims.getGroupIds();
        groupIds.addAll(Arrays.asList("group1", "group2", "group2")); // Deliberate duplicate.

        assertThat(groupIds).hasSize(2).containsOnly("group1", "group2");
    }

}
