package nl.mirila.security.auth.core.authorization;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static nl.mirila.security.auth.core.authorization.SecretType.GPG;
import static nl.mirila.security.auth.core.authorization.SecretType.PASSWORD;
import static org.assertj.core.api.Assertions.assertThat;

class CredentialsTest {

    private ObjectMapper mapper;

    @BeforeEach
    void setUp() {
        mapper = new ObjectMapper();
    }

    @Test
    void test1() throws IOException {
        String json = "{\"realm\":\"test\"," +
                "\"identity\":\"mirila-user-id\"," +
                "\"secret\":\"this-is-my-secret\"}";

        Credentials credentials = mapper.readValue(json, Credentials.class);

        assertThat(credentials.getRealm()).isEqualTo("test");
        assertThat(credentials.getIdentity()).isEqualTo("mirila-user-id");
        assertThat(credentials.getSecret()).isEqualTo("this-is-my-secret");
        assertThat(credentials.getSecretType()).isEqualTo(PASSWORD);

        String expectedJson = "{\"realm\":\"test\"," +
                "\"identity\":\"mirila-user-id\"," +
                "\"secret\":\"this-is-my-secret\"," +
                "\"secretType\":\"password\"}";

        String jsonClone = mapper.writeValueAsString(credentials);
        assertThat(jsonClone).isEqualTo(expectedJson);
    }


    @Test
    void test2() throws IOException {
        String json = "{\"realm\":\"test\"" +
                ",\"identity\":\"mirila-user-id\"" +
                ",\"secret\":\"this-is-my-secret\"" +
                ",\"secretType\":\"gpg\"}";

        Credentials credentials = mapper.readValue(json, Credentials.class);

        assertThat(credentials.getRealm()).isEqualTo("test");
        assertThat(credentials.getIdentity()).isEqualTo("mirila-user-id");
        assertThat(credentials.getSecret()).isEqualTo("this-is-my-secret");
        assertThat(credentials.getSecretType()).isEqualTo(GPG);

        String jsonClone = mapper.writeValueAsString(credentials);
        assertThat(jsonClone).isEqualTo(json);
    }

}
