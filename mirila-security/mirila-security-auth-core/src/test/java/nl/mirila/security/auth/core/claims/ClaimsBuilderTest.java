package nl.mirila.security.auth.core.claims;

import nl.mirila.security.auth.core.authorization.Roles;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class ClaimsBuilderTest {

    private Claims claims;

    @BeforeEach
    void setUp() {
        claims = ClaimsBuilder.forRealm("test-realm")
                .withIssuer("test-app")
                .withLocaleId("nl_NL")
                .withSubject("mirila-user-name")
                .withAudience("aud1", "aud2")
                .withExpirationTime(1585393927L)
                .withSubjectName("Mirila User")
                .withSubjectId("123")
                .withGroupIds("group1", "group2")
                .asAdministrator()
                .getClaims();
    }

    @Test
    void testRealm() {
        assertThat(claims.getRealm()).isEqualTo("test-realm");
    }

    @Test
    void testIssuer() {
        assertThat(claims.getIssuer()).isEqualTo("test-app");
    }

    @Test
    void testLocaleId() {
        assertThat(claims.getLocaleId()).isEqualTo("nl_NL");
    }

    @Test
    void testSubject() {
        assertThat(claims.getSubject()).isEqualTo("mirila-user-name");
    }

    @Test
    void testSubjectName() {
        assertThat(claims.getSubjectName()).isEqualTo("Mirila User");
    }

    @Test
    void testSubjectId() {
        assertThat(claims.getSubjectId()).isEqualTo("123");
    }

    @Test
    void testAudience() {
        assertThat(claims.getAudience()).containsOnly("aud1", "aud2");
    }

    @Test
    void testExpirationTime() {
        assertThat(claims.getExpirationTime()).isEqualTo(1585393927L);
    }

    @Test
    void testRoles() {
        assertThat(claims.getRoles()).containsOnly(Roles.ADMIN);
    }

    @Test
    void testGroupIds() {
        assertThat(claims.getGroupIds()).containsOnly("group1", "group2");
    }

}
