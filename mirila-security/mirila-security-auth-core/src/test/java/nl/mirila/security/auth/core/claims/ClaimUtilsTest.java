package nl.mirila.security.auth.core.claims;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static nl.mirila.security.auth.core.claims.ClaimUtils.getClaimByKey;
import static org.assertj.core.api.Assertions.assertThat;

class ClaimUtilsTest {

    private static Claims claims;

    @BeforeAll
    static void beforeAll() {
        claims = ClaimsBuilder.forRealm("test-realm")
                .withSubjectId("123")
                .withSubjectName("Test")
                .withExpirationTime(123456L)
                .withRoles("admin", "super-user", "manager")
                .getClaims();
    }

    @Test
    void testGetClaimByKeyWithValidStringClaim() {
        assertThat(getClaimByKey(claims, Claims.CLAIM_SUBJECT_NAME))
                .isPresent()
                .contains("Test");
    }

    @Test
    void testGetClaimByKeyWithInvalidStringClaim() {
        assertThat(getClaimByKey(claims, Claims.CLAIM_SUBJECT)).isNotPresent();
    }

    @Test
    void testGetClaimByKeyWithValidLongClaim() {
        assertThat(getClaimByKey(claims, Claims.CLAIM_EXPIRATION_TIME))
                .isPresent()
                .contains("123456");
    }

    @Test
    void testGetClaimByKeyWithValidSetClaim() {
        assertThat(getClaimByKey(claims, Claims.CLAIM_ROLES))
                .isPresent()
                .contains("admin,manager,super-user");
    }

    @Test
    void testGetClaimByKeyWithNotExistingKey() {
        assertThat(getClaimByKey(claims, "not-existing-key")).isNotPresent();
    }

    @Test
    void testGetClaimByKeyWithEmptyKey() {
        assertThat(getClaimByKey(claims, "")).isNotPresent();
    }

    @Test
    void testGetClaimByKeyWithNullKey() {
        assertThat(getClaimByKey(claims, null)).isNotPresent();
    }

}
