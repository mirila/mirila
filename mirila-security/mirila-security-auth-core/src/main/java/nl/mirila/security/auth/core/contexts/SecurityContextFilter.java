package nl.mirila.security.auth.core.contexts;

import com.google.inject.Inject;
import com.google.inject.Injector;
import nl.mirila.core.settings.Settings;
import nl.mirila.security.auth.core.authorization.AuthorizationTokenProvider;
import nl.mirila.security.auth.core.claims.Claims;
import nl.mirila.security.auth.core.exceptions.InvalidTokenException;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.util.List;

import static javax.ws.rs.core.HttpHeaders.AUTHORIZATION;
import static org.apache.commons.lang3.StringUtils.isEmpty;

/**
 * A filter to create a {@link SecurityContext} based on an authorization token.
 */
@Provider
@PreMatching
public class SecurityContextFilter implements ContainerRequestFilter {

    private static final Logger logger = LogManager.getLogger(SecurityContextFilter.class);

    private static final String UNSUPPORTED_REALM = "Received a request with unsupported realm {}";
    private static final String HTTP_HEADER_REALM = "Realm";

    private final Injector injector;

    /**
     * Initialize a new instance.
     */
    @Inject
    public SecurityContextFilter(Injector injector) {
        this.injector = injector;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void filter(ContainerRequestContext requestContext) {
        SecurityContextProvider securityContextProvider = injector.getInstance(SecurityContextProvider.class);
        String authorization = requestContext.getHeaderString(AUTHORIZATION);
        if (isEmpty(authorization)) {
            initializeSecurityContextForGuest(requestContext, securityContextProvider);
        } else {
            initializeSecurityContextForSubject(requestContext, securityContextProvider, authorization);
        }
    }

    /**
     * Initialize the given {@link SecurityContextProvider} for a guest.
     * <p>
     * @param requestContext The context of the request.
     * @param provider The provider of the security context.
     */
    private void initializeSecurityContextForGuest(ContainerRequestContext requestContext,
                                                   SecurityContextProvider provider) {
        Settings settings = injector.getInstance(Settings.class);
        List<String> supportedRealms = settings.getStringList("realms");
        String realm;
        if (supportedRealms.isEmpty()) {
            realm = StringUtils.EMPTY;
        } else if (supportedRealms.size() == 1) {
            realm = supportedRealms.get(0);
        } else {
            realm = requestContext.getHeaderString(HTTP_HEADER_REALM);
            if (!supportedRealms.contains(realm)) {
                requestContext.abortWith(getUnsupportedRealmResponse(realm));
                logger.warn(UNSUPPORTED_REALM, realm);
                return;
            }
        }
        provider.generateContextForGuest(realm);
    }

    /**
     * Initialize the given {@link SecurityContextProvider} for an authenticated subject.
     * <p>
     * @param requestContext The context of the request.
     * @param provider The provider of the security context.
     * @param authorization The authorization string.
     */
    private void initializeSecurityContextForSubject(ContainerRequestContext requestContext,
                                                     SecurityContextProvider provider,
                                                     String authorization) {
        String token = (authorization.contains(" "))
                ? authorization.split(" ", 2)[1]
                : authorization;

        AuthorizationTokenProvider authorizationTokenProvider = injector.getInstance(AuthorizationTokenProvider.class);
        Claims claims;
        try {
            claims = authorizationTokenProvider.parseToken(token);
        } catch (InvalidTokenException e) {
            logger.warn(e.getMessage());
            requestContext.abortWith(getInvalidTokenResponse());
            initializeSecurityContextForGuest(requestContext, provider);
            return;
        }
        provider.generateContextWithClaims(claims);
    }

    /**
     * Return a {@link Response} that the provided authorization token is invalid.
     */
    private Response getUnsupportedRealmResponse(String realm) {
        return Response
                .status(Response.Status.BAD_REQUEST)
                .type(MediaType.TEXT_PLAIN_TYPE)
                .entity("The realm '" + realm + "' is not supported.")
                .build();
    }

    /**
     * Return a {@link Response} that the provided authorization token is invalid.
     */
    private Response getInvalidTokenResponse() {
        return Response
                .status(Response.Status.UNAUTHORIZED)
                .type(MediaType.TEXT_PLAIN_TYPE)
                .entity("The authorization token is invalid.")
                .build();
    }

}
