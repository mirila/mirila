package nl.mirila.security.auth.core.exceptions;

public class InvalidTokenException extends AbstractAuthenticationFailedException {

    public InvalidTokenException(String reason) {
        super("The token is invalid: " + reason);
    }

}
