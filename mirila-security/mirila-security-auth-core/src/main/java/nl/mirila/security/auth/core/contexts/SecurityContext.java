package nl.mirila.security.auth.core.contexts;

import nl.mirila.core.datatype.Id;
import nl.mirila.security.auth.core.authorization.Roles;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * The SecurityContext is an interface for security contexts.
 */
public interface SecurityContext {

    /**
     * Return the realm for this context.
     */
    String getRealm();

    /**
     * Return the locale id that is used for this context.
     */
    String getLocaleId();

    /**
     * Return the id of the credential.
     */
    Id getCredentialId();

    /**
     * Return the name of the credential, wrapped in an {@link Optional}.
     */
    Optional<String> getCredentialName();

    /**
     * Return the name of the subject, wrapped in an {@link Optional}.
     */
    Optional<String> getSubjectName();

    /**
     * Return the list of roles that the subject has.
     */
    List<String> getRoles();

    /**
     * Return the id of the user.
     */
    Id getUserId();

    /**
     * Return the list of group ids to which the subject is a group member.
     */
    List<String> getGroupIds();

    /**
     * Return true if the subject is a member of the group with the given id.
     */
    default boolean isGroupMember(String groupId) {
        return getGroupIds().contains(groupId);
    }

    /**
     * Return true if the subject has the given role.
     */
    default boolean hasRole(String role) {
        return getRoles().contains(role);
    }

    /**
     * Return true if the subject is an administrator.
     */
    default boolean isAdministrator() {
        return hasRole(Roles.ADMIN);
    }

    /**
     * Return true if the subject is an super-user.
     * <p>
     * NB: Subjects with the role of administrator are also considered to be super-users.
     */
    default boolean isSuperUser() {
        return getRoles().stream().anyMatch(Arrays.asList(Roles.ADMIN, Roles.SUPER_USER)::contains);
    }

    /**
     * Return true if the subject is a guest.
     */
    default boolean isGuest() {
        return !isAuthenticated();
    }

    /**
     * Return true if the subject is authenticated.
     */
    default boolean isAuthenticated() {
        return getCredentialName().isPresent();
    }

}
