package nl.mirila.security.auth.core.claims;

import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.TimeUnit;

import static nl.mirila.security.auth.core.authorization.Roles.ADMIN;
import static nl.mirila.security.auth.core.authorization.Roles.SUPER_USER;

/**
 * The ClaimsBuilder is a builder for {@link Claims}.
 */
public class ClaimsBuilder {

    private final Claims claims;

    /**
     * Initialize a new instance.
     */
    ClaimsBuilder() {
        claims = new Claims();
    }

    /**
     * Create a new payload.
     */
    public static ClaimsBuilder forRealm(String realm) {
        return (new ClaimsBuilder()).withRealm(realm);
    }

    /**
     * Set the realm and return the payload.
     */
    protected ClaimsBuilder withRealm(String realm) {
        claims.setRealm(realm);
        return this;
    }

    /**
     * Set the locale id and return the payload.
     */
    public ClaimsBuilder withLocaleId(String localeId) {
        claims.setLocaleId(localeId);
        return this;
    }

    /**
     * Set the issuer and return the payload.
     */
    public ClaimsBuilder withIssuer(String issuer) {
        claims.setIssuer(issuer);
        return this;
    }

    /**
     * Set the subject and return the payload.
     */
    public ClaimsBuilder withSubject(String subject) {
        claims.setSubject(subject);
        return this;
    }

    /**
     * Set the audience and return the payload.
     */
    public ClaimsBuilder withAudience(String... audience) {
        return withAudience(Arrays.asList(audience));
    }

    /**
     * Set the audience and return the payload.
     */
    public ClaimsBuilder withAudience(Collection<String> audience) {
        if (audience != null) {
            claims.getAudience().addAll(audience);
        }
        return this;
    }

    /**
     * Set the expiration time in milliseconds, and return the payload.
     */
    public ClaimsBuilder withExpirationTime(Long expirationTime) {
        if (expirationTime != null) {
            claims.setExpirationTime(expirationTime);
        }
        return this;
    }

    /**
     * Set the expiration time with an offset from now, and return the payload.
     */
    public ClaimsBuilder withExpirationTimeFromNow(int offsetInMinutes) {
        long time = System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(offsetInMinutes);
        claims.setExpirationTime(time);
        return this;
    }

    /**
     * Set the subject name and return the payload.
     */
    public ClaimsBuilder withSubjectName(String subjectName) {
        claims.setSubjectName(subjectName);
        return this;
    }

    /**
     * Set the subject id and return the payload.
     */
    public ClaimsBuilder withSubjectId(String subjectId) {
        claims.setSubjectId(subjectId);
        return this;
    }

    /**
     * Set the roles and return the payload.
     */
    public ClaimsBuilder withRoles(String... roles) {
        return withRoles(Arrays.asList(roles));
    }

    /**
     * Set the roles and return the payload.
     */
    public ClaimsBuilder withRoles(Collection<String> roles) {
        if ((roles != null) && (!roles.isEmpty())) {
            claims.getRoles().addAll(roles);
        }
        return this;
    }

    /**
     * Set the user id and return the payload.
     */
    public ClaimsBuilder withUserId(String userId) {
        claims.setUserId(userId);
        return this;
    }

    /**
     * Set the group ids and return the payload.
     */
    public ClaimsBuilder withGroupIds(String... groupIds) {
        return withGroupIds(Arrays.asList(groupIds));
    }

    /**
     * Set the group ids and return the payload.
     */
    public ClaimsBuilder withGroupIds(Collection<String> groupIds) {
        claims.getGroupIds().addAll(groupIds);
        return this;
    }

    /**
     * Add the special role of administrator.
     */
    public ClaimsBuilder asAdministrator() {
        claims.getRoles().add(ADMIN);
        return this;
    }

    /**
     * Add the special role of a super user.
     */
    public ClaimsBuilder asSuperUser() {
        claims.getRoles().add(SUPER_USER);
        return this;
    }

    /**
     * Return the built claims.
     */
    public Claims getClaims() {
        return claims;
    }

}
