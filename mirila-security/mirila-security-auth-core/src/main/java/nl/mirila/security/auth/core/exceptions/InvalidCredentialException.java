package nl.mirila.security.auth.core.exceptions;

public class InvalidCredentialException extends AbstractAuthenticationFailedException {

    public InvalidCredentialException() {
        super("The given credentials are invalid.");
    }

}
