package nl.mirila.security.auth.core.claims;

import nl.mirila.security.auth.core.authorization.Credentials;
import nl.mirila.security.auth.core.exceptions.AbstractAuthenticationFailedException;
import nl.mirila.security.auth.core.exceptions.InvalidCredentialException;

import java.util.concurrent.TimeUnit;

/**
 * The Authenticator authenticates a subject and returns the related {@link Claims}.
 */
public interface Authenticator {

    /**
     * Authenticate the subject with the given {@link Credentials}, and return the {@link Claims} that are relevant.
     * <p>
     * <p>If the credentials are invalid, an {@link InvalidCredentialException} must be thrown.
     * <p>
     * @param credentials The credentials.
     * @return The Mirila claims.
     */
    Claims authenticate(Credentials credentials) throws AbstractAuthenticationFailedException;

    /**
     * Return the lifespan of a set of {@link Claims} in seconds.
     */
    default long getExpiresAfter() {
        return TimeUnit.HOURS.toSeconds(1);
    }

}
