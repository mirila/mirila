package nl.mirila.security.auth.core;

import com.google.inject.AbstractModule;
import com.google.inject.multibindings.OptionalBinder;
import com.google.inject.name.Names;
import nl.mirila.security.auth.core.contexts.RealmProvider;
import nl.mirila.security.auth.core.contexts.SecurityContext;
import nl.mirila.security.auth.core.contexts.SecurityContextProvider;
import nl.mirila.security.auth.core.hashing.BcryptPasswordHashService;
import nl.mirila.security.auth.core.hashing.PasswordHashService;

public class SecurityCoreModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(SecurityContext.class).toProvider(SecurityContextProvider.class);
        bind(String.class).annotatedWith(Names.named("realm")).toProvider(RealmProvider.class);

        OptionalBinder.newOptionalBinder(binder(), PasswordHashService.class)
                .setDefault()
                .to(BcryptPasswordHashService.class);
    }

}
