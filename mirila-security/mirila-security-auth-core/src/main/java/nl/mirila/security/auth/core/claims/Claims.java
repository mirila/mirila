package nl.mirila.security.auth.core.claims;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

/**
 * Represent all claims for the payload.
 */
@JsonInclude(NON_NULL)
public class Claims {

    public static final String CLAIM_REALM = "rlm";
    public static final String CLAIM_ISSUER = "iss";
    public static final String CLAIM_SUBJECT = "sub";
    public static final String CLAIM_AUDIENCE = "aud";
    public static final String CLAIM_EXPIRATION_TIME = "exp";
    public static final String CLAIM_SUBJECT_NAME = "snm";
    public static final String CLAIM_SUBJECT_ID = "sid";
    public static final String CLAIM_ROLES = "rls";
    public static final String CLAIM_USER_ID = "uid";
    public static final String CLAIM_GROUP_IDS = "grp";
    public static final String CLAIM_LOCALE_ID = "lcl";

    @JsonProperty(CLAIM_REALM)
    private String realm;

    @JsonProperty(CLAIM_ISSUER)
    private String issuer;

    @JsonProperty(CLAIM_SUBJECT)
    private String subject;

    @JsonProperty(CLAIM_SUBJECT_NAME)
    private String subjectName;

    @JsonProperty(CLAIM_SUBJECT_ID)
    private String subjectId;

    @JsonProperty(CLAIM_AUDIENCE)
    private final Set<String> audience;

    @JsonProperty(CLAIM_EXPIRATION_TIME)
    private Long expirationTime;

    @JsonProperty(CLAIM_ROLES)
    private final Set<String> roles;

    @JsonProperty(CLAIM_USER_ID)
    private String userId;

    @JsonProperty(CLAIM_GROUP_IDS)
    private final Set<String> groupIds;

    @JsonProperty(CLAIM_LOCALE_ID)
    private String localeId;

    /**
     * Initialize a new instance.
     */
    Claims() {
        audience = new TreeSet<>();
        groupIds = new TreeSet<>();
        roles = new TreeSet<>();
        expirationTime = System.currentTimeMillis() + TimeUnit.HOURS.toMillis(3); // Default 3 hours.
    }

    /**
     * Return the realm.
     */
    public String getRealm() {
        return realm;
    }

    /**
     * Set the realm.
     */
    public void setRealm(String realm) {
        this.realm = realm;
    }

    /**
     * Return the issuing application.
     */
    public String getIssuer() {
        return issuer;
    }

    /**
     * Set the issuing application.
     */
    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }

    /**
     * Return the identification of the subject.
     */
    public String getSubject() {
        return subject;
    }

    /**
     * Set the identification of the subject.
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * Return the name of the subject.
     */
    public String getSubjectName() {
        return subjectName;
    }

    /**
     * Set the name of the subject.
     */
    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    /**
     * Return the subjectId.
     */
    public String getSubjectId() {
        return subjectId;
    }

    /**
    /**
     * Set the given subjectId.
     */
    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    /**
     * Return the audience, the set of application for which these claims are intended.
     */
    public Set<String> getAudience() {
        return (audience != null)
                ? audience
                : Collections.emptySet();
    }

    /**
     * Return the expiration time.
     */
    public Long getExpirationTime() {
        return expirationTime;
    }

    /**
     * Set the expiration time.
     */
    public void setExpirationTime(long expirationTime) {
        this.expirationTime = expirationTime;
    }

    /**
     * Return the roles.
     */
    public Set<String> getRoles() {
        return roles;
    }

    /**
     * Return the id of the user.
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Set the given id of the user.
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * Return the group ids.
     */
    public Set<String> getGroupIds() {
        return groupIds;
    }

    /**
     * Return the locale id.
     */
    public String getLocaleId() {
        return localeId;
    }

    /**
     * Set the locale id.
     */
    public void setLocaleId(String localeId) {
        this.localeId = localeId;
    }

}
