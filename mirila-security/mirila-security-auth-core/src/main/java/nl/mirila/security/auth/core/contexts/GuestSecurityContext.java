package nl.mirila.security.auth.core.contexts;

import nl.mirila.core.datatype.Id;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * A representation of the {@link SecurityContext} for subjects that not have been authorized.
 */
public class GuestSecurityContext implements SecurityContext {

    private final String realm;
    private final String localeId;

    /**
     * Initialize a new instance, with the given realm.
     */
    public GuestSecurityContext(String realm, String localeId) {
        this.realm = Optional.ofNullable(realm).orElse("");
        this.localeId = localeId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getRealm() {
        return realm;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getLocaleId() {
        return localeId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<String> getCredentialName() {
        return Optional.empty();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<String> getSubjectName() {
        return Optional.empty();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Id getCredentialId() {
        return Id.empty();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> getRoles() {
        return Collections.emptyList();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Id getUserId() {
        return Id.empty();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> getGroupIds() {
        return Collections.emptyList();
    }

}
