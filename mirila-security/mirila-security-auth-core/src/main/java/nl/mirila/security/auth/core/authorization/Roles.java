package nl.mirila.security.auth.core.authorization;

/**
 * The standard roles within any Mirila system.
 */
public final class Roles {

    /**
     * The role ADMIN grants access to everything.
     */
    public static final String ADMIN = "admin";

    /**
     * The role SUPER_USER grants access to all data related functionality, excluding system administration.
     */
    public static final String SUPER_USER = "super-user";


    /**
     * This utility class may not be instantiated.
     */
    private Roles() {
        throw new UnsupportedOperationException();
    }

}
