package nl.mirila.security.auth.core.hashing;

/**
 * The PasswordHash deals with.
 */
public interface PasswordHashService {

    /**
     * Return the hash for a given password.
     * <p>
     * @param password The password.
     * @return The hash.
     */
    String hashPassword(String password);

    /**
     * Return true if the given password and hash verify correctly.
     * <p>
     * @param password The password.
     * @param hash The hash.
     * @return True if correctly verified.
     */
    boolean verifyPassword(String password, String hash);

}
