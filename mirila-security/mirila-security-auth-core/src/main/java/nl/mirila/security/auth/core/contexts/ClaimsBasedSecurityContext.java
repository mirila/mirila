package nl.mirila.security.auth.core.contexts;

import nl.mirila.core.datatype.Id;
import nl.mirila.security.auth.core.claims.Claims;
import org.apache.commons.lang3.StringUtils;
import org.jboss.resteasy.plugins.guice.RequestScoped;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * A {@link SecurityContext} based on {@link Claims}.
 */
@RequestScoped
public class ClaimsBasedSecurityContext implements SecurityContext {

    private final Claims claims;
    private final String defaultRealm;
    private final String defaultLocaleId;

    /**
     * Initialize a new instance.
     */
    public ClaimsBasedSecurityContext(Claims claims, String defaultRealm, String defaultLocaleId) {
        this.claims = claims;
        this.defaultRealm = defaultRealm;
        this.defaultLocaleId = defaultLocaleId;
    }

    /**
     * Initialize a new instance.
     */
    public ClaimsBasedSecurityContext(Claims claims) {
        this.claims = claims;
        this.defaultRealm = Optional.ofNullable(claims.getRealm()).orElse("");
        this.defaultLocaleId = Optional.ofNullable(claims.getLocaleId()).orElse("");
    }

    /**
     * Return the claims.
     */
    public Claims getClaims() {
        return claims;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getRealm() {
        String realm = claims.getRealm();
        return (StringUtils.isNotBlank(realm)) ? realm : defaultRealm;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getLocaleId() {
        String localeId = claims.getLocaleId();
        return (StringUtils.isNotBlank(localeId)) ? localeId : defaultLocaleId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<String> getCredentialName() {
        return Optional.ofNullable(claims.getSubject());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<String> getSubjectName() {
        return Optional.ofNullable(claims.getSubjectName());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Id getCredentialId() {
        return Id.of(claims.getSubjectId());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> getRoles() {
        return new ArrayList<>(claims.getRoles());
    }

    @Override
    public Id getUserId() {
        return Id.of(claims.getUserId());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> getGroupIds() {
        return new ArrayList<>(claims.getGroupIds());
    }

}
