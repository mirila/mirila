package nl.mirila.security.auth.core.authorization;

import nl.mirila.security.auth.core.claims.Claims;
import nl.mirila.security.auth.core.exceptions.AbstractAuthenticationFailedException;
import nl.mirila.security.auth.core.exceptions.InvalidCredentialException;
import nl.mirila.security.auth.core.exceptions.InvalidTokenException;

/**
 * The AuthorizationTokenProvider is an interface for generating and parsing authorization tokens.
 */
public interface AuthorizationTokenProvider {

    /**
     * Generate a token for the given {@link Credentials}. This method performs both the validation of the credentials
     * as well as the generation of the related authorization token.
     * <p>
     * <p>If the authentication fails an {@link AbstractAuthenticationFailedException} is thrown.
     * <p>
     * @param credentials The credential to generate a token for.
     * @return The generated token.
     */
    String generateToken(Credentials credentials) throws AbstractAuthenticationFailedException;

    /**
     * Generate a token for the given {@link Claims}.
     * <p>
     * @param claims The credential to generate a token for.
     * @return The generated token.
     */
    String generateToken(Claims claims);

    /**
     * Return the claims that are in the token.
     * <p>If the token is is invalid, an {@link InvalidCredentialException} is thrown.
     * <p>
     * @param token The token
     * @return The claims related to the token.
     */
    Claims parseToken(String token) throws InvalidTokenException;

}
