package nl.mirila.security.auth.core.contexts;

import com.google.inject.Inject;
import com.google.inject.Provider;
import nl.mirila.core.settings.ApplicationSettings;
import nl.mirila.security.auth.core.claims.Claims;
import org.jboss.resteasy.plugins.guice.RequestScoped;


/**
 * The SecurityContextProvider returns the {@link SecurityContext} for the current request.
 * <p>
 * By default, a {@link GuestSecurityContext} is provided. However, by processing {@link Claims} for an
 * authenticated subject, this provider may also return {@link ClaimsBasedSecurityContext}.
 */
@RequestScoped
public class SecurityContextProvider implements Provider<SecurityContext> {

    private final String defaultRealm;
    private final String defaultLocaleId;
    private SecurityContext securityContext;

    @Inject
    public SecurityContextProvider(ApplicationSettings settings) {
        defaultRealm = settings.getRealm();
        defaultLocaleId = settings.getLocale();
        this.securityContext = new GuestSecurityContext(defaultRealm, defaultLocaleId);
    }

    /**
     * Generate the security context with the given claims. If these claims are related to an authorized subject,
     * the security context that is used in the provider, will be transformed to a
     * {@link ClaimsBasedSecurityContext}, which includes the given claims.
     * <p>
     * @param claims The claims.
     */
    public void generateContextWithClaims(Claims claims) {
        securityContext = new ClaimsBasedSecurityContext(claims, defaultRealm, defaultLocaleId);
    }

    /**
     * Generate a security context for a guest, within the given realm.
     * <p>
     * @param realm The realm.
     */
    public void generateContextForGuest(String realm) {
        securityContext = new GuestSecurityContext(realm, defaultLocaleId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SecurityContext get() {
        return securityContext;
    }

}
