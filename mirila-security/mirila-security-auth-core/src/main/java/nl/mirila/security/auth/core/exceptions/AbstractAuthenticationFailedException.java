package nl.mirila.security.auth.core.exceptions;

/**
 * The {@link AbstractAuthenticationFailedException} may be thrown to indicate that an a
 */
public abstract class AbstractAuthenticationFailedException extends Exception {

    public AbstractAuthenticationFailedException(String message) {
        super(message);
    }

}
