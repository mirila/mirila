 package nl.mirila.security.auth.core.authorization;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.stream.Stream;

/**
 * The type of secret.
 */
public enum SecretType {

    PASSWORD("password"),
    GPG("gpg"),
    SSH("ssh");

    private final String value;

    /**
     * Initialize a new instance with the given value.
     */
    SecretType(String value) {
        this.value = value;
    }

    /**
     * Return the string value of the secret type.
     */
    @Override
    @JsonValue
    public String toString() {
        return value;
    }

    /**
     * Return the secret type that matches the given name.
     * <p>
     * If the given name doesn't represents a secret type, an {@link IllegalArgumentException} is thrown.
     * <p>
     * @param name The name.
     * @return The secret type.
     */
    public static SecretType findByName(String name) {
        return Stream.of(SecretType.values())
                .filter((t) -> t.toString().equals(name))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }

}
