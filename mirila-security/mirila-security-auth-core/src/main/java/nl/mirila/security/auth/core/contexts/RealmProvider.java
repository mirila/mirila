package nl.mirila.security.auth.core.contexts;

import com.google.inject.Inject;
import org.jboss.resteasy.plugins.guice.RequestScoped;

import javax.inject.Provider;

/**
 * The RealmProvider can be used to make the realm an injectable named string.
 * <p>
 * Preferably, the {@link SecurityContext} is used to fetch the realm, but in some case, like in mirila-cache-core,
 * there's no knowledge about security contexts. So, alternatively, we can inject the named String "realm".
 */
@RequestScoped
public class RealmProvider implements Provider<String> {

    private final SecurityContext securityContext;

    /**
     * Initialize a new instance.
     */
    @Inject
    public RealmProvider(SecurityContext securityContext) {
        this.securityContext = securityContext;
    }

    /**
     * Return the realm, based on the current security context.
     */
    @Override
    public String get() {
        return securityContext.getRealm();
    }

}
