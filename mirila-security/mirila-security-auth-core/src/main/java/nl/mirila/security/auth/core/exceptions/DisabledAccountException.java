package nl.mirila.security.auth.core.exceptions;

public class DisabledAccountException extends AbstractAuthenticationFailedException {

    public DisabledAccountException() {
        super("The account is disabled.");
    }

}
