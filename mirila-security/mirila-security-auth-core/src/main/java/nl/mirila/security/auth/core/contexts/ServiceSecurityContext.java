package nl.mirila.security.auth.core.contexts;

import nl.mirila.core.datatype.Id;
import nl.mirila.security.auth.core.authorization.Roles;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * A representation of the {@link SecurityContext} for services that automatically have administrative rights.
 */
public class ServiceSecurityContext implements SecurityContext {

    private final String realm;
    private final String localeId;
    private final Id actorId;
    private final String serviceId;
    private final String serviceName;

    /**
     * Initialize a new instance, with the given realm.
     */
    public ServiceSecurityContext(String realm, String localeId, Id actorId, String serviceId, String serviceName) {
        this.realm = realm;
        this.localeId = localeId;
        this.actorId = actorId;
        this.serviceId = serviceId;
        this.serviceName = serviceName;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getRealm() {
        return realm;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getLocaleId() {
        return localeId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<String> getCredentialName() {
        return Optional.of(serviceId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<String> getSubjectName() {
        return Optional.of(serviceName);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Id getCredentialId() {
        return actorId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> getRoles() {
        return Collections.singletonList(Roles.ADMIN);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Id getUserId() {
        return Id.empty();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> getGroupIds() {
        return Collections.emptyList();
    }

}
