package nl.mirila.security.auth.core.hashing;

import at.favre.lib.crypto.bcrypt.BCrypt;
import com.google.inject.Inject;
import org.apache.commons.lang3.StringUtils;

/**
 * A PasswordHashService that uses BCrypt.
 */
public class BcryptPasswordHashService implements PasswordHashService {

    private final BcryptSettings settings;

    /**
     * Initialize a new instance.
     */
    @Inject
    public BcryptPasswordHashService(BcryptSettings settings) {
        this.settings = settings;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String hashPassword(String password) {
        return BCrypt
                .withDefaults()
                .hashToString(settings.getRounds(), password.toCharArray());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean verifyPassword(String password, String hash) {
        if (StringUtils.isAnyBlank(password, hash)) {
            return false;
        }
        return BCrypt.verifyer()
                .verify(password.toCharArray(), hash)
                .verified;
    }

}
