package nl.mirila.security.auth.core.hashing;

import com.google.inject.Inject;
import nl.mirila.core.settings.Settings;

/**
 * Settings that are needed for the internal messaging system.
 */
public class BcryptSettings {

    public static final String KEY_ROUNDS = "bcrypt.rounds";

    private static final int DEFAULT_ROUNDS = 12;

    private final Settings settings;

    /**
     * Initialize a new instance.
     */
    @Inject
    public BcryptSettings(Settings settings) {
        this.settings = settings;
    }

    /**
     * Return the amount of Bcrypt rounds to use.
     * <p>
     * Setting: {@value KEY_ROUNDS}. Default: {@value DEFAULT_ROUNDS}.
     */
    public int getRounds() {
        return settings.getInteger(KEY_ROUNDS).orElse(DEFAULT_ROUNDS);
    }

}

