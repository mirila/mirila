package nl.mirila.security.auth.core.claims;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.util.ReflectionUtil;

import java.lang.reflect.Field;
import java.util.Optional;
import java.util.Set;

public class ClaimUtils {

    /**
     * Return the string representation of the value for the claim with the given key, wrapped in an {@link Optional}.
     * <p>
     * @param claimKey The key of the claim.
     * @return The value of the claim.
     */
    public static Optional<String> getClaimByKey(Claims claims, String claimKey) {
        Field[] fields = Claims.class.getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(JsonProperty.class)) {
                JsonProperty annotation = field.getAnnotation(JsonProperty.class);
                if (annotation.value().equals(claimKey)) {
                    Object object = ReflectionUtil.getFieldValue(field, claims);
                    if (object == null) {
                        return Optional.empty();
                    } else if (object instanceof Set set) {
                        return Optional.of(StringUtils.join(set, ","));
                    } else {
                        return Optional.of(object.toString());
                    }
                }
            }
        }
        return Optional.empty();
    }

}
