package nl.mirila.security.auth.core.authorization;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The Credentials object is a wrapper for information that is needed to authenticate a subject.
 */
public class Credentials {

    @JsonProperty
    private String realm;

    @JsonProperty
    private String identity;

    @JsonProperty
    private String secret;

    @JsonProperty
    private SecretType secretType;


    /**
     * Initialize a new instance, with empty values. We use this empty constructor for Jackson.
     **/
    private Credentials() {
    }

    /**
     * Initialize a new instance, for credentials with the secret of type {@link SecretType#PASSWORD}.
     * <p>
     * @param realm The realm for which the subject authenticates.
     * @param identity The identification of the subject, like a username.
     * @param secret The secret of the subject, like a username.
     */
    public Credentials(String realm, String identity, String secret) {
        this.realm = realm;
        this.identity = identity;
        this.secret = secret;
        this.secretType = SecretType.PASSWORD;
    }

    /**
     * Initialize a new instance.
     * <p>
     * @param realm The realm for which the subject authenticates.
     * @param identity The identification of the subject, like a username.
     * @param secret The secret of the subject, like a username.
     * @param secretType The {@link SecretType} of the secret.
     */
    public Credentials(String realm, String identity, String secret, SecretType secretType) {
        this.realm = realm;
        this.identity = identity;
        this.secret = secret;
        this.secretType = secretType;
    }

    /**
     * Return the realm for which the subject authenticates.
     */
    public String getRealm() {
        return (realm == null) ? "" : realm;
    }

    /**
     * Return the identification of the subject, like a username.
     */
    public String getIdentity() {
        return (identity == null) ? "" : identity;
    }

    /**
     * Return the secret knowledge of the subject, like a password.
     */
    public String getSecret() {
        return (secret == null) ? "" : secret;
    }

    /**
     * Return the {@link SecretType} of the secret.
     */
    public SecretType getSecretType() {
        return (secretType == null) ? SecretType.PASSWORD : secretType;
    }

}
