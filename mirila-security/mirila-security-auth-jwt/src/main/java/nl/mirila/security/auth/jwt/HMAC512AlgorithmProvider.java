package nl.mirila.security.auth.jwt;

import com.auth0.jwt.algorithms.Algorithm;
import com.google.inject.Inject;

/**
 * Provides an {@link Algorithm} based on HMAC512.
 */
public class HMAC512AlgorithmProvider implements AlgorithmProvider {

    private final JwtSettings settings;

    /**
     * Initialize a new instance.
     */
    @Inject
    HMAC512AlgorithmProvider(JwtSettings settings) {
        this.settings = settings;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Algorithm get() {
        return Algorithm.HMAC512(settings.getSecret());
    }

}
