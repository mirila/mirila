package nl.mirila.security.auth.jwt;

import com.auth0.jwt.algorithms.Algorithm;
import com.google.inject.Inject;
import org.apache.commons.text.CharacterPredicates;
import org.apache.commons.text.RandomStringGenerator;

import javax.inject.Named;

import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * Provide a simple algorithm, with a random secret.
 * <p>
 * <p>If the secret is not provided via "JWT_SECRET", it is generated once,
 * and will be only valid at while the application runs.
 */
public class SimpleAlgorithmProvider implements AlgorithmProvider {

    public static final String JWT_SECRET = "jwt-secret";

    private static String secret = null;

    /**
     * Initialize a new instance.
     */
    @Inject
    SimpleAlgorithmProvider(@Named(JWT_SECRET) String jwtSecret) {
        if ((isEmpty(secret)) && (isNotBlank(jwtSecret))) {
            secret = jwtSecret;
        } else {
            RandomStringGenerator generator = new RandomStringGenerator.Builder()
                    .withinRange('0', 'z')
                    .filteredBy(CharacterPredicates.LETTERS, CharacterPredicates.DIGITS)
                    .build();
            secret = generator.generate(16);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Algorithm get() {
        return Algorithm.HMAC512(secret);
    }

}
