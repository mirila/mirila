package nl.mirila.security.auth.jwt;

import com.google.inject.Inject;
import nl.mirila.core.exceptions.MissingSettingException;
import nl.mirila.core.settings.ApplicationSettings;
import nl.mirila.core.settings.Settings;

import java.util.HashSet;
import java.util.Set;

/**
 * Settings that are related to Json Web Tokens.
 */
public class JwtSettings extends ApplicationSettings {

    public static final String KEY_JWT_AUDIENCE = "jwt.audience";
    public static final String KEY_JWT_SECRET = "jwt.secret";

    /**
     * Initialize a new instance.
     */
    @Inject
    JwtSettings(Settings settings) {
        super(settings);
    }

    /**
     * Return the recipients for which the JWT is intended. By default, this is at minimal the application name.
     */
    public Set<String> getAudience() {
        Set<String> audience = new HashSet<>(settings.getStringList(KEY_JWT_AUDIENCE));
        audience.add(getAppName());
        return audience;
    }

    /**
     * Return the secret value that is used for the signing of the JWT.
     */
    public String getSecret() {
        return settings.getString(KEY_JWT_SECRET).orElseThrow(() -> new MissingSettingException(KEY_JWT_SECRET));
    }

}
