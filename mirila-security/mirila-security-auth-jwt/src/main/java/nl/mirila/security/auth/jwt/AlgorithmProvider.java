package nl.mirila.security.auth.jwt;

import com.auth0.jwt.algorithms.Algorithm;
import com.google.inject.Provider;

/**
 * Provides an {@link Algorithm}.
 */
public interface AlgorithmProvider extends Provider<Algorithm> {

}
