package nl.mirila.security.auth.jwt;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.google.inject.Inject;
import nl.mirila.security.auth.core.authorization.AuthorizationTokenProvider;
import nl.mirila.security.auth.core.authorization.Credentials;
import nl.mirila.security.auth.core.claims.Authenticator;
import nl.mirila.security.auth.core.claims.Claims;
import nl.mirila.security.auth.core.claims.ClaimsBuilder;
import nl.mirila.security.auth.core.exceptions.AbstractAuthenticationFailedException;
import nl.mirila.security.auth.core.exceptions.InvalidTokenException;
import org.apache.commons.lang3.StringUtils;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static nl.mirila.security.auth.core.claims.Claims.*;
import static org.apache.commons.lang3.ObjectUtils.defaultIfNull;

/**
 * The JwtProvider generates and parses Json Web Tokens from and to {@link Claims}.
 * <p>
 * When supplying credentials, this class uses the {@link Authenticator#authenticate(Credentials)} to fetch the claims.
 */
public class JwtProvider implements AuthorizationTokenProvider {

    private final Algorithm algorithm;
    private final Authenticator authenticator;
    private final JwtSettings jwtSettings;

    /**
     * Initialize a new instance.
     */
    @Inject
    JwtProvider(Algorithm algorithm, Authenticator authenticator, JwtSettings jwtSettings) {
        this.algorithm = algorithm;
        this.authenticator = authenticator;
        this.jwtSettings = jwtSettings;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String generateToken(Credentials credentials) throws AbstractAuthenticationFailedException {
        Claims claims = authenticator.authenticate(credentials);
        return generateToken(claims);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String generateToken(Claims claims) {
        Instant expiresAt = Instant.now().plusSeconds(authenticator.getExpiresAfter());
        if (claims.getAudience().isEmpty()) {
            claims.getAudience().addAll(jwtSettings.getAudience());
        }
        return JWT.create()
                .withClaim(CLAIM_REALM, claims.getRealm())
                .withIssuer(claims.getIssuer())
                .withSubject(claims.getSubject())
                .withAudience(claims.getAudience().toArray(String[]::new))
                .withExpiresAt(Date.from(expiresAt))
                .withClaim(CLAIM_SUBJECT_NAME, claims.getSubjectName())
                .withClaim(CLAIM_SUBJECT_ID, claims.getSubjectId())
                .withArrayClaim(CLAIM_ROLES, claims.getRoles().toArray(String[]::new))
                .withClaim(CLAIM_USER_ID, claims.getUserId())
                .withArrayClaim(CLAIM_GROUP_IDS, claims.getGroupIds().toArray(String[]::new))
                .withClaim(CLAIM_LOCALE_ID, claims.getLocaleId())
                .sign(algorithm);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Claims parseToken(String token) throws InvalidTokenException {
        JWTVerifier verifier = JWT.require(algorithm)
                .withClaimPresence(CLAIM_SUBJECT_ID)
                .withAnyOfAudience(jwtSettings.getAudience().toArray(String[]::new))
                .acceptLeeway(TimeUnit.MINUTES.toSeconds(1)) // Accept 1 minute leeway.
                .build();

        DecodedJWT jwt;
        try {
            jwt = verifier.verify(token);
        } catch (JWTVerificationException e) {
            throw new InvalidTokenException(e.getMessage());
        }

        String realm = StringUtils.defaultString(jwt.getClaim(CLAIM_REALM).asString());
        String subjectName = StringUtils.defaultString(jwt.getClaim(CLAIM_SUBJECT_NAME).asString());
        String subjectId = StringUtils.defaultString(jwt.getClaim(CLAIM_SUBJECT_ID).asString());
        String userId = StringUtils.defaultString(jwt.getClaim(CLAIM_USER_ID).asString());
        String localeId = StringUtils.defaultString(jwt.getClaim(CLAIM_LOCALE_ID).asString());

        long expiresAt = defaultIfNull(jwt.getExpiresAt().getTime(), 0L);

        List<String> roles = (jwt.getClaim(CLAIM_ROLES) == null)
                ? new ArrayList<>()
                : jwt.getClaim(CLAIM_ROLES).asList(String.class);

        List<String> groupIds = (jwt.getClaim(CLAIM_GROUP_IDS) == null)
                ? new ArrayList<>()
                : jwt.getClaim(CLAIM_GROUP_IDS).asList(String.class);

        return ClaimsBuilder.forRealm(realm)
                .withIssuer(jwt.getIssuer())
                .withSubject(jwt.getSubject())
                .withAudience(jwt.getAudience())
                .withExpirationTime(expiresAt)
                .withSubjectName(subjectName)
                .withSubjectId(subjectId)
                .withRoles(roles)
                .withUserId(userId)
                .withGroupIds(groupIds)
                .withLocaleId(localeId)
                .getClaims();
    }

}
