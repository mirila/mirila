package nl.mirila.security.auth.jwt;

import com.auth0.jwt.algorithms.Algorithm;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import nl.mirila.core.settings.Settings;
import nl.mirila.security.auth.core.authorization.Credentials;
import nl.mirila.security.auth.core.claims.Authenticator;
import nl.mirila.security.auth.core.claims.Claims;
import nl.mirila.security.auth.core.claims.ClaimsBuilder;
import nl.mirila.security.auth.core.exceptions.AbstractAuthenticationFailedException;
import nl.mirila.security.auth.core.exceptions.InvalidCredentialException;
import nl.mirila.security.auth.core.exceptions.InvalidTokenException;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static nl.mirila.core.settings.ApplicationSettings.KEY_APP_NAME;
import static nl.mirila.security.auth.jwt.JwtSettings.KEY_JWT_SECRET;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

class JwtProviderTest {

    private JwtProvider provider;

    @BeforeEach
    void setUp() {
        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                Map<String, String> settingsMap = new HashMap<>();
                settingsMap.put(KEY_JWT_SECRET, "This is a secret");
                settingsMap.put(KEY_APP_NAME, "aud1");
                bind(Settings.class).toInstance(Settings.withMap(settingsMap));

                bind(Algorithm.class).toProvider(HMAC512AlgorithmProvider.class);
                bind(Authenticator.class).to(TestClassProviderAuthentication.class);
            }
        });
        provider = injector.getInstance(JwtProvider.class);
    }

    @Test
    void testGenerateToken() throws AbstractAuthenticationFailedException {
        Credentials credentials = new Credentials("test-realm", "mirila-user-id", "secret");
        String token = provider.generateToken(credentials);

        Claims claims = provider.parseToken(token);
        assertThat(claims.getRealm()).isEqualTo("test-realm");
        assertThat(claims.getIssuer()).isEqualTo("test-app");
        assertThat(claims.getSubject()).isEqualTo("mirila-user-id");
        assertThat(claims.getAudience()).containsOnly("aud1", "aud2");
        assertThat(claims.getSubjectName()).isEqualTo("Mirila User");
        assertThat(claims.getExpirationTime() > System.currentTimeMillis()).isTrue();
        assertThat(claims.getGroupIds()).containsOnly("group1", "group2");
    }

    @Test
    void testParseToken() throws InvalidTokenException {
        // Token expires at 9999-12-31 23:59:49 == 253402300799000L.
        @SuppressWarnings("SpellCheckingInspection")
        String token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9." +
                "eyJybG0iOiJ0ZXN0LXJlYWxtIiwic3ViIjoibWlyaWxhLXVzZXItbmFtZSIsImF1" +
                "ZCI6WyJhdWQxIiwiYXVkMiJdLCJzbm0iOiJNaXJpbGEgVXNlciIsImdycCI6WyJn" +
                "cm91cDEiLCJncm91cDIiXSwic2lkIjoibWlyaWxhLXVzZXItaWQiLCJpc3MiOiJ0" +
                "ZXN0LWFwcCIsImV4cCI6MjUzNDAyMzAwNzk5fQ." +
                "1CPPrtQckb5phmHWemkGnJK75_nVqKV-4QoM6UYTs07_UynVSU7mcxM2IYIg0E3j4OSZlMSr0bOCk9abCJgeyw";

        Claims claims = provider.parseToken(token);
        assertThat(claims.getRealm()).isEqualTo("test-realm");
        assertThat(claims.getIssuer()).isEqualTo("test-app");
        assertThat(claims.getSubject()).isEqualTo("mirila-user-name");
        assertThat(claims.getSubjectId()).isEqualTo("mirila-user-id");
        assertThat(claims.getAudience()).containsOnly("aud1", "aud2");
        assertThat(claims.getSubjectName()).isEqualTo("Mirila User");
        assertThat(claims.getExpirationTime()).isEqualTo(253402300799000L);
        assertThat(claims.getGroupIds()).containsOnly("group1", "group2");
    }

    @Test
    void testInvalidCredentials() {
        Credentials credentials = new Credentials("test", "", "");

        assertThatExceptionOfType(InvalidCredentialException.class).isThrownBy(() -> {
            provider.generateToken(credentials);
        });
    }

    @Test
    void testInvalidToken() {
        @SuppressWarnings("SpellCheckingInspection")
        String token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.tempered-payload." +
                "sYSLdHFWFegM02HBHNe8r2mXHLpFt_VvtOQfEWaTGbqsSfeqoBdE0m5KZKZSLRDgRjk7QBKkCnc3BToQNCloyQ";

        assertThatExceptionOfType(InvalidTokenException.class).isThrownBy(() -> {
            provider.parseToken(token);
        });

    }

    public static class TestClassProviderAuthentication implements Authenticator {

        @Override
        public Claims authenticate(Credentials credentials) throws InvalidCredentialException {
            if (StringUtils.isBlank(credentials.getIdentity())) {
                throw new InvalidCredentialException();
            }
            return ClaimsBuilder.forRealm("test-realm")
                    .withIssuer("test-app")
                    .withSubject(credentials.getIdentity())
                    .withAudience("aud1", "aud2")
                    .withExpirationTimeFromNow(30)
                    .withSubjectName("Mirila User")
                    .withSubjectId("345")
                    .withGroupIds("group1", "group2")
                    .getClaims();
        }

    }

}
