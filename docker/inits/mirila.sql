CREATE TABLE IF NOT EXISTS test_users (
  `user_id` int AUTO_INCREMENT PRIMARY KEY,
  `username` varchar(50) NOT NULL,
  `first_name` varchar(200) NULL,
  `last_name` varchar(200) NOT NULL,
  `age` int DEFAULT 0 NULL
);

CREATE TABLE IF NOT EXISTS scheduled_jobs (
  `id` int AUTO_INCREMENT PRIMARY KEY,
  `group` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `status` varchar(15) NULL,
  `run_at` timestamp NOT NULL,
  `started_at` timestamp NULL,
  `processed_at` timestamp NULL,
  `pool_id` varchar(25) NULL,
  `pattern` varchar(100) NULL,
  `data` text NULL,
  INDEX (`group`,`name`,`status`,`run_at`)
);
