package nl.mirila.app.web;

import com.google.inject.AbstractModule;
import nl.mirila.app.service.ServiceAppModule;
import nl.mirila.security.auth.rest.DefaultJwtAuthRestModule;

public class WebAppModule extends AbstractModule {

    @Override
    protected void configure() {
        install(new ServiceAppModule());
        install(new DefaultJwtAuthRestModule());
    }

}
