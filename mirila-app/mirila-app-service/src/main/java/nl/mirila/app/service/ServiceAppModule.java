package nl.mirila.app.service;

import com.google.inject.AbstractModule;
import nl.mirila.cache.core.CacheCoreModule;
import nl.mirila.drivers.core.DriverCoreModule;
import nl.mirila.messaging.core.MessagingCoreModule;
import nl.mirila.metrics.core.MetricsCoreModule;
import nl.mirila.rest.RestModule;

public class ServiceAppModule extends AbstractModule {

    @Override
    protected void configure() {
        install(new CacheCoreModule());
        install(new MessagingCoreModule());
        install(new MetricsCoreModule());
        install(new RestModule());
        install(new DriverCoreModule());
    }

}
